PhotoRT
==============================

PhotoRT is a physically based ray tracer. It is very much a toy at this point being written in my spare time for fun and for exploring the depth of C++ and programming in general. It is all CPU based.

Some renders: [DeviantArt](https://www.deviantart.com/patlefort)

List of notable features:

*  Path tracing and bi-directional path tracing.
*  BVH acceleration structure.
*  Triangle, sphere, plane, disk primitives.
*  Instancing with possibility to change materials for separate instances of same model.
*  Some NUMA support.
*  Fully parallelized. Task based parallelism for certain tasks such as loading and processing scene data and building acceleration structures.
*  XML scene definition. XSL support with Xalan.
*  Extensible through a module and factory system.
*  Color profiles.
*  Optimized with intrinsics, but can be compiled without.
*  Single and double precision.
*  Multi-process rendering.
*  Some networking support. It can have a tree of clients and servers.
*  Scattering in mediums.
*  Multi-layers material. Simulated bounces between layers.
*  Substance stack for entering and exiting substances with priority indexes.
*  Light portals.
*  Tone mapping (simple exposure based, drago).
*  Light groups and separate output for groups.
*  Adaptive sampling based on perceived variance on image.
*  Many light sources sampling (thousands of light sources).
*  Texture and bump mapping.
*  Render state can be saved and loaded.
*  File format support through assimp and openimageio.
*  CLI and GUI.

### Compiling

Required libraries:

*  [Shared Access](https://gitlab.com/patlefort/sharedaccess)
*  [PatLib](https://gitlab.com/patlefort/patlib)
*  [boost](https://www.boost.org/) (at least version 1.74)
*  [range-v3](https://github.com/ericniebler/range-v3)
*  [half](http://half.sourceforge.net/index.html)
*  [PCG](http://www.pcg-random.org/)
*  [yamc](https://github.com/yohhoy/yamc)
*  [fmt](https://fmt.dev/)

If you are on Arch Linux, you can install some of these from AUR (sharedaccess, half, pcg-cpp-git, yamc-git).

Optional libraries:

*  [OpenMP](https://www.openmp.org/)
*  [libnuma](https://github.com/numactl/numactl)
*  [Xalan-C](https://xalan.apache.org/) (XSLT support)
*  [Xerces-C](https://xerces.apache.org/) (required for GUI and CLI)

For modules:

*  [Assimp](https://assimp.org/)
*  [OpenImageIO](http://www.openimageio.org/)

For the GUI:

*  [wxWidget](https://www.wxwidgets.org/)
*  [OpenGL](https://www.opengl.org/) and [GLEW](http://glew.sourceforge.net/)

A C++20 compiler is required, only GCC is supported at this time. Only linux with very up to date packages is supported. It is almost working on Windows by cross-compiling from MingW on linux.

#### Linux - GCC

```sh
cmake <source dir> -DCMAKE_CXX_FLAGS_RELEASE="-march=native -O3 -DNDEBUG -fno-signed-zeros -fno-math-errno -fassociative-math -fno-signaling-nans -fno-trapping-math -fcx-limited-range -fexcess-precision=fast"
cmake --build .
sudo cmake --install .
```
