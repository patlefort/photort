/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/ToneMapper/Common.hpp>
#include <PhotoRT/Core/ColorProfile.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>


namespace PhotoRT
{
	// Tone mappers
	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedToneMapperClamper)


	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedToneMapperExposure)
	void AugmentedToneMapperExposure::setParameters(const ParameterTree &params)
	{
		params.getValueIfExistsBind("exposure", PL_LAMBDA_FORWARD_THIS(setExposure));
	}

	ParameterTree AugmentedToneMapperExposure::getParameters(bool /*deep*/) const
	{
		ParameterTree params;

		params["exposure"] = mExposure;

		return params;
	}


	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedToneMapperScale)
	void AugmentedToneMapperScale::setParameters(const ParameterTree &params)
	{
		params.getValueIfExists("power", mPower);
	}

	ParameterTree AugmentedToneMapperScale::getParameters(bool /*deep*/) const
	{
		ParameterTree params;

		params["power"] = mPower;

		return params;
	}

}
