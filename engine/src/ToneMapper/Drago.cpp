/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/ToneMapper/Drago.hpp>
#include <PhotoRT/Bitmap/Bitmap.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

#include <patlib/accumulator.hpp>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedToneMapperDrago)
	void AugmentedToneMapperDrago::setParameters(const ParameterTree &params)
	{
		params.getValueIfExistsBind("bias", PL_LAMBDA_FORWARD_THIS(setBias));
		params.getValueIfExistsBind("constant_brightness", PL_LAMBDA_FORWARD_THIS(setConstantBrightness));
	}

	ParameterTree AugmentedToneMapperDrago::getParameters(bool /*deep*/) const
	{
		ParameterTree params;

		params["bias"] = mBias;
		params["constant_brightness"] = mConstantBrightness;

		return params;
	}


	Color ToneMapperDrago::operator() (Color value) const noexcept
	{
		return toneMapDrago(value);
	}

	#ifdef PRT_FEATURE_PACKED_COLOR
		PackedColor ToneMapperDrago::operator() (PackedColor value) const noexcept
		{
			return toneMapDrago(value);
		}
	#endif

	void ToneMapperDrago::prepare(const FilmBitmap &bitmap, const IColorProfile &colorProfile)
	{
		ToneMapperBase::prepare(bitmap, colorProfile);

		namespace ac = pl::accumulators;
		constexpr auto max = ac::tag::max_c;
		constexpr auto sum = ac::tag::sum_c;

		mLavg = 0;
		mLmax = 0;

		ac::accumulator_set Lavg{type_v<CReal>, sum};
		ac::accumulator_set Lmax{type_v<CReal>, max};

		const auto rect = getBitmapRect(bitmap);
		auto areaInv = 1 / (CReal)rect.area();

		[[maybe_unused]] const auto nb = bitmap.getNbPixels();

		#pragma omp parallel if(nb >= 1024*16)
		{
			ac::accumulator_set localLavg{type_v<CReal>, sum};
			ac::accumulator_set localLmax{type_v<CReal>, max};

			#pragma omp for
			for(auto&& c : bitmap)
			{
				if(const auto cmax = colorProfile.getY(c); cmax > 0)
				{
					localLavg( cmax * areaInv );
					localLmax( cmax );
				}
			}

			#pragma omp critical
			{
				Lavg += localLavg;
				Lmax += localLmax;
			}
		}

		mLavg = Lavg.extract(sum) /olog;
		mLmax = Lmax.extract(max);

		if(mLmax > 0)
		{
			mLavg = mLavg /oexp;

			if(getConstantBrightness())
				mLavg /= pl::plpow(mBias, 5);

			mLmax = mLmax / mLavg;

			mLavgInv = 1 / mLavg;
			mLmaxInv = 1 / mLmax;

			mScaleFactor = 1 / pl::pllog10(mLmax + 1);
		}
		else
		{
			mLavgInv = mLmaxInv = mScaleFactor = 1;
		}
	}
}
