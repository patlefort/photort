/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Bitmap/Bitmap.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("texture-greyscale", BitmapEntity<TextureBitmapBuffer<u8>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("texture-8", BitmapEntity<TextureBitmapPaletteBuffer<u32, u8, 256>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("texture-16", BitmapEntity<TextureBitmapBuffer<u16>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("texture-24", BitmapEntity<TextureBitmapBuffer<ColorInt24>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("texture-32", BitmapEntity<TextureBitmapBuffer<u32>>)

}
