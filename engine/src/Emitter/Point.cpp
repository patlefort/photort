/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Emitter/Point.hpp>
#include <PhotoRT/Core/RayTracer.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_DEFINE(EmitterPoint)

	void EmitterPoint::setParameters(const ParameterTree &params)
	{
		EmitterEntityBase::setParameters(params);

		params.getValueIfExists
			("position", mPosition)
			("color", mDiffuseColor)
			("attenuation_constant", mAttenuationConstant)
			("attenuation_linear", mAttenuationLinear)
			("attenuation_quadratic", mAttenuationQuadratic)
		;
	}

	ParameterTree EmitterPoint::getParameters(bool deep) const
	{
		ParameterTree params {EmitterEntityBase::getParameters(deep)};

		params.insert({
			{"position", mPosition},
			{"color", mDiffuseColor},
			{"attenuation_constant", mAttenuationConstant},
			{"attenuation_linear", mAttenuationLinear},
			{"attenuation_quadratic", mAttenuationQuadratic}
		});

		return params;
	}

	pl::marked_optional<SampledSensorIn> EmitterPoint::sampleIn(const MaterialInteraction &surface, IRNGInt &, RandomReals<2> /*rndDir*/) const noexcept
	{
		auto dir = (mPosition - pl::visit_one(surface, [](const auto &i){ return i.worldPos(); })).as_dir();
		const auto colDist = dir /oprtlength;
		dir = dir /olnormalize/ colDist;
		const auto color = mDiffuseColor *
			((mAttenuationConstant + mAttenuationLinear * colDist + mAttenuationQuadratic * (colDist /opow2)) /omax/ Real{1} /oprtreciprocal);

		if(isAlmostBlack(color))
			return pl::nullopt;

		return SampledSensorIn{
				.sensor{
					.interaction = PointInteraction{
						.mLocalPos = mPosition,
						.mWorldPos = mPosition,
						.colDist = colDist
					},
					.pdf = 1
				},
				.color = color,
				.dir = dir,
				.pdf = 1
			};
	}

	pl::marked_optional<SampledSensor> EmitterPoint::sampleArea(IRNGInt &, RandomReals<2> /*rndPos*/) const noexcept
	{
		return SampledSensor{
				.interaction = PointInteraction{
					.mLocalPos = mPosition,
					.mWorldPos = mPosition
				},
				.pdf = 1
			};
	}

	pl::marked_optional<SampledSensorOut> EmitterPoint::sampleOut(IRNGInt &, RandomReals<2>, RandomReals<2>) const noexcept
	{
		return pl::nullopt;
	}

	pl::marked_optional<SampledSensorOutDir> EmitterPoint::sampleOutDir(const SampledSensor &, IRNGInt &, RandomReals<2>) const noexcept
	{
		return pl::nullopt;
	}

	Color EmitterPoint::radiance(const MaterialInteraction &sensorSurface, VecD /*dir*/) const noexcept
	{
		const auto dist = (mPosition - std::get<PointInteraction>(sensorSurface).worldPos()) /oprtlength;
		const auto intens = (mAttenuationConstant + mAttenuationLinear * dist + mAttenuationQuadratic * (dist /opow2)) /omax/ Real{1} /oprtreciprocal;

		return mDiffuseColor * intens;
	}
}
