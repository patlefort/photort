/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Emitter/Panoramic.hpp>
#include <PhotoRT/Core/RayTracer.hpp>
#include <PhotoRT/Core/Transform.hpp>
#include <PhotoRT/Core/Scene.hpp>
#include <PhotoRT/Core/DistributionSampling.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

#include <patlib/rectangle.hpp>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_DEFINE(EmitterPanoramic)

	void EmitterPanoramic::setParameters(const ParameterTree &params)
	{
		EmitterEntityBase::setParameters(params);

		addParamReference(mLightMap, params, "lightmap");
		addParamReference(mLightSampler, params, "lightsampler");
		addParamReference(mOrientation, params, "orientation");
	}

	ParameterTree EmitterPanoramic::getParameters(bool deep) const
	{
		ParameterTree params {EmitterEntityBase::getParameters(deep)};

		if(mLightMap)
			params["lightmap"].fromEntityRef(*mLightMap, deep);

		if(mLightSampler)
			params["lightsampler"].fromEntityRef(*mLightSampler, deep);

		if(mOrientation)
			params["orientation"].fromEntityRef(*mOrientation, deep);

		return params;
	}

	OptionalPDF<Real> EmitterPanoramic::panDirPdf(VecD odir) const noexcept
	{
		if(mLightSampler)
		{
			const auto locDir = mOriMatrixInv * odir /osshuffle<0, 2, 1, 3>;
			const auto thetaPhi = sphereThetaPhi(locDir);
			const auto sinTheta = pl::plsin(thetaPhi[size_v<1>]) /oabs;
			const auto uv = thetaPhi * Point2Dr{1 / (std::numbers::pi_v<Real> * 2), std::numbers::inv_pi_v<Real>};
			const auto lsPdf = mLightSampler->PDF(uv);
			if(!sinTheta || !lsPdf)
				return pl::nullopt;

			return pl::assert_valid(lsPdf.value() * mConstDirPdf / sinTheta, valid::PDF );
		}
		else
			return mConstDirPdf;
	}

	pl::marked_optional<EmitterPanoramic::DirSample> EmitterPanoramic::sampleDir(RandomReals<2> rnd) const noexcept
	{
		if(mLightSampler)
		{
			return (*mLightSampler)(rnd)
				.and_then([&](auto sample) -> pl::marked_optional<EmitterPanoramic::DirSample> {
					const auto sinTheta = pl::plsin(sample.value[size_v<1>] * std::numbers::pi_v<Real>) /oabs;

					if(sinTheta <= constants::epsilon<Real>) [[unlikely]]
						return pl::nullopt;

					pl::assert_valid( sample.pdf *= mConstDirPdf / sinTheta, valid::PDF );

					return EmitterPanoramic::DirSample{
						.dir = mOriMatrix * CoordinatePosition{tag::polar, sample.value}.to(tag::cartesian, type_v<VecD>)->as_dir() /osshuffle<0, 2, 1, 3>,
						.sample = sample
					};
				});
		}
		else
		{
			const auto dir = static_cast<VecD>(uniformSampleSphere(static_cast<pl::values<Real, 2>>(rnd)));

			return EmitterPanoramic::DirSample{
				.dir = mOriMatrix * dir,
				.sample = {
					.value = *CoordinatePosition{tag::cartesian, dir /osshuffle<0, 2, 1, 3>}.to(tag::polar, type_v<Point2Dr>),
					.pdf = mConstDirPdf
				}
			};
		}
	}

	Color EmitterPanoramic::averagePower() const noexcept
	{
		if(!mLightMap)
			return Black;

		constexpr if32 nb = 100;
		constexpr Real
			nbInv = Real{1} / nb,
			weight = Real{1} / pl::pow2(nb);

		return rg::accumulate(pl::rectangle<>{pl::size_p{nb}}, Color{}, rg::plus{},
				[&](const auto p)
				{
					const auto sp = p * nbInv + nbInv * 0.5;
					const auto sampled = sampleDir({sp[0], sp[1]});
					return (*mLightMap)(sampled->sample.value[0], sampled->sample.value[1]) / sampled->sample.pdf * weight;
				}
			);
	}

	pl::marked_optional<SampledSensorIn> EmitterPanoramic::sampleIn(const MaterialInteraction &surface, IRNGInt &, RandomReals<2> rndPos) const noexcept
	{
		if(!mLightMap)
			return pl::nullopt;

		const auto worldPos = pl::visit_one(surface, [](const auto &i){ return i.worldPos(); });

		return sampleDir(rndPos)
			.and_then([&](const auto &sampled) -> pl::marked_optional<SampledSensorIn> {
				const auto color = pl::assert_valid( (*mLightMap)(sampled.sample.value[0], sampled.sample.value[1]) );
				if(isAlmostBlack(color))
					return pl::nullopt;

				return SampledSensorIn{
						.sensor{
							.interaction = PointInteraction{
								.mLocalPos = worldPos,
								.mWorldPos = worldPos,
								.colDist = std::numeric_limits<Real>::infinity()
							},
							.pdf = 1
						},
						.color = color,
						.dir = sampled.dir,
						.pdf = sampled.sample.pdf
					};
			});
	}

	pl::marked_optional<SampledSensor> EmitterPanoramic::sampleArea(IRNGInt &, RandomReals<2> /*rndPos*/) const noexcept
	{
		return pl::nullopt;
	}

	pl::marked_optional<SampledSensorOut> EmitterPanoramic::sampleOut(IRNGInt &, RandomReals<2> rndPos, RandomReals<2> rndDir) const noexcept
	{
		if(!mAreaRadius || !mLightMap)
			return pl::nullopt;

		return sampleDir(rndDir)
			.and_then([&](const auto &sampled) -> pl::marked_optional<SampledSensorOut> {
				const auto color = pl::assert_valid( (*mLightMap)(sampled.sample.value[0], sampled.sample.value[1]) );
				if(isAlmostBlack(color))
					return pl::nullopt;

				const auto diskPos = uniformSampleDiskPoint(static_cast<pl::values<Real, 2>>(rndPos));
				const Lobe<> lobe{sampled.dir};
				const auto wPos = mMiddle + mRadiusVec * (lobe.lobeX * diskPos[0] + lobe.lobeY * diskPos[1]);
				const auto lPos = wPos + sampled.dir * mRadiusVec;

				return SampledSensorOut{
						.sensor{
							.interaction = PointInteraction{
								.mLocalPos = wPos,
								.mWorldPos = wPos,
								.colDist = std::numeric_limits<Real>::infinity()
							},
							.pdf = mAreaPdf
						},
						.dir{
							.color = color,
							.dir = -sampled.dir,
							.origin = lPos,
							.pdf = sampled.sample.pdf
						}
					};
			});
	}

	pl::marked_optional<SampledSensorOutDir> EmitterPanoramic::sampleOutDir(const SampledSensor &sensorSurface, IRNGInt &, RandomReals<2> rndDir) const noexcept
	{
		return sampleDir(rndDir)
			.and_then([&](const auto &sampled) -> pl::marked_optional<SampledSensorOutDir> {
				const auto color = pl::assert_valid( (*mLightMap)(sampled.sample.value[0], sampled.sample.value[1]) );
				if(isAlmostBlack(color))
					return pl::nullopt;

				return pl::visit_one(sensorSurface.interaction, [&](const auto &i)
				{
					return SampledSensorOutDir{
							.color = color,
							.dir = -sampled.dir,
							.origin = i.worldPos() + sampled.dir * mRadiusVec,
							.pdf = sampled.sample.pdf,
							.hemisphere = i.getHemisphere(sampled.dir)
						};
				});
			});
	}

	Color EmitterPanoramic::radiance(const MaterialInteraction &, VecD dir) const noexcept
	{
		if(!mLightMap)
			return Black;

		const auto p = getUV(mOriMatrixInv * dir /osshuffle<0, 2, 1, 3>);

		return pl::assert_valid( (*mLightMap)(p[0], p[1]) );
	}

	OptionalPDF<Real> EmitterPanoramic::PDF(const MaterialInteraction &sensorSurface) const noexcept
	{
		if(!mLightMap)
			return pl::nullopt;

		const auto dist2 = static_cast<VecD>(pl::visit_one(sensorSurface, [&](const auto &i){ return i.worldPos(); }) - mMiddle) /osdot;

		return dist2 <= mAreaRadius2 ? mAreaPdf : 0;
	}

	OptionalPDF<Real> EmitterPanoramic::PDFA(const MaterialInteraction &, const MaterialInteraction &, VecD dir) const noexcept
		{ return panDirPdf(dir); }

	OptionalPDF<Real> EmitterPanoramic::PDFW(const MaterialInteraction &, VecD dir) const noexcept
		{ return panDirPdf(dir); }

	void EmitterPanoramic::init(const Scene &scene)
	{
		EmitterBase::init(scene);

		if(mOrientation)
		{
			mOriMatrix = mOrientation->getMatrix();
			mOriMatrixInv = mOrientation->inverse();
		}
		else
		{
			mOriMatrix.set_identity();
			mOriMatrixInv.set_identity();
		}

		const auto box = scene.getAABox();
		mMiddle = pl::assert_valid(box.middle());
		mAreaRadius = pl::assert_valid( std::sqrt(box.dims() * 0.5 /osdot), valid::Weight );
		mRadiusVec = VecD{mAreaRadius - constants::epsilon<Real>}.as_dir();
		mAreaRadius2 = pl::assert_valid( mAreaRadius /opow2, valid::Weight );
		mArea = pl::assert_valid( std::numbers::pi_v<Real> * mAreaRadius2, valid::Weight );
		mAreaPdf = pl::assert_valid( 1 / mArea, valid::PDF );

		if(!mLightMap)
			log() << "No light map associated with this panoramic light source.";

		/*log() << "mMiddle: " << mMiddle;
		log() << "mAreaRadius: " << mAreaRadius;
		log() << "mArea: " << mArea;
		log() << "mAreaPdf: " << mAreaPdf;*/
	}
}
