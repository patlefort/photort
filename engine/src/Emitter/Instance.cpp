/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Emitter/Instance.hpp>
#include <PhotoRT/Core/Instance.hpp>
#include <PhotoRT/Core/Edf.hpp>

#include <patlib/rectangle.hpp>

namespace PhotoRT
{
	void EmitterObjectInstance::init(const Scene &scene)
	{
		EmitterBase::init(scene);

		if(mInstance)
		{
			auto &object = *mInstance->getObject();
			mNbPrimitives = object.getNbPrimitives();
			if(!mInstanceProps)
				mInstanceProps = &object;

			mIsDirac = true;
			bool hasNoNormalMap = false;
			for(const auto i : mir(mNbPrimitives))
			{
				if(const auto mat = mInstanceProps->getMaterial(i); mat && mat->material && !mat->material->mLayers.empty())
				{
					if(const auto em = std::to_address(mat->material->getEmission()); em)
					{
						hasNoNormalMap = !mat->material->getNormalMapper() ? true : hasNoNormalMap;
						mIsDirac = !em->isDiracDelta() ? false : mIsDirac;
					}
				}
			}

			mHasFlatSurfaces = object.isAlwaysFlat() && hasNoNormalMap;

			object.buildCdf();
		}
		else
			mNbPrimitives = 0;
	}

	void EmitterObjectInstance::setInstance(EntityPtr<IObjectInstance> inst, const Matrix &worldMatrix)
	{
		mInstance = std::move(inst);
		mWorldMatrix = worldMatrix;
		mWorldToLocalMatrix = pl::try_inverse(worldMatrix, "Failed to inverse light source world matrix.");
	}

	VecP EmitterObjectInstance::getMiddle() const noexcept
	{
		if(!mInstance)
			return {};

		return mInstance->getObject()->getAABox().middle();
	}

	Color EmitterObjectInstance::averagePower() const noexcept
	{
		if(!mInstance || !hasArea())
			return Black;

		const auto &object = *mInstance->getObject();

		const auto proj = [&](const auto i) -> Color
			{
				if(mLightPortal)
					return object.getSurfaceArea(i);

				const auto matEntry = mInstanceProps->getMaterial(i);
				if(!matEntry || matEntry->material->mLayers.empty())
					return Black;

				const auto emission = std::to_address(matEntry->material->getEmission());
				if(!emission)
					return Black;

				static constexpr if32 nbSp = 4;
				return rg::accumulate(pl::rectangle<>{pl::size_p{nbSp}}, Color{}, rg::plus{}, [&](const auto p)
				{
					const auto rnbSp = r_(nbSp);
					const auto sp = p / rnbSp + 1 / (rnbSp*2);

					auto areaSp = object.sampleArea({sp[0], sp[1]}, i);
					if(!areaSp)
						return Black;

					return rg::accumulate(pl::rectangle<>{pl::size_p{nbSp}}, Color{}, rg::plus{}, [&](const auto d)
					{
						auto inter = sampledAreaToShadedInteraction(*areaSp, *mInstanceProps, *mInstance, mWorldMatrix, mWorldMatrix * areaSp->pos, 0, i, -areaSp->shadingNormal, false, matEntry);
						inter.initialSurface(*mSubstanceStack.getEnteredSub());
						const auto sp = d / rnbSp + 1 / (rnbSp*2);
						const auto dirSp = emission->sampleDir( {sp[0], sp[1]}, inter );
						if(!dirSp)
							return Black;

						return dirSp->color * (1 / (rnbSp /opow2)) / (areaSp->pdf * dirSp->pdf);
					});
				});

			};

		return rg::accumulate(mir(mNbPrimitives), Color{}, rg::plus{}, proj);
	}

	pl::marked_optional<SampledSensor> EmitterObjectInstance::sampleArea(IRNGInt &, RandomReals<2> rndPos) const noexcept
	{
		if(!mNbPrimitives)
			return pl::nullopt;

		const auto &object = *mInstance->getObject();
		const auto psp = object.samplePrimitive(rndPos[0]);

		return object.sampleArea({psp.offset, rndPos[1]}, psp.index)
			.transform([&](const auto &areaSp)
			{
				SampledSensor sp{
						.interaction = sampledAreaToShadedInteraction(areaSp, *mInstanceProps, *mInstance, mWorldMatrix, mWorldMatrix * areaSp.pos, 0, psp.index, -areaSp.normal, false),
						.pdf = areaSp.pdf * psp.pdf
					};

				auto &inter = std::get<SurfaceInteraction>(sp.interaction);
				inter.surface().minimalSafeInc = pl::plmax<Real>( {inter.worldPos().minimal_safe_increment(), constants::epsilon<Real>, inter.localPos().minimal_safe_increment()} );

				return sp;
			});
	}

	pl::marked_optional<SampledSensorIn> EmitterObjectInstance::sampleIn(const MaterialInteraction &surface, IRNGInt &, RandomReals<2> rndPos) const noexcept
	{
		if(!mNbPrimitives)
			return pl::nullopt;

		const auto &object = *mInstance->getObject();
		const auto psp = object.samplePrimitive(rndPos[0]);
		const auto matEntry = mInstanceProps->getMaterial(psp.index);
		const auto emission = matEntry && !matEntry->material->mLayers.empty() ? std::to_address(matEntry->material->getEmission()) : nullptr;
		const bool isDelta = emission ? emission->isDiracDelta() : false;

		if(isDelta && matEntry && matEntry->material->getNormalMapper())
			return pl::nullopt;

		const auto surfWorldPoint = pl::visit_one(surface, [](const auto &i){ return i.worldPos(); });
		const auto surfLocalPoint = mWorldToLocalMatrix * surfWorldPoint;

		if(isDelta)
		{
			const auto normal = object.getSurfaceNormal(psp.index);
			const auto pt = object.getPointOnSurface(psp.index);
			EnumHemisphere hemi = EnumHemisphere::North;
			auto testdir = -normal;
			if(normal /odot/ (surfLocalPoint - pt) < 0)
			{
				hemi = EnumHemisphere::South;
				testdir = -testdir;
			}

			return object.intersectExt(psp.index, surfLocalPoint, testdir, dirReciprocal(testdir), std::numeric_limits<Real>::infinity(), true)
				.and_then([&](const auto &collision) -> pl::marked_optional<SampledSensorIn>
				{
					SampledSensorIn sp{
							.sensor{
								.interaction = collisionToShadedInteraction(collision, *mInstanceProps, *mInstance, mWorldMatrix, normal, normal, surfLocalPoint, testdir, matEntry),
								.pdf = psp.pdf
							},
							.color{},
							.dir = pl::assert_valid( dirSafeTransform(testdir, mWorldMatrix) ),
							.pdf = 1,
							.delta = true
						};

					auto &inter = std::get<SurfaceInteraction>(sp.sensor.interaction);
					sp.color = emission ? emission->radianceDiracDelta(inter, hemi) : White;
					if(isAlmostBlack(sp.color))
						return pl::nullopt;

					return sp;
				});
		}

		return [&]() -> pl::marked_optional<SampledAreaResultBackface<VecP>>
			{
				// Try to sample only the visible area if material has no transparency.
				if(!matEntry || !matEntry->material->isTransparent())
					return object.sampleVisibleSurfaceArea({psp.offset, rndPos[1]}, surfLocalPoint, psp.index);

				return object.sampleArea({psp.offset, rndPos[1]}, psp.index)
					.and_then([&](auto areaSp) -> pl::marked_optional<SampledAreaResultBackface<VecP>> {
						auto dir = static_cast<VecD>(areaSp.pos - surfLocalPoint);
						const auto dist = dir /oprtlength;
						if(!dist)
							return pl::nullopt;

						dir = dir /olnormalize/ dist;
						auto dp = areaSp.normal /odot/ dir;

						const bool backface = dp > 0;
						dp = dp /oabs;
						if(dp < constants::epsilon<Real>)
							return pl::nullopt;

						areaSp.pdf *= geometricTermPos(dist) * (dp /oprtreciprocal);
						return pl::marked_optional<SampledAreaResultBackface<VecP>>{std::in_place, areaSp, backface};
					});
			}()
			.and_then([&](const auto &areaSp) -> pl::marked_optional<SampledSensorIn>
			{
				const auto worldPos = mWorldMatrix * areaSp.pos;
				auto dir = (worldPos - surfWorldPoint).as_dir();
				const auto colDist = dir /oprtlength;
				dir = pl::assert_valid( dir /olnormalize/ colDist );

				SampledSensorIn sp{
						.sensor{
							.interaction = sampledAreaToShadedInteraction(areaSp, *mInstanceProps, *mInstance, mWorldMatrix, worldPos, colDist, psp.index, dir, areaSp.backface, matEntry),
							.pdf = psp.pdf
						},
						.color{},
						.dir = dir,
						.pdf = areaSp.pdf
					};

				auto &inter = std::get<SurfaceInteraction>(sp.sensor.interaction);
				if(!inter.isSafeDir(dir) || !pl::visit_one(surface, [&](const auto &i){ return i.isSafeDir(dir); }))
					return pl::nullopt;

				sp.color = pl::assert_valid( emission ? (*emission)(inter, -dir) : (mLightPortal ? White : Black) );
				if(isAlmostBlack(sp.color))
					return pl::nullopt;

				inter.surface().minimalSafeInc = pl::plmax<Real>( {inter.worldPos().minimal_safe_increment(), constants::epsilon<Real>, inter.localPos().minimal_safe_increment()} );

				return sp;
			});
	}

	pl::marked_optional<SampledSensorOut> EmitterObjectInstance::sampleOut(IRNGInt &rng, RandomReals<2> rndPos, RandomReals<2> rndDir) const noexcept
	{
		return sampleArea(rng, rndPos)
			.and_then([&](auto areaSp) -> pl::marked_optional<SampledSensorOut>
			{
				return sampleOutDir(areaSp, rng, rndDir)
					.transform([&](auto outDirSp) -> SampledSensorOut
					{
						SampledSensorOut sp{
								.sensor = std::move(areaSp),
								.dir = std::move(outDirSp)
							};

						auto &inter = std::get<SurfaceInteraction>(sp.sensor.interaction);
						inter.surface().isBackfaceHit(sp.dir.hemisphere == EnumHemisphere::South);
						inter.surface().minimalSafeInc = pl::plmax<Real>( {inter.worldPos().minimal_safe_increment(), constants::epsilon<Real>, inter.localPos().minimal_safe_increment()} );

						return sp;
					});
			});
	}

	pl::marked_optional<SampledSensorOutDir> EmitterObjectInstance::sampleOutDir(const SampledSensor &sensorSurface, IRNGInt &, RandomReals<2> rndDir) const noexcept
	{
		auto &inter = std::get<SurfaceInteraction>(sensorSurface.interaction);

		if(!inter.surface().matEntry || inter.surface().matEntry->material->mLayers.empty())
			return pl::nullopt;

		const auto emission = std::to_address(inter.surface().matEntry->material->getEmission());
		if(!emission)
			return pl::nullopt;

		return emission->sampleDir(rndDir, inter)
			.transform([&](const auto &emissionSample)
			{
				return SampledSensorOutDir{
						.color = emissionSample.color,
						.dir = emissionSample.dir,
						.origin = inter.worldPos(),
						.pdf = emissionSample.pdf,
						.hemisphere = emissionSample.hemisphere,
						.delta = emissionSample.delta
					};
			});
	}

	Color EmitterObjectInstance::radiance(const MaterialInteraction &sensorSurface, VecD dir) const noexcept
	{
		const auto &inter = std::get<SurfaceInteraction>(sensorSurface);
		if(!inter.surface().matEntry || inter.surface().matEntry->material->mLayers.empty())
			return Black;

		const auto emission = std::to_address(inter.surface().matEntry->material->getEmission());
		if(!emission)
			return Black;

		return (*emission)(inter, -dir);
	}

	OptionalPDF<Real> EmitterObjectInstance::worldPDF(const MaterialInteraction &sensorSurface) const noexcept
	{
		if(!mNbPrimitives)
			return pl::nullopt;

		const auto &inter = std::get<SurfaceInteraction>(sensorSurface);
		const auto &object = *mInstance->getObject();
		return object.PDF(mWorldMatrix, inter.surface().primIndex).value() * object.PMF(inter.surface().primIndex);
	}

	OptionalPDF<Real> EmitterObjectInstance::PDF(const MaterialInteraction &sensorSurface) const noexcept
	{
		if(!mNbPrimitives)
			return pl::nullopt;

		const auto &inter = std::get<SurfaceInteraction>(sensorSurface);
		const auto &object = *mInstance->getObject();
		return object.PDF(inter.surface().primIndex).value() * object.PMF(inter.surface().primIndex);
	}

	OptionalPDF<Real> EmitterObjectInstance::PDFA(const MaterialInteraction &sensorSurface, const MaterialInteraction &surface, VecD dir) const noexcept
	{
		if(!mNbPrimitives)
			return pl::nullopt;

		const auto &inter = std::get<SurfaceInteraction>(sensorSurface);
		const VecP pw = mWorldToLocalMatrix * pl::visit_one(surface, [](const auto &i){ return i.worldPos(); });
		const VecD
			ldir = dirSafeTransform(dir, mWorldToLocalMatrix),
			dirInv = dirReciprocal(ldir);
		const auto &object = *mInstance->getObject();
		const SampledAreaResult<VecP> areaSp{
				.pos = inter.surface().localPos,
				.uv = inter.surface().uv,
				.normal = inter.surface().surfaceNormal,
				.shadingNormal = inter.surface().shadingNormal,
				.pdf = 0
			};

		return object.PDF(pw, ldir, dirInv, inter.surface().primIndex, areaSp).value() * object.PMF(inter.surface().primIndex);
	}

	OptionalPDF<Real> EmitterObjectInstance::PDFW(const MaterialInteraction &sensorSurface, VecD dir) const noexcept
	{
		const auto &inter = std::get<SurfaceInteraction>(sensorSurface);

		if(!mNbPrimitives || !inter.surface().matEntry || inter.surface().matEntry->material->mLayers.empty())
			return pl::nullopt;

		const auto emission = std::to_address(inter.surface().matEntry->material->getEmission());
		if(!emission)
			return pl::nullopt;

		return emission->PDF(inter, -dir);
	}
}
