/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Camera/Lens.hpp>
#include <PhotoRT/Core/RayTracer.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/PrimitiveGroup.hpp>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_DEFINE(LensCircle)
	PRT_ENTITY_FACTORY_DEFINE(LensPrimitive)

	PRT_ENTITY_FACTORY_DEFINE(CameraLens)

	pl::marked_optional<SampledSensor> LensCircle::sampleArea(RandomReals<2> p) const noexcept
	{
		return SampledSensor{
				.interaction = SurfaceInteraction{
					{
						.localPos = pl::assert_valid( mDisk(p[0], p[1]) )
					},
					{}
				},
				.pdf = diskPdf()
			};
	}

	void LensCircle::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		params.getValueIfExistsBind("size", PL_LAMBDA_FORWARD_OBJ(setConcentricDisk, this->mDisk));

	}

	ParameterTree LensCircle::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params["size"] = mDisk.mSize;

		return params;
	}

	void LensPrimitive::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		addParamReference(mPrimitiveGroup, params, "primitive");
	}

	ParameterTree LensPrimitive::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		if(mPrimitiveGroup)
			params["primitive"].fromEntityRef(*mPrimitiveGroup, deep);

		return params;
	}

	OptionalPDF<Real> LensPrimitive::PDF(const MaterialInteraction &inter) const noexcept
		{ return mPrimitiveGroup->PDF(std::get<SurfaceInteraction>(inter).surface().primIndex); }

	OptionalPDF<Real> LensPrimitive::PDF(const Matrix &m, const MaterialInteraction &inter) const noexcept
		{ return mPrimitiveGroup->PDF(m, std::get<SurfaceInteraction>(inter).surface().primIndex); }

	void LensPrimitive::update()
	{
		mPrimitiveGroup->buildCdf();
	}

	pl::marked_optional<SampledSensor> LensPrimitive::sampleArea(RandomReals<2> p) const noexcept
	{
		auto sampledPrim = mPrimitiveGroup->samplePrimitive(p[0]);

		return mPrimitiveGroup->sampleArea({sampledPrim.offset, p[1]}, sampledPrim.index)
			.transform([&](const auto &sampled)
			{
				return SampledSensor{
						.interaction = sampledAreaToInteraction(sampled),
						.pdf = sampled.pdf * sampledPrim.pdf
					};
			});
	}

	Real LensPrimitive::intersect(VecP origin, VecD dir, VecD dirInv) const noexcept
	{
		const auto [res, nbt] = mPrimitiveGroup->intersectExt(origin, dir, dirInv);

		return res ? res->dist : -1;
	}


	void CameraLens::setParameters(const ParameterTree &params)
	{
		CameraPinHole::setParameters(params);

		if(const auto p = params.findParam("lens"); p && !p->value.empty())
			setLens(addReference(type_v<ILens>, *p));

	}

	ParameterTree CameraLens::getParameters(bool deep) const
	{
		ParameterTree params {CameraPinHole::getParameters(deep)};

		params.insert({
			{"focusdist", mFocusDist}
		});

		if(mLens)
			params["lens"].fromEntityRef(*mLens, deep);

		return params;
	}

	VecP CameraLens::getFocusPosition(const VecP localOrigin, const VecD localDir) const noexcept
	{
		auto fDist = mFocusDist;

		if(isFlat())
			fDist *= localDir[size_v<2>] /oprtreciprocal;

		return localOrigin + localDir * fDist;
	}

	pl::marked_optional<SampledSensor> CameraLens::sampleArea(IRNGInt &rng, RandomReals<2> rndPos) const noexcept
	{
		if(!mLens)
			return CameraPinHole::sampleArea(rng, rndPos);

		return mLens->sampleArea(rndPos)
			.transform([&](auto sp){
				auto &inter = std::get<SurfaceInteraction>(sp.interaction);
				inter.worldPos( (mPosition + mMatrix.world * inter.localPos()).as_point() );

				return sp;
			});
	}

	pl::marked_optional<CameraIntersection> CameraLens::intersectExt(const VecP origin, const VecD dir) const noexcept
	{
		if(!mLens)
			return pl::nullopt;

		const auto lOrigin = mMatrix.worldToCam * origin;
		const auto lDir = dirSafeTransform(dir, mMatrix.worldToCam);
		const auto lDirInv = dirReciprocal(lDir);

		if(auto dist = mLens->intersect(lOrigin, lDir, lDirInv); dist >= 0)
		{
			auto dest = lOrigin + lDir * dist;
			dest = mMatrix.world * dest;
			dist = (dest - origin) /oprtlength;

			return getImagePosition(origin, dir, dest)
				.transform([&](const auto &v){
					return CameraIntersection{
						.position {v},
						.color {White},
						.dist {dist}
					};
				}, type_v<pl::marked_optional_default_policy<CameraIntersection>>);
		}

		return pl::nullopt;
	}

	bool CameraLens::intersect(const VecP origin, const VecD dir) const noexcept
	{
		if(!mLens)
			return false;

		const auto lOrigin = mMatrix.worldToCam * origin;
		const auto lDir = dirSafeTransform(dir, mMatrix.worldToCam);
		const auto lDirInv = dirReciprocal(lDir);

		return mLens->intersect(lOrigin, lDir, lDirInv) >= 0;
	}

	pl::marked_optional<Point2Dr> CameraLens::getImagePosition(const VecP /*origin*/, const VecD dir, const VecP destination) const noexcept
	{
		auto fDist = mFocusDist;

		if(mFlat)
		{
			const auto dp = -(dir /odot/ mImagePlaneLobe.normal);
			if(dp <= 0)
				return pl::nullopt;

			fDist *= dp /oprtreciprocal;
		}

		// Get position on image plane
		const auto focusPos = mMatrix.worldToCam * (destination - dir * fDist);
		const auto imagePos = mMatrix.cameraToRaster * pl::normalize_point_p{focusPos};

		if(const Point2Dr imagePoint{imagePos[size_v<0>], imagePos[size_v<1>]};
				pl::mask_all(imagePoint >= 0) && pl::mask_all(imagePoint < mFilmSize))
			return imagePoint;

		return pl::nullopt;
	}

	void CameraLens::setFocus(Real dist)
	{
		mFocusDist = dist;
	}

	void CameraLens::setFocusPos(VecP pos)
	{
		const auto dir = pos - getPosition();
		auto dist = dir /oprtlength;

		if(isFlat())
			dist *= dir /olnormalize/ dist /odot/ mImagePlaneLobe.normal;

		setFocus(dist);
	}

	void CameraLens::update()
	{
		CameraPinHole::update();

		//focusOnFocal = mFocusDist / focalDist;
		//focalOnFocus = focalDist / mFocusDist;

		if(mLens)
			mLens->update();

		mFocusDistSquared = mFocusDist /opow2;
		mFocusDistInv = 1 / mFocusDist;
		mFocusDistInvSquared = 1 / mFocusDistSquared;
	}
}
