/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Camera/Controller.hpp>
#include <PhotoRT/Core/Transform.hpp>
#include <PhotoRT/Core/RayTracer.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_DEFINE(CameraControllerSingle)
	PRT_ENTITY_FACTORY_DEFINE(CameraController2Eyes)

	void CameraControllerSingle::update()
	{
		CameraControllerBase::update();

		if(mCamera)
		{
			mCamera->setPosition(mwPosition);
			mCamera->setLookAt(mwLookAt);
			mCamera->setUp(mwUp);

			mCamera->update();
		}
	}

	void CameraControllerSingle::setParameters(const ParameterTree &params)
	{
		setCamera(addParamReference(type_v<ICamera>, params, "camera"));

		CameraControllerBase::setParameters(params);

	}

	ParameterTree CameraControllerSingle::getParameters(bool deep) const
	{
		ParameterTree params {CameraControllerBase::getParameters(deep)};

		if(mCamera)
			params["camera"].fromEntityRef(*mCamera, deep);

		return params;
	}


	CameraController2Eyes::CameraController2Eyes()
	{
		mReferenceCamera.projectionTransform = std::make_shared<TransformPerspective>();
	}

	/*void CameraController2Eyes::setFieldOfView(Real fov)
	{
		//CameraControllerBase::setFieldOfView(fov, aspect, sizeU, sizeV);

		mReferenceCamera.setFieldOfView(fov);

		mCamera[CameraEyes::LeftEye]->setFieldOfView(fov);
		mCamera[CameraEyes::RightEye]->setFieldOfView(fov);
	}*/

	void CameraController2Eyes::setAspectRatio(Real aspect)
	{
		mReferenceCamera.setAspectRatio(aspect);

		mCamera[(szt)CameraEyes::LeftEye]->setAspectRatio(aspect);
		mCamera[(szt)CameraEyes::RightEye]->setAspectRatio(aspect);
	}

	void CameraController2Eyes::setFilmSize(Dir2Dr s)
	{
		mReferenceCamera.setFilmSize(s);

		mCamera[(szt)CameraEyes::LeftEye]->setFilmSize(s);
		mCamera[(szt)CameraEyes::RightEye]->setFilmSize(s);
	}

	void CameraController2Eyes::update()
	{
		CameraControllerBase::update();

		if(mCamera[(szt)CameraEyes::LeftEye])
		{
			mReferenceCamera.setAspectRatio(mCamera[(szt)CameraEyes::LeftEye]->getAspectRatio());
			mReferenceCamera.setFilmSize(mCamera[(szt)CameraEyes::LeftEye]->getFilmSize());
			mReferenceCamera.setFoVbounds(mCamera[(szt)CameraEyes::LeftEye]->getFoVbounds());
			mReferenceCamera.setPosition(mwPosition);
			mReferenceCamera.setLookAt(mwLookAt);
			mReferenceCamera.setUp(mwUp);

			mReferenceCamera.update();

			const auto lobe = getCameraViewLobe();

			const VecP lookAtFocus = mwPosition + lobe.normal * getFocus();

			mCamera[(szt)CameraEyes::LeftEye]->setPosition(mReferenceCamera.getPosition() + lobe.lobeX * -getEyeDistance());
			mCamera[(szt)CameraEyes::LeftEye]->setLookAt(lookAtFocus);
			mCamera[(szt)CameraEyes::LeftEye]->setUp( (mCamera[(szt)CameraEyes::LeftEye]->getPosition() - lobe.lobeY).as_point() );

			mCamera[(szt)CameraEyes::LeftEye]->setFocusPos(lookAtFocus);

			/*mCamera[CameraEyes::LeftEye]->setPosition(mwPosition);
			mCamera[CameraEyes::LeftEye]->setLookAt(mwLookAt);
			mCamera[CameraEyes::LeftEye]->setUp(mwUp);*/

			mCamera[(szt)CameraEyes::LeftEye]->update();

			if(mCamera[(szt)CameraEyes::RightEye])
			{
				mCamera[(szt)CameraEyes::RightEye]->setPosition(mReferenceCamera.getPosition() + lobe.lobeX * getEyeDistance());
				mCamera[(szt)CameraEyes::RightEye]->setLookAt(lookAtFocus);
				mCamera[(szt)CameraEyes::RightEye]->setUp( (mCamera[(szt)CameraEyes::RightEye]->getPosition() - lobe.lobeY).as_point() );

				mCamera[(szt)CameraEyes::RightEye]->setFocusPos(lookAtFocus);

				mCamera[(szt)CameraEyes::RightEye]->update();
			}
		}

	}

	void CameraController2Eyes::setParameters(const ParameterTree &params)
	{
		setCamera(addParamReference(type_v<ICamera>, params, "camera_left"), CameraEyes::LeftEye);
		setCamera(addParamReference(type_v<ICamera>, params, "camera_right"), CameraEyes::RightEye);

		CameraControllerBase::setParameters(params);

		params.getValueIfExistsBind("eye_distance", PL_LAMBDA_FORWARD_THIS(setEyeDistance));
	}

	ParameterTree CameraController2Eyes::getParameters(bool deep) const
	{
		ParameterTree params {CameraControllerBase::getParameters(deep)};

		if(getCamera(CameraEyes::LeftEye))
			params["camera_left"].fromEntityRef(*getCamera(CameraEyes::LeftEye), deep);

		if(getCamera(CameraEyes::RightEye))
			params["camera_right"].fromEntityRef(*getCamera(CameraEyes::RightEye), deep);

		params["eye_distance"] = mEyeDistance;

		return params;
	}
}
