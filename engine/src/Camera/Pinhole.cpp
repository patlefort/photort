/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Camera/Pinhole.hpp>
#include <PhotoRT/Core/RayTracer.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/Emitter.hpp>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_DEFINE(CameraPinHole)

	void CameraPinHole::setParameters(const ParameterTree &params)
	{
		CameraBase::setParameters(params);

		params.getValueIfExistsBind
			("position", PL_LAMBDA_FORWARD_THIS(setPosition))
			("lookat", PL_LAMBDA_FORWARD_THIS(setLookAt))
			("up", PL_LAMBDA_FORWARD_THIS(setUp))
			("flat", PL_LAMBDA_FORWARD_THIS(setFlat))
			("dirac_delta", PL_LAMBDA_FORWARD_THIS(setDiracDelta))
			("focusdist", PL_LAMBDA_FORWARD_THIS(setFocus))
		;

		params.getValueIfExists("aspectratio", mAspectRatio);

		addParamReference(projectionTransform, params, "projection");
	}

	ParameterTree CameraPinHole::getParameters(bool deep) const
	{
		ParameterTree params {CameraBase::getParameters(deep)};

		params.insert({
			{"position", mPosition},
			{"lookat", mLookAt},
			{"up", mUp},
			{"aspectratio", mAspectRatio},

			{"flat", mFlat},
			{"dirac_delta", mDiracDelta}
		});

		if(projectionTransform)
			params["projection"].fromEntityRef(*projectionTransform, deep);

		return params;
	}

	void CameraPinHole::update()
	{
		CameraBase::update();

		mImagePlaneLobe.normal = (mLookAt - mPosition).as_dir() /onormalize;
		mImagePlaneLobe.lobeY = (mUp - mPosition).as_dir() /onormalize;
		mImagePlaneLobe.lobeX = mImagePlaneLobe.normal /ocross/ mImagePlaneLobe.lobeY /onormalize;
		mImagePlaneLobe.lobeY = -(mImagePlaneLobe.lobeX /ocross/ mImagePlaneLobe.normal /onormalize);

		mMatrix.worldToCam.set_identity();

		mMatrix.worldToCam[0] = mImagePlaneLobe.lobeX;
		mMatrix.worldToCam[1] = mImagePlaneLobe.lobeY;
		mMatrix.worldToCam[2] = mImagePlaneLobe.normal;

		mMatrix.worldToCam = TransformTranslation(-mPosition) * mMatrix.worldToCam;
		mMatrix.world = pl::try_inverse(mMatrix.worldToCam, "Failed to inverse world to camera matrix.");

		if(projectionTransform)
		{
			mMatrix.cameraToScreen = *projectionTransform;
			mMatrix.screenToCamera = projectionTransform->inverse();
		}
		else
		{
			mMatrix.cameraToScreen.set_identity();
			mMatrix.screenToCamera.set_identity();
		}

		Point2Dr pmin, pmax;
		const auto fovSize = mFovBounds.dims();
		const auto fovBoundRatio = mFilmSize / fovSize.as_point();

		if(mAspectRatio > 1)
		{
			pmin.set<0>(-mAspectRatio);
			pmax.set<0>(mAspectRatio);
			pmin.set<1>(-1);
			pmax.set<1>(1);
		}
		else
		{
			pmin.set<0>(-1);
			pmax.set<0>(1);
			pmin.set<1>(-1 / mAspectRatio);
			pmax.set<1>(1 / mAspectRatio);
		}

		pmin *= fovBoundRatio;
		pmax *= fovBoundRatio;

		pmin += mFovBounds.v1 / mFilmSize * fovBoundRatio;

		mMatrix.screenToRaster = TransformTranslation{{-pmin[0], -pmin[1], 0}}
			* TransformScale{{mFilmSize[0], mFilmSize[1], 1}}
			* TransformScale{{1 / (pmax[0] - pmin[0]), 1 / (pmax[1] - pmin[1]), 1}};
		mMatrix.rasterToScreen = pl::try_inverse(mMatrix.screenToRaster, "Failed to inverse screen to raster matrix.");

		mMatrix.rasterToCamera = mMatrix.rasterToScreen * mMatrix.screenToCamera;
		mMatrix.cameraToRaster = mMatrix.cameraToScreen * mMatrix.screenToRaster;

		mMatrix.worldToRaster = mMatrix.worldToCam * mMatrix.cameraToRaster;
		mMatrix.rasterToWorld = mMatrix.rasterToCamera * mMatrix.world;

		auto p1 = mMatrix.rasterToCamera * pl::normalize_point_p{VecP{pl::tag::identity}};
		auto p2 = mMatrix.rasterToCamera * pl::normalize_point_p{VecP{mFilmSize[0], mFilmSize[1], 0, 1}};

		p1 /= p1[size_v<2>];
		p2 /= p2[size_v<2>];

		mCameraArea = ((p2[size_v<0>] - p1[size_v<0>]) * (p2[size_v<1>] - p1[size_v<1>])) /oabs;
		mCameraAreaInv = 1 / mCameraArea;

		pl::assert_valid(mMatrix.rasterToWorld);
		pl::assert_valid(mMatrix.worldToRaster);
		pl::assert_valid(mMatrix.cameraToRaster);
		pl::assert_valid(mMatrix.rasterToCamera);
		pl::assert_valid(mMatrix.rasterToScreen);
		pl::assert_valid(mMatrix.screenToRaster);
		pl::assert_valid(mMatrix.worldToCam);
		pl::assert_valid(mMatrix.world);

		/*log() << "Raster to world matrix:\n" << mMatrix.rasterToWorld <<
			"\nWorld to raster matrix:\n" << mMatrix.worldToRaster <<
			"\nCamera to raster matrix:\n" << mMatrix.cameraToRaster <<
			"\nRaster to camera matrix:\n" << mMatrix.rasterToCamera <<
			"\nRaster to screen matrix:\n" << mMatrix.rasterToScreen <<
			"\nScreen to raster matrix:\n" << mMatrix.screenToRaster <<
			"\nWorld to camera matrix:\n" << mMatrix.worldToCam <<
			"\ncamera to world matrix:\n" << mMatrix.world << '\n' <<
			"\nLobe: " << mImagePlaneLobe << '\n';*/
	}

	Ray CameraPinHole::getFocusDir(Point2Dr pos) const noexcept
	{
		Ray ray;
		std::tie(ray.origin, ray.dir) = rasterToRay(pos);
		ray.origin = mMatrix.world * ray.origin;
		ray.dir = dirSafeTransform(ray.dir, mMatrix.world);
		ray.length = Ray::MaxDistance;

		return ray;
	}

	OptionalPDF<Real> CameraPinHole::PDFA(const MaterialInteraction &sensorSurface, const MaterialInteraction &surface, VecD dir) const noexcept
	{
		return pl::visit_one(surface, [&](const auto &i)
		{
			const auto &si = std::get<PointInteraction>(sensorSurface);
			return pdfAtoW( PDF(sensorSurface).value(), geometricTermPos(si.worldPos(), i.worldPos()), (isFlat() ? mImagePlaneLobe.normal /odot/ dir /oabs : 1) );
		});
	}

	OptionalPDF<Real> CameraPinHole::PDFW(const MaterialInteraction &, VecD dir) const noexcept
	{
		if(isDiracDelta())
			return 1;

		const auto dp = -(dir /odot/ mImagePlaneLobe.normal);
		if(dp <= 0)
			return pl::nullopt;

		return (dp * dp * dp * mCameraArea) /oabs /oprtreciprocal;
	}

	pl::marked_optional<SampledSensor> CameraPinHole::sampleArea(IRNGInt &, RandomReals<2> /*rndPos*/) const noexcept
	{
		SampledSensor sp{
				.interaction = PointInteraction{
					.mLocalPos = mPosition,
					.mWorldPos = mPosition
				},
				.pdf = 1
			};

		return sp;
	}

	pl::marked_optional<SampledSensorOut> CameraPinHole::sampleOut(IRNGInt &rng, RandomReals<2> rndPos, RandomReals<2> rndDir) const noexcept
	{
		return sampleImageDir(rng, rndPos, {static_cast<Real>(rndDir[0]) * mFilmSize[0], static_cast<Real>(rndDir[1]) * mFilmSize[1]});
	}

	pl::marked_optional<SampledSensorOutDir> CameraPinHole::sampleOutDir(const SampledSensor &sensorSurface, IRNGInt &, RandomReals<2> rndDir) const noexcept
	{
		SampledSensorOutDir sp{};

		const auto &si = std::get<PointInteraction>(sensorSurface.interaction);

		VecP localPt;
		std::tie(localPt, sp.dir) = rasterToRay({static_cast<Real>(rndDir[0]) * mFilmSize[0], static_cast<Real>(rndDir[1]) * mFilmSize[1]});

		// Point is behind the camera
		if(isFlat() && sp.dir[size_v<2>] <= constants::epsilon<Real>)
			return pl::nullopt;

		const auto focusP = mMatrix.world * getFocusPosition(localPt, sp.dir);
		sp.dir = (focusP - si.worldPos()).as_dir() /oprtnormalize;

		if( !(sp.pdf = PDFW(sensorSurface.interaction, -sp.dir).value()) )
			return pl::nullopt;
		//const auto posPdf = PDF(sensorSurface);

		if(isDiracDelta())
		{
			sp.color = sensorSurface.pdf;
			return sp;
		}

		const auto dp = isFlat() ? sp.dir /odot/ mImagePlaneLobe.normal /oabs : Real{1};

		if(dp <= constants::epsilon<Real>)
			return pl::nullopt;

		pl::assert_valid(mCameraArea, valid::Weight);
		sp.color = pl::assert_valid( (mCameraArea * (dp /opow2 /opow2)) /oprtreciprocal * sensorSurface.pdf );

		if(isAlmostBlack(sp.color))
			return pl::nullopt;

		return sp;
	}

	pl::marked_optional<SampledSensorIn> CameraPinHole::sampleIn(const MaterialInteraction &surface, IRNGInt &rng, RandomReals<2> rndPos) const noexcept
	{
		auto sp = sampleImagePhotonDir(surface, rng, rndPos);
		if(!sp)
			return pl::nullopt;

		return std::move(sp->sensorIn);
	}

	pl::marked_optional<SampledSensorOut> CameraPinHole::sampleImageDir(IRNGInt &rng, RandomReals<2> rndPos, Point2Dr pos) const noexcept
	{
		const auto [localPt, dir] = rasterToRay(pos);

		// Point is behind the camera
		if(isFlat() && dir[size_v<2>] <= constants::epsilon<Real>)
			return pl::nullopt;

		return sampleArea(rng, rndPos)
			.and_then([&](auto areaSp) -> pl::marked_optional<SampledSensorOut>
			{
				SampledSensorOut sp{
						.sensor = std::move(areaSp),
						.dir{}
					};

				pl::visit_one(sp.sensor.interaction, [&](auto &li)
				{
					li.localPos( li.localPos() + localPt.as_dir() );
					li.worldPos( mMatrix.world * li.localPos() );

					pl::assert_valid(li.worldPos());

					const auto focusP = mMatrix.world * getFocusPosition(localPt, dir);
					sp.dir.dir = static_cast<VecD>(focusP - li.worldPos()) /oprtnormalize;

				});

				if( !(sp.dir.pdf = PDFW(sp.sensor.interaction, -sp.dir.dir).value()) )
					return pl::nullopt;

				pl::assert_valid(sp.dir.dir);
				pl::assert_valid(sp.sensor.pdf, valid::Weight);
				pl::assert_valid(sp.dir.pdf, valid::Weight);

				if(isDiracDelta())
				{
					sp.dir.color = sp.sensor.pdf;
					return sp;
				}

				const auto dp = sp.dir.dir /odot/ mImagePlaneLobe.normal /oabs;

				if(isFlat() && dp <= constants::epsilon<Real>)
					return pl::nullopt;

				pl::assert_valid(mCameraArea, valid::Weight);
				sp.dir.color = pl::assert_valid( (mCameraArea * (dp /opow2 /opow2)) /oprtreciprocal * sp.sensor.pdf );

				if(isAlmostBlack(sp.dir.color))
					return pl::nullopt;

				return sp;
			});
	}

	pl::marked_optional<Point2Dr> CameraPinHole::getImagePosition(const VecP origin, const VecD /*dir*/, const VecP /*destination*/) const noexcept
	{
		const auto imagePos = mMatrix.worldToRaster * pl::normalize_point_p{origin};

		if(const Point2Dr imagePoint{imagePos[size_v<0>], imagePos[size_v<1>]};
				pl::mask_all(imagePoint >= 0) && pl::mask_all(imagePoint < mFilmSize))
			return imagePoint;

		return pl::nullopt;
	}

	pl::marked_optional<SampledImagePhotonDir> CameraPinHole::sampleImagePhotonDir(const MaterialInteraction &surface, IRNGInt &rng, RandomReals<2> rndPos, const ISensor *scaleTo) const noexcept
	{
		return pl::visit_one(surface, [&](const auto &i) -> pl::marked_optional<SampledImagePhotonDir>
		{
			const auto localPt = mMatrix.worldToCam * i.worldPos();

			// Point is behind the camera
			if(isFlat() && localPt[size_v<2>] <= constants::epsilon<Real>)
				return pl::nullopt;

			const auto [ori, d, ip] = projectPoint(localPt);

			return sampleArea(rng, rndPos)
				.and_then([&](auto areaSp)
				{
					SampledImagePhotonDir sp{
							.sensorIn{
								.sensor = std::move(areaSp),
								.color{},
								.dir{},
								.pdf{}
							},
							.imagePoint = static_cast<Point2Dr>(ip)
						};

					return pl::visit_one(sp.sensorIn.sensor.interaction, [&](auto &li) -> pl::marked_optional<SampledImagePhotonDir>
						{
							li.localPos( li.localPos() + ori.as_dir() );
							li.worldPos( mMatrix.world * li.localPos() );

							pl::assert_valid(li.worldPos());

							sp.sensorIn.dir = static_cast<VecD>(li.worldPos() - i.worldPos());
							li.colDist = sp.sensorIn.dir /oprtlength;
							sp.sensorIn.dir = sp.sensorIn.dir /olnormalize/ li.colDist;

							if(!pl::is_valid(sp.sensorIn.dir))
								return pl::nullopt;

							return getImagePosition(i.worldPos(), sp.sensorIn.dir, li.worldPos())
								.and_then([&](const auto &ip) -> pl::marked_optional<SampledImagePhotonDir>
								{
									sp.imagePoint = ip;

									sp.sensorIn.pdf = scaleTo ?
										geometricTermPosEpsilon(scaleTo->worldToLocal(li.worldPos()), scaleTo->worldToLocal(i.worldPos())) :
										geometricTermPosEpsilon(li.worldPos(), i.worldPos());

									if(!sp.sensorIn.pdf)
										return pl::nullopt;

									if(isDiracDelta())
									{
										//sp.sensorIn.pdf *= sp.sensorIn.sensor.pdf;
										sp.sensorIn.color = sp.sensorIn.sensor.pdf;
										return sp;
									}

									auto dp = sp.sensorIn.dir /odot/ mImagePlaneLobe.normal /oabs;

									if(dp <= constants::epsilon<Real>)
										return pl::nullopt;

									dp = dp /oprtreciprocal;

									pl::assert_valid( sp.sensorIn.pdf *= dp, valid::PDF );
									pl::assert_valid( sp.sensorIn.color = mCameraAreaInv * (dp /opow2 /opow2) * sp.sensorIn.sensor.pdf );

									if(isAlmostBlack(sp.sensorIn.color))
										return pl::nullopt;

									return sp;
								});
						});
				});
		});
	}
}
