/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Device/LightTrace.hpp>
#include <PhotoRT/Core/Camera.hpp>
#include <PhotoRT/Core/Integrator.hpp>
#include <PhotoRT/Core/Integration.hpp>
#include <PhotoRT/Core/RayTracer.hpp>
#include <PhotoRT/Core/Scene.hpp>
#include <PhotoRT/Core/Film.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("device-light_trace", RenderDevice<RenderDeviceMasterLightTrace, RenderDeviceWorkerLightTrace, RenderDeviceWorkerLightTrace::WorkUnitType>)


	void RenderDeviceWorkerLightTrace::init()
	{
		RenderDeviceWorkerBase<PhotonSampleList>::init();

		mIntegrator = std::dynamic_pointer_cast<IIntegratorPhoton>(mIntegratorFactory->createIntegrator());
		if(!mIntegrator)
			throw Exception("Invalid integrator generated.");

		mIntegrator->init(*mScene);
		mIntegrator->setPhotonBuffers(mLightBuffers);
		mIntegrator->initRNG(mSampler);
	}

	void RenderDeviceWorkerLightTrace::resetStats()
	{
		RenderDeviceWorkerBase<PhotonSampleList>::resetStats();

		if(mIntegrator)
			mIntegrator->resetStats();
	}

	void RenderDeviceWorkerLightTrace::accumulateStats(StatsAccumulatorList &s)
	{
		s = mIntegrator ? mIntegrator->getStats() : StatsAccumulatorList();
	}

	pl::EnumWorkUnitState RenderDeviceWorkerLightTrace::process(PhotonSampleList &unit)
	{
		auto tracer = TracerParams{*mRayTracer, mSampler};

		mIntegrator->integrate(1, 1, 1024, unit, tracer);

		return pl::EnumWorkUnitState::Done;
	}

	void RenderDeviceWorkerLightTrace::restart()
	{
		RenderDeviceWorkerBase<PhotonSampleList>::restart();
	}

	void RenderDeviceWorkerLightTrace::initLightTrace(std::span<EntityPtr<ContributionBufferLight>> lightBuffers)
	{
		mLightBuffers = lightBuffers;
	}

	void RenderDeviceWorkerLightTrace::start(std::vector<pl::work_unit<PhotonSampleList>> &workUnits1, std::vector<pl::work_unit<PhotonSampleList>> &workUnits2)
	{
		RenderDeviceWorkerBase<PhotonSampleList>::start(workUnits1, workUnits2);
	}


	RenderDeviceMasterLightTrace::RenderDeviceMasterLightTrace()
	{
		mEntityList.mFactories = std::make_unique<EntityFactoriesProxy>(factories);
	}

	void RenderDeviceMasterLightTrace::prepareWorker(RenderDeviceWorkerLightTrace &worker, pl::back_buffered_work_units<PhotonSampleList> &workUnits)
	{
		RenderDeviceMasterBase<RenderDeviceWorkerLightTrace, PhotonSampleList>::prepareWorker(worker, workUnits);

		worker.initLightTrace(mLightBuffers);

		const auto initUnits = [&](auto &l)
			{
				l.mWorkData.list.resize(mLightBuffers.size());
			};

		workUnits.call([&](auto&& u){ u.mUnits | pl::for_each(initUnits); });
	}

	void RenderDeviceMasterLightTrace::init(const Scene &scene)
	{
		RenderDeviceMasterBase<RenderDeviceWorkerLightTrace, PhotonSampleList>::init(scene);

		mContributionBuffers.clear();
		mLightBuffers.clear();

		const auto nbLightGroups = scene.getLightGroups().size() + 1;

		using std::to_string;
		boost::uuids::random_generator gen;
		boost::uuids::uuid id = gen();
		const std::string rndId = "cbuf-light-trace:" + to_string(id) + ":";

		for(const auto &filmData : mRenderContext->mFilms | rgv::values)
		{
			auto facc = filmData.mFilm.access(sa::mode::shared);
			const auto &film = **facc;
			auto lightTarget = std::make_shared<ContributionBufferLight>();

			lightTarget->setEntityId(rndId + film.getEntityId() + ":light");
			lightTarget->setFilmID(std::string{film.getEntityId()});
			lightTarget->setNbLayers(nbLightGroups);
			lightTarget->setRenderRect({pl::size_p{film.getBufferSize()}});
			lightTarget->setCamera(film.getCamera());

			mContributionBuffers.push_back(lightTarget);
			mLightBuffers.push_back(lightTarget);
		}
	}

	void RenderDeviceMasterLightTrace::restart()
	{
		RenderDeviceMasterBase<RenderDeviceWorkerLightTrace, PhotonSampleList>::restart();

		mNbSamplesGenerated = 0;

		mRestoredState = false;
	}

	void RenderDeviceMasterLightTrace::end()
	{
		RenderDeviceMasterBase<RenderDeviceWorkerLightTrace, PhotonSampleList>::end();
	}

	bool RenderDeviceMasterLightTrace::prepare(PhotonSampleList &unit)
	{
		if(!mNbSamplesMax || mNbSamplesGenerated < mNbSamplesMax)
		{
			unit.clear();

			return true;
		}

		return false;
	}

	pl::EnumWorkUnitState RenderDeviceMasterLightTrace::process(PhotonSampleList &unit)
	{
		for(auto &l : unit.list)
			mNbSamplesDone += l.nbSamples;

		return pl::EnumWorkUnitState::Done;
	}

	void RenderDeviceMasterLightTrace::work()
	{
		RenderDeviceMasterBase<RenderDeviceWorkerLightTrace, PhotonSampleList>::work();
	}

	void RenderDeviceMasterLightTrace::dumpInfo(Scene &, const std::filesystem::path &/*folder*/, const std::string &/*name*/)
	{
	}

	bool RenderDeviceMasterLightTrace::setRenderRect(std::string_view /*filmID*/, const pl::rectangle<> &/*rect*/)
	{
		return false;
	}
}
