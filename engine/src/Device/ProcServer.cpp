/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>

#ifdef PRT_IPC

#include <PhotoRT/Device/ProcServer.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/Parser.hpp>

#include <boost/interprocess/ipc/message_queue.hpp>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_DEFINE(RenderDeviceProcServer)

	const char *RenderDeviceProcServer::ModuleException::module() const noexcept
		{ return "RenderDeviceProcServer"; }

	struct RenderDeviceProcServer::Client
	{
		std::string command, id;
		//boost::interprocess::message_queue mesQueue;
		boost::process::ipstream is, err;
		boost::process::opstream os;
		boost::process::child proc;

		boost::interprocess::message_queue logQueue;

		std::vector<EntityPtr<ContributionBuffer>> buffers;

		Client(std::string command_, std::string id_, boost::process::group &g, std::string_view args = {}) :
				command(std::move(command_)),
				id(std::move(id_)),
				logQueue(boost::interprocess::create_only, (id + "-mq").c_str(), 1024, 1024)
			{ launch(g, args); }

		~Client()
		{
			boost::interprocess::message_queue::remove((id + "-mq").c_str());
		}

		bool running()
			{ return proc.running(); }

		void terminate()
			{ proc.terminate(); }

		void wait()
			{ proc.wait(); }

		void launch(boost::process::group &g, std::string_view args = {})
		{
			pl::recreate(os);
			pl::recreate(err);
			pl::recreate(is);

			pl::recreate(proc,
				command + " --id=" + id +
					" --msysid_default=" + getMemorySystem<>()->instance_id() +
					" --msysid_primitive=" + getMemorySystem<memtag::Primitive>()->instance_id() +
					" --msysid_contbuf=" + getMemorySystem<memtag::ContBuffer>()->instance_id() +
					" --msysid_texture=" + getMemorySystem<memtag::Texture>()->instance_id() +
					" --msysid_film=" + getMemorySystem<memtag::Film>()->instance_id() +
					' ' + args,
				g,
				boost::process::std_out > is,
				boost::process::std_in < os,
				boost::process::std_err > err
			);

			is.imbue(std::locale::classic());
			os.imbue(std::locale::classic());
		}
	};


	RenderDeviceProcServer::~RenderDeviceProcServer()
	{
		broadcastCommand("terminate");
	}

	void RenderDeviceProcServer::setParameters(const ParameterTree &params)
	{
		RenderDeviceServer::setParameters(params);

		broadcastCommand("terminate");

		mClients.clear();
		mClientDeviceParams.clear();
		mClientConfig.clear();

		if(auto p = params.findParam("device_params"); p && p->childParams)
		{
			mClientDeviceParams = {
				{"e", {p->value, *p->childParams}}
			};
		}

		if(auto p = params.findParam("config"); p && p->childParams)
			mClientConfig = *p->childParams;

		if(auto p = params.findParam("clients"); p && p->childParams)
		{
			for(auto&& [i, param] : rgv::enumerate(*p->childParams))
			{
				auto id = pl::io::stream_into(std::string{}, pl::io::throw_on_failure,
					[&](auto &os, auto&& str){ os << "prt_" << std::this_thread::get_id() << '_' << (i + 1); return str; });
				std::string cmd{param.second.value};

				if(mCommandFilter)
					mCommandFilter->filterCommand(cmd);

				auto &client = mClients.emplace_back(std::move(cmd), std::move(id), mProcGroup);

				log() << "Launching render process with command \"" << client.command << "\"";
				launchClient(client);
			}
		}
	}

	ParameterTree RenderDeviceProcServer::getParameters(bool deep) const
	{
		ParameterTree params {RenderDeviceServer::getParameters(deep)};

		for(auto &clientsParam = params["clients"]; auto&& [i, client] : rgv::enumerate(mClients))
			clientsParam[i].value = client.command;

		return params;
	}

	void RenderDeviceProcServer::clearClientBuffers(Client &client)
	{
		for(auto &buf : client.buffers)
		{
			mRenderContext->unregisterBuffer(buf);
			contributionBuffers.erase(rg::find(contributionBuffers, buf));
		}

		client.buffers.clear();
	}

	void RenderDeviceProcServer::getBufferTypes(Client &client)
	{
		sendCommand("get_cb_types", client, [this] (Client &client, std::string_view /*command*/)
		{
			for(const auto &line : rg::getlines(client.is))
			{
				if(line == "done.")
					break;

				std::string filmID, type, id;

				pl::io::stream_from(line, pl::io::throw_on_failure, [&](auto&& is, auto&&){ is >> filmID >> type >> id; });

				if(!filmID.empty() && !type.empty() && !id.empty())
				{
					if(auto filmIt = mRenderContext->mFilms.find(filmID);
							filmIt != mRenderContext->mFilms.end() &&
							factories.factoryExists("contribution_buffer-" + type) &&
							!mReceiveBuffers[filmID][type])
					{
						auto facc = filmIt->second.mFilm.access(sa::mode::excl);
						const auto &film = **facc;
						auto compiledTarget = factories.createEntity<ContributionBuffer>("contribution_buffer-" + type);

						compiledTarget->setOwner(false);
						compiledTarget->setEntityId(id);
						compiledTarget->setFilm(&film);
						compiledTarget->setFilmID(filmID);

						contributionBuffers.push_back(compiledTarget);
						client.buffers.push_back(compiledTarget);

						mRenderContext->registerBuffer(compiledTarget);

						log() << '[' << client.id << "] Buffer: " << type << " for film " << filmID << " id " << id << " size " << compiledTarget->getRenderRect();
					}
				}
			}

			client.is >> pl::io::skip_eol;

			mRenderContext->initBuffers();
		});
	}

	void RenderDeviceProcServer::sendData(Client &client)
	{
		// Send render parameters
		ParameterTree renderRectParams{
			{"param_id", mRenderContext->getEntityId()},
			{"film", mRenderFilmID},
			{"rect", mRenderRect},
			{"shmid", mScene->getShmID()}
		};

		mScene->mParser->save(renderRectParams, client.os);

		// Send scene parameters
		client.os << *mScene;

		// Send scene data
		DataSourceStream dsn;

		dsn.setOStream(client.os);

		std::string id;
		id.reserve(256);

		log() << "Sending scene data.";

		for(const auto &line : rg::getlines(client.is))
		{
			if(line.starts_with("data:"))
			{
				id = line.substr(5);

				if(auto entity = mScene->mEntities.left.find(id); entity != mScene->mEntities.left.end())
				{
					log() << '[' << client.id << "] Sending data for entity \"" << id << "\" of type \"" << entity->second->getEntityType() << "\"";
					dsn.serialize(*entity->second);
				}
			}
			else if(line == "done.")
				break;
		}

		client.is >> pl::io::skip_eol;

		log() << "Done.";
	}

	bool RenderDeviceProcServer::prepare_to_end()
	{
		broadcastCommand("end");

		return true;
	}

	bool RenderDeviceProcServer::prepare_to_pause()
	{
		broadcastCommand("pause");

		return true;
	}

	void RenderDeviceProcServer::start()
	{
		if(paused())
			broadcastCommand("start");

		RenderDeviceServer::start();
	}

	void RenderDeviceProcServer::on_start()
	{
		log() << "Starting.";

		for(auto &t : contributionBuffers)
			mRenderContext->unregisterBuffer(t);

		contributionBuffers.clear();

		for(auto &client : mClients)
		{
			client.buffers.clear();
		}

		broadcastCommand("scene_data", [this] (Client &client, std::string_view /*command*/)
		{
			sendData(client);

			getBufferTypes(client);

			sendCommand("start", client);

			if(paused())
				sendCommand("pause", client);
		});

		RenderDeviceServer::on_start();
	}

	void RenderDeviceProcServer::on_restart()
	{
		broadcastCommand("restart");

		RenderDeviceServer::on_restart();
	}

	void RenderDeviceProcServer::on_end()
	{
		log() << "Ending.";

		broadcastCommand("end");

		RenderDeviceServer::on_end();
	}

	void RenderDeviceProcServer::sendCommand(std::string_view command, Client &client, CommandHandler handler)
	{
		client.os << "$cmd: " << command << std::endl;

		if(handler)
			handler(client, command);
	}

	void RenderDeviceProcServer::broadcastCommand(std::string_view command, CommandHandler handler)
	{
		for(auto &client : mClients)
			sendCommand(command, client, handler);
	}

	void RenderDeviceProcServer::launchClient(Client &client)
	{
		client.is.exceptions(pl::io::failure_bits);
		client.os.exceptions(pl::io::failure_bits);
		client.err.exceptions(pl::io::failure_bits);

		client.is >> pl::io::binary >> pl::io::ipc_on;
		client.os << pl::io::binary << pl::io::ipc_on;

		mScene->mParser->save(mClientConfig, client.os);
		mScene->mParser->save(mClientDeviceParams, client.os);
		client.os << std::flush;

		log() << '[' << client.id << "] Render process launched.";
	}

	void RenderDeviceProcServer::process_loop()
	{
		std::string logEntry, logModule;

		work_loop([&]{

			try
			{
				std::scoped_lock g{mStatsMutex};

				mClientStats.clear();

				for(auto&& [i, client] : rgv::enumerate(mClients))
				{
					for([[maybe_unused]] const auto i : mir(client.logQueue.get_num_msg()))
					{
						decltype(client.logQueue)::size_type size;
						unsigned int priority;
						logEntry.resize(client.logQueue.get_max_msg_size());
						if(!client.logQueue.try_receive(logEntry.data(), logEntry.size() * sizeof(logEntry[0]), size, priority))
							break;

						logEntry.resize(size);

						try
						{
							pl::io::stream_from(logEntry, pl::io::throw_on_failure, [&](auto&& is, auto&&){
								int logtype = 0;

								is >> logtype >> logModule >> pl::io::skip_one;

								const auto doLog = [&](auto tag)
									{ log(tag) << '[' << client.id << "] [" << logModule << "] " << is.rdbuf(); };

								if(logtype == 1)
									doLog(pl::logtag::error);
								else
									doLog(pl::logtag::message);
							});
						}
						catch(...)
						{
							log(pl::logtag::error, "[{}] Failed to read log message from client.", client.id);
						}
					}

					if(!client.running())
					{
						log(pl::logtag::error, "[{}] Render process has probably crashed.", client.id);

						client.terminate();
						client.wait();

						clearClientBuffers(client);

						log("[{}] Relaunching render process.", client.id);

						client.launch(mProcGroup);
						launchClient(client);

						sendCommand("scene_data", client, [this] (Client &client, std::string_view /*command*/)
						{
							sendData(client);
						});
						getBufferTypes(client);

						if(started())
						{
							sendCommand("start", client);

							if(paused())
								sendCommand("pause", client);
						}
					}
					else
					{
						sendCommand("stats", client, [&] (Client &client, std::string_view /*command*/)
						{
							const auto params = mScene->mParser->load(client.is);

							const auto strI = "proc_" + pl::io::to_string_noloc(i);
							for(const auto &stat : params | rgv::values)
							{
								mClientStats[strI + ": " + stat["n"].value] = stat["v"].value;
							}
						});
					}
				}
			}
			catch(const boost::process::process_error &err)
			{
				log(pl::logtag::error) << "Error: " << err.what();
			}
			catch(const std::exception &err)
			{
				log(pl::logtag::error) << "Error: " << err.what();
			}
			catch(...)
			{
				log(pl::logtag::error) << "Unknown error occured.";
			}

			sleep_for(3s);

			return true;
		});
	}

	void RenderDeviceProcServer::getStats(Statistics &stats, const std::locale &) const
	{
		std::scoped_lock g(mStatsMutex);

		stats.insert(mClientStats.begin(), mClientStats.end());
	}
}

#endif
