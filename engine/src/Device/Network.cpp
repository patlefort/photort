/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>

#ifdef PRT_NETWORKING

#include <PhotoRT/Device/Network.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/Parser.hpp>
#include <PhotoRT/Core/Compression.hpp>

#include <boost/logic/tribool.hpp>
#include <boost/logic/tribool_io.hpp>
#include <boost/iostreams/filtering_stream.hpp>

#include <fmt/ostream.h>
#include <fmt/std.h>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_DEFINE(RenderDeviceServerIOStream)


	RenderDeviceServerIOStream::ModuleException::ModuleException(std::string_view message, const boost::system::error_code &er_, const std::experimental::source_location &sl) noexcept
		: Exception(message, sl), er{er_}
	{}

	const char *RenderDeviceServerIOStream::ModuleException::module() const noexcept
		{ return "RenderDeviceServerIOStream"; }

	void RenderDeviceServerIOStream::setParameters(const ParameterTree &params)
	{
		RenderDeviceServer::setParameters(params);

		params.getValueIfExistsBind("max_nbclients", PL_LAMBDA_FORWARD_THIS(setMaxNbClients));
		params.getValueIfExistsBind("port", PL_LAMBDA_FORWARD_THIS(setPort));
		params.getValueIfExistsBind("interval", PL_LAMBDA_FORWARD_THIS(setRefreshRate));
		params.getValueIfExistsBind("binary", PL_LAMBDA_FORWARD_THIS(setBinary));

		if(const auto p = params.findValue("compressor"); p)
		{
			if(p->empty())
				setCompressor(std::string{compressorString});
			else
				setCompressor(*p);
		}
	}

	ParameterTree RenderDeviceServerIOStream::getParameters(bool deep) const
	{
		ParameterTree params {RenderDeviceServer::getParameters(deep)};

		params["max_nbclients"] = mMaxNbClients;
		params["port"] = mPort;
		params["interval"] = std::chrono::duration_cast<std::chrono::milliseconds>(mRefreshRate).count();
		params["binary"] = mIoBinary;
		params["compressor"] = mIoCompressor;

		return params;
	}

	void RenderDeviceServerIOStream::start()
	{
		if(!started())
		{
			log() << "Starting.";

			{
				using std::to_string;
				boost::uuids::random_generator gen;
				boost::uuids::uuid id = gen();

				mRenderGUID = to_string(id);
			}

			mClients.clear();

			mReceiveBuffers.clear();
			mCompileBuffers.clear();

			for(auto &t : mContributionBuffers)
				mRenderContext->unregisterBuffer(t);

			mContributionBuffers.clear();
		}

		if(paused())
		{
			broadcastCommand("start");
		}

		RenderDeviceServer::start();
	}

	void RenderDeviceServerIOStream::on_end()
	{
		log() << "Ending.";

		mAcceptor = nullptr;
		//acceptor6 = nullptr;
		mClients.clear();
	}

	bool RenderDeviceServerIOStream::prepare_to_end()
	{
		broadcastCommand("end");

		return true;
	}

	bool RenderDeviceServerIOStream::prepare_to_pause()
	{
		broadcastCommand("pause");

		return true;
	}

	void RenderDeviceServerIOStream::on_restart()
	{
		broadcastCommand("restart");
		getClientDataAsync();

		RenderDeviceServer::on_restart();
	}

	void RenderDeviceServerIOStream::broadcastCommand(std::string_view command, CommandHandler handler)
	{
		for(auto it = mClients.begin(); it != mClients.end();)
		{
			try
			{
				sendCommand(command, *it, handler);
				++it;
			}
			catch(...)
			{
				it = mClients.erase(it);
			}
		}
	}

	void RenderDeviceServerIOStream::sendCommand(std::string_view command, Client &client, CommandHandler handler)
	{
		*client.stream << "$cmd: " << command << std::endl;

		auto er = client.stream->error();

		if(er)
		{
			log(pl::logtag::error, "Client {} disconnected: {}", fmt::streamed(client.remote_endpoint), er.message());

			client.valid = false;
		}
		else
			log("[{}] Command sent: {}", fmt::streamed(client.remote_endpoint), command);

		if(handler)
			handler(client, command, er);

		if(er && !handler)
			throw ModuleException(er.message(), er);
	}

	void RenderDeviceServerIOStream::process_loop()
	{
		work_loop([this]{

			if(!mAcceptor)
			{
				const auto port = pl::io::from_string_noloc(mPort, type_v<bio::ip::port_type>);

				try {
					mAcceptor = std::make_shared<bio::ip::tcp::acceptor>(mIoService, bio::ip::tcp::endpoint{bio::ip::tcp::v4(), port});
				} catch(...) {
					mAcceptor = std::make_shared<bio::ip::tcp::acceptor>(mIoService, bio::ip::tcp::endpoint{bio::ip::tcp::v6(), port});
				}

				if(!mAcceptor)
				{
					log(pl::logtag::error, "Unable to open {}.", mPort);
					sleep_for(1s);
					return true;
				}

				acceptClient();

				getClientDataAsync();
			}

			try {
				mWaiting = !mIoService.poll();
			} catch(const boost::system::error_code &er) {
				log(pl::logtag::error) << "Error while polling handlers: " << er.message();
			}

			mIoService.restart();

			sleep_for(100ms);

			return true;
		});
	}

	void RenderDeviceServerIOStream::getClientData()
	{
		broadcastCommand("get_buffers", [this](Client &client, std::string_view /*command*/, boost::system::error_code er)
			{
				if(er)
					return;

				for(const auto &line : rg::getlines(*client.stream))
				{
					if(line.empty())
						break;

					std::string filmID, type;

					std::stringstream ss(line);
					std::getline(ss, filmID, ' ');
					std::getline(ss, type, ' ');

					if(!filmID.empty() && !type.empty())
					{
						if(mReceiveBuffers.find(filmID) != mReceiveBuffers.end() && mReceiveBuffers[filmID].find(type) != mReceiveBuffers[filmID].end())
						{
							auto contribBuffer = mReceiveBuffers[filmID][type],
									compiledTarget = mCompileBuffers[filmID][type];

							try
							{
								selectDecompressor(client.finalOptions["compressor"].value, [&](auto&& decompressor){
									pl::io::read_stream(*client.stream, [&](auto& is){ is >> *contribBuffer; }, PL_FWD(decompressor));
								});

								//*client.stream >> *contribBuffer;
								compiledTarget->merge(*contribBuffer);
							}
							catch(const std::exception &e)
							{
								log(pl::logtag::error, "[{}] Error receiving buffer for film {} and type {}: {}",
									fmt::streamed(client.remote_endpoint), pl::io::quoted{filmID}, pl::io::quoted{type}, e.what());
							}
							catch(...)
							{
								log(pl::logtag::error, "[{}] Error receiving buffer for film {} and type {}.",
									fmt::streamed(client.remote_endpoint), pl::io::quoted{filmID}, pl::io::quoted{type});
							}
						}
					}
				}

				*client.stream >> pl::io::skip_eol;
			});
	}

	void RenderDeviceServerIOStream::setRenderRect(std::string_view /*filmID*/, const pl::rectangle<> &/*rect*/)
	{
		/*call([this, filmID, rect]{
			std::scoped_lock g(mRenderParamMutex);

			RenderDeviceServer::setRenderRect(filmID, rect);

			broadcastCommand("set_render_rect", [this] (Client &client, std::string_view, boost::system::error_code er)
				{
					if(er)
						return;

					*client.stream << mRenderFilmID << '\n' << mRenderRect << '\n';

					getBufferTypes(client);
				});
		});*/
	}

	void RenderDeviceServerIOStream::notifyEntityUpdate(IRenderEntity &entity)
	{
		ParameterTree params{
			{"0", {std::string{entity.getEntityId()}, entity.getParameters(false)}}
		};

		broadcastCommand("entity_update", [&] (Client &client, std::string_view /*command*/, boost::system::error_code er)
			{
				if(er)
					return;

				//params >> *client.stream;
				mScene->mParser->save(params, *client.stream);
			});
	}

	void RenderDeviceServerIOStream::getClientDataAsync()
	{
		mTimer = std::make_shared<bio::steady_timer>(mIoService, mRefreshRate);

		mTimer->async_wait([this](const boost::system::error_code &er)
			{
				if(er)
				{
					log(pl::logtag::error) << "Error on async_wait routine: " << er.message();
					return;
				}

				const auto nbClients = getNbClients();

				if(nbClients)
					log() << "Doing routine. Nb clients: " << nbClients;

				std::scoped_lock g(mRenderParamMutex);

				getClientData();

				getClientDataAsync();

				for(auto it=mClients.begin(); it!=mClients.end(); ++it)
					if(!it->valid)
						it = mClients.erase(it);
			});
	}

	void RenderDeviceServerIOStream::acceptClient()
	{
		const auto doAccept = [this](auto &acceptor)
			{
				Client client;

				client.stream = std::make_unique<bio::ip::tcp::iostream>();
				auto &sock = client.stream->socket();

				acceptor->async_accept(sock, [this, &sock, &client](boost::system::error_code er)
				{
					if(er)
					{
						log(pl::logtag::error) << "Error on client connection: " << er.message();
					}
					else if(getNbClients() >= getMaxNbClients())
					{
						log(pl::logtag::error) << "Connection request received but maximum number of clients reached.";
					}
					else
					{
						client.remote_endpoint = sock.remote_endpoint();

						log() << "Client connected: " << client.remote_endpoint;

						try
						{
							setSocketOptions(sock, 10000);

							sock.set_option(bio::socket_base::send_buffer_size(1024 * 1024 * 1024));
							sock.set_option(bio::socket_base::receive_buffer_size(1024 * 1024 * 1024));

							client.stream->imbue(Parameter::loc);
							client.stream->exceptions(pl::io::failure_bits);

							ParameterTree clientOptions;

							clientOptions = mScene->mParser->load(*client.stream);

							if(clientOptions.getValueOrDefault("64_bits_index", false) != use64BitsIndex)
								throw Exception("Both the client and server must use the same type of indexes.");

							if(clientOptions.getValueOrDefault("double", false) != useDouble)
								throw Exception("Both the client and server must use the same precision level for floating points.");

							const auto checkOverride = [&clientOptions](bool &v, std::string_view key)
								{
									if(auto p = clientOptions.findParam(key); p)
									{
										boost::tribool opt = *p;

										if(!boost::indeterminate(opt))
											v = (bool)opt;
									}
								};

							checkOverride(mIoBinary, "binary");

							client.finalOptions["binary"] = mIoBinary;
							client.finalOptions["compressor"] = clientOptions.findValue("compressor").value_or(mIoCompressor);

							if constexpr(pl::native_endian == std::endian::big)
								client.finalOptions["endian"].value = "big";
							else if constexpr(pl::native_endian == std::endian::little)
								client.finalOptions["endian"].value = "little";
							else
								client.finalOptions["endian"].value = "native";

							mScene->mParser->save(client.finalOptions, *client.stream);

							if(mIoBinary)
							{
								log("[{}] Using binary serialization.", fmt::streamed(client.remote_endpoint));
								*client.stream << pl::io::binary;
								*client.stream >> pl::io::binary;
							}
							else
							{
								log("[{}] Using text serialization.", fmt::streamed(client.remote_endpoint));
								*client.stream << pl::io::text;
								*client.stream >> pl::io::text;
							}

							if(!client.finalOptions["compressor"].value.empty())
							{
								log("[{}] Using {} compression.", fmt::streamed(client.remote_endpoint), pl::io::quoted{client.finalOptions["compressor"].value});
							}
							else
							{
								log("[{}] Using no compression.", fmt::streamed(client.remote_endpoint));
							}

							std::scoped_lock g{mRenderParamMutex};

							log("[{}] Sending scene data...", fmt::streamed(client.remote_endpoint));

							sendCommand("scene_data", client, [this] (Client &client, std::string_view /*command*/, boost::system::error_code er)
							{
								if(er)
									return;

								// Send render parameters
								ParameterTree renderRectParams{
									{"render_guid", mRenderGUID},
									{"param_id",    mRenderContext->getEntityId()},
									{"film",        mRenderFilmID},
									{"rect",        mRenderRect}
								};

								mScene->mParser->save(renderRectParams, *client.stream);

								log("[{}] Film: {}", fmt::streamed(client.remote_endpoint), pl::io::quoted{renderRectParams["film"].value});
								log("[{}] Rectangle: {}", fmt::streamed(client.remote_endpoint), pl::io::quoted{renderRectParams["rect"].value});

								bool needData = std::invoke([&]{
										std::string line;
										std::getline(*client.stream, line);

										return line == "need_data";
									});

								if(needData)
								{
									// Send scene parameters
									*client.stream << *mScene;

									// Send scene data
									DataSourceStream dsn;

									dsn.setOStream(*client.stream);
									dsn.setCompressor(client.finalOptions["compressor"].value);

									std::string id;
									id.reserve(256);

									for(const auto &line : rg::getlines(*client.stream))
									{
										if(line.starts_with("data:"))
										{
											id = line.substr(5);

											if(auto entity = mScene->mEntities.left.find(id); entity != mScene->mEntities.left.end())
											{
												log("[{}] Sending data for entity {} [{}]", fmt::streamed(client.remote_endpoint), pl::io::quoted{id}, entity->second->getEntityType());

												dsn.serialize(*entity->second);
											}
										}
										else if(line == "done.")
											break;
									}

									*client.stream >> pl::io::skip_eol;
								}
							});

							log("[{}] Done.", fmt::streamed(client.remote_endpoint));

							getBufferTypes(client);

							sendCommand("start", client);

							if(paused())
								sendCommand("pause", client);

							log("Client {} accepted.", fmt::streamed(client.remote_endpoint));

							mClients.emplace_back(std::move(client));
						}
						catch(const std::exception &e)
						{
							log(pl::logtag::error, "[{}] Error: {}", fmt::streamed(client.remote_endpoint), e.what());
						}
						catch(...)
						{
							log(pl::logtag::error, "[{}] Error on client connection.", fmt::streamed(client.remote_endpoint));
						}
					}

					acceptClient();
				});
			};

		if(mAcceptor)
			doAccept(mAcceptor);

		//if(acceptor6)
			//doAccept(bio::ip::tcp::v6(), acceptor6);
	}

	void RenderDeviceServerIOStream::getBufferTypes(Client &client)
	{
		sendCommand("get_cb_types", client, [this] (Client &client, std::string_view /*command*/, boost::system::error_code er)
		{
			if(er)
				return;

			using std::to_string;
			boost::uuids::random_generator gen;
			boost::uuids::uuid id = gen();
			const std::string rndId = "cbuf-network:" + to_string(id) + ":";

			for(const auto &line : rg::getlines(*client.stream))
			{
				if(line == "done.")
					break;

				std::string filmID, type;

				pl::io::stream_from(line, pl::io::throw_on_failure, [&](auto&& is, auto&&){ is >> filmID >> type; });

				if(!filmID.empty() && !type.empty())
				{
					if(auto filmIt = mRenderContext->mFilms.find(filmID); filmIt != mRenderContext->mFilms.end() && factories.factoryExists("contribution_buffer-" + type) && !mReceiveBuffers[filmID][type])
					{
						auto facc = filmIt->second.mFilm.access(sa::mode::shared);
						const auto &film = **facc;
						const auto filmBufSize = film.getBufferSize();

						auto contribBuffer = mReceiveBuffers[filmID][type] = factories.createEntity<ContributionBuffer>("contribution_buffer-" + type);
						contribBuffer->setEntityId(rndId + filmID + ":cbuf-" + type);
						contribBuffer->setFilm(&film);
						contribBuffer->setFilmID(filmID);
						contribBuffer->setNbLayers(mRenderContext->getNbLayers());
						contribBuffer->setRenderRect({pl::size_p{filmBufSize}});

						auto compiledTarget = mCompileBuffers[filmID][type] = factories.createEntity<ContributionBuffer>("contribution_buffer-" + type);
						compiledTarget->setEntityId(rndId + filmID + ":cbuf-" + type + ":compiled");
						compiledTarget->setFilm(&film);
						compiledTarget->setFilmID(filmID);
						compiledTarget->setNbLayers(mRenderContext->getNbLayers());
						compiledTarget->setRenderRect({pl::size_p{filmBufSize}});
						compiledTarget->zeroBuffers();

						mContributionBuffers.push_back(compiledTarget);
						mRenderContext->registerBuffer(compiledTarget);

						log("[{}] Buffer: {} for film {}.", fmt::streamed(client.remote_endpoint), pl::io::quoted{type}, pl::io::quoted{filmID});
					}
				}
			}

			*client.stream >> pl::io::skip_eol;

			mRenderContext->initBuffers();
		});
	}

}

#endif
