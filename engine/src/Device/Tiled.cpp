/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Device/Tiled.hpp>
#include <PhotoRT/Core/Camera.hpp>
#include <PhotoRT/Core/Integrator.hpp>
#include <PhotoRT/Core/Integration.hpp>
#include <PhotoRT/Core/RayTracer.hpp>
#include <PhotoRT/Core/Scene.hpp>
#include <PhotoRT/Core/Film.hpp>
#include <PhotoRT/Core/PixelSampler.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("device-tiled", RenderDevice<RenderDeviceMasterTiled, RenderDeviceWorkerTiled, RenderDeviceWorkerTiled::WorkUnitType>)

	void RenderDeviceWorkerTiled::init()
	{
		RenderDeviceWorkerBase<PixelSampleTileLayers>::init();

		mIntegrator = std::dynamic_pointer_cast<IIntegratorTile>(mIntegratorFactory->createIntegrator());
		if(!mIntegrator)
			throw Exception("Invalid integrator generated.");

		mIntegrator->init(*mScene);
		mIntegrator->setPhotonBuffers(mLightBuffers);

		const auto nbRng = std::max<szt>(mSamplers.size(), 1);
		mRngEngines.clear();
		mRngEngines.reserve(nbRng);
		mRngEngines.shrink_to_fit();

		pl::repeat(nbRng, [&]{
			mIntegrator->initRNG(mRngEngines.emplace_back());
		});
	}

	void RenderDeviceWorkerTiled::resetStats()
	{
		RenderDeviceWorkerBase<PixelSampleTileLayers>::resetStats();

		if(mIntegrator)
			mIntegrator->resetStats();
	}

	void RenderDeviceWorkerTiled::accumulateStats(StatsAccumulatorList &s)
	{
		s = mIntegrator ? mIntegrator->getStats() : StatsAccumulatorList();
	}

	pl::EnumWorkUnitState RenderDeviceWorkerTiled::process(PixelSampleTileLayers &unit)
	{
		auto tracer = TracerParams{*mRayTracer, mRngEngines[unit.samplerIndex]};

		mIntegrator->integrate(unit, tracer);

		return pl::EnumWorkUnitState::Done;
	}

	void RenderDeviceWorkerTiled::restart()
	{
		RenderDeviceWorkerBase<PixelSampleTileLayers>::restart();

		for(auto &r : mRngEngines)
			r.seed();
	}

	void RenderDeviceWorkerTiled::initTiled(
		std::span<EntityPtr<IPixelSampler>> samplers,
		std::span<EntityPtr<ContributionBufferLight>> lightBuffers,
		std::span<EntityPtr<ContributionBufferPixel>> pixelBuffers)
	{
		mSamplers = samplers;
		mLightBuffers = lightBuffers;
		mPixelBuffers = pixelBuffers;
	}


	RenderDeviceMasterTiled::RenderDeviceMasterTiled()
	{
		mEntityList.mFactories = std::make_unique<EntityFactoriesProxy>(factories);
	}

	void RenderDeviceMasterTiled::prepareWorker(RenderDeviceWorkerTiled &worker, pl::back_buffered_work_units<PixelSampleTileLayers> &workUnits)
	{
		RenderDeviceMasterBase<RenderDeviceWorkerTiled, PixelSampleTileLayers>::prepareWorker(worker, workUnits);

		worker.initTiled(mPixelSamplers, mLightBuffers, mPixelBuffers);
	}

	void RenderDeviceMasterTiled::init(const Scene &scene)
	{
		RenderDeviceMasterBase<RenderDeviceWorkerTiled, PixelSampleTileLayers>::init(scene);

		mContributionBuffers.clear();
		mPixelSamplers.clear();
		mPixelBuffers.clear();
		mLightBuffers.clear();

		const auto nbLightGroups = scene.getLightGroups().size() + 1;

		using std::to_string;
		boost::uuids::random_generator gen;
		boost::uuids::uuid id = gen();
		const std::string rndId = "cbuf-tiled:" + to_string(id) + ":";

		for(const auto &filmData : mRenderContext->mFilms | rgv::values)
		{
			auto facc = filmData.mFilm.access(sa::mode::shared);
			const auto &film = **facc;
			const pl::rectangle<> renderRect{pl::size_p{film.getBufferSize()}};

			if(mUsePixelBuffer)
			{
				auto sampler = mEntityList.createEntity(type_v<IPixelSampler>, mPixelSamplerParams);
				mPixelSamplers.push_back(sampler);

				sampler->setCamera(*film.getCamera());

				auto contribBuffer = std::make_shared<ContributionBufferPixel>();

				contribBuffer->setEntityId(rndId + film.getEntityId() + ":pixel");
				contribBuffer->setFilm(&film);
				contribBuffer->setFilmID(std::string{film.getEntityId()});
				contribBuffer->setNbLayers(nbLightGroups);
				contribBuffer->setRenderRect(renderRect);
				contribBuffer->setCamera(film.getCamera());

				sampler->setDimensions(PRT_SAMPLE_RECT_SIZE_MAX, contribBuffer->getNbTiles(), contribBuffer->getPadding(), contribBuffer->getRenderRect().dims());

				mContributionBuffers.push_back(contribBuffer);
				mPixelBuffers.push_back(contribBuffer);
			}

			if(mUseLightBuffer)
			{
				auto lightContribBuffer = std::make_shared<ContributionBufferLight>();

				lightContribBuffer->setEntityId(rndId + film.getEntityId() + ":light");
				lightContribBuffer->setFilm(&film);
				lightContribBuffer->setFilmID(std::string{film.getEntityId()});
				lightContribBuffer->setNbLayers(nbLightGroups);
				lightContribBuffer->setRenderRect(renderRect);
				lightContribBuffer->setCamera(film.getCamera());

				mContributionBuffers.push_back(lightContribBuffer);
				mLightBuffers.push_back(lightContribBuffer);
			}

		}

		for(auto &ps : mPixelSamplers)
		{
			ps->init();
			ps->reset();
		}
	}

	void RenderDeviceMasterTiled::restart()
	{
		RenderDeviceMasterBase<RenderDeviceWorkerTiled, PixelSampleTileLayers>::restart();

		mNbSamplesGenerated = 0;
		mPixelSampleIndex = 0;
		mNbTilesGenerated = 0;
		mNbTilesDone = 0;

		for(auto &sp : mPixelSamplers)
			sp->reset();

		mRestoredState = false;

		mRng.seed();
	}

	void RenderDeviceMasterTiled::end()
	{
		RenderDeviceMasterBase<RenderDeviceWorkerTiled, PixelSampleTileLayers>::end();
	}

	bool RenderDeviceMasterTiled::workAllDone() const
	{
		if(RenderDeviceMasterBase<RenderDeviceWorkerTiled, PixelSampleTileLayers>::workAllDone())
			return true;

		if(!mPixelSamplers.empty())
		{
			for(auto &ps : mPixelSamplers)
				if(!ps->done())
					return false;
			return mNbTilesDone == mNbTilesGenerated;
		}

		return false;
	}

	bool RenderDeviceMasterTiled::prepare(PixelSampleTileLayers &unit)
	{
		if(!mNbSamplesMax || mNbSamplesGenerated < mNbSamplesMax)
		{
			if(mUsePixelBuffer)
			{
				if(mTargetPs)
				{
					if(!mTargetPs->generateTile(unit, mRng))
						return false;

					unit.samplerIndex = mTargetPsIndex;
				}
				else
				{
					if(!mPixelSamplers[mPixelSampleIndex]->generateTile(unit, mRng))
						return false;

					unit.samplerIndex = mPixelSampleIndex;
					mPixelSampleIndex = (mPixelSampleIndex + 1) % mPixelSamplers.size();
				}

				if(unit.tiles.empty())
				{
					unit.tiles.resize(mScene->getLightGroups().size() + 1);
					for(auto &t : unit.tiles)
						t.color.resize(unit.bufferSize[0] * unit.bufferSize[1]);

					const auto &film = *mPixelBuffers[unit.samplerIndex]->getFilm();
					unit.filterSize = film.getFilterTargetSize();
					unit.filter = pl::mask_all(unit.filterSize > 0) ? &film.getFilter() : nullptr;
				}

				unit.zero();

				mNbSamplesGenerated += PRT_SAMPLE_RECT_SIZE_MAX * PRT_SAMPLE_RECT_SIZE_MAX;
				++mNbTilesGenerated;
			}

			return true;
		}

		return false;
	}

	pl::EnumWorkUnitState RenderDeviceMasterTiled::process(PixelSampleTileLayers &unit)
	{
		if(mUsePixelBuffer)
		{
			unit.pixelSampler->tileDone(unit);

			mPixelBuffers[unit.samplerIndex]->addContributionTile(unit);

			mNbSamplesDone += PRT_SAMPLE_RECT_SIZE_MAX * PRT_SAMPLE_RECT_SIZE_MAX;
			++mNbTilesDone;
		}

		return pl::EnumWorkUnitState::Done;
	}

	void RenderDeviceMasterTiled::work()
	{
		RenderDeviceMasterBase<RenderDeviceWorkerTiled, PixelSampleTileLayers>::work();
	}

	void RenderDeviceMasterTiled::dumpInfo(Scene &scene, const std::filesystem::path &folder, const std::string &name)
	{
		uf32 i=0;
		for(auto &sp : mPixelSamplers)
			sp->dumpInfo(scene, folder, pl::strconcat(std::string{}, name, "pixel_sampler_", pl::io::to_string_noloc(i++), '_'));
	}

	bool RenderDeviceMasterTiled::setRenderRect(std::string_view filmID, const pl::rectangle<> &rect)
	{
		bool hasChanged = false;
		RenderDeviceMasterBase<RenderDeviceWorkerTiled, PixelSampleTileLayers>::setRenderRect(filmID, rect);

		return hasChanged;
	}
}
