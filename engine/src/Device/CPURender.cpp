/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Device/CPURender.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_DEFINE(RenderDeviceCPU)

	void RenderDeviceCPU::setRenderContext(const EntityPtr<RenderContext> &context)
	{
		if(mOtherThread)
		{
			removeReference(mOtherThread);
			entityCollection->removeEntity(*mOtherThread);
			mOtherThread = nullptr;
		}

		if(auto p = context->mRenderParams.findParam("integrator"); p && p->childParams)
		{
			const auto &intParams = *p->childParams;
			const std::string deviceId = pl::strconcat(std::string{}, '/', getEntityId(), '_', intParams["device_type"].value);

			for(const auto &deviceParam : mDeviceParams)
			{
				if(deviceParam.second.isEntity() && deviceParam.second.value.ends_with(deviceId))
				{
					addReferenceInto(mOtherThread, deviceParam.second);
					mOtherThread->setRenderContext(context);
					mOtherThread->priority(mPriority);
					mOtherThread->name(mName.empty() ? deviceId.substr(1) : pl::strconcat(std::string{}, mName, '-', deviceId.substr(1)));
					break;
				}
			}

			if(!mOtherThread)
				log(pl::logtag::error, "No device matching given type {} found for CPU device in configuration.", pl::io::quoted{intParams["device_type"].value});
		}
	}

	void RenderDeviceCPU::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		auto p = params.findParam("devices");
		mDeviceParams = p && p->childParams ? *p->childParams : ParameterTree{};
	}

	ParameterTree RenderDeviceCPU::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params["devices"] = mDeviceParams;

		return params;
	}
}
