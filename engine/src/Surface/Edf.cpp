/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Surface/Edf.hpp>
#include <PhotoRT/Core/RayTracer.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/Interaction.hpp>

namespace PhotoRT
{
	// EDF
	PRT_ENTITY_FACTORY_DEFINE(EDFDiffuse)
	PRT_ENTITY_FACTORY_DEFINE(EDFProjector)


	Color EDFBase::getColor(const SurfaceInteraction &inter, const VecD idir) const noexcept
	{
		Color color = mColorMapper ? mColorMapper->map(inter, idir) : White;

		color *= color /osshuffle<3, 3, 3, 3>;

		return pl::assert_valid(color);
	}

	EnumHemisphere EDFBase::getHemisphere(const SurfaceInteraction &inter, const VecD idir) const noexcept
	{
		const auto hemi = inter.lobe().getHemisphere(idir);

		return inter.surface().isBackfaceHit() ? swapHemisphere(hemi) : hemi;
	}

	void EDFBase::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		addParamReference(mColorMapper, params, "texture");

		params.getValueIfExistsBind
			("power", PL_LAMBDA_FORWARD_THIS(setPower))
			("emit_back", PL_LAMBDA_FORWARD_THIS(setEmitBack))
			("emit_front", PL_LAMBDA_FORWARD_THIS(setEmitFront))
		;
	}

	ParameterTree EDFBase::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		if(mColorMapper)
			params["texture"].fromEntityRef(*mColorMapper, deep);

		params.insert({
			{"power", mPower},
			{"emit_back", mEmitBack},
			{"emit_front", mEmitFront}
		});

		return params;
	}

	pl::marked_optional<EDFSample> EDFBase::sampleDir(RandomReals<2> rnd, const SurfaceInteraction &inter) const noexcept
	{
		if(!mSidePdf)
			return pl::nullopt;

		const auto [roff, side] = std::invoke([&]
			{
				if(isEmittingFront() && isEmittingBack())
					return remapRng(rnd[0], szt{2});

				if(isEmittingFront())
					return std::make_pair(rnd[0], szt{});

				return std::make_pair(rnd[0], szt{1});
			});

		const auto [z, idir] = inter.lobe().sampleCosineDir({static_cast<Real>(roff), static_cast<Real>(rnd[1])});

		EDFSample sp;
		sp.pdf = z * (std::numbers::inv_pi_v<Real> * mSidePdf);
		if(!sp.pdf)
			return pl::nullopt;

		if(side == 0)
		{
			sp.dir = idir;
			sp.hemisphere = EnumHemisphere::North;
		}
		else
		{
			sp.dir = -idir;
			sp.hemisphere = EnumHemisphere::South;
		}

		sp.color = pl::assert_valid( radiance(inter, sp.dir) );

		return sp;
	}

	OptionalPDF<Real> EDFBase::PDF(const SurfaceInteraction &inter, VecD idir) const noexcept
	{
		if(!isHemisphereValid(getHemisphere(inter, idir)))
			return pl::nullopt;

		return (idir /odot/ inter.lobe().normal /oabs) * (std::numbers::inv_pi_v<Real> * mSidePdf);
	}



	pl::marked_optional<EDFSample> EDFDiffuse::sampleDir(RandomReals<2> rnd, const SurfaceInteraction &inter) const noexcept
	{
		if(!mSidePdf)
			return pl::nullopt;

		const auto [roff, side] = std::invoke([&]
			{
				if(isEmittingFront() && isEmittingBack())
					return remapRng(rnd[0], szt{2});

				if(isEmittingFront())
					return std::make_pair(rnd[0], szt{});

				return std::make_pair(rnd[0], szt{1});
			});

		const auto [z, idir] = inter.lobe().sampleCosineDir({static_cast<Real>(roff), static_cast<Real>(rnd[1])});

		EDFSample sp;
		sp.pdf = z * (std::numbers::inv_pi_v<Real> * mSidePdf);
		if(!sp.pdf)
			return pl::nullopt;

		if(side == 0)
		{
			sp.dir = idir;
			sp.hemisphere = EnumHemisphere::North;
		}
		else
		{
			sp.dir = -idir;
			sp.hemisphere = EnumHemisphere::South;
		}

		sp.color = pl::assert_valid( getColor(inter, sp.dir) * mPower * z * std::numbers::inv_pi_v<Real> );
		if(isAlmostBlack(sp.color) || !inter.isSafeDir(sp.dir))
			return pl::nullopt;

		return sp;
	}

	Color EDFDiffuse::radiance(const SurfaceInteraction &inter, VecD idir) const noexcept
	{
		const auto color = getColor(inter, idir);

		return pl::assert_valid( isHemisphereValid(getHemisphere(inter, idir)) ? color * mPower * (idir /odot/ inter.lobe().normal /oabs) * std::numbers::inv_pi_v<Real> : Black );
	}


	pl::marked_optional<EDFSample> EDFProjector::sampleDir(RandomReals<2> rnd, const SurfaceInteraction &inter) const noexcept
	{
		if(!mSidePdf)
			return pl::nullopt;

		EDFSample sp;
		sp.pdf = mSidePdf;
		const bool both = isEmittingFront() && isEmittingBack();

		if((!both && isEmittingBack()) || (both && rnd[0] > 0.5))
		{
			sp.dir = -inter.lobe().normal;
			sp.hemisphere = EnumHemisphere::South;
		}
		else
		{
			sp.dir = inter.lobe().normal;
			sp.hemisphere = EnumHemisphere::North;
		}

		sp.color = pl::assert_valid( getColor(inter, sp.dir) * mPower );
		sp.delta = true;

		if(isAlmostBlack(sp.color) || !inter.isSafeDir(sp.dir))
			return pl::nullopt;

		return sp;
	}

	Color EDFProjector::radiance(const SurfaceInteraction &, VecD /*idir*/) const noexcept
	{
		return Black;
	}

	Color EDFProjector::radianceDiracDelta(const SurfaceInteraction &inter, EnumHemisphere hemisphere) const noexcept
	{
		return isHemisphereValid(hemisphere) ?
			getColor(inter, hemisphere == EnumHemisphere::North ? -inter.lobe().normal : inter.lobe().normal) * mPower :
			Black;
	}

	OptionalPDF<Real> EDFProjector::PDF(const SurfaceInteraction &, VecD /*idir*/) const noexcept
	{
		return pl::nullopt;
	}


}
