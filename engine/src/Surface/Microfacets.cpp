/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Surface/Microfacets.hpp>
#include <PhotoRT/Core/RayTracer.hpp>
#include <PhotoRT/Core/Distribution.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/Interaction.hpp>

namespace PhotoRT
{
	// Microfacets
	PRT_ENTITY_FACTORY_DEFINE(MicrofacetsGGX)
	PRT_ENTITY_FACTORY_DEFINE(MicrofacetsTrowbridgeReitz)


	Real MicrofacetsGGX::distribution(const SurfaceInteraction &inter, const VecD odir, const VecD /*idir*/, const VecD halfVector) const noexcept
	{
		const Real r = mParam ? mParam->map(inter, odir) : 0;

		if(r <= 0)
			return 0;

		const Real cosTheta = pl::plmin<Real>(halfVector /odot/ inter.lobe().normal, 1);

		if(cosTheta <= constants::epsilon<Real>) [[unlikely]]
			return 0;

		const Real tanTheta = cosTheta >= 1 - constants::epsilon<Real> ? 0 : (pl::plmax<Real>(0, 1 - cosTheta /opow2)) /oprtsqrt * (cosTheta /oprtreciprocal);

		return getD(r, tanTheta /opow2, cosTheta);
		//return getD(r, cosTheta);
	}

	pl::marked_optional<SampledHalfVector> MicrofacetsGGX::sampleHalfVector(RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir) const noexcept
	{
		SampledHalfVector sp;

		const Real r = mParam ? mParam->map(inter, odir) : 0;

		if(r <= 0)
		{
			sp.pdf = 1;
			sp.dir = inter.lobe().normal;
			return sp;
		}

		const Real tanTheta2 = r /opow2 * rnd[0] * pl::reciprocal(1 - rnd[0]);
		const Real cosTheta = pl::plmin<Real>(1, (tanTheta2 + 1) /orecsqrt);

		//if(rnd[0] >= (1 - constants::epsilon<Real> * 2000) || cosTheta < 0.2)
		if(cosTheta <= constants::epsilon<Real>) [[unlikely]]
		{
			sp.dir = inter.lobe().normal;
			return sp;
		}

		const Real sinTheta = (pl::plmax<Real>(0, 1 - cosTheta /opow2)) /oprtsqrt;
		const Real phi = (std::numbers::pi_v<Real> * 2) * rnd[1];

		sp.dir = inter.lobe().sphericalDirection(cosTheta, sinTheta, phi);

		if(odir /odot/ sp.dir >= -constants::epsilon<Real>)
			return pl::nullopt;

		sp.D = getD(r, tanTheta2, cosTheta);
		sp.pdf = sp.D * cosTheta;

		pl::assert_valid(sp.D, valid::Weight);
		pl::assert_valid(sp.pdf, valid::Weight);
		plassert(sp.dir /odot/ inter.lobe().normal >= 0);

		return sp;
	}

	Color MicrofacetsGGX::shadowMasking(const SurfaceInteraction &inter, const VecD dir, const VecD halfVector) const noexcept
	{
		const Real r = mParam ? mParam->map(inter, dir) : 0;
		if(r <= 0)
			return White;

		const Real dp_on = dir /odot/ inter.lobe().normal /oabs;

		if(dp_on <= constants::epsilon<Real>) [[unlikely]]
			return Black;

		const Real dp_on_rec = dp_on /oprtreciprocal;
		const Real tanTheta = (pl::plmax<Real>(0, 1 - dp_on /opow2)) /oprtsqrt * dp_on_rec;
		const Real dp_oh = dir /odot/ halfVector /oabs;

		return pl::assert_valid( pl::plmin<Real>(1, (dp_oh * dp_on_rec) * (2 * pl::reciprocal(1 + (1 + r /opow2 * (tanTheta /opow2)) /oprtsqrt))), valid::Weight );
	}

	OptionalPDF<Real> MicrofacetsGGX::PDF(const SurfaceInteraction &inter, const VecD odir, const VecD /*idir*/, const VecD halfVector) const noexcept
	{
		const Real r = mParam ? mParam->map(inter, odir) : 0;

		if(r <= 0)
			return 0;

		const Real cosTheta = pl::plmin<Real>(1, halfVector /odot/ inter.lobe().normal);

		if(cosTheta <= constants::epsilon<Real>) [[unlikely]]
			return 0;

		const Real tanTheta = cosTheta >= 1 - constants::epsilon<Real> ? 0 : (pl::plmax<Real>(0, 1 - cosTheta /opow2)) /oprtsqrt * (cosTheta /oprtreciprocal);
		const Real D = getD(r, tanTheta /opow2, cosTheta);

		return pl::assert_valid(D * cosTheta, valid::Weight);
	}


	void MicrofacetsTrowbridgeReitz::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		const Real ori = params.getValueOrDefault<Real>("orientation", 0);
		setOrientation(pl::degree_to_radian(ori));

		mParamu = nullptr;
		mParamv = nullptr;

		if(const auto p = params.findParam("roughness"); p && !p->value.empty())
		{
			if(p->isEntity())
				addReferenceInto(mParamu, *p);
			else
			{
				auto v = entityCollection->createEntity(type_v<MapperRealValue>, "surfacemap_float-value", "");
				v->mValue = pl::plclamp<Real>(*p, 0, 1);
				mParamu = v;
			}
		}
		else
		{
			if(const auto p = params.findParam("roughnessu"); p && !p->value.empty())
			{
				if(p->isEntity())
					addReferenceInto(mParamu, *p);
				else
				{
					auto v = entityCollection->createEntity(type_v<MapperRealValue>, "surfacemap_float-value", "");
					v->mValue = pl::plclamp<Real>(*p, 0, 1);
					mParamu = v;
				}
			}

			if(const auto p = params.findParam("roughnessv"); p && !p->value.empty())
			{
				if(p->isEntity())
					addReferenceInto(mParamv, *p);
				else
				{
					auto v = entityCollection->createEntity(type_v<MapperRealValue>, "surfacemap_float-value", "");
					v->mValue = pl::plclamp<Real>(*p, 0, 1);
					mParamv = v;
				}
			}
		}
	}

	ParameterTree MicrofacetsTrowbridgeReitz::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params["orientation"] = pl::radian_to_degree(mOrientation);

		if(mParamu)
			params["roughnessu"].fromEntityRef(*mParamu, deep);
		if(mParamv)
			params["roughnessv"].fromEntityRef(*mParamv, deep);

		return params;
	}

	Real MicrofacetsTrowbridgeReitz::distribution(const SurfaceInteraction &inter, const VecD odir, const VecD /*idir*/, const VecD halfVector) const noexcept
	{
		const Real ru = mParamu ? paramToAlpha(mParamu->map(inter, odir)) : 0;
		const Real rv = mParamv ? paramToAlpha(mParamv->map(inter, odir)) : ru;

		if(ru <= 0 && rv <= 0)
			return 0;

		const Real cosTheta = pl::plmin<Real>(1, halfVector /odot/ inter.lobe().normal);
		if(cosTheta <= constants::epsilon<Real>) [[unlikely]]
			return 0;

		const Real sinTheta = pl::plmax<Real>(0, 1 - cosTheta /opow2) /oprtsqrt;
		const Real tanTheta = sinTheta * (cosTheta /oprtreciprocal);

		Lobe<> rotated{inter.lobe()};
		rotated.rotateOnNormal(mRotCos, mRotSin);

		const Real sinThetaR = sinTheta /oprtreciprocal;

		const Real cosPhi = !sinTheta ? 1 : pl::plclamp<Real>(halfVector /odot/ rotated.lobeX * sinThetaR, -1, 1);
		const Real sinPhi = !sinTheta ? 0 : pl::plclamp<Real>(halfVector /odot/ rotated.lobeY * sinThetaR, -1, 1);

		return getD(ru, rv, cosPhi, sinPhi, tanTheta /opow2, cosTheta);
	}

	pl::marked_optional<SampledHalfVector> MicrofacetsTrowbridgeReitz::sampleHalfVector(RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir) const noexcept
	{
		SampledHalfVector sp;

		const Real ru = mParamu ? paramToAlpha(mParamu->map(inter, odir)) : 0;
		const Real rv = mParamv ? paramToAlpha(mParamv->map(inter, odir)) : ru;

		if(ru <= 0 && rv <= 0)
		{
			sp.pdf = 1;
			sp.dir = inter.lobe().normal;
			return sp;
		}

		Real tanTheta2;
		Real phi;
		pl::sincos_res<Real> scphi;

		if(ru == rv)
		{
			tanTheta2 = ru /opow2 * rnd[0] * pl::reciprocal(1 - rnd[0]);
			phi = (2 * std::numbers::pi_v<Real>) * rnd[1];
			scphi = pl::sincos(phi);
		}
		else
		{
			phi = ru ? std::atan(rv * (ru /oprtreciprocal) * std::tan(2 * std::numbers::pi_v<Real> * rnd[1] + std::numbers::pi_v<Real> * 0.5)) : 0;
			if(rnd[1] > 0.5)
				phi += std::numbers::pi_v<Real>;

			scphi = pl::sincos(phi);
			Real rtotal = ( (ru ? scphi.c /opow2 * (ru /opow2 /oprtreciprocal) : 0) + (rv ? scphi.s /opow2 * (rv /opow2 /oprtreciprocal) : 0)) /oprtreciprocal;
			tanTheta2 = rtotal * rnd[0] * pl::reciprocal(1 - rnd[0]);
		}

		const Real cosTheta = pl::plmin<Real>(1, (tanTheta2 + 1) /orecsqrt);

		//if(rnd[0] >= (1 - constants::epsilon<Real> * 2000) || cosTheta <= 0.2)
		if(cosTheta <= constants::epsilon<Real>) [[unlikely]]
		{
			sp.dir = inter.lobe().normal;
			return sp;
		}

		const Real sinTheta = pl::plmax<Real>(0, 1 - cosTheta /opow2) /oprtsqrt;

		sp.dir = inter.lobe().sphericalDirection(cosTheta, sinTheta, phi + mOrientation);

		if(odir /odot/ sp.dir >= -constants::epsilon<Real>)
			return pl::nullopt;

		sp.D = getD(ru, rv, scphi.c, scphi.s, tanTheta2, cosTheta);
		sp.pdf = sp.D * cosTheta;

		pl::assert_valid(sp.D, valid::Weight);
		pl::assert_valid(sp.pdf, valid::Weight);
		pl::assert_valid(cosTheta, valid::Weight);
		pl::assert_valid(sinTheta, valid::Weight);
		pl::assert_valid(tanTheta2, valid::Weight);

		return sp;
	}

	Color MicrofacetsTrowbridgeReitz::shadowMasking(const SurfaceInteraction &inter, const VecD dir, const VecD /*halfVector*/) const noexcept
	{
		const Real ru = mParamu ? paramToAlpha(mParamu->map(inter, dir)) : 0;
		const Real rv = mParamv ? paramToAlpha(mParamv->map(inter, dir)) : ru;

		if(ru <= 0 && rv <= 0)
			return White;

		const Real dp_on = dir /odot/ inter.lobe().normal /oabs;

		if(dp_on <= constants::epsilon<Real>) [[unlikely]]
			return Black;

		const Real sinTheta = pl::plmax<Real>(0, 1 - dp_on /opow2) /oprtsqrt;
		const Real tanTheta = sinTheta * (dp_on /oprtreciprocal);

		Lobe<> rotated{inter.lobe()};
		rotated.rotateOnNormal(mRotCos, mRotSin);

		const Real sinThetaR = sinTheta /oprtreciprocal;

		const Real cosPhi = !sinTheta ? 1 : pl::plclamp<Real>(dir /odot/ rotated.lobeX * sinThetaR, -1, 1);
		const Real sinPhi = !sinTheta ? 0 : pl::plclamp<Real>(dir /odot/ rotated.lobeY * sinThetaR, -1, 1);

		const Real rtotal = prtsqrt( (ru ? cosPhi /opow2 * (ru /opow2) : 0) + (rv ? sinPhi /opow2 * (rv /opow2) : 0) );
		const Real res = pl::plmax<Real>(0, ((rtotal * tanTheta) /opow2 + 1) /oprtsqrt - 1) * 0.5;

		return pl::assert_valid(res, valid::Weight);
	}

	OptionalPDF<Real> MicrofacetsTrowbridgeReitz::PDF(const SurfaceInteraction &inter, const VecD odir, const VecD /*idir*/, const VecD halfVector) const noexcept
	{
		const Real ru = mParamu ? paramToAlpha(mParamu->map(inter, odir)) : 0;
		const Real rv = mParamv ? paramToAlpha(mParamv->map(inter, odir)) : ru;

		if(ru <= 0 && rv <= 0)
			return 0;

		const Real cosTheta = pl::plmin<Real>(1, halfVector /odot/ inter.lobe().normal);

		if(cosTheta <= constants::epsilon<Real>) [[unlikely]]
			return 0;

		const Real sinTheta = pl::plmax<Real>(0, 1 - cosTheta /opow2) /oprtsqrt;
		const Real tanTheta = sinTheta * (cosTheta /oprtreciprocal);

		Lobe<> rotated{inter.lobe()};
		rotated.rotateOnNormal(mRotCos, mRotSin);

		const Real sinThetaR = sinTheta /oprtreciprocal;

		const Real cosPhi = !sinTheta ? 1 : pl::plclamp<Real>(halfVector /odot/ rotated.lobeX * sinThetaR, -1, 1);
		const Real sinPhi = !sinTheta ? 0 : pl::plclamp<Real>(halfVector /odot/ rotated.lobeY * sinThetaR, -1, 1);

		const Real D = getD(ru, rv, cosPhi, sinPhi, tanTheta /opow2, cosTheta);

		return D * cosTheta;
	}

}
