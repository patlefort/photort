/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Surface/Reflection.hpp>
#include <PhotoRT/Core/Material.hpp>
#include <PhotoRT/Core/RayTracer.hpp>
#include <PhotoRT/Core/Distribution.hpp>
#include <PhotoRT/Core/Normals.hpp>
#include <PhotoRT/Core/DistributionSampling.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/Interaction.hpp>

namespace PhotoRT
{
	// BxDF
	PRT_ENTITY_FACTORY_DEFINE(BxDFVoid)
	PRT_ENTITY_FACTORY_DEFINE(BxDFLambertian)
	PRT_ENTITY_FACTORY_DEFINE(BxDFOrenNayar)
	PRT_ENTITY_FACTORY_DEFINE(BxDFInvert)
	PRT_ENTITY_FACTORY_DEFINE(BxDFNormalMap)
	PRT_ENTITY_FACTORY_DEFINE(BxDFMulti)
	PRT_ENTITY_FACTORY_DEFINE(BxDFAlphaBlendMulti)
	PRT_ENTITY_FACTORY_DEFINE(BxDFSpecularReflection)
	PRT_ENTITY_FACTORY_DEFINE(BxDFSpecularTransmission)
	PRT_ENTITY_FACTORY_DEFINE(BxDFSpecularCombined)
	PRT_ENTITY_FACTORY_DEFINE(BxDFAlphaBlend)
	PRT_ENTITY_FACTORY_DEFINE(BxDFTabulated)


	pl::marked_optional<SampledBxDF> BxDFLambertian::sampleDir(EnumTransportMode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &/*rng*/, EnumRayType typeMask) const noexcept
	{
		if(!isType(*this, typeMask) || !isHemisphereValid(inter.lobe().getHemisphere(-odir))) [[unlikely]]
			return SampledBxDF::NullOpaque();

		const auto [z, idir] = inter.lobe().sampleCosineDir(static_cast<pl::values<Real, 2>>(rnd));
		const auto color = getColor(inter, odir);

		SampledBxDF sp{
				.dir = idir,
				.color = color,
				.albedo = getColorAlbedo(color.color),
				.pdf = pl::assert_valid( z * std::numbers::inv_pi_v<Real> ),
				.type = EnumRayType::Diffuse,
				.hemisphere = EnumHemisphere::North
			};

		sp.color.color *= sp.pdf;

		return sp;
	}

	BxDFRadianceResult BxDFLambertian::radiance(EnumTransportMode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		const auto color = getColor(inter, odir);
		const auto dp = idir /odot/ inter.lobe().normal;

		return {{dp > 0 ? color.color * (std::numbers::inv_pi_v<Real> * dp) : Black, color.alpha}, getColorAlbedo(color.color)};
	}

	OptionalPDF<Real> BxDFLambertian::PDF(EnumTransportMode, const SurfaceInteraction &inter, const VecD /*odir*/, const VecD idir) const noexcept
	{
		const auto dp = idir /odot/ inter.lobe().normal;

		return dp > 0 ? dp * std::numbers::inv_pi_v<Real> : 0;
	}

	pl::marked_optional<SampledBxDF> BxDFOrenNayar::sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask) const noexcept
	{
		auto sp = BxDFLambertian::sampleDir(mode, rnd, inter, odir, rng, typeMask);

		if(sp && !isAlmostBlack(sp->color.color) && !isAlmostBlack(sp->color.alpha))
			sp->color.color *= compute(inter, odir, sp->dir);

		return sp;
	}

	BxDFRadianceResult BxDFOrenNayar::radiance(EnumTransportMode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		const auto color = getColor(inter, odir);

		auto dp = idir /odot/ inter.lobe().normal;

		return {{dp > 0 ? color.color * (std::numbers::inv_pi_v<Real> * compute(inter, odir, idir) * dp) : Black, color.alpha}, getColorAlbedo(color.color)};
	}

	Real BxDFOrenNayar::compute(const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		const auto lod = -inter.lobe().toLocal(odir);
		const auto lid = inter.lobe().toLocal(idir);
		const auto &lodir = pl::simd::vector_access(lod);
		const auto &lidir = pl::simd::vector_access(lid);

		const Real dp_on = lodir[size_v<2>] /oabs;
		const Real dp_in = lidir[size_v<2>] /oabs;

		if(dp_on < constants::epsilon<Real> || dp_in < constants::epsilon<Real>) [[unlikely]]
			return 0;

		const Real v = mSigma ? mSigma->map(inter, odir) : 0;

		const auto [A, B] = getAB(v);

		const Real sinThetaI = pl::plmax<Real>(0, 1 - dp_in /opow2) /oprtsqrt;
		const Real sinThetaO = pl::plmax<Real>(0, 1 - dp_on /opow2) /oprtsqrt;
		Real maxCos = 0;

		[[likely]] if(sinThetaO > constants::epsilon<Real> && sinThetaI > constants::epsilon<Real>)
		{
			const Real
				stirec = !sinThetaI ? 0 : sinThetaI /oprtreciprocal,
				storec = !sinThetaO ? 0 : sinThetaO /oprtreciprocal,
				cosPhiI = pl::plclamp<Real>(lidir[size_v<0>] * stirec, -1, 1),
				sinPhiI = pl::plclamp<Real>(lidir[size_v<1>] * stirec, -1, 1),
				cosPhiO = pl::plclamp<Real>(lodir[size_v<0>] * storec, -1, 1),
				sinPhiO = pl::plclamp<Real>(lodir[size_v<1>] * storec, -1, 1);

			maxCos = pl::plmax<Real>(0, cosPhiI * cosPhiO + sinPhiI * sinPhiO);
		}

		Real sinAlpha, tanBeta;
		if(dp_in > dp_on)
		{
			sinAlpha = sinThetaO;
			tanBeta = sinThetaI * (dp_in /oprtreciprocal);
		}
		else
		{
			sinAlpha = sinThetaI;
			tanBeta = sinThetaO * (dp_on /oprtreciprocal);
		}

		return A + B * maxCos * sinAlpha * tanBeta;
	}

	void BxDFOrenNayar::setParameters(const ParameterTree &params)
	{
		BxDFLambertian::setParameters(params);

		if(const auto p = params.findParam("roughness"); p && !p->value.empty())
		{
			if(p->isEntity())
				addReferenceInto(mSigma, *p);
			else
			{
				auto v = entityCollection->createEntity(type_v<MapperRealValue>, "surfacemap_float-value", "");
				v->mValue = pl::plclamp<Real>(*p, 0, 1);
				mSigma = v;
			}
		}
		else
			mSigma = nullptr;

	}

	ParameterTree BxDFOrenNayar::getParameters(bool deep) const
	{
		ParameterTree params {BxDFLambertian::getParameters(deep)};

		if(mSigma)
			params["roughness"].fromEntityRef(*mSigma, deep);

		return params;
	}


	void BxDFNormalMap::setParameters(const ParameterTree &params)
	{
		BxDFChainBase::setParameters(params);

		addParamReference(mNormalMapper, params, "normals");
	}

	ParameterTree BxDFNormalMap::getParameters(bool deep) const
	{
		ParameterTree params {BxDFChainBase::getParameters(deep)};

		if(mNormalMapper)
			params["normals"].fromEntityRef(*mNormalMapper, deep);

		return params;
	}

	void BxDFMulti::setParameters(const ParameterTree &params)
	{
		BxDFBase::setParameters(params);

		clearBxDFs();

		if(const auto p = params.findParam("bxdfs"); p && p->childParams)
			for(const auto &param : *p->childParams)
			{
				if(const auto p = param.second.findParam("bxdf"); p)
					addBxDF(addReference(type_v<const IBxDF>, *p), param.second.getValueOrDefault<Real>("power", 1));
			}
	}

	ParameterTree BxDFMulti::getParameters(bool deep) const
	{
		ParameterTree params {BxDFBase::getParameters(deep)};

		for(auto&& [i, bxdf] : rgv::enumerate(mBxdfs))
		{
			auto &p = params["bxdfs"][i];
			p["bxdf"].fromEntityRef(*bxdf.bxdf, deep);
			p["power"] = bxdf.power;
		}

		return params;
	}

	void BxDFMulti::addBxDF(EntityPtr<const IBxDF> bxdf, Real power)
	{
		if(!bxdf)
			return;

		const auto btype = bxdf->getType();
		const bool dd = bxdf->isDiracDelta();
		const bool anis = bxdf->isAnisotropic();
		mBxdfs.push_back({std::move(bxdf), btype, power, dd, anis});

		mInvSamplersCount = 1.0 / mBxdfs.size();

		mType = mType | btype;
		mDiracDelta &= dd;
		mAnisotropic |= anis;
	}

	void BxDFMulti::clearBxDFs() noexcept
	{
		mBxdfs.clear();
		mBxdfs.shrink_to_fit();
		mType = EnumRayType::Empty;
		mDiracDelta = true;
		mAnisotropic = false;
	}

	bool BxDFMulti::isHemisphereValid(EnumHemisphere hemisphere) const noexcept
	{
		return rg::any_of(mBxdfs, [&](const auto &b){
			return b.bxdf->isHemisphereValid(hemisphere);
		});
	}

	pl::marked_optional<SampledBxDF> BxDFMulti::sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask) const noexcept
	{
		if(!isType(*this, typeMask)) [[unlikely]]
			return SampledBxDF::NullOpaque();

		const auto thisColor = getColor(inter, odir);

		const auto nbValid = getNbMasked(typeMask);
		if(!nbValid) [[unlikely]]
			return SampledBxDF::NullTransparent(thisColor.alpha);

		const auto [remapped0, remappedIndex] = remapRng(rnd[0], nbValid);
		const auto chosenBxDF = pickBxDF(remappedIndex, nbValid, typeMask);
		rnd = {remapped0, rnd[1]};

		if(isAlmostBlack(thisColor.color) || isAlmostBlack(thisColor.alpha))
			return SampledBxDF::NullTransparent(thisColor.alpha);

		const auto &chosenEntry = mBxdfs[chosenBxDF];
		auto chosenSp = chosenEntry.bxdf->sampleDir(mode, rnd, inter, odir, rng, typeMask);

		if(!chosenSp)
			return SampledBxDF::NullTransparent(thisColor.alpha);

		Color bxdfAlphaTotal{};

		if(bool(chosenSp->type & EnumRayType::Specular))
		{
			for(const auto &bEntry : mBxdfs)
			{
				if(&bEntry != &chosenEntry)
				{
					bxdfAlphaTotal += bEntry.bxdf->alpha(mode, inter, odir, chosenSp->dir) * bEntry.power;
				}
				else
				{
					Color a = chosenSp->color.alpha * bEntry.power;
					bxdfAlphaTotal += a;
				}
			}

			chosenSp->color.color *= chosenEntry.power;
			chosenSp->albedo *= chosenEntry.power;
		}
		else
		{
			Color colorTotal{};
			CReal albedo = 0;

			for(const auto &bEntry : mBxdfs)
			{
				if(&bEntry != &chosenEntry && !bool(bEntry.type & EnumRayType::Specular))
				{
					auto [c, alb] = bEntry.bxdf->radiance(mode, inter, odir, chosenSp->dir);
					c.alpha *= bEntry.power;

					if(bool(bEntry.type & typeMask))
					{
						chosenSp->pdf += bEntry.bxdf->PDF(mode, inter, odir, chosenSp->dir).value();
						colorTotal += c.color * bEntry.power;
						albedo += alb * bEntry.power;
					}

					bxdfAlphaTotal += c.alpha;
				}
				else
				{
					const Color a = chosenSp->color.alpha * bEntry.power;
					bxdfAlphaTotal += a;
					colorTotal += chosenSp->color.color * bEntry.power;
					albedo += chosenSp->albedo * bEntry.power;
				}
			}

			chosenSp->color.color = colorTotal;
			chosenSp->albedo = albedo;
		}

		pl::assert_valid( chosenSp->color.color *= thisColor.color );
		chosenSp->albedo *= getColorAlbedo(thisColor.color);
		chosenSp->color.alpha = pl::assert_valid( clampColor(bxdfAlphaTotal) * thisColor.alpha );

		pl::assert_valid( chosenSp->pdf /= nbValid, valid::Weight );

		return chosenSp;
	}

	BxDFRadianceResult BxDFMulti::radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		const auto bcolor = getColor(inter, odir);

		if(isAlmostBlack(bcolor.color) || isAlmostBlack(bcolor.alpha))
			return {bcolor, 0};

		Color surfaceRadiance{}, alpha{};
		CReal albedo = 0;

		for(const auto &b : mBxdfs)
		{
			if(!bool(b.type & EnumRayType::Specular))
			{
				const auto [color, alb] = b.bxdf->radiance(mode, inter, odir, idir);
				surfaceRadiance += color.color * b.power;
				alpha += color.alpha * b.power;
				albedo += alb * b.power;
			}
		}

		surfaceRadiance *= bcolor.color;
		alpha = clampColor(alpha) * bcolor.alpha;
		albedo *= getColorAlbedo(bcolor.color);

		return {{pl::assert_valid(surfaceRadiance), pl::assert_valid(alpha)}, albedo};
	}

	Color BxDFMulti::alpha(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		Color balpha = getColorAlpha(inter, odir);

		if(isAlmostBlack(balpha))
			return Black;

		const auto alphaTotal = rg::accumulate(mBxdfs, Color{}, rg::plus{},
			[&](const auto &b){ return b.bxdf->alpha(mode, inter, odir, idir) * b.power; });

		return clampColor(pl::assert_valid(alphaTotal)) * balpha;
	}

	OptionalPDF<Real> BxDFMulti::PDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		const auto pdfTotal = rg::accumulate(mBxdfs, Real{}, rg::plus{},
			[&](const auto &b){ return b.bxdf->PDF(mode, inter, odir, idir).value(); });

		return pl::assert_valid(pdfTotal * mInvSamplersCount, valid::Weight);
	}



	pl::marked_optional<SampledBxDF> BxDFAlphaBlendMulti::sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask) const noexcept
	{
		if(!isType(*this, typeMask)) [[unlikely]]
			return SampledBxDF::NullOpaque();

		const auto thisColor = getColor(inter, odir);

		const auto nbValid = getNbMasked(typeMask);
		if(!nbValid) [[unlikely]]
			return SampledBxDF::NullTransparent(thisColor.alpha);

		const auto [remapped0, remappedIndex] = remapRng(rnd[0], nbValid);
		const auto chosenBxDF = pickBxDF(remappedIndex, nbValid, typeMask);
		rnd = {remapped0, rnd[1]};

		if(isAlmostBlack(thisColor.color) || isAlmostBlack(thisColor.alpha))
			return SampledBxDF::NullTransparent(thisColor.alpha);

		const auto &chosenEntry = mBxdfs[chosenBxDF];
		auto chosenSp = chosenEntry.bxdf->sampleDir(mode, rnd, inter, odir, rng, typeMask);

		if(!chosenSp)
			return SampledBxDF::NullTransparent(thisColor.alpha);

		Color bxdfAlphaTotal{};

		if(bool(chosenSp->type & EnumRayType::Specular))
		{
			for(const auto &bEntry : mBxdfs)
			{
				if(&bEntry != &chosenEntry)
					bxdfAlphaTotal += (White - bxdfAlphaTotal) * bEntry.bxdf->alpha(mode, inter, odir, chosenSp->dir) * bEntry.power;
				else
				{
					const Color a = (White - bxdfAlphaTotal) * chosenSp->color.alpha * bEntry.power;
					bxdfAlphaTotal += a;
					chosenSp->color.color *= a;
					chosenSp->albedo *= getColorAlbedo(a);
				}
			}
		}
		else
		{
			Color colorTotal{};
			CReal albedo = 0;

			for(const auto &bEntry : mBxdfs)
			{
				if(&bEntry != &chosenEntry && !bool(bEntry.type & EnumRayType::Specular))
				{
					auto [c, alb] = bEntry.bxdf->radiance(mode, inter, odir, chosenSp->dir);

					if(bool(bEntry.type & typeMask))
					{
						pl::assert_valid(c.color);

						chosenSp->pdf += bEntry.bxdf->PDF(mode, inter, odir, chosenSp->dir).value();
						colorTotal += c.color * c.alpha;
						albedo += alb * getColorAlbedo(c.alpha);
					}

					c.alpha = (White - bxdfAlphaTotal) * c.alpha * bEntry.power;
					bxdfAlphaTotal += c.alpha;
				}
				else
				{
					const Color a = (White - bxdfAlphaTotal) * chosenSp->color.alpha * bEntry.power;
					bxdfAlphaTotal += a;
					colorTotal += chosenSp->color.color * a;
					albedo += chosenSp->albedo * getColorAlbedo(a);
				}
			}

			chosenSp->color.color = colorTotal;
			chosenSp->albedo = albedo;
		}

		pl::assert_valid( chosenSp->color.color *= thisColor.color );
		chosenSp->albedo *= getColorAlbedo(thisColor.color);
		chosenSp->color.alpha = pl::assert_valid( clampColor(bxdfAlphaTotal) * thisColor.alpha );

		pl::assert_valid( chosenSp->pdf /= nbValid, valid::Weight );

		return chosenSp;
	}

	BxDFRadianceResult BxDFAlphaBlendMulti::radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		const auto bcolor = getColor(inter, odir);

		if(isAlmostBlack(bcolor.color) || isAlmostBlack(bcolor.alpha))
			return {bcolor, 0};

		Color surfaceRadiance{}, alpha{};
		CReal albedo = 0;

		for(const auto &b : mBxdfs)
		{
			auto [color, alb] = b.bxdf->radiance(mode, inter, odir, idir);
			color.alpha = (White - alpha) * color.alpha * b.power;

			surfaceRadiance += color.color * color.alpha;
			albedo += alb * getColorAlbedo(color.alpha);

			alpha += color.alpha;
		}

		surfaceRadiance *= bcolor.color;
		albedo *= getColorAlbedo(bcolor.color);
		alpha = clampColor(alpha) * bcolor.alpha;

		return {{pl::assert_valid(surfaceRadiance), pl::assert_valid(alpha)}, albedo};
	}

	Color BxDFAlphaBlendMulti::alpha(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		const Color balpha = getColorAlpha(inter, odir);

		if(isAlmostBlack(balpha))
			return Black;

		const auto alpha = rg::accumulate(mBxdfs, Color{},
			[&](const auto &a, const auto &b){ return a + ((White - a) * b.bxdf->alpha(mode, inter, odir, idir) * b.power); });

		return clampColor(pl::assert_valid(alpha)) * balpha;
	}



	void BxDFSpecularBase::setParameters(const ParameterTree &params)
	{
		BxDFBase::setParameters(params);

		setFresnel(addParamReference(type_v<const IFresnel>, params, "fresnel"));
		setMicrofacet(addParamReference(type_v<const IMicrofacetsDistribution>, params, "microfacets"));
		params.getValueIfExistsBind("dual_layer", PL_LAMBDA_FORWARD_THIS(setDualLayer));
	}

	ParameterTree BxDFSpecularBase::getParameters(bool deep) const
	{
		ParameterTree params {BxDFBase::getParameters(deep)};

		if(mMicrofacets)
			params["microfacets"].fromEntityRef(*mMicrofacets, deep);
		if(mFresnel)
			params["fresnel"].fromEntityRef(*mFresnel, deep);

		params["dual_layer"] = mDualLayer;

		return params;
	}

	VecD BxDFSpecularBase::getHalfVector(const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		VecD hv;

		if(inter.lobe().getHemisphere(idir) == EnumHemisphere::North)
		{
			hv = (idir - odir) /oprtnormalize;
		}
		else
		{
			const Real refractionIndexExiting = inter.enteredSub->getRefractionIndex();
			const Real refractionIndexExitingInv = inter.enteredSub->getRefractionIndexInvert();
			const Real refractionIndexEntering = inter.nextSubstance->getRefractionIndex();

			if(refractionIndexExiting == refractionIndexEntering)
				return inter.lobe().normal;

			const Real refractionIndexRatio = refractionIndexExitingInv * refractionIndexEntering;

			hv = (idir * refractionIndexRatio - odir) /oprtnormalize;

			const Real dp = hv /odot/ inter.lobe().normal;

			if(dp < 0)
				hv = -hv;
		}

		return pl::assert_valid(hv);
	}


	void BxDFSpecularReflection::setParameters(const ParameterTree &params)
	{
		BxDFSpecularBase::setParameters(params);

		params.getValueIfExistsBind("fresnel_as_alpha", PL_LAMBDA_FORWARD_THIS(setFresnelAsAlpha));
	}

	ParameterTree BxDFSpecularReflection::getParameters(bool deep) const
	{
		ParameterTree params {BxDFSpecularBase::getParameters(deep)};

		params["fresnel_as_alpha"] = mFresnelAsAlpha;

		return params;
	}

	Color BxDFSpecularReflection::specRadiance(EnumTransportMode, const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD &hv, bool isMicrofacet, Real dp_on, Real D) const noexcept
	{
		Color color{pl::tag::one};

		if(isMicrofacet)
		{
			const auto G = mMicrofacets->shadowMasking2(inter, odir, idir, hv);

			pl::assert_valid( color *= G * D * pl::reciprocal(4 * (dp_on /oabs)) );
		}

		return color;
	}

	Real BxDFSpecularReflection::specPDF(EnumTransportMode, const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD &hv) const noexcept
	{
		Real dp = odir /odot/ hv /oabs;
		if(dp <= constants::epsilon<Real>) [[unlikely]]
			return 0;

		if(!mMicrofacets)
			return 1;

		return pl::assert_valid( mMicrofacets->PDF(inter, odir, idir, hv).value() * pl::reciprocal(4 * dp), valid::Weight );
	}

	OptionalPDF<Real> BxDFSpecularReflection::PDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		if(!mMicrofacets || idir /odot/ inter.lobe().normal <= constants::epsilon<Real> || odir /odot/ inter.lobe().normal >= -constants::epsilon<Real>) [[unlikely]]
			return pl::nullopt;

		const auto hv = getHalfVector(inter, odir, idir);

		return specPDF(mode, inter, odir, idir, hv);
	}

	BxDFRadianceResult BxDFSpecularReflection::radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		const bool isMicrofacet = mMicrofacets != nullptr;

		if(!isMicrofacet && !mFresnelAsAlpha) [[unlikely]]
			return {{Black, getColorAlpha(inter, odir)}, 0};

		const Real dp_on = odir /odot/ inter.lobe().normal;

		if(idir /odot/ inter.lobe().normal <= constants::epsilon<Real> || dp_on >= -constants::epsilon<Real>) [[unlikely]]
			return {{Black, getColorAlpha(inter, odir)}, 0};

		const auto hv = getHalfVector(inter, odir, idir);

		const Real dp_oh = odir /odot/ hv;
		const Real dp_ih = idir /odot/ hv;
		if(dp_ih <= constants::epsilon<Real> || dp_oh >= -constants::epsilon<Real>) [[unlikely]]
			return {{Black, getColorAlpha(inter, odir)}, 0};

		const Real D = isMicrofacet ? (*mMicrofacets)(inter, odir, idir, hv) : 0;
		auto color = getColor(inter, odir);
		auto F = mFresnel ? (*mFresnel)(mode, hv, odir, inter) : White;

		if(mFresnelAsAlpha)
		{
			Color FT;

			if(mDualLayer)
			{
				FT = dualLayerRefr(F);
				F += F * FT;
			}
			else
				FT = White - F;

			color.alpha = White - (FT * color.alpha);
			F *= pl::select(color.alpha, White, color.alpha > 0) /oprtreciprocal;

			pl::assert_valid(F);
		}

		color.color *= F;
		const auto albedo = getColorAlbedo(color.color);
		color.color *= !isMicrofacet ? Black : specRadiance(mode, inter, odir, idir, hv, isMicrofacet, dp_on, D);

		return {pl::assert_valid(color), albedo};
	}

	Color BxDFSpecularReflection::alpha(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD /*idir*/) const noexcept
	{
		auto alpha = getColorAlpha(inter, odir);

		if(mMicrofacets || !mFresnelAsAlpha)
			return alpha;

		const Real dp_on = odir /odot/ inter.lobe().normal;
		if(dp_on >= -constants::epsilon<Real>) [[unlikely]]
			return alpha;

		if(mFresnel)
		{
			const auto F = (*mFresnel)(mode, inter.lobe().normal, odir, inter);
			Color FT;

			if(mDualLayer)
				FT = dualLayerRefr(F);
			else
				FT = White - F;

			alpha = White - (FT * alpha);
		}

		return alpha;
	}

	pl::marked_optional<SampledBxDF> BxDFSpecularReflection::sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &, EnumRayType typeMask) const noexcept
	{
		const Real dp_on = odir /odot/ inter.lobe().normal;

		if(!isType(*this, typeMask) || dp_on >= -constants::epsilon<Real>) [[unlikely]]
			return SampledBxDF::NullOpaque();

		SampledBxDF sp;
		SampledHalfVector hv;

		bool isMicrofacet = mMicrofacets != nullptr;

		sp.color = getColor(inter, odir);

		if(isMicrofacet)
		{
			const auto shv = mMicrofacets->sampleHalfVector(rnd, inter, odir);
			if(!shv)
				return sp;

			hv = *shv;
			sp.pdf = hv.pdf;

			isMicrofacet = pl::assert_valid(sp.pdf, valid::Weight) != 1;
		}
		else
		{
			hv.D = 0;
			hv.dir = inter.lobe().normal;
			sp.pdf = 1;
		}

		if(isMicrofacet)
		{
			const Real dp_oh = -(odir /odot/ hv.dir);
			if(dp_oh <= constants::epsilon<Real>)
			{
				sp.pdf = 0;
				return sp;
			}

			sp.pdf *= (4 * dp_oh) /oprtreciprocal;
			sp.type = EnumRayType::SpecularGlossy;
		}
		else
		{
			sp.type = EnumRayType::Specular;
		}

		auto F = mFresnel ? (*mFresnel)(mode, hv.dir, odir, inter) : White;

		sp.dir = PhotoRT::reflect(odir, hv.dir);

		if(sp.dir /odot/ inter.lobe().normal <= constants::epsilon<Real>) [[unlikely]]
		{
			sp.pdf = 0;
			return sp;
		}

		if(mFresnelAsAlpha)
		{
			Color FT;

			if(mDualLayer)
			{
				FT = dualLayerRefr(F);
				F += F * FT;
			}
			else
				FT = White - F;

			sp.color.alpha = White - (FT * sp.color.alpha);
			pl::assert_valid( F *= pl::select(sp.color.alpha, White, sp.color.alpha > 0) /oprtreciprocal );
		}

		sp.color.color *= F;
		sp.albedo = getColorAlbedo(sp.color.color);
		sp.color.color *= specRadiance(mode, inter, odir, sp.dir, hv.dir, isMicrofacet, dp_on, hv.D);
		sp.hemisphere = EnumHemisphere::North;

		pl::assert_valid(sp.color);

		return sp;
	}


	Color BxDFSpecularTransmission::specRadiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD &hv, bool isMicrofacet, Real dp_on, Real D, Real *pdf) const noexcept
	{
		Color color{pl::tag::one};

		if(isMicrofacet)
		{
			const Real dp_ih = idir /odot/ hv;

			if(dp_ih >= -constants::epsilon<Real>) [[unlikely]]
			{
				if(pdf) *pdf = 0;
				return Black;
			}

			const Real dp_oh = -(odir /odot/ hv);

			Real refractionIndexExiting, refractionIndexEnteringInv;

			if(dp_on < 0)
			{
				refractionIndexExiting = inter.nextSubstance->getRefractionIndex();
				refractionIndexEnteringInv = inter.enteredSub->getRefractionIndexInvert();
			}
			else
			{
				refractionIndexExiting = inter.enteredSub->getRefractionIndex();
				refractionIndexEnteringInv = inter.nextSubstance->getRefractionIndexInvert();
			}

			const Real iorEta = refractionIndexExiting * refractionIndexEnteringInv;
			const Real term = (dp_oh + iorEta * dp_ih) /opow2;

			const Real dpterm = -dp_on * term;
			if(dpterm <= 0 || term <= 0) [[unlikely]]
			{
				if(pdf) *pdf = 0;
				return Black;
			}

			const auto G = mMicrofacets->shadowMasking2(inter, odir, idir, hv);

			pl::assert_valid( color *= G * (D * pl::plabs(dp_ih * dp_oh * (mode == EnumTransportMode::Radiance ? 1 : iorEta /opow2) * (dpterm /oprtreciprocal))) );

			if(pdf)
				*pdf = iorEta /opow2 * (dp_ih /oabs) * (term /oprtreciprocal);
		}
		else
		{
			if(mode == EnumTransportMode::Radiance)
			{
				Real refractionIndexExiting, refractionIndexEnteringInv;

				if(dp_on < 0)
				{
					refractionIndexExiting = inter.nextSubstance->getRefractionIndex();
					refractionIndexEnteringInv = inter.enteredSub->getRefractionIndexInvert();
				}
				else
				{
					refractionIndexExiting = inter.enteredSub->getRefractionIndex();
					refractionIndexEnteringInv = inter.nextSubstance->getRefractionIndexInvert();
				}

				color *= (refractionIndexExiting * refractionIndexEnteringInv) /opow2;
			}

			if(pdf) *pdf = 1;
		}

		return color;
	}

	Real BxDFSpecularTransmission::specPDF(EnumTransportMode, const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD &hv) const noexcept
	{
		const Real dp_ih = idir /odot/ hv;
		Real dp_oh = -(odir /odot/ hv);
		Real iorEntering, iorExiting;

		if(dp_oh < 0)
		{
			dp_oh = -dp_oh;
			iorEntering = inter.enteredSub->getRefractionIndex();
			iorExiting = inter.nextSubstance->getRefractionIndexInvert();
		}
		else
		{
			iorEntering = inter.nextSubstance->getRefractionIndex();
			iorExiting = inter.enteredSub->getRefractionIndexInvert();
		}

		const Real iorEta = iorExiting * iorEntering;
		const Real term = (dp_oh + iorEta * dp_ih) /opow2;

		if(term <= constants::epsilon<Real>) [[unlikely]]
			return 0;

		plassert(mMicrofacets);

		return pl::assert_valid( iorEta /opow2 * mMicrofacets->PDF(inter, odir, idir, hv).value() * (dp_ih /oabs) * (term /oprtreciprocal), valid::Weight );
	}

	OptionalPDF<Real> BxDFSpecularTransmission::PDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		if(!mMicrofacets || idir /odot/ inter.lobe().normal >= -constants::epsilon<Real> || odir /odot/ inter.lobe().normal >= -constants::epsilon<Real>) [[unlikely]]
			return pl::nullopt;

		const auto hv = getHalfVector(inter, odir, idir);

		return specPDF(mode, inter, odir, idir, hv);
	}

	BxDFRadianceResult BxDFSpecularTransmission::radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		if(!mMicrofacets)
			return {{Black, getColorAlpha(inter, odir)}, 0};

		const Real dp_on = odir /odot/ inter.lobe().normal;

		if(dp_on >= -constants::epsilon<Real> || idir /odot/ inter.lobe().normal >= -constants::epsilon<Real>) [[unlikely]]
			return {{Black, getColorAlpha(inter, odir)}, 0};

		const auto hv = getHalfVector(inter, odir, idir);
		const auto D = (*mMicrofacets)(inter, odir, idir, hv);
		Color F = mFresnel ? (*mFresnel)(mode, hv, odir, inter) : Black;

		if(mDualLayer)
			F = dualLayerRefr(F);
		else
			F = White - F;

		auto color = getColor(inter, odir);
		color.color *= F;
		const auto albedo = getColorAlbedo(color.color);
		color.color *= specRadiance(mode, inter, odir, idir, hv, true, dp_on, D);

		return {pl::assert_valid(color), albedo};
	}

	Color BxDFSpecularTransmission::alpha(EnumTransportMode, const SurfaceInteraction &inter, const VecD odir, const VecD /*idir*/) const noexcept
	{
		return getColorAlpha(inter, odir);
	}

	pl::marked_optional<SampledBxDF> BxDFSpecularTransmission::sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &, EnumRayType typeMask) const noexcept
	{
		const Real dp_on = odir /odot/ inter.lobe().normal;

		if(!isType(*this, typeMask) || dp_on >= -constants::epsilon<Real>) [[unlikely]]
			return SampledBxDF::NullOpaque();

		SampledBxDF sp;
		SampledHalfVector hv;

		bool isMicrofacet = mMicrofacets != nullptr;
		Real mpdf;

		sp.color = getColor(inter, odir);

		if(isMicrofacet)
		{
			const auto shv = mMicrofacets->sampleHalfVector(rnd, inter, odir);
			if(!shv)
				return sp;

			hv = *shv;
			sp.pdf = hv.pdf;
			isMicrofacet = sp.pdf != 1;
		}
		else
		{
			hv.dir = inter.lobe().normal;
			sp.pdf = 1;
		}

		Color F;
		if(mFresnel)
		{
			F = (*mFresnel)(mode, hv.dir, odir, inter);

			if(isWhite(F))
			{
				sp.reset();
				return sp;
			}

			sp.dir = *mFresnel->refract(mode, inter, odir, hv.dir);

			if(sp.dir /odot/ inter.lobe().normal >= -constants::epsilon<Real>) [[unlikely]]
			{
				sp.reset();
				return sp;
			}
		}
		else
		{
			F = Black;
			sp.dir = odir;
		}

		if(mDualLayer)
			F = dualLayerRefr(F);
		else
			F = White - F;

		sp.color.color *= F;
		sp.albedo = getColorAlbedo(sp.color.color);
		sp.color.color *= specRadiance(mode, inter, odir, sp.dir, hv.dir, isMicrofacet, dp_on, hv.D, &mpdf);
		sp.type = getType();
		sp.hemisphere = EnumHemisphere::South;

		pl::assert_valid( sp.pdf *= mpdf, valid::PDF );
		pl::assert_valid( sp.color );

		return sp;
	}


	BxDFRadianceResult BxDFSpecularCombined::radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		auto color = getColor(inter, odir);

		if(!mMicrofacets && !mBottom)
			return {{Black, color.alpha}, 0};

		const Real dp_on = odir /odot/ inter.lobe().normal;

		if(dp_on >= -constants::epsilon<Real> || idir /odot/ inter.lobe().normal /oabs <= constants::epsilon<Real>) [[unlikely]]
			return {{Black, color.alpha}, 0};

		auto hemisphere = inter.lobe().getHemisphere(idir);
		VecD hv;
		Real D;
		const bool isMicrofacet = mMicrofacets != nullptr;
		if(isMicrofacet)
		{
			hv = getHalfVector(inter, odir, idir);
			D = (*mMicrofacets)(inter, odir, idir, hv);
		}
		else
		{
			hv = inter.lobe().normal;
			D = 0;
		}

		Color F = mFresnel ? (*mFresnel)(mode, hv, odir, inter) : Black;
		CReal albedo = 0;

		if(hemisphere == EnumHemisphere::South)
		{
			if(mBottom || !isMicrofacet)
				return {{Black, color.alpha}, 0};

			// Account for light bouncing between the 2 layers
			if(mDualLayer)
				F = dualLayerRefr(F);
			else
				F = White - F;

			color.color *= F;
			albedo = getColorAlbedo(color.color);
			color.color *= mTrans.getColor(inter, odir).color * mTrans.specRadiance(mode, inter, odir, idir, hv, isMicrofacet, dp_on, D);
		}
		else
		{
			if(mBottom)
			{
				if(isMicrofacet || isWhite(F))
					return {{Black, color.alpha}, 0};

				const auto bodir = mFresnel->refract(mode, inter, odir, hv);
				auto bidir = mFresnel->refract(switchTransportMode(mode), inter, -idir, hv);
				if(!bodir || !bidir)
					return {{Black, color.alpha}, 0};

				*bidir = -(*bidir);

				if(*bidir /odot/ hv <= constants::epsilon<Real> || *bodir /odot/ hv >= -constants::epsilon<Real>) [[unlikely]]
					return {{Black, color.alpha}, 0};

				auto newInter = inter;
				newInter.enteredSub = inter.nextSubstance;

				auto [colorBottom, albedoBottom] = mBottom->radiance(mode, newInter, *bodir, *bidir);
				colorBottom.color *= colorBottom.alpha;
				albedoBottom *= getColorAlbedo(colorBottom.alpha);

				const auto Fb = (*mFresnel)(mode, hv, *bidir, inter);
				const auto tr1 = mTrans.getColor(inter, odir).color * mTrans.specRadiance(mode, inter, odir, *bodir, hv, false, dp_on, D);
				const auto tr2 = mTrans.getColor(inter, *bidir).color * mTrans.specRadiance(mode, inter, *bidir, idir, hv, false, *bidir /odot/ inter.lobe().normal, D);

				const auto R = (White - Fb) * (White - F) * tr1 * tr2;
				albedo = albedoBottom * getColorAlbedo(R);
				color.color *= colorBottom.color * R * (*bidir /odot/ inter.lobe().normal /oabs /oprtreciprocal) * (idir /odot/ inter.lobe().normal /oabs);
			}
			else if(isMicrofacet)
			{
				if(mDualLayer)
					F = dualLayerRefl(F);

				color.color = mRefl.getColor(inter, odir).color * F;
				albedo = getColorAlbedo(color.color);
				color.color *= mRefl.specRadiance(mode, inter, odir, idir, hv, true, dp_on, D);
			}
		}

		return {pl::assert_valid(color), albedo};
	}

	pl::marked_optional<SampledBxDF> BxDFSpecularCombined::sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask) const noexcept
	{
		if(!isType(*this, typeMask)) [[unlikely]]
			return SampledBxDF::NullOpaque();

		const auto color = getColor(inter, odir);

		if(isAlmostBlack(color.color) || isAlmostBlack(color.alpha))
			return SampledBxDF::NullTransparent(color.alpha);

		if(!mFresnel)
		{
			auto sp = mRefl.sampleDir(mode, rnd, inter, odir, rng, typeMask);
			sp->color.color *= color.color;
			sp->color.alpha *= color.alpha;
			sp->albedo *= getColorAlbedo(color.color);
			return sp;
		}

		SampledBxDF sp;
		sp.color = color;

		const Real dp_on = odir /odot/ inter.lobe().normal;

		if(dp_on >= -constants::epsilon<Real>) [[unlikely]]
			return sp;

		SampledHalfVector hv;
		Real dp_oh;
		bool isMicrofacet = mMicrofacets != nullptr;

		if(mMicrofacets)
		{
			const auto shv = mMicrofacets->sampleHalfVector(rnd, inter, odir);
			if(!shv)
				return sp;

			hv = *shv;
		}
		else
		{
			hv.dir = inter.lobe().normal;
			hv.D = 0;
			dp_oh = 0;
		}

		isMicrofacet = hv.D > 0;

		if(isMicrofacet)
		{
			dp_oh = -(hv.dir /odot/ odir);
			if(dp_oh <= constants::epsilon<Real>) [[unlikely]]
				return sp;

			sp.type = EnumRayType::SpecularGlossy;
		}
		else
		{
			hv.pdf = 1;
			sp.type = EnumRayType::Specular;
		}

		auto F = (*mFresnel)(mode, hv.dir, odir, inter);

		const Real reflProb = pl::assert_valid( reflectProb(F), valid::Weight );

		if(mBottom)
		{

			if(uniformRealRng<Real>()(rng) > reflProb)
			{
				const auto bodir = mFresnel->refract(mode, inter, odir, hv.dir);
				if(!bodir)
					return sp;

				auto newInter = inter;
				newInter.enteredSub = inter.nextSubstance;

				const auto bottomSp = mBottom->sampleDir(mode, rnd, newInter, *bodir, rng, typeMask);

				if(!bottomSp)
					return sp;

				if(const auto Fb = (*mFresnel)(mode, hv.dir, bottomSp->dir, inter); isWhite(Fb))
				{
					sp.dir = PhotoRT::reflect(odir, hv.dir);

					if(sp.dir /odot/ inter.lobe().normal <= constants::epsilon<Real>) [[unlikely]]
					{
						sp.reset();
						return sp;
					}

					sp.color.color.set_zero();
					sp.albedo = 0;
				}
				else
				{
					const auto rDir = mFresnel->refract(mode, inter, bottomSp->dir, hv.dir);
					if(!rDir)
					{
						sp.reset();
						return sp;
					}

					sp.dir = *rDir;

					const auto tr1 = mTrans.getColor(inter, odir).color * mTrans.specRadiance(mode, inter, odir, *bodir, hv.dir, false, dp_on, hv.D);
					const auto tr2 = mTrans.getColor(inter, bottomSp->dir).color * mTrans.specRadiance(mode, inter, bottomSp->dir, sp.dir, hv.dir, false, bottomSp->dir /odot/ inter.lobe().normal, hv.D);

					const auto R = (White - Fb) * (White - F) * tr1 * tr2;
					sp.color.color *= R;
					sp.albedo = bottomSp->albedo * getColorAlbedo(sp.color.color);
					pl::assert_valid( sp.color.color *= bottomSp->color.color * (bottomSp->dir /odot/ inter.lobe().normal /oabs /oprtreciprocal) * (sp.dir /odot/ inter.lobe().normal /oabs) );
				}

				sp.type = bottomSp->type;
				sp.hemisphere = bottomSp->hemisphere;
				sp.pdf = bottomSp->pdf * (1 - reflProb);
			}
			else
			{
				sp.hemisphere = EnumHemisphere::North;

				sp.dir = PhotoRT::reflect(odir, hv.dir);

				if(sp.dir /odot/ inter.lobe().normal <= constants::epsilon<Real>) [[unlikely]]
					return sp;

				sp.pdf = hv.pdf * reflProb;

				sp.color.color *= F;
				sp.albedo = getColorAlbedo(sp.color.color);
				sp.color.color *= mRefl.getColor(inter, odir).color * mRefl.specRadiance(mode, inter, odir, sp.dir, hv.dir, isMicrofacet, dp_on, hv.D);

				pl::assert_valid(sp.color);
				pl::assert_valid(sp.pdf, valid::Weight);
				pl::assert_valid(sp.dir);
			}

		}
		else
		{

			if(!reflProb || uniformRealRng<Real>()(rng) >= reflProb)
			{
				sp.hemisphere = EnumHemisphere::South;

				sp.dir = *mFresnel->refract(mode, inter, odir, hv.dir);

				if(sp.dir /odot/ inter.lobe().normal >= -constants::epsilon<Real>) [[unlikely]]
					return sp;

				// Account for light bouncing between the 2 layers
				if(mDualLayer)
					F = dualLayerRefr(F);
				else
					F = White - F;

				sp.color.color *= mTrans.getColor(inter, odir).color * F;
				sp.albedo = getColorAlbedo(sp.color.color);
				sp.color.color *= mTrans.specRadiance(mode, inter, odir, sp.dir, hv.dir, isMicrofacet, dp_on, hv.D, &sp.pdf);

				sp.pdf *= hv.pdf * (1 - reflProb);

				pl::assert_valid(sp.color);
				pl::assert_valid(sp.pdf, valid::Weight);
				pl::assert_valid(sp.dir);
			}
			else
			{
				sp.hemisphere = EnumHemisphere::North;

				sp.dir = PhotoRT::reflect(odir, hv.dir);

				if(sp.dir /odot/ inter.lobe().normal <= constants::epsilon<Real>) [[unlikely]]
					return sp;

				// Account for light bouncing between the 2 layers
				if(mDualLayer)
					F = dualLayerRefl(F);

				sp.pdf = hv.pdf * reflProb;

				if(isMicrofacet)
					sp.pdf *= (dp_oh * 4) /oprtreciprocal;

				sp.color.color *= mRefl.getColor(inter, odir).color * F;
				sp.albedo = getColorAlbedo(sp.color.color);
				sp.color.color *= mRefl.specRadiance(mode, inter, odir, sp.dir, hv.dir, isMicrofacet, dp_on, hv.D);

				pl::assert_valid(sp.color);
				pl::assert_valid(sp.pdf, valid::Weight);
				pl::assert_valid(sp.dir);
			}

		}

		pl::assert_valid(sp.pdf, valid::Weight);
		pl::assert_valid(sp.color);

		return sp;
	}

	Color BxDFSpecularCombined::alpha(EnumTransportMode, const SurfaceInteraction &inter, const VecD odir, const VecD /*idir*/) const noexcept
	{
		return getColorAlpha(inter, odir);
	}

	OptionalPDF<Real> BxDFSpecularCombined::PDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		if( (!mMicrofacets && !mBottom) || (mMicrofacets && mBottom) )
			return pl::nullopt;

		Real pdf = 0;

		if(!mFresnel)
			pdf = mMicrofacets ? mRefl.PDF(mode, inter, odir, idir).value() : 0;
		else
		{
			const auto hemisphere = inter.lobe().getHemisphere(idir);
			if(mBottom && hemisphere == EnumHemisphere::South)
				return pl::nullopt;

			const bool isMicrofacet = mMicrofacets != nullptr;
			const VecD hv = isMicrofacet ? getHalfVector(inter, odir, idir) : inter.lobe().normal;
			const Color F = (*mFresnel)(mode, hv, odir, inter);

			const Real reflProb = reflectProb(F);

			if(mBottom)
			{
				if(isMicrofacet || isWhite(F))
					return pl::nullopt;

				if(reflProb < 1)
				{
					const auto bodir = mFresnel->refract(mode, inter, odir, hv);
					const auto bidir = mFresnel->refract(switchTransportMode(mode), inter, -idir, hv);

					if(bodir && bidir)
					{
						auto newInter = inter;
						newInter.enteredSub = inter.nextSubstance;

						pdf += mBottom->PDF(mode, newInter, *bodir, -(*bidir)).value() * (1 - reflProb);
					}
				}
			}
			else if(hemisphere == EnumHemisphere::South)
			{
				if(isMicrofacet)
				{
					if(idir /odot/ hv < -constants::epsilon<Real>)
						pdf = mTrans.specPDF(mode, inter, odir, idir, hv) * (1 - reflProb);
				}
			}
			else
			{
				pdf = !isMicrofacet ? 0 : mRefl.specPDF(mode, inter, odir, idir, hv) * reflProb;
			}
		}

		return pl::assert_valid(pdf, valid::Weight);
	}

	void BxDFSpecularCombined::setParameters(const ParameterTree &params)
	{
		BxDFSpecularBase::setParameters(params);

		addParamReference(mBottom, params, "bottom");
		addParamReference(mTrans.mColorMapper, params, "texture_trans");
		addParamReference(mRefl.mColorMapper, params, "texture_refl");
	}

	ParameterTree BxDFSpecularCombined::getParameters(bool deep) const
	{
		ParameterTree params {BxDFSpecularBase::getParameters(deep)};

		if(mBottom)
			params["bottom"].fromEntityRef(*mBottom, deep);

		if(mTrans.mColorMapper)
			params["texture_trans"].fromEntityRef(*mTrans.mColorMapper, deep);

		if(mRefl.mColorMapper)
			params["texture_refl"].fromEntityRef(*mRefl.mColorMapper, deep);

		return params;
	}


	void BxDFAlphaBlend::setParameters(const ParameterTree &params)
	{
		BxDFBase::setParameters(params);

		addParamReference(mTop, params, "top");
		addParamReference(mBottom, params, "bottom");
	}

	ParameterTree BxDFAlphaBlend::getParameters(bool deep) const
	{
		ParameterTree params {BxDFBase::getParameters(deep)};

		if(mTop)
			params["top"].fromEntityRef(*mTop, deep);

		if(mBottom)
			params["bottom"].fromEntityRef(*mBottom, deep);

		return params;
	}

	BxDFRadianceResult BxDFAlphaBlend::radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		auto ohemisphere = inter.lobe().getHemisphere(odir);

		if(inter.surface().isBackfaceHit())
			ohemisphere = swapHemisphere(ohemisphere);

		const IBxDF *first, *second;

		Color alpha;
		auto [res, abalpha] = getColor(inter, odir);
		auto albedo = getColorAlbedo(res);

		if(isAlmostBlack(abalpha))
			return {};

		if(ohemisphere == EnumHemisphere::North)
		{
			first = mBottom.get();
			second = mTop.get();
		}
		else
		{
			first = mTop.get();
			second = mBottom.get();
		}

		if(first)
		{
			const auto [c, alb] = first->radiance(mode, inter, odir, idir);
			alpha = c.alpha;
			res *= c.color;
			albedo *= getColorAlbedo(c.color);
		}
		else
		{
			res.set_zero();
			alpha.set_zero();
			albedo = 0;
		}

		if(second)
		{
			const auto [bColor, bAlbedo] = second->radiance(mode, inter, odir, idir);

			res = res * alpha + bColor.color * (White - alpha);
			const auto alphaAlb = getColorAlbedo(alpha);
			albedo = albedo * alphaAlb + bAlbedo * (1 - alphaAlb);
			alpha = White - (White - bColor.alpha) * (White - alpha);
		}

		alpha *= abalpha;

		return {{pl::assert_valid(res), pl::assert_valid(alpha)}, albedo};
	}

	pl::marked_optional<SampledBxDF> BxDFAlphaBlend::sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask) const noexcept
	{
		auto ohemisphere = inter.lobe().getHemisphere(odir);

		if(inter.surface().isBackfaceHit())
			ohemisphere = swapHemisphere(ohemisphere);

		const IBxDF *first, *second, *sampled = nullptr;

		if(ohemisphere == EnumHemisphere::North)
		{
			first = mBottom.get();
			second = mTop.get();
		}
		else
		{
			first = mTop.get();
			second = mBottom.get();
		}

		pl::marked_optional<SampledBxDF> sp;

		if(first)
		{
			sp = first->sampleDir(mode, rnd, inter, odir, rng, typeMask);
			sampled = first;
		}

		Real firstProb = !second ? 1 : firstSelectProb(sp->color.alpha);
		Real otherProb;

		if(uniformRealRng<Real>()(rng) > firstProb)
		{
			sp = second->sampleDir(mode, rnd, inter, odir, rng, typeMask);

			//res *= alpha;
			otherProb = firstProb;
			sp->pdf *= 1 - firstProb;
			sampled = second;
		}
		else
		{
			otherProb = 1 - firstProb;
			sp->pdf *= firstProb;
			//res *= alpha;
		}

		if(sampled)
		{
			const IBxDF *other = sampled == first ? second : first;

			if(other == first)
				pl::adl_swap(firstProb, otherProb);

			if(other && otherProb)
			{
				BxDFRadianceResult scolor;

				if(sampled->isDiracDelta())
				{
					scolor.color.alpha = other->alpha(mode, inter, odir, sp->dir);
					scolor.color.color.set_zero();
					scolor.albedo = 0;
				}
				else
				{
					scolor = other->radiance(mode, inter, odir, sp->dir);
					sp->pdf += other->PDF(mode, inter, odir, sp->dir).value() * otherProb;
				}

				if(other == first)
					pl::adl_swap(sp->color, scolor.color);

				sp->color.color = sp->color.color * sp->color.alpha + scolor.color.color * (White - sp->color.alpha);
				const auto alphaAlbedo = getColorAlbedo(sp->color.alpha);
				sp->albedo = sp->albedo * alphaAlbedo + scolor.albedo * (1 - alphaAlbedo);
				sp->color.alpha = blendAlpha(scolor.color.alpha, sp->color.alpha);

				pl::assert_valid(sp->color);
			}
		}

		const auto c = getColor(inter, odir);
		sp->albedo *= getColorAlbedo(c.color);
		pl::assert_valid( sp->color = sp->color * c );

		return sp;
	}

	Color BxDFAlphaBlend::alpha(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		auto ohemisphere = inter.lobe().getHemisphere(odir);
		Color alpha;

		if(inter.surface().isBackfaceHit())
			ohemisphere = swapHemisphere(ohemisphere);

		const IBxDF *first, *second;

		if(ohemisphere == EnumHemisphere::North)
		{
			first = mBottom.get();
			second = mTop.get();
		}
		else
		{
			first = mTop.get();
			second = mBottom.get();
		}

		if(first)
			alpha = first->alpha(mode, inter, odir, idir);
		else
			alpha.set_zero();

		if(second)
		{
			const auto balpha = second->alpha(mode, inter, odir, idir);

			alpha = blendAlpha(balpha, alpha);
		}

		alpha *= getColorAlpha(inter, odir);

		return pl::assert_valid(alpha);
	}

	OptionalPDF<Real> BxDFAlphaBlend::PDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		auto ohemisphere = inter.lobe().getHemisphere(odir);

		if(inter.surface().isBackfaceHit())
			ohemisphere = swapHemisphere(ohemisphere);

		const IBxDF *first, *second, *sampled;

		if(ohemisphere == EnumHemisphere::North)
		{
			first = mBottom.get();
			second = mTop.get();
		}
		else
		{
			first = mTop.get();
			second = mBottom.get();
		}

		sampled = first ? first : second;
		if(!sampled)
			return pl::nullopt;

		const auto alpha = sampled->alpha(mode, inter, odir, idir);
		const Real firstProb = !second ? 1 : firstSelectProb(alpha);
		Real pdf = 0;

		if(firstProb)
			pdf += sampled->PDF(mode, inter, odir, idir).value() * firstProb;

		if(firstProb < 1)
			pdf += second->PDF(mode, inter, odir, idir).value() * (1 - firstProb);

		return pdf;
	}


	void BxDFTabulated::setParameters(const ParameterTree &params)
	{
		BxDFBase::setParameters(params);

		Real ori = params.getValueOrDefault<Real>("orientation", 0);
		setOrientation(pl::degree_to_radian(ori));

		addParamReference(mSampler, params, "sampler");
		addParamReference(mRadianceMap, params, "radiance_map");

		if(auto bxdf = entityCollection->createEntityIfParamExists(type_v<const IBxDF>, params, "from_bxdf"); bxdf)
			createFromBxDF(*bxdf);
	}

	ParameterTree BxDFTabulated::getParameters(bool deep) const
	{
		ParameterTree params {BxDFBase::getParameters(deep)};

		params["orientation"] = pl::radian_to_degree(mOrientation);

		if(mSampler)
			params["sampler"].fromEntityRef(*mSampler, deep);
		if(mRadianceMap)
			params["radiance_map"].fromEntityRef(*mRadianceMap, deep);

		return params;
	}

	void BxDFTabulated::createFromBxDF(const IBxDF &bxdf)
	{
		if(mRadianceMap)
		{
			auto &bitmap = dynamic_cast<BitmapBase &>(*mRadianceMap);

			SurfaceInteraction inter;

			inter.surface().shadingNormal = inter.surface().surfaceNormal = {0, 1, 0, 0};
			inter.lobe().calcLobeDirsAnisotropic(inter.surface().shadingNormal);

			const auto revdim = Point2Dr{r_(bitmap.getWidth()), r_(bitmap.getHeight())} /oprtreciprocal;
			const VecD odir{0, -1, 0, 0};

			for(auto p : bitmap.range2d(bitmap.getRect()) | pl::iterator_view)
			{
				const auto pos = Point2Dr{p.getPosition()} * revdim;
				*p = bxdf(EnumTransportMode::Importance, inter, odir, inter.lobe().sphericalDirection(pos)).color.color;
			}
		}

		if(mSampler)
			mSampler->init();
	}

	BxDFRadianceResult BxDFTabulated::radiance(EnumTransportMode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		Lobe<> rotated{inter.lobe()};
		rotated.rotateOnNormal(mRotCos, mRotSin);

		const auto bcolor = getColor(inter, odir);
		const auto p = rotated.sphereUV(idir);
		const auto res = !mRadianceMap ? Black : (*mRadianceMap)(p[0], p[1]);

		return {{pl::assert_valid(res) * bcolor.color, bcolor.alpha}, getColorAlbedo(bcolor.color)};
	}

	OptionalPDF<Real> BxDFTabulated::PDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		if(!mSampler) [[unlikely]]
			return BxDFBase::PDF(mode, inter, odir, idir);

		Lobe<> rotated{inter.lobe()};
		rotated.rotateOnNormal(mRotCos, mRotSin);

		return mSampler->PDF(rotated.sphereUV(idir)).value() * pl::constants::sphere_pdf<Real>;
	}

	pl::marked_optional<SampledBxDF> BxDFTabulated::sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask) const noexcept
	{
		if(!mSampler) [[unlikely]]
			return BxDFBase::sampleDir(mode, rnd, inter, odir, rng, typeMask);

		SampledBxDF sp;
		const auto sampled = (*mSampler)(rnd);

		sp.pdf = sampled->pdf * pl::constants::sphere_pdf<Real>;

		Lobe<> rotated{inter.lobe()};
		rotated.rotateOnNormal(mRotCos, mRotSin);

		sp.dir = rotated.sphericalDirection(sampled->value);

		sp.color = getColor(inter, odir);
		sp.albedo = getColorAlbedo(sp.color.color);
		sp.color.color *= !mRadianceMap ? Black : (*mRadianceMap)(sampled->value[0], sampled->value[1]) * pl::plsin(sampled->value[1] * std::numbers::pi_v<Real>);

		sp.type = EnumRayType::Diffuse;
		sp.hemisphere = inter.lobe().getHemisphere(sp.dir);

		return sp;
	}
}
