/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Surface/Fresnel.hpp>
#include <PhotoRT/Core/Material.hpp>
#include <PhotoRT/Core/RayTracer.hpp>
#include <PhotoRT/Core/Distribution.hpp>
#include <PhotoRT/Core/Interaction.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

namespace PhotoRT
{
	// Fresnel
	PRT_ENTITY_FACTORY_DEFINE(FresnelDielectric)
	PRT_ENTITY_FACTORY_DEFINE(FresnelSchlickDielectric)
	PRT_ENTITY_FACTORY_DEFINE(FresnelConductor)
	PRT_ENTITY_FACTORY_DEFINE(FresnelSchlickConductor)


	Color FresnelDielectric::fresnel(EnumTransportMode, const VecD normal, const VecD odir, const SurfaceInteraction &inter) const noexcept
	{
		Real cos1 = normal /odot/ odir;
		Real refractionIndexExiting, refractionIndexEntering, refractionIndexEnteringInv;

		if(cos1 >= 0)
		{
			refractionIndexExiting = inter.nextSubstance->isSubstance() ? inter.nextSubstance->getRefractionIndex() : ior;
			refractionIndexEntering = inter.enteredSub->getRefractionIndex();
			refractionIndexEnteringInv = inter.enteredSub->getRefractionIndexInvert();
		}
		else
		{
			cos1 = -cos1;
			refractionIndexExiting = inter.enteredSub->getRefractionIndex();
			refractionIndexEntering = inter.nextSubstance->isSubstance() ? inter.nextSubstance->getRefractionIndex() : ior;
			refractionIndexEnteringInv = inter.nextSubstance->isSubstance() ? inter.nextSubstance->getRefractionIndexInvert() : iorInv;
		}

		cos1 = pl::plmin<Real>(cos1, 1);

		const Real refractionIndexRatio = refractionIndexExiting * refractionIndexEnteringInv;
		const Real sin1 = refractionIndexRatio * (pl::plmax<Real>(1 - cos1 /opow2, 0) /oprtsqrt);

		// Total internal reflection
		if(sin1 >= 1)
			return White;

		const Real cos2 = pl::plmax<Real>(1 - sin1 /opow2, 0) /oprtsqrt;

		const Real rparl =
			((refractionIndexEntering * cos1) - (refractionIndexExiting * cos2)) *
			pl::reciprocal((refractionIndexEntering * cos1) + (refractionIndexExiting * cos2));
		const Real rperp =
			((refractionIndexExiting * cos1) - (refractionIndexEntering * cos2)) *
			pl::reciprocal((refractionIndexExiting * cos1) + (refractionIndexEntering * cos2));

		return pl::assert_valid( pl::plmin<Real>((rparl /opow2 + rperp /opow2) * 0.5, 1), valid::Weight );
	}

	// From https://seblagarde.wordpress.com/2013/04/29/memo-on-fresnel-equations/
	Color FresnelSchlickDielectric::fresnel(EnumTransportMode, const VecD normal, const VecD odir, const SurfaceInteraction &inter) const noexcept
	{
		Real CosX = normal /odot/ odir;
		Real refractionIndexExiting, refractionIndexEntering, refractionIndexEnteringInv;

		if(CosX >= 0)
		{
			refractionIndexExiting = inter.nextSubstance->isSubstance() ? inter.nextSubstance->getRefractionIndex() : ior;
			refractionIndexEntering = inter.enteredSub->getRefractionIndex();
			refractionIndexEnteringInv = inter.enteredSub->getRefractionIndexInvert();
		}
		else
		{
			CosX = -CosX;
			refractionIndexExiting = inter.enteredSub->getRefractionIndex();
			refractionIndexEntering = inter.nextSubstance->isSubstance() ? inter.nextSubstance->getRefractionIndex() : ior;
			refractionIndexEnteringInv = inter.nextSubstance->isSubstance() ? inter.nextSubstance->getRefractionIndexInvert() : iorInv;
		}

		CosX = pl::plmin<Real>(CosX, 1);

		const Real R0 = ((refractionIndexEntering - refractionIndexExiting) * pl::reciprocal(refractionIndexEntering + refractionIndexExiting)) /opow2;

		if(refractionIndexExiting > refractionIndexEntering)
		{
			const Real inv_eta = refractionIndexExiting * refractionIndexEnteringInv;
			const Real SinT2 = inv_eta /opow2 * (1 - CosX /opow2);

			if(SinT2 >= 1)
				return White; // TIR

			CosX = (1 - SinT2) /oprtsqrt;
		}

		return pl::assert_valid( pl::plmin<Real>(R0 + (1 - R0) * pl::plpow(1 - CosX, 5), 1), valid::Weight );
	}

	Color FresnelSchlickConductor::fresnel(EnumTransportMode, const VecD normal, const VecD odir, const SurfaceInteraction &inter) const noexcept
	{
		const Color refractionIndexExiting {inter.enteredSub->getRefractionIndex()};
		const Color iorColor{ior};

		const auto dp_on = -(normal /odot/ odir);
		auto res = (pl::pow2(iorColor - refractionIndexExiting) + iorColor * refractionIndexExiting * 4 * pl::plpow(pl::plmax<Real>(0, 1 - dp_on), 5) + (mExtinctionCoef /opow2))
			* pl::reciprocal(pl::pow2(iorColor + refractionIndexExiting) + (mExtinctionCoef /opow2));

		res = res /omin/ White;

		return pl::assert_valid(res);
	}


	void FresnelConductor::setParameters(const ParameterTree &params)
	{
		FresnelBase::setParameters(params);

		params.getValueIfExists("extinction", mExtinctionCoef);
	}

	ParameterTree FresnelConductor::getParameters(bool deep) const
	{
		ParameterTree params {FresnelBase::getParameters(deep)};

		params["extinction"] = mExtinctionCoef;

		return params;
	}

	// From https://seblagarde.wordpress.com/2013/04/29/memo-on-fresnel-equations/
	Color FresnelConductor::fresnel(EnumTransportMode, const VecD normal, const VecD idir, const SurfaceInteraction &inter) const noexcept
	{
		const Real refractionIndexExitingInv = inter.enteredSub->getRefractionIndexInvert();
		const Real refractionIndexEntering = ior;
		const Real refractionIndexRatio = refractionIndexExitingInv * refractionIndexEntering;

		const Real cosThetaI = pl::plmin<Real>(1, idir /odot/ normal /oabs);
		const Color etak = mExtinctionCoef * refractionIndexExitingInv;

		const Real cosThetaI2 = cosThetaI /opow2;
		const Real sinThetaI2 = 1 - cosThetaI2;
		const Color eta2 = refractionIndexRatio /opow2;
		const Color etak2 = etak /opow2;

		const Color t0 = eta2 - etak2 - sinThetaI2;
    const Color a2plusb2 = (t0 /opow2 + eta2 * etak2 * 4) /oprtsqrt;
    const Color t1 = a2plusb2 + cosThetaI2;
    const Color a = ((a2plusb2 + t0) * pl::tag::half) /oprtsqrt;
    const Color t2 = a * cosThetaI * pl::tag::two;
    const Color Rs = (t1 - t2) * pl::reciprocal(t1 + t2);

    const Color t3 = a2plusb2 * cosThetaI2 + sinThetaI2 /opow2;
    const Color t4 = t2 * sinThetaI2;
    const Color Rp = Rs * (t3 - t4) * pl::reciprocal(t3 + t4);

		return pl::assert_valid( (Rp + Rs) * pl::tag::half );
	}
}
