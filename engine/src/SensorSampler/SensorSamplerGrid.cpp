/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/SensorSampler/SensorSamplerGrid.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedLightSamplerGridDistribution)
	void AugmentedLightSamplerGridDistribution::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		params.getValueIfExistsBind
			("nb_cells",                PL_LAMBDA_FORWARD_THIS(setNbCells))
			("max_lights_per_cells",    PL_LAMBDA_FORWARD_THIS(setMaxNbLightsPerCell))
			("sample_area",             PL_LAMBDA_FORWARD_THIS(setSampleArea))
			("test_visibility",         PL_LAMBDA_FORWARD_THIS(setTestVisibility));

		addParamReference(mFallbackSampler, params, "fallback_sampler");
	}

	ParameterTree AugmentedLightSamplerGridDistribution::getParameters(bool deep) const
	{
		ParameterTree params{RenderEntity::getParameters(deep)};

		params.insert({
			{"nb_cells",                 mNbCells},
			{"max_lights_per_cells",     mMaxNbLightsPerCell},
			{"sample_area",              mSampleArea},
			{"test_visibility",          mTestVisibility}
		});

		if(mFallbackSampler)
			params["fallback_sampler"].fromEntityRef(*mFallbackSampler, deep);

		return params;
	}
}