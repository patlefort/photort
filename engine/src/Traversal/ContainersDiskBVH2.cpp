/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Traversal/Containers.hpp>
#include <PhotoRT/Traversal/ContainerSetDisk.hpp>
#include <PhotoRT/PrimitiveGroup/Disk.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

namespace PhotoRT::Traversal
{
	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(Augmented_container_factory__disk_bvh_cached)
	PRT_ENTITY_FACTORY_ALIAS(container_factory__disk_best, "container_factory-disk_best", "container_factory-disk_bvh_cached")
}
