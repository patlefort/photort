/*
	PhotoRT

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/PrimitiveGroup/Triangle.hpp>
//#include <PhotoRT/Core/Distribution.hpp>
//#include <PhotoRT/Core/Material.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/Compression.hpp>
#include <PhotoRT/Core/SurfaceMapper.hpp>
#include <PhotoRT/Core/Interaction.hpp>

#include <patlib/task.hpp>

#include <boost/dynamic_bitset.hpp>

#include <optional>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_DEFINE(PrimGroupTriangles)
}

namespace PhotoRT::PrimGroupTriImpl
{
	namespace
	{
		[[nodiscard]] constexpr auto triangleEntryRange(auto &pg) noexcept
		{
			return
				mir(pg.getNbPrimitives())
				| rgv::transform([&](const auto i){
					return pg.mData[i];
				});
		}

		[[nodiscard]] constexpr auto triangleVerticesRange(auto &pg) noexcept
		{
			return rgv::cartesian_product(
					mir(pg.getNbPrimitives()),
					mir(3)
				)
				| rgv::transform(pl::applier([&](const auto i, const auto vi){
					return std::make_tuple(std::ref(pg.mData->primitives.mTriangles[i]), i, vi);
				}));
		}

		[[nodiscard]] constexpr auto triangleVerticesRangeWithPrimitive(auto &pg) noexcept
		{
			return triangleVerticesRange(pg)
				| rgv::transform(pl::applier([&](auto&& t, const auto ti, const auto vi){
					return std::make_tuple(std::ref(PL_FWD(t)), pg.mData->primitives.getPrimitive(ti), ti, vi);
				}));
		}

		[[nodiscard]] constexpr bool triangleVertexValid(const auto &p, const auto vi) noexcept
			{ return pl::is_valid(p.mVn[vi]) && p.mNormal /odot/ p.mVn[vi] > 0; }

		[[nodiscard]] constexpr auto vtsTriIndexRange(auto &pg, PrimitiveIndex index) noexcept
		{
			return rgv::all(pg.mVtsTriIndex) |
				rgv::slice(
					pg.mVtsTriIndexStart[index],
					index == pg.mVtsTriIndexStart.size() - 1 ? (PrimitiveIndex)pg.mVtsTriIndex.size() : pg.mVtsTriIndexStart[index + 1]
				);
		}
	} // namespace

	void PrimGroupTriangles::setParameters(const ParameterTree &params)
	{
		ThisBase::setParameters(params);

		if(const auto p = params.findParam("cube"); p)
		{
			const auto &param = *p;

			clearAll();

			initCube(
				param.getValueOrDefault<Real>("x1", 0),
				param.getValueOrDefault<Real>("y1", 0),
				param.getValueOrDefault<Real>("z1", 0),
				param.getValueOrDefault<Real>("x2", 1),
				param.getValueOrDefault<Real>("y2", 1),
				param.getValueOrDefault<Real>("z2", 1)
			);

			computeTriangleNormals();
		}
		else if(const auto p = params.findParam("sphere"); p)
		{
			const auto &param = *p;

			clearAll();

			initSphere(
				param.getValueOrDefault<Real>("sizex", 1),
				param.getValueOrDefault<Real>("sizey", 1),
				param.getValueOrDefault<Real>("sizez", 1),
				param.getValueOrDefault<PrimitiveIndex>("steps_radius", 10),
				param.getValueOrDefault<PrimitiveIndex>("steps_circ", 40)
			);

			computeTriangleNormals();
		}
		else if(const auto p = params.findParam("map"); p)
		{
			const auto &param = *p;

			clearAll();

			initMap(
				param.getValueOrDefault<Real>("sizeu", 1),
				param.getValueOrDefault<Real>("sizev", 1),
				param.getValueOrDefault<PrimitiveIndex>("nbu", 1),
				param.getValueOrDefault<PrimitiveIndex>("nbv", 1)
			);

			computeTriangleNormals();
		}

		if(const auto p = params.findParam("transform"); p)
		{
			auto transform = addReference(type_v<ITransform>, *p);
			transformVertices((*transform)());
			entityCollection->releaseEntityReference(*this, DynEntityCast<IRenderEntity>(transform));
		}

		if(const auto p = params.findParam("interpolate_normals"); p)
		{
			const Real smoothAmount = *p;
			computeSmoothVerticesNormals(pl::plcos(pl::degree_to_radian(smoothAmount)));
		}
	}

	ParameterTree PrimGroupTriangles::getParameters(bool deep) const
	{
		ParameterTree params {ThisBase::getParameters(deep)};

		return params;
	}

	void PrimGroupTriangles::serialize(std::ostream &outputStream) const
	{
		ThisBase::serialize(outputStream);

		auto &memSys = *getMemorySystem<memtag::Primitive>();

		if(!mData || (pl::io::is_ipc(outputStream) && memSys.is_shared()))
		{
			outputStream.put('\0');
			return;
		}

		std::array<char, 1024> buf;
		char *bend = buf.data() + buf.size();

		outputStream
			<< sizeof(PrimitiveIndex) << '\n'
			<< sizeof(Real) << '\n';

		const auto writeFc =
			[&](auto &&os)
			{
				const bool is_binary = pl::io::is_binary(os);

				const auto writeVts =
					[&](const auto &v)
					{
						char *cur = buf.begin();

						for(const auto i : mir(v.size()))
						{
							auto res = std::to_chars(cur, bend, v[i], std::chars_format::hex);
							if(res.ec != std::errc{})
								throw Exception{fmt::format("Failed writing vertex value: {}", std::make_error_condition(res.ec).message())};

							cur = res.ptr;
							if(i - 1 != v.size())
								*cur++ = ' ';
						}

						os << std::string_view{buf.data(), cur};
					};

				//const auto hexFloatTransform = [](auto&& v){ return pl::io::write_hex_float{v}; };

				os << std::dec << std::hexfloat
					<< pl::io::range{pl::io::mode_select, '\n',
						std::make_tuple(
							// Vertices
							pl::io::container(mData->primitives.mVts, pl::io::mode_select, true,
								[&](pl::io::dir_out_t, auto&& fc, auto&& v){
									if(is_binary)
										PL_FWD(fc)(pl::io::range{pl::io::mode_select, ' ', rgv::all(v)});
									else
										writeVts(v);
								}
							),

							// Triangles
							pl::io::container(mData->primitives.mTriangles, pl::io::mode_select, true,
								[](pl::io::dir_out_t, auto&& fc, auto&& v){ PL_FWD(fc)(pl::io::range{pl::io::mode_select, ' ', rgv::all(v)}); }),

							// Vertices normal
							pl::io::container(mData->primitives.mVtsNormals, pl::io::mode_select, true,
								[&](pl::io::dir_out_t, auto&& fc, auto&& v){
									if(is_binary)
										PL_FWD(fc)(pl::io::range{pl::io::mode_select, ' ', rgv::all(v)});
									else
										writeVts(v);
								}
							)
						)
					}
					<< '\n';

				// UV mappings
				serializeUVmaps(os);
			};

		//pl::io::write_stream(outputStream, Compressor{}, writeFc, headers.mCompressed);
		pl::io::stream_safe_from(outputStream, [&](auto &&os){ writeFc(PL_FWD(os)); });
	}

	void PrimGroupTriangles::unserialize(std::istream &is)
	{
		ThisBase::unserialize(is);

		auto &memSys = *getMemorySystem<memtag::Primitive>();

		if(!mData || (pl::io::is_ipc(is) && memSys.is_shared()))
		{
			is >> pl::io::skip_one;
			return;
		}

		clearAll();

		const bool is_binary = pl::io::is_binary(is);
		const bool is64Index = std::invoke([&]{
				const auto line = pl::io::readline(is);
				const bool is64 = line == "8";

				if(!is64 && line != "4")
					throw Exception("Unsupported index size of " + line + " (4 or 8 bytes supported).");

				return is64;
			});
		const bool isDouble = std::invoke([&]{
				const auto line = pl::io::readline(is);
				const bool is64 = line == "8";

				if(!is64 && line != "4")
					throw Exception("Unsupported floating-point size of " + line + " (4 or 8 bytes supported).");

				return is64;
			});

		if( (isDouble ? sizeof(double) : sizeof(float)) != sizeof(VT::value_type) )
			log() << "Floating-point format is different and will be translated.";

		if( (is64Index ? sizeof(u64) : sizeof(u32)) != sizeof(PrimitiveIndex) )
			log() << "Index format is different and will be translated.";

		std::array<char, 1024> buf;
		const char *bstart, *bend = buf.data() + buf.size();

		const auto readTextReal =
			[&](auto type)
			{
				using T = decltype(+type)::type;

				T val;
				const auto res = std::from_chars(bstart, bend, val, std::chars_format::hex);

				if(res.ptr == bstart)
					throw Exception(fmt::format("Failed to read floating point value for triangle data: {}", std::make_error_condition(res.ec).message()));

				if(!pl::fits_in(val, type_v<Real>))
					throw Exception("Floating-point value too large to be converted.");

				bstart = res.ptr + 1;

				return static_cast<Real>(val);

				/*char *nstart;
				const auto res = std::invoke([&]{
						if constexpr(std::same_as<T, float>)
							return std::strtof(bstart, &nstart);
						else
							return std::strtod(bstart, &nstart);
					});

				if(nstart == bstart)
					throw Exception("Failed to read floating point value for triangle data.");

				if(!pl::fits_in(res, type_v<Real>))
					throw Exception("Floating-point value too large to be converted.");

				bstart = nstart;
				return static_cast<Real>(res);*/
			};

		const auto testIndexLimit =
			[](const auto v)
			{
				if(!pl::fits_in(v, type_v<PrimitiveIndex>))
					throw Exception("Index too large to be stored.");

				return (PrimitiveIndex)v;
			};

		const auto readTextIndex =
			[&]
			{
				char *nstart;
				PrimitiveIndex res;

				if(is64Index)
					res = testIndexLimit(std::strtoull(bstart, &nstart, 10));
				else
					res = testIndexLimit(std::strtoul(bstart, &nstart, 10));

				if(nstart == bstart)
					throw Exception("Failed to read index value for triangle data.");

				bstart = nstart;
				return res;
			};

		const auto readImpl =
			[&](auto &tg, auto &is)
			{
				const auto scanVts =
					[&](auto &v)
					{
						const auto impl = [&](auto type)
							{
								using T = decltype(+type)::type;
								if(is_binary)
								{
									pl::simd::vector_init_loop(v, [&](auto&&)
									{
										using namespace pl::io::rawio;
										T res;
										is >= res;

										return r_(res);
									});
								}
								else
								{
									if(!is.getline(buf.data(), buf.size()))
										throw Exception("Failed to read vertex for triangle data.");

									bstart = buf.data();
									pl::simd::vector_init_loop(v, [&](auto&&){ return readTextReal(type); });
								}
							};

						if(isDouble)
							impl(type_v<double>);
						else
							impl(type_v<float>);
					};

				const auto scanIndex =
					[&](auto &v)
					{
						if(is_binary)
						{
							using namespace pl::io::rawio;

							if(is64Index)
							{
								for(auto &i : v)
								{
									u64 tv; is >= tv;
									i = testIndexLimit(tv);
								}
							}
							else
							{
								for(auto &i : v)
								{
									u32 tv; is >= tv;
									i = testIndexLimit(tv);
								}
							}
						}
						else
						{
							if(!is.getline(buf.data(), buf.size()))
								throw Exception("Failed to read vertex for triangle data.");

							bstart = buf.data();
							for(auto &i : v)
								i = readTextIndex();
						}
					};

				is >> std::hexfloat >>
					pl::io::range{pl::io::mode_select, '\n', std::make_tuple(
						// Vertices
						pl::io::container(mData->primitives.mVts, pl::io::mode_select, (isDouble ? sizeof(double) : sizeof(float)) == sizeof(VT::value_type), [&](pl::io::dir_in_t, auto&& fc){
							VT v{};
							scanVts(v);
							PL_FWD(fc)(v);
						}),

						// Triangles
						pl::io::container(mData->primitives.mTriangles, pl::io::mode_select, (is64Index ? sizeof(u64) : sizeof(u32)) == sizeof(PrimitiveIndex), [&](pl::io::dir_in_t, auto&& fc){
							std::array<PrimitiveIndex, 3> v{};
							scanIndex(v);
							PL_FWD(fc)(v);
						}),

						// Vertices normal
						pl::io::container(mData->primitives.mVtsNormals, pl::io::mode_select, (isDouble ? sizeof(double) : sizeof(float)) == sizeof(NT::value_type),
							[&](pl::io::dir_in_t, auto&& fc){
								NT v{};
								scanVts(v);
								PL_FWD(fc)(v);
							},
							[&](const auto){ tg.run([this]{ computeTriangleNormals(); }); }
						)
					)};

				// Skip trailing \n only in binary mode since is.getline already extracted it in text mode.
				if(is_binary)
					is >> pl::io::skip_one;

				// UV mappings
				unserializeUVmaps(is, isDouble);

				if(!trianglesValid())
					log(pl::logtag::error) << "Some triangles are invalid.";

				if(!verticesValid())
					log(pl::logtag::error) << "Some vertices are invalid.";
			};

		pl::tasks.task_group([&](auto &tg)
			{
				pl::io::stream_safe_from(is, [&](auto &is){ readImpl(tg, is); });
			});

		if(!checkAndFixVertexNormals())
			log(pl::logtag::error) << "Some vertex normals are invalid.";
	}

	void PrimGroupTriangles::initCube(Real sizex, Real sizey, Real sizez)
	{
		initCube(-sizex / 2, -sizey / 2, -sizez / 2, sizex / 2, sizey / 2, sizez / 2);
	}

	void PrimGroupTriangles::initCube(Real posx1, Real posy1, Real posz1, Real posx2, Real posy2, Real posz2)
	{
		auto &vts = mData->primitives.mVts;
		auto &tris = mData->primitives.mTriangles;
		const auto vi = vts.size();
		const auto ti = addTriangle(12);
		addVertex(8);

		vts[vi].set_all(posx1, posy1, posz1);
		vts[vi+1].set_all(posx1, posy1, posz2);
		vts[vi+2].set_all(posx2, posy1, posz2);
		vts[vi+3].set_all(posx2, posy1, posz1);
		vts[vi+4].set_all(posx1, posy2, posz1);
		vts[vi+5].set_all(posx1, posy2, posz2);
		vts[vi+6].set_all(posx2, posy2, posz2);
		vts[vi+7].set_all(posx2, posy2, posz1);

		// top face
		tris[ti][0] = vi + 2;
		tris[ti][1] = vi + 1;
		tris[ti][2] = vi + 3;

		tris[ti+1][0] = vi + 0;
		tris[ti+1][1] = vi + 3;
		tris[ti+1][2] = vi + 1;

		// front face
		tris[ti+2][0] = vi + 3;
		tris[ti+2][1] = vi + 0;
		tris[ti+2][2] = vi + 7;

		tris[ti+3][0] = vi + 4;
		tris[ti+3][1] = vi + 7;
		tris[ti+3][2] = vi + 0;

		// bottom face
		tris[ti+4][0] = vi + 7;
		tris[ti+4][1] = vi + 4;
		tris[ti+4][2] = vi + 6;

		tris[ti+5][0] = vi + 5;
		tris[ti+5][1] = vi + 6;
		tris[ti+5][2] = vi + 4;

		// back face
		tris[ti+6][0] = vi + 1;
		tris[ti+6][1] = vi + 2;
		tris[ti+6][2] = vi + 5;

		tris[ti+7][0] = vi + 6;
		tris[ti+7][1] = vi + 5;
		tris[ti+7][2] = vi + 2;

		// left face
		tris[ti+8][0] = vi + 0;
		tris[ti+8][1] = vi + 1;
		tris[ti+8][2] = vi + 4;

		tris[ti+9][0] = vi + 5;
		tris[ti+9][1] = vi + 4;
		tris[ti+9][2] = vi + 1;

		// right face
		tris[ti+10][0] = vi + 2;
		tris[ti+10][1] = vi + 3;
		tris[ti+10][2] = vi + 6;

		tris[ti+11][0] = vi + 7;
		tris[ti+11][1] = vi + 6;
		tris[ti+11][2] = vi + 3;

	}

	void PrimGroupTriangles::invertNormals() noexcept
	{
		for(auto &n : mData->primitives.mVtsNormals)
			n = -n;
	}

	void PrimGroupTriangles::clearVertices() noexcept
	{
		mData->primitives.mVts.clear();
		mData->primitives.mVts.shrink_to_fit();
	}

	void PrimGroupTriangles::clearTriangles() noexcept
	{
		mData->primitives.mTriangles.clear();
		mData->primitives.mTriangles.shrink_to_fit();
	}

	void PrimGroupTriangles::clearNormals() noexcept
	{
		mData->primitives.mVtsNormals.clear();
		mData->primitives.mVtsNormals.shrink_to_fit();
	}

	void PrimGroupTriangles::clearAll() noexcept
	{
		mData->primitives.clear();
	}

	void PrimGroupTriangles::initMap(Real tilesizex, Real tilesizez, PrimitiveIndex nbTileX, PrimitiveIndex nbTileZ)
	{
		const PrimitiveIndex nbvx = nbTileX+1, nbvz=nbTileZ+1, vIndexStart = mData->primitives.mVts.size();

		addVertex(nbvx * nbvz);

		for(auto vIt = mData->primitives.mVts.begin(); const auto i : mir(nbvz))
		{
			for(const auto c : mir(nbvx))
			{
				vIt->set_all(c * tilesizex, 0, i * tilesizez);
				++vIt;
			}
		}

		const auto tIndex = addTriangle(nbTileX * nbTileZ * 2);
		auto tIt = mData->primitives.mTriangles.begin() + tIndex;

		auto vIndex = vIndexStart;
		pl::repeat(nbTileZ, [&]
		{
			for(const auto c : mir(nbTileX))
			{
				(*tIt)[0] = vIndex;
				(*tIt)[1] = vIndex + nbvx;
				(*tIt)[2] = vIndex + nbvx + 1;

				++tIt;

				(*tIt)[0] = vIndex + nbvx + 1;
				(*tIt)[1] = vIndex + 1;
				(*tIt)[2] = vIndex;

				++tIt;
				++vIndex;
				if(c == nbTileX-1)
					++vIndex;
			}
		});
	}

	void PrimGroupTriangles::initSphere(Real sizex, Real sizey, Real sizez, PrimitiveIndex nbStepsRad, PrimitiveIndex nbStepsCirc)
	{
		auto &vts = mData->primitives.mVts;
		auto &tris = mData->primitives.mTriangles;

		Real cury, cura, xrad, yrad, zrad, incay, cdistx, cdistz, curacir, incacir;
		PrimitiveIndex startIndexVertex = vts.size(), spvCount;
		PrimitiveListType::VertexList::iterator cvt;
		PrimitiveIndex ctri, ltri;

		xrad = sizex/2;
		yrad = sizey/2;
		zrad = sizez/2;
		cury = -yrad;
		cura = 0;
		incay = std::numbers::pi_v<Real> / (r_(nbStepsRad)*2);
		incacir = (std::numbers::pi_v<Real> * 2) / r_(nbStepsCirc);

		cvt = addVertex();
		cvt->set_all(0, cury, 0);

		cura += incay;

		for(PrimitiveIndex i=0; i<nbStepsRad*2-1; ++i)
		{
			const auto curasin = cura /osin;
			cdistx = curasin * xrad;
			cdistz = curasin * zrad;
			cury = cura /ocos * -yrad;
			curacir = 0;
			for(PrimitiveIndex c=0; c<nbStepsCirc; c++)
			{
				cvt = addVertex();
				cvt->set_all(curacir /ocos * cdistx, cury, curacir /osin * cdistz);
				curacir += incacir;
			}
			cura += incay;
		}

		cvt = addVertex();
		cvt->set_all(0.0, yrad, 0.0);

		spvCount = vts.size() - startIndexVertex;

		for(PrimitiveIndex i=0; i<nbStepsCirc-1; ++i)
		{
			ctri = addTriangle();
			tris[ctri][0] = startIndexVertex + i+2;
			tris[ctri][1] = startIndexVertex;
			tris[ctri][2] = startIndexVertex + i+1;
		}
		ctri = addTriangle();
		tris[ctri][0] = startIndexVertex + 1;
		tris[ctri][1] = startIndexVertex;
		tris[ctri][2] = startIndexVertex + nbStepsCirc;

		for(PrimitiveIndex c=0; c<nbStepsRad*2-2; c++)
		{
			for(PrimitiveIndex i=0; i<nbStepsCirc-1; ++i)
			{
				ctri = addTriangle();
				tris[ctri][0] = startIndexVertex + 1+c*nbStepsCirc+i;
				tris[ctri][1] = startIndexVertex + 2+(c+1)*nbStepsCirc+i;
				tris[ctri][2] = startIndexVertex + 2+c*nbStepsCirc+i;

				ctri = addTriangle();
				ltri = ctri - 1;
				tris[ctri][0] = startIndexVertex + 1+(c+1)*nbStepsCirc+i;
				tris[ctri][1] = tris[ltri][1];
				tris[ctri][2] = tris[ltri][0];
			}

			ctri = addTriangle();
			tris[ctri][0] = startIndexVertex + (c+1)*nbStepsCirc;
			tris[ctri][1] = startIndexVertex + 1+(c+1)*nbStepsCirc;
			tris[ctri][2] = startIndexVertex + 1+c*nbStepsCirc;

			ctri = addTriangle();
			ltri = ctri - 1;
			tris[ctri][0] = startIndexVertex + (c+2)*nbStepsCirc;
			tris[ctri][1] = tris[ltri][1];
			tris[ctri][2] = tris[ltri][0];

		}

		for(PrimitiveIndex i=0; i<nbStepsCirc-1; ++i)
		{
			ctri = addTriangle();
			tris[ctri][0] = startIndexVertex + spvCount-i-3;
			tris[ctri][1] = vts.size() - 1;
			tris[ctri][2] = startIndexVertex + spvCount-i-2;
		}
		ctri = addTriangle();
		tris[ctri][0] = startIndexVertex + spvCount-2;
		tris[ctri][1] = vts.size() - 1;
		tris[ctri][2] = startIndexVertex + spvCount-nbStepsCirc-1;
	}

	PrimitiveIndex PrimGroupTriangles::addTriangle(PrimitiveIndex number)
	{
		const auto start = static_cast<PrimitiveIndex>(mData->primitives.mTriangles.size());
		mData->primitives.mTriangles.resize(mData->primitives.mTriangles.size() + number);

		return start;
	}

	void PrimGroupTriangles::removeVertices(PrimitiveIndex number)
	{
		mData->primitives.mVts.resize(mData->primitives.mVts.size() - number);
	}

	void PrimGroupTriangles::removeTriangles(PrimitiveIndex number)
	{
		mData->primitives.mTriangles.resize(mData->primitives.mTriangles.size() - number);
	}

	void PrimGroupTriangles::removeNormals(PrimitiveIndex number)
	{
		mData->primitives.mVtsNormals.resize(mData->primitives.mVtsNormals.size() - number);
	}

	void PrimGroupTriangles::moveVertices(const VecD &av)
	{
		VT av2{av};

		for(auto &v : mData->primitives.mVts)
			v += av2;
	}

	void PrimGroupTriangles::transformVertices(const Matrix &m)
	{
		pl::tasks.sub_tasks(
			[&]{ mData->primitives.mVts | pl::transform(mData->primitives.mVts.begin(), [&](auto &&v){ return m * pl::normalize_point_p{v}; }, std::identity{}, pl::policy::parallel_taskqueue_t{}.chunk_size(1024*128)); },
			[&]{ mData->primitives.mVtsNormals | pl::transform(mData->primitives.mVtsNormals.begin(), [&](auto &&v){ return dirSafeTransform(v, m); }, std::identity{}, pl::policy::parallel_taskqueue_t{}.chunk_size(1024*128)); },
			[&]{ mData->primitives.mTriangleNormals | pl::for_each([&](auto &&n){ n = dirSafeTransform(n, m); }, std::identity{}, pl::policy::parallel_taskqueue_t{}.chunk_size(1024*128)); },
			[&]
			{
				mData->uvMaps | pl::for_each([&](auto &&uvmap){
					uvmap.tangents | pl::for_each([&](auto &&t){
						t[0] = m * t[0];
						t[1] = m * t[1];
					}, std::identity{}, pl::policy::parallel_taskqueue_t{}.chunk_size(1024*128));
				}, std::identity{}, pl::policy::parallel_taskqueue_one);
			}
		);
	}

	void PrimGroupTriangles::buildVtsTriIndex()
	{
		std::vector<PrimitiveIndex> vtsTriCount(mData->primitives.mVts.size(), 0);

		for(const auto &t : mData->primitives.mTriangles)
		{
			++vtsTriCount[t[0]];
			++vtsTriCount[t[1]];
			++vtsTriCount[t[2]];
		}

		mVtsTriIndexStart.reserve(mData->primitives.mVts.size());

		PrimitiveIndex total = 0;
		for(const auto i : vtsTriCount)
		{
			mVtsTriIndexStart.push_back(total);
			total += i;
		}

		vtsTriCount.assign(vtsTriCount.size(), 0);
		mVtsTriIndex.resize(total);

		for(const auto i : mir(mData->primitives.mTriangles.size()))
		{
			const auto &t = mData->primitives.mTriangles[i];
			for(PrimitiveIndex c=0; c<3; ++c)
			{
				mVtsTriIndex[mVtsTriIndexStart[t[c]] + vtsTriCount[t[c]]] = i;
				++vtsTriCount[t[c]];
			}
		}
	}

	void PrimGroupTriangles::clearVtsTriIndex() noexcept
	{
		mVtsTriIndexStart.clear();
		mVtsTriIndexStart.shrink_to_fit();
		mVtsTriIndex.clear();
		mVtsTriIndex.shrink_to_fit();
	}

	void PrimGroupTriangles::removeDuplicateVertices()
	{
		PrimitiveIndex curCount = mData->primitives.mVts.size();

		std::vector<PrimitiveIndex> oldToNewIndex(curCount), swappedIndex(curCount);

		for(const auto i : mir(curCount))
		{
			oldToNewIndex[i] = i;
			swappedIndex[i] = i;
		}

		const auto swapVertex = [this](PrimitiveIndex index1, PrimitiveIndex index2)
			{
				swap(mData->primitives.mVts[index1], mData->primitives.mVts[index2]);

				if(!mData->primitives.mVtsNormals.empty())
					swap(mData->primitives.mVtsNormals[index1], mData->primitives.mVtsNormals[index2]);

				for(auto &uvmap : mData->uvMaps)
				{
					if(!uvmap.uv.empty())
						swap(uvmap.uv[index1], uvmap.uv[index2]);
				}
			};

		for(const auto i : mir(curCount))
		{
			for(PrimitiveIndex c=i+1; c<curCount; ++c)
			{
				if( pl::mask_all((mData->primitives.mVts[i] - mData->primitives.mVts[c]) /oabs <= constants::epsilon<Real>) )
				{
					--curCount;
					oldToNewIndex[swappedIndex[c]] = i;

					if(c != curCount)
					{
						oldToNewIndex[swappedIndex[curCount]] = c;

						swapVertex(c, curCount);
					}

					swappedIndex[c] = swappedIndex[curCount];

					--c;
				}
			}
		}

		mData->primitives.mVts.resize(curCount);
		mData->primitives.mVts.shrink_to_fit();

		if(!mData->primitives.mVtsNormals.empty())
		{
			mData->primitives.mVtsNormals.resize(curCount);
			mData->primitives.mVtsNormals.shrink_to_fit();
		}

		for(auto &uvmap : mData->uvMaps)
		{
			if(!uvmap.uv.empty())
			{
				uvmap.uv.resize(curCount);
				uvmap.uv.shrink_to_fit();
			}
		}

		for(auto &t : mData->primitives.mTriangles)
		{
			for(auto &vi : t)
			{
				plassert(oldToNewIndex[vi] < mData->primitives.mVts.size());
				vi = oldToNewIndex[vi];
			}
		}
	}

	void PrimGroupTriangles::findConnectedTriangles(TriOrientationData &data)
	{
		const auto edgeCheck = [&data, this] (PrimitiveIndex edgeNo, PrimitiveIndex i1, PrimitiveIndex i2)
			{
				PrimitiveIndex pIndex = 0, vi2 = this->mData->primitives.mTriangles[data.index][i2];

				for(const auto &ti1 : this->mData->primitives.mTriangles)
				{
					for(PrimitiveIndex c=0; c<3; ++c)
					{
						if((pIndex != data.index || c != i1) && this->mData->primitives.mTriangles[data.index][i1] == ti1[c])
						{
							for(PrimitiveIndex i=0; i<3; ++i)
							{
								if(ti1[i] == vi2)
								{
									data.connected[edgeNo] = pIndex;
									data.connIndex[edgeNo][0] = c;
									data.connIndex[edgeNo][1] = i;
									return;
								}
							}
						}
					}

					++pIndex;
				}

			};

		data.connected.fill(constants::noIndex<PrimitiveIndex>);

		edgeCheck(0, 0, 1);
		edgeCheck(1, 1, 2);
		edgeCheck(2, 2, 0);

	}

	void PrimGroupTriangles::swapTrianglesOrientation() noexcept
	{
		pl::tasks.sub_tasks(
			[&]
			{
				mData->primitives.mTriangles |
					pl::for_each
					(
						[](auto &&t){ pl::adl_swap(t[1], t[2]); },
						std::identity{}, pl::policy::parallel_taskqueue_t{}.chunk_size(1024*128)
					);
			},
			[&]
			{
				mData->primitives.mTriangleNormals |
					pl::for_each
					(
						[](auto &&n){ n = -n; },
						std::identity{}, pl::policy::parallel_taskqueue_t{}.chunk_size(1024*128)
					);
			},
			[&]
			{
				mData->primitives.mVtsNormals |
					pl::for_each
					(
						[](auto &&n){ n = -n; },
						std::identity{}, pl::policy::parallel_taskqueue_t{}.chunk_size(1024*128)
					);
			}
		);
	}

	void PrimGroupTriangles::orientTriangles(PrimitiveIndex refTriIndex) noexcept
	{
		VT bc1, bc2;
		std::vector<TriOrientationData> triOriData;
		TriOrientationData *curData, *lastData;
		PrimitiveIndex lastTri, nextIndex;
		VT *v1, *v2;
		boost::dynamic_bitset checked(getNbPrimitives(), 0);

		auto &vts = mData->primitives.mVts;
		auto &tris = mData->primitives.mTriangles;

		checked[refTriIndex] = true;

		triOriData.reserve(getNbPrimitives());

		triOriData.emplace_back();

		curData = &triOriData.back();
		curData->index = refTriIndex;
		curData->checkIndex = -1;

		while(true)
		{
			++curData->checkIndex;
			if(curData->checkIndex == 0)
				findConnectedTriangles(*curData);

			if(curData->checkIndex > 2)
			{
				triOriData.pop_back();
				if(triOriData.empty())
					break;
				curData = &triOriData.back();
				continue;
			}

			if(curData->connected[(int)curData->checkIndex] == constants::noIndex<PrimitiveIndex> || checked[curData->connected[(int)curData->checkIndex]])
				continue;

			triOriData.emplace_back();

			lastData = &triOriData[triOriData.size() - 2];
			curData = &triOriData.back();
			lastTri = lastData->index;
			curData->index = lastData->connected[(int)lastData->checkIndex];
			curData->checkIndex = -1;

			nextIndex = (lastData->checkIndex + 1) % 3;

			v1 = &vts[tris[lastTri][(int)lastData->checkIndex]];
			v2 = &vts[tris[lastTri][nextIndex]];
			bc1 = *v1 - *v2;

			v1 = &vts[tris[curData->index][lastData->connIndex[(int)lastData->checkIndex][0]]];
			v2 = &vts[tris[curData->index][lastData->connIndex[(int)lastData->checkIndex][1]]];
			bc2 = *v1 - *v2;

			if(bc1 /odot/ bc2 > 0)
				pl::adl_swap(tris[curData->index][lastData->connIndex[(int)lastData->checkIndex][0]], tris[curData->index][lastData->connIndex[(int)lastData->checkIndex][1]]);

			checked[curData->index] = true;

		}

	}

	void PrimGroupTriangles::reorientNormals() noexcept
	{
		mir(mData->primitives.mTriangles.size()) |
			pl::for_each
			(
				[this](const auto ti)
				{
					const auto &tri = mData->primitives.mTriangles[ti];
					const auto &tn = mData->primitives.mTriangleNormals[ti];

					for(const auto i : mir(3))
					{
						auto &n = mData->primitives.mVtsNormals[tri[i]];

						if(n /odot/ tn < 0)
							n = -n;
					}
				},
				std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
			);
	}

	void PrimGroupTriangles::computeSmoothVerticesNormals(Real maxdist)
	{
		struct AttTri
		{
			PrimitiveIndex tIndex;
			u8 vIndex;
			bool done;
		};

		VectorNoInit<AttTri> attTris;
		const auto nbVts = mData->primitives.mVts.size();

		clearNormals();
		addNormal(nbVts);

		attTris.reserve(1024*16);

		for(const auto t : mir(nbVts))
		{
			attTris.clear();

			const auto findAttached =
				rgv::for_each([&](const auto ti){
					return rg::yield_from( pl::find_view(std::as_const(mData->primitives.mTriangles[ti]), t, ti) );
				})
				| rgv::transform(pl::applier([&](const auto &t, auto it, const auto ti){
					return AttTri{ti, (u8)std::distance(t.begin(), it), false};
				}));

			if(!mVtsTriIndex.empty())
			{
				auto r = vtsTriIndexRange(*this, t);

				attTris.resize(r.size());

				r
					| findAttached
					| pl::copy(attTris.begin());
			}
			else
			{
				mir((PrimitiveIndex)mData->primitives.mTriangles.size())
					| findAttached
					| pl::back_insert_copy(attTris);
			}

			if(!attTris.empty())
			{
				PrimitiveIndex vCount = 0;

				attTris
					| rgv::remove_if([](const auto &e){ return e.done; })

					| pl::for_each([&](auto &at1)
					{
						PrimitiveIndex nCount = 1;
						VecD normal = mData->primitives.mTriangleNormals[at1.tIndex];
						at1.done = true;

						if(vCount)
							mData->primitives.mTriangles[at1.tIndex][at1.vIndex] = (PrimitiveIndex)mData->primitives.mVts.size();

						attTris
							| rgv::remove_if([&](const auto &e){
								return e.done ||
									mData->primitives.mTriangleNormals[at1.tIndex] /odot/ mData->primitives.mTriangleNormals[e.tIndex] < maxdist;
							})

							| pl::for_each([&](auto &at2)
							{
								++nCount;
								normal += mData->primitives.mTriangleNormals[at2.tIndex];
								at2.done = true;

								if(vCount)
									mData->primitives.mTriangles[at2.tIndex][at2.vIndex] = (PrimitiveIndex)mData->primitives.mVts.size();
							});

						normal = (normal / nCount) /oprtnormalize;

						if(!pl::is_valid(normal))
							normal = mData->primitives.mTriangleNormals[at1.tIndex];
						else if(normal /odot/ mData->primitives.mTriangleNormals[at1.tIndex] < 0)
							normal = -normal;

						if(!vCount)
							mData->primitives.mVtsNormals[t] = normal;
						else
						{
							const auto vcp = mData->primitives.mVts[t];
							*addVertex() = vcp;
							*addNormal() = normal;

							mData->uvMaps
								| rgv::remove_if([](const auto &e){ return e.uv.empty(); })
								| pl::for_each([&](auto &uvm){
									uvm.uv.push_back(auto(uvm.uv[t]));
								});
						}

						++vCount;
					});
			}
		}

	}

	void PrimGroupTriangles::clearUVs() noexcept
	{
		mData->uvMaps.clear();
		mData->uvMaps.shrink_to_fit();
	}

	Box PrimGroupTriangles::computeAABox() noexcept
	{
		const auto &data = *pl::assert_valid(mData);

		mAabox = {pl::tag::range_init,
				triangleVerticesRange(*this)
				| rgv::transform(pl::applier([&](const auto &t, const auto, const auto vi){
					return data.primitives.mVts[t[vi]];
				}))
			};

		return pl::assert_valid(mAabox);
	}

	bool PrimGroupTriangles::computeTriangleNormals(bool removeIfInvalid)
	{
		bool valid = true;

		mData->primitives.mTriangleNormals.clear();
		mData->primitives.mTriangleNormals.shrink_to_fit();
		mData->primitives.mTriangleNormals.resize(mData->primitives.mTriangles.size());

		mir((PrimitiveIndex)mData->primitives.mTriangleNormals.size()) |
			pl::for_each
			(
				[this](const auto i) { mData->primitives.mTriangleNormals[i] = getTriangle(i).getSurfaceNormal({}); },
				std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
			);

		if(removeIfInvalid)
		{
			const auto oldSize = mData->primitives.mTriangles.size();

			auto itn = mData->primitives.mTriangleNormals.begin();
			auto itnr = mData->primitives.mTriangleNormals.rbegin();
			auto itr = mData->primitives.mTriangles.rbegin();
			for(auto it = mData->primitives.mTriangles.begin(), end = mData->primitives.mTriangles.end(); it!=end; ++it, ++itn)
			{
				if(!pl::is_valid(*itn))
				{
					std::iter_swap(itn, itnr++);
					std::iter_swap(it, itr++);
				}
			}

			mData->primitives.mTriangles.erase(itr.base(), mData->primitives.mTriangles.end());
			mData->primitives.mTriangleNormals.erase(itnr.base(), mData->primitives.mTriangleNormals.end());

			/*mData->primitives.mTriangleNormals = mData->primitives.mTriangleNormals
				| rg::move
				| rg::actions::remove_if([](const auto &t){ return !pl::is_valid(t); });*/

			if(oldSize != mData->primitives.mTriangles.size())
				valid = false;
		}

		return valid;
	}

	bool PrimGroupTriangles::verticesValid() const noexcept
	{
		return rg::all_of(mData->primitives.mVts, [](const auto &v){
			return pl::is_valid(v);
		});
	}

	bool PrimGroupTriangles::trianglesValid() const noexcept
	{
		return rg::all_of(triangleVerticesRange(*this), pl::applier(
			[nbVts = mData->primitives.mVts.size()]
			(const auto &t, const auto, const auto vi)
				{ return t[vi] != constants::noIndex<PrimitiveIndex> && t[vi] < nbVts; }
		));
	}

	bool PrimGroupTriangles::vertexNormalsValid() const noexcept
	{
		return rg::all_of(triangleVerticesRangeWithPrimitive(*this), pl::applier([&](const auto &, const auto &p, const auto, const auto vi){
			return triangleVertexValid(p, vi);
		}));
	}

	bool PrimGroupTriangles::checkAndFixVertexNormals()
	{
		bool valid = true;

		if(mData->primitives.mVtsNormals.empty())
			return true;

		triangleVerticesRangeWithPrimitive(*this)
			| rgv::remove_if(pl::applier([&](const auto &, const auto &p, const auto, const auto vi){
				return triangleVertexValid(p, vi);
			}))
			| pl::for_each(pl::applier([&](auto &t, const auto &p, const auto, const auto vi)
			{
				valid = false;

				auto vind = t[vi];
				auto vCopy = mData->primitives.mVts[vind];
				mData->primitives.mVtsNormals.push_back(p.mNormal);
				mData->primitives.mVts.push_back(vCopy);
				t[vi] = mData->primitives.mVts.size() - 1;

				mData->uvMaps
					| rgv::remove_if([](const auto &uvm){ return uvm.uv.empty(); })
					| pl::for_each([&](auto &uvm){
						uvm.uv.push_back(auto(uvm.uv[vind]));
					});
			}));

		if(!valid)
		{
			mData->primitives.mVtsNormals.shrink_to_fit();
			mData->primitives.mVts.shrink_to_fit();
		}

		return valid;
	}

	Matrix PrimGroupTriangles::getTextureTangentSpace(const SurfaceData &surfaceData, const UVMapping *tcMaps, const Matrix &texSpaceTransform) const noexcept
	{
		// Build primitive space -> texture space matrix
		const auto primSpaceToTexSpace = std::invoke([&]{
				if(!tcMaps)
					return texSpaceTransform;

				const auto &tri = mData->primitives.mTriangles[surfaceData.primIndex];
				const auto &uv1 = tcMaps->uv[tri[0]];
				const auto &uv2 = tcMaps->uv[tri[1]];
				const auto &uv3 = tcMaps->uv[tri[2]];

				const auto mat = Matrix{
						UVdir{uv2 - uv1},
						UVdir{uv3 - uv1},
						{0, 0, 1, 0},
						uv1
					}.transpose();

				pl::assert_valid(mat);

				return pl::assert_valid( mat * texSpaceTransform );
			});

		if(!tcMaps->tangents.empty())
		{
			const auto &tg = tcMaps->tangents[surfaceData.primIndex];
			Matrix tangentSpace{
				tg[0],
				tg[1],
				{0, 0, 1, 0},
				pl::tag::identity
			};

			tangentSpace = surfaceData.worldMatrix * tangentSpace.transpose();

			{
				const pl::scoped_transpose sg{tangentSpace};
				tangentSpace[3] = primSpaceToTexSpace * surfaceData.uv;
			}

			pl::assert_valid(surfaceData.uv);
			return pl::assert_valid(tangentSpace);
		}
		// Get texture space -> primitive space matrix
		else if(const auto texSpaceToPrimSpace = primSpaceToTexSpace.inverse(); texSpaceToPrimSpace)
		{
			// Map texture space -> primitive space -> object space to get tangents space
			auto tangentSpace = pl::assert_valid( *texSpaceToPrimSpace * surfaceData.pMat );

			tangentSpace.transpose_inplace();
			tangentSpace[3] = primSpaceToTexSpace * surfaceData.uv;
			tangentSpace.transpose_inplace();

			return pl::assert_valid(tangentSpace);
		}

		return {pl::tag::identity};
	}

	void PrimGroupTriangles::computeTangents(const Matrix &texSpaceTransform)
	{
		pl::assert_valid(mData)->uvMaps | pl::for_each([&](auto &uvmap)
		{
			uvmap.tangents.resize(uvmap.uv.empty() ? 0 : getNbPrimitives());
			uvmap.tangents.shrink_to_fit();

			if(uvmap.tangents.empty())
				return;

			const auto nb = uvmap.tangents.size();

			if(mData->primitives.mVts.size() != uvmap.uv.size())
				throw Exception("Vertices list size doesn't match UV map size in triangle group.");

			mir(nb) | pl::for_each([&](const auto i)
			{
				const auto &tri = mData->primitives.mTriangles[i];
				const auto p = getPrimitive(i);
				const auto &uv1 = uvmap.uv[tri[0]];
				const auto &uv2 = uvmap.uv[tri[1]];
				const auto &uv3 = uvmap.uv[tri[2]];

				const auto primSpaceToTexSpace = Matrix{
						UVdir{uv2 - uv1},
						UVdir{uv3 - uv1},
						{0, 0, 1, 0},
						uv1
					}.transpose() * texSpaceTransform;

				const auto maybeInverse = [&]() -> pl::marked_optional<Matrix> {
						if(bool(uv1 == uv2) || bool(uv1 == uv3))
							return pl::nullopt;

						return primSpaceToTexSpace.inverse();
					};

				uvmap.tangents[i] = maybeInverse()
					.transform([&](const auto &texSpaceToPrimSpace) -> UVDirPair
					{
						const auto tangentSpace = (texSpaceToPrimSpace * p.getTangentSpace({}, type_v<Matrix>)).transpose();

						if(!pl::is_valid(tangentSpace[0]) || !pl::is_valid(tangentSpace[1]))
						{
							if(const auto sdir = p.getSurfaceDir({}); pl::is_valid(sdir))
							{
								const Lobe<> lobe{sdir};
								return pl::assert_valid(UVDirPair{UVdir{lobe.lobeX}, UVdir{lobe.lobeY}});
							}
							else
								return {};
						}

						return {UVdir{tangentSpace[0]}, UVdir{tangentSpace[1]}};
					})
					.or_else([&]() -> pl::marked_optional<UVDirPair>
					{
						const auto sdir = p.getSurfaceDir({});
						if(!pl::is_valid(sdir))
							return pl::nullopt;

						const Lobe<> lobe{sdir};
						return pl::assert_valid(UVDirPair{UVdir{lobe.lobeX}, UVdir{lobe.lobeY}});
					})
					.value_or(pl::tag::zero);

			}, std::identity{}, pl::policy::parallel_taskqueue_t{}.chunk_size(1024*16) );

		}, std::identity{}, pl::policy::parallel_taskqueue_one );
	}

	szt PrimGroupTriangles::computeUVmapping(const IUVMapper &uvMapper)
	{
		auto &uvmap = pl::assert_valid(mData)->uvMaps.emplace_back();

		uvmap.uv.reserve(mData->primitives.mVts.size());
		for(auto&& [vit, vnit] : rgv::zip(mData->primitives.mVts, mData->primitives.mVtsNormals))
		{
			const auto uv = uvMapper.map(SurfaceInteraction{
					{
						.shadingNormal = vnit,
						.localPos = vit
					},
					{vnit}
				}, {}).transpose();

			uvmap.uv.push_back(uv[3]);
		}

		return mData->uvMaps.size() - 1;
	}

	void PrimGroupTriangles::alignToAxis(int axis, bool left) noexcept
	{
		const auto box = getAABox();
		Point3Dr tr{};

		if(left)
			tr.set(-box.v1[axis], axis);
		else
			tr.set(-box.v2[axis], axis);

		transformVertices(TransformTranslation{tr});
	}

	bool PrimGroupTriangles::uvsValid() const noexcept
	{
		auto r = rgv::for_each(rgv::all(mData->uvMaps),
			[&](const auto &uvm){
				return rg::yield_from(rgv::all(uvm.uv));
			}
		);

		return rg::all_of(r, [](auto&& uv){ return pl::is_valid(uv); });
	}
}
