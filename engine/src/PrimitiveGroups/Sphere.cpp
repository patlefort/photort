/*
	PhotoRT

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/PrimitiveGroup/Sphere.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_DEFINE(PrimGroupSpheres)

	void PrimGroupSpheres::getShadingInfo(SurfaceData &surfaceData, PrimitiveIndex primIndex) const noexcept
	{
		const auto p = getPrimitive(primIndex);

		surfaceData.shadingNormal = surfaceData.surfaceNormal;
		surfaceData.uv = CoordinatePosition{tag::cartesian, surfaceData.surfaceNormal}.to(tag::polar)->as_point();
		surfaceData.pMat = p.getTangentSpaceFromNormal<Matrix>(surfaceData.localPos, surfaceData.surfaceNormal);
	}
}
