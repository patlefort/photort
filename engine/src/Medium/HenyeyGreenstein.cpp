/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Medium/HenyeyGreenstein.hpp>
#include <PhotoRT/Core/Material.hpp>
#include <PhotoRT/Core/RayTracer.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

namespace PhotoRT
{
	// Mediums
	PRT_ENTITY_FACTORY_DEFINE(MediumHenyeyGreenstein)


	void MediumHenyeyGreenstein::setParameters(const ParameterTree &params)
	{
		MediumBase::setParameters(params);

		addParamReference(mSigmaS, params, "sigma_s");
		addParamReference(mSigmaA, params, "sigma_a");
		addParamReference(mG, params, "g");

		params.getValueIfExistsBind
			("scale", PL_LAMBDA_FORWARD_THIS(setScale))
			("maximum_distance", PL_LAMBDA_FORWARD_THIS(setMaximumDistance))
			("step_size", PL_LAMBDA_FORWARD_THIS(setStepSize))
		;
	}

	ParameterTree MediumHenyeyGreenstein::getParameters(bool deep) const
	{
		ParameterTree params {MediumBase::getParameters(deep)};

		if(mSigmaS)
			params["sigma_s"].fromEntityRef(*mSigmaS, deep);

		if(mSigmaA)
			params["sigma_a"].fromEntityRef(*mSigmaA, deep);

		if(mG)
			params["g"].fromEntityRef(*mG, deep);

		params.insert({
			{"scale", mScale},
			{"maximum_distance", mMaximumDistance},
			{"step_size", mStepSize}
		});

		return params;
	}

	[[nodiscard]] static constexpr Real HenyeyGreensteinPhase(Real cosTheta, Real G) noexcept
	{
		const Real G2 = G /opow2;
		const Real p = 1 + G2 + G * 2 * cosTheta;
		return pl::constants::sphere_pdf<Real> * (1 - G2) / (p * (p /oprtsqrt));
	}

	SampledMedium MediumHenyeyGreenstein::tryScatter(const Color sigmaA, const Color sigmaS, RandomReals<2> rnd, const MaterialInteraction &inter, const VecD odir, Real distance, IRNGInt &rng) const noexcept
	{
		SampledMedium sp{};

		const auto sigmaT = sigmaA + sigmaS;

		if(isBlack(sigmaT))
			return sp;

		const auto channel = uniformIndexRng<u32>(3)(rng);

		const auto channelSigma = sigmaT[channel];
		if(!channelSigma)
			return sp;

		Real dist = -pl::pllog(1 - uniformRealRng<>()(rng)) * (channelSigma /oprtreciprocal);
		sp.scatterDist = dist * mScale;
		const bool hasScattering = sp.scatterDist < distance;

		plassert(channel < 3);

		// distance can be infinite
		sp.scatterDist = pl::plmin({sp.scatterDist, mMaximumDistance, distance});
		dist = sp.scatterDist * mScaleInv /omin/ std::numeric_limits<Real>::max();

		const auto ph = phase(sigmaT, dist);
		Color density;

		if(hasScattering)
		{
			// Sample direction
			const Real
				G = getG(inter, odir),
				cosTheta = std::invoke([&]{
						if(G /oabs <= constants::epsilon<Real>)
							return 1 - rnd[0] * 2;

						const Real G2 = G /opow2;
						return (1 + G2 - pl::pow2((1 - G2) / (1 - G + G * 2 * rnd[0]))) / (G * 2);
					}),
				sinTheta = pl::plmax<Real>(0, 1 - cosTheta /opow2) /oprtsqrt,
				phi = 2 * std::numbers::pi_v<Real> * rnd[1];

			const Lobe<> lobe{odir};

			sp.dir = lobe.sphericalDirection(cosTheta, sinTheta, phi);
			sp.type = EnumRayType::Diffuse;
			sp.pdfDir = pl::assert_valid( HenyeyGreensteinPhase(-cosTheta, G) );

			density = sigmaT * ph;
			sp.color = pl::assert_valid( sigmaS * ph );
		}
		else
		{
			density = ph;
			sp.color = pl::assert_valid(ph);
		}

		density.set(0, size_v<3>);
		sp.pdfScatter = pl::one_if_lezero((density /ohadd)[size_v<0>] * (1.0 / 3.0));

		return sp;
	}

	SampledMedium MediumHenyeyGreenstein::sampleScattering(RandomReals<2> rnd, const MaterialInteraction &inter, const VecD odir, Real distance, IRNGInt &rng, EnumRayType typeMask) const noexcept
	{
		if(!isType(*this, typeMask))
			return {};

		if(mHomogeneous)
		{
			return tryScatter(getSigmaA(inter, odir), getSigmaS(inter, odir), rnd, inter, odir, distance, rng);
		}
		else
		{
			SampledMedium sp{};
			Real pdfScatter = 0;

			distance = distance /omin/ mMaximumDistance;

			sp.color = pl::assert_valid( walkMedium(inter, odir, distance, mStepSize, uniformRealRng<>()(rng),
				[&](const Color &sigmaA, const Color &sigmaS, const auto &i, const Real curDist, const Real totalDist, bool &doContinue)
				{
					sp = tryScatter(sigmaA, sigmaS, rnd, i, odir, totalDist, rng);

					pdfScatter += (1 - pdfScatter) * sp.pdfScatter;

					if(sp.pdfDir > 0)
					{
						sp.scatterDist += curDist - totalDist;
						doContinue = false;
					}

					return sp.color;
				}
			) );

			sp.pdfScatter = pdfScatter;

			return sp;
		}
	}

	Color MediumHenyeyGreenstein::transmittance(const MaterialInteraction &inter, const VecD odir, Real scatterDist, IRNGInt &rng) const noexcept
	{
		Color color;

		if(mHomogeneous)
		{
			const Color
				sigmaA{getSigmaA(inter, odir)},
				sigmaS{getSigmaS(inter, odir)},
				sigmaT{sigmaA + sigmaS};

			const Real dist = scatterDist /omin/ mMaximumDistance * mScaleInv /omin/ std::numeric_limits<Real>::max();
			color = pl::assert_valid( phase(sigmaT, dist) );
		}
		else
		{
			scatterDist = scatterDist /omin/ mMaximumDistance;

			color = pl::assert_valid( walkMedium(inter, odir, scatterDist, mStepSize, uniformRealRng<>()(rng),
				[&](const Color sigmaA, const Color sigmaS, const auto &/*i*/, Real /*curDist*/, const Real totalDist, bool &/*doContinue*/)
				{
					const auto sigmaT = sigmaA + sigmaS;
					const auto dist = totalDist * mScaleInv /omin/ std::numeric_limits<Real>::max();

					return phase(sigmaT, dist);
				}
			) );
		}

		return color;
	}

	OptionalPDF<Real> MediumHenyeyGreenstein::dirPDF(const MediumInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		const auto G = getG(inter, odir);
		return pl::assert_valid( HenyeyGreensteinPhase(-(odir /odot/ idir), G) );
	}
}
