/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/DistributionFactory.hpp>

namespace PhotoRT::DistFactoriesImpl
{
	struct Names
	{
		inline static constexpr char name[] = "floatmap";
		struct MultiName { inline static constexpr auto name = pl::concat_char_arrays(Names::name, "-multi"); };
		struct AdaptorName { inline static constexpr auto name = pl::concat_char_arrays(Names::name, "-adaptor"); };
		struct CompareName { inline static constexpr auto name = pl::concat_char_arrays(Names::name, "-compare"); };
	};

	static RegisterDistributionMappers<Real, Real, Names> prtffloatmap;
	static RegisterDistributionsCompare<Real, Real, Names::CompareName> prtffloatmapcompare;
	static RegisterIOdistributions<Real, Names> prtffloatmapio;
	static RegisterDistributionsAdaptor<Real, Real, Names::AdaptorName> prtffloatmapadaptor;
	static RegisterMultiDistributions<Real, Real, Names::MultiName> prtffloatmapmulti;

	class AugmentedDistributionMapperPerlin3DRealReal : public RenderEntityAugment<DistributionMapperPerlin3D<Real, Real>>
	{
	public:
		PRT_ENTITY_FACTORY_INLINE("floatmap-perlin3d", AugmentedDistributionMapperPerlin3DRealReal)

		virtual void setParameters(const ParameterTree &params) final
		{
			int tableSize = 1024;
			EntityPtr<IRNGInt> randomizer;

			params.getValueIfExists("table_size", tableSize);
			addParamReference(randomizer, params, "randomizer");

			if(randomizer && tableSize > 0)
				init(tableSize, randomizer);

			params.getValueIfExistsBind("strength", PL_LAMBDA_FORWARD_THIS(setStrength));
			params.getValueIfExistsBind("start", PL_LAMBDA_FORWARD_THIS(setStart));
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const final
		{
			ParameterTree params;

			params["table_size"] = mTabSize;
			if(mRandomizer)
				params["randomizer"].fromEntityRef(*mRandomizer, deep);
			params["strength"] = mStrength;
			params["start"] = mStart;

			return params;
		}
	};
}
