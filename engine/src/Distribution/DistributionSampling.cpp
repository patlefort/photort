/*
	PhotoRT

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <tuple>

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/DistributionSampling.hpp>

#include <patlib/rectangle.hpp>
#include <patlib/task.hpp>

namespace PhotoRT
{
	// Distribution samplers
	PRT_ENTITY_FACTORY_AUGMENT_INLINE("pdfsampler_point2dr-", AugmentedPDFSamplerPoint2Dr, PDFSamplerPoint2Dr)


	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("pdfsampler_point2dr-dist_importance_tiled", AugmentedPDFSamplerColorDistImportanceTiled, PDFSamplerColorDistImportanceTiled)
	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedPDFSamplerColorDistImportanceTiled)
	void AugmentedPDFSamplerColorDistImportanceTiled::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		params.getValueIfExistsBind
			("tilesize", PL_LAMBDA_FORWARD_THIS(setTileSize))
			("spherical", PL_LAMBDA_FORWARD_THIS(setSpherical))
			("size", PL_LAMBDA_FORWARD_THIS(setDimensions))
		;

		setDist(addParamReference(type_v<IColorDistribution2D>, params, "dist"));

		init();
	}

	ParameterTree AugmentedPDFSamplerColorDistImportanceTiled::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters()};

		if(getDist())
			params["dist"].fromEntityRef(*getDist(), deep);

		params["tilesize"] = mTileSize;
		params["size"] = mDims;
		params["spherical"] = mSpherical;

		return params;
	}


	void PDFSamplerColorDistImportanceTiled::setDist(EntityPtr<const IColorDistribution2D> d)
	{
		mDist = std::move(d);

		if(const auto bitmap = DynEntityCast<const IBitmap2DRead>(mDist); bitmap)
			setDimensions((pl::point2d<u32>)bitmap->getRect().p2);
	}

	void PDFSamplerColorDistImportanceTiled::init()
	{
		mCdfYdata.clear();
		mCdfYdata.shrink_to_fit();
		mCdfXdata.clear();
		mCdfXdata.shrink_to_fit();
		mDistrib.clear();
		mDistrib.shrink_to_fit();

		if(!mDist)
			return;

		const auto w = mDims[0];
		const auto h = mDims[1];

		mNbTilesX = r_(w) / mTileSize /oceil;
		const auto nbTilesY = u32( r_(h) / mTileSize /oceil );
		mNbTiles = mNbTilesX * nbTilesY;

		mMaxSize = {
			(mNbTilesX * mTileSize) / r_(w),
			(nbTilesY * mTileSize) / r_(h)
		};

		mCdfXdata.resize(nbTilesY * (mNbTilesX + 2));
		mCdfXdata.shrink_to_fit();

		mDistrib.resize(mNbTiles);
		mDistribY.reserve(nbTilesY);

		const auto widthRec = 1 / r_(w);
		const auto heightRec = 1 / r_(h);

		const auto texelCenterX = widthRec * 0.5;
		const auto texelCenterY = heightRec * 0.5;

		const auto halfTileSize = r_(mTileSize) * 0.5;

		mir(nbTilesY) |
			pl::for_each
			(
				[&](const auto ty)
				{
					for(auto&& [tx, dis] : rgv::enumerate(mDistrib | rgv::drop(ty * mNbTilesX) | rgv::take(mNbTilesX)))
					{
						const auto stx = u32(tx * mTileSize), sty = u32(ty * mTileSize);
						const auto sizex = u32(stx + mTileSize > w ? w - stx : mTileSize);
						const auto sizey = u32(sty + mTileSize > h ? h - sty : mTileSize);
						Real total = 0;

						const auto &D = *mDist;
						const auto sphereFactor = mSpherical ? pl::plclamp<Real>(pl::plsin(std::numbers::pi_v<Real> * (Real(sty) + halfTileSize) * heightRec), 0, 1) : 1;

						pl::rectangle<u32>{{}, {sizex, sizey}}.loop([&](const auto p){
							total += getColorY(D(Real(stx + p[0]) * widthRec + texelCenterX, Real(sty + p[1]) * heightRec + texelCenterY)) * sphereFactor;
						});

						dis = pl::assert_valid( total / (sizex * sizey), valid::Weight );
					}

					getCdfX(ty).createDistribution();
				},
				std::identity{}//,
				//pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(10)
			);

		mCdfYdata.resize(nbTilesY + 2);
		mCdfYdata.shrink_to_fit();

		for(const auto ty : mir(nbTilesY))
		{
			const auto c = getCdfX(ty).integral();
			mDistribY.push_back( pl::assert_valid(c ? 1 / c : 0, valid::Weight) );
		}

		getCdfY().createDistribution();
	}

	pl::marked_optional<Sample<Point2Dr>> PDFSamplerColorDistImportanceTiled::operator() (RandomReals<2> rnd) const noexcept
	{
		const auto cdfY = getCdfY();
		pl::point2d<decltype(cdfY)::size_type> p;

		p.set(cdfY.sample(rnd[1]), size_v<1>);

		const auto xcdf = getCdfX(p[1]);

		p.set(xcdf.sample(rnd[0]), size_v<0>);

		Point2Dr res{
			xcdf.getSampleOffset(rnd[0], p[0]),
			cdfY.getSampleOffset(rnd[1], p[1])
		};

		res *= mMaxSize;
		if(res >= 1)
			return pl::nullopt;

		return Sample<Point2Dr>{
			.value = res,
			.pdf = pl::assert_valid( cdfY.PMF(p[1]) * xcdf.PMF(p[0]), valid::Weight )
		};
	}

	OptionalPDF<Real> PDFSamplerColorDistImportanceTiled::PDF(Point2Dr v) const noexcept
	{
		v /= mMaxSize;

		if(v >= 1)
			return pl::nullopt;

		const auto cdfY = getCdfY();
		const auto indexY = pl::plmin<u32>(v[1] * cdfY.size(), cdfY.size() - 1);
		const auto xcdf = getCdfX(indexY);

		const auto pdfY = cdfY.PMF(indexY);
		const auto pdfX = xcdf.PDF(v[0]);
		const auto pdf = pdfY * pdfX;

		return pl::assert_valid(pdf, valid::Weight);
	}
}
