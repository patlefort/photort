/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Integrator/Path.hpp>
#include <PhotoRT/Core/Film.hpp>
#include <PhotoRT/Core/Distribution.hpp>
#include <PhotoRT/Core/BxDF.hpp>
#include <PhotoRT/Core/Sampling.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/Instance.hpp>
#include <PhotoRT/Core/Edf.hpp>

namespace PhotoRT::PathIntegrator
{
	PRT_ENTITY_FACTORY_DEFINE(IntegratorFactoryPath)

	bool IntegratorFactoryPath::connect(std::string_view socket, const EntityPtrBase &entity)
	{
		PRT_TRY_CONNECT_SOCKET("direct_light_sampler", setDirectLightSampler, IEmitterSampler)
		PRT_TRY_CONNECT_SOCKET("light_portal_sampler", setLightPortalSampler, IEmitterSampler)

		if(socket.starts_with("mutators."))
		{
			mMutatorFactories[getSocketIndex(socket, "mutators.")].factory = DynEntityCast<IMutatorFactory>(entity);
			return true;
		}

		return false;
	}

	RenderEntitySocketDescMap IntegratorFactoryPath::getSockets() const
	{
		RenderEntitySocketDescMap v{
			{"direct_light_sampler", {"lightsampler"}},
			{"light_portal_sampler", {"lightsampler"}}
		};

		for(const auto i : mir(mMutatorFactories.size()))
			v["mutators." + pl::io::to_string_noloc(i)] = {"mutator"};

		return v;
	}

	EntityPtrBase IntegratorFactoryPath::getConnected(std::string_view socket)
	{
		PRT_TRY_GETCONNECTED_SOCKET("direct_light_sampler", getDirectLightSampler)
		PRT_TRY_GETCONNECTED_SOCKET("light_portal_sampler", getLightPortalSampler)

		if(socket.starts_with("mutators."))
			return DynEntityCast<IRenderEntity>(mMutatorFactories[getSocketIndex(socket, "mutators.")].factory);

		return nullptr;
	}

	void IntegratorFactoryPath::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		params.getValueIfExistsBind
			("max_path_length_camera",           PL_LAMBDA_FORWARD_THIS(setMaxPathLengthCamera))
			("use_rr",                           PL_LAMBDA_FORWARD_THIS(setUseRR))
			("rr_factor",                        PL_LAMBDA_FORWARD_THIS(setRRfactor))
			("rr_min",                           PL_LAMBDA_FORWARD_THIS(setRRmin))
			("jitter",                           PL_LAMBDA_FORWARD_THIS(setJittering))
			("random_pixel",                     PL_LAMBDA_FORWARD_THIS(setRandomPixel))
			("nb_samples_pixel",                 PL_LAMBDA_FORWARD_THIS(setNbSamplesPixel))
			("nb_samples_path",                  PL_LAMBDA_FORWARD_THIS(setNbSamplesPath))
			("nb_samples_direct_light",          PL_LAMBDA_FORWARD_THIS(setNbSamplesDirectLight))
			("nb_samples_direct_per_light",      PL_LAMBDA_FORWARD_THIS(setNbSamplesDirectPerLight))
			("nb_layer_bounces_max",             PL_LAMBDA_FORWARD_THIS(setLayerBouncesMax))
			("sample_light_portal_prob",         PL_LAMBDA_FORWARD_THIS(setLightPortalSampleProb))
			("trace_shadows",                    PL_LAMBDA_FORWARD_THIS(setTraceShadow))
		;

		connectEntity(*this)
			("direct_light_sampler", params)
			("light_portal_sampler", params)
		;

		mMutatorFactories.clear();
		mMutatorFactories.shrink_to_fit();

		if(const auto p = params.findParam("mutators"); p && p->childParams)
			for(const auto &param : *p->childParams)
			{
				mMutatorFactories.push_back({
					param.second.getValueOrDefault<uf32>("nb_samples", 4),
					addReference(type_v<IMutatorFactory>, param.second["mutator"])
				});
			}

	}

	ParameterTree IntegratorFactoryPath::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params.insert({
			{"max_path_length_camera",          getMaxPathLengthCamera()},
			{"use_rr",                          isUsingRR()},
			{"rr_factor",                       getRRfactor()},
			{"rr_min",                          getRRmin()},
			{"jitter",                          isJittering()},
			{"random_pixel",                    useRandomPixel()},
			{"nb_samples_pixel",                getNbSamplesPixel()},
			{"nb_samples_path",                 getNbSamplesPath()},
			{"nb_samples_direct_light",         getNbSamplesDirectLight()},
			{"nb_samples_direct_per_light",     getNbSamplesDirectPerLight()},
			{"nb_layer_bounces_max",            getLayerBouncesMax()},
			{"sample_light_portal_prob",        getLightPortalSampleProb()},
			{"trace_shadows",                   isTracingShadow()}
		});

		params.insertReference
			("direct_light_sampler",  std::to_address(mDirectLightSampler), deep)
			("light_portal_sampler",  std::to_address(mLightPortalSampler), deep)
		;

		for(auto&& [i, mutator] : rgv::enumerate(mMutatorFactories))
		{
			params["mutators"][i]["nb_samples"] = mutator.nbSamples;
			params["mutators"][i]["mutator"].fromEntityRef(*mutator.factory, deep);
		}

		return params;
	}

	void IntegratorFactoryPath::init(const Scene &scene, TracerParams &tracer)
	{
		if(mDirectLightSampler)
		{
			mDirectLightSampler->setList(scene.getLights());
			mDirectLightSampler->init(scene, tracer);
			mHasLightsM = !scene.getLights().empty();
		}
		else
			mHasLightsM = false;

		if(mLightPortalSampler)
		{
			mLightPortalSampler->setList(scene.getLightPortalsPtr());
			mLightPortalSampler->init(scene, tracer);
		}
	}

	std::shared_ptr<IIntegrator> IntegratorFactoryPath::createIntegrator() const
	{
		return std::make_shared<IntegratorPath>(*this);
	}


	namespace
	{
		struct PathState : PathStateBasic {};

		[[nodiscard]] constexpr auto mis(auto v) noexcept
				//{ return misBalance(v); }
				{ return misPower(v); }
	}


	IntegratorPath::IntegratorPath(const IntegratorFactoryPath &factory)
		: mFactory(factory)
	{
		mMutators.clear();
		mMutators.shrink_to_fit();

		for(auto &f : mFactory.mMutatorFactories)
			mMutators.push_back({f.nbSamples, f.factory->create_shared()});

		/*for(const auto &param : mFactory.mutatorParams)
		{
			mMutators.push_back({
				param.second.getValueOrDefault<uf32>("nb_samples", 4),
				mFactory.addReference(type_v<IMutator>, param.second["mutator"])
			});
		}*/
	}

	IntegratorPath::~IntegratorPath() = default;

	StatsAccumulatorList IntegratorPath::getStats() const
	{
		StatsAccumulatorList list;

		list.emplace("samples", pl::clone(mStats[statscollection::Sample]));
		list.emplace("paths", pl::clone(mStats[statscollection::Path]));
		list.emplace("zero-contrib-path", pl::clone(mStats[statscollection::ZeroContribPath]));
		list.emplace("shadow-rays", pl::clone(mStats[statscollection::ShadowRays]));
		list.emplace("shadow-rays-occluded", pl::clone(mStats[statscollection::ShadowRaysOccluded]));

		#ifdef PRT_PROFILER
		{
			auto profilerst = mProfiler.getStats();
			mergeStats(list, profilerst);
		}
		#endif

		return list;
	}

	void IntegratorPath::resetStats()
	{
		hn::for_each(mStats, [](auto&& s){ hn::second(s).reset(); });
	}

	void IntegratorPath::init(const Scene &scene)
	{
		IntegratorBase::init(scene);

		mContribution.resize(scene.getLightGroups().size() + 1);

		mLpSampleProb = mFactory.getLightPortalSampler() && !mScene->getLightPortals().empty() ? mFactory.getLightPortalSampleProb() : 0;
	}

	void IntegratorPath::initRNG(Sampler &sampler)
	{
		sampler.setSampleBufferSize(10, mFactory.getNbSamplesPath());
	}

	void IntegratorPath::integrate(PixelSampleTileLayers &samples, TracerParams &tracer)
	{
		const auto &camSubStack = samples.camera->getSubstanceStack();

		const auto sampleLights = [&](
				const Contributor &contrib,
				SubstanceStack &shadowSubsStack,
				const PathState &pathState,
				const Ray &oray,
				const PathSegment &segment,
				const auto &interaction)
			{
				const uf32 curNbSamplesPerLights = pl::plmax<uf32>(1, mFactory.getNbSamplesDirectPerLight() * pathState.distributionFactor);

				// Avoid heap allocation with std::function and large capture.
				const auto handleLight = [&](const IEmitter &light, uf32 /*index*/, Real weight, Real pdf, RandomReals<2> rnd)
					{
						auto lsp = sampleLightDirect(
							rnd, light, oray,
							interaction, mLpSampleProb, std::to_address(mFactory.getLightPortalSampler()),
							std::to_address(mFactory.getDirectLightSampler()), mScene->getInfiniteLights(),
							tracer.sampler);

						const bool isInfLight = light.isInfinite();

						if(!lsp || isAlmostBlack(lsp->radiance.color) || (!isInfLight && isAlmostBlack(lsp->sample.color)))
							return;

						pdf *= lsp->sample.pdf * lsp->sample.sensor.pdf;

						const auto traceShadowToLight =
							[&]() -> std::pair<bool, Real>
							{
								if(!mFactory.isTracingShadow())
									return {true, 0};

								const auto [nbTraced, visible, pdfTr] = std::invoke([&]() -> std::tuple<StatCounter, bool, Real>
									{
										shadowSubsStack = pathState.substanceStack;
										shadowSubsStack.updateSubstanceStack( shadowSubsStack.checkSubstanceEntry(lsp->hemisphere) );

										const auto sres = traceShadow(EnumTransportMode::Radiance, lsp->ray, *segment.end, tracer, shadowSubsStack, 100);
										lsp->radiance.color *= (Color)sres.color;

										mStats[statscollection::ShadowRays](sres.nbTraced);

										if(lsp->lp && (!sres.lp || sres.lp->getEmitter() != lsp->lp))
											return {sres.nbTraced, false, sres.pdfTr};

										if(!lsp->lp && !lsp->sample.delta && isInfLight && sres.lp && mLpSampleProb > 0 && sres.pdfLpForW * mLpSampleProb * mFactory.getLightPortalSampler()->PDF(*segment.end, *sres.lp->getEmitter()).value() >= lsp->sample.pdf)
											return {sres.nbTraced, false, sres.pdfTr};

										return {sres.nbTraced, !isAlmostBlack(lsp->radiance.color), sres.pdfTr};
									});

								if(!visible)
									mStats[statscollection::ShadowRaysOccluded](nbTraced);

								return {visible, pdfTr};
							};

						if(const auto [visible, pdfTr] = traceShadowToLight(); visible)
						{
							const auto misWeight = misWeight2(
									pdf,
									interaction.PDF(EnumTransportMode::Radiance, oray.dir, lsp->sample.dir).value() * pdfTr * (1 - lsp->sample.sensor.pdf) * mFactory.getPathWalker().getContinuationProb(lsp->radiance.albedo),
									mis<Real>
								);

							const auto contribWeight = lsp->radiance.color * misWeight * (pdf /oprtreciprocal) * weight;
							if(isAlmostBlack(contribWeight))
								return;

							contrib( lsp->sample.color * contribWeight, light.getGroupIndex() );

							if(isInfLight && !lsp->sample.delta)
							{
								for(const auto &l : mScene->getInfiniteLights())
									if(!pl::same_underlying_address<IEmitter>(l, &light))
										contrib( l->radiance(*segment.end, lsp->ray.dir) * contribWeight, l->getGroupIndex() );
							}
						}
					};

				// Sample light sources
				mFactory.getDirectLightSampler()->sample(
					*segment.end, mFactory.getNbSamplesDirectLight(), curNbSamplesPerLights, tracer.sampler,
					std::ref(handleLight)
				);
			};

		const auto integrateCamera =
			[&](const Contributor &contrib, auto &collision, auto &trEntry, const auto index)
			{
				SubstanceStack shadowSubsStack;
				PathState pathState{{
						.importance = samples.rayImportance,
						.distributionFactor = samples.rayDistributionFactor,
						.mode = EnumTransportMode::Radiance,
						.mutated = tracer.sampler.isMutatedPath(),
						.substanceStack = camSubStack
					}};

				if(!pathState.mutated)
				{
					tracer.sampler.clearMainRecordedStream();
					tracer.sampler.appendStream(index + 1);
				}

				const auto weightLight = [&](const PathSegment &segment, const IEmitter &l, VecD dir, const auto &interaction) noexcept
					{
						const auto pdfSeg = segment.pdfW * segment.pdfTr * segment.contProb;
						const auto pdfLight = l.PDFA(interaction, *segment.start, dir).value() * mFactory.getDirectLightSampler()->PDF(*segment.start, l).value();
						return misWeight2(pdfSeg, pdfLight, mis<Real>);
					};

				PRT_SCOPED_PROFILE(Profiler::raytrace, Profiler::tag::PathWalk);

				mFactory.getPathWalker().integrateAndTracePath(

					contrib.scale({.weight = trEntry.color}), pathState, collision.ray, trEntry.segment, tracer,

					[&] [[nodiscard]] (auto&&... args) -> auto& { return mInteractions.emplace_back(PL_FWD(args)...); },

					//////////////////////////////////////////////////////
					// Prepare path state
					//////////////////////////////////////////////////////
					pl::do_nothing,

					//////////////////////////////////////////////////////
					// Path split
					//////////////////////////////////////////////////////
					[](PathState &, const Ray &, const PathSegment &, auto&& fc) { PL_FWD(fc)(); },

					//////////////////////////////////////////////////////
					// Path pass-through
					//////////////////////////////////////////////////////
					pl::do_nothing,

					//////////////////////////////////////////////////////
					// Path contribution
					//////////////////////////////////////////////////////
					[&]<typename Interaction_> [[nodiscard]] (const Contributor &contrib, PathState &pathState, const Ray &ray, const PathSegment &segment, const Interaction_ &interaction)
					{
						const bool isDeltaRay = segment.isType(EnumRayType::Specular | EnumRayType::Primary);
						const bool doDirectMis = !isDeltaRay && mFactory.getDirectLightSampler();
						constexpr bool isSurface = std::same_as<Interaction_, SurfaceInteraction>;

						// Emission contribution
						{
							PRT_SCOPED_PROFILE(mProfiler, tag_Emission);

							if(const Color ecolor = interaction.emission(-ray.dir); !isAlmostBlack(ecolor))
							{
								const auto l = interaction.instance()->getEmitter();

								// MIS
								const auto w = !l || !doDirectMis ? 1 : weightLight(segment, *l, ray.dir, interaction);

								contrib(ecolor * w, l ? l->getGroupIndex() : 0);
							}
						}

						// Do direct lightning
						if(mFactory.hasLights() && !interaction.isDelta() && (isSurface || interaction.enteredSub->getMaterial().isSamplingDirectLightMedium()))
						{
							PRT_SCOPED_PROFILE(mProfiler, tag_DirectLight);
							sampleLights(contrib, shadowSubsStack, pathState, ray, segment, interaction);
						}

						return true;
					},

					//////////////////////////////////////////////////////
					// Path end
					//////////////////////////////////////////////////////
					[&](const Contributor &contrib, PathState &pathState, const Ray &ray, const PathSegment &segment)
					{
						mStats[statscollection::Path](pathState.nbBounces + 1);

						if(!segment.end && !segment.insideLayer)
						{
							PRT_SCOPED_PROFILE(mProfiler, tag_Background);

							const bool isDeltaRay = segment.isType(EnumRayType::Specular | EnumRayType::Primary);
							const bool doDirectMis = !isDeltaRay && mFactory.getDirectLightSampler();

							// Background contribution
							if(const auto bg = std::to_address(mScene->getBackground()); bg)
								contrib( bg->map(*segment.start, ray.dir), 0 );

							// Infinite lights contribution
							const auto &infLights = mScene->getInfiniteLights();

							if(!infLights.empty())
							{
								const auto [pdfLpW, pdfLpS] = std::invoke([&]{
										if(!segment.lpSurface)
											return std::make_pair(Real{}, Real{});

										return pl::visit_one(*segment.lpSurface, [&](const auto &lpi)
											{
												return std::make_pair(
														pdfAtoW(
																lpi.instance()->getEmitter()->PDF(*segment.lpSurface).value(),
																geometricTermPosEpsilon(lpi.localPos(), lpi.instance()->getEmitter()->worldToLocal( pl::visit_one(*segment.start, [&](const auto &si){ return si.worldPos(); }) )),
																lpi.geometricTermDir(ray.dir)
															) * mLpSampleProb,
														mFactory.getLightPortalSampler()->PDF(*segment.start, *lpi.instance()->getEmitter()).value()
													);
											});
									});

								const auto [ipdfW, ipdfS] = !doDirectMis || mLpSampleProb >= 1 ?
									std::pair<Real, Real>{} :
									std::invoke([&]{
										const auto [nb, ipdfW, ipdfS] = infiniteLightsPDF(infLights, *mFactory.getDirectLightSampler(), nullptr, *segment.start, ray.dir);
										return std::pair<Real, Real>{ipdfW / nb * (1 - mLpSampleProb), ipdfS / nb};
									});
								const Real misWeight = !doDirectMis ? 1 : misWeight2(segment.pdfW * segment.pdfTr * segment.contProb, (pdfLpW * pdfLpS >= ipdfW ? pdfLpW * ipdfS : ipdfW * ipdfS), mis<Real>);

								for(const auto &l : infLights)
									contrib( l->radiance(*segment.start, ray.dir) * misWeight, l->getGroupIndex() );
							}
						}

						if(mContribution.isBlack())
							mStats[statscollection::ZeroContribPath](1);
					}

				);
			};

		//////////////////////////////////////////////////////
		// Generate camera rays
		//////////////////////////////////////////////////////

		samples.addSample();

		samplePixels(
			tracer,
			mFactory.isJittering(),
			mFactory.useRandomPixel(),
			camSubStack.back(),
			samples,
			mFactory.getNbSamplesPixel(),

			[&] [[nodiscard]] (auto&&... args) -> auto& { return mInteractions.emplace_back(PL_FWD(args)...); },

			[&]{ mInteractions.clear(); },

			[&](auto &collision, auto &trEntry, const auto spPos, const auto index)
				{
					if(!samples.tiles.empty())
					{
						mContribution.reset();

						integrateCamera({mContribution, {.weight = CReal{1} / mFactory.getNbSamplesPixel()}}, collision, trEntry, index);

						for(const auto i : mir(mContribution.size()))
							samples.addContribution(spPos, mContribution.getrr(i), i);
					}

					//auto pathColor = mContribution.rrtotal();

					// Trace mutated paths
					/*if(!mMutators.empty())
					{
						sampleMutations(
							mMutators,
							camSubStack.back(),
							mFactory.isJittering(),
							mFactory.useRandomPixel(),
							true,
							samples,
							tracer,
							spPos,
							pathColor,
							integrateCamera
						);

						for(auto &w : samples.weightRange(rect.area()))
							++w;
					}
					else
					{*/
						//samples.addSample(spPos, pathColor, 1);
					//}
				}
		);

		mStats[statscollection::Sample](mFactory.getNbSamplesPixel() * samples.tileDims.area());
	}

}
