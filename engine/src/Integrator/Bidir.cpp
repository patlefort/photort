/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Integrator/Bidir.hpp>
#include <PhotoRT/Core/Film.hpp>
#include <PhotoRT/Core/Distribution.hpp>
#include <PhotoRT/Core/BxDF.hpp>
#include <PhotoRT/Core/Sampling.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/Instance.hpp>
#include <PhotoRT/Core/Edf.hpp>

#include <patlib/scoped.hpp>

#include <optional>

namespace PhotoRT::BidirIntegrator
{
	PRT_ENTITY_FACTORY_DEFINE(IntegratorFactoryBidir)

	bool IntegratorFactoryBidir::connect(std::string_view socket, const EntityPtrBase &entity)
	{
		PRT_TRY_CONNECT_SOCKET("light_sampler", setLightSampler, IEmitterSampler)
		PRT_TRY_CONNECT_SOCKET("light_portal_sampler", setLightPortalSampler, IEmitterSampler)

		if(socket.starts_with("mutators."))
		{
			mMutatorFactories[getSocketIndex(socket, "mutators.")].factory = DynEntityCast<IMutatorFactory>(entity);
			return true;
		}

		return false;
	}

	RenderEntitySocketDescMap IntegratorFactoryBidir::getSockets() const
	{
		RenderEntitySocketDescMap v{
			{"light_sampler", {"lightsampler"}},
			{"light_portal_sampler", {"lightsampler"}}
		};

		for(const auto i : mir(mMutatorFactories.size()))
			v["mutators." + pl::io::to_string_noloc(i)] = {"mutator"};

		return v;
	}

	EntityPtrBase IntegratorFactoryBidir::getConnected(std::string_view socket)
	{
		PRT_TRY_GETCONNECTED_SOCKET("light_sampler",        getLightSampler)
		PRT_TRY_GETCONNECTED_SOCKET("light_portal_sampler", getLightPortalSampler)

		if(socket.starts_with("mutators."))
			return DynEntityCast<IRenderEntity>(mMutatorFactories[getSocketIndex(socket, "mutators.")].factory);

		return nullptr;
	}

	void IntegratorFactoryBidir::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		params.getValueIfExistsBind
			("max_path_length_light",               PL_LAMBDA_FORWARD_THIS(setMaxPathLengthLight))
			("max_path_length_camera",              PL_LAMBDA_FORWARD_THIS(setMaxPathLengthCamera))
			("use_rr",                              PL_LAMBDA_FORWARD_THIS(setUseRR))
			("rr_factor",                           PL_LAMBDA_FORWARD_THIS(setRRfactor))
			("rr_min",                              PL_LAMBDA_FORWARD_THIS(setRRmin))
			("jitter",                              PL_LAMBDA_FORWARD_THIS(setJittering))
			("random_pixel",                        PL_LAMBDA_FORWARD_THIS(setRandomPixel))
			("connect_light_to_camera",             PL_LAMBDA_FORWARD_THIS(setConnectLightToCamera))
			("nb_samples_pixel",                    PL_LAMBDA_FORWARD_THIS(setNbSamplesPixel))
			("nb_samples_path",                     PL_LAMBDA_FORWARD_THIS(setNbSamplesPath))
			("nb_samples_light",                    PL_LAMBDA_FORWARD_THIS(setNbSamplesLight))
			("nb_samples_light_path",               PL_LAMBDA_FORWARD_THIS(setNbSamplesLightPath))
			("nb_samples_direct_light",             PL_LAMBDA_FORWARD_THIS(setNbSamplesDirectLight))
			("nb_samples_direct_per_light",         PL_LAMBDA_FORWARD_THIS(setNbSamplesDirectPerLight))
			("nb_layer_bounces_max",                PL_LAMBDA_FORWARD_THIS(setLayerBouncesMax))
			("sample_light_portal_prob",            PL_LAMBDA_FORWARD_THIS(setLightPortalSampleProb))
			("sample_light_portal_prob_dl",         PL_LAMBDA_FORWARD_THIS(setLightPortalSampleProbDL))
		;

		connectEntity(*this)
			("light_sampler", params)
			("light_portal_sampler", params)
		;

		mMutatorFactories.clear();
		mMutatorFactories.shrink_to_fit();

		if(const auto p = params.findParam("mutators"); p && p->childParams)
			for(const auto &param : *p->childParams)
			{
				mMutatorFactories.push_back({
					param.second.getValueOrDefault<uf32>("nb_samples", 4),
					addReference(type_v<IMutatorFactory>, param.second["mutator"])
				});
			}
	}

	ParameterTree IntegratorFactoryBidir::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params.insert({
			{"max_path_length_light",               getMaxPathLengthLight()},
			{"max_path_length_camera",              getMaxPathLengthCamera()},
			{"use_rr",                              isUsingRR()},
			{"rr_factor",                           getRRfactor()},
			{"rr_min",                              getRRmin()},
			{"jitter",                              isJittering()},
			{"random_pixel",                        useRandomPixel()},
			{"connect_light_to_camera",             isConnectingLightToCamera()},
			{"nb_samples_pixel",                    getNbSamplesPixel()},
			{"nb_samples_path",                     getNbSamplesPath()},
			{"nb_samples_light",                    getNbSamplesLight()},
			{"nb_samples_light_path",               getNbSamplesLightPath()},
			{"nb_samples_direct_light",             getNbSamplesDirectLight()},
			{"nb_samples_direct_per_light",         getNbSamplesDirectPerLight()},
			{"nb_layer_bounces_max",                getLayerBouncesMax()},
			{"sample_light_portal_prob",            getLightPortalSampleProb()},
			{"sample_light_portal_prob_dl",         getLightPortalSampleProbDL()}
		});

		params.insertReference
			("light_sampler",           std::to_address(mLightSampler), deep)
			("light_portal_sampler",    std::to_address(mLightPortalSampler), deep)
		;

		for(auto&& [i, mutator] : rgv::enumerate(mMutatorFactories))
		{
			params["mutators"][i]["nb_samples"] = mutator.nbSamples;
			params["mutators"][i]["mutator"].fromEntityRef(*mutator.factory, deep);
		}

		return params;
	}

	void IntegratorFactoryBidir::init(const Scene &scene, TracerParams &tracer)
	{
		if(mLightPortalSampler)
		{
			mLightPortalSampler->setList(scene.getLightPortalsPtr());
			mLightPortalSampler->init(scene, tracer);
		}

		if(mLightSampler)
		{
			mLightSampler->setList(scene.getLights());
			mLightSampler->init(scene, tracer);
			mHasLightsM = !scene.getLights().empty();
		}
		else
			mHasLightsM = false;
	}

	std::shared_ptr<IIntegrator> IntegratorFactoryBidir::createIntegrator() const
	{
		return std::make_shared<IntegratorBidir>(*this);
	}


	IntegratorBidir::IntegratorBidir(const IntegratorFactoryBidir &factory)
		: mFactory(factory)
	{
		mMutators.clear();
		mMutators.shrink_to_fit();

		for(auto &f : factory.mMutatorFactories)
			mMutators.push_back({f.nbSamples, f.factory->create_shared()});
	}

	IntegratorBidir::~IntegratorBidir() = default;

	StatsAccumulatorList IntegratorBidir::getStats() const
	{
		StatsAccumulatorList list;

		list.emplace("samples", pl::clone(mStats[statscollection::Sample]));

		if(mFactory.getMaxPathLengthCamera())
		{
			list.emplace("paths", pl::clone(mStats[statscollection::Path]));
			list.emplace("zero-contrib-path", pl::clone(mStats[statscollection::ZeroContribPath]));
			list.emplace("shadow-rays", pl::clone(mStats[statscollection::ShadowRays]));
			list.emplace("shadow-rays-occluded", pl::clone(mStats[statscollection::ShadowRaysOccluded]));

			if(mFactory.getLightSampler() && mFactory.getMaxPathLengthLight())
			{
				list.emplace("connection", pl::clone(mStats[statscollection::Conn]));
				list.emplace("connection-occluded", pl::clone(mStats[statscollection::ConnOccluded]));
				list.emplace("connection-rr-end", pl::clone(mStats[statscollection::ConnRR]));
			}
		}

		if(mFactory.getLightSampler() && mFactory.getMaxPathLengthLight())
		{
			list.emplace("light-paths", pl::clone(mStats[statscollection::LightPath]));
		}

		#ifdef PRT_PROFILER
		{
			auto profilerst = mProfiler.getStats();
			mergeStats(list, profilerst);
		}
		#endif

		return list;
	}

	void IntegratorBidir::resetStats()
	{
		hn::for_each(mStats, [](auto&& s){ hn::second(s).reset(); });
	}

	void IntegratorBidir::init(const Scene &scene)
	{
		IntegratorBase::init(scene);

		mContribution.resize(scene.getLightGroups().size() + 1);

		mDoBidirMIS = mFactory.getNbSamplesLight() && mFactory.getNbSamplesLightPath() &&
			mFactory.getMaxPathLengthLight() && mFactory.getLightSampler() &&
			mFactory.getMaxPathLengthCamera() && mFactory.getNbSamplesPath() && mFactory.getNbSamplesPixel();

		mLpSampleProb = mFactory.getLightPortalSampler() && !mScene->getLightPortals().empty() ? mFactory.getLightPortalSampleProb() : 0;
		mLpSampleProbDL = mFactory.getLightPortalSampler() && !mScene->getLightPortals().empty() ? mFactory.getLightPortalSampleProbDL() : 0;
	}

	void IntegratorBidir::initRNG(Sampler &sampler)
	{
		sampler.setSampleBufferSize(10, mFactory.getNbSamplesPath());
	}


	namespace
	{
		template <typename PathType_>
		struct BidirPathState : public PathStateBasic
		{
			Path<PathType_> *path = nullptr;

			constexpr void storeCurrent(Color contrib, Color rrContrib) noexcept
			{
				path->vertex.push_back({
						.state{ *path->vertexState.front() },
						.contrib{ contrib },
						.rrContrib{ rrContrib },
						.misWeight{ path->misWeight.value }
					});
			}

			[[nodiscard]] constexpr auto &front() noexcept { return path->vertex.back(); }
			[[nodiscard]] constexpr const auto &front() const noexcept { return path->vertex.back(); }
			[[nodiscard]] constexpr auto &prev() noexcept { return *std::prev(path->vertex.end(), 2); }
			[[nodiscard]] constexpr const auto &prev() const noexcept { *std::prev(path->vertex.end(), 2); }
			[[nodiscard]] constexpr auto &prev2() noexcept { return *std::prev(path->vertex.end(), 3); }
			[[nodiscard]] constexpr const auto &prev2() const noexcept { return *std::prev(path->vertex.end(), 3); }

			[[nodiscard]] constexpr auto currentIterator() noexcept { return std::prev(path->vertex.end()); }
			[[nodiscard]] constexpr auto currentIterator() const noexcept { return std::prev(path->vertex.end()); }
			[[nodiscard]] constexpr auto size() const noexcept { return path->vertex.size(); }
		};


		[[nodiscard]] constexpr auto mis(auto v) noexcept
				//{ return misBalance(v); }
				{ return misPower(v); }
				//{ return 1; }

		template <typename T_>
		[[nodiscard]] Real getBackPdfA(const Path<T_> &path, const EnumTransportMode mode, VertexState &frontv, const VertexState &backv, VecD dir, Real contProb, u32 level, [[maybe_unused]] Real lpSampleProb = 0, bool delta = false) noexcept
		{
			if(delta)
				return contProb * frontv.pdfTr;

			const auto pdf = std::invoke([&]{
					const auto backPdfW = [&]
						{
							return pl::visit_one(
									frontv.interaction,
									[&](auto&& i)
									{
										const InteractionReverseGuard sg{i, dir};
										return i.PDF(switchTransportMode(mode), -dir, -frontv.dir).value();
									}
								);
						};

					const auto worldMis = [&]
						{
							return frontv.insideLayer ?
								backPdfW() :
									pdfWtoA(
										backPdfW(),
										frontv.distSqr,
										backv.geometricTermDir(frontv.dir)
									);
						};

					const auto localMis = [&]
						{
							return frontv.insideLayer ?
								backPdfW() :
									pdfWtoA(
										backPdfW(),
										geometricTermPosEpsilon(backv.localPos(), path.startedFrom->worldToLocal(frontv.worldPos())),
										backv.geometricTermDir(frontv.dir)
									);
						};

					if(level > 0)
						return worldMis() * frontv.pdfTr;

					if constexpr(std::same_as<T_, IEmitter>)
					{
						const auto &light = *path.startedFrom;

						if(light.isInfinite())
						{
							const auto pdfLightBackW = backPdfW();
							const auto pdfLightBackA = pdfLightBackW * path.infPdfTr;

							if(!path.lp || !lpSampleProb)
								return pdfLightBackA * frontv.pdfTr;

							const auto pdfBackLpA = pl::visit_one(*path.lpSurface, [&](const auto &i){
									return pdfWtoA(
											pdfLightBackW,
											geometricTermPosEpsilon(i.localPos(), path.lp->worldToLocal(frontv.worldPos())),
											i.geometricTermDir(frontv.dir)
										);
								});

							return pl::pllerp(pdfLightBackA * frontv.pdfTr, pdfBackLpA * (frontv.pdfTr / path.infPdfTr), lpSampleProb);
						}
						else
							return localMis() * frontv.pdfTr;
					}
					else
						return localMis() * frontv.pdfTr;
				});

			return mis(pdf * contProb);
		}

		template <typename T_>
		[[nodiscard]] Real getForPdfA(
			const Path<T_> &path,
			const EnumTransportMode mode,
			const VertexState &frontv,
			const VertexState *backv,
			u32 level,
			const auto &infLights,
			[[maybe_unused]] Real lpSampleProb = 0) noexcept
		{
			if(frontv.delta)
				return frontv.contProb * frontv.pdfTr;

			const auto pdf = std::invoke([&]
				{

					return pl::visit_one(frontv.interaction, [&](const auto &fi)
					{
						const bool isInfinite = std::invoke([&]{
								if constexpr(!std::same_as<T_, IEmitter>)
									return false;
								else
									return path.startedFrom->isInfinite();
							});

						const auto normalMis = [&]
							{
								return pl::visit_one(backv->interaction, [&](const auto &bi)
									{
										const auto pdfW = std::invoke([&]{
												if(level == 0)
													return path.startedFrom->PDFW(backv->interaction, -frontv.dir).value();

												return bi.PDF(mode, backv->dir, frontv.dir).value();
											});

										return frontv.insideLayer ? pdfW : pdfWtoA(
												pdfW,
												geometricTermPosEpsilon(fi.worldPos(), bi.worldPos()),
												fi.geometricTermDir(frontv.dir)
											);
									});
							};

						if(level > 0 || !isInfinite)
							return normalMis() * frontv.pdfTr;

						if constexpr(std::same_as<T_, IEmitter>)
						{
							const auto dotTerm = fi.geometricTermDir(frontv.dir);
							const auto pdfLightForA = path.startedFrom->PDF(backv->interaction).value() * dotTerm * path.infPdfTr;

							if(!path.lp || !lpSampleProb)
								return pdfLightForA * frontv.pdfTr;

							const auto [nb, ipdfWt] = infiniteLightsPDFW(infLights, nullptr, frontv.interaction, -frontv.dir);

							const auto pdfLpForA = pdfWtoA(
									ipdfWt / nb,
									geometricTermPosEpsilon(fi.worldPos(), pl::visit_one(*path.lpSurface, [&](const auto &i){ return i.worldPos(); })),
									dotTerm
								);

							return pl::pllerp(pdfLightForA * frontv.pdfTr, pdfLpForA * (frontv.pdfTr / path.infPdfTr), lpSampleProb);
						}
						else
							return normalMis() * frontv.pdfTr;
					});

				});

			return mis(pdf * frontv.contProb);
		}

		template <typename T_>
		void updateBackDendity(BidirPathState<T_> &pathState, const EnumTransportMode mode, const VecD dir, const PathSegment &segment, Real lpSampleProb = 0) noexcept
		{
			const bool isDelta = segment.isType(EnumRayType::Specular);
			auto &path = *pathState.path;

			if(segment.level > 0 && (mode != EnumTransportMode::Radiance || segment.level != 1))
			{
				const auto pdfBackA = getBackPdfA(path, mode, *path.vertexState.front(), *path.vertexState.back(), dir, segment.level - 1, lpSampleProb, isDelta);

				path.misWeight.update(
					path.vertexState.back()->pdfForA,
					pdfBackA * segment.contProb * segment.pdfTr,
					path.vertexState.front()->delta || (segment.level == 1 ? path.deltaSource : path.vertexState.back()->delta)
				);
			}
		}

		template <typename T_, typename Interaction_>
		void updateWeight(
			BidirPathState<T_> &pathState,
			const EnumTransportMode mode,
			const Ray &ray,
			const PathSegment &segment,
			const Interaction_ &interaction,
			Real lpSampleProb = 0,
			Real lightPosPdf = 0) noexcept
		{
			const bool isDelta = segment.isType(EnumRayType::Specular);
			auto &path = *pathState.path;

			if(!ray.isType(EnumRayType::Transparency))
			{
				updateBackDendity(pathState, mode, ray.dir, segment, lpSampleProb);
				path.vertexState.flip();
			}

			const bool isInfinite = std::invoke([&]{
					if constexpr(!std::same_as<T_, IEmitter>)
						return false;
					else
						return path.startedFrom->isInfinite();
				});
			const auto distSqr = segment.insideLayer || (isInfinite && segment.level == 0) ? 0 : geometricTermPosEpsilon(interaction.worldPos(), pl::visit_one(*segment.start, [&](const auto &i){ return i.worldPos(); }));

			path.vertexState.front().emplace( VertexState{
					.distSqr = distSqr,
					.pdfForA = mis(std::invoke([&]() -> Real
						{
							if(isDelta)
								return segment.pdfTr;

							const auto normalMis = [&]
								{
									return segment.insideLayer ? segment.pdfW : pdfWtoA(segment.pdfW, distSqr, interaction.geometricTermDir(ray.dir));
								};

							if(segment.level > 0 || !isInfinite)
								return normalMis() * segment.pdfTr;

							if constexpr(std::same_as<T_, IEmitter>)
							{
								const auto pdfLightForA = lightPosPdf * interaction.geometricTermDir(ray.dir);

								if(!path.lp)
									return pdfLightForA * segment.pdfTr;

								const auto pdfLpForA = pdfWtoA(
										segment.pdfW,
										geometricTermPosEpsilon(interaction.worldPos(), pl::visit_one(*path.lpSurface, [&](const auto &i){ return i.worldPos(); })),
										interaction.geometricTermDir(ray.dir)
									);

								return pl::pllerp(pdfLightForA * segment.pdfTr, pdfLpForA * (segment.pdfTr / path.infPdfTr), lpSampleProb);
							}
							else
								return normalMis() * segment.pdfTr;
						}) * segment.contProb),
					.pdfTr = segment.pdfTr,
					.contProb = segment.contProb,
					.dir = ray.dir,
					.interaction = interaction,
					.delta = isDelta,
					.insideLayer = segment.insideLayer
				} );

			#ifndef NDEBUG
				++path.debugSize;
			#endif
		}

		template <typename Interaction_>
		void connectCameraToLight(
			auto &lightPaths,
			const auto &settings,
			auto &statsCol,
			const Contributor &contrib,
			TracerParams &tracer,
			SubstanceStack &shadowSubsStack,
			BidirPathState<ICamera> &pathState,
			const PathSegment &segment,
			const Interaction_ &interaction)
		{
			constexpr bool isSurface = std::same_as<Interaction_, SurfaceInteraction>;

			auto &path = *pathState.path;
			auto &camVertexState = *path.vertexState.front();
			StatCounter nbConn = 0, nbConnRRend = 0;
			auto &rng = tracer.sampler;

			Ray shadowRay;
			shadowRay.setupShadow();

			auto lpRange = lightPaths | rgv::remove_if([](const auto &p){ return p.vertex.empty(); });

			auto connectionsRange = rgv::for_each(lpRange, [&](auto &lp)
				{
					plassert(lp.startedFrom);

					const bool isInfinite = lp.startedFrom->isInfinite();
					const auto startIt = lp.sampleStart == lp.vertex.begin() && (settings.getLightSampler() || isInfinite) ? std::next(lp.sampleStart) : lp.sampleStart;

					auto subr = rg::make_subrange(startIt, lp.vertex.end())
						| pl::iterator_view
						| rgv::transform([
								&lp,
								nextAfterBeginLight = std::next(lp.vertex.begin()),
								groupIndex = lp.startedFrom->getGroupIndex()
							](auto it){ return std::make_tuple(std::ref(lp), it, std::ref(it->state.interaction), nextAfterBeginLight, groupIndex); });

					return rg::yield_from(subr);
				});

			connectionsRange
				| rgv::transform(pl::applier_visitor([&]<typename LightInteraction_>(
					auto &lp,
					auto lit,
					const LightInteraction_ &li,
					const auto nextAfterBeginLight,
					const auto groupIndex)
						-> Contribution
				{
					auto &lightVertex = *lit;

					constexpr bool isLightSurface = std::same_as<LightInteraction_, SurfaceInteraction>;

					const IBxDF *lbxdf = nullptr;

					if(lit != lp.vertex.begin())
						if constexpr(isLightSurface)
							lbxdf = std::to_address(li.getBxDF());

					const bool lightMediumCond = isLightSurface || li.enteredSub->getMaterial().isSamplingDirectLightMedium();
					const bool camMediumCond = isSurface || interaction.enteredSub->getMaterial().isSamplingDirectLightMedium();
					const bool subsCond = [&]
						{
							bool cond = li.enteredSub != interaction.enteredSub;

							if constexpr(isLightSurface)
								cond &= li.nextSubstance != interaction.enteredSub;
							if constexpr(isSurface)
								cond &= li.enteredSub != interaction.nextSubstance;

							return cond;
						}();

					if((!lbxdf && isLightSurface) ||
								lightVertex.state.delta ||
								( (!lightMediumCond || !camMediumCond) && subsCond )
							) [[unlikely]]
						return {};

					auto &prevLvt = *std::prev(lit);

					const auto dir = (li.worldPos() - interaction.worldPos()).as_dir() /oprtnormalize;
					if(!pl::is_valid(dir)) [[unlikely]]
						return {};

					const EnumHemisphere
						hemisphereC = interaction.getHemisphere(dir),
						hemisphereL = li.getHemisphere(-dir);

					// Connection contribution
					auto connColor = interaction.radiance(EnumTransportMode::Radiance, camVertexState.dir, dir, hemisphereC);
					const auto liColor = li.radiance(EnumTransportMode::Importance, prevLvt.state.dir, -dir, hemisphereL);

					pl::assert_valid( connColor.color *= liColor.color );

					if(isAlmostBlack(connColor.color))
						return {};

					// Russian roulette
					const auto rrFactor = std::invoke([&]() -> Real
						{
							if(const auto contProb = pl::plmin(settings.getCameraPathWalker().getContinuationProb(connColor.albedo), settings.getCameraPathWalker().getContinuationProb(liColor.albedo));
								contProb < 1)
							{
								if(uniformRealRng<Real>()(rng.mRng) >= contProb)
								{
									++nbConnRRend;
									return 0;
								}

								return contProb /oprtreciprocal;
							}

							return 1;
						});

					if(!rrFactor)
						return {};

					// Setup shadow ray

					const auto destination = li.safeIncrement(interaction.worldPos(), li.worldPos(), hemisphereL);
					shadowRay.origin = interaction.safeIncrement(destination, interaction.worldPos(), hemisphereC);
					shadowRay.setDestination(destination);

					if(!shadowRay.length
								|| !interaction.isSafeDir(shadowRay.dir)
								|| !li.isSafeDir(shadowRay.dir)
								|| !interaction.isSafeDir(dir)
								|| !li.isSafeDir(dir)
							) [[unlikely]]
						return {};

					++nbConn;

					shadowSubsStack = pathState.substanceStack;
					shadowSubsStack.updateSubstanceStack( shadowSubsStack.checkSubstanceEntry(hemisphereC) );

					const auto sres = traceShadow(EnumTransportMode::Radiance, shadowRay, *segment.end, tracer, shadowSubsStack, 100);
					connColor.color *= sres.color;

					statsCol[statscollection::ShadowRays](sres.nbTraced);

					// Add contribution if not in shadow
					if(isAlmostBlack(connColor.color))
					{
						statsCol[statscollection::ShadowRaysOccluded](sres.nbTraced);
						statsCol[statscollection::ConnOccluded](1);
						return {};
					}

					const auto G = shadowRay.length /opow2 /oprtreciprocal;

					const auto w = std::invoke([&]() -> Real
						{
							const auto &camPrevVertexState = *path.vertexState.back();
							MisWeight<Real> misCameraWeight{path.misWeight}, misLightWeight{lightVertex.misWeight};
							const auto trProb = sres.pdfTr * (1 - liColor.laPdf) * (1 - connColor.laPdf);

							if(segment.level > 0)
							{
								const auto pdfCameraPrevBackA = getBackPdfA(path, EnumTransportMode::Radiance, camVertexState, camPrevVertexState, dir, settings.getCameraPathWalker().getContinuationProb(connColor.albedo), segment.level);

								misCameraWeight.update(camPrevVertexState.pdfForA, pdfCameraPrevBackA, camVertexState.delta || camPrevVertexState.delta);
							}

							{
								const auto pdfCameraBackW = li.PDF(EnumTransportMode::Importance, lightVertex.state.dir, -dir).value() * settings.getCameraPathWalker().getContinuationProb(liColor.albedo) * trProb;
								const auto pdfCameraBackA = mis(pdfCameraBackW * G * interaction.geometricTermDir(shadowRay.dir));

								misCameraWeight.update(camVertexState.pdfForA, pdfCameraBackA, camVertexState.delta);
							}

							if(lit != lp.vertex.begin())
							{
								const auto pdfLightPrevBackA = getBackPdfA(lp, EnumTransportMode::Importance, lightVertex.state, prevLvt.state, -dir, settings.getCameraPathWalker().getContinuationProb(liColor.albedo), lit != nextAfterBeginLight, settings.getLightPortalSampleProb());

								misLightWeight.update(prevLvt.state.pdfForA, pdfLightPrevBackA, lightVertex.state.delta || prevLvt.state.delta);
							}

							{
								const auto pdfLightBackW = interaction.PDF(EnumTransportMode::Radiance, camVertexState.dir, dir).value() * settings.getCameraPathWalker().getContinuationProb(connColor.albedo) * trProb;
								const auto pdfLightBackA = mis(pdfLightBackW * G * li.geometricTermDir(shadowRay.dir));

								misLightWeight.update(lightVertex.state.pdfForA, pdfLightBackA, lightVertex.state.delta);
							}

							const auto w = (misCameraWeight.value + misLightWeight.value + 1) /oprtreciprocal;

							return pl::is_valid(w, valid::Weight) ? w : 0;
						});

					const auto totalWeight = connColor.color * G * lp.weight * w;

					return {
							.color = lightVertex.contrib * totalWeight,
							.rrColor = lightVertex.rrContrib * totalWeight,
							.rrFactor = rrFactor,
							.groupIndex = groupIndex
						};
				}))
				| accumulateContribution(contrib);

			statsCol[statscollection::Conn](nbConn);
			statsCol[statscollection::ConnRR](nbConnRRend);
		}

	} // namespace

	void IntegratorBidir::traceCameraPath(
		const Contributor &contrib,
		TracerParams &tracer,
		Path<ICamera> &path,
		RayBatchEntry &rbEntry,
		TraversalEntry &trEntry,
		Real distributionFactor,
		Real rayImportance)
	{
		PRT_SCOPED_PROFILE(mProfiler, tag_CameraPath);

		if(!mFactory.getMaxPathLengthCamera())
			return;

		const auto sampleLights = [&](
				const Contributor &contrib,
				SubstanceStack &shadowSubsStack,
				BidirPathState<ICamera> &pathState,
				const Ray &oray,
				const PathSegment &segment,
				const auto &interaction)
			{
				const auto curNbSamplesPerLights = pl::plmax<uf32>(1, mFactory.getNbSamplesDirectPerLight() * pathState.distributionFactor);
				auto &path = *pathState.path;

				const auto handleLight = [&](const IEmitter &light, uf32 /*index*/, Real weight, Real pdf, RandomReals<2> rnd)
					{
						std::optional<MaterialInteraction> lpSurface;

						auto lsp = sampleLightDirect(
							rnd, light, oray,
							interaction, mLpSampleProbDL, std::to_address(mFactory.getLightPortalSampler()),
							std::to_address(mFactory.getLightSampler()), mScene->getInfiniteLights(),
							tracer.sampler);

						const bool isInfLight = light.isInfinite();

						if(!lsp || isAlmostBlack(lsp->radiance.color) || (!isInfLight && isAlmostBlack(lsp->sample.color)))
							return;

						shadowSubsStack = pathState.substanceStack;
						shadowSubsStack.updateSubstanceStack( shadowSubsStack.checkSubstanceEntry(lsp->hemisphere) );
						const auto sres = traceShadow(EnumTransportMode::Radiance, lsp->ray, interaction, tracer, shadowSubsStack, 100, &lpSurface);

						lsp->radiance.color *= (Color)sres.color;

						mStats[statscollection::ShadowRays](sres.nbTraced);

						if(isAlmostBlack(lsp->radiance.color) || (lsp->lp && (!sres.lp || sres.lp->getEmitter() != lsp->lp)))
						{
							mStats[statscollection::ShadowRaysOccluded](sres.nbTraced);
							return;
						}

						const auto sPdf = pdf * lsp->sample.pdf * lsp->sample.sensor.pdf;

						// Compute MIS weights
						const Real w = std::invoke([&]{
							const bool doBidirMis = mDoBidirMIS && isInfLight ?
								light.PDF(lsp->sample.sensor.interaction).value() > 0 :
								mDoBidirMIS;

							if(!doBidirMis)
							{
								return lsp->sample.delta ? 1 :
									misWeight2(
										sPdf,
										interaction.PDF(EnumTransportMode::Radiance, oray.dir, lsp->sample.dir).value() * sres.pdfTr * (1 - lsp->sample.sensor.pdf) * mFactory.getCameraPathWalker().getContinuationProb(lsp->radiance.albedo),
										mis
									);
							}
							else
							{
								MisWeight<Real> misCameraWeight{path.misWeight};
								const auto contProb = mFactory.getCameraPathWalker().getContinuationProb(lsp->radiance.albedo);
								const auto lpSampleProb = getLpSampleProb(light);
								auto &frontv = *path.vertexState.front();

								if(segment.level > 1)
								{
									const auto &backv = *path.vertexState.back();
									const auto pdfCameraPrevBackA = getBackPdfA(path, EnumTransportMode::Radiance, frontv, backv, lsp->sample.dir, contProb, segment.level);

									misCameraWeight.update(backv.pdfForA, pdfCameraPrevBackA, frontv.delta || backv.delta);
								}

								mDirectLightPath.startedFrom = &light;
								mDirectLightPath.lpSurface = lsp->lp ? &lsp->sample.sensor.interaction : (sres.lp && mLpSampleProb > 0 ? &lpSurface.value() : nullptr);
								mDirectLightPath.lp = sres.lp && mLpSampleProb > 0 ? sres.lp->getEmitter() : nullptr;
								mDirectLightPath.infPdfTr = sres.infPdfTr;

								const pl::fast_scoped_assignment bDir{frontv.dir, -lsp->sample.dir};
								const pl::fast_scoped_assignment bPdfTr{frontv.pdfTr, sres.pdfTr * (1 - lsp->sample.sensor.pdf)};
								const pl::fast_scoped_assignment bCP{frontv.contProb, 1};
								const pl::fast_scoped_assignment bIL{frontv.insideLayer, false};

								const VertexState lv{.dir{}, .interaction{lsp->sample.sensor.interaction}};

								{
									const auto pdfCameraBackA = getForPdfA(mDirectLightPath, EnumTransportMode::Importance, frontv, &lv, 0, mScene->getInfiniteLights(), lpSampleProb);

									misCameraWeight.update(frontv.pdfForA, pdfCameraBackA, frontv.delta);
								}

								const auto pdfLightBackA = getBackPdfA(mDirectLightPath, EnumTransportMode::Importance, frontv, lv, -bDir.old_value(), contProb, 0, lpSampleProb);
								const auto pdfLightForA = std::invoke([&]() -> Real {
										if(!isInfLight)
											return light.PDFW(lsp->sample.sensor.interaction, lsp->sample.dir).value() * pdf;

										const auto [nb, ipdfW, ipdfS] = infiniteLightsPDF(mScene->getInfiniteLights(), *mFactory.getLightSampler(), nullptr, *mLightSamplingSurface, lsp->sample.dir);
										const auto pdfLightW = (ipdfW * ipdfS) / nb;

										return pl::pllerp(pdfLightW, !mDirectLightPath.lpSurface ? 0 : mDirectLightPath.lp->PDF(*mDirectLightPath.lpSurface).value() * (ipdfS / nb) * mFactory.getLightPortalSampler()->PDF(*mLightSamplingSurface, *mDirectLightPath.lp).value(), lpSampleProb);
									});
								const auto misLightWeight = pdfLightForA /oprtreciprocal * pdfLightBackA;

								const auto w = (misCameraWeight.value + misLightWeight + 1) /oprtreciprocal;
								return pl::is_valid(w, valid::Weight) ? w : 0;
							}
						});

						const auto contribWeight = lsp->radiance.color * weight * w * (sPdf /oprtreciprocal);
						if(isAlmostBlack(contribWeight))
							return;

						contrib( lsp->sample.color * contribWeight, light.getGroupIndex() );

						if(isInfLight && !lsp->sample.delta)
						{
							for(const auto &l : mScene->getInfiniteLights())
								if(!pl::same_underlying_address<IEmitter>(l, &light))
									contrib( l->radiance(*segment.end, lsp->ray.dir) * contribWeight, l->getGroupIndex() );
						}

					};

				// Sample light sources.
				mFactory.getLightSampler()->sample(*segment.end, mFactory.getNbSamplesDirectLight(), curNbSamplesPerLights, tracer.sampler,
					std::ref(handleLight)
				);
			};

		// Initialize path state

		SubstanceStack shadowSubsStack;
		BidirPathState<ICamera> pathState{
				{
					.importance = rayImportance,
					.distributionFactor = distributionFactor,
					.mode = EnumTransportMode::Radiance,
					.substanceStack = path.startedFrom->getSubstanceStack()
				},
				&path
			};

		path.vertexState.front().emplace(VertexState{
				.dir{},
				.interaction = *trEntry.segment.start,
				.delta = true
			});

		PRT_SCOPED_PROFILE(Profiler::raytrace, Profiler::tag::PathWalk);

		mFactory.getCameraPathWalker().integrateAndTracePath(

			contrib.scale({.weight = trEntry.color}), pathState, rbEntry.ray, trEntry.segment, tracer,

			[&] [[nodiscard]] (auto&&... args) -> auto& { return mCameraInteractions.emplace_front(PL_FWD(args)...); },

			//////////////////////////////////////////////////////
			// Prepare path state
			//////////////////////////////////////////////////////
			pl::do_nothing,

			//////////////////////////////////////////////////////
			// Path split
			//////////////////////////////////////////////////////
			[](BidirPathState<ICamera> &newPathState, const Ray &/*ray*/, const PathSegment &/*segment*/, auto&& fc)
			{
				Path<ICamera> newPath{*newPathState.path};
				newPathState.path = &newPath;

				// Continue recursion
				PL_FWD(fc)();
			},

			//////////////////////////////////////////////////////
			// Path pass-through
			//////////////////////////////////////////////////////
			pl::do_nothing,

			//////////////////////////////////////////////////////
			// Path contribution
			//////////////////////////////////////////////////////
			[&]<typename Interaction_> [[nodiscard]] (const Contributor &contrib, BidirPathState<ICamera> &pathState, const Ray &ray, PathSegment &segment, const Interaction_ &interaction)
			{
				constexpr bool isSurface = std::same_as<Interaction_, SurfaceInteraction>;

				//segment.end->checkIfDelta();

				// Fill vertex information
				if(mDoBidirMIS)
					updateWeight(pathState, EnumTransportMode::Radiance, ray, segment, interaction);

				// Execute connection strategies
				if(!interaction.isDelta())
				{
					// Do direct lighting
					if(mFactory.hasLights() && (isSurface || interaction.enteredSub->getMaterial().isSamplingDirectLightMedium()) &&
						(!mDoBidirMIS || !mLightPaths.empty())
					)
					{
						PRT_SCOPED_PROFILE(mProfiler, tag_DirectLight);
						sampleLights(contrib, shadowSubsStack, pathState, ray, segment, interaction);
					}

					// Connect to light path
					if(!mLightPaths.empty() && mDoBidirMIS)
					{
						PRT_SCOPED_PROFILE(mProfiler, tag_Connections);
						connectCameraToLight(mLightPaths, mFactory, mStats, contrib, tracer, shadowSubsStack, pathState, segment, interaction);
					}
				}

				// Emission contribution
				{
					PRT_SCOPED_PROFILE(mProfiler, tag_Emission);

					if(const Color ecolor = interaction.emission(-ray.dir); !isAlmostBlack(ecolor))
					{
						const auto light = interaction.instance()->getEmitter();

						const auto w = std::invoke([&]() -> Real {
							// Skip MIS if current vertex is connected to camera
							if(!light || !segment.level)
								return 1;

							const auto &path = *pathState.path;

							if(mLightPaths.empty() || !mDoBidirMIS)
							{
								const bool isDeltaRay = segment.isType(EnumRayType::Specular | EnumRayType::Primary);
								const bool doDirectMis = !isDeltaRay && mFactory.getLightSampler();

								if(!doDirectMis)
									return 1;

								// Possibly fallback to unidirectionnal path-tracing MIS
								return misWeight2(
									segment.pdfW * segment.pdfTr * segment.contProb,
									light->PDFA(*segment.end, *segment.start, ray.dir).value() * mFactory.getLightSampler()->PDF(*trEntry.segment.start, *light).value(),
									mis
								);
							}
							else
							{
								MisWeight<Real> misCameraWeight{path.misWeight};
								const auto &frontv = *path.vertexState.front();
								const auto &backv = *path.vertexState.back();

								backv.visit([&](const auto &bi)
								{
									const auto lightPdfW = interaction.emissionPDF(-ray.dir).value();
									const auto pdfCameraPrevBackA =
										mis((frontv.insideLayer ? lightPdfW :
											pdfWtoA(
												lightPdfW,
												frontv.distSqr,
												bi.geometricTermDir(frontv.dir)
											)) * segment.pdfTr);

									misCameraWeight.update(backv.pdfForA, pdfCameraPrevBackA, frontv.delta || backv.delta);
								});

								{
									const auto pdfCameraBackA = mis(
											light->PDF(*segment.end).value() * mFactory.getLightSampler()->PDF(*trEntry.segment.start, *light).value()
										);

									misCameraWeight.update(frontv.pdfForA, pdfCameraBackA, frontv.delta);
								}

								const auto w = (misCameraWeight.value + 1) /oprtreciprocal;
								return pl::is_valid(w, valid::Weight) ? w : 0;
							}
						});

						contrib(ecolor * w, light ? light->getGroupIndex() : 0);
					}
				}

				return true;
			},

			//////////////////////////////////////////////////////
			// Path end
			//////////////////////////////////////////////////////
			[&](const Contributor &contrib, BidirPathState<ICamera> &pathState, const Ray &ray, const PathSegment &segment)
			{
				mStats[statscollection::Path](pathState.nbBounces + 1);

				if(!segment.end && !segment.insideLayer)
				{
					PRT_SCOPED_PROFILE(mProfiler, tag_Background);

					// Background contribution
					if(const auto bg = std::to_address(mScene->getBackground()); bg)
						contrib( bg->map(*segment.start, ray.dir), 0 );

					const bool isDeltaRay = segment.isType(EnumRayType::Specular | EnumRayType::Primary);

					// Infinite lights contribution
					const auto &infLights = mScene->getInfiniteLights();

					if(!infLights.empty())
					{
						const auto w = std::invoke([&]() -> Real {
							if(!segment.level)
								return 1;

							if(mLightPaths.empty() || !mDoBidirMIS ||
								!infLights[0]->PDF(pl::visit_one(*segment.start, [&](const auto &i){
										return PointInteraction{.mWorldPos = i.worldPos()};
									}))
								)
							{
								if(!mFactory.getLightSampler() || isDeltaRay)
									return 1;

								// Possibly fallback to unidirectionnal path-tracing MIS
								const auto [pdfLpW, pdfLpS] = std::invoke([&]{
										if(!segment.lpSurface)
											return std::make_pair(Real{}, Real{});

										return pl::visit_one(*segment.lpSurface, [&](const auto &i)
											{
												return std::make_pair(
													pdfAtoW(
															i.instance()->getEmitter()->PDF(*segment.lpSurface).value(),
															geometricTermPosEpsilon(i.localPos(), i.instance()->getEmitter()->worldToLocal(pl::visit_one(*segment.start, [](const auto &si){ return si.worldPos(); }))),
															i.geometricTermDir(ray.dir)
														) * mFactory.getLightPortalSampleProb(),
													mFactory.getLightPortalSampler()->PDF(*segment.start, *i.instance()->getEmitter()).value()
												);
											});
									});

								const auto [ipdfW, ipdfS] = std::invoke([&]{
										const auto [nb, ipdfW, ipdfS] = infiniteLightsPDF(infLights, *mFactory.getLightSampler(), nullptr, *segment.start, ray.dir);
										return std::pair<Real, Real>{ipdfW / nb * (1 - mFactory.getLightPortalSampleProb()), ipdfS / nb};
									});

								return segment.pdfW * segment.pdfTr * segment.contProb > (pdfLpW * pdfLpS >= ipdfW ? pdfLpW * ipdfS : ipdfW * ipdfS);
							}
							else
							{
								auto &path = *pathState.path;
								const auto lpSampleProb = getLpSampleProb(*infLights[0]);

								updateBackDendity(pathState, EnumTransportMode::Radiance, ray.dir, segment);

								const bool isDelta = segment.isType(EnumRayType::Specular);
								auto &frontv = *path.vertexState.front();

								mDirectLightPath.startedFrom = std::to_address(infLights[0]);
								mDirectLightPath.lpSurface = lpSampleProb > 0 ? segment.lpSurface : nullptr;
								mDirectLightPath.lp = mDirectLightPath.lpSurface ? pl::visit_one(*mDirectLightPath.lpSurface, [](const auto &i){ return i.instance()->getEmitter(); }) : nullptr;
								mDirectLightPath.infPdfTr = segment.infPdfTr;

								frontv.dir = -ray.dir;
								frontv.delta = false;
								frontv.contProb = 1;
								frontv.pdfTr = segment.pdfTr;
								const VertexState bv{
										.dir{},
										.interaction = PointInteraction{.mWorldPos = pl::visit_one(*segment.start, [&](const auto &i){ return i.worldPos(); })}
									};

								MisWeight<Real> misCameraWeight{path.misWeight};

								{

									const auto pdfCameraPrevBackA = getForPdfA(mDirectLightPath, EnumTransportMode::Importance, frontv, &bv, 0, mScene->getInfiniteLights(), lpSampleProb);

									misCameraWeight.update(frontv.pdfForA, pdfCameraPrevBackA, isDelta || frontv.delta);
								}

								{
									const auto [nb, ipdfWt, ipdfSt] = infiniteLightsPDF(infLights, *mFactory.getLightSampler(), nullptr, *segment.start, ray.dir);
									const auto ipdfW = ipdfWt / nb;
									const auto ipdfS = ipdfSt / nb;

									const auto [pdfLpBack, pdfLpFor] = std::invoke([&]{
											if(!mDirectLightPath.lpSurface)
												return std::make_pair(Real{}, Real{});

											return pl::visit_one(*mDirectLightPath.lpSurface, [&](const auto &i)
											{
												return std::make_pair(
													i.instance()->getEmitter()->PDF(*mDirectLightPath.lpSurface).value() * mFactory.getLightPortalSampler()->PDF(*mDirectLightPath.lpSurface, *mDirectLightPath.lp).value() * ipdfS,
													pdfWtoA(
															segment.pdfW,
															geometricTermPosEpsilon(i.localPos(), i.instance()->getEmitter()->worldToLocal(std::get<PointInteraction>(bv.interaction).worldPos())),
															i.geometricTermDir(ray.dir) * (segment.pdfTr / segment.infPdfTr)
														)
												);
											});
										});

									const auto pdfCameraBackA = mis(pl::pllerp(
										ipdfW * ipdfS,
										pdfLpBack,
										lpSampleProb));
									const auto pdfCameraForA = mis((isDelta ? 1 : pl::pllerp(
										segment.pdfW * segment.pdfTr,
										pdfLpFor,
										lpSampleProb)) * segment.contProb);

									misCameraWeight.update(pdfCameraForA, pdfCameraBackA, isDelta);
								}

								const auto w = (misCameraWeight.value + 1) /oprtreciprocal;

								return pl::is_valid(w, valid::Weight) ? w : 0;
							}
						});

						if(w > 0)
							for(const auto &l : infLights)
								contrib(l->radiance(*segment.start, ray.dir) * w, l->getGroupIndex());
					}
				}

				if(mContribution.isBlack())
					mStats[statscollection::ZeroContribPath](1);
			}

		);
	}

	void IntegratorBidir::traceLightPaths(TracerParams &tracer, const MaterialInteraction &cameraSurface)
	{
		mLightSamplingSurface = &cameraSurface;

		if(mFactory.hasLights() && mFactory.getMaxPathLengthLight())
		{
			PRT_SCOPED_PROFILE(mProfiler, tag_LightPath);

			const auto handleLight = [&](const IEmitter &light, auto /*index*/, ValidWeight<Real> weight, ValidWeight<Real> pdf, RandomReals<2> rnd)
				{
					auto &p = mLightPaths.emplace_front(light, pdf, weight, mMemoryArena);

					traceLightPath(tracer, p, rnd);
				};

			mFactory.getLightSampler()->sample(cameraSurface, mFactory.getNbSamplesLight(), 1, tracer.sampler,
				std::ref(handleLight)
			);
		}
	}

	// Light tracing integration mode
	void IntegratorBidir::integrate(Real /*rayImportance*/, Real /*rayDistributionFactor*/, u32 nbSamples, PhotonSampleList &, TracerParams &tracer)
	{
		//if(mMutators.empty())
		//{
			//const auto nbSamples = photonSamples.list[0].individualSamples.capacity();

			MaterialInteraction sd{PointInteraction{}};

			pl::repeat(nbSamples, [&]{
					pl::scoped_guard arenaGuard{[&]{
						clearLightPath();
						mMemoryArena.clear();
					}};

					// Trace light paths
					traceLightPaths(tracer, sd);
				});

			mStats[statscollection::Sample](nbSamples);
		/*}
		else
		{
			for(auto &pb : photonSamples.list)
			{
				const auto &camera = *pb.contribBuffer->getCamera();
				const auto &camSubStack = camera.getSubstanceStack();

				FilmTile filmTile{
					pb.contribBuffer->getRenderRect(),
					rayImportance,
					0,
					pb,
					&camera
				};

				mCameraPath.startedFrom = &camera;

				std::array<Point2Di, 1> spPos;
				RayBatch rayBatch;

				const auto integrate =
					[&](auto &collision)
					{
						// Trace light paths
						traceLightPaths(tracer, photonSamples);

						// Trace camera paths
						return traceCameraPath(tracer, mCameraPath, collision.ray, collision.segment, rayDistributionFactor);
					};

				// Sample a pixel
				samplePixels(
					tracer,
					rayBatch,
					mFactory.isJittering(),
					true,
					camSubStack.back(),
					filmTile,
					spPos.size(),
					spPos.data(),

					[&](auto &collision, const auto index)
						{
							tracer.sampler.clearMainRecordedStream();
							tracer.sampler.appendStream(index + 1);

							// Trace first path
							auto pathColor = integrate(collision);

							// Trace mutated paths
							pb.nbSamples = sampleMutations(
								mMutators,
								camSubStack.back(),
								mFactory.isJittering(),
								true,
								false,
								filmTile,
								tracer,
								spPos[index],
								pathColor,
								integrate
							);
						}
				);

				mStats[statscollection::Sample](pb.nbSamples);
			}
		}*/
	}

	// Tile integration mode
	void IntegratorBidir::integrate(PixelSampleTileLayers &samples, TracerParams &tracer)
	{
		if(samples.tiles.empty() || !mFactory.getMaxPathLengthCamera())
		{
			MaterialInteraction sd{PointInteraction{}};
			const auto nb = mFactory.getNbSamplesPixel() * samples.tileDims.area();

			pl::repeat(nb, [&]{
					pl::scoped_guard arenaGuard{[&]{
						clearLightPath();
						mMemoryArena.clear();
					}};

					// Trace light paths
					traceLightPaths(tracer, sd);
				});

			mStats[statscollection::Sample](nb);

			return;
		}

		const auto &camSubStack = samples.camera->getSubstanceStack();

		const auto integrate =
			[&](const Contributor &contrib, auto &collision, auto &trEntry)
			{
				// Trace light paths
				traceLightPaths(tracer, *trEntry.segment.start);

				Path<ICamera> cameraPath;
				cameraPath.startedFrom = samples.camera;

				// Trace camera paths
				traceCameraPath(contrib, tracer, cameraPath, collision, trEntry, samples.rayDistributionFactor, samples.rayImportance);
			};

		samples.addSample();

		// Sample pixels
		samplePixels(
			tracer,
			mFactory.isJittering(),
			mFactory.useRandomPixel(),
			camSubStack.back(),
			samples,
			mFactory.getNbSamplesPixel(),

			[&] [[nodiscard]] (auto&&... args) -> auto& { return mCameraInteractions.emplace_front(PL_FWD(args)...); },

			[&]{ mCameraInteractions.clear(); mMemoryArenaCameraInteractions.clear(); },

			[&](auto &collision, auto &trEntry, const auto spPos, const auto index)
				{
					pl::scoped_guard arenaGuard{[&]{
						clearLightPath();
						mMemoryArena.clear();
					}};

					tracer.sampler.clearMainRecordedStream();
					tracer.sampler.appendStream(index + 1);

					{
						mContribution.reset();
						integrate({mContribution, {.weight = CReal{1} / mFactory.getNbSamplesPixel()}}, collision, trEntry);
					}

					for(const auto i : mir(mContribution.size()))
						samples.addContribution(spPos, mContribution.getrr(i), i);

					//auto pathColor = mContribution.rrtotal();

					// Trace mutated paths
					/*if(!mMutators.empty())
					{
						sampleMutations(
							mMutators,
							camSubStack.back(),
							mFactory.isJittering(),
							mFactory.useRandomPixel(),
							true,
							samples,
							tracer,
							spPos[index],
							pathColor,
							integrate
						);

						for(auto &w : samples.weightRange(rect.area()))
							++w;
					}
					else
					{*/
						//samples.addSample(spPos[index], pathColor, 1);
					//}
				}
		);

		mStats[statscollection::Sample](mFactory.getNbSamplesPixel() * samples.tileDims.area());
	}

	void IntegratorBidir::traceLightPath(TracerParams &tracer, Path<IEmitter> &path, RandomReals<2> rnd)
	{
		if(!mFactory.getMaxPathLengthLight())
			return;

		auto &rng = tracer.sampler;

		// Increase sample count for all cameras.
		if(mFactory.isConnectingLightToCamera())
			for(auto &b : mPhotonBuffers)
				b->addSamples();


		// Sample the light source and generate ray.

		const auto &light = *pl::assert_valid(path.startedFrom);
		Ray ray;
		const bool isInfinite = light.isInfinite();
		Real lpPdfS = 1;
		const auto lpSampleProb = getLpSampleProb(light);

		auto lightSample = std::invoke([&]() -> pl::marked_optional<SampledSensorOut>
			{
				if(!lpSampleProb || rng() >= lpSampleProb)
					return light.sampleOut(rng, {uniformRealRng<>()(rng), uniformRealRng<>()(rng)}, rnd);

				const auto lpSample = mFactory.getLightPortalSampler()->sampleOne(*mLightSamplingSurface, rng);
				if(!lpSample.value)
					return pl::nullopt;

				path.lp = lpSample.value;
				lpPdfS = lpSample.pdf;

				auto areaSp = path.lp->sampleArea(rng, {uniformRealRng<>()(rng), uniformRealRng<>()(rng)});
				if(!areaSp)
					return pl::nullopt;

				auto dirSp = light.sampleOutDir(*areaSp, rng, rnd);
				if(!dirSp)
					return pl::nullopt;

				path.lpSurface = &mLightInteractions.emplace_front(areaSp->interaction);

				return SampledSensorOut{
						.sensor = std::move(*areaSp),
						.dir = std::move(*dirSp)
					};
			});

		if(!lightSample)
			return;

		const auto &lightSubsStack = light.getSubstanceStack();
		pl::visit_one(lightSample->sensor.interaction, [&](auto &i){ i.initialSurface(lightSubsStack.back()); });

		ray.setupPhoton();
		const bool lsDelta = lightSample->dir.delta;

		if(lsDelta)
			ray.type = ray.type | EnumRayType::Specular;

		ray.dir = lightSample->dir.dir;
		if(isInfinite)
			ray.origin = lightSample->dir.origin;
		else
		{
			pl::visit_one(lightSample->sensor.interaction, [&](const auto &i){
					ray.origin = i.safeIncrement(lightSample->dir.origin, lightSample->dir.origin, lightSample->dir.hemisphere);
				});
		}

		// Initialize path state.

		BidirPathState<IEmitter> pathState{
				{
					.mode = EnumTransportMode::Importance,
					.substanceStack = lightSubsStack
				},
				&path
			};
		const auto lightGroupIndex = light.getGroupIndex();
		const auto lightPdfForA = std::invoke([&]() -> Real
			{
				// Make adjustments for infinite light sources.
				if(isInfinite)
				{
					if(!lsDelta)
					{
						const auto [nb, icolor, ipdfW, ipdfS] = infiniteLightsContribution(mScene->getInfiniteLights(), *mFactory.getLightSampler(), &light, *mLightSamplingSurface, -lightSample->dir.dir);

						lightSample->dir.color += (Color)icolor;
						lightSample->dir.pdf = (lightSample->dir.pdf + ipdfW) / (nb + 1);
						path.pdf = (path.pdf + ipdfS) / (nb + 1);
					}

					if(mDoBidirMIS)
					{
						return path.lp ?
							mis(pl::pllerp(lightSample->dir.pdf, lightSample->sensor.pdf * lpPdfS, lpSampleProb) * path.pdf) :
							mis(lightSample->dir.pdf * path.pdf);
					}

					return 0;
				}

				if(mDoBidirMIS)
					return mis(lightSample->sensor.pdf * path.pdf);

				return 0;
			});

		// Generate first vertex.

		if(mDoBidirMIS)
		{
			path.vertex.push_back({
					.state{
						.pdfForA = lightPdfForA,
						.dir = ray.dir,
						.interaction = lightSample->sensor.interaction,
						.delta = lsDelta
					},
					.contrib = lightSample->dir.color,
					.rrContrib = lightSample->dir.color,
					.misWeight = 1
				});

			path.vertexState.front().emplace(path.vertex.front().state);

			path.deltaSource = lsDelta || !light.hasArea();
			path.sampleStart = path.vertex.begin();
		}

		pl::assert_valid(lightSample->dir.color);


		// Initialize first segment and surface.

		SubstanceStack shadowSubsStack;
		PathSegment segment{
				&lightSample->sensor.interaction,
				lightSample->dir.pdf,
				ray.type
			};

		{
			const Real sPdf = (path.lp ? path.lp->worldPDF(lightSample->sensor.interaction).value() * lpPdfS : lightSample->sensor.pdf) * path.pdf * lightSample->dir.pdf;

			pl::assert_valid( lightSample->dir.color *= sPdf /oprtreciprocal );
		}

		// Walk the paths.

		PRT_SCOPED_PROFILE(Profiler::raytrace, Profiler::tag::PathWalk);

		if(path.lp)
		{
			pl::visit_one(lightSample->sensor.interaction, [&](const auto &i){
					ray.setDestination( i.safeIncrement(ray.origin, i.worldPos(), lightSample->dir.hemisphere) );
				});

			if(auto res = tracer.rayTracer.traceRay(ray); res)
			{
				segment.end = &mLightInteractions.emplace_front(collisionToShadedInteraction(*res, ray.dir, true));

				std::get<SurfaceInteraction>(*segment.end).initialSurface(lightSubsStack.back());

				segment = mFactory.getLightPathWalker().traceLine(
						pathState, ray, segment, tracer,

						[&](auto&&... args) -> auto& { return mLightInteractions.emplace_front(PL_FWD(args)...); },

						[&](BidirPathState<IEmitter> &pathState, const PathSegment &segment) -> bool
						{
							const bool isLp = !lsDelta && lpSampleProb > 0 && segment.end &&
								std::get<SurfaceInteraction>(*segment.end).surface().objProps &&
								std::get<SurfaceInteraction>(*segment.end).surface().objProps->isLightPortal();

							if(isLp)
								pathState.throughput.set_zero();

							return !isLp;
						}
					);
			}

			if(isAlmostBlack(pathState.throughput))
			{
				mStats[statscollection::LightPath](1);
				return;
			}

			pl::visit_one(lightSample->sensor.interaction, [&](auto &i){
					if constexpr(pl::is_a<decltype(i), SurfaceInteraction>)
						pathState.substanceStack.interaction(i);

					ray.origin = i.safeIncrement(ray.origin, i.worldPos(), swapHemisphere(lightSample->dir.hemisphere));
				});

			ray.dir = lightSample->dir.dir;
			ray.length = Ray::MaxDistance;

			path.infPdfTr = segment.pdfTr;

			segment.startsAt(&lightSample->sensor.interaction);
			segment.infPdfTr = segment.pdfTr;

			pathState.substanceStack.updateSubstanceStack(pathState.substanceStack.checkSubstanceEntry(EnumHemisphere::South));

			if(auto res = tracer.rayTracer.traceRay(ray); !res)
			{
				mStats[statscollection::LightPath](1);
				return;
			}
			else
				segment.end = &mLightInteractions.emplace_front(collisionToShadedInteraction(*res, ray.dir, true));
		}
		else
		{
			if(auto res = tracer.rayTracer.traceRay(ray); !res)
			{
				mStats[statscollection::LightPath](1);
				return;
			}
			else
			{
				segment.end = &mLightInteractions.emplace_front(collisionToShadedInteraction(*res, ray.dir, true));

				std::get<SurfaceInteraction>(*segment.end).initialSurface(lightSubsStack.back());
			}
		}

		const MaterialInteraction *lpSurface = nullptr;

		Contributor pathContrib(mContribution);
		mFactory.getLightPathWalker().integrateAndTracePath(

			pathContrib, pathState, ray, segment, tracer,

			[&] [[nodiscard]] (auto&&... args) -> auto& { return mLightInteractions.emplace_front(PL_FWD(args)...); },

			//////////////////////////////////////////////////////
			// Prepare path state
			//////////////////////////////////////////////////////
			pl::do_nothing,

			//////////////////////////////////////////////////////
			// Path split
			//////////////////////////////////////////////////////
			[&](BidirPathState<IEmitter> &newPathState, const Ray &/*ray*/, const PathSegment &/*segment*/, auto&& fc)
			{
				// Create new path and copy current path.
				auto &newPath = mLightPaths.emplace_front(*newPathState.path);
				newPathState.path = &newPath;
				newPath.sampleStart = newPathState.currentIterator();

				// Continue recursion.
				PL_FWD(fc)();
			},

			//////////////////////////////////////////////////////
			// Path pass-through
			//////////////////////////////////////////////////////
			[&](BidirPathState<IEmitter> &, const Ray &, PathSegment &segment)
			{
				pl::visit_one(*segment.end, [&](const auto &i){
					if constexpr(pl::is_a<decltype(i), SurfaceInteraction>)
					{
						if(!lpSurface && segment.level == 0 && !lsDelta && lpSampleProb > 0 && i.surface().objProps && i.surface().objProps->isLightPortal())
							lpSurface = segment.end;
					}
				});
			},

			//////////////////////////////////////////////////////
			// Path contribution
			//////////////////////////////////////////////////////
			[&]<typename Interaction_> [[nodiscard]] (const Contributor &/*contrib*/, BidirPathState<IEmitter> &pathState, const Ray &ray, PathSegment &segment, const Interaction_ &interaction)
			{
				constexpr bool isSurface = std::same_as<Interaction_, SurfaceInteraction>;

				if(isInfinite)
				{
					if(!light.PDF(pl::visit_one(*segment.start, [&](const auto &i){
								return PointInteraction{.mWorldPos = i.worldPos()};
							})))
						return false;

					if(segment.level == 0 && lpSampleProb > 0)
					{
						auto &path = *pathState.path;

						if(!path.lp)
						{
							if(lpSurface)
							{
								path.lpSurface = lpSurface;
								path.lp = std::get<SurfaceInteraction>(*lpSurface).instance()->getEmitter();
								path.infPdfTr = segment.pdfTr;

								if(mDoBidirMIS)
									path.vertex.front().state.pdfForA = mis(pl::pllerp(path.vertex.front().state.pdfForA, path.lp->PDF(*lpSurface).value() * mFactory.getLightPortalSampler()->PDF(*mLightSamplingSurface, *path.lp).value(), lpSampleProb) * path.pdf);
							}
							else
							{
								if(mDoBidirMIS)
									path.vertex.front().state.pdfForA *= 1 - lpSampleProb;

								lightSample->sensor.pdf *= 1 - lpSampleProb;
								lightSample->dir.color /= 1 - lpSampleProb;
							}

							if(mDoBidirMIS)
								path.vertexState.front()->pdfForA = path.vertex.front().state.pdfForA;
						}
					}
				}

				const auto contrib = pathState.throughput * (lightSample->dir.color * pathState.weight);
				const auto rrContrib = contrib * pathState.rrFactor;

				// Fill vertex information.
				if(mDoBidirMIS)
				{
					updateWeight(pathState, EnumTransportMode::Importance, ray, segment, interaction, lpSampleProb, lightSample->sensor.pdf);

					if(ray.isType(EnumRayType::Transparency))
						pathState.path->vertex.pop_back();

					pathState.storeCurrent(contrib, rrContrib);
				}

				// Sample cameras directly.
				if(
					mFactory.isConnectingLightToCamera() && !interaction.isDelta() &&
					(isSurface || interaction.enteredSub->getMaterial().isSamplingDirectLightMedium())
				)
				{
					PRT_SCOPED_PROFILE(mProfiler, tag_LightToCamera);

					for(auto &pb : mPhotonBuffers)
					{
						const auto &camera = *pb->getCamera();

						// Sample camera.
						const auto csp = sampleCameraDirect({uniformRealRng<>()(rng), uniformRealRng<>()(rng)}, camera, light, rng, ray, interaction);
						if(!csp)
							continue;

						// Check visibility.
						shadowSubsStack = pathState.substanceStack;
						shadowSubsStack.updateSubstanceStack( shadowSubsStack.checkSubstanceEntry(csp->hemisphere) );
						const auto sres = traceShadow(EnumTransportMode::Importance, csp->ray, *segment.end, tracer, shadowSubsStack, 100);
						mStats[statscollection::ShadowRays](sres.nbTraced);

						PixelSample psp{
								.p = csp->sample.imagePoint,
								.weight = 1,
								.color = csp->radiance.color * csp->sample.sensorIn.color * (Color)sres.color,
								.layerIndex = lightGroupIndex
							};

						if(isAlmostBlack(psp.color))
						{
							mStats[statscollection::ShadowRaysOccluded](sres.nbTraced);
							continue;
						}

						const auto w = std::invoke([&]() -> Real
							{
								// MIS
								if(!mDoBidirMIS)
									return 1;

								auto &path = *pathState.path;
								MisWeight<Real> misWeight{path.misWeight};
								auto &frontv = *path.vertexState.front();

								{
									const auto &backv = *path.vertexState.back();
									const auto pdfLightPrevBackA = getBackPdfA(path, EnumTransportMode::Importance, frontv, backv, csp->ray.dir, mFactory.getLightPathWalker().getContinuationProb(csp->radiance.albedo), segment.level, lpSampleProb);

									misWeight.update(backv.pdfForA, pdfLightPrevBackA, frontv.delta || backv.delta);
								}

								{
									const auto pdfCameraW = camera.PDFW(csp->sample.sensorIn.sensor.interaction, csp->sample.sensorIn.dir).value();
									const auto pdfLightBackA =
										mis(pdfWtoA(
											pdfCameraW,
											csp->ray.length /opow2,
											interaction.geometricTermDir(csp->ray.dir)
										) * sres.pdfTr * (1 - csp->radiance.laPdf));

									misWeight.update(frontv.pdfForA, pdfLightBackA, frontv.delta);
								}

								const auto w = (misWeight.value + 1) /oprtreciprocal;
								return pl::is_valid(w, valid::Weight) ? w : 0;
							});

						pl::assert_valid( psp.color *= rrContrib * (pathState.weight * w * (csp->sample.sensorIn.pdf * csp->sample.sensorIn.sensor.pdf /oprtreciprocal)) );

						pb->addContribution(psp);

					}
				}

				return true;
			},

			//////////////////////////////////////////////////////
			// Path end
			//////////////////////////////////////////////////////
			[&](const Contributor &, BidirPathState<IEmitter> &pathState, const Ray &, const PathSegment &)
			{
				mStats[statscollection::LightPath](pathState.nbBounces + 1);
			}
		);

	}
}
