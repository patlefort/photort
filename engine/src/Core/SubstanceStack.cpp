/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/SubstanceStack.hpp>
#include <PhotoRT/Core/Material.hpp>
#include <PhotoRT/Core/Interaction.hpp>

namespace PhotoRT
{
	SubstanceEntryEvent SubstanceStack::checkSubstanceEntry(EnumHemisphere hemisphere) const noexcept
	{
		if(hemisphere == EnumHemisphere::South && mNextSubstance)
			return {
					.enterSubsEvent = mNextEnterSubsEvent,
					.exitSubsEvent = mNextExitSubsEvent
				};

		return {
				.enterSubsEvent = nullptr,
				.exitSubsEvent = nullptr
			};
	}

	void SubstanceStack::initialSubstance(const MaterialLayer &substance) noexcept
	{
		mNextSubstance = &substance;
		reset();
		mSubstances.push_back({&substance, substance.getMaterial().getRefractionPriority()});
	}

	void SubstanceStack::interaction(SurfaceInteraction &inter) noexcept
	{
		inter.enteredSub = getEnteredSub();

		mNextExitSubsEvent = nullptr;
		mNextEnterSubsEvent = nullptr;

		if(!inter.surface().matEntry)
		{
			mNextSubstance = inter.nextSubstance = nullptr;
			return;
		}

		const auto &curSubs = inter.surface().matEntry->material->mLayers.back();
		mNextSubstance = nullptr;

		if(curSubs.isSubstance())
		{
			if(hasSubstance(curSubs))
			{
				mNextExitSubsEvent = &curSubs;
				mNextSubstance = &back() == &curSubs ? &previous() : &back();
			}
			else
			{
				mNextEnterSubsEvent = &curSubs;
				mNextSubstance = curSubs.getMaterial().getRefractionPriority() >= back().getMaterial().getRefractionPriority() ?
					&curSubs :
					&back();
			}
		}

		inter.nextSubstance = mNextSubstance ? mNextSubstance : &curSubs;
	}

	void SubstanceStack::updateSubstanceStack(const SubstanceEntryEvent &evt) noexcept
	{
		if(evt.exitSubsEvent)
			remove(*evt.exitSubsEvent);

		if(evt.enterSubsEvent)
			push(*evt.enterSubsEvent, evt.enterSubsEvent->getMaterial().getRefractionPriority());

		//print(std::cout); std::cout << '\n';
	}

	void SubstanceStack::print(std::ostream &os) const
	{
		os << pl::io::range{pl::io::mode_text, ", ",
			mSubstances | rgv::transform([&](auto&& e){ return e.mat->getMaterial().getEntityId(); })
		};
	}
}