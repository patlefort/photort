/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Bitmap.hpp>
#include <PhotoRT/Bitmap/BitmapAlgo.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

#include <patlib/task.hpp>

namespace PhotoRT
{
	// Read-only bitmaps
	PRT_ENTITY_FACTORY_DEFINE(BitmapReadFiltered2D)
	PRT_ENTITY_FACTORY_DEFINE(BitmapReadInvert1D)
	PRT_ENTITY_FACTORY_DEFINE(BitmapReadInvert2D)
	PRT_ENTITY_FACTORY_DEFINE(BitmapReadInvert3D)


	BitmapBase::BitmapBase(const BitmapBase &o) = default;
	BitmapBase::BitmapBase(BitmapBase &&o) = default;

	BitmapBase &BitmapBase::operator= (const BitmapBase &o) = default;
	BitmapBase &BitmapBase::operator= (BitmapBase &&o) = default;

	BitmapBase &BitmapBase::operator-= (const BitmapBase &other)
	{
		const auto nb = other.getNbPixels() /omin/ getNbPixels();

		other | rgv::take_exactly(nb) |
			pl::transform
			(
				*this, begin(),
				[&](auto&& oc, auto&& ic) { return ic - oc; },
				std::identity{}, std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
			);

		return *this;
	}

	BitmapBase &BitmapBase::operator+= (const BitmapBase &other)
	{
		const auto nb = other.getNbPixels() /omin/ getNbPixels();

		other | rgv::take_exactly(nb) |
			pl::transform
			(
				*this, begin(),
				[&](auto&& oc, auto&& ic) { return ic + oc; },
				std::identity{}, std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
			);

		return *this;
	}

	BitmapBase &BitmapBase::operator/= (const BitmapBase &other)
	{
		const auto nb = other.getNbPixels() /omin/ getNbPixels();

		other | rgv::take_exactly(nb) |
			pl::transform
			(
				*this, begin(),
				[&](auto&& oc, auto&& ic) { return ic / oc; },
				std::identity{}, std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
			);

		return *this;
	}

	BitmapBase &BitmapBase::operator*= (const BitmapBase &other)
	{
		const auto nb = other.getNbPixels() /omin/ getNbPixels();

		other | rgv::take_exactly(nb) |
			pl::transform
			(
				*this, begin(),
				[&](auto&& oc, auto&& ic) { return ic * oc; },
				std::identity{}, std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
			);

		return *this;
	}

	Color BitmapBase::operator() (Real u) const noexcept
	{
		return mLinearFilter ? BitmapAlgo::getPixelLinearFilter(*this, u, mTiled) : getPixel(u);
	}

	Color BitmapBase::operator() (Real u, Real v) const noexcept
	{
		return mLinearFilter ? BitmapAlgo::getPixelLinearFilter(*this, u, v, mTiled) : getPixel(u, v);
	}

	Color BitmapBase::operator() (Real u, Real v, Real w) const noexcept
	{
		return getPixel(u, v, w);
	}

	BitmapBase &BitmapBase::copyData(const BitmapBase &other)
	{
		const auto nb = other.getNbPixels() /omin/ getNbPixels();

		other | rgv::take_exactly(nb) |
			pl::copy
			(
				begin(),
				pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
			);

		return *this;
	}

	void BitmapBase::fitToAspectRatio(if32 width, if32 height, szt maxNbPixels)
	{
		const auto newSize = BitmapAlgo::fitSizeToAspectRatio(width, height, maxNbPixels);

		allocate(Dir2Di{newSize[0], newSize[1]});
	}

	void BitmapBase::allocate(const Dir3Di dim)
	{
		allocMemory(dim);
	}

	BitmapBase &BitmapBase::copyData(const IBitmap1DRead &bitmap)
	{
		mir(bitmap.getNbPixels() /omin/ getNbPixels()) |
			pl::for_each
			(
				[&](const auto i){ this->setPixel(bitmap.getPixel(i), i); },
				std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
			);

		return *this;
	}

	BitmapBase &BitmapBase::copyData(const IBitmap2DRead &bitmap)
	{
		const pl::rectangle<> r{ {}, getRect().p2 /omin/ bitmap.getRect().p2 };

		range2d(r) | pl::iterator_view |
			pl::for_each
			(
				[&](auto i){ *i = bitmap.getPixel(i.getPosition()[size_v<0>], i.getPosition()[size_v<1>]); },
				std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
			);

		return *this;
	}

	BitmapBase &BitmapBase::copyData(const IBitmap3DRead &bitmap)
	{
		const pl::box<VecP3i> b{ {}, getBox().v2 /omin/ bitmap.getBox().v2 };

		range3d(b) | pl::iterator_view |
			pl::for_each
			(
				[&](auto i){ *i = bitmap.getPixel(i.getPosition()[size_v<0>], i.getPosition()[size_v<1>], i.getPosition()[size_v<2>]); },
				std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
			);

		return *this;
	}


	Color BitmapReadFiltered2D::getPixel(if32 u, if32 v) const noexcept
	{
		return BitmapAlgo::filterPixelBounded(*mBitmap, mFilter, u, v);
	}

	void BitmapReadFiltered2D::setParameters(const ParameterTree &params)
	{
		BitmapReadChainBase2D::setParameters(params);

		if(const auto p = params.findParam("filter"); p && !p->value.empty())
		{
			params.getValueIfExists("filter_size", mFilter.mSize);

			setFilter(addReference(type_v<IColorFilterDistribution2D>, *p));

			mFilter.initFilter();
		}
		else
			setFilter(nullptr);
	}

	ParameterTree BitmapReadFiltered2D::getParameters(bool deep) const
	{
		ParameterTree params {BitmapReadChainBase2D::getParameters(deep)};

		if(mFilter.mDistribution)
			params["filter"].fromEntityRef(*mFilter.mDistribution, deep);

		params["filter_size"] = mFilter.mSize;

		return params;
	}
}
