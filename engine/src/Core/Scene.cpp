/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Scene.hpp>
#include <PhotoRT/Core/Instance.hpp>
#include <PhotoRT/Core/PrimitiveGroup.hpp>
#include <PhotoRT/Core/Transform.hpp>
#include <PhotoRT/Core/Emitter.hpp>
#include <PhotoRT/Core/Camera.hpp>
#include <PhotoRT/Core/Material.hpp>
#include <PhotoRT/Core/Normals.hpp>
#include <PhotoRT/Core/Parser.hpp>

#include <patlib/scoped.hpp>
#include <patlib/task.hpp>

#include <fmt/format.h>
#include <fmt/ranges.h>
#include <fmt/ostream.h>

namespace PhotoRT
{
	Scene::Scene() = default;
	Scene::~Scene() = default;

	std::ostream &operator<<(std::ostream &os, const Scene &scene)
	{
		const pl::io::scoped_exceptions_state se{os, ~std::ios_base::goodbit};

		if(!scene.mParser)
			throw ExceptionEntityCollection("No parser given to serialize scene.");

		ParameterTree params {scene.getParameters(false)};

		params["entities"].hasChilds(true);

		auto &childs = *params["entities"].childParams;

		for(const auto &[i, entity] : rgv::enumerate(scene.mEntities.left | rgv::values))
		{
			childs[i].toEntity(entity->getEntityType(), entity->getEntityId());

			childs[i].hasChilds(true);
			*childs[i].childParams = entity->getParameters(false);
		}

		scene.mParser->save(params, os);

		return os;
	}

	std::istream &operator>>(std::istream &is, Scene &scene)
	{
		const pl::io::scoped_exceptions_state se{is, ~std::ios_base::goodbit};

		if(!scene.mParser)
			throw ExceptionEntityCollection("No parser given to parse scene.");

		pl::scoped_guard sg{[&]{ scene.mGlobalParamList.clear(); }};

		scene.mGlobalParamList = scene.mParser->load(is);

		if(const auto p = scene.mGlobalParamList.findParam("entities"); p && p->childParams)
		{
			std::string type, id;

			for(const auto &entity : *p->childParams)
			{
				entity.second.getEntityType(type, id);

				if(!type.empty() && scene.mEntities.left.find(id) == scene.mEntities.left.end())
					scene.createEntity(type, id, entity.second.childParams ? *entity.second.childParams : ParameterTree{});
			}
		}

		scene.setParameters(scene.mGlobalParamList);

		return is;
	}

	void Scene::saveToDirectory(const std::filesystem::path &path)
	{
		if(!mParser)
			throw ExceptionEntityCollection("No parser given to serialize scene.");

		setFilePath(path);

		ParameterTree params {getParameters(false)};

		auto dsf = std::make_shared<DataSourceFile>();

		const pl::fast_scoped_assignment ta{mDataSource, std::dynamic_pointer_cast<IDataSource>(dsf)};

		dsf->setFilePath(path);

		const auto fullPath = getFilePath() / "scene.prts";

		params["entities"].hasChilds(true);

		auto &childs = *params["entities"].childParams;

		for(const auto &[i, entity] : rgv::enumerate(mEntities.left | rgv::values))
		{
			childs[i].toEntity(entity->getEntityType(), entity->getEntityId());

			if(auto entityParams = entity->getParameters(false); !entityParams.empty())
				childs[i].setChildParams(std::move(entityParams));

			pl::log("Scene", "Serializing entity {}.", pl::io::quoted{entity->getEntityId()});
			mDataSource->serialize(*entity);
		}

		mParser->save(params, fullPath);
	}

	void Scene::loadFromDirectory(const std::filesystem::path &path)
	{
		if(!mParser)
			throw ExceptionEntityCollection("No parser given to parse scene.");

		setFilePath(path);

		auto dsf = std::make_shared<DataSourceFile>();

		const pl::fast_scoped_assignment ta{mDataSource, std::dynamic_pointer_cast<IDataSource>(dsf)};

		dsf->setFilePath(getFilePath());

		clearScene();

		ParameterTree params;

		mGlobalParamList.clear();
		mIncludedFiles.clear();
		mCreatedEntities.clear();

		const auto fullPath = getFilePath() / "scene.prts";

		mIncludedFiles.emplace(fullPath);

		params = mParser->load(fullPath);

		//params.print(std::cout);

		if(const auto p = params.findParam("include"); p && p->childParams)
			for(const auto &param : *p->childParams | rgv::values |
				rgv::remove_if([](auto&& p){ return p.value.empty(); })
			)
				addEntitiesFromFile(param.value, param.getValueOrDefault("prefix", std::string{}));

		if(const auto p = params.findParam("entities"); p && p->childParams)
		{
			const pl::scoped_swap tm{params, mGlobalParamList};

			addEntities(*p->childParams);
		}

		setParameters(params);

		mGlobalParamList.clear();
		mIncludedFiles.clear();
		mCreatedEntities.clear();
	}

	void Scene::setParameters(const ParameterTree &params)
	{
		setBackground(        createEntityIfParamExists(type_v<IColorMapper>, params, "background"));
		setAmbientSubstance(  createEntityIfParamExists(type_v<Material>, params, "ambient_substance"));
		setRootInstance(      createEntityIfParamExists(type_v<IObjectInstance>, params, "root_instance"));
	}

	ParameterTree Scene::getParameters(bool deep) const
	{
		ParameterTree params;

		if(mBackground)
			params["background"].fromEntityRef(*mBackground, deep);

		if(mAmbientSubstance)
			params["ambient_substance"].fromEntityRef(*mAmbientSubstance, deep);

		if(mRootInstance)
			params["root_instance"].fromEntityRef(*mRootInstance, deep);

		return params;
	}

	void Scene::onEntityCreate(const EntityPtrBase &entity, std::string &id)
	{
		const auto type = entity->getEntityType();

		const auto genIdIfEmpty =
			[&]
			{
				if(id.empty())
					id = generateID(type);
			};

		const auto checkAndAdd =
			[&](auto &container)
			{
				genIdIfEmpty();

				pl::try_reserve(container, 100);
				container.push_back(DynEntityCast<typename pl::remove_cvref_t<decltype(container)>::value_type::element_type>(entity));
			};

		if(type.starts_with("object-"))
		{
			checkAndAdd(mObjects);
		}
		else if(type.starts_with("light-"))
		{
			checkAndAdd(mStaticLights);
		}
		else if(type.starts_with("material-"))
		{
			checkAndAdd(mMaterials);
		}
		else if(type.starts_with("camera-"))
		{
			checkAndAdd(mCameras);
		}
		else if(type.starts_with("cameractl-"))
		{
			checkAndAdd(mCameraCtrls);
		}
		else if(type.starts_with("instance-"))
			genIdIfEmpty();
	}

	void Scene::removeEntity(const IRenderEntity &entity)
	{
		const auto type = entity.getEntityType();

		if(type.starts_with("object-"))
		{
			const auto &casted = dynamic_cast<const IPrimitiveGroup &>(entity);
			pl::erase_one_if(mObjects, [&](const auto &o){ return pl::same_underlying_address<IPrimitiveGroup>(o, &casted); });
		}
		else if(type.starts_with("light-"))
		{
			const auto &casted = dynamic_cast<const IEmitter &>(entity);
			pl::erase_one_if(mStaticLights, [&](const auto &o){ return pl::same_underlying_address<IEmitter>(o, &casted); });
		}
		else if(type.starts_with("material-"))
		{
			const auto &casted = dynamic_cast<const Material &>(entity);
			pl::erase_one_if(mMaterials, [&](const auto &o){ return pl::same_underlying_address<Material>(o, &casted); });
		}
		else if(type.starts_with("camera-"))
		{
			const auto &casted = dynamic_cast<const ICamera &>(entity);
			pl::erase_one_if(mCameras, [&](const auto &o){ return pl::same_underlying_address<ICamera>(o, &casted); });
		}
		else if(type.starts_with("cameractl-"))
		{
			const auto &casted = dynamic_cast<const ICameraController &>(entity);
			pl::erase_one_if(mCameraCtrls, [&](const auto &o){ return pl::same_underlying_address<ICameraController>(o, &casted); });
		}

		EntityCollection::removeEntity(entity);
	}

	void Scene::removeObject(const IPrimitiveGroup &obj)
	{
		pl::erase_one_if(mObjects, [&](const auto &o){ return pl::same_underlying_address<IPrimitiveGroup>(o, &obj); });
		EntityCollection::removeEntity(dynamic_cast<const IRenderEntity &>(obj));
	}

	void Scene::init()
	{
		mLightObjects.clear();
		mLightObjects.shrink_to_fit();
		mInfiniteLights.clear();
		mInfiniteLights.shrink_to_fit();
		mFiniteLights.clear();
		mFiniteLights.shrink_to_fit();
		mLightGroups.clear();
		mLightsNoDelta.clear();
		mLightsNoDelta.shrink_to_fit();
		mLightPortals.clear();
		mLightPortals.shrink_to_fit();
		mLightObjectsPtr.clear();
		mLightObjectsPtr.shrink_to_fit();
		mLightPortalsPtr.clear();
		mLightPortalsPtr.shrink_to_fit();

		mInfiniteLights.reserve(10);
		mFiniteLights.reserve(100);
		mLightsNoDelta.reserve(100);
		mLightObjects.reserve(100);
		mLightPortals.reserve(100);

		mLights.clear();
		mLights.reserve(pl::plmax<szt>(mStaticLights.size(), 100));

		mStaticLights | rgv::transform([&](auto&& v){ return v.get(); })
			| pl::back_insert_copy(mLights);

		if(!mRootInstance)
			return;

		mObjects | pl::for_each([&](auto &obj)
		{
			obj->computeAABox();
		}, std::identity{}, pl::policy::parallel_taskqueue);

		Matrix matrix{pl::tag::identity}, matrixInv{pl::tag::identity};

		if(const auto tr = std::to_address(mRootInstance->getTransform()); tr)
		{
			matrix = *tr;
			matrixInv = tr->inverse();
		}

		mAaboxTotal.set_inflowest();

		pl::log("Scene") << "Scanning scene for light sources...";

		std::atomic<bool> hasInfSurface{};

		traverseRecursiveProperties(mRootInstance,
			[&](auto &inst, PrimGroupPropertiesMapPtr &newPropMap, const Matrix &curMatrix, const Matrix &/*curMatrixInv*/)
			{
				if(const auto objPtr = std::to_address(inst->getObject()); objPtr)
				{
					const auto &object = *objPtr;
					const auto propsPtr = selectPrimGroupProps(object.getClassName(), objPtr, newPropMap);
					const auto &props = *propsPtr;

					if(props.isEmitter())
					{
						EmitterObjectInstance light;

						light.setInstance(std::experimental::get_underlying(inst), curMatrix);
						light.setInstanceProps(propsPtr);
						light.setGroupName(props.getEmitterGroup());

						pl::tasks.critical([&]
						{
							mLightObjects.push_back(std::move(light));
						});
					}

					if(props.isLightPortal())
					{
						EmitterObjectInstance light;

						light.setInstance(std::experimental::get_underlying(inst), curMatrix);
						light.setInstanceProps(propsPtr);
						light.setLightPortal(true);

						pl::tasks.critical([&]
						{
							mLightPortals.push_back(std::move(light));
						});
					}

					if(const auto aabox = inst->getAABox(); !pl::plisinf(aabox))
						pl::tasks.critical([&]{ mAaboxTotal = mAaboxTotal.join(pl::assert_valid( aabox * curMatrix )); });
					else
						hasInfSurface = true;
				}

				return TraverseRecursiveState::Continue;

			},

			pl::do_nothing, {}, pl::tag::identity, pl::tag::identity, pl::policy::parallel_taskqueue
		);

		mHasInfiniteSurface = hasInfSurface.load();

		mLightPortals.shrink_to_fit();
		mLightObjects.shrink_to_fit();

		mLightObjectsPtr.reserve(mLightObjects.size());
		for(auto &l : mLightObjects)
		{
			l.getInstance()->setEmitter(&l);
			mLights.push_back(&l);
			mLightObjectsPtr.push_back(&l);
		}

		pl::log("Scene", "{} light(s) found.", mLights.size());

		mLightPortalsPtr.reserve(mLightPortals.size());
		for(auto &l : mLightPortals)
		{
			l.getInstance()->setEmitter(&l);
			mLightPortalsPtr.push_back(&l);
		}

		for(auto &l : mLights)
		{
			l->init(*this);

			if(l->isInfinite())
				mInfiniteLights.push_back(l.get());
			else
				mFiniteLights.push_back(l.get());

			if(!l->isDiracDelta())
				mLightsNoDelta.push_back(l.get());

			if(const auto lightGroup = l->getGroupName(); !lightGroup.empty())
				mLightGroups.emplace(lightGroup);
		}

		for(auto &l : mLights)
		{
			const auto lightGroup = l->getGroupName();
			l->setGroupIndex( lightGroup.empty() ? 0 : std::distance(mLightGroups.begin(), mLightGroups.find(lightGroup)) + 1 );
		}

		for(auto &l : mLightPortals)
			l.init(*this);

		pl::log("Scene", "{} light groups found: {}", mLightGroups.size(), fmt::join(mLightGroups, ", "));
		pl::log("Scene", "{} light portals found.", mLightPortals.size());

		pl::log("Scene", "Scene bounds: {}", mAaboxTotal);

		mObjects.shrink_to_fit();
		mStaticLights.shrink_to_fit();
		mLights.shrink_to_fit();
		mCameras.shrink_to_fit();
		mMaterials.shrink_to_fit();
		mInfiniteLights.shrink_to_fit();
		mFiniteLights.shrink_to_fit();
		mLightsNoDelta.shrink_to_fit();
		mCameraCtrls.shrink_to_fit();

		initMaterialsForRendering();
	}

	void Scene::initMaterialsForRendering()
	{
		for(auto &mat : mMaterials)
			mat->initForRendering();
	}

	void Scene::updateCameras()
	{
		for(auto &cam : mCameraCtrls)
			cam->update();

		for(auto &cam : mCameras)
			cam->update();
	}


	void Scene::clearScene()
	{
		mObjects.clear();
		mObjects.shrink_to_fit();

		mStaticLights.clear();
		mStaticLights.shrink_to_fit();
		mLights.clear();
		mLights.shrink_to_fit();
		mInfiniteLights.clear();
		mInfiniteLights.shrink_to_fit();
		mFiniteLights.clear();
		mFiniteLights.shrink_to_fit();
		mLightObjects.clear();
		mLightObjects.shrink_to_fit();
		mLightsNoDelta.clear();
		mLightsNoDelta.shrink_to_fit();
		mLightPortals.clear();
		mLightPortals.shrink_to_fit();

		mLightObjectsPtr.clear();
		mLightObjectsPtr.shrink_to_fit();
		mLightPortalsPtr.clear();
		mLightPortalsPtr.shrink_to_fit();

		mCameras.clear();
		mCameras.shrink_to_fit();

		clearEntities();

		mMaterials.clear();
		mMaterials.shrink_to_fit();

		mAmbientSubstance = nullptr;
		mBackground = nullptr;
		mRootInstance = nullptr;

		generateShmID();
	}

	void Scene::setRootInstance(EntityPtr<IObjectInstance> inst) noexcept
		{ mRootInstance = std::move(inst); }

	void Scene::setBackground(EntityPtr<const IColorMapper> bg) noexcept
		{ mBackground = std::move(bg); }

	void Scene::setAmbientSubstance(EntityPtr<const Material> ambientSubstance) noexcept
		{ mAmbientSubstance = std::move(ambientSubstance); }
}
