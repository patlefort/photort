/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Emitter.hpp>

namespace PhotoRT
{
	void EmitterEntityBase::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		params.getValueIfExistsBind
			("group", PL_LAMBDA_FORWARD_THIS(setGroupName))
		;
	}

	ParameterTree EmitterEntityBase::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params.insert({
			{"group", mGroupName}
		});

		return params;
	}
}
