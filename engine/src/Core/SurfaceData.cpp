/*
	PhotoRT

	Copyright (C) 2022 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/SurfaceData.hpp>

namespace PhotoRT
{
	Real SurfaceData::geometricTermBackface(VecP point) noexcept
	{
		VecD dir {localPos - point};
		Real dist = dir /oprtlength;
		if(!dist)
			return 0;

		dir = dir /olnormalize/ dist;

		Real dp = surfaceNormal /odot/ dir;

		isBackfaceHit(dp > 0);

		dp = dp /oabs;
		if(dp < constants::epsilon<Real>)
			return 0;

		return geometricTermPos(dist) * (dp /oprtreciprocal);
	}
}
