/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Film.hpp>
#include <PhotoRT/Core/Camera.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

#include <patlib/task.hpp>
#include <patlib/array_view.hpp>

namespace PhotoRT
{
	// Render parameters
	PRT_ENTITY_FACTORY_DEFINE(RenderContext)

	// Film
	PRT_ENTITY_FACTORY_DEFINE(Film)

	// Contribution buffers
	PRT_ENTITY_FACTORY_DEFINE(ContributionBufferPixel)
	PRT_ENTITY_FACTORY_DEFINE(ContributionBufferLight)


	void ContributionBuffer::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		/*params.getValueIfExistsBind
			("rect", PL_LAMBDA_FORWARD_THIS(setRenderRect))
			("film_id", PL_LAMBDA_FORWARD_THIS(setFilmID))
			("nb_layers", PL_LAMBDA_FORWARD_THIS(setNbLayers))
		;*/

	}

	ParameterTree ContributionBuffer::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		/*params.insert({
			{"rect", mData->rect},
			{"film_id", mFilmID},
			{"nb_layers", getNbLayers()}
		});*/

		return params;
	}


	void ContributionBufferPixel::setEntityId(std::string v)
	{
		auto &memSys = *getMemorySystem<memtag::ContBuffer>();
		std::string buffId = entityCollection ? entityCollection->getShmID() + v : v;

		mData.find_or_construct(memSys, std::move(buffId), is_owner());

		ContributionBuffer::setEntityId(std::move(v));
	}

	void ContributionBufferPixel::setRenderRect(pl::rectangle<> rect)
	{
		plassert(mData);

		mData->rect = rect;

		const auto padSize = mFilm->getFilterTargetSize() /oceil;
		const auto size = mData->rect.dims();
		const auto newDims = size + size / PRT_SAMPLE_RECT_SIZE_MAX * padSize * 2 + padSize * 2;

		mData->buffers | sa::mode::excl |
			[&](auto &buffers)
			{
				if(!buffers.samplesWeight.getNbPixels() || newDims != mData->size)
				{
					mData->nbTiles = pl::dir2d<double>{size} / PRT_SAMPLE_RECT_SIZE_MAX /oceil;
					pl::static_cast_into(padSize, mData->tileStart);
					mData->tileSize = (padSize * 2) + PRT_SAMPLE_RECT_SIZE_MAX;
					mData->size = newDims;

					for(auto &hdrBuffer : buffers.hdrBuffers)
						hdrBuffer.allocate(mData->size);

					buffers.samplesWeight.allocate(mData->nbTiles);
				}

				mData->bufferRect = getBitmapRect(buffers.hdrBuffers[0]);
			};
	}

	void ContributionBufferPixel::zeroBuffers()
	{
		plassert(mData);
		mData->buffers | sa::mode::excl |
			[&](auto &buffers)
			{
				for(auto &hdrBuffer : buffers.hdrBuffers)
					hdrBuffer.zeroBitmap();

				buffers.samplesWeight.zeroBitmap();
			};
	}

	void ContributionBufferPixel::setNbLayers(szt v)
	{
		plassert(mData);
		mData->nbLayers = v;

		mData->buffers.access(sa::mode::excl)->hdrBuffers.resize(v);
	}

	void mergePixelBuffersImpl(auto &buffers, auto &otherBuffers, const Dir2Di tileSize, const pl::rectangle<> &bufRect)
	{
		const auto r = getBitmapRect(buffers.samplesWeight);

		buffers.samplesWeight.range2d(r) | pl::iterator_view |
			pl::for_each
			(
				[&](const auto tile)
				{
					const auto tilePos = tile.getPosition();
					const auto weightOther = otherBuffers.samplesWeight.get(tilePos);
					const auto weight = *tile;
					const auto rect = pl::rectangle<>{tilePos * tileSize, pl::size_p{tileSize}}.clip(bufRect);

					const auto newW = weight + weightOther;
					if(!newW)
						return;

					const auto lerpW = FilmColor{weightOther / newW};

					for(auto&& [b1, b2] : rgv::zip(buffers.hdrBuffers, otherBuffers.hdrBuffers))
						for(auto p : b1.range2d(rect) | pl::iterator_view)
							*p = pl::pllerp(*p, b2.get(p.getPosition()), lerpW);

					*tile = newW;
				},
				std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(100)
			);
	}

	void ContributionBufferPixel::merge(const ContributionBuffer &contributionBuffer)
	{
		plassert(mData);
		const auto &contribBufferPixel = dynamic_cast<const ContributionBufferPixel &>(contributionBuffer);

		mData->buffers                    | sa::mode::excl |
		contribBufferPixel.mData->buffers | sa::mode::shared |
			[&](auto &buffers, auto &otherBuffers)
			{
				mergePixelBuffersImpl(buffers, otherBuffers, mData->tileSize, mData->bufferRect);
			};
	}

	void ContributionBufferPixel::mergeAndZero(ContributionBuffer &contributionBuffer)
	{
		plassert(mData);
		auto &contribBufferPixel = dynamic_cast<ContributionBufferPixel &>(contributionBuffer);

		mData->buffers                    | sa::mode::excl |
		contribBufferPixel.mData->buffers | sa::mode::excl |
			[&](auto &buffers, auto &otherBuffers)
			{
				mergePixelBuffersImpl(buffers, otherBuffers, mData->tileSize, mData->bufferRect);

				for(auto&& b : otherBuffers.hdrBuffers)
					b.zeroBitmap();

				otherBuffers.samplesWeight.zeroBitmap();
			};
	}

	void ContributionBufferPixel::copyFrom(const ContributionBuffer &contributionBuffer)
	{
		plassert(mData);
		const auto &contribBufferPixel = dynamic_cast<const ContributionBufferPixel &>(contributionBuffer);

		mData->buffers                    | sa::mode::excl |
		contribBufferPixel.mData->buffers | sa::mode::shared |
			[&](auto &buffers, const auto &otherBuffers)
			{
				otherBuffers.hdrBuffers | pl::copy(buffers.hdrBuffers.begin());
				buffers.samplesWeight = otherBuffers.samplesWeight;
			};
	}

	void ContributionBufferPixel::addContributionTile(PixelSampleTileLayers &tile)
	{
		plassert(mData);
		int nbInvalid = 0;

		const auto rectSize = tile.tileDims.dims() + mData->tileStart.as_dir() * 2;
		const pl::rectangle<> bufferRect{ tile.tilePos * mData->tileSize, pl::size_p{rectSize} };

		mData->buffers | sa::mode::excl |
			[&](auto &data)
			{
				const auto oldW = data.samplesWeight[tile.tileIndex];
				const auto newW = oldW + tile.weight;
				if(!newW)
					return;

				const auto lerpW = Color{oldW / newW};

				for(auto&& [colors, hdrBuffer] : rgv::zip(tile.tiles, data.hdrBuffers))
				{
					const pl::av::array_view colorsView{std::as_const(colors.color), pl::av::bounds<2>{tile.bufferSize[1], tile.bufferSize[0]}};

					bufferRect.loop([&](const auto p){
						const auto pc = p - bufferRect.p1;
						const auto &color = colorsView[{pc[1], pc[0]}];

						#ifdef PRT_REJECT_INVALID_SAMPLES
							if(!pl::is_valid(color))
							{
								++nbInvalid;
								return;
							}
						#else
							pl::assert_valid(color);
						#endif

						decltype(auto) bufColor = hdrBuffer.get(p);
						bufColor = static_cast<FilmColor>(pl::pllerp(color, static_cast<Color>(bufColor), lerpW));
					});
				}

				data.samplesWeight[tile.tileIndex] = newW;
			};

		if(nbInvalid)
			log(pl::logtag::error) << nbInvalid << " invalid sample(s) detected and discarded on pixel buffer.";
	}

	void ContributionBufferPixel::normalize(FilmBitmap &into, szt index) const
	{
		plassert(mData);

		into.zeroBitmap();

		const auto intoBufRect = getBitmapRect(into);

		mData->buffers | sa::mode::shared |
			[&](const auto &data)
			{
				const auto &hdrBuffer = data.hdrBuffers[index];
				const auto r = pl::rectangle<>{{}, pl::size_p{mData->nbTiles}};

				r | pl::for_each([&](const auto tilePos)
				{
					const auto sourceRectClipped = pl::rectangle<>{tilePos * mData->tileSize, pl::size_p{mData->tileSize}}
						.clip(mData->bufferRect);

					const auto intoRect = pl::rectangle<>{tilePos * PRT_SAMPLE_RECT_SIZE_MAX - mData->tileStart, pl::size_p{sourceRectClipped.dims()}};
					const auto clippedRect = intoRect.clip(intoBufRect);

					const auto sourceStart = sourceRectClipped.p1 + (clippedRect.p1 - intoRect.p1);

					for(auto p : into.range2d(clippedRect) | pl::iterator_view)
					{
						const auto srcPos = p.getLocalPosition() + sourceStart;
						*p += hdrBuffer.get(srcPos);
					}
				}, std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(128) );
			};
	}

	void ContributionBufferPixel::serialize(std::ostream &outputStream) const
	{
		plassert(mData);

		mData->buffers | sa::mode::shared |
			[&](const auto &data)
			{
				outputStream
					<< pl::io::range{pl::io::mode_text, '\n', rgv::all(data.hdrBuffers)}
					<< '\n'
					<< data.samplesWeight;
			};
	}

	void ContributionBufferPixel::unserialize(std::istream &inputStream)
	{
		plassert(mData);

		mData->buffers | sa::mode::excl |
			[&](auto &data)
			{
				inputStream
					>> pl::io::range{pl::io::mode_text, '\n', rgv::all(data.hdrBuffers)}
					>> pl::io::skip_one
					>> data.samplesWeight;
			};
	}


	void ContributionBufferLight::setEntityId(std::string v)
	{
		auto &memSys = *getMemorySystem<memtag::ContBuffer>();
		std::string buffId = entityCollection ? entityCollection->getShmID() + v : v;

		mData.find_or_construct(memSys, std::move(buffId), is_owner());

		ContributionBuffer::setEntityId(std::move(v));
	}

	void ContributionBufferLight::setNbLayers(szt v)
	{
		plassert(mData);
		mData->hdrBuffers.resize(v);
	}

	void ContributionBufferLight::setRenderRect(pl::rectangle<> rect)
	{
		plassert(mData);
		mData->rect = rect;

		auto newDims = mData->rect.dims();

		if(mData->hdrBuffers[0].empty() || newDims != mData->size)
		{
			mData->size = newDims;

			for(auto &hdrBuffer : mData->hdrBuffers)
				hdrBuffer.allocate(mData->size);

			mData->nbSamples = 0;
		}

		mData->bufferRect = {{}, pl::size_p{mData->size}};

		const auto filterSize = mFilm->getFilterTargetSize();

		if(pl::mask_all(filterSize > 0))
		{
			mData->filterTotalSize = filterSize * 2 /oceil + 1;
			mData->nbColors = (mData->filterTotalSize /ohmul)[0];
		}
		else
		{
			mData->filterTotalSize = {};
			mData->nbColors = 0;
		}
	}

	void ContributionBufferLight::zeroBuffers()
	{
		plassert(mData);
		for(auto &hdrBuffer : mData->hdrBuffers)
			for(auto &c : hdrBuffer)
				for(const auto i : mir(c.size()))
					c[i].store(0, std::memory_order::relaxed);

		mData->nbSamples.store(0, std::memory_order::relaxed);

		std::atomic_thread_fence(std::memory_order::release);
	}

	void ContributionBufferLight::merge(const ContributionBuffer &contributionBuffer)
	{
		plassert(mData);
		const auto &contribBufferLight = dynamic_cast<const ContributionBufferLight &>(contributionBuffer);

		std::atomic_thread_fence(std::memory_order::acquire);

		for(auto&& [b1, b2] : rgv::zip(mData->hdrBuffers, contribBufferLight.mData->hdrBuffers))
			mir(b1.size()) |
				pl::for_each
				(
					[&](const auto r)
					{
						auto &c1 = b1[r];
						const auto &c2 = b2[r];

						for(const auto i : mir(c1.size()))
							c1[i].fetch_add(c2[i].load(std::memory_order::relaxed), std::memory_order::relaxed);
					},
					std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
				);

		mData->nbSamples.fetch_add(contribBufferLight.mData->nbSamples, std::memory_order::relaxed);

		std::atomic_thread_fence(std::memory_order::release);
	}

	void ContributionBufferLight::mergeAndZero(ContributionBuffer &contributionBuffer)
	{
		merge(contributionBuffer);
		contributionBuffer.zeroBuffers();
	}

	void ContributionBufferLight::copyFrom(const ContributionBuffer &contributionBuffer)
	{
		plassert(mData);
		const auto &contribBufferLight = dynamic_cast<const ContributionBufferLight &>(contributionBuffer);

		std::atomic_thread_fence(std::memory_order::acquire);

		for(auto&& [b1, b2] : rgv::zip(mData->hdrBuffers, contribBufferLight.mData->hdrBuffers))
			for(auto&& [c1, c2] : rgv::zip(b1, b2))
				for(const auto i : mir(c1.size()))
					c1[i].store(c2[i].load(std::memory_order::relaxed), std::memory_order::relaxed);

		mData->nbSamples.store(contribBufferLight.mData->nbSamples, std::memory_order::relaxed);

		std::atomic_thread_fence(std::memory_order::release);
	}

	void ContributionBufferLight::normalize(FilmBitmap &into, szt index) const
	{
		plassert(mData);
		const auto nbs = (MReal)mData->nbSamples.load(std::memory_order::relaxed);

		if(!nbs)
		{
			into.zeroBitmap();
			return;
		}

		const auto &hdrBuffer = mData->hdrBuffers[index];
		const auto weight = nbs ? pl::plmin<MReal>(1, (MReal)hdrBuffer.getNbPixels() / nbs) : 0;

		hdrBuffer |
			pl::transform
			(
				into.begin(),
				[&](auto&& c)
				{
					return pl::construct_from_index_sequence<FilmColor>(size_v<FilmColor::NbValues>,
							[&]<szt I_>(size_c<I_>) -> FilmColor::value_type
							{
								if constexpr(I_ > 2)
									return 0;
								else
									return static_cast<FilmColor::value_type>(static_cast<MReal>(c[I_].load(std::memory_order::relaxed)) * weight);
							}
						);
				},
				std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
			);

		std::atomic_thread_fence(std::memory_order::release);
	}

	void ContributionBufferLight::addContribution(const PixelSample &sp)
	{
		plassert(mData);
		if(!mData->bufferRect.overlap(sp.p))
			return;

		#ifdef PRT_REJECT_INVALID_SAMPLES
			if(!pl::is_valid(sp.color))
			{
				log(pl::logtag::error) << "Invalid sample detected and discarded on light buffer.";
				return;
			}
		#else
			pl::assert_valid(sp.color);
		#endif

		plassert(sp.layerIndex < mData->hdrBuffers.size());

		auto &buffer = mData->hdrBuffers[sp.layerIndex];

		std::atomic_thread_fence(std::memory_order::acquire);

		if(!mData->nbColors)
			buffer.get(static_cast<Point2Di>(sp.p)) += sp.color;
		else
		{
			const auto filterSize = mFilm->getFilterTargetSize();
			const auto pStart = static_cast<Point2Di>(sp.p - filterSize);
			thread_local std::vector<Color> colors;

			colors.resize(mData->nbColors);
			colors | pl::zero_range;

			pl::av::array_view colorsView{colors, pl::av::bounds<2>{mData->filterTotalSize[1], mData->filterTotalSize[0]}};

			BitmapAlgo::splatColor(mFilm->getFilter(), filterSize, sp.p,
					[&](auto&& pos, auto&& weight)
					{
						const auto p = pos - pStart;
						colorsView[{p[1], p[0]}] += pl::assert_valid(sp.color * weight);
					}
				);

			const auto rect = pl::rectangle<>{pStart, pl::size_p{mData->filterTotalSize}};
			const auto clipped = rect.clip(mData->bufferRect);
			const auto size = clipped.dims();

			pl::rectangle<>{clipped.p1 - rect.p1, pl::size_p{size}}.loop([&](auto p){
				buffer.get(pStart + p) += static_cast<FilmColor>(colorsView[{p[1], p[0]}]);
			});
		}

		std::atomic_thread_fence(std::memory_order::release);
	}

	auto buildContributionBufferLightSerializer(auto&& data) noexcept
	{
		return pl::io::range{pl::io::mode_text, '\n', std::make_tuple(
			pl::io::atomic_value(data.nbSamples, pl::io::mode_text),
			pl::io::range{pl::io::mode_text, '\n', rgv::all(data.hdrBuffers)}
		)};
	}

	void ContributionBufferLight::serialize(std::ostream &outputStream) const
	{
		outputStream << std::hexfloat << buildContributionBufferLightSerializer(*pl::assert_valid(mData));
	}

	void ContributionBufferLight::unserialize(std::istream &inputStream)
	{
		inputStream >> std::hexfloat >> buildContributionBufferLightSerializer(*pl::assert_valid(mData));
	}



	void ContributionBufferAggregator::compile()
	{
		for(auto &t : mBuffers)
			mFinalBuffer->mergeAndZero(*t);
	}

	void ContributionBufferAggregator::normalize()
	{
		for(const auto i : mir(mNormalizedBuffers.size()))
			mFinalBuffer->normalize(mNormalizedBuffers[i], i);
	}

	void ContributionBufferAggregator::zero()
	{
		for(auto &t : mBuffers)
			t->zeroBuffers();

		if(mFinalBuffer)
			mFinalBuffer->zeroBuffers();
	}


	void Film::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		params.getValueIfExistsBind
			("size", PL_LAMBDA_FORWARD_THIS(setSize))
			("filter_size", PL_LAMBDA_FORWARD_THIS(setFilterSize))
			("filter_target_size", PL_LAMBDA_FORWARD_THIS(setFilterTargetSize))
		;

		setCamera(addParamReference(type_v<ICamera>, params, "camera"));
		setToneMapper(addParamReference(type_v<IToneMapper>, params, "tonemapper"));
		setFilterDist(addParamReference(type_v<IColorFilterDistribution2D>, params, "filter"));
		setColorProfile(addParamReference(type_v<IColorProfile>, params, "colorprofile"));
	}

	ParameterTree Film::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params["size"] = mSize;
		params["filter_size"] = mFilter.mSize;
		params["filter_target_size"] = mFilterTargetSize;

		if(mCamera)
			params["camera"].fromEntityRef(*mCamera, deep);

		if(mToneMapper)
			params["tonemapper"].fromEntityRef(*mToneMapper, deep);

		if(mFilter.mDistribution)
			params["filter"].fromEntityRef(*mFilter.mDistribution, deep);

		if(mColorProfile)
			params["colorprofile"].fromEntityRef(*mColorProfile, deep);

		return params;
	}

	void Film::setSize(Dir2Di s)
	{
		mSize = s;
		calcBufferSize();
	}

	void Film::calcBufferSize()
	{
		pl::static_cast_into(mFilterTargetSize /oceil, mFilmBounds.p1);
		mFilmBounds.p2 = mSize + mFilmBounds.p1;

		mBufferSize = mFilmBounds.p2.as_dir() + mFilmBounds.p1.as_dir();
	}

	void Film::initFilter()
	{
		mFilter.initFilter();
		calcBufferSize();
	}

	void Film::initFilmBuffer()
	{
		mHdrBitmap.allocate(mBufferSize);
	}

	void Film::setFilterDist(EntityPtr<const IColorFilterDistribution2D> filterDist) noexcept
	{
		mFilter.mDistribution = std::move(filterDist);
	}

	ContBufferBitmap Film::getCroppedBitmap() const
	{
		ContBufferBitmap cropped;

		cropped.allocate(mSize);

		for(auto it : cropped.range2d(getBitmapRect(cropped)) | pl::iterator_view)
		{
			const auto p = it.getPosition();
			*it = mHdrBitmap.buffer().get(Point2Di{p[0] + mFilmBounds.p1[0], p[1] + mFilmBounds.p1[1]});
		}

		return cropped;
	}

	void Film::zero()
	{
		mHdrBitmap.zeroBitmap();
	}

	void ContributionToFilm::zeroFilm()
	{
		(**mFilm.access(sa::mode::excl)).zero();
	}

	void ContributionToFilm::zeroBuffers()
	{
		for(auto &ag : mAggregators)
			ag.second.zero();
	}

	void ContributionToFilm::compile(i32 index)
	{
		auto film = mFilm.access(sa::mode::excl);
		auto &hdrBitmap = (*film)->getBitmap();

		if(mAggregators.size() == 1 && mAggregators.begin()->second.mNormalizedBuffers.size() == 1 && (!mLayerParams || (*mLayerParams)[0].power == 1))
		{
			auto &ag = mAggregators.begin()->second;

			if(!ag.mNormalizedBuffers[0].empty())
			{
				ag.compile();
				ag.normalize();

				pl::adl_swap(hdrBitmap, ag.mNormalizedBuffers[0]);
			}
		}
		else
		{
			hdrBitmap.zeroBitmap();

			for(auto &ag : mAggregators | rgv::values)
			{
				if(!ag.mNormalizedBuffers.empty())
				{
					ag.compile();
					ag.normalize();

					if(index < 0)
					{
						for(auto&& [i, b] : rgv::enumerate(ag.mNormalizedBuffers))
						{
							if(mLayerParams && (*mLayerParams)[i].power != 1)
								hdrBitmap.transform(b, hdrBitmap, [power = (*mLayerParams)[i].power](auto&& p1, auto&& p2){ return p1 + p2 * power; });
							else
								hdrBitmap += b;
						}
					}
					else
					{
						const auto &b = ag.mNormalizedBuffers[index];

						if(mLayerParams && (*mLayerParams)[index].power != 1)
							hdrBitmap.transform(b, hdrBitmap, [power = (*mLayerParams)[index].power](auto&& p1, auto&& p2){ return p1 + p2 * power; });
						else
							hdrBitmap += b;
					}
				}
			}
		}
	}

	std::istream &operator>> (std::istream &is, ContributionToFilm &cf)
	{
		for(const auto &line : rg::getlines(is))
		{
			if(line.empty())
				break;

			auto &ag = cf.mAggregators[line];

			cf.initAggregator(ag, line);

			is >> *ag.mFinalBuffer >> pl::io::skip_one;
		}

		return is;
	}

	std::ostream &operator<< (std::ostream &os, const ContributionToFilm &cf)
	{
		for(const auto &a : cf.mAggregators)
		{
			if(!a.second.mFinalBuffer)
				continue;

			os << a.first << '\n' << *a.second.mFinalBuffer << '\n';
		}

		return os << '\n';
	}

	void ContributionToFilm::registerBuffer(EntityPtr<ContributionBuffer> contribBuffer)
	{
		mAggregators[std::string{contribBuffer->getType()}].mBuffers.push_back(std::move(contribBuffer));
	}

	void ContributionToFilm::unregisterBuffer(const EntityPtr<ContributionBuffer> &contribBuffer)
	{
		auto type = contribBuffer->getType();

		if(auto agit = mAggregators.find(type); agit != mAggregators.end())
			pl::erase_one(agit->second.mBuffers, contribBuffer);
	}

	void ContributionToFilm::initBuffers()
	{
		for(auto &ag : mAggregators)
			initAggregator(ag.second, ag.first);
	}

	void ContributionToFilm::initAggregator(ContributionBufferAggregator &ag, std::string_view type)
	{
		if(!ag.mFinalBuffer)
		{
			auto film = mFilm.access(sa::mode::shared);
			const auto bufSize = (*film)->getBufferSize();

			ag.mFinalBuffer = factories.createEntity<ContributionBuffer>("contribution_buffer-" + type);

			using std::to_string;
			boost::uuids::random_generator gen;
			boost::uuids::uuid id = gen();
			ag.mFinalBuffer->setEntityId("cbuf-final:" + to_string(id));

			ag.mFinalBuffer->setFilm(film->get());
			ag.mFinalBuffer->setFilmID(std::string{(*film)->getEntityId()});
			ag.mFinalBuffer->setNbLayers(getNbLayers());
			ag.mFinalBuffer->setRenderRect({pl::size_p{bufSize}});
			ag.mNormalizedBuffers.resize(getNbLayers());

			for(auto &b : ag.mNormalizedBuffers)
				b.allocate(bufSize);
		}
	}

	void ContributionToFilm::setNbLayers(i32 nb)
	{
		mNbLayers = nb;
		(**mFilm.access(sa::mode::excl)).setNbLayers(nb);
	}


	void RenderContext::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		if(auto p = params.findParam("render_params"); p && p->childParams)
			mRenderParams = *p->childParams;

		//mRenderParams.print(std::cout);

		mFilms.clear();

		if(auto p = params.findParam("films"); p && p->childParams)
			for(const auto &param : *p->childParams)
			{
				auto f = addReference(type_v<Film>, param.second);
				*mFilms[std::string{f->getEntityId()}].mFilm.access(sa::mode::excl) = f;
			}

		if(auto p = params.findParam("layers"); p && p->childParams)
		{
			setNbLayers(p->childParams->size());
			for(const auto &param : *p->childParams)
				if(const auto index = pl::io::from_string_noloc<szt>(param.first); index < mLayerParams.size())
					param.second.getValueOrDefault("power", mLayerParams[index].power, 1);
		}
	}

	ParameterTree RenderContext::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params["render_params"] = mRenderParams;

		for(auto&& [i, film] : rgv::enumerate(mFilms | rgv::values))
			params["films"][i].fromEntityRef<Film>(**film.mFilm.access(sa::mode::shared), deep);

		params["layers"].hasChilds(true);
		for(auto &childs = *params["layers"].childParams; auto&& [i, p] : rgv::enumerate(mLayerParams))
			childs[i]["power"] = p.power;

		return params;
	}

	void RenderContext::setNbLayers(i32 nb)
	{
		mNbLayers = nb;
		mLayerParams.resize(nb);

		for(auto &film : mFilms | rgv::values)
		{
			film.setNbLayers(nb);
			film.setLayerParams(&mLayerParams);
		}
	}

	void RenderContext::zeroFilms()
	{
		for(auto &film : mFilms | rgv::values)
			film.zeroFilm();
	}

	void RenderContext::zeroBuffers()
	{
		for(auto &film : mFilms | rgv::values)
			film.zeroBuffers();
	}

	void RenderContext::initBuffers()
	{
		for(auto &film : mFilms | rgv::values)
			film.initBuffers();

		for(const auto &[filmId, film] : mFilms)
		{
			log() << "Film: " << filmId;
			for(const auto &[agId, ag] : film.mAggregators)
			{
				log() << pl::io::indent{1} << "Aggregator: " << agId;
				for(const auto &buf : ag.mBuffers)
					log() << pl::io::indent{2} << "Buffer: " << buf->getType();
			}
		}
	}

	void RenderContext::initFilters()
	{
		for(auto &film : mFilms | rgv::values)
			(*film.mFilm.access(sa::mode::excl))->initFilter();
	}

	void RenderContext::initFilmBuffers()
	{
		for(auto &film : mFilms | rgv::values)
			(*film.mFilm.access(sa::mode::excl))->initFilmBuffer();
	}

	void RenderContext::initCamera()
	{
		for(const auto &film : mFilms | rgv::values)
		{
			auto facc = film.mFilm.access(sa::mode::shared);

			const auto bufSize = (*facc)->getBufferSize();
			const auto bounds = (*facc)->getFilmBounds();

			auto &camera = *(*facc)->getCamera();

			camera.setAspectRatio(r_(bufSize[0]) / bufSize[1]);
			camera.setFilmSize(bufSize);
			camera.setFoVbounds({bounds.p1, bounds.p2});

			log() << std::dec << "Camera aspect ratio/image width/height: " << camera.getAspectRatio() << " / " << bufSize[0] << " / " << bufSize[1];
		}
	}

	void RenderContext::registerBuffer(EntityPtr<ContributionBuffer> contribBuffer)
	{
		if(auto it = mFilms.find(contribBuffer->getFilmID()); it != mFilms.end())
			it->second.registerBuffer(std::move(contribBuffer));
	}

	void RenderContext::unregisterBuffer(const EntityPtr<ContributionBuffer> &contribBuffer)
	{
		if(auto it = mFilms.find(contribBuffer->getFilmID()); it != mFilms.end())
			it->second.unregisterBuffer(contribBuffer);
	}

	void RenderContext::deleteBuffers()
	{
		for(auto &film : mFilms | rgv::values)
			film.mAggregators.clear();
	}
}
