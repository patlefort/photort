/*
	PhotoRT

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/RenderEntityInterface.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/RenderEntityCollection.hpp>
#include <PhotoRT/Core/RenderEntityImportExport.hpp>
#include <PhotoRT/Core/Parser.hpp>
#include <PhotoRT/Core/Compression.hpp>

#include <patlib/scoped.hpp>
#include <patlib/path.hpp>

#include <boost/config.hpp>

#include <fmt/std.h>

namespace PhotoRT
{
	void DataSourceFile::serialize(const IRenderEntity &entity)
	{
		if(entity.hasData())
		{
			const auto file = mPath / (entity.getEntityId() + ".prte");
			std::ofstream entityFile;
			entityFile.rdbuf()->pubsetbuf(mBuffer.data(), mBuffer.size());
			entityFile.open(file, std::ios_base::binary | std::ios_base::out);

			if(!entityFile)
				throw ExceptionEntityCollection(fmt::format("Error opening file {} for writing.", file));

			entityFile.exceptions(pl::io::failure_bits);

			pl::io::data_header headers;

			headers.mIsBinary = true;

			#ifdef PRT_HAS_COMPRESSION
				headers.mCompressor = compressorString;
			#endif

			headers.apply_to(entityFile);

			entityFile << headers;

			pl::io::write_stream(entityFile, [&](auto &os){ os << entity; }, compressor{});
			//pl::io::write_stream(entityFile, [&](auto &os){ os << entity; }, pl::io::uncompressed);
		}
	}

	void DataSourceFile::unserialize(IRenderEntity &entity)
	{
		if(entity.hasData())
		{
			const auto file = mPath / (entity.getEntityId() + ".prte");
			std::ifstream entityFile;
			entityFile.rdbuf()->pubsetbuf(mBuffer.data(), mBuffer.size());
			entityFile.open(file, std::ios_base::binary | std::ios_base::in);

			//if(!entityFile.good())
				//throw ExceptionEntityCollection("Error opening file \"" + file.string() + "\" for reading.");

			if(entityFile)
			{
				entityFile.exceptions(pl::io::failure_bits);

				pl::io::data_header headers;
				entityFile >> headers;
				headers.apply_to(entityFile);

				const bool valid = selectDecompressor(headers.mCompressor, [&](auto&& decompressor){
					pl::io::read_stream(entityFile, [&](auto &is){ is >> entity; }, PL_FWD(decompressor));
				});

				if(!valid)
					throw ExceptionEntityCollection(fmt::format("Invalid or unsupported compression type {} for entity {}.", pl::io::quoted{headers.mCompressor}, pl::io::quoted{entity.getEntityId()}));
			}
		}
	}

	void DataSourceStream::serialize(const IRenderEntity &entity)
	{
		if(os && entity.hasData(pl::io::is_ipc(*os)))
		{
			//std::cout << "Serializing " << entity.getEntityId() << '\n';
			*os << entity << std::flush;
			//std::cout << "Done serializing " << entity.getEntityId() << '\n';
		}
	}

	void DataSourceStream::unserialize(IRenderEntity &entity)
	{
		if(os && is && entity.hasData(pl::io::is_ipc(*is)))
		{
			*os << "data:" << entity.getEntityId() << std::endl;
			*is >> entity;
		}
	}


	std::vector<EntityPtrBase> EntityCollection::getEntitiesByType(std::string_view type)
	{
		std::vector<EntityPtrBase> ents;

		for(auto &e : mEntities.left | rgv::values)
		{
			if(e->getEntityType().find(type) == 0)
				ents.push_back(e);
		}

		return ents;
	}

	EntityCollection::EntityCollection()
	{
		generateShmID();
	}

	EntityCollection::~EntityCollection() = default;

	void EntityCollection::generateShmID()
	{
		using std::to_string;
		boost::uuids::random_generator gen;
		boost::uuids::uuid id = gen();

		mShmID = "ec_" + to_string(id);
	}

	std::string EntityCollection::generateID(std::string_view type)
	{
		return pl::strconcat(std::string{}, mIdPrefix, type, '_', fmt::to_string(++mCounter));
	}

	EntityPtrBase EntityCollection::createEntity(std::string_view type, std::string id, const ParameterTree &params)
	{
		std::string rtypeContainer;
		std::string_view rtype;

		const ParameterTree *entityParams = nullptr;

		if(id.empty())
		{
			entityParams = &params;
			rtype = type;
		}
		else
		{
			if(id[0] == '&')
			{
				if(id.size() <= 1)
					throw ExceptionEntityCollection(fmt::format("Empty reference to entity of type {}.", pl::io::quoted{type}));

				if(!mIdPrefix.empty())
					id.replace(0, 1, mIdPrefix);
				else
					id.erase(id.begin());

				if(auto it = mEntities.left.find(id); it != mEntities.left.end())
				{
					if(type.substr(0, type.find_first_of('-')) != it->second->getEntityType().substr(0, it->second->getEntityType().find_first_of('-')))
						throw ExceptionEntityCollection(fmt::format("Invalid reference to entity {} of type {} with type {}.", pl::io::quoted{id}, pl::io::quoted{it->second->getEntityType()}, pl::io::quoted{type}));

					return it->second;
				}

				if(auto res = mGlobalParamList.findEntityRef(id); res)
				{
					auto [resType, resId] = res->getEntityType();

					if(type.substr(0, type.find_first_of('-')) != resType.substr(0, resType.find_first_of('-')))
						throw ExceptionEntityCollection(fmt::format("Invalid reference to entity {} of type {} with type {}.", pl::io::quoted{id}, pl::io::quoted{resType}, pl::io::quoted{type}));

					entityParams = &*res->childParams;
					rtypeContainer = std::move(resType);
					rtype = rtypeContainer;

					mCreatedEntities.emplace(id);
				}
				else
					throw ExceptionEntityCollection(fmt::format("Reference to entity {} of type {} not found.", pl::io::quoted{id}, pl::io::quoted{type}));
			}
			else
			{
				id.insert(0, mIdPrefix);
				entityParams = &params;
				rtype = type;

				if(auto ce = mCreatedEntities.find(id); ce != mCreatedEntities.end())
				{
					mCreatedEntities.erase(ce);

					return mEntities.left.find(id)->second;
				}

				if(!id.empty())
				{
					if(auto it = mEntities.left.find(id); it != mEntities.left.end())
					{
						if(!mOverwriteExisting)
							throw ExceptionEntityCollection(fmt::format("Entity ID {} already exists.", pl::io::quoted{id}));

						if(it->second->getEntityType() != rtype)
							throw ExceptionEntityCollection(fmt::format("Entity type mismatch for entity ID {}, type {} to {}.", pl::io::quoted{id}, pl::io::quoted{it->second->getEntityType()}, pl::io::quoted{rtype}));

						it->second->setParameters(params);

						return it->second;
					}
				}
			}
		}

		return createNewEntity(rtype, std::move(id), entityParams ? *entityParams : ParameterTree{});
	}

	EntityPtrBase EntityCollection::createEntity(const Parameter &param)
	{
		if(!param.isEntity())
			throw ExceptionEntityCollection("Invalid parameter given for entity reference.");

		std::string type, id;
		param.getEntityType(type, id);

		return createEntity(type, std::move(id), param.childParams ? *param.childParams : ParameterTree{});
	}

	EntityPtrBase EntityCollection::createEntity(std::string_view params)
	{
		if(!mParser)
			throw ExceptionEntityCollection("No parser given to parse entity string.");

		return createEntity(std::move(mParser->loadFragment(params)["e"]));
	}

	EntityPtrBase EntityCollection::createEntity(std::istream &params)
	{
		if(!mParser)
			throw ExceptionEntityCollection("No parser given to parse input.");

		return createEntity(std::move(mParser->load(params)["e"]));
	}

	EntityPtrBase EntityCollection::createNewEntity(std::string_view type, std::string id, const ParameterTree &params)
	{
		EntityPtrBase entity;

		if(const auto pimport = params.findParam("import"); pimport && !pimport->value.empty())
		{
			const auto genericType = pimport->getValueOrDefault("category", std::string{});
			const auto mimeType = std::invoke([&]{
					if(const auto p = pimport->findParam("mimetype"); p && !p->value.empty())
						return p->value;

					return factories.getMimeTypeImport(pimport->value);
				});

			if(mimeType.empty() && genericType.empty())
				throw ExceptionEntityCollection(fmt::format("Mime type for {} not registered.", pl::io::quoted{pimport->value}));

			entity = importEntity(pl::paths::absolute(pimport->value, mPath), genericType, mimeType, std::move(id), pimport->childParams ? *pimport->childParams : ParameterTree{}, type);
		}
		else
		{
			entity = mFactories->createEntity(id, type);

			setupEntity(entity, std::move(id), params);
		}

		return entity;
	}

	EntityPtrBase EntityCollection::createEntity(std::string_view type, std::string id, std::istream &params)
	{
		if(!mParser)
			throw ExceptionEntityCollection("No parser given to parse input.");

		const auto entityParams = mParser->load(params);

		return createEntity(type, std::move(id), entityParams);
	}

	EntityPtrBase EntityCollection::createEntity(std::string_view type, std::string id, std::string_view params)
	{
		if(!mParser)
			throw ExceptionEntityCollection("No parser given to parse string.");

		const auto entityParams = mParser->loadFragment(params);

		return createEntity(type, std::move(id), entityParams);
	}

	void EntityCollection::insertEntity(std::string id, const EntityPtrBase &entity)
	{
		if(id.empty())
			id = generateID(entity->getEntityType());
		else if(!mIdPrefix.empty())
			id.insert(0, mIdPrefix);

		entity->setEntityId(id);

		pl::log("EntityCollection", "Inserting entity id {} of type {}.", pl::io::quoted{id}, pl::io::quoted{entity->getEntityType()});

		entity->setEntityCollection(this);

		mEntities.insert({std::move(id), entity});
	}

	void EntityCollection::addEntityReference(const IRenderEntity &entity, const EntityPtrBase &entityRel)
	{
		if(!entity.getEntityId().empty() && !entityRel->getEntityId().empty())
			mEntityRelations.push_back({std::string(entity.getEntityId()), std::string(entityRel->getEntityId())});
	}

	void EntityCollection::releaseEntityReference(const IRenderEntity &entity, const EntityPtrBase &entityRel)
	{
		if(auto it = pl::find_unique_relation(mEntityRelations.left, std::string(entity.getEntityId()), entityRel->getEntityId()); it != mEntityRelations.left.end())
			mEntityRelations.left.erase(it);
	}

	void EntityCollection::releaseAllEntityReferences(const IRenderEntity &entity)
	{
		mEntityRelations.left.erase(std::string(entity.getEntityId()));
	}

	void EntityCollection::removeEntity(const IRenderEntity &entity)
	{
		if(mEntityRelations.right.find(std::string(entity.getEntityId())) != mEntityRelations.right.end())
			throw ExceptionEntityCollection(fmt::format("Entity {} is being removed but it is used by other entities.", pl::io::quoted{entity.getEntityId()}));

		pl::log("EntityCollection", "Removing entity id {} of type {}.", pl::io::quoted{entity.getEntityId()}, pl::io::quoted{entity.getEntityType()});

		std::string s{entity.getEntityId()};
		mEntityRelations.right.erase(s);
		mEntities.left.erase(s);
	}

	void EntityCollection::clearEntities()
	{
		mEntityRelations.clear();
		mEntities.clear();
	}

	EntityPtrBase EntityCollection::importEntity(std::istream &inputStream, std::string_view genericType, std::string_view mimeType, std::string id, const ParameterTree &params, std::string_view expectedType)
	{
		pl::log("EntityCollection", "Importing entity from stream, id {}, type: {}, mime-type {}.", pl::io::quoted{id}, pl::io::quoted{genericType}, pl::io::quoted{mimeType});

		if(genericType == "params")
		{
			if(!mParser)
				throw ExceptionEntityCollection("No parser given to parse input.");

			return createEntity(expectedType, std::move(id), mParser->load(inputStream));
		}
		else if(mimeType.empty())
		{
			for(const auto &entityie : factories.mGenericTypesHandlers.left.equal_range(std::string(genericType)) | pl::map_pair_to_range)
			{
				try
				{
					if(auto res = entityie.second->importEntity(*this, inputStream, genericType, mimeType, std::move(id), params); res)
						return res;
				} catch(...) {}
			}

			throw ExceptionEntityCollection(fmt::format("No entity importer found for type {} with streaming.", pl::io::quoted{genericType}));
		}
		else
		{
			auto entityie = factories.getEntityImporter(mimeType, expectedType, true);

			if(!entityie)
				throw ExceptionEntityCollection(fmt::format("No entity importer found for type {} and mime-type {} with streaming.", pl::io::quoted{expectedType}, pl::io::quoted{mimeType}));

			return entityie->importEntity(*this, inputStream, genericType, mimeType, std::move(id), params);
		}
	}

	EntityPtrBase EntityCollection::importEntity(std::span<const std::byte> data, std::string_view genericType, std::string_view mimeType, std::string id, const ParameterTree &params, std::string_view expectedType)
	{
		pl::log("EntityCollection", "Importing entity from data, id {}, type: {}, mime-type {}.", pl::io::quoted{id}, pl::io::quoted{genericType}, pl::io::quoted{mimeType});

		if(genericType == "params")
		{
			if(!mParser)
				throw ExceptionEntityCollection("No parser given to parse input.");

			std::span<const char> cData{reinterpret_cast<const char *>(data.data()), data.size()};
			return pl::io::stream_from(cData, pl::io::throw_on_failure, [&](auto&& is, auto&&){
				is.imbue(Parameter::loc);
				return createEntity(expectedType, std::move(id), mParser->load(is));
			});
		}
		else if(mimeType.empty())
		{
			for(const auto &entityie : factories.mGenericTypesHandlers.left.equal_range(std::string(genericType)) | pl::map_pair_to_range)
			{
				try
				{
					if(auto res = entityie.second->importEntity(*this, data, genericType, mimeType, std::move(id), params))
						return res;
				} catch(...) {}
			}

			throw ExceptionEntityCollection(fmt::format("No entity importer found or capable for type {} with streaming.", pl::io::quoted{genericType}));
		}
		else
		{
			auto entityie = factories.getEntityImporter(mimeType, expectedType, true);

			if(!entityie)
				throw ExceptionEntityCollection(fmt::format("No entity importer found for type {} and mime-type {} with streaming.", pl::io::quoted{expectedType}, pl::io::quoted{mimeType}));

			return entityie->importEntity(*this, data, genericType, mimeType, std::move(id), params);
		}
	}

	EntityPtrBase EntityCollection::importEntity(const std::filesystem::path &filename, std::string_view genericType, std::string_view mimeType, std::string id, const ParameterTree &params, std::string_view expectedType)
	{
		pl::log("EntityCollection", "Importing entity from file {}, id {}, type: {}, mime-type {}.", filename, pl::io::quoted{id}, pl::io::quoted{genericType}, pl::io::quoted{mimeType});

		if(genericType == "params")
		{
			if(!mParser)
				throw ExceptionEntityCollection("No parser given to parse file.");

			return createEntity(expectedType, std::move(id), mParser->load(filename));
		}
		else if(mimeType.empty())
		{
			for(const auto &entityie : factories.mGenericTypesHandlers.left.equal_range(std::string(genericType)) | pl::map_pair_to_range)
			{
				try
				{
					if(auto res = entityie.second->importEntity(*this, filename, genericType, mimeType, std::move(id), params); res)
						return res;
				} catch(...) {}
			}

			throw ExceptionEntityCollection(fmt::format("No entity importer found for type {} and file {}.", pl::io::quoted{genericType}, filename));
		}
		else
		{
			auto entityie = factories.getEntityImporter(mimeType, expectedType, false);

			if(!entityie)
				throw ExceptionEntityCollection(fmt::format("No entity importer found for file {}.", filename));

			return entityie->importEntity(*this, filename, genericType, mimeType, std::move(id), params);
		}
	}

	bool EntityCollection::canImport(std::string_view mimeType)
	{
		auto ie = factories.getEntityImporter(mimeType);

		return ie && ie->canImport(mimeType);
	}

	bool EntityCollection::canExport(std::string_view mimeType)
	{
		auto ie = factories.getEntityExporter(mimeType);

		return ie && ie->canExport(mimeType);
	}

	void EntityCollection::exportEntity(const IExportHook &entity, std::ostream &outputStream, std::string_view mimeType, const ParameterTree &params)
	{
		for(auto &entityie : factories.mEntityImportExport | rgv::values)
		{
			if(entityie->canExport(mimeType))
			{
				entityie->exportEntity(*this, entity, outputStream, mimeType, params);
				return;
			}
		}

		throw ExceptionEntityCollection(fmt::format("No entity exporter found for mime-type {}.", mimeType));
	}

	void EntityCollection::exportEntity(const IExportHook &entity, const std::filesystem::path &filename, std::string_view mimeType, const ParameterTree &params)
	{
		for(auto &entityie : factories.mEntityImportExport | rgv::values)
		{
			if(entityie->canExport(mimeType))
			{
				entityie->exportEntity(*this, entity, filename, mimeType, params);
				return;
			}
		}

		throw ExceptionEntityCollection(fmt::format("No entity exporter found for mime-type {}.", mimeType));
	}

	std::vector<std::byte> EntityCollection::exportEntity(const IExportHook &entity, std::string_view mimeType, const ParameterTree &params)
	{
		for(auto &entityie : factories.mEntityImportExport | rgv::values)
			if(entityie->canExport(mimeType))
				return entityie->exportEntity(*this, entity, mimeType, params);

		throw ExceptionEntityCollection(fmt::format("No entity exporter found for mime-type {}.", mimeType));
	}

	void EntityCollection::addEntities(const ParameterTree &params)
	{
		for(const auto &v : params)
			if(v.second.isEntity())
				if(auto [etype, eid] = v.second.getEntityType(); !etype.empty())
					createEntity(etype, std::move(eid), v.second.childParams ? *v.second.childParams : ParameterTree{});

		mIdPrefix.clear();
	}

	void EntityCollection::addEntitiesFromFile(const std::filesystem::path &filename, std::string idPrefix)
	{
		if(!mParser)
			throw ExceptionEntityCollection("No parser given to parse file.");

		if(auto absPath = pl::paths::absolute(filename, mPath); mIncludedFiles.emplace(std::filesystem::canonical(absPath)).second)
		{
			pl::log("EntityCollection", "Loading entities from file {}.", absPath);

			auto params = mParser->load(absPath);
			absPath.remove_filename();

			const pl::fast_scoped_assignment tp{mPath, std::move(absPath)};

			if(const auto p = params.findParam("include"); p && p->childParams)
			{
				for(const auto &param : *p->childParams)
					if(!param.second.value.empty())
						addEntitiesFromFile(param.second.value, param.second.getValueOrDefault("prefix", std::string{}));
			}

			mIdPrefix = std::move(idPrefix);
			const pl::scoped_guard prefixg{[&]{ mIdPrefix.clear(); }};

			if(const auto p = params.findParam("entities"); p && p->childParams)
			{
				ParameterTree paramsBackup;
				const pl::scoped_swap tm1{mGlobalParamList, paramsBackup};
				const pl::scoped_swap tm2{params, mGlobalParamList};

				addEntities(*p->childParams);
			}
		}
	}

	void EntityCollection::saveEntitiesToFile(const std::filesystem::path &filename)
	{
		const auto absPath = pl::paths::absolute(filename, mPath);

		std::ofstream entityFile(absPath);

		if(!entityFile.good())
			throw ExceptionEntityCollection(fmt::format("Unable to open file {}.", absPath));

		serializeEntities(entityFile);
	}

	void EntityCollection::serializeEntities(std::ostream &outputStream)
	{
		if(!mParser)
			throw ExceptionEntityCollection("No parser given to serialize entities.");

		outputStream.imbue(Parameter::loc);

		ParameterTree params;

		params["entities"].hasChilds(true);

		auto &childs = *params["entities"].childParams;

		for(const auto &entity : mEntities.left | rgv::values)
		{
			auto entityParams = entity->getParameters(false);
			Parameter p{pl::strconcat(std::string{}, entity->getEntityType(), '/', entity->getEntityId())};
			if(!entityParams.empty())
				p.setChildParams(std::move(entityParams));

			childs.emplace(entity->getEntityId(), std::move(p));
		}

		mParser->save(params, outputStream);
	}

	void EntityCollection::unserializeEntities(std::istream &inputStream, std::string_view idPrefix)
	{
		if(!mParser)
			throw ExceptionEntityCollection("No parser given to unserialize entities.");

		mIdPrefix = idPrefix;

		inputStream.imbue(Parameter::loc);

		ParameterTree paramsBackup;
		const pl::scoped_swap tm{mGlobalParamList, paramsBackup};

		mGlobalParamList.clear();
		mGlobalParamList = mParser->load(inputStream);

		if(const auto p = mGlobalParamList.findParam("entities"); p && p->childParams)
		{
			for(const auto &entity : *p->childParams)
			{
				std::string type, id;

				entity.second.getEntityType(type, id);

				if(!type.empty() && mEntities.left.find(id) == mEntities.left.end())
					createEntity(type, std::move(id), entity.second.childParams ? *entity.second.childParams : ParameterTree{});
			}
		}

		mIdPrefix.clear();
	}

	void EntityCollection::setFilePath(const std::filesystem::path &p)
	{
		mPath = p;
		if(std::filesystem::is_regular_file(mPath))
			mPath = mPath.parent_path();
	}

	EntityPtrBase EntityCollection::createEntity(IRenderEntityFactory &factory, std::string id, const ParameterTree &params)
	{
		auto entity = factory.create_shared();

		if(!entity)
			throw ExceptionEntityCollection(fmt::format("Entity creation failed for id {}.", pl::io::quoted{id}));

		setupEntity(entity, std::move(id), params);

		return entity;
	}

	EntityPtrBase EntityCollection::createEntity(IRenderEntityFactory &factory, std::string id, std::string_view params)
	{
		return createEntity(factory, std::move(id), mParser->loadFragment(params));
	}

	EntityPtrBase EntityCollection::createEntity(IRenderEntityFactory &factory, std::string id, std::istream &params)
	{
		return createEntity(factory, std::move(id), mParser->load(params));
	}

	void EntityCollection::setupEntity(EntityPtrBase entity, std::string id, const ParameterTree &params)
	{
		auto &entityRef = *entity;

		entityRef.setEntityCollection(this);

		onEntityCreate(entity, id);

		if(id.empty() && entityRef.canHaveData())
			id = generateID(entityRef.getEntityType());

		entityRef.setEntityId(id);

		if(!id.empty())
		{
			pl::log("EntityCollection", "Created new entity id {} of type {}.", pl::io::quoted{id}, pl::io::quoted{entityRef.getEntityType()});
			mEntities.insert({std::move(id), std::move(entity)});
		}

		entityRef.setParameters(params);

		if(mDataSource)
			mDataSource->unserialize(entityRef);
	}
}
