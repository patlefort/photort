/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Integration.hpp>
#include <PhotoRT/Core/Film.hpp>
#include <PhotoRT/Core/Instance.hpp>
#include <PhotoRT/Core/Scene.hpp>

#include <patlib/task.hpp>
#include <patlib/back_buffered.hpp>

#include <optional>

namespace PhotoRT
{
	[[nodiscard]] pl::marked_optional<SampledBxDF> sampleLayer(
		EnumTransportMode mode,
		RandomReals<2> rnd,
		const VecD odir,
		const SurfaceInteraction &interaction,
		Sampler &sampler,
		Real importance) noexcept
	{
		const auto &material = interaction.getMaterial();
		const auto &layer = interaction.getLayer();
		const IBxDF *bxdf = nullptr;

		if(importance <= 0.8)
			bxdf = std::to_address(layer.getOptimizedBxDF());

		if(!bxdf)
			bxdf = std::to_address(layer.getBxDF());

		if(!bxdf)
			return pl::nullopt;

		// Sample the bxdf
		auto sp = bxdf->sampleDir(mode, rnd, interaction, odir, sampler);

		bool passthrough = false;
		Real aprob = 1, passthroughProb = 0;

		// Handle transparency
		if(material.isTransparent())
		{
			aprob = alphaProbability(sp->color.alpha);
			passthroughProb = 1 - aprob;
			passthrough = passthroughProb == 1 ? true : (!passthroughProb ? false : uniformRealRng<Real>()(sampler) < passthroughProb);
		}

		if(passthrough)
		{
			sp->pdf = passthroughProb;
			sp->color.color = pl::assert_valid(White - sp->color.alpha);
			sp->albedo = getColorAlbedo(sp->color.color);
			sp->dir = odir;
			sp->hemisphere = EnumHemisphere::South;
			sp->type = EnumRayType::Transparency;
		}
		else
		{
			if(!sp || isAlmostBlack(sp->color.color))
				return pl::nullopt;

			// Reject sample too close to parallel to the surface
			const auto rayDirection = sp->dir /odot/ interaction.surface().surfaceNormal;
			if(rayDirection /oabs <= constants::epsilon<Real> || sp->dir /odot/ interaction.surface().shadingNormal /oabs <= constants::epsilon<Real>)
				return pl::nullopt;

			sp->color.color *= interaction.shadingNormalCorrection(mode, odir, sp->dir);

			sp->pdf *= aprob;
			sp->color.color *= sp->color.alpha;
			sp->albedo *= getColorAlbedo(sp->color.alpha);

			pl::assert_valid(sp->pdf, valid::Weight);
			pl::assert_valid(sp->dir);
			pl::assert_valid(sp->color.color);
		}

		return sp;
	}

	[[nodiscard]] ShadowRayResult traceShadow(
		EnumTransportMode mode,
		const Ray &shadowRay,
		const MaterialInteraction &fromInter,
		TracerParams &tracer,
		SubstanceStack &substanceStack,
		uf32 maxBounces,
		std::optional<MaterialInteraction> *lpSurface,
		bool passthroughLightPortals) noexcept
	{
		PathSegmentConst segment{&fromInter, nullptr};
		ShadowRayResult res{};
		const Ray *ray = &shadowRay;
		Ray nray;
		Color throughput{pl::tag::one};
		pl::back_buffered<pl::uninitialized_value<MaterialInteraction>> newInter;
		const auto destination = shadowRay.origin + shadowRay.dir * shadowRay.length;

		res.pdfTr = 1;
		res.infPdfTr = 1;

		do
		{
			pl::assert_valid(throughput);

			if(isAlmostBlack(throughput) || res.nbTraced >= (StatCounter)maxBounces)
			{
				throughput.set_zero();
				break;
			}

			++res.nbTraced;

			const auto &enteredSub = substanceStack.getEnteredSub()->getMaterial();

			auto trres = tracer.rayTracer.traceRay(*ray);

			if(!trres)
			{
				// Color absorption
				if(enteredSub.isMedium())
				{
					const auto &medium = *enteredSub.getMedium();
					auto mtr = medium(*segment.start, ray->dir, ray->length, tracer.sampler);
					mtr.set(0, size_v<3>);

					throughput *= mtr;

					const auto p = (mtr /ohadd)[size_v<0>] * (1.0 / 3.0);
					res.pdfTr *= p;
					res.infPdfTr *= p;
				}
				else
				{
					throughput *= enteredSub.absorbColor(ray->length);
				}

				break;
			}

			newInter.front() = collisionToShadedInteraction(*trres, ray->dir, true);
			segment.next(&newInter.front().value());
			auto &inter = std::get<SurfaceInteraction>(*newInter.front());
			substanceStack.interaction(inter);

			Real mediumProb = 1;

			// Color absorption
			if(enteredSub.isMedium())
			{
				const auto &medium = *enteredSub.getMedium();

				auto mtr = medium(*segment.end, ray->dir, inter.colDist, tracer.sampler);
				mtr.set(0, size_v<3>);

				throughput *= mtr;
				mediumProb = (mtr /ohadd)[size_v<0>] * (1.0 / 3.0);
			}
			else
			{
				throughput *= enteredSub.absorbColor(inter.colDist);
			}

			LayerAlpha bxdfAlpha{};
			const auto &object = *inter.instance()->getObject();
			const bool isLPortal = object.isLightPortal();
			const bool passthrough = inter.shouldPassthrough() && (!isLPortal || passthroughLightPortals);

			if(!passthrough)
			{
				if(!inter.surface().matEntry || !inter.surface().matEntry->material->isTransparent())
				{
					throughput.set_zero();
					break;
				}

				const auto &mat = *inter.surface().matEntry->material;

				// Get alpha of material
				bxdfAlpha = mat.getLayerAlpha(mat.getFrontLayerIndex(inter.surface().isBackfaceHit()), mode, inter, ray->dir, nray.dir, EnumHemisphere::South, false);
			}
			else if(isLPortal)
			{
				res.lp = inter.instance();

				res.infPdfTr = 1;

				const auto &lp = *res.lp->getEmitter();
				const auto pdf = lp.PDF(*segment.end).value();
				const auto dotTerm = inter.geometricTermDir(ray->dir);

				res.lpLocalPos = inter.localPos();
				res.pdfLpA = pdf;
				res.pdfLpForW = pdfAtoW(
						pdf,
						geometricTermPos(inter.localPos(), lp.worldToLocal(shadowRay.origin)),
						dotTerm
					);

				if(lpSurface)
					*lpSurface = *newInter.front();
			}

			if(isWhite(bxdfAlpha.alpha))
			{
				throughput.set_zero();
				break;
			}

			nray.type = EnumRayType::Shadow | EnumRayType::Transparency;
			nray.origin = inter.safeIncrement(ray->origin, inter.worldPos(), EnumHemisphere::South);

			if(ray->length != Ray::MaxDistance)
			{
				nray.setDestination(destination);
			}
			else
			{
				nray.length = Ray::MaxDistance;
				nray.dir = ray->dir;
			}

			throughput *= (White - bxdfAlpha.alpha);

			if(!passthrough)
			{
				const auto p = (1 - bxdfAlpha.pdf) * mediumProb;
				res.pdfTr *= p;
				res.infPdfTr *= p;
			}

			substanceStack.updateSubstanceStack(substanceStack.checkSubstanceEntry(EnumHemisphere::South));

			newInter.flip();
			segment.next(nullptr);

			ray = &nray;
		}
		while(true);

		res.color = pl::assert_valid(throughput);

		return res;
	}


	void buildSubstanceStack(IRayTracer &rayTracer, VecP position, SubstanceStack &stack)
	{
		pl::back_buffered<Ray> rays;

		constexpr u32 maxCollisions = 1024 * 16;

		const auto &scene = *rayTracer.getScene();

		const auto aabox = scene.getAABox();

		if(!aabox.overlap(position))
			return;

		VecD origDir;

		{
			auto &ray = rays.front();

			ray.origin = aabox.v2 + VecD{100, 0};
			ray.dir = (position - ray.origin).as_dir();
			ray.length = ray.dir /oprtlength;
			ray.dir = ray.dir /olnormalize/ ray.length;
			ray.length -= constants::epsilon<Real> * 100;

			origDir = ray.dir;
		}

		u32 nbCollisions = 0;

		while(!stack.full() && nbCollisions < maxCollisions)
		{
			auto &oray = rays.front();

			auto res = rayTracer.traceRay(oray);
			if(!res)
				break;

			auto inter = collisionToShadedInteraction(*res, oray.dir, true);

			stack.interaction(inter);
			stack.updateSubstanceStack(stack.checkSubstanceEntry(EnumHemisphere::South));

			auto &ray = rays.back();

			ray.origin = inter.safeIncrement(oray.origin, inter.worldPos(), EnumHemisphere::South);
			ray.dir = (position - ray.origin).as_dir();
			ray.length = ray.dir /oprtlength;
			if(ray.length <= constants::epsilon<Real> || origDir /odot/ ray.dir <= 0)
				break;
			ray.dir = ray.dir /olnormalize/ ray.length;

			++nbCollisions;

			rays.flip();
		}

		if(stack.full())
			pl::log("PhotoRT", pl::logtag::error, "Substance stack is full with {} substances.", stack.size());

		if(nbCollisions == maxCollisions)
			pl::log("PhotoRT", pl::logtag::error) << "Maximum number of collisions reached while building the substance stack.";

		//stack.print(std::cout);
	}

	void updateSensorSubstanceStack(ISensor &sensor, IRayTracer &rayTracer)
	{
		auto &stack = sensor.getSubstanceStack();

		const auto &scene = *rayTracer.getScene();
		const auto &ambientSubs = scene.getAmbientSubstance()->mLayers.back();

		stack.reset();
		stack.initialSubstance(ambientSubs);

		if(sensor.isInfinite())
			return;

		//std::cout << "updateSensorSubstanceStack: " << dynamic_cast<IRenderEntity &>(sensor).getEntityId() << '\n';

		buildSubstanceStack(rayTracer, sensor.localToWorld(sensor.getMiddle()), stack);
	}

	void updateSceneSubstanceStack(IRayTracer &rayTracer, Scene &scene)
	{
		std::forward_as_tuple(scene.getCameras(), scene.getLights(), scene.getLightPortals())
			| pl::for_each([&](auto&& sensors)
			{
				sensors
					| pl::to_ref_view
					| pl::for_each
					(
						[&](auto &sensor){ updateSensorSubstanceStack(sensor, rayTracer); },
						std::identity{}, pl::policy::parallel_taskqueue_one
					);
			}, std::identity{}, pl::policy::parallel_taskqueue_one);
	}
}
