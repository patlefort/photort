/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/BxDF.hpp>
#include <PhotoRT/Core/Material.hpp>
#include <PhotoRT/Core/Interaction.hpp>

namespace PhotoRT
{
	void BxDFBase::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		addParamReference(mColorMapper, params, "texture");
		addParamReference(mAlphaMapper, params, "alpha");
	}

	ParameterTree BxDFBase::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		if(mColorMapper)
			params["texture"].fromEntityRef(*mColorMapper, deep);

		if(mAlphaMapper)
			params["alpha"].fromEntityRef(*mAlphaMapper, deep);

		return params;
	}

	ColorPair<Color> BxDFBase::getColor(const SurfaceInteraction &inter, const VecD odir) const noexcept
	{
		const auto color = mColorMapper ? mColorMapper->map(inter, odir) : White;
		auto alpha = (mAlphaMapper ? mAlphaMapper->map(inter, odir) : White) * fillAlphaColor(color);

		alpha = clampColor(alpha);//.blend<0b1000>(Black);

		pl::assert_valid(color);
		pl::assert_valid(alpha);

		return {color, alpha};
	}

	pl::marked_optional<SampledBxDF> BxDFBase::sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &/*rng*/, EnumRayType typeMask) const noexcept
	{
		if(!isType(*this, typeMask) || !isHemisphereValid(inter.lobe().getHemisphere(-odir))) [[unlikely]]
			return SampledBxDF::NullOpaque();

		const auto [z, idir] = inter.lobe().sampleCosineDir(static_cast<pl::values<Real, 2>>(rnd));
		const auto rad = radiance(mode, inter, odir, idir);

		return SampledBxDF{
				.dir = idir,
				.color = rad.color,
				.albedo = rad.albedo,
				.pdf = pl::assert_valid( z * std::numbers::inv_pi_v<Real>, valid::Weight ),
				.type = getType(),
				.hemisphere = EnumHemisphere::North
			};
	}

	BxDFRadianceResult BxDFBase::radiance(EnumTransportMode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
	{
		const auto color = getColor(inter, odir);

		return {
			{isHemisphereValid(inter.lobe().getHemisphere(idir)) ? color.color * (idir /odot/ inter.lobe().normal /oabs) * std::numbers::inv_pi_v<Real> : Black, color.alpha},
			getColorAlbedo(color.color)
		};
	}

	Color BxDFBase::alpha(EnumTransportMode, const SurfaceInteraction &inter, const VecD odir, const VecD /*idir*/) const noexcept
	{
		return getColorAlpha(inter, odir);
	}

	OptionalPDF<Real> BxDFBase::PDF(EnumTransportMode, const SurfaceInteraction &inter, const VecD /*odir*/, const VecD idir) const noexcept
	{
		return isHemisphereValid(inter.lobe().getHemisphere(idir)) ? (idir /odot/ inter.lobe().normal /oabs) * std::numbers::inv_pi_v<Real> : 0;
	}

	void BxDFChainBase::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		addParamReference(mBxdf, params, "bxdf");
	}

	ParameterTree BxDFChainBase::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		if(mBxdf)
			params["bxdf"].fromEntityRef(*mBxdf, deep);

		return params;
	}

	#define PRT_BxDFChainBase_GetSurfaceInfo auto newInter = getSurfaceInfo(inter, odir)

	BxDFRadianceResult BxDFChainBase::radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
		{ PRT_BxDFChainBase_GetSurfaceInfo; return mBxdf->radiance(mode, newInter, odir, idir); }
	pl::marked_optional<SampledBxDF> BxDFChainBase::sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask) const noexcept
		{ PRT_BxDFChainBase_GetSurfaceInfo; return mBxdf->sampleDir(mode, rnd, newInter, odir, rng, typeMask); }
	Color BxDFChainBase::alpha(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
		{ PRT_BxDFChainBase_GetSurfaceInfo; return mBxdf->alpha(mode, newInter, odir, idir); }
	OptionalPDF<Real> BxDFChainBase::PDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
		{ PRT_BxDFChainBase_GetSurfaceInfo; return mBxdf->PDF(mode, newInter, odir, idir); }

	SurfaceInteraction BxDFChainBase::getSurfaceInfo(const SurfaceInteraction &inter, const VecD /*odir*/) const noexcept
	{
		return inter;
	}


	void MicrofacetsBase::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		if(const auto p = params.findParam("roughness"); p && !p->value.empty())
		{
			if(p->isEntity())
				addReferenceInto(mParam, *p);
			else
			{
				auto v = entityCollection->createEntity(type_v<MapperRealValue>, "surfacemap_float-value", "");
				v->mValue = pl::plclamp<Real>(params["roughness"], 0, 1);
				mParam = v;
			}
		}
		else
			mParam = nullptr;
	}

	ParameterTree MicrofacetsBase::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		if(mParam)
			params["roughness"].fromEntityRef(*mParam, deep);

		return params;
	}



	void FresnelBase::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		if(const auto p = params.findParam("rn"); p)
			setIOR(schlickR0toIndex(r_(*p)));
		else
			params.getValueIfExistsBind("ior", PL_LAMBDA_FORWARD_THIS(setIOR));
	}

	ParameterTree FresnelBase::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params["ior"] = ior;

		return params;
	}

	pl::marked_optional<VecD> FresnelBase::refract(EnumTransportMode, const SurfaceInteraction &inter, const VecD odir, const VecD halfVector) const noexcept
	{
		auto dp_oh = -(odir /odot/ halfVector);
		Real iorExiting, ciorInv, dir = 1;

		if(dp_oh < 0)
		{
			dp_oh = -dp_oh;
			dir = -1;
			iorExiting = inter.nextSubstance->isSubstance() ? inter.nextSubstance->getRefractionIndex() : ior;
			ciorInv = inter.enteredSub->getRefractionIndexInvert();
		}
		else
		{
			iorExiting = inter.enteredSub->getRefractionIndex();
			ciorInv = inter.nextSubstance->isSubstance() ? inter.nextSubstance->getRefractionIndexInvert() : iorInv;
		}

		const auto eta = iorExiting * ciorInv;
		const auto eta2 = eta /opow2;

    const auto sin2ThetaI = pl::plmax<Real>(0, 1 - dp_oh /opow2);
    const auto sin2ThetaT = eta2 * sin2ThetaI;

		if(sin2ThetaT >= 1) // TIR
			return pl::nullopt;

		const auto cosThetaT = (1 - sin2ThetaT) /oprtsqrt;
    const auto resdir = odir * eta + halfVector * (eta * dp_oh - cosThetaT) * dir;

		return pl::assert_valid(resdir);
	}

}
