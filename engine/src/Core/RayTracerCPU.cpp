/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/RayTracerCPU.hpp>
#include <PhotoRT/Core/Scene.hpp>
#include <PhotoRT/Core/Instance.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

#include <patlib/pointer.hpp>
#include <patlib/cpu.hpp>

namespace PhotoRT
{
	// Ray-tracers
	PRT_ENTITY_FACTORY_DEFINE(CPURayTracerEngine)

	CPURayTracerEngine::CPURayTracerEngine()
	{
		#ifdef PL_LIB_NUMA
			if(numa_available() > -1)
				mContainerData.resize(numa_max_node() + 1);
			else
		#endif
				mContainerData.resize(1);

		mContainerGenerators.resize(mContainerData.size());
	}

	void CPURayTracerEngine::clearAccelStructures()
	{
		mReady = false;
		mFirstContainer = nullptr;

		for(auto &c : mContainerData)
			c.clear();
		for(auto &c : mContainerGenerators)
			c.clear();
	}

	void CPURayTracerEngine::copyContainer(const ContainerData &from, ContainerData &to, ContainerGenerators &generators)
	{
		to.accelStructures.reserve(from.accelStructures.size());
		for(const auto &c : from.accelStructures)
		{
			auto newc = c->clone_unique(generators.monoMemRes);
			newc->setMemoryResource(generators.defaultMemRes, &generators.monoMemRes);
			newc->mGlobalContainers = &to.accelStructures;
			*newc = *c;
			to.accelStructures.push_back(std::move(newc));
		}
	}

	void CPURayTracerEngine::buildContainer(ContainerData &data, ContainerGenerators &generators)
	{
		if(!mScene->getRootInstance())
			return;

		log() << "Building acceleration structures...";

		std::map<const IObjectInstance *, u32> instToContMap;
		std::vector<EntityContainerInstanceTree *> trees;

		//data.monoMemRes.setup(1024 * 1024 * 1024, alignof(std::max_align_t), *data.defaultMemRes);
		/*pl::make_unique(data.monoMemResBoost, 1024 * 1024 * 1024, &data.defaultMemResAdapted);
		data.monoMemRes.setResource(*data.monoMemResBoost);*/
		trees.reserve(1024);
		data.accelStructures.reserve(1024);

		const auto addToTree = [&](EntityContainerInstanceTree &t, szt index, PrimGroupPropertiesMapPtr &newPropMap, const Matrix &curMatrix, const Matrix &curMatrixInv)
			{
				t.mLeafContainer.mContainers.reserve(1024);
				auto &entry = t.mLeafContainer.mContainers.emplace_back();

				if(t.doAABBchecks())
				{
					const auto b = data.accelStructures[index]->getAABox();
					plassert(!pl::plisnan(b));

					if(!b.is_valid())
						t.setAABBchecks(false);

					entry.aaboxWorld = pl::plisinf(b) ? b : safeIncrement(b * curMatrix);
					//std::cout << b << '\n' << entry.aaboxWorld << '\n';
					t.mAaboxTotal = t.mAaboxTotal.join(entry.aaboxWorld);
				}

				entry.worldMatrix = curMatrix;
				entry.worldToLocalMatrix = curMatrixInv;
				entry.containerIndex = index;
				entry.propMap = newPropMap;
			};

		pl::tasks.task_group([&](auto &tg)
		{

			traverseRecursiveProperties(mScene->getRootInstance(),
				// Visit node
				[&](auto &inst, PrimGroupPropertiesMapPtr &newPropMap, const Matrix &curMatrix, const Matrix &curMatrixInv)
				{
					auto curTree = trees.empty() ? nullptr : trees.back();
					TraverseRecursiveState state = TraverseRecursiveState::Continue;

					if(const auto flags = inst->getFlags(); flags[InstanceFlagEnum::TreeGroup])
					{
						auto index = pl::find_key_value(instToContMap, std::to_address(inst));

						if(!index)
						{
							auto treeInstPtr = generators.containerTreeFactory.create_unique(type_v<EntityContainer>, data.accelStructures);
							auto &treeInst = static_cast<EntityContainerInstanceTree &>(*treeInstPtr);
							treeInst.setMemoryResource(generators.defaultMemRes, &generators.monoMemRes);
							treeInst.attachToInstance(*inst);

							treeInst.setAABBchecks(flags[InstanceFlagEnum::AabbCheck]);
							treeInst.mAaboxTotal.set_inflowest();

							index = static_cast<u32>(data.accelStructures.size());
							instToContMap.emplace(std::to_address(inst), *index);
							data.accelStructures.push_back(std::move(treeInstPtr));

							trees.push_back(&treeInst);
						}

						if(curTree)
							addToTree(*curTree, *index, newPropMap, curMatrix, curMatrixInv);
					}
					else if(flags[InstanceFlagEnum::Group])
					{
						auto index = pl::find_key_value(instToContMap, std::to_address(inst));

						if(!index)
						{
							auto accelStructMultiPtr = generators.containerMultiFactory.create_unique(type_v<EntityContainer>, data.accelStructures);
							auto &accelStructMulti = static_cast<EntityContainerMulti &>(*accelStructMultiPtr);

							accelStructMulti.setMemoryResource(generators.defaultMemRes, &generators.monoMemRes);

							accelStructMulti.mChildContainers.reserve(generators.containerFactories.size());
							for(auto &cf : generators.containerFactories | rgv::values)
							{
								auto cont = cf.factory->create_unique();
								cont->setMemoryResource(generators.defaultMemRes, &generators.monoMemRes);
								cont->mGlobalContainers = &data.accelStructures;
								cont->setParameters(cf.params);

								accelStructMulti.mChildContainers.push_back(data.accelStructures.size());
								data.accelStructures.push_back(std::move(cont));
							}

							accelStructMulti.attachToInstance(*inst);

							if(!accelStructMulti.mChildContainers.empty())
							{
								index = static_cast<u32>(data.accelStructures.size());

								instToContMap.emplace(std::to_address(inst), *index);
								data.accelStructures.push_back(std::move(accelStructMultiPtr));

								tg.run([as = &accelStructMulti]{ as->finalize(); });
							}
						}

						if(curTree && index)
							addToTree(*curTree, *index, newPropMap, curMatrix, curMatrixInv);

						state = TraverseRecursiveState::Stop;
					}

					return state;
				},

				// After visiting child nodes
				[&](auto &inst, auto&&...)
				{
					if(!trees.empty() && trees.back()->getInstance() == std::to_address(inst))
					{
						auto t = trees.back();
						t->mLeafContainer.mContainers.shrink_to_fit();
						tg.run([=]{ t->finalize(); });

						trees.pop_back();
					}
				}
			);

			/*for(auto &ac : data.accelStructures)
			{
				if(const auto acs = std::to_address(ac); typeid(*acs) == pl::any_of{typeid(EntityContainerInstanceTree), typeid(EntityContainerMulti)})
					tg.run([acs]{ acs->finalize(); });
			}*/

		});

		data.accelStructures.shrink_to_fit();

		for(auto &ac : data.accelStructures | rgv::filter([&](auto &ac){ return typeid(*ac) == typeid(EntityContainerMulti); }))
		{
			auto &ecm = static_cast<EntityContainerMulti &>(*ac);

			std::erase_if(ecm.mChildContainers, [&](const auto &c){ return !data.accelStructures[c]->getNbPrimitives(); });

			if(ecm.mChildContainers.size() == 1)
			{
				ac.swap(data.accelStructures[ecm.mChildContainers[0]]);
				ecm.mChildContainers.clear();
				ecm.mChildContainers.shrink_to_fit();
			}
		}

		const auto rootIndex = pl::find_key_value(instToContMap, std::to_address(mScene->getRootInstance()));
		if(!rootIndex)
			throw Exception("No root instance found in scene.");

		mRootIndex = *rootIndex;
		data.rootAccelStructureIndex = mRootIndex;

		log() << "Building acceleration structures done.";
	}

	void CPURayTracerEngine::init(const Scene &scene)
	{
		clearAccelStructures();

		mScene = &scene;

		#ifdef PL_LIB_NUMA

			if(numa_available() > -1)
			{
				const auto mask = numa_get_run_node_mask();

				for(const auto i : mir(static_cast<int>(mContainerData.size())))
				{
					if(numa_bitmask_isbitset(mask, i))
					{
						pl::numa::scoped_alloc_on_node numarun{i};

						mContainerGenerators[i].numaMemRes.node(i);

						mFirstContainer = &mContainerData[i];

						buildContainer(mContainerData[i], mContainerGenerators[i]);
						break;
					}
				}

				pl::tasks.task_group([&](auto &tg)
				{
					for(const auto i : mir(static_cast<int>(mContainerData.size())))
					{
						if(numa_bitmask_isbitset(mask, i) && &mContainerData[i] != mFirstContainer)
						{
							tg.run([this, i]
							{
								pl::numa::scoped_run_on_node numarun{i};

								copyContainer(*mFirstContainer, mContainerData[i], mContainerGenerators[i]);
								mContainerData[i].rootAccelStructureIndex = mRootIndex;
							});
						}
					}
				});
			}
			else
		#endif
			{
				buildContainer(mContainerData[0], mContainerGenerators[0]);
				mFirstContainer = &mContainerData[0];
			}

		mReady = true;
	}

	void CPURayTracerEngine::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		for(auto &c : mContainerGenerators)
			c.containerFactories.clear();

		if(const auto p = params.findParam("accel_struct"); p && p->childParams)
		{
			for(const auto &param : *p->childParams)
			{
				if(param.second.contains("pid") && param.second.contains("factory"))
				{
					u32 pid = param.second["pid"];
					auto params = param.second.contains("params") && param.second["params"].childParams ? *param.second["params"].childParams : ParameterTree{};

					for(auto &c : mContainerGenerators)
					{
						auto &entry = c.containerFactories[pid] = {
							addReference(type_v<pl::IGenericSmartFactory<EntityContainer>>, param.second["factory"]),
							std::move(params)
						};

						if(!entry.factory)
						{
							log(pl::logtag::error) << "Error creating container factory " << pid;
							c.containerFactories.erase(pid);
						}
						else
							dynamic_cast<pl::IMemoryResourceConsumer &>(*entry.factory).memory_resource(&c.monoMemRes);
					}
				}
			}
		}
	}

	ParameterTree CPURayTracerEngine::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		for(auto&& [i, cf] : rgv::enumerate(mContainerGenerators[0].containerFactories))
		{
			params["accel_struct"][i]["pid"] = cf.first;
			params["accel_struct"][i]["factory"].fromEntityRef(*cf.second.factory, deep);
			params["accel_struct"][i]["params"] = cf.second.params;
		}

		return params;
	}

	void CPURayTracerEngine::printInfo(std::ostream &outputStream) const
	{
		if(mFirstContainer)
		{
			outputStream << "Registered containers: " << mFirstContainer->accelStructures.size() << '\n';

			if(mFirstContainer)
				mFirstContainer->accelStructures[mFirstContainer->rootAccelStructureIndex]->dumpInfo(outputStream, 1);
		}
	}

	std::unique_ptr<IRayTracer> CPURayTracerEngine::createTracer([[maybe_unused]] int index, bool withStats)
	{
		const auto createRT =
			[&](auto type)
			{
				auto rt = std::make_unique<RayTracerVirtual<typename decltype(+type)::type>>(*this);

				#ifdef PL_LIB_NUMA

					if(index < 0)
						index = numa_available() > -1 ? numa_node_of_cpu(pl::get_cpu()) : 0;

					plassert(index < static_cast<int>(mContainerData.size()));

					rt->mData = &mContainerData[index];

				#else

					rt->mData = &mContainerData[0];

				#endif

				return rt;
			};

		std::unique_ptr<IRayTracer> rt;

		if(withStats)
			rt = createRT(type_v<CPURayTracer<true>>);
		else
			rt = createRT(type_v<CPURayTracer<false>>);

		return rt;
	}
}
