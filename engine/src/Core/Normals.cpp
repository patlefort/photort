/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Normals.hpp>
#include <PhotoRT/Core/BxDF.hpp>
#include <PhotoRT/Core/PrimitiveGroup.hpp>
#include <PhotoRT/Core/Material.hpp>
#include <PhotoRT/Core/RayTracer.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

namespace PhotoRT
{
	// Read-only bitmaps
	PRT_ENTITY_FACTORY_DEFINE(BitmapToBumpmap2D)

	// Normal maps
	PRT_ENTITY_FACTORY_DEFINE(NormalMapBump2D)
	PRT_ENTITY_FACTORY_DEFINE(NormalMapBump3D)
	PRT_ENTITY_FACTORY_DEFINE(NormalMapMulti)


	Color BitmapToBumpmap2D::getPixel(if32 u, if32 v) const noexcept
	{
		Color res{};
		Real pixCenter, pixLeft, pixRight, pixUp, pixDown;

		const auto width = (if32)mBitmap->getWidth();
		const auto height = (if32)mBitmap->getHeight();

		if(mTiled)
		{
			pixCenter = mBitmap->getPixel(u, v).max3(),
			pixLeft = mBitmap->getPixel(u == 0 ? width - 1 : u - 1, v).max3(),
			pixRight = mBitmap->getPixel((u + 1) % width, v).max3(),
			pixUp = mBitmap->getPixel(u, v == 0 ? height - 1 : v - 1).max3(),
			pixDown = mBitmap->getPixel(u, (v + 1) % height).max3();
		}
		else
		{
			pixCenter = mBitmap->getPixel(u, v).max3(),
			pixLeft = u == 0 ? 0 : mBitmap->getPixel(u - 1, v).max3(),
			pixRight = u >= width - 1 ? 0 : mBitmap->getPixel(u + 1, v).max3(),
			pixUp = v == 0 ? 0 : mBitmap->getPixel(u, v - 1).max3(),
			pixDown = v >= height - 1 ? 0 : mBitmap->getPixel(u, (v + 1) % height).max3();
		}

		res.set( (pixLeft - pixCenter) + (pixCenter - pixRight), size_v<0> );
		res.set( (pixUp - pixCenter) + (pixCenter - pixDown), size_v<1> );

		return res;
	}

	VecD NormalMapBump2D::map(const MaterialInteraction &inter, const VecD odir, type_c<VecD>) const noexcept
	{
		return pl::visit_one(inter, pl::overload_set
			{
				[&](const SurfaceInteraction &i)
				{
					if(!mDist)
						return i.surface().shadingNormal;

					const auto tangentSpace = std::invoke([&]{
							auto m = getTangentSpace(i, odir).transpose();
							m[0] = m[0] /oprtnormalize;
							m[1] = m[1] /oprtnormalize;
							return m;
						});
					const auto color = (*mDist)(tangentSpace[3][size_v<0>], tangentSpace[3][size_v<1>]);
					const auto res = i.surface().shadingNormal + (VecD)tangentSpace[0] * color[size_v<0>] * mStrength + (VecD)tangentSpace[1] * color[size_v<1>] * mStrength;
					const auto l = res /oprtlength;

					return pl::assert_valid( l && pl::is_valid(res, valid::Weight) ? res /olnormalize/ l : i.surface().shadingNormal );
				},
				[&](const auto &)
				{
					return VecD{pl::tag::identity};
				}
			});
	}

	void NormalMapBump2D::setParameters(const ParameterTree &params)
	{
		SurfaceMapperDist<VecD, IColorDistribution2D>::setParameters(params);

		params.getValueIfExistsBind("strength", PL_LAMBDA_FORWARD_THIS(setStrength));
	}

	ParameterTree NormalMapBump2D::getParameters(bool deep) const
	{
		ParameterTree params {SurfaceMapperDist<VecD, IColorDistribution2D>::getParameters(deep)};

		params["strength"] = mStrength;

		return params;
	}


	VecD NormalMapBump3D::map(const MaterialInteraction &inter, const VecD odir, type_c<VecD>) const noexcept
	{
		return pl::visit_one(inter, pl::overload_set
			{
				[&](const SurfaceInteraction &i)
				{
					if(!mDist)
						return i.surface().shadingNormal;

					const auto tangentSpace = std::invoke([&]{
							auto m = getTangentSpace(i, odir).transpose();
							m[0] = m[0] /oprtnormalize;
							m[1] = m[1] /oprtnormalize;
							return m;
						});
					const auto color = (*mDist)(tangentSpace[3][size_v<0>], tangentSpace[3][size_v<1>], tangentSpace[3][size_v<2>]);
					const auto res = i.surface().shadingNormal + (VecD)tangentSpace[0] * color[size_v<0>] * mStrength + (VecD)tangentSpace[1] * color[size_v<1>] * mStrength;
					const auto l = res /oprtlength;

					return pl::assert_valid( l && pl::is_valid(res, valid::Weight) ? res /olnormalize/ l : i.surface().shadingNormal );
				},
				[&](const auto &)
				{
					return VecD{pl::tag::identity};
				}
			});
	}

	void NormalMapBump3D::setParameters(const ParameterTree &params)
	{
		SurfaceMapperDist<VecD, IColorDistribution3D>::setParameters(params);

		params.getValueIfExistsBind("strength", PL_LAMBDA_FORWARD_THIS(setStrength));

	}

	ParameterTree NormalMapBump3D::getParameters(bool deep) const
	{
		ParameterTree params {SurfaceMapperDist<VecD, IColorDistribution3D>::getParameters(deep)};

		params["strength"] = mStrength;

		return params;
	}


	VecD NormalMapMulti::map(const MaterialInteraction &inter, const VecD odir, type_c<VecD>) const noexcept
	{
		return pl::visit_one(inter, pl::overload_set
			{
				[&](const SurfaceInteraction &i)
				{
					auto interCopy = i;

					for(const auto &m : mMappers)
						interCopy.surface().shadingNormal = m->map(interCopy, odir);

					return interCopy.surface().shadingNormal;
				},
				[&](const auto &)
				{
					return VecD{pl::tag::identity};
				}
			});
	}
}
