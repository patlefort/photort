/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/RayTraversalContainers.hpp>
#include <PhotoRT/Core/Instance.hpp>
#include <PhotoRT/Core/PrimitiveGroup.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/Intersection.hpp>

#include <patlib/scoped.hpp>

namespace PhotoRT
{
	RayBatchMask EntityContainer::traverseBatch(RayBatch &batch, RayBatchMask mask) const noexcept
	{
		RayBatchMask resMask{};

		for(const auto i : mir(batch.size()) | rgv::filter([&](const auto i){ return mask[i]; }))
			batch[i].distmax = batch[i].result.update(
					traverse(batch[i].ray, batch[i].distmax),
					batch[i].distmax,
					[&](const auto &){ resMask.set(i); return true; }
				);

		return resMask;
	}

	void EntityContainerMulti::dumpInfo(std::ostream &outputStream, if32 level) const
	{
		EntityContainer::dumpInfo(outputStream, level);

		++level;

		outputStream << pl::io::indent(level) << "Nb: " << mChildContainers.size() << '\n';

		for(const auto &c : getContainerRange())
			c.second.dumpInfo(outputStream, level);
	}

	Box EntityContainerMulti::getAABox() const noexcept
	{
		return rg::accumulate(
			getContainerRange(),
			Box{pl::tag::inflowest},
			pl::accumulator_join,
			[&](auto&& c){ return c.second.getAABox(); }
		);
	}

	CollisionResult EntityContainerMulti::traverse(const BasicRay &ray, Real distmax) const noexcept
	{
		CollisionResult res;

		for(const auto &c : getContainerRange())
			distmax = res.update(c.second.traverse(ray, distmax), distmax);

		return res;
	}

	RayBatchMask EntityContainerMulti::traverseBatch(RayBatch &batch, RayBatchMask mask) const noexcept
	{
		return rg::accumulate(getContainerRange(), RayBatchMask{},
				[&](const auto &init, auto &&c){ return init | c.second.traverseBatch(batch, mask); }
			);
	}


	void EntityContainerInstanceTree::finalize()
	{
		mBvhData.reset();
		mLeafContainer.clear();

		if(doAABBchecks() && mLeafContainer.mContainers.size() >= 4)
		{
			{
				BuilderType::BuildOptions opts;
				opts.targetCost = 100;

				BuilderType bvhBuilder{mLeafContainer, opts, {}, {},
						typename BuilderType::NodeVectorType::allocator_type{std::in_place, mMemRes}
					};

				bvhBuilder.build(mLeafContainer.createPrimitiveBoundaryList(), mAaboxTotal);
				auto data = bvhBuilder.compile();

				#ifndef NDEBUG
					bvhBuilder.validate<VecP>(mAaboxTotal, data);
				#endif

				mBvhData.emplace(std::move(data));
			}

			if(mLeafContainer.mLeaves.size() > 1)
				mLeafContainer.finalize();
			else
			{
				mBvhData.reset();
				mLeafContainer.clear();
			}
		}
	}

	CollisionResult EntityContainerInstanceTree::traverse(const BasicRay &ray, Real distmax) const noexcept
	{
		CollisionResult data;
		VecP colPos;

		if(!pl::plisinf(distmax))
			colPos = ray.origin + ray.dir * (distmax /oprtsqrt);

		auto handlerFc =
			[&](const ChildContainerEntry &c)
			{
				const auto dir = dirSafeTransform(ray.dir, c.worldToLocalMatrix);
				const BasicRay newRay{
						.origin = c.worldToLocalMatrix * ray.origin,
						.dir = dir,
						.dirInv = dirReciprocal(dir)
					};

				const auto dist = std::invoke([&]{
						if(pl::plisinf(distmax))
							return distmax;

						const auto localPos = c.worldToLocalMatrix * colPos;
						return static_cast<VecD>(localPos - newRay.origin) /osdot;
					});

				data.update(
						getContainer(c.containerIndex).traverse(newRay, dist),
						dist,
						[&](auto &col)
						{
							colPos = col.surface.worldPos = c.worldMatrix * col.surface.worldPos;
							col.surface *= c.worldMatrix;
							distmax = col.distSqr = (colPos - ray.origin) /osdot;

							col.surface.objProps = selectPrimGroupProps(col.surface.inst->getObject()->getClassName(), col.surface.objProps, c.propMap);

							return true;
						}
					);
			};

		if(!doAABBchecks())
		{
			for(const auto &c : mLeafContainer.mContainers)
				handlerFc(c);
		}
		else
		{
			if(mLeafContainer.mLeaves.empty())
			//if(true)
			{
				#ifdef PRT_EXT_STATS
					data.nbAABBTest += mLeafContainer.mContainers.size();
				#endif

				for(const auto &c : mLeafContainer.mContainers)
				{
					const auto [mask, distmin2, distmax2] = Intersection::testAABBMinMax(c.aaboxWorld.v1, c.aaboxWorld.v2, ray.dirInv, ray.origin);

					// Must use <= to handle infinity
					if( mask.any() && (distmin2 < 0 || distmin2 /opow2 <= distmax) )
						handlerFc(c);
				}
			}
			else
			{
				#ifdef PRT_EXT_STATS
					++data.nbAABBTest;
				#endif

				const auto [mask, distmin2, distmax2] = Intersection::testAABBMinMax(mAaboxTotal.v1, mAaboxTotal.v2, ray.dirInv, ray.origin);

				if(mask.any())
				{
					AccelStruct::BVH<typename BuilderType::IndexType, VecP>
						bvh{ mBvhData->mNodes, mBvhData->mRootNode };

					bvh.traverse(ray.origin, ray.dir, ray.dirInv, distmin2, distmax2, distmax,
						[&](auto index, Real /*dmin*/, Real /*dmax*/, Real /*colDistSqr*/)
						{
							const auto start = mLeafContainer.mLeaves[index];
							const auto nb = mLeafContainer.getNbPrimitives(index);

							#ifdef PRT_EXT_STATS
								data.nbAABBTest += nb;
							#endif

							mLeafContainer.mLeafContainers
								| rgv::drop_exactly(start) | rgv::take_exactly(nb)
								| rgv::transform([&](auto&& e) -> const auto& { return mLeafContainer.mContainers[e]; })
								| pl::for_each([&](const auto &c)
									{
										const auto [mask, distmin3, distmax3] = Intersection::testAABBMinMax(c.aaboxWorld.v1, c.aaboxWorld.v2, ray.dirInv, ray.origin);

										// Must use <= to handle infinity
										if( mask.any() && (distmin3 < 0 || distmin3 /opow2 <= distmax) )
											handlerFc(c);
									});

							return distmax;
						}
					);
				}
			}
		}

		return data;
	}

	RayBatchMask EntityContainerInstanceTree::traverseBatch(RayBatch &batch, RayBatchMask mask) const noexcept
	{
		struct RayInfoBackup
		{
			VecP bkOrigin;
			VecD bkDir, bkDirInv;
			Real bkColDist;
		};

		if(!mLeafContainer.mLeaves.empty() || !doAABBchecks())
			return EntityContainer::traverseBatch(batch, mask);

		RayBatchMask hitMask{};
		std::array<RayInfoBackup, RayBatchMaxSize> bk;

		for(const auto &c : mLeafContainer.mContainers)
		{
			RayBatchMask aabbMask{};
			szt nbValid = 0, validIndex = std::numeric_limits<szt>::max();

			for(const auto r : mir(batch.size()))
			{
				auto &d = batch[r];
				auto &ray = d.ray;

				if(mask[r])
				{
					#ifdef PRT_EXT_STATS
						++d.result.nbAABBTest;
					#endif

					const auto [mask, distmin, distmax] = Intersection::testAABBMinMax(c.aaboxWorld.v1, c.aaboxWorld.v2, ray.dirInv, ray.origin);

					// Must use <= to handle infinity
					if( mask.any() && (distmin < 0 || distmin /opow2 <= d.distmax) )
					{
						aabbMask.set(r);
						++nbValid;
						validIndex = r;

						auto &b = bk[r];

						d.distmax = distmax;

						b.bkOrigin = ray.origin;
						b.bkDir = ray.dir;
						b.bkDirInv = ray.dirInv;
						b.bkColDist = d.distmax;

						ray.origin = c.worldToLocalMatrix * ray.origin;
						ray.dir = dirSafeTransform(ray.dir, c.worldToLocalMatrix);
						ray.dirInv = dirReciprocal(ray.dir);

						if(d.result.collision)
						{
							const auto p = c.worldToLocalMatrix * (ray.origin + ray.dir * d.distmax);
							d.distmax = static_cast<VecD>(p - ray.origin) /osdot;
						}
					}
				}
			}

			if(!nbValid)
				continue;

			RayBatchMask curHitMask{};

			if(nbValid > 1)
			{
				curHitMask = getContainer(c.containerIndex).traverseBatch(batch, aabbMask);
			}
			else
			{
				auto &d = batch[validIndex];

				d.distmax = d.result.update(
						getContainer(c.containerIndex).traverse(d.ray, d.distmax),
						d.distmax,
						[&](const auto &){ curHitMask.set(validIndex); return true; }
					);
			}

			hitMask |= curHitMask;

			for(const auto r : mir(batch.size()))
			{
				auto &d = batch[r];
				auto &ray = d.ray;

				if(curHitMask[r])
				{
					auto &b = bk[r];

					ray.origin = b.bkOrigin;
					ray.dir = b.bkDir;
					ray.dirInv = b.bkDirInv;

					auto &surface = d.result.collision->surface;

					surface.worldPos = c.worldMatrix * surface.worldPos;
					surface *= c.worldMatrix;

					d.distmax = d.result.collision->distSqr = static_cast<VecD>(surface.worldPos - ray.origin) /osdot;

					surface.objProps = selectPrimGroupProps(surface.inst->getObject()->getClassName(), surface.objProps, c.propMap);
				}
				else if(aabbMask[r])
				{
					auto &b = bk[r];

					ray.origin = b.bkOrigin;
					ray.dir = b.bkDir;
					ray.dirInv = b.bkDirInv;

					d.distmax = b.bkColDist;
				}
			}
		}

		return hitMask;
	}

	void EntityContainerInstanceTree::attachToInstance(const IObjectInstance &inst)
	{
		mInstance = &inst;
	}

	void EntityContainerInstanceTree::setMemoryResource(std::pmr::memory_resource *mr, std::pmr::memory_resource *monomr)
	{
		EntityContainer::setMemoryResource(mr, monomr);

		pl::recreate(mLeafContainer.mContainers, typename decltype(mLeafContainer.mContainers)::allocator_type{std::in_place, mr});
		mLeafContainer.setMemoryResource(mr);
	}

	void EntityContainerInstanceTree::clearContainer()
	{
		mBvhData.reset();
		mLeafContainer.clear();
		mLeafContainer.mContainers.clear();
	}

	Real EntityContainerInstanceTree::judgeCost() const
	{
		return rg::accumulate(
			getContainerRange(),
			Real{},
			std::plus{},
			[&](auto &&c){ return c.second.judgeCost(); }
		);
	}

	Real EntityContainerInstanceTree::judgeGeometryCost() const
	{
		return rg::accumulate(
			getContainerRange(),
			Real{},
			std::plus{},
			[&](auto &&c){ return c.second.judgeGeometryCost(); }
		);
	}

	void EntityContainerInstanceTree::dumpInfo(std::ostream &outputStream, if32 level) const
	{
		EntityContainer::dumpInfo(outputStream, level);

		++level;

		outputStream
			<< pl::io::indent(level) << "Nb: " << mLeafContainer.mContainers.size() << '\n'
			<< pl::io::indent(level) << "AABB checks: " << mDoAABB << '\n';

		if(!mLeafContainer.mLeaves.empty())
		{
			outputStream << pl::io::indent(level) << "BVH: \n";
			//mBvh.dump(outputStream, level);
		}

		for(auto &c : mLeafContainer.mContainers)
			getContainer(c.containerIndex).dumpInfo(outputStream, level);
	}

	PrimitiveIndex EntityContainerInstanceTree::getNbPrimitives() const
	{
		return rg::accumulate(
			getContainerRange(),
			PrimitiveIndex{},
			std::plus{},
			[&](auto &&c){ return c.second.getNbPrimitives(); }
		);
	}

	Box EntityContainerInstanceTree::getAABox() const noexcept
	{
		return mAaboxTotal;
	}
}
