/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Texture.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/Instance.hpp>
#include <PhotoRT/Core/PrimitiveGroup.hpp>
#include <PhotoRT/Core/Transform.hpp>

namespace PhotoRT
{
	void MapperUVTransformBase::setTransform(EntityPtr<const ITransform> tr)
	{
		mTransform = std::move(tr);

		if(mTransform)
		{
			mMat = (*mTransform)();
			mInvMat = mTransform->inverse();
		}
		else
		{
			mMat.set_identity();
			mInvMat.set_identity();
		}

		mMatTransposed = mMat.transpose();
	}

	Matrix MapperUVTextureCoord::map(const MaterialInteraction &inter, const VecD /*odir*/, type_c<Matrix>) const noexcept
	{
		const auto index = getUVIndex();

		auto res = pl::visit_one(inter, [&](const auto &i) -> Matrix
		{
			if constexpr(pl::is_a<decltype(i), SurfaceInteraction>)
			{
				return i.instance() ?
					i.instance()->getObject()->getTextureTangentSpace(
							i.surface(),
							index >= 0 ? &i.instance()->getObject()->getUVmap(index) : nullptr,
							mMat
						)
					:
						TransformTranslation{i.surface().uv}();
			}
			else
				return {};
		});

		if(mTiled)
		{
			const pl::scoped_transpose sg{res};
			res[3] = doTiling(res[3]);
		}

		return pl::assert_valid(res);
	}


	// Non-discrete color distributions
	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedColorDistributionSimple)
	void AugmentedColorDistributionSimple::setParameters(const ParameterTree &params)
	{
		params.getValueIfExistsBind("v", PL_LAMBDA_FORWARD_THIS(setColor));
	}

	ParameterTree AugmentedColorDistributionSimple::getParameters(bool /*deep*/) const
	{
		ParameterTree params;

		params["v"] = mColor;

		return params;
	}


	PRT_ENTITY_FACTORY_DEFINE(ColorDistributionBitmap1D)
	PRT_ENTITY_FACTORY_DEFINE(ColorDistributionBitmap2D)
	PRT_ENTITY_FACTORY_DEFINE(ColorDistributionBitmap3D)
	PRT_ENTITY_FACTORY_DEFINE(ColorDistributionSingleChannel1D)
	PRT_ENTITY_FACTORY_DEFINE(ColorDistributionMultiChannel1D)
	PRT_ENTITY_FACTORY_DEFINE(ColorDistributionSingleChannel2D)
	PRT_ENTITY_FACTORY_DEFINE(ColorDistributionMultiChannel2D)
	PRT_ENTITY_FACTORY_DEFINE(ColorDistributionSingleChannel3D)
	PRT_ENTITY_FACTORY_DEFINE(ColorDistributionMultiChannel3D)

	// UV maps
	PRT_ENTITY_FACTORY_DEFINE(MapperUVPlanar)
	PRT_ENTITY_FACTORY_DEFINE(MapperUVPlanarWorld)
	PRT_ENTITY_FACTORY_DEFINE(MapperUVPanoramic)
	PRT_ENTITY_FACTORY_DEFINE(MapperUVPanoramicView)
	PRT_ENTITY_FACTORY_DEFINE(MapperUV3D)
	PRT_ENTITY_FACTORY_DEFINE(MapperUV3Dworld)
	PRT_ENTITY_FACTORY_DEFINE(MapperUVTextureCoord)
	PRT_ENTITY_FACTORY_DEFINE(MapperUVSpherical)

	// Surface mappers
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("surfacemap_color-value", AugmentedMapperColorSimple, MapperColorSimple)
	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedMapperColorSimple)
	void AugmentedMapperColorSimple::setParameters(const ParameterTree &params)
	{
		params.getValueIfExistsBind("v", PL_LAMBDA_FORWARD_THIS(setColor));
	}

	ParameterTree AugmentedMapperColorSimple::getParameters(bool /*deep*/) const
	{
		ParameterTree params;

		params["v"] = mColor;

		return params;
	}


	PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("surfacemap_color-dist2d", SurfaceMapper2D<Color, IColorDistribution2D>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("surfacemap_color-dist3d", SurfaceMapper3D<Color, IColorDistribution3D>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("surfacemap_color-multi", SurfaceMapperMulti<Color, std::plus<>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("surfacemap_color-multi_multiply", SurfaceMapperMulti<Color, std::multiplies<>>)

	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("surfacemap_float-value", AugmentedMapperRealValue, MapperRealValue)
	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedMapperRealValue)
	void AugmentedMapperRealValue::setParameters(const ParameterTree &params)
	{
		params.getValueIfExists("v", mValue);
	}

	ParameterTree AugmentedMapperRealValue::getParameters(bool /*deep*/) const
	{
		ParameterTree params;

		params["v"] = mValue;

		return params;
	}

	PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("surfacemap_float-dist2d", SurfaceMapper2D<Real, IDistributionMapper2D<Real, Real>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("surfacemap_float-dist3d", SurfaceMapper3D<Real, IDistributionMapper3D<Real, Real>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("surfacemap_float-multi", SurfaceMapperMulti<Real, std::plus<>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("surfacemap_float-multi_multiply", SurfaceMapperMulti<Real, std::multiplies<>>)
}
