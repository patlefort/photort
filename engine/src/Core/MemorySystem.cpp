/*
	PhotoRT

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/MemorySystem.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

#ifdef PRT_IPC
	#include <boost/interprocess/managed_shared_memory.hpp>
	#include <boost/interprocess/managed_heap_memory.hpp>
	#include <boost/interprocess/managed_mapped_file.hpp>

	#ifdef _WIN32
		#include <boost/interprocess/managed_windows_shared_memory.hpp>
	#endif
#endif

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("memory_system-heap", MemorySystemEntity<pl::HeapMemorySystem<VoidOffsetPtr>>)

	#ifdef PRT_IPC
		#ifdef _WIN32
			PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("memory_system-shared", MemorySystemEntity<pl::SharedMemorySystem<boost::interprocess::managed_windows_shared_memory>>)
		#else
			PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("memory_system-shared", MemorySystemEntity<pl::SharedMemorySystem<boost::interprocess::managed_shared_memory>>)
		#endif

		PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE("memory_system-file", MemorySystemEntity<pl::SharedMemorySystem<boost::interprocess::managed_mapped_file>>)
	#endif
}
