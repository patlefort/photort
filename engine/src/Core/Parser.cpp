/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Parser.hpp>

#ifdef PRT_XML_PARSER

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/sax/InputSource.hpp>
#include <xercesc/util/BinInputStream.hpp>
#include <xercesc/framework/BinOutputStream.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/StdOutFormatTarget.hpp>

#ifdef PRT_XML_PARSER_XALAN
	#include <xalanc/Include/PlatformDefinitions.hpp>
	#include <xalanc/XalanTransformer/XalanTransformer.hpp>
#endif

#include <boost/hana.hpp>

namespace PhotoRT
{
	const char *ParserException::module() const noexcept
		{ return "ParameterTreeParser"; }

	namespace
	{
		namespace xe = XERCES_CPP_NAMESPACE;

		#ifdef PRT_XML_PARSER_XALAN
			namespace xalan = XALAN_CPP_NAMESPACE;
		#endif

		template <typename T_>
		struct [[nodiscard]] XMLResourceGuard
		{
			pl::unique_unmanaged_pointer<T_> o;

			XMLResourceGuard() = default;
			XMLResourceGuard(T_ &o_) : o{&o_} {}

			~XMLResourceGuard() { if(o) o->release(); }
		};

		template <typename T_>
		struct [[nodiscard]] XMLStringGuard
		{
			pl::unique_unmanaged_pointer<T_> s;

			XMLStringGuard() = default;
			XMLStringGuard(T_ *o_) : s{o_} {}

			~XMLStringGuard() { if(s) xe::XMLString::release(&s); }

			[[nodiscard]] auto operator* () const noexcept { return s.get(); }
		};

		[[nodiscard]] auto transcode(const char *ch)
		{
			return XMLStringGuard{xe::XMLString::transcode(ch)};
		}

		[[nodiscard]] auto transcode(std::string_view ch)
		{
			return XMLStringGuard{xe::XMLString::transcode(ch.data())};
		}

		[[nodiscard]] auto transcode(const XMLCh *ch)
		{
			return std::string(*XMLStringGuard{xe::XMLString::transcode(ch)});
		}

		#ifdef PRT_XML_PARSER_XALAN
			[[nodiscard]] auto transcode(const xalan::XalanDOMString &str)
			{
				xalan::XalanDOMString::CharVectorType res;
				str.transcode(res);
				return std::string{res.begin(), res.end()};
			}
		#endif

		template <typename... Tag_>
		struct XmlAttr
		{
			hn::map<hn::pair<hn::type<Tag_>, XMLCh *>...> tags;

			XmlAttr()
			{
				hn::for_each(tags, [](auto&& c){ hn::second(c) = xe::XMLString::transcode(pl::hana_type_as_value(hn::first(c)).name); });
			}

			~XmlAttr()
			{
				hn::for_each(tags, [](auto&& c){ xe::XMLString::release(&hn::second(c)); hn::second(c) = nullptr; });
			}

			template <typename T_>
			[[nodiscard]] auto get() const noexcept
			{
				return tags[hn::type_c<T_>];
			}
		};


		struct XmlAttrValue { static constexpr char name[] = "value"; };
		struct XmlAttrType { static constexpr char name[] = "type"; };
		struct XmlAttrArray { static constexpr char name[] = "array"; };
		struct XmlAttrLink { static constexpr char name[] = "link"; };
		struct XmlAttrId { static constexpr char name[] = "id"; };
		struct XmlAttrNS { static constexpr char name[] = "http://photort.org/params/"; };
		struct XmlAttrScene { static constexpr char name[] = "scene"; };
		struct XmlAttr1 { static constexpr char name[] = "1"; };

		using XmlAttrList = XmlAttr<
			XmlAttrValue,
			XmlAttrType,
			XmlAttrArray,
			XmlAttrLink,
			XmlAttrId,
			XmlAttrNS,
			XmlAttrScene,
			XmlAttr1
		>;

		void handleXmlException(std::exception_ptr eptr, std::string_view message)
		{
			const auto buildMessage = [&](const auto &exceptionMessage, const char * const type)
				{
					return pl::strconcat(std::string{}, message, '\n', type, ": ", transcode(exceptionMessage));
				};

			try { std::rethrow_exception(eptr); }
			catch(const xe::XMLException &e)
			{
				throw ParserException(buildMessage(e.getMessage(), "XMLException"));
			}
			catch(const xe::DOMException &e)
			{
				throw ParserException(buildMessage(e.getMessage(), "DOMException"));
			}
			catch(const xe::SAXException &e)
			{
				throw ParserException(buildMessage(e.getMessage(), "SAXException"));
			}
			/*catch(const xe::OutOfMemoryException &e)
			{
				throw ParserException(buildMessage(e.getMessage(), "OutOfMemoryException"));
			}*/
			#ifdef PRT_XML_PARSER_XALAN
				catch(const xalan::XSLException &e)
				{
					throw ParserException(buildMessage(e.getMessage(), "XSLException"));
				}
			#endif
		}

		class StdInputBinStream : public xe::BinInputStream
		{
		public:
			StdInputBinStream(std::istream &is) : mIs(is) {}

			virtual XMLFilePos curPos() const final { return mIs.tellg(); }
			virtual XMLSize_t readBytes(XMLByte* const toFill, const XMLSize_t maxToRead) final
			{
				if(!mIs || mEndReached)
					return 0;

				mIs.getline(reinterpret_cast<char *>(toFill), maxToRead * sizeof(XMLByte), '\0');

				auto nb = mIs.gcount();

				if(mIs.bad())
					nb = 0;
				else
				{
					if(!mIs.fail() && !mIs.eof())
					{
						--nb;
						mEndReached = true;
					}

					mIs.clear();
				}

				return nb;
			}

			virtual const XMLCh *getContentType() const final { return nullptr; }

		protected:
			std::istream &mIs;
			bool mEndReached = false;
		};

		class StdInputStream : public xe::InputSource
		{
		public:

			StdInputStream(std::istream &is) : mIs(is) {}

			virtual xe::BinInputStream *makeStream() const final { return new StdInputBinStream(mIs); }

		protected:
			std::istream &mIs;
		};

		class StdOutputBinStream : public xe::XMLFormatTarget
		{
		public:
			StdOutputBinStream(std::ostream &os_) : os(os_) {}

			virtual void flush() final { os.flush(); }
			virtual void writeChars(const XMLByte *const toWrite, const XMLSize_t count, xe::XMLFormatter *const /*formatter*/) final
			{
				os.write(reinterpret_cast<const char *>(toWrite), count * sizeof(XMLByte));
			}

		protected:
			std::ostream &os;
		};

		void parseXercesDocRecursive(const XmlAttrList &attrList, ParameterTree &params, const xe::DOMElement &node)
		{
			const auto tx_ = [](const XMLCh *ch)
				{
					XMLStringGuard transcoded{xe::XMLString::transcode(ch)};
					return std::string{ pl::trim(std::string_view{*transcoded}, Parameter::loc) };
				};

			const auto getText = [&tx_](const xe::DOMNode &node)
				{
					std::string str;

					const auto &childs = *node.getChildNodes();
					for(const auto i : mir(childs.getLength()))
					{
						const auto &childNode = *childs.item(i);

						if(childNode.getNodeType() == xe::DOMNode::TEXT_NODE)
						{
							str = tx_(static_cast<const xe::DOMText &>(childNode).getWholeText());
							break;
						}
					}

					return str;
				};

			const auto valueOrText = [&tx_, &getText](const xe::DOMNode &node){ return node.getNodeType() == xe::DOMNode::ELEMENT_NODE ? getText(node) : tx_(node.getNodeValue()); };

			const bool isArray = tx_(node.getAttribute(attrList.get<XmlAttrArray>())) == "1";

			if(node.hasChildNodes())
			{
				uf32 index = 0;

				for(auto *c = node.getFirstChild(); c != nullptr; c = c->getNextSibling())
				{
					const auto &childNode = *c;

					if(childNode.getNodeType() != xe::DOMNode::ELEMENT_NODE ||
						!childNode.getNamespaceURI() ||
						!xe::XMLString::equals(childNode.getNamespaceURI(), attrList.get<XmlAttrNS>()))
						continue;

					const auto &element = static_cast<const xe::DOMElement &>(childNode);

					std::string key;

					if(isArray)
						key = pl::io::to_string_noloc(index++);
					else
					{
						key = tx_(element.getNodeName());

						if(key[0] == '_' && key.length() > 1)
							key.erase(0, 1);
					}

					if(params.find(key) != params.end())
						continue;

					auto &p = params[key];

					std::string typeAttr, idAttr, linkAttr, valueAttr;

					const auto handleNode = [&](const xe::DOMNode &childNode)
						{
							if((childNode.getNodeType() != xe::DOMNode::ELEMENT_NODE && childNode.getNodeType() != xe::DOMNode::ATTRIBUTE_NODE) || (childNode.getNamespaceURI() && !xe::XMLString::equals(childNode.getNamespaceURI(), attrList.get<XmlAttrNS>())))
								return;

							if(xe::XMLString::compareString(childNode.getNodeName(), attrList.get<XmlAttrType>()) == 0)
								typeAttr = valueOrText(childNode);
							else if(xe::XMLString::compareString(childNode.getNodeName(), attrList.get<XmlAttrId>()) == 0)
								idAttr = valueOrText(childNode);
							else if(xe::XMLString::compareString(childNode.getNodeName(), attrList.get<XmlAttrLink>()) == 0)
								linkAttr = valueOrText(childNode);
							else if(xe::XMLString::compareString(childNode.getNodeName(), attrList.get<XmlAttrValue>()) == 0)
								valueAttr = valueOrText(childNode);

							if(childNode.getNodeType() == xe::DOMNode::ATTRIBUTE_NODE)
							{
								std::string key { tx_(childNode.getNodeName()) };

								if(isReservedAttribute(key))
									return;

								if(key[0] == '_' && key.length() > 1)
									key.erase(0, 1);

								auto &ap = p[key];
								ap.value = tx_(childNode.getNodeValue());

								if(ap.value.empty())
									ap.value = "0";
							}
						};

					const auto &attributes = *element.getAttributes();
					for(const auto i : mir(attributes.getLength()))
						handleNode(*attributes.item(i));

					for(auto *c = element.getFirstChild(); c != nullptr; c = c->getNextSibling())
						handleNode(*c);

					if(!typeAttr.empty())
					{
						if(!linkAttr.empty())
							p.toEntity(typeAttr, linkAttr, true);
						else
							p.toEntity(typeAttr, idAttr, false);
					}
					else
					{
						if(!valueAttr.empty())
							p.value = valueAttr;
						else
						{
							p.value = getText(element);

							if(p.value.empty() && !element.hasChildNodes())
								p.value = "1";
						}
					}

					if(element.hasChildNodes() || element.hasAttributes())
					{
						p.hasChilds(true);
						parseXercesDocRecursive(attrList, *p.childParams, element);

						if(p.childParams->empty())
							p.hasChilds(false);
					}
				}
			}
		}

		void printDomLocator(std::ostream &os, const xe::DOMLocator &locator)
		{
			os << std::dec << "line " << locator.getLineNumber() << ", col " << locator.getColumnNumber();
			if(xe::XMLString::stringLen(locator.getURI()))
				os << " - " << transcode(locator.getURI());
		}

		class PRTDOMErrorHandler : public xe::DOMErrorHandler
		{
		public:
			inline static const std::map<int, std::string> errorLevelStr{
				{xe::DOMError::DOM_SEVERITY_WARNING,          "Warning"},
				{xe::DOMError::DOM_SEVERITY_ERROR,            "Error"},
				{xe::DOMError::DOM_SEVERITY_FATAL_ERROR,      "Fatal error"}
			};

			virtual bool handleError(const xe::DOMError &domError) final
			{
				//std::stringstream os;
				auto os = pl::log("ParameterTreeParser", pl::logtag::error);
				os << errorLevelStr.at(domError.getSeverity()) << ':';
				if(domError.getLocation())
					{ os << " ["; printDomLocator(os.stream(), *domError.getLocation()); os << ']'; }

				if(const auto m = transcode(domError.getMessage()); !m.empty())
					os << ' ' << m;

				return domError.getSeverity() == xe::DOMError::DOM_SEVERITY_WARNING;

				/*if(domError.getSeverity() == DOMError::DOM_SEVERITY_WARNING)
				{
					pl::log("ParameterTreeParser", pl::logtag::error) << os.str();
					return true;
				}

				throw Exception(os.str());

				return false;*/
			}
		};

		class PRTSAXErrorHandler : public xe::ErrorHandler
		{
		private:

			void displayMessage(const xe::SAXParseException &e, auto &os) const
			{
				os << '[' << std::dec << "line " << e.getLineNumber() << ", col " << e.getColumnNumber() << "] "
					<< transcode(e.getMessage());
			}

		public:

			virtual void warning(const xe::SAXParseException &e) override
			{
				auto entry = pl::log("ParameterTreeParser", pl::logtag::error);
				entry << "Warning: "; displayMessage(e, entry);
			}

			virtual void error(const xe::SAXParseException &e) override
			{
				auto entry = pl::log("ParameterTreeParser", pl::logtag::error);
				entry << "Error: "; displayMessage(e, entry);
			}

			virtual void fatalError(const xe::SAXParseException &e) override
			{
				auto entry = pl::log("ParameterTreeParser", pl::logtag::error);
				entry << "Fatal error: "; displayMessage(e, entry);
			}

			virtual void resetErrors() override {}
		};


		void prepareXercesParser(std::invocable<xe::DOMImplementationLS &, xe::DOMLSParser &, xe::DOMErrorHandler &> auto&& fc)
		{
			try
			{
				xe::XMLPlatformUtils::Initialize();
			}
			catch(...)
			{
				handleXmlException(std::current_exception(), "Failed to initialize Xerces-C.");
			}

			pl::scoped_guard xercesguard{[]{ xe::XMLPlatformUtils::Terminate(); }};

			#ifdef PRT_XML_PARSER_XALAN
				try
				{
					xalan::XalanTransformer::initialize();
				}
				catch(...)
				{
					handleXmlException(std::current_exception(), "Failed to initialize Xalan.");
				}

				pl::scoped_guard xalanguard{[]{ xalan::XalanTransformer::terminate(); }};
			#endif

			try
			{
				auto tempStr = transcode("LS");
				auto &impl = static_cast<xe::DOMImplementationLS &>(*xe::DOMImplementationRegistry::getDOMImplementation(*tempStr));
				auto &parser = *impl.createLSParser(xe::DOMImplementationLS::MODE_SYNCHRONOUS, 0);
				PRTDOMErrorHandler errorHandler;
				XMLResourceGuard rg{parser};

				{
					auto &domConfig = *parser.getDomConfig();
					domConfig.setParameter(xe::XMLUni::fgDOMValidate, false);
					domConfig.setParameter(xe::XMLUni::fgDOMNamespaces, true);
					//domConfig.setParameter(xe::XMLUni::fgDOMDatatypeNormalization, true);
					domConfig.setParameter(xe::XMLUni::fgXercesDoXInclude, true);
					domConfig.setParameter(xe::XMLUni::fgXercesLoadExternalDTD, true);
					domConfig.setParameter(xe::XMLUni::fgDOMNamespaceDeclarations, true);
					//domConfig.setParameter(xe::XMLUni::fgDOMElementContentWhitespace, true);

					domConfig.setParameter(xe::XMLUni::fgDOMErrorHandler, &errorHandler);
				}

				PL_FWD(fc)(impl, parser, errorHandler);

			}
			catch(...)
			{
				handleXmlException(std::current_exception(), "Failed to parse document.");
			}
		}


		void prepareXercesSerializer(std::invocable<xe::DOMImplementationLS &, xe::DOMLSSerializer &> auto&& fc)
		{
			try
			{
				xe::XMLPlatformUtils::Initialize();
			}
			catch(...)
			{
				std::throw_with_nested( Exception("Failed to initialize Xerces-C.") );
			}

			pl::scoped_guard xercesguard{[]{ xe::XMLPlatformUtils::Terminate(); }};

			try
			{
				auto tempStr = transcode("LS");
				auto &impl = static_cast<xe::DOMImplementationLS &>(*xe::DOMImplementationRegistry::getDOMImplementation(*tempStr));
				auto &serializer = *impl.createLSSerializer();
				XMLResourceGuard rg{serializer};

				PL_FWD(fc)(impl, serializer);

			}
			catch(...)
			{
				handleXmlException(std::current_exception(), "Failed to write document.");
			}
		}

		void buildXercesDocRecursive(const XmlAttrList &xmlAttr, const ParameterTree &params, xe::DOMElement &node)
		{
			const bool isArray = rg::all_of(mir(params.size()), [&](const auto i){ return params.contains(pl::io::to_string_noloc(i)); });

			if(isArray)
				node.setAttribute(xmlAttr.get<XmlAttrArray>(), xmlAttr.get<XmlAttr1>());

			for(const auto &child : params
					| rgv::remove_if([](const auto &p){ return p.second.value.empty() && !p.second.childParams; }))
			{
				const bool hasEol = child.second.value.find_first_of('\n') != std::string::npos;

				if(!node.isSameNode(node.getOwnerDocument()->getDocumentElement()) && !child.second.childParams && !isArray && !hasEol && child.second.value.size() <= 15 && !isReservedAttribute(child.first))
				{
					node.setAttribute(
						transcode(child.first[0] >= '0' && child.first[0] <= '9' ? "_" + child.first : child.first).s,
						transcode(child.second.value).s
					);
				}
				else
				{
					const auto name = std::invoke([&]() -> std::string {
							if(isArray)
								return "e";
							else if(child.first[0] >= '0' && child.first[0] <= '9')
								return "_" + child.first;
							else
								return child.first;
						});

					xe::DOMElement &elem = *node.getOwnerDocument()->createElementNS(xmlAttr.get<XmlAttrNS>(), transcode(name).s);

					if(hasEol)
						elem.setTextContent(transcode(child.second.value).s);
					else
					{
						if(child.second.isEntity())
						{
							const auto [type, id] = child.second.getEntityType();

							elem.setAttribute(xmlAttr.get<XmlAttrType>(), transcode(type).s);

							if(!id.empty())
							{
								if(id[0] == '&')
									elem.setAttribute(xmlAttr.get<XmlAttrLink>(), transcode(id.substr(1)).s);
								else
									elem.setAttribute(xmlAttr.get<XmlAttrId>(), transcode(id).s);
							}
						}
						else if(!child.second.value.empty())
							elem.setAttribute(xmlAttr.get<XmlAttrValue>(), transcode(child.second.value).s);
					}

					if(child.second.childParams)
						buildXercesDocRecursive(xmlAttr, *child.second.childParams, elem);

					node.appendChild(&elem);
				}
			}
		}

	} // namespace

	ParameterTree ParameterParserXerces::load(const std::filesystem::path &filename)
	{
		ParameterTree params;

		prepareXercesParser(
			[&]([[maybe_unused]] auto&& impl, auto&& parser, [[maybe_unused]] auto&& errorHandler)
			{
				xe::DOMDocument *docPtr = nullptr;

				{
				#ifdef PRT_XML_PARSER_XALAN
					xalan::XalanTransformer xalan;
					PRTSAXErrorHandler xalanErrorHandler;

					xalan.setErrorHandler(&xalanErrorHandler);

					bool success = false;
					const auto str = pl::io::stream_into(std::string{}, pl::io::throw_on_failure, [&](auto &os, auto&& str){
							success = xalan.transform(std::to_address(transcode(filename.string()).s), os) == 0;
							return str;
						});

					if(success)
					{
						auto &input = *impl.createLSInput();
						xe::MemBufInputSource inputSource{(const XMLByte *)str.c_str(), str.size(), "root_doc"};

						input.setByteStream(&inputSource);
						docPtr = parser.parse(&input);
					}
					else
				#endif
						docPtr = parser.parseURI(std::to_address(transcode(filename.string()).s));
				}

				if(!docPtr)
					throw ParserException("Failed to load XML document \"" + filename.string() + "\"");

				if(docPtr->getDocumentElement())
				{
					XmlAttrList attrList;

					parseXercesDocRecursive(attrList, params, *docPtr->getDocumentElement());
				}
			}
		);

		return params;
	}

	ParameterTree ParameterParserXerces::load(std::istream &is)
	{
		ParameterTree params;

		prepareXercesParser(
			[&](auto&& impl, auto&& parser, [[maybe_unused]] auto&& errorHandler)
			{
				xe::DOMDocument *docPtr = nullptr;

				{
					StdInputStream xmlis(is);

					auto &input = *impl.createLSInput();
					input.setByteStream(&xmlis);

					docPtr = parser.parse(&input);
				}

				if(!docPtr)
					throw ParserException("Failed to load XML document from stream.");

				if(docPtr->getDocumentElement())
				{
					XmlAttrList attrList;

					parseXercesDocRecursive(attrList, params, *docPtr->getDocumentElement());
				}
			}
		);

		return params;
	}

	ParameterTree ParameterParserXerces::loadFragment(std::string_view s)
	{
		ParameterTree params;

		prepareXercesParser(
			[&](auto&& impl, auto&& parser, [[maybe_unused]] auto&& errorHandler)
			{
				xe::DOMDocument *docPtr = nullptr;

				{
					std::string docstr;

					docstr.reserve(s.size() + 100);
					docstr += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<root xmlns=\"http://photort.org/params/\">";
					docstr += s;
					docstr += "</root>";

					std::istringstream docStream(docstr, std::ios_base::binary | std::ios_base::in);
					StdInputStream xmlis(docStream);

					auto &input = *impl.createLSInput();
					input.setByteStream(&xmlis);

					docPtr = parser.parse(&input);

				}

				if(!docPtr)
					throw ParserException("Failed to load XML document fragment.");

				if(docPtr->getDocumentElement() && docPtr->getDocumentElement()->getFirstElementChild())
				{
					XmlAttrList attrList;

					parseXercesDocRecursive(attrList, params, *docPtr->getDocumentElement()->getFirstElementChild());
				}
			}
		);

		return params;
	}

	void ParameterParserXerces::save(const ParameterTree &params, const std::filesystem::path &filename)
	{
		prepareXercesSerializer(
			[&](auto&& impl, auto&& serializer)
			{
				XmlAttrList attrList;
				xe::DOMDocument &doc = *static_cast<xe::DOMImplementation &>(impl).createDocument(attrList.get<XmlAttrNS>(), attrList.get<XmlAttrScene>(), nullptr);
				const XMLResourceGuard rg{doc};

				buildXercesDocRecursive(attrList, params, *doc.getDocumentElement());

				serializer.writeToURI(&doc, std::to_address(transcode(filename.string()).s));
			}
		);
	}

	void ParameterParserXerces::save(const ParameterTree &params, std::ostream &os)
	{
		prepareXercesSerializer(
			[&](auto&& impl, auto&& serializer)
			{
				XmlAttrList attrList;
				xe::DOMDocument &doc = *static_cast<xe::DOMImplementation &>(impl).createDocument(attrList.get<XmlAttrNS>(), attrList.get<XmlAttrScene>(), nullptr);
				XMLResourceGuard rg{doc};

				buildXercesDocRecursive(attrList, params, *doc.getDocumentElement());

				StdOutputBinStream xmlos(os);
				auto &output = *impl.createLSOutput();
				output.setByteStream(&xmlos);
				serializer.write(&doc, &output);

				os << '\0' << std::flush;
			}
		);
	}

	std::string ParameterParserXerces::save(const ParameterTree &params)
	{
		return pl::io::stream_into(std::string{}, pl::io::throw_on_failure, [&](auto &os, auto&& str){
			os.imbue(std::locale::classic());
			save(params, os);
			return str;
		});
	}
}

#endif
