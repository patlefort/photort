/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Interaction.hpp>
#include <PhotoRT/Core/Instance.hpp>
#include <PhotoRT/Core/BxDF.hpp>
#include <PhotoRT/Core/Emitter.hpp>
#include <PhotoRT/Core/Medium.hpp>
#include <PhotoRT/Core/Edf.hpp>

namespace PhotoRT
{
	bool SurfaceInteraction::shouldPassthrough() const noexcept
	{
		return (mSurface.inst && mSurface.inst->getObject()->isLightPortal()) ||
			(mSurface.matEntry && (
				(mSurface.matEntry->material->isSubstance() && nextSubstance == enteredSub) ||
				((mSurface.matEntry->material->mLayers.empty() || !mSurface.matEntry->material->mLayers.front().getBxDF()) && (mSurface.matEntry->material->isTransparent() || mSurface.matEntry->material->isMedium()))
			));
	}

	bool SurfaceInteraction::isDelta() const noexcept
	{
		if(const auto bxdf = getBxDF(); bxdf)
			return bxdf->isDiracDelta();

		return true;
	}

	OptionalPDF<Real> SurfaceInteraction::PDF(const EnumTransportMode mode, const VecD odir, const VecD idir) const noexcept
	{
		const auto bxdf = getBxDF();
		if(!bxdf)
			return pl::nullopt;

		return bxdf->PDF(mode, *this, odir, idir)
			.transform([&](const auto pdf)
			{
				return getMaterial().isTransparent() ?
						pdf * alphaProbability(bxdf->alpha(mode, *this, odir, idir))
					:
						pdf;
			});
	}

	Color SurfaceInteraction::emission(const VecD idir) const noexcept
	{
		const auto emission = getEmission();
		return emission ? emission->radiance(*this, idir) : Black;
	}

	OptionalPDF<Real> SurfaceInteraction::emissionPDF(const VecD idir) const noexcept
	{
		const auto emission = getEmission();
		if(!emission)
			return pl::nullopt;

		return emission->PDF(*this, idir);
	}

	RadianceResult SurfaceInteraction::radiance(const EnumTransportMode mode, const VecD odir, const VecD idir) const noexcept
	{
		return radiance(mode, odir, idir, mLobe.getHemisphere(idir));
	}

	RadianceResult SurfaceInteraction::radiance(const EnumTransportMode mode, const VecD odir, const VecD idir, const EnumHemisphere idirHemisphere) const noexcept
	{
		const auto bxdf = getBxDF();
		if(!bxdf)
			return {};

		auto [color, albedo] = bxdf->radiance(mode, *this, odir, idir);
		const auto lalpha = getMaterial().getLayerAlpha(mSurface.layerIndex, EnumTransportMode::Radiance, *this, odir, idir, idirHemisphere);
		const auto alpha = color.alpha * (White - lalpha.alpha);
		color.color *= alpha;
		albedo *= getColorAlbedo(alpha);

		return {.color = color.color, .albedo = albedo, .laPdf = lalpha.pdf};
	}

	bool MediumInteraction::isDelta() const noexcept
		{ return getMedium().isDiracDelta(); }

	OptionalPDF<Real> MediumInteraction::PDF(const EnumTransportMode, const VecD odir, const VecD idir) const noexcept
		{ return getMedium().dirPDF(*this, odir, idir); }

	RadianceResult MediumInteraction::radiance(const EnumTransportMode, const VecD odir, const VecD idir) const noexcept
		{ return {.color = getMedium().dirPDF(*this, odir, idir).value(), .albedo = 1, .laPdf = 0}; }

	RadianceResult MediumInteraction::radiance(const EnumTransportMode, const VecD odir, const VecD idir, const EnumHemisphere) const noexcept
		{ return {.color = getMedium().dirPDF(*this, odir, idir).value(), .albedo = 1, .laPdf = 0}; }


	InteractionReverseGuard<SurfaceInteraction>::InteractionReverseGuard(SurfaceInteraction &si, VecD dir) noexcept :
		interaction{si}, rg{si.surface(), si.lobe()}
	{
		if(dir /odot/ si.lobe().normal < 0)
		{
			swapSubs = si.nextSubstance->isSubstance();

			if(swapSubs)
				pl::adl_swap(si.enteredSub, si.nextSubstance);

			rg.reverse();
		}
	}


	SurfaceInteraction sampledAreaToShadedInteraction(
		const SampledAreaResult<VecP> &areaSp,
		const IPrimitiveGroupProperties &props,
		const IObjectInstance &inst,
		const Matrix &worldMatrix,
		const VecP worldPos,
		const Real dist,
		const PrimitiveIndex primIndex,
		const VecD odir,
		bool backface,
		pl::marked_optional<const PrimitiveMaterial &> matEntry) noexcept
	{
		return shadedInteraction(
				{
					.shadingNormal = areaSp.shadingNormal,
					.surfaceNormal = areaSp.normal,
					.uv = areaSp.uv,
					.worldPos = worldPos,
					.localPos = areaSp.pos,
					.worldMatrix = worldMatrix,
					.inst = &inst,
					.objProps = &props,
					.matEntry = matEntry.to_pointer(),
					.primIndex = primIndex,
					.flags{std::make_pair(SurfaceData::EnumFlags::BackfaceHit, backface)}
				},
				dist,
				odir,
				true
			);
	}

	SurfaceInteraction sampledAreaToShadedInteraction(
		const SampledAreaResult<VecP> &areaSp,
		const IPrimitiveGroupProperties &props,
		const IObjectInstance &inst,
		const Matrix &worldMatrix,
		const VecP worldPos,
		const Real dist,
		const PrimitiveIndex primIndex,
		const VecD odir,
		bool backface) noexcept
	{
		return sampledAreaToShadedInteraction(areaSp, props, inst, worldMatrix, worldPos, dist, primIndex, odir, backface, props.getMaterial(primIndex));
	}

	SurfaceInteraction collisionToShadedInteraction(
		const PrimGroupCollision &collision,
		const IPrimitiveGroupProperties &props,
		const IObjectInstance &inst,
		const Matrix &worldMatrix,
		const VecD surfaceNormal,
		const VecD shadingNormal,
		const VecP localPt,
		const VecD odir,
		pl::marked_optional<const PrimitiveMaterial &> matEntry) noexcept
	{
		const auto colPt = localPt + odir * collision.dist;
		const auto worldPos = worldMatrix * colPt;

		return shadedInteraction(
				{
					.shadingNormal = shadingNormal,
					.surfaceNormal = surfaceNormal,
					.uv = collision.uv,
					.worldPos = worldPos,
					.localPos = colPt,
					.worldMatrix = worldMatrix,
					.inst = &inst,
					.objProps = &props,
					.matEntry = matEntry.to_pointer(),
					.primIndex = collision.primIndex,
					.flags{std::make_pair(SurfaceData::EnumFlags::BackfaceHit, collision.backface)}
				},
				collision.dist,
				odir,
				true
			);
	}

	SurfaceInteraction collisionToShadedInteraction(
		const PrimGroupCollision &collision,
		const IPrimitiveGroupProperties &props,
		const IObjectInstance &inst,
		const Matrix &worldMatrix,
		const VecD surfaceNormal,
		const VecD shadingNormal,
		const VecP localPt,
		const VecD odir) noexcept
	{
		return collisionToShadedInteraction(
				collision, props, inst, worldMatrix, surfaceNormal, shadingNormal, localPt, odir, props.getMaterial(collision.primIndex)
			);
	}

	SurfaceInteraction collisionToShadedInteraction(const CollisionData &cd, VecD odir, bool transformToWorld) noexcept
	{
		return shadedInteraction(cd.surface, cd.distSqr /oprtsqrt, odir, transformToWorld);
	}

	SurfaceInteraction shadedInteraction(const SurfaceData &sd, const Real dist, VecD odir, bool transformToWorld) noexcept
	{
		SurfaceInteraction inter{sd, dist};

		auto &surface = inter.surface();
		const auto &obj = *surface.inst->getObject();

		surface.matEntry = surface.objProps ? surface.objProps->getMaterial(surface.primIndex).to_pointer() : nullptr;
		surface.surfaceNormal = obj.getSurfaceNormal(surface.localPos, surface.primIndex);

		if(surface.matEntry)
			surface.layerIndex = !surface.matEntry->material->mLayers.empty() ? surface.matEntry->material->getFrontLayerIndex(surface.isBackfaceHit()) : -1;

		obj.getShadingInfo(surface, surface.primIndex);

		if(surface.isBackfaceHit())
		{
			surface.surfaceNormal = -surface.surfaceNormal;
			surface.shadingNormal = -surface.shadingNormal;
		}

		if(transformToWorld)
		{
			surface.pMat *= surface.worldMatrix;
			surface.surfaceNormal = dirSafeTransform(surface.surfaceNormal, surface.worldMatrix);
			surface.shadingNormal = dirSafeTransform(surface.shadingNormal, surface.worldMatrix);
		}

		if(surface.matEntry)
			if(const auto normalMapper = std::to_address(surface.matEntry->material->getNormalMapper()); normalMapper)
				surface.shadingNormal = normalMapper->map(inter, odir);

		if(odir /odot/ surface.shadingNormal > 0)
			surface.shadingNormal = surface.surfaceNormal;

		inter.lobe() = surface.getShadingLobe();

		return inter;
	}
}
