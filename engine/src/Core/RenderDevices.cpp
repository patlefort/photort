/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/RenderDevices.hpp>
#include <PhotoRT/Core/Film.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

namespace PhotoRT
{
	void RenderDeviceController::init(Scene &scene)
	{
		RenderDeviceMulti::init(scene);
	}

	void RenderDeviceController::initTargets()
	{
		mRenderContext->deleteBuffers();

		for(auto &device : mThreads)
			for(auto &contribBuffer : device->getContributionBuffers())
				mRenderContext->registerBuffer(contribBuffer);
	}


	bool ClientDevice::handleCommand(std::string_view command)
	{
		if(auto it = mCommandHandlers.find(command); it != mCommandHandlers.end())
		{
			it->second(command);
			return true;
		}

		return false;
	}

}