/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/PrimitiveGroup.hpp>
#include <PhotoRT/Core/Scene.hpp>
#include <PhotoRT/Core/Distribution.hpp>
#include <PhotoRT/Core/Material.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/Normals.hpp>
#include <PhotoRT/Core/Instance.hpp>

namespace PhotoRT
{
	// Primitive group properties
	PRT_ENTITY_FACTORY_DEFINE(PrimitiveGroupProperties)


	void PrimitiveGroupProperties::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		params.getValueIfExistsBind
			("class",                     PL_LAMBDA_FORWARD_THIS(setClassName))
			("emitter",                   PL_LAMBDA_FORWARD_THIS(setEmitter))
			("emitter_group",             PL_LAMBDA_FORWARD_THIS(setEmitterGroup))
			("light_portal",              PL_LAMBDA_FORWARD_THIS(setLightPortal))
		;

		mMaterials.clear();
		mMaterials.shrink_to_fit();

		if(const auto p = params.findParam("material"); p && p->childParams)
		{
			std::string matName;

			for(const auto &mat : *p->childParams)
			{
				PrimitiveIndex start = 0;
				matName.clear();

				pl::io::stream_from(mat.second.value, pl::io::throw_on_failure, [&](auto &is, auto&&){
					is >> start >> matName;
				});

				if(matName.empty())
					continue;

				auto entity = addReference(type_v<Material>, "material", "&" + matName);

				if(!entity)
					continue;

				setMaterial(std::move(entity), start);
			}
		}

	}

	ParameterTree PrimitiveGroupProperties::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params.insert({
			{"class",                    mClassName.first},
			{"emitter",                  mEmitter},
			{"emitter_group",            mEmitterGroup},
			{"light_portal",             mLightPortal}
		});

		auto &matParam = params["material"];
		for(auto&& [i, mat] : rgv::enumerate(mMaterials))
			matParam[i].value = pl::strconcat(std::string{}, pl::io::to_string_noloc(mat.first), ' ', mat.second.material->getEntityId());

		return params;
	}

	pl::marked_optional<const PrimitiveMaterial &> PrimitiveGroupProperties::getMaterial(PrimitiveIndex primitiveIndex) const noexcept
	{
		if(mMaterials.empty())
			return pl::nullopt;

		const auto it = rg::upper_bound(mMaterials, primitiveIndex, rg::less{}, [&](const auto &e){ return e.first; });

		if(it == mMaterials.cbegin())
			return pl::nullopt;

		return {(it - 1)->second};
	}

	void PrimitiveGroupProperties::setMaterial(EntityPtr<const Material> mat, PrimitiveIndex start)
	{
		if(!mat)
			return;

		mMaterials[start] = {
			.material = std::move(mat)
		};
	}

	void PrimitiveGroupProperties::setMaterial(EntityPtr<const Material> mat)
	{
		mMaterials.clear();

		if(mat)
		{
			mMaterials[0] = {
				.material = std::move(mat)
			};
		}
	}
}
