/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>

namespace PhotoRT
{
	namespace memtag
	{
		PRT_ALLOC_STATS_DEF(Texture)
		PRT_ALLOC_STATS_DEF(ContBuffer)
		PRT_ALLOC_STATS_DEF(Film)
		PRT_ALLOC_STATS_DEF(Primitive)
		PRT_ALLOC_STATS_DEF(AccelStruct)
		PRT_ALLOC_STATS_DEF(Entity)
		PRT_ALLOC_STATS_DEF(Other)
	} // namespace memtag

	bool &getMemoryOwnership()
	{
		static bool v = true; return v;
	}

	template <>
	std::shared_ptr<pl::IMemorySystem<VoidOffsetPtr>> &getMemorySystem<void>()
	{
		static std::shared_ptr<pl::IMemorySystem<VoidOffsetPtr>> ms;
		return ms;
	}

	template <>
	std::shared_ptr<pl::IMemorySystem<VoidOffsetPtr>> &getMemorySystem<memtag::Primitive>()
	{
		static std::shared_ptr<pl::IMemorySystem<VoidOffsetPtr>> ms;
		return ms;
	}

	template <>
	std::shared_ptr<pl::IMemorySystem<VoidOffsetPtr>> &getMemorySystem<memtag::Texture>()
	{
		static std::shared_ptr<pl::IMemorySystem<VoidOffsetPtr>> ms;
		return ms;
	}

	template <>
	std::shared_ptr<pl::IMemorySystem<VoidOffsetPtr>> &getMemorySystem<memtag::Film>()
	{
		static std::shared_ptr<pl::IMemorySystem<VoidOffsetPtr>> ms;
		return ms;
	}

	template <>
	std::shared_ptr<pl::IMemorySystem<VoidOffsetPtr>> &getMemorySystem<memtag::ContBuffer>()
	{
		static std::shared_ptr<pl::IMemorySystem<VoidOffsetPtr>> ms;
		return ms;
	}
}
