/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/RenderEntityInterface.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/RenderEntityImportExport.hpp>

namespace PhotoRT
{
	Factories &impl::factories()
	{
		static Factories factories;
		return factories;
	}

	void Factories::registerFactory(std::string type, IRenderEntityFactory &factory)
	{
		mFactories.emplace(std::move(type), &factory);

		#ifndef NDEBUG
			//std::cout << "Factory registered: " << type << ": " << boost::typeindex::type_id_runtime(*factory).pretty_name() << '\n';
		#endif
	}

	void Factories::registerAlias(std::string alias, std::string_view aliased)
	{
		auto it = mFactories.find(aliased);
		if(it == mFactories.end())
			throw ExceptionEntityCollection("Factory type \"" + aliased + "\" not registered.");

		mFactories.emplace(std::move(alias), it->second);
	}

	EntityFactoryMap Factories::getTypeFactories(std::string_view type)
	{
		EntityFactoryMap types;

		for(auto &f : mFactories)
			if(f.first.starts_with(type))
				types[f.first] = f.second;

		return types;
	}

	void Factories::dumpFactories(std::ostream &os)
	{
		os << mFactories.size() << " factories registered.\n";

		for(auto&& [type, factory] : mFactories)
			os << type << ": " << boost::typeindex::type_id_runtime(*factory).pretty_name() << '\n';
	}

	bool Factories::factoryExists(std::string_view type) const
	{
		return mFactories.find(type) != mFactories.end();
	}


	[[nodiscard]] std::string getMimeTypes(const auto &ieMap, const std::filesystem::path &filename)
	{
		std::string ext = filename.extension().string();
		if(ext.empty())
			return {};

		// Remove dot
		std::move(ext.cbegin() + 1, ext.cend(), ext.begin());
		ext.resize(ext.size() - 1);

		ext | pl::transform(PL_LAMBDA_FORWARD(std::tolower));

		if(auto it = ieMap.left.find(ext); it != ieMap.left.end())
			return it->second;

		return {};
	}

	std::string Factories::getMimeTypeImport(const std::filesystem::path &filename)
	{
		return getMimeTypes(mMimeTypesImport, filename);
	}

	std::string Factories::getMimeTypeExport(const std::filesystem::path &filename)
	{
		return getMimeTypes(mMimeTypesExport, filename);
	}

	std::string Factories::getMimeExtImport(std::string_view mimeType)
	{
		if(auto it = mMimeTypesImport.right.find(std::string(mimeType)); it != mMimeTypesImport.right.end())
			return it->second;

		return {};
	}

	std::string Factories::getMimeExtExport(std::string_view mimeType)
	{
		if(auto it = mMimeTypesExport.right.find(std::string(mimeType)); it != mMimeTypesExport.right.end())
			return it->second;

		return {};
	}

	void Factories::registerImportExporter(std::string name, IEntityImportExport &importExport)
	{
		mEntityImportExport.emplace(std::move(name), &importExport);

		const auto ieMimeTypesImport = importExport.getMimeTypesImport();
		const auto ieMimeTypesExport = importExport.getMimeTypesExport();
		const auto ieGenericTypes = importExport.getGenericTypes();

		for(const auto &t : ieGenericTypes)
			mGenericTypesHandlers.left.insert({t, &importExport});

		for(const auto &k : ieMimeTypesImport.right | rgv::keys)
			mMimeTypeHandlersImport[k] = &importExport;
		for(const auto &k : ieMimeTypesExport.right | rgv::keys)
			mMimeTypeHandlersExport[k] = &importExport;

		mMimeTypesImport.left.insert(ieMimeTypesImport.left.begin(), ieMimeTypesImport.left.end());
		mMimeTypesExport.left.insert(ieMimeTypesExport.left.begin(), ieMimeTypesExport.left.end());

		#ifndef NDEBUG
			//std::cout << "Import/exporter registered: " << boost::typeindex::type_id_runtime(importExport).pretty_name() << '\n';
		#endif
	}

	IEntityImportExport *Factories::getEntityImporter(std::string_view mimeType, std::string_view expectedType, bool streamable)
	{
		if(auto it = mMimeTypeHandlersImport.find(mimeType);
			it != mMimeTypeHandlersImport.end() && (expectedType.empty() || expectedType == it->second->getEntityType(mimeType)) && (!streamable || it->second->isStreamable(mimeType))
		)
			return it->second;

		return nullptr;
	}

	IEntityImportExport *Factories::getEntityExporter(std::string_view mimeType, std::string_view expectedType, bool streamable)
	{
		if(auto it = mMimeTypeHandlersExport.find(mimeType);
			it != mMimeTypeHandlersExport.end() && (expectedType.empty() || expectedType == it->second->getEntityType(mimeType)) && (!streamable || it->second->isStreamable(mimeType))
		)
			return it->second;

		return nullptr;
	}

	void Factories::dumpImporterExporter(std::ostream &os)
	{
		os << mEntityImportExport.size() << " importers/exporters:\n";
		for(const auto &f : mEntityImportExport)
			os << "  " << std::setfill(' ') << std::left << std::setw(25) << f.first << " -> " << boost::typeindex::type_id_runtime(*f.second).pretty_name() << '\n';

		/*os << mMimeTypes.size() << " mime-types:\n";
		for(auto&& [ext, mime] : mMimeTypes.left | pl::pair_view)
			os << "  " << std::setfill(' ') << std::left << std::setw(25) << ext << " -> " << mime << '\n';*/

		os << mMimeTypeHandlersImport.size() << " mime-type import handlers:\n";
		for(auto&& [mime, handler] : mMimeTypeHandlersImport)
			os << "  " << std::setfill(' ') << std::left << std::setw(25) << mime << " -> " << boost::typeindex::type_id_runtime(*handler).pretty_name() << '\n';

		os << mMimeTypeHandlersExport.size() << " mime-type export handlers:\n";
		for(auto&& [mime, handler] : mMimeTypeHandlersExport)
			os << "  " << std::setfill(' ') << std::left << std::setw(25) << mime << " -> " << boost::typeindex::type_id_runtime(*handler).pretty_name() << '\n';
	}

	EntityPtrBase Factories::createEntity(std::string_view type)
	{
		EntityPtrBase object;

		try
		{
			auto it = mFactories.find(type);
			if(it == mFactories.end())
				throw ExceptionEntityCollection("Factory type \"" + type + "\" not registered.");

			auto &factory = *it->second;

			object = factory.create_shared();
		}
		catch(...)
		{
			std::throw_with_nested( ExceptionEntityCollection("Entity creation failed for type \"" + type + "\"" ) );
		}

		if(!object)
			throw ExceptionEntityCollection("Entity creation failed for type \"" + type + "\".");

		return object;
	}

	EntityPtrBase EntityFactoriesProxy::createEntity(std::string_view id, std::string_view type)
	{
		auto factory = mFactorySearch.find(type);
		if(!factory)
			throw ExceptionEntityCollection("Factory type \"" + type + "\" not registered.");

		auto object = factory->create_shared();

		if(!object)
			throw ExceptionEntityCollection("Entity creation failed for id \"" + id + "\" and type \"" + type + "\".");

		return object;
	}

	EntityPtrBase EntityFactoriesProxyMemoryResource::createEntity(std::string_view id, std::string_view type)
	{
		auto factory = mFactorySearch.find(type);
		if(!factory)
			throw ExceptionEntityCollection("Factory type \"" + type + "\" not registered.");

		EntityPtrBase object;

		try
		{
			const auto storageInfo = factory->storage_info();

			auto ptr = mMemRes->allocate(storageInfo.size, storageInfo.align);

			try
			{
				factory->create(ptr);
				object = EntityPtrBase( static_cast<IRenderEntity *>(ptr), [factory, this](IRenderEntity *p){ factory->destroy(p); const auto s = factory->storage_info(); mMemRes->deallocate(p, s.size, s.align); } );
			}
			catch(...)
			{
				mMemRes->deallocate(ptr, storageInfo.size, storageInfo.align);
				throw;
			}
		}
		catch(...)
		{
			std::throw_with_nested( ExceptionEntityCollection("Entity creation failed for id \"" + id + "\" and type \"" + type + "\"" ) );
		}

		if(!object)
			throw ExceptionEntityCollection("Entity creation failed for type \"" + type + "\".");

		return object;
	}

	EntityFactoryAlias::EntityFactoryAlias(std::string_view alias, std::string_view aliased)
	{
		factories.registerAlias(std::string{alias}, aliased);
	}
}
