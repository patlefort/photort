/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Camera.hpp>
#include <PhotoRT/Core/RayTracer.hpp>
#include <PhotoRT/Core/Material.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

namespace PhotoRT
{
	void CameraBase::update()
	{
	}

	void CameraBase::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

	}

	ParameterTree CameraBase::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		return params;
	}

	// Return origin / direction / image point
	std::tuple<VecP, VecD, VecP> CameraBase::projectPoint(const VecP &point) const noexcept
	{
		// Differentiate between the near and far planes to find the direction and origin of the ray pointing at point
		const auto screenPoint = mMatrix.cameraToScreen * pl::normalize_point_p{point};

		auto cameraPointNear = screenPoint;
		cameraPointNear.set(0, size_v<2>);
		auto cameraPointFar = screenPoint;
		cameraPointFar.set(-1, size_v<2>);

		cameraPointNear = mMatrix.screenToCamera * pl::normalize_point_p{cameraPointNear};
		cameraPointFar = mMatrix.screenToCamera * pl::normalize_point_p{cameraPointFar};

		const auto dir = pl::assert_valid( static_cast<VecD>(cameraPointFar - cameraPointNear) /oprtnormalize );

		return {
			pl::assert_valid( VecP{cameraPointNear[0] - dir[0] * cameraPointNear[2], cameraPointNear[1] - dir[1] * cameraPointNear[2], 0, 1} ),
			dir,
			pl::assert_valid(screenPoint)
		};
	}

	// Return origin / direction
	std::pair<VecP, VecD> CameraBase::rasterToRay(const Point2Dr &pos) const noexcept
	{
		// Differentiate between the near and far planes to find the direction and origin of the ray
		const auto rasterPoint = static_cast<VecP>(pos);

		auto cameraPointNear = mMatrix.rasterToScreen * pl::normalize_point_p{rasterPoint};
		cameraPointNear.set(0, size_v<2>);
		auto cameraPointFar = cameraPointNear;
		cameraPointFar.set(-1, size_v<2>);

		cameraPointNear = mMatrix.screenToCamera * pl::normalize_point_p{cameraPointNear};
		cameraPointFar = mMatrix.screenToCamera * pl::normalize_point_p{cameraPointFar};

		const auto dir = pl::assert_valid( static_cast<VecD>(cameraPointFar - cameraPointNear) /oprtnormalize );

		return {
			pl::assert_valid( VecP{cameraPointNear[0] - dir[0] * cameraPointNear[2], cameraPointNear[1] - dir[1] * cameraPointNear[2], 0, 1} ),
			dir
		};
	}


	void CameraControllerBase::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		setTransform(addParamReference(type_v<ITransform>, params, "transform"));

		params.getValueIfExists
			("lookat", mLookAt)
			("up", mUp)
		;

		params.getValueIfExistsBind
			("focusdist", PL_LAMBDA_FORWARD_THIS(setFocus))
			("aspectratio", PL_LAMBDA_FORWARD_THIS(setAspectRatio))
		;
	}

	ParameterTree CameraControllerBase::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		if(mTransform)
			params["transform"].fromEntityRef(*mTransform, deep);

		params["lookat"] = mLookAt;
		params["up"] = mUp;

		params["focusdist"] = getFocus();
		params["aspectratio"] = getAspectRatio();

		return params;
	}

	void CameraControllerBase::update()
	{
		if(mTransform)
		{
			const auto matrix = (*mTransform)();

			mwPosition = matrix * VecP{pl::tag::identity};
			mwUp = matrix * mUp;
			mwLookAt = matrix * mLookAt;
		}
		else
		{
			mwPosition.set_identity();
			mwUp = mUp;
			mwLookAt = mLookAt;
		}
	}
}
