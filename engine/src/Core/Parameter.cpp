/*
	PhotoRT

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>

#include <PhotoRT/Core/RenderEntityInterface.hpp>
#include <PhotoRT/Core/Parameter.hpp>

namespace PhotoRT
{
	std::locale Parameter::loc = std::locale::classic();

	Parameter::Parameter(const Parameter &other)
	{
		*this = other;
	}

	Parameter &Parameter::operator= (const Parameter &other)
	{
		value = other.value;
		if(other.childParams)
			setChildParams(*other.childParams);

		return *this;
	}

	Parameter &Parameter::operator= (const ParameterTree &other)
	{
		childParams = other;

		return *this;
	}

	Parameter::Parameter(Parameter &&other) noexcept = default;
	Parameter &Parameter::operator= (Parameter &&other) noexcept = default;

	Parameter::Parameter(const ParameterTree &other)
	{
		childParams = other;
	}

	Parameter::Parameter(ParameterTree &&other)
	{
		childParams = std::move(other);
	}

	Parameter &Parameter::operator= (ParameterTree &&other)
	{
		childParams = std::move(other);

		return *this;
	}

	Parameter::Parameter(std::string value_, ParameterTree params)
	{
		value = std::move(value_);

		setChildParams(std::move(params));
	}

	Parameter::Parameter(std::string_view entityType, std::string_view entityId, ParameterTree params)
	{
		toEntity(entityType, entityId, false);
		setChildParams(std::move(params));
	}

	pl::marked_optional<const Parameter &> Parameter::findParam(std::string_view key) const
	{
		return childParams.and_then([&](const auto &cp){ return cp.findParam(key); });
	}

	pl::marked_optional<Parameter &> Parameter::findParam(std::string_view key)
	{
		return childParams.and_then([&](auto &cp){ return cp.findParam(key); });
	}

	bool Parameter::contains(std::string_view key) const
		{ return childParams && childParams->contains(key); }

	// Treat char and u8 as numbers and not characters
	template <>
	u8 Parameter::to<u8>(type_c<u8>) const
	{
		return to<uf32>();
	}

	template <>
	char Parameter::to<char>(type_c<char>) const
	{
		return to<if32>();
	}

	template <>
	std::string Parameter::to<std::string>(type_c<std::string>) const
	{
		return value;
	}

	template <>
	std::string_view Parameter::to<std::string_view>(type_c<std::string_view>) const
	{
		return value;
	}


	void Parameter::from(Parameter val)
	{
		*this = std::move(val);
	}

	void Parameter::from(ParameterTree val)
	{
		childParams = std::move(val);
	}

	void Parameter::from(const char *val)
	{
		value = val;
	}

	void Parameter::from(u8 val)
	{
		from((uf32)val);
	}

	void Parameter::from(char val)
	{
		from((if32)val);
	}

	void Parameter::from(std::string val) noexcept
	{
		value = std::move(val);
	}

	/*void Parameter::from(std::string_view val)
	{
		value = val;
	}*/

	void Parameter::hasChilds(bool hasChilds)
	{
		if(hasChilds)
		{
			if(!childParams)
				childParams = ParameterTree{};
		}
		else
			childParams.reset();
	}

	ParameterTree Parameter::getChildParamsCopy() const
		{ return childParams ? *childParams : ParameterTree{}; }

	ParameterTree Parameter::getChildParamsMove()
		{ return childParams ? std::move(*childParams) : ParameterTree{}; }

	bool Parameter::isEntity() const
	{
		return !value.empty() && value[0] == '$';
	}

	bool Parameter::isEntityLink() const
	{
		return isEntity() && value.find_first_of("/&") != std::string::npos;
	}

	void Parameter::getEntityType(std::string &type, std::string &id) const
	{
		auto pos = value.cbegin() + value.find('/');

		type.clear();
		type.append(value.cbegin() + 1, pos);

		id.clear();
		id.append(pos + 1, value.cend());
	}

	std::pair<std::string, std::string> Parameter::getEntityType() const noexcept
	{
		std::pair<std::string, std::string> p;
		getEntityType(p.first, p.second);
		return p;
	}

	void Parameter::fromEntityRef(const IRenderEntity &entity, bool deep)
	{
		if(!deep && !entity.getEntityId().empty())
		{
			toEntity(entity.getEntityType(), entity.getEntityId(), true);
		}
		else
		{
			toEntity(entity.getEntityType(), entity.getEntityId(), false);
			setChildParams(entity.getParameters(deep));
		}
	}

	void Parameter::toEntity(std::string_view type, std::string_view id, bool ref)
	{
		value.clear();

		if(ref)
			pl::strconcat(value, '$', type, "/&", id);
		else
			pl::strconcat(value, '$', type, '/', id);
	}

	void Parameter::setChildParams(ParameterTree params)
	{
		childParams = std::move(params);
	}


	[[nodiscard]] const Parameter *ParameterTree::findEntityRef(std::string_view id) const
	{
		const auto entityRefStr = pl::strconcat(std::string{}, '/', id);

		for(auto &param : mProps)
		{
			if(param.second.isEntity() && param.second.value.ends_with(entityRefStr))
				return &param.second;

			if(param.second.childParams)
			{
				if(auto res = param.second.childParams->findEntityRef(id); res)
					return res;
			}
		}

		return nullptr;
	}

	void ParameterTree::print(std::ostream &os) const
	{
		traverse([&](auto&& e, if32 level){
			os << pl::io::indent(level) << '"' << e.first << "\": \"" << e.second.value << "\"\n";
		});
	}
}
