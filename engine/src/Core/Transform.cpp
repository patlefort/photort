/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Transform.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedTransformIdentity)


	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedTransformMulti)
	void AugmentedTransformMulti::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		mTransforms.clear();

		if(const auto p = params.findParam("transforms"); p && p->childParams)
		{
			rgv::enumerate(*p->childParams)
				| rgv::transform([](auto&& p){
					return std::make_tuple(p.first, std::ref(p.second.first), p.second.second.findParam("t"), p.second.second.findParam("o"));
				})
				| rgv::remove_if([](auto&& p){ return !std::get<2>(p); })
				| pl::for_each(pl::applier([&](auto&& i, auto&& name, auto&& pt, auto&& po){
					mTransforms.insert({
						po ? (int)*po : (int)i,
						name,
						addReference(type_v<ITransform>, *pt)
					});
				}));
		}
	}

	ParameterTree AugmentedTransformMulti::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		for(auto &transforms = params["transforms"]; auto &item : mTransforms.get<ByOrder>())
		{
			transforms[item.name]["o"] = item.order;
			transforms[item.name]["t"].fromEntityRef<ITransform>(*item.transform, deep);
		}

		return params;
	}


	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedTransformMatrix)
	void AugmentedTransformMatrix::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		matrix.set_identity();

		params.getValueIfExists
			("v0", matrix.v[0])
			("v1", matrix.v[1])
			("v2", matrix.v[2])
			("v3", matrix.v[3])
		;
	}

	ParameterTree AugmentedTransformMatrix::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		if(!matrix.is_identity())
		{
			params.insert({
				{"v0", matrix.v[0]},
				{"v1", matrix.v[1]},
				{"v2", matrix.v[2]},
				{"v3", matrix.v[3]}
			});
		}

		return params;
	}


	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedTransformRotation)
	void AugmentedTransformRotation::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		auto a = params.getValueOrDefault<Point3Dr>("angle", 0);

		for(auto &v : a)
			v = pl::degree_to_radian(v);

		setAngle(a);
	}

	ParameterTree AugmentedTransformRotation::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params["angle"] = pl::construct_from_index_sequence<Point3Dr>(size_v<Point3Dr::NbValues>, [&](const auto i){ return pl::radian_to_degree(mAngle[i]); });

		return params;
	}


	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedTransformTranslation)
	void AugmentedTransformTranslation::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		params.getValueIfExistsBind("position", PL_LAMBDA_FORWARD_THIS(setPosition));
	}

	ParameterTree AugmentedTransformTranslation::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params["position"] = mPosition;

		return params;
	}


	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedTransformScale)
	void AugmentedTransformScale::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		params.getValueIfExistsBind("scale", PL_LAMBDA_FORWARD_THIS(setScale));
	}

	ParameterTree AugmentedTransformScale::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params["scale"] = mScale;

		return params;
	}


	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedTransformCombined)
	void AugmentedTransformCombined::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		params.getValueIfExistsBind("scale", PL_LAMBDA_FORWARD_THIS(setScale));

		auto a = params.getValueOrDefault<Point3Dr>("angle", 0);

		for(auto &v : a)
			v = pl::degree_to_radian(v);

		setAngle(a);

		params.getValueIfExistsBind
			("position", PL_LAMBDA_FORWARD_THIS(setPosition))
			("pivot", PL_LAMBDA_FORWARD_THIS(setPivot))
		;
	}

	ParameterTree AugmentedTransformCombined::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params.insert({
			{"scale", mScale.getScale()},
			{"angle", pl::construct_from_index_sequence<Point3Dr>(size_v<Point3Dr::NbValues>, [&](const auto i){ return pl::radian_to_degree(getAngle()[i]); })},
			{"position", mTrans.getPosition()},
			{"pivot", mPivot.getPosition()}
		});

		return params;
	}


	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedTransformPerspective)
	void AugmentedTransformPerspective::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		setFoV(pl::degree_to_radian(params.getValueOrDefault<Real>("fov", 40)));
		params.getValueIfExistsBind
			("near", PL_LAMBDA_FORWARD_THIS(setNear))
			("far", PL_LAMBDA_FORWARD_THIS(setFar))
		;
	}

	ParameterTree AugmentedTransformPerspective::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params.insert({
			{"fov", pl::radian_to_degree(mFov)},
			{"near", mNearp},
			{"far", mFarp}
		});

		return params;
	}


	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedTransformOrthographic)
	void AugmentedTransformOrthographic::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		params.getValueIfExistsBind
			("fov", PL_LAMBDA_FORWARD_THIS(setFoV))
			("near", PL_LAMBDA_FORWARD_THIS(setNear))
			("far", PL_LAMBDA_FORWARD_THIS(setFar))
		;
	}

	ParameterTree AugmentedTransformOrthographic::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params.insert({
			{"fov", mFov},
			{"near", mNearp},
			{"far", mFarp}
		});

		return params;
	}



	Matrix TransformMulti::getMatrix() const noexcept
	{
		Matrix matrix{pl::tag::identity};

		for(auto &item : mTransforms.get<ByOrder>())
			matrix *= (*item.transform)();

		return matrix;
	}

	Matrix TransformMulti::inverse() const
	{
		Matrix matrix{pl::tag::identity};

		for(auto &item : mTransforms.get<ByOrder>() | rgv::reverse)
			matrix *= item.transform->inverse();

		return matrix;
	}


	void TransformRotation::setAngleX(Real ax) noexcept
	{
		mAngle.set(ax, size_v<0>);
	}

	void TransformRotation::setAngleY(Real ay) noexcept
	{
		mAngle.set(ay, size_v<1>);
	}

	void TransformRotation::setAngleZ(Real az) noexcept
	{
		mAngle.set(az, size_v<2>);
	}

	void TransformRotation::setAngle(Real ax, Real ay, Real az) noexcept
	{
		mAngle.set_all(ax, ay, az);
	}

	void TransformRotation::setAngle(Point3Dr a) noexcept
	{
		mAngle = a;
	}

	void TransformRotation::rotate(Dir3Dr rot) noexcept
	{
		mAngle += rot;
	}

	void TransformRotation::rotate(Real ax, Real ay, Real az) noexcept
	{
		mAngle += Dir3Dr{ax, ay, az};
	}

	void TransformRotation::rotatex(Real ax) noexcept
	{
		mAngle[0] += ax;
	}

	void TransformRotation::rotatey(Real ay) noexcept
	{
		mAngle[1] += ay;
	}

	void TransformRotation::rotatez(Real az) noexcept
	{
		mAngle[2] += az;
	}

	Matrix TransformRotation::getMatrix() const noexcept
	{
		Matrix matrix, tmpMat;

		matrix.set_rotation_z(mAngle[size_v<2>]);

		tmpMat.set_rotation_x(mAngle[size_v<0>]);
		matrix *= tmpMat;

		tmpMat.set_rotation_y(mAngle[size_v<1>]);
		matrix *= tmpMat;

		return matrix;
	}

	Matrix TransformRotation::inverse() const
	{
		Matrix matrix, tmpMat;

		matrix.set_rotation_y(mAngle[size_v<1>]);

		tmpMat.set_rotation_x(mAngle[size_v<0>]);
		matrix = matrix * pl::no_transpose_p{tmpMat.transpose()};

		tmpMat.set_rotation_z(mAngle[size_v<2>]);
		matrix *= tmpMat.transpose();

		return matrix;
	}


	void TransformTranslation::setPositionX(Real px) noexcept
	{
		mPosition.set(px, size_v<0>);
	}

	void TransformTranslation::setPositionY(Real py) noexcept
	{
		mPosition.set(py, size_v<1>);
	}

	void TransformTranslation::setPositionZ(Real pz) noexcept
	{
		mPosition.set(pz, size_v<2>);
	}

	void TransformTranslation::setPosition(Point3Dr p) noexcept
	{
		mPosition = p;
	}

	void TransformTranslation::setPosition(Real px, Real py, Real pz) noexcept
	{
		mPosition.set_all(px, py, pz);
	}

	void TransformTranslation::moveX(Real px) noexcept
	{
		mPosition[0] += px;
	}

	void TransformTranslation::moveY(Real py) noexcept
	{
		mPosition[1] += py;
	}

	void TransformTranslation::moveZ(Real pz) noexcept
	{
		mPosition[2] += pz;
	}

	void TransformTranslation::move(Real px, Real py, Real pz) noexcept
	{
		mPosition += Dir3Dr{px, py, pz};
	}

	void TransformTranslation::move(Dir3Dr p) noexcept
	{
		mPosition += p;
	}

	Matrix TransformTranslation::getMatrix() const noexcept
	{
		Matrix matrix{pl::tag::identity};
		const auto &var = pl::simd::vector_access(mPosition);

		matrix.v[0].set(var[0], size_v<3>);
		matrix.v[1].set(var[1], size_v<3>);
		matrix.v[2].set(var[2], size_v<3>);

		return matrix;
	}

	Matrix TransformTranslation::inverse() const
	{
		Matrix matrix{pl::tag::identity};
		const auto &var = pl::simd::vector_access(mPosition);

		matrix.v[0].set(-var[0], size_v<3>);
		matrix.v[1].set(-var[1], size_v<3>);
		matrix.v[2].set(-var[2], size_v<3>);

		return matrix;
	}



	void TransformScale::setScaleX(Real sx) noexcept
	{
		mScale.set(sx, size_v<0>);
	}

	void TransformScale::setScaleY(Real sy) noexcept
	{
		mScale.set(sy, size_v<1>);
	}

	void TransformScale::setScaleZ(Real sz) noexcept
	{
		mScale.set(sz, size_v<2>);
	}

	void TransformScale::setScale(Real sx, Real sy, Real sz) noexcept
	{
		mScale.set_all(sx, sy, sz);
	}

	void TransformScale::setScale(Point3Dr s) noexcept
	{
		mScale = s;
	}

	void TransformScale::scaleX(Real sx) noexcept
	{
		mScale[0] *= sx;
	}

	void TransformScale::scaleY(Real sy) noexcept
	{
		mScale[1] *= sy;
	}

	void TransformScale::scaleZ(Real sz) noexcept
	{
		mScale[2] *= sz;
	}

	void TransformScale::scale(Real sx, Real sy, Real sz) noexcept
	{
		mScale *= Dir3Dr{sx, sy, sz};
	}

	void TransformScale::scale(Dir3Dr s) noexcept
	{
		mScale *= s;
	}

	Matrix TransformScale::getMatrix() const noexcept
	{
		Matrix matrix{pl::tag::identity};
		const auto &var = pl::simd::vector_access(mScale);

		matrix.v[0].set(var[0], size_v<0>);
		matrix.v[1].set(var[1], size_v<1>);
		matrix.v[2].set(var[2], size_v<2>);

		return matrix;
	}

	Matrix TransformScale::inverse() const
	{
		Matrix matrix{pl::tag::identity};
		const auto inv = 1 / mScale;
		const auto &var = pl::simd::vector_access(inv);

		matrix.v[0].set(var[0], size_v<0>);
		matrix.v[1].set(var[1], size_v<1>);
		matrix.v[2].set(var[2], size_v<2>);

		return matrix;
	}

	Matrix TransformCombined::getMatrix() const noexcept
	{
		return mPivot.inverse() * (mScale * mRot) * mPivot * mTrans;
	}

	Matrix TransformCombined::inverse() const
	{
		return mTrans.inverse() * mPivot.inverse() * (mRot.inverse() * mScale.inverse()) * mPivot;
	}

	void TransformCombined::scale(Dir3Dr s) noexcept
	{
		mScale.scale(s);
	}

	void TransformCombined::setScale(Point3Dr s) noexcept
	{
		mScale.setScale(s);
	}

	void TransformCombined::rotate(Dir3Dr s) noexcept
	{
		mRot.rotate(s);
	}

	void TransformCombined::setAngle(Point3Dr s) noexcept
	{
		mRot.setAngle(s);
	}

	void TransformCombined::translate(Dir3Dr s) noexcept
	{
		mTrans.move(s);
	}

	void TransformCombined::setPosition(Point3Dr s) noexcept
	{
		mTrans.setPosition(s);
	}

	void TransformCombined::setPivot(Point3Dr s) noexcept
	{
		mPivot.setPosition(s);
	}


	Matrix TransformPerspective::getMatrix() const noexcept
	{
		Matrix matrix{pl::tag::identity};

		matrix.v[2]
			.set(mFarp / (mFarp - mNearp), size_v<2>)
			.set(-mFarp * mNearp / (mFarp - mNearp), size_v<3>);
		matrix.v[3]
			.set(1, size_v<2>)
			.set(0, size_v<3>);

		const Real fovTan = 1 / std::tan(mFov * 0.5);

		return matrix * TransformScale{{fovTan, fovTan, -1}};
	}

	Matrix TransformPerspective::inverse() const
	{
		return pl::try_inverse(getMatrix(), "Failed to inverse transformation matrix.");
	}


	Matrix TransformOrthographic::getMatrix() const noexcept
	{
		return TransformTranslation{{0, 0, -mNearp}} * TransformScale{{1, 1, 1 / (mFarp - mNearp)}} * TransformScale{{mFov, mFov, -1}};
	}

	Matrix TransformOrthographic::inverse() const
	{
		return pl::try_inverse(getMatrix(), "Failed to inverse transformation matrix.");
	}

}
