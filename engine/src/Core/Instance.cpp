/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Instance.hpp>
#include <PhotoRT/Core/PrimitiveGroup.hpp>
#include <PhotoRT/Core/Transform.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

#include <patlib/scoped.hpp>
#include <patlib/io.hpp>

#include <fmt/format.h>

namespace PhotoRT
{
	// Instances
	PRT_ENTITY_FACTORY_DEFINE(ObjectInstance)
	PRT_ENTITY_FACTORY_DEFINE(ObjectInstancePtr)


	Box ObjectInstance::getAABox() const noexcept
	{
		return mObject->getAABox();
	}

	InstanceNode &ObjectInstance::addNode(InstanceNode node)
	{
		mNodes.push_back(std::move(node));
		return mNodes.back();
	}

	[[nodiscard]] auto buildObjectInstanceNodesSerializer(auto& instance) noexcept
	{
		return pl::io::container(instance.mNodes, pl::io::mode_text, false, pl::io::default_initialize_proj(type_v<InstanceNode>, [&](auto, auto&& fc, auto&& n)
			{
				fc(pl::io::range{pl::io::mode_text, '\n', std::make_tuple(
					std::ref(n.mat),
					std::ref(n.matInv),
					pl::io::value{pl::io::mode_text, pl::bool_false, pl::io::default_initialize_proj(type_v<std::string>, pl::overload_set{
						[&](pl::io::dir_in_t, auto&& fc, auto&& instId)
						{
							if(fc() == pl::io::failure || instId.empty())
								throw Exception("Failed to read instance id.");

							n.instance = pl::assert_valid(instance.addReference(type_v<IObjectInstance>, "instance", "&" + instId));
						},
						[&](pl::io::dir_out_t, auto&& fc)
						{
							fc(dynamic_cast<const IRenderEntity &>(*n.instance).getEntityId());
						}
					})}
				)});
			}));
	}

	void ObjectInstance::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		mNodes.clear();

		setTransform(addParamReference(type_v<ITransform>, params, "p"));
		setObject(addParamReference(type_v<IPrimitiveGroup>, params, "o"));

		const auto createEntityInBuffer =
			[&](auto &buffer, auto &factorySearch, std::string name, const auto &params)
			{
				auto factory = factorySearch.find(name);
				if(!factory)
					throw Exception(fmt::format("Invalid transform {} given for node.", pl::io::quoted{name}));

				void *ptr = buffer.to_void();
				const auto si = factory->storage_info();
				szt space = buffer.size();
				if(!std::align(si.size, si.align, ptr, space))
					throw Exception(fmt::format("Buffer too small to create transform of type {}.", pl::io::quoted{name}));

				pl::scoped_pointer_destruct transformEntity{factory->create(ptr)};
				if(params)
					transformEntity->setParameters(*params);

				return transformEntity.release();
			};

		if(const auto p = params.findParam("c"); p && p->childParams)
			for(const auto &param : *p->childParams)
				if(!param.second.value.empty())
					mNodes.push_back({.instance = addReference(type_v<IObjectInstance>, param.second)});

		if(const auto p = params.findParam("nt"); p)
		{
			pl::io::stream_from(p->value, pl::io::throw_on_failure, [&](auto&& is, auto&&)
			{
				is >> buildObjectInstanceNodesSerializer(*this);
			});
		}

		if(const auto p = params.findParam("n"); p && p->childParams)
		{
			//std::aligned_storage_t<1024> buffer;
			pl::unitialized_storage<1024> buffer;
			CachedFactorySearch factorySearch{factories};

			mNodes.reserve(mNodes.size() + p->childParams->size());

			for(const auto &param : *p->childParams)
			{
				if(auto iParam = param.second.findParam("i"); iParam)
				{
					InstanceNode node{.instance = addReference(type_v<IObjectInstance>, *iParam)};

					if(auto pParam = param.second.findParam("p"); pParam)
					{
						const pl::scoped_pointer_destruct transformEntity{ createEntityInBuffer(buffer, factorySearch, "transform-" + pParam->value, pParam->childParams) };
						const auto &transform = dynamic_cast<const ITransform &>(*transformEntity);

						node.mat = transform;
						node.matInv = transform.inverse();
					}
					else
					{
						node.mat = param.second.getValueOrDefault("m", Matrix{pl::tag::identity});
						node.matInv = param.second.getValueOrDefault("im", Matrix{pl::tag::identity});
					}

					mNodes.push_back(std::move(node));
				}
			}
		}

		if(const auto p = params.findParam("ng"); p && p->childParams)
		{
			//std::aligned_storage_t<1024> buffer;
			pl::unitialized_storage<1024> buffer;
			CachedFactorySearch factorySearch{factories};

			for(const auto &groupParam : *p->childParams)
			{
				auto iParam = groupParam.second.findParam("i");
				auto listParam = groupParam.second.findParam("c");

				if(iParam && listParam && listParam->childParams)
				{
					auto instance = addReference(type_v<IObjectInstance>, *iParam);

					mNodes.reserve(mNodes.size() + listParam->childParams->size());

					for(const auto &param : *listParam->childParams)
					{
						InstanceNode node{.instance = instance};

						if(auto pParam = param.second.findParam("p"); pParam)
						{
							const pl::scoped_pointer_destruct transformEntity{ createEntityInBuffer(buffer, factorySearch, "transform-" + pParam->value, pParam->childParams) };
							const auto &transform = dynamic_cast<const ITransform &>(*transformEntity);

							node.mat = transform;
							node.matInv = transform.inverse();
						}
						else
						{
							node.mat = param.second.getValueOrDefault("m", Matrix{pl::tag::identity});
							node.matInv = param.second.getValueOrDefault("im", Matrix{pl::tag::identity});
						}

						mNodes.push_back(std::move(node));
					}
				}
			}
		}

		const auto setFlag = [&](InstanceFlagEnum flag)
			{ return [=, this](const bool v){ mFlags.set(flag, v); }; };

		params.getValueIfExistsBind
			("group",                   setFlag(InstanceFlagEnum::Group))
			("treegroup",               setFlag(InstanceFlagEnum::TreeGroup))
			("treegroup_aabbcheck",     setFlag(InstanceFlagEnum::AabbCheck))
			//("has_scaling",             setFlag(InstanceFlagEnum::HasScaling))
		;

		if(const auto p = params.findParam("obj_properties"); p && p->childParams)
		{
			for(const auto &child : *p->childParams)
			{
				auto props = addReference(type_v<IPrimitiveGroupProperties>, child.second);

				mPrimGroupProperties.emplace_back(props->getClassName(), props);
			}
		}

		mNodes.shrink_to_fit();
	}

	ParameterTree ObjectInstance::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		if(mTransform)
			params["p"].fromEntityRef(*mTransform, deep);

		if(mObject)
			params["o"].fromEntityRef(*mObject, deep);

		if(!mNodes.empty())
		{
			pl::io::stream_into(params["nt"].value, pl::io::throw_on_failure, [&](auto&& os, auto&&){
				os << std::dec << std::hexfloat << buildObjectInstanceNodesSerializer(*this);
			});
		}

		const auto getFlag = [&](InstanceFlagEnum flag)
			{ return mFlags[flag]; };

		params.insert({
			{"group",                getFlag(InstanceFlagEnum::Group)},
			{"treegroup",            getFlag(InstanceFlagEnum::TreeGroup)},
			{"treegroup_aabbcheck",  getFlag(InstanceFlagEnum::AabbCheck)}/*,
			{"has_scaling",          getFlag(InstanceFlagEnum::HasScaling)}*/
		});

		if(!mPrimGroupProperties.empty())
		{
			params["obj_properties"].hasChilds(true);

			for(auto &childs = *params["obj_properties"].childParams; const auto &[i, child] : rgv::enumerate(mPrimGroupProperties))
				childs[i].fromEntityRef(*child.props, deep);
		}

		return params;
	}

	void ObjectInstancePtr::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		setTransform(addParamReference(type_v<ITransform>, params, "p"));
		setPointer(addParamReference(type_v<IObjectInstance>, params, "ptr"));

		if(const auto p = params.findParam("obj_properties"); p && p->childParams)
		{
			for(const auto &child : *p->childParams)
			{
				auto props = addReference(type_v<IPrimitiveGroupProperties>, child.second);

				mPrimGroupProperties.emplace_back(props->getClassName(), props);
			}
		}
	}

	ParameterTree ObjectInstancePtr::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		if(mTransform)
			params["p"].fromEntityRef(*mTransform, deep);

		if(mInstance)
			params["ptr"].fromEntityRef(*mInstance, deep);

		if(!mPrimGroupProperties.empty())
		{
			params["obj_properties"].hasChilds(true);

			auto &childs = *params["obj_properties"].childParams;
			for(auto&& [i, child] : rgv::enumerate(mPrimGroupProperties))
				childs[i].fromEntityRef(*child.props, deep);
		}

		return params;
	}
}
