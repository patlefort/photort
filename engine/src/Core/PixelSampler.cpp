/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/PixelSampler.hpp>
#include <PhotoRT/Core/Distribution.hpp>
#include <PhotoRT/Core/Camera.hpp>
#include <PhotoRT/Core/Integrator.hpp>
#include <PhotoRT/Core/Film.hpp>
#include <PhotoRT/Core/Bitmap.hpp>
#include <PhotoRT/Core/Scene.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

#include <patlib/rectangle.hpp>
#include <patlib/accumulator.hpp>

namespace PhotoRT
{
	// Pixel samplers
	PRT_ENTITY_FACTORY_DEFINE(PixelSamplerUniform)
	PRT_ENTITY_FACTORY_DEFINE(PixelSamplerVariance)


	void PixelSamplerBase::init()
	{
		mSequenceCount = 0;
	}

	void PixelSamplerBase::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		params.getValueIfExistsBind("spp", PL_LAMBDA_FORWARD_THIS(setSPP));
	}

	ParameterTree PixelSamplerBase::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params["spp"] = mSpp;

		return params;
	}

	void PixelSamplerBase::serialize(std::ostream &) const
	{
		/*outputStream
			<< mNbSamplesTotal << '\n'
			<< mSequenceCount;*/
	}

	void PixelSamplerBase::unserialize(std::istream &)
	{
		//inputStream >> mNbSamplesTotal >> mSequenceCount;
	}



	void PixelSamplerUniform::setDimensions(Dir2Di tileSize, Dir2Di nbTiles, Dir2Di padding, Dir2Di size)
	{
		PixelSamplerBase::setDimensions(tileSize, nbTiles, padding, size);

		mNbTiles = nbTiles;
		mTileSize = tileSize;
		mPadding = padding;
		mTileSizePad = mTileSize + mPadding * 2;

		mTileMid = mNbTiles / 4;
		mNbTilesTotal = mNbTiles[0] * mNbTiles[1];
	}

	void PixelSamplerUniform::init()
	{
		PixelSamplerBase::init();

		mSpaceFillingCurve.setBounds(mNbTiles);

		mTileSampleCount = 0;
	}

	void PixelSamplerUniform::reset()
	{
		PixelSamplerBase::reset();

		mTileSampleCount = 0;
		mSpaceFillingCurve.reset();
	}

	void PixelSamplerUniform::generateTileIndex(PixelTile &tile, if32 index)
	{
		pl::static_cast_into(mPadding, tile.bufferStart);
		tile.bufferSize = mTileSizePad;

		tile.tilePos[1] = index / mNbTiles[0];
		tile.tilePos[0] = index - tile.tilePos[1] * mNbTiles[0];

		tile.tileDims = {tile.tilePos * mTileSize, pl::size_p{mTileSize}};
		tile.tileDims = tile.tileDims.clip(pl::rectangle<>{{}, pl::size_p{mSize}});

		tile.tileIndex = index;
		tile.rayImportance = 1;
		tile.rayDistributionFactor = 1;
		tile.pixelSampler = this;
		tile.tileSampleCount = mTileSampleCount;
		tile.camera = mCamera;
	}

	void PixelSamplerUniform::generateTileIndex(PixelTile &tile, const Point2Di pos)
	{
		generateTileIndex(tile, pos[1] * mNbTiles[0] + pos[0]);
	}

	bool PixelSamplerUniform::generateTile(PixelTile &tile, IRNGInt &)
	{
		if(mSpp && mTileSampleCount >= mSpp)
			return false;

		generateTileIndex(tile, advanceCurve());

		if(++mSequenceCount >= (if32)mNbTilesTotal)
		{
			++mTileSampleCount;
			mSequenceCount = 0;
		}

		return true;
	}


	void PixelSamplerVariance::setParameters(const ParameterTree &params)
	{
		PixelSamplerUniform::setParameters(params);

		params.getValueIfExistsBind("weight_cap", PL_LAMBDA_FORWARD_THIS(setWeightCap));
		params.getValueIfExistsBind("nb_calculate_weight_cap", PL_LAMBDA_FORWARD_THIS(setNbWeightCalculatedCap));
	}

	ParameterTree PixelSamplerVariance::getParameters(bool deep) const
	{
		ParameterTree params {PixelSamplerUniform::getParameters(deep)};

		params["weight_cap"] = mWeightCap;
		params["nb_calculate_weight_cap"] = mNbWeightCalculatedCap;

		return params;
	}

	void PixelSamplerVariance::init()
	{
		PixelSamplerUniform::init();

		mTileVariance.resize(mNbTilesTotal);
		mTileRecorded.resize(mNbTilesTotal);
		mTileWeights.resize(mNbTilesTotal);
		mTilesSampleCount.resize(mNbTilesTotal);
		mRecordedTiles.resize(mNbTilesTotal * mTileSize[0] * mTileSize[1]);

		mTileVariance.shrink_to_fit();
		mTileRecorded.shrink_to_fit();
		mTileWeights.shrink_to_fit();
		mTilesSampleCount.shrink_to_fit();
		mRecordedTiles.shrink_to_fit();

		mNbVarianceReported = 0;
		mNbWeightCalculated = 0;
		mTileSeqIndex = 0;
		mNbTilesWeighted = mNbTilesTotal;
	}

	void PixelSamplerVariance::tileDone(PixelSampleTileLayers &tile)
	{
		if(tile.weight <= 0)
			return;

		if(mNbWeightCalculated < mNbWeightCalculatedCap)
		{
			namespace ac = pl::accumulators;
			constexpr auto max = ac::tag::max_c;

			ac::accumulator_set maxDif{type_v<CReal>, max};
			auto rt = mRecordedTiles.begin() + tile.tileIndex * mTileSize[0] * mTileSize[1];

			// Get the maximum color difference between current and the last sample after tone mapping
			pl::rectangle<>{mPadding.as_point(), pl::size_p{tile.tileDims.dims()}}.loop([&](const auto p){
				auto color = tile.getContribution(p);

				// Use a simple linear tone mapper
				color = (White - ((-color / tile.weight) /oprtexp2));
				color.set(1, size_v<3>);

				maxDif((color - Color{*rt}) /osdot);

				*rt++ = static_cast<Color3_16>(color);
			});

			const auto maxDifSqr = maxDif.extract(max) /oprtsqrt;

			//std::cout << maxDif << '\n';

			if(mTileRecorded[tile.tileIndex] && pl::is_valid(maxDifSqr, valid::Weight))
			{
				mTileVariance[tile.tileIndex] = pl::plmax(maxDifSqr, mTileVariance[tile.tileIndex]);
				mNbVarianceReported++;
			}

			mTileRecorded[tile.tileIndex] = true;

			// Calculate tile weights every mNbTilesWeighted
			if(mNbVarianceReported % mNbTilesWeighted == 0)
				calculateTileVariance();
		}
	}

	void PixelSamplerVariance::dumpInfo(Scene &scene, const std::filesystem::path &folder, const std::string &name)
	{
		TextureBitmap<u32> debugBitmap;

		debugBitmap.allocate(mNbTiles * mTileSize);

		const auto r = pl::rectangle<>{{}, pl::size_p{mNbTiles}};
		for(auto&& [rect, weight] :
			rgv::zip(
				r | rgv::transform([&](auto&& p){ return pl::rectangle<>{p * mTileSize, pl::size_p{mTileSize}}; }),
				mTileWeights
			)
		)
		{
			const Color color{((CReal)weight - 1) / (CReal)mWeightCap, 1};

			rect.loop([&](const auto p2){
				debugBitmap.setPixel(color, p2[0], p2[1]);
			});
		}

		scene.exportEntity(debugBitmap, folder / (name + "variance.png"), "image/png");
	}

	void PixelSamplerVariance::reset()
	{
		PixelSamplerUniform::reset();

		mTileVariance.assign(mTileVariance.size(), 0);
		mTileRecorded.assign(mTileRecorded.size(), false);
		mTilesSampleCount.assign(mTilesSampleCount.size(), 0);
		mRecordedTiles.assign(mRecordedTiles.size(), pl::tag::zero);

		mNbWeightCalculated = 0;
		mTileSeqIndex = 0;
		mSequenceIndex = 0;
		mNbTilesWeighted = mNbTilesTotal;
	}

	bool PixelSamplerVariance::generateTile(PixelTile &tile, IRNGInt &)
	{
		if(mSpp && mTileSampleCount >= mSpp)
			return false;

		if(!mNbWeightCalculated)
		{
			auto pos = advanceCurve();
			mSequenceIndex = pos[1] * mNbTiles[0] + pos[0];
			generateTileIndex(tile, mSequenceIndex);
			tile.tileSampleCount = mTilesSampleCount[mSequenceIndex]++;
		}
		else
		{
			// Sample tile mTileWeights[mSequenceIndex] number of times
			generateTileIndex(tile, mSequenceIndex);
			tile.tileSampleCount = mTilesSampleCount[mSequenceIndex]++;

			if(++mTileSeqIndex >= mTileWeights[mSequenceIndex])
			{
				auto pos = advanceCurve();
				mSequenceIndex = pos[1] * mNbTiles[0] + pos[0];
				mTileSeqIndex = 0;
			}
		}

		if(++mSequenceCount >= mNbTilesTotal)
		{
			++mTileSampleCount;
			mSequenceCount = 0;
		}

		return true;
	}

	void PixelSamplerVariance::calculateTileVariance()
	{
		namespace ac = pl::accumulators;
		constexpr auto max = ac::tag::max_c;
		constexpr auto min = ac::tag::min_c;

		ac::accumulator_set minMax{type_v<CReal>, min, max};

		// Find the min and max variance
		for(auto&& [recTile, varTile] : rgv::zip(mTileRecorded, mTileVariance))
		{
			if(!recTile)
				return;

			minMax(varTile);
		}

		const auto [minVariance, maxVariance] = minMax.extract(min, max);
		const auto varRange = maxVariance - minVariance;

		// Calculate weight of tiles based on the variance
		if(varRange > 0)
		{
			auto tileWeightView = mTileVariance | rgv::transform([&](auto&& varTile){
				return pl::plmax<Real>(1 + ((varTile - minVariance) / varRange) /opow2 * r_(mWeightCap), 1);
			});

			for(mNbTilesWeighted = 0; auto&& [tileWeight, w] : rgv::zip(mTileWeights, tileWeightView))
			{
				tileWeight = w;
				mNbTilesWeighted += w;
			}

			++mNbWeightCalculated;
		}
	}

	void PixelSamplerVariance::serialize(std::ostream &) const
	{
	}

	void PixelSamplerVariance::unserialize(std::istream &)
	{
	}

}
