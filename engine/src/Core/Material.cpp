/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Material.hpp>
#include <PhotoRT/Core/RayTracer.hpp>
#include <PhotoRT/Core/Normals.hpp>
#include <PhotoRT/Core/BxDF.hpp>
#include <PhotoRT/Core/Medium.hpp>
#include <PhotoRT/Core/Edf.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_DEFINE(Material)


	Material::Material()
	{
		mLayers.emplace_back(this);
	}

	Material::~Material() = default;

	void Material::setParameters(const ParameterTree &params)
	{
		RenderEntity::setParameters(params);

		params.getValueIfExistsBind
			("refraction_priority", PL_LAMBDA_FORWARD_THIS(setRefractionPriority))
			("sampling_importance", PL_LAMBDA_FORWARD_THIS(setSamplingImportance))
			("transparent", PL_LAMBDA_FORWARD_THIS(setTransparent))
			("absorbant", PL_LAMBDA_FORWARD_THIS(setAbsorbant))
			("absorbance", PL_LAMBDA_FORWARD_THIS(setAbsorbance))
			("sample_direct_light_medium", PL_LAMBDA_FORWARD_THIS(setSampleDirectLightMedium))
		;
		setNormalMapper(addParamReference(type_v<INormalMapper>, params, "normalmap"));
		setMedium(addParamReference(type_v<IMedium>, params, "medium"));

		mLayers.clear();

		if(const auto p = params.findParam("layers"); p && p->childParams)
		{
			for(const auto &paramPair : *p->childParams)
			{
				const auto &param = paramPair.second;

				if(!param.childParams)
					continue;

				MaterialLayer layer{this};

				param.getValueIfExistsBind
					("refraction_index", PL_LAMBDA_FORWARD_OBJ(setRefractionIndex, layer))
					("substance", PL_LAMBDA_FORWARD_OBJ(setSubstance, layer))
				;
				layer.setBxDF(addParamReference(type_v<IBxDF>, *param.childParams, "bxdf"));
				layer.setOptimizedBxDF(addParamReference(type_v<IBxDF>, *param.childParams, "optimized_bxdf"));
				layer.setEmission(addParamReference(type_v<IEDF>, *param.childParams, "emission"));

				mLayers.push_back(layer);
			}
		}
		else
		{
			mLayers.emplace_back(this);

			params.getValueIfExistsBind
				("refraction_index", PL_LAMBDA_FORWARD_OBJ(setRefractionIndex, this->mLayers.back()))
				("substance", PL_LAMBDA_FORWARD_OBJ(setSubstance, this->mLayers.back()))
			;

			setBxDF(addParamReference(type_v<IBxDF>, params, "bxdf"));
			setOptimizedBxDF(addParamReference(type_v<IBxDF>, params, "optimized_bxdf"));
			setEmission(addParamReference(type_v<IEDF>, params, "emission"));
		}

		if(mLayers.empty())
			mLayers.emplace_back(this);

		mLayers.shrink_to_fit();
	}

	ParameterTree Material::getParameters(bool deep) const
	{
		ParameterTree params {RenderEntity::getParameters(deep)};

		params.insert({
			{"refraction_priority", mRefractionPriority},
			{"sampling_importance", mSamplingImportance},
			{"transparent", mTransparent},
			{"absorbant", mAbsorbant},
			{"absorbance", mAbsorbance},
			{"sample_direct_light_medium", mSampleDirectLightMedium}
		});

		if(mNormalMap)
			params["normalmap"].fromEntityRef(*mNormalMap, deep);

		if(mMedium)
			params["medium"].fromEntityRef(*mMedium, deep);

		if(mLayers.size() == 1)
		{
			params["refraction_index"] = mLayers.back().getRefractionIndex();
			params["substance"] = mLayers.back().isSubstance();

			if(getBxDF())
				params["bxdf"].fromEntityRef(*getBxDF(), deep);

			if(getOptimizedBxDF())
				params["optimized_bxdf"].fromEntityRef(*getOptimizedBxDF(), deep);

			if(getEmission())
				params["emission"].fromEntityRef(*getEmission(), deep);
		}
		else
		{
			for(auto&& [i, l] : rgv::enumerate(mLayers))
			{
				auto &lparam = params["layers"][i];

				lparam["refraction_index"] = l.getRefractionIndex();
				lparam["substance"] = l.isSubstance();
				if(l.getBxDF())
					lparam["bxdf"].fromEntityRef(*l.getBxDF(), deep);
				if(l.getOptimizedBxDF())
					lparam["optimized_bxdf"].fromEntityRef(*l.getOptimizedBxDF(), deep);
				if(l.getEmission())
					lparam["emission"].fromEntityRef(*l.getEmission(), deep);
			}
		}

		return params;
	}

	LayerAlpha Material::getLayerAlpha(if32 layerIndex, EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir, EnumHemisphere hemisphere, bool ignoreSelf) const noexcept
	{
		const auto traverseLayers = [&](const auto bxdfRange)
			{
				auto r = bxdfRange
					| rgv::transform([](const auto &l){ return std::to_address(l.getBxDF()); })
					| rgv::transform([&](const auto bxdf) -> LayerAlpha {
						if(bxdf)
						{
							const auto a = bxdf->alpha(mode, inter, odir, idir);
							return {.alpha = a, .pdf = alphaProbability(a)};
						}

						return {.alpha = White, .pdf = 1};
					});

				return pl::accumulate_until(r, LayerAlpha{}, [](const auto &acc){ return acc.pdf == 1; }, [](const auto &acc, const auto &cur){
						return LayerAlpha{
							.alpha = pl::pllerp(acc.alpha, White, cur.alpha),
							.pdf = pl::pllerp(acc.pdf, Real{1}, cur.pdf)
						};
					});
			};

		if(const auto layerHemisphere = inter.surface().isBackfaceHit() ? swapHemisphere(hemisphere) : hemisphere;
				layerHemisphere == EnumHemisphere::South)
			return traverseLayers(mLayers | rgv::drop_exactly(layerIndex + (if32)ignoreSelf));

		return traverseLayers(mLayers | rgv::take_exactly(layerIndex + (if32)!ignoreSelf) | rgv::reverse);
	}

	Color Material::absorbColor(Real distance) const noexcept
	{
		return isSubstance() && isAbsorbant() ?
			(pl::plisinf(distance) ?
				mAbsorbanceInf
				: (mAbsorbance * -distance) /oexp)
			: White;
	}

	void Material::setColor(CReal cr, CReal cg, CReal cb, CReal ca)
	{
		mColor.set_all(
			pl::plclamp<CReal>(cr, 0, 1),
			pl::plclamp<CReal>(cg, 0, 1),
			pl::plclamp<CReal>(cb, 0, 1),
			pl::plclamp<CReal>(ca, 0, 1)
		);
	}

	void Material::setColor(u32 col)
	{
		mColor.set_all(
			((col&0x00ff0000)>>16) * (1.0 / 255.0),
			((col&0x0000ff00)>>8) * (1.0 / 255.0),
			(col&0x000000ff) * (1.0f / 255.0),
			((col&0xff000000)>>24) * (1.0 / 255.0)
		);
	}

	void Material::setColor(Color col)
	{
		mColor.set_all(
			pl::plclamp<CReal>(col[0], 0, 1),
			pl::plclamp<CReal>(col[1], 0, 1),
			pl::plclamp<CReal>(col[2], 0, 1),
			pl::plclamp<CReal>(col[3], 0, 1)
		);
	}

	void Material::initForRendering()
	{
		mAnisotropy = std::any_of(mLayers.cbegin(), mLayers.cend(), [](const auto &l){
			return (l.getOptimizedBxDF() && l.getOptimizedBxDF()->isAnisotropic())
				|| (l.getBxDF() && l.getBxDF()->isAnisotropic())
				|| (l.getEmission() && l.getEmission()->isAnisotropic());
		});
	}

}
