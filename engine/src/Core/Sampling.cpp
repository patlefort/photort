/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Sampling.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>

namespace PhotoRT
{
	// MIS heuristics
	PRT_ENTITY_FACTORY_AUGMENT_INLINE("mis-average", AugmentedMISHeuristicAverage, MISHeuristicAverage)
	PRT_ENTITY_FACTORY_AUGMENT_INLINE("mis-balance", AugmentedMISHeuristicBalance, MISHeuristicBalance)
	PRT_ENTITY_FACTORY_AUGMENT_INLINE("mis-power", AugmentedMISHeuristicPower, MISHeuristicPower)


	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("mis-cutoff", AugmentedMISHeuristicCutoff, MISHeuristicCutoff)
	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedMISHeuristicCutoff)
	void AugmentedMISHeuristicCutoff::setParameters(const ParameterTree &params)
	{
		params.getValueIfExists("cutoff", mCutoff);
	}

	ParameterTree AugmentedMISHeuristicCutoff::getParameters(bool /*deep*/) const
	{
		ParameterTree params;

		params["cutoff"] = mCutoff;

		return params;
	}


	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("misset-", AugmentedMISHeuristicSet, MISHeuristicSet)
	PRT_ENTITY_FACTORY_AUGMENT_DEFINE(AugmentedMISHeuristicSet)
	void AugmentedMISHeuristicSet::setParameters(const ParameterTree &params)
	{
		addParamReference(mMis, params, "mis");
	}

	ParameterTree AugmentedMISHeuristicSet::getParameters(bool deep) const
	{
		ParameterTree params;

		if(mMis)
			params["mis"].fromEntityRef(*mMis, deep);

		return params;
	}


	PRT_ENTITY_FACTORY_AUGMENT_INLINE("misset-maximum", AugmentedMISHeuristicSetMaximum, MISHeuristicSetMaximum)

	// Samplers
	/*PRT_ENTITY_FACTORY_AUGMENT_INLINE("sampler1dr-", AugmentedSamplerBasicReal, SamplerBasic<Real>)
	PRT_ENTITY_FACTORY_AUGMENT_INLINE("sampler1dr-stratified", AugmentedSamplerStratified1D, SamplerStratified1D)

	PRT_ENTITY_FACTORY_AUGMENT_INLINE("sampler2dr-", AugmentedSamplerBasicPoint2Dr, SamplerBasic<Point2Dr>)
	PRT_ENTITY_FACTORY_AUGMENT_INLINE("sampler2dr-stratified", AugmentedSamplerStratified2D, SamplerStratified2D)*/

	// Mutators
	//PRT_ENTITY_FACTORY_DEFINE("mutator-metropolis", MutatorFactoryMetropolis)


	/*void SamplerStratified1D::requestSamples(std::span<Real> samples, const Real *rng) noexcept
	{
		if(samples.size() == 1)
		{
			samples[0] = rng[0];
			return;
		}

		const auto stratSize = pl::plmin<Real>(1, r_(samples.size()) /oprtreciprocal);

		for(const auto i : mir(samples.size()))
			samples[i] = ImplRng::clampRng(rng[i] * stratSize + i * stratSize);
	}

	void SamplerStratified2D::requestSamples(std::span<Point2Dr> samples, const Point2Dr *rng) noexcept
	{
		if(samples.size() == 1)
		{
			samples[0] = rng[0];
			return;
		}

		Point2Di nb;
		Point2Dr stratSize;

		nb[0] = pl::plmax<uf32>(1, r_(samples.size()) /oprtsqrt);
		nb[1] = samples.size() / nb[0];
		stratSize[0] = pl::plmin<Real>(1, r_(nb[0]) /oprtreciprocal);
		stratSize[1] = pl::plmin<Real>(1, r_(nb[1]) /oprtreciprocal);

		auto spIt = samples.begin();

		for(Point2Di::value_type y=0; y<nb[1]; ++y)
			for(Point2Di::value_type x=0; x<nb[0]; ++x, ++spIt, ++rng)
				*spIt = ImplRng::clampRng(*rng * stratSize + Point2Di{x, y} * stratSize);

		std::copy_n(rng, samples.size() - nb[0] * nb[1], spIt);
	}*/


	/*Real MutatorMetropolis::get1d()
	{
		if(mIndexSp1d == mSp1d->size())
		{
			auto r = (*mRng1d)();
			//lastModificationIteration1d.push_back(mIteration);
			mSp1d->push_back(r);
			++mIndexSp1d;
			return r;
		}

		Real r;

		const auto old = (*mSp1d)[mIndexSp1d];
		//const if32 nSmall = (if32)mIteration - (if32)lastModificationIteration1d[mIndexSp1d];
		const Real normalSp = std::numbers::sqrt2_v<Real> * pl::erf_inv((*mRng1d)() * 2 - 1);
		//const Real curSigma = mFactory.sigma * prtsqrt(r_(nSmall));
		r = old + normalSp * mFactory.mSigma;
		r -= r /ofloor;
		r *= 1 - constants::epsilon<Real>;

		plassert(pl::is_valid(r) && !(r < 0 || r >= 1) );

		//lastModificationIteration1d[mIndexSp1d] = mIteration;
		(*mSp1d)[mIndexSp1d++] = r;

		return r;
	}

	Point2Dr MutatorMetropolis::get2d()
	{
		if(mIndexSp2d == mSp2d->size())
		{
			auto r = (*mRng2d)();
			//lastModificationIteration2d.push_back(mIteration);
			mSp2d->push_back(r);
			++mIndexSp2d;
			return r;
		}

		Point2Dr r;

		const auto old = (*mSp2d)[mIndexSp2d];
		//const if32 nSmall = (if32)mIteration - (if32)lastModificationIteration2d[mIndexSp2d];
		r = (*mRng2d)();
		const Point2Dr normalSp {std::numbers::sqrt2_v<Real> * pl::erf_inv(r[0] * 2 - 1), std::numbers::sqrt2_v<Real> * pl::erf_inv(r[1] * 2 - 1)};
		//const Real curSigma = mFactory.sigma * prtsqrt(r_(nSmall));
		r = old + normalSp * mFactory.mSigma;
		r -= r /ofloor;
		r *= 1 - constants::epsilon<Real>;

		plassert(pl::is_valid(r) && !(r < 0 || r >= 1) );

		//lastModificationIteration2d[mIndexSp2d] = mIteration;
		(*mSp2d)[mIndexSp2d++] = r;

		return r;
	}

	void MutatorMetropolis::newPath(std::vector<Real> &sp1d, std::vector<Point2Dr> &sp2d, IRNGInt &rng1d, IRNG2D &rng2d)
	{
		mSp1d = &sp1d;
		mSp2d = &sp2d;
		mRng1d = &rng1d;
		mRng2d = &rng2d;

		mIteration = 0;
		mLastLargeStepIteration = 0;
	}

	void MutatorMetropolis::newIteration()
	{
		mIndexSp1d = 0;
		mIndexSp2d = 0;
		++mIteration;
		mBksp1d = *mSp1d;
		mBksp2d = *mSp2d;
		//bklmi1d = lastModificationIteration1d;
		//bklmi2d = lastModificationIteration2d;

		mLargeStep = (*mRng1d)() < mFactory.mLargeStepProbability;

		if(mLargeStep)
		{
			mSp1d->clear();
			mSp2d->clear();
		}
	}

	void MutatorMetropolis::accept()
	{
		if(mLargeStep)
			mLastLargeStepIteration = mIteration;
	}

	void MutatorMetropolis::reject()
	{
		*mSp1d = mBksp1d;
		*mSp2d = mBksp2d;
		//lastModificationIteration1d = bklmi1d;
		//lastModificationIteration2d = bklmi2d;
		--mIteration;
	}*/


	void Sampler::seed()
	{
		mRng.seed();
	}

	void Sampler::setPosition(u64 /*start*/, const Point2Di &/*p*/)
	{
		/*for(u64 i=0, nb=mSamples1d.size(), st = start * nb; i<nb; ++i)
		{
			mRng1d->setPosition(st + i, p);
			mSamples1d[i] = (*mRng1d)();
		}

		for(u64 i=0, nb=mSamples2d.size(), st = start * nb; i<nb; ++i)
		{
			mRng2d->setPosition(st + i, p);
			mSamples2d[i] = (*mRng2d)();
		}*/

		mSampleIndex = 0;
	}

	void Sampler::setBounds(const Point2Di &r)
	{
		mRng.setBounds(r);
	}

	void Sampler::setSampleBufferSize(u32 nb, u32 nbPaths)
	{
		mSamples.resize(nb);
		mRecordedsp.resize(nbPaths);

		for(auto &e : mRecordedsp)
			e.reserve(nb);
	}
}
