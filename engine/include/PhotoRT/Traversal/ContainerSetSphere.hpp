/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>
#include "ContainerSet.hpp"
#include <PhotoRT/PrimitiveGroup/Sphere.hpp>

namespace PhotoRT::Traversal
{
	#ifdef PRT_FEATURE_PACKED_VECTOR

		struct PackedSpheres
		{
			using PrimitiveType = PrimitiveSphere<>;
			static constexpr auto NbAxis = PrimitiveType::point_type::category::NbAxis;
			using StorageType = std::array<PackedVec::aligned_array_type, NbAxis>;
			using Packer = VecPackerAligned<StorageType::value_type>;
			using point_type = pl::simd::basic_geometric_vector<PackedVec, size_c<NbAxis>, pl::simd::categories::geometric_point_t, NbAxis>;
			using dir_type = pl::simd::basic_geometric_vector<PackedVec, size_c<NbAxis>, pl::simd::categories::geometric_dir_t, NbAxis>;

			StorageType mPos;
			StorageType::value_type mRadius;

			[[nodiscard]] CollisionResult testCollision(std::bitset<point_type::value_type::NbValues> mask, const point_type &packedOrig, const dir_type &packedDir, auto&& checkFc) const noexcept
			{
				CollisionResult res;

				const auto [resMask, tdist, bf] = Intersection::testSphere(
						packedOrig, packedDir,
						point_type{pl::tag::array_init, mPos.data()},
						point_type::value_type{mRadius} /opow2
					);

				mask &= resMask;

				if(mask.none())
					return res;

				const auto [index, cdist] = plmin_index(tdist, mask);
				const auto distSqr = cdist /opow2;
				if(index < 0 || !PL_FWD(checkFc)(index, cdist, distSqr))
					return res;

				res.collision.emplace();
				res.collision->distSqr = distSqr;
				res.collision->surface.isBackfaceHit(bf[index]);
				res.collision->surface.primIndex = index;

				return res;
			}

			template <typename... Ts_>
			void pack(const ImplPrimitives::PrimitiveSphere<Ts_...> &p, uf32 index) noexcept
			{
				Packer::pack(p.mPos, mPos, index);
				mRadius()[index] = p.mRadius;
			}

			[[nodiscard]] PrimitiveType unpack(uf32 index) const noexcept
			{
				return PrimitiveType(
					Packer::unpack<VecP>(mPos, index),
					mRadius()[index]
				);
			}
		};

	#endif

	PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("container_factory-sphere_generic", Augmented_container_factory__sphere_generic, PrimitiveContainerFactoryType<EntityContainerGeneric<PrimGroupSpheres>>)
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("container_factory-sphere_generic_cached", Augmented_container_factory__sphere_generic_cached, PrimitiveContainerFactoryType<EntityContainerCached<SetCached<PrimGroupSpheres>>>)

	#ifdef PRT_FEATURE_PACKED_VECTOR
		PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("container_factory-sphere_prepacked", Augmented_container_factory__sphere_prepacked, PrimitiveContainerFactoryType<EntityContainerCached<SetPrepacked<PackedSpheres, PrimGroupSpheres>>>)
		PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("container_factory-sphere_bvh_cached", Augmented_container_factory__sphere_bvh_cached, PrimitiveContainerFactoryType<EntityContainerBVH<SetPrepacked<PackedSpheres, PrimGroupSpheres>>>)
		PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("container_factory-sphere_bvh_generic", Augmented_container_factory__sphere_bvh_generic, PrimitiveContainerFactoryType<EntityContainerBVH<SetPacked<PackedSpheres, PrimGroupSpheres>>>)
	#else
		PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("container_factory-sphere_bvh_cached", Augmented_container_factory__sphere_bvh_cached, PrimitiveContainerFactoryType<EntityContainerBVH<SetCached<PrimGroupSpheres>>>)
		PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("container_factory-sphere_bvh_generic", Augmented_container_factory__sphere_bvh_generic, PrimitiveContainerFactoryType<EntityContainerBVH<SetGeneric<PrimGroupSpheres>>>)
	#endif
}