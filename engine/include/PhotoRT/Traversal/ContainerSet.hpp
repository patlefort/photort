/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/RayTraversalContainers.hpp>
#include <PhotoRT/Core/Instance.hpp>
#include <PhotoRT/Core/PrimitiveGroup.hpp>

#include <patlib/crtp.hpp>

#include <bitset>

namespace PhotoRT::Traversal
{
	struct SubContainer
	{
		PrimitiveIndex indexStart;
	};

	struct InstanceEntry
	{
		const IObjectInstance *instance;
		Matrix localToWorld, worldToLocal;
		PrimGroupPropertiesMapPtr propMap;
	};

	struct PrimitiveEntry
	{
		PrimitiveIndex pIndex, iIndex;
	};

	namespace impl
	{
		template <typename DerivedType_, typename SubContainerType_ = SubContainer>
		class Set : public pl::crtp_base<DerivedType_>
		{
		public:

			using pl::crtp_base<DerivedType_>::derived;

			using SubContainerType = SubContainerType_;

			std::span<const InstanceEntry> mInstances;
			ContainerVectorType<PrimitiveEntry, PrimitiveIndex> mPrimitives;
			ContainerVectorType<SubContainerType_, u32> mContainers;

			Set(std::pmr::memory_resource *memRes = std::pmr::get_default_resource()) :
				mPrimitives{typename decltype(mPrimitives)::allocator_type{std::in_place, memRes}},
				mContainers{typename decltype(mContainers)::allocator_type{std::in_place, memRes}}
			{}

			void setInstanceList(std::span<const InstanceEntry> l) noexcept { mInstances = l; }

			void setMemoryResource(std::pmr::memory_resource *memRes)
			{
				pl::recreate(mContainers,
					typename decltype(mContainers)::allocator_type{std::in_place, memRes}
				);
				pl::recreate(mPrimitives,
					typename decltype(mPrimitives)::allocator_type{std::in_place, memRes}
				);
			}

			[[nodiscard]] szt memory_capacity() const noexcept
			 { return pl::container_memory_capacity(mContainers) + pl::container_memory_capacity(mPrimitives); }

			void fill()
			{
				for(const auto i : mir(derived().getNbInstances()))
				{
					const auto &entry = getInstance(i);
					const auto &object = *entry.instance->getObject();

					plassert(entry.instance != nullptr);

					mPrimitives.reserve(mPrimitives.size() + object.getNbPrimitives());

					for(const auto c : mir(object.getNbPrimitives()))
						mPrimitives.push_back({c, i});
				}
			}


			[[nodiscard]] Real getContainerGeometricCost(const u32 containerIndex) const noexcept
				{ return derived().getNbPrimitives(containerIndex); }

			[[nodiscard]] Real getGeometricCost(const PrimitiveIndex nbPrimitives) const noexcept { return r_(nbPrimitives) * 80; }

			void newContainer()
			{
				SubContainerType_ entry;
				entry.indexStart = mPrimitives.size();

				mContainers.push_back(std::move(entry));
			}

			void endContainer() {}

			void clear()
			{
				mPrimitives.clear();
				mPrimitives.shrink_to_fit();
				mContainers.clear();
				mContainers.shrink_to_fit();
			}

			void reserve(u32 nbContainers, PrimitiveIndex nbPrimitives)
			{
				mContainers.reserve(nbContainers);
				mPrimitives.reserve(nbPrimitives);
			}

			void shrinkToFit()
			{
				mPrimitives.shrink_to_fit();
				mContainers.shrink_to_fit();
			}

			[[nodiscard]] u32 getNbContainers() const noexcept { return mContainers.size(); }
			[[nodiscard]] u32 getNbInstances() const noexcept { return mInstances.size(); }

			[[nodiscard]] SubContainerType_ &getContainer(const u32 index) noexcept { plassert(index < mContainers.size()); return mContainers[index]; }
			[[nodiscard]] const SubContainerType_ &getContainer(const u32 index) const noexcept { plassert(index < mContainers.size()); return mContainers[index]; }

			[[nodiscard]] auto &operator [] (const u32 index) noexcept { return derived().getContainer(index); }
			[[nodiscard]] const auto &operator [] (const u32 index) const noexcept { return derived().getContainer(index); }

			[[nodiscard]] auto &getCurrentContainer() noexcept { return mContainers.back(); }
			[[nodiscard]] const auto &getCurrentContainer() const noexcept { return mContainers.back(); }

			[[nodiscard]] const InstanceEntry &getInstance(const u32 index) const noexcept { plassert(mInstances.data()); return mInstances[index]; }

			void addPrimitive(PrimitiveEntry entry)
				{ mPrimitives.push_back(std::move(entry)); }

			template <typename T_>
			void addPrimitive(PrimitiveEntry entry, const T_ &)
				{ derived().addPrimitive(std::move(entry)); }

			void swapPrimitives(const u32 containerIndex, const PrimitiveIndex primIndex1, const PrimitiveIndex primIndex2)
			{
				swap(derived().getPrimitiveIndex(containerIndex, primIndex1), derived().getPrimitiveIndex(containerIndex, primIndex2));
			}

			[[nodiscard]] auto &getPrimitiveEntry(const PrimitiveIndex index) noexcept { return mPrimitives[index]; }
			[[nodiscard]] const auto &getPrimitiveEntry(const PrimitiveIndex index) const noexcept { return mPrimitives[index]; }

			[[nodiscard]] const PrimitiveEntry &getPrimitiveIndex(const u32 containerIndex, const PrimitiveIndex primIndex) const noexcept
			{
				return mPrimitives[derived().getContainer(containerIndex).indexStart + primIndex];
			}

			[[nodiscard]] PrimitiveEntry &getPrimitiveIndex(const u32 containerIndex, const PrimitiveIndex primIndex) noexcept
			{
				return mPrimitives[derived().getContainer(containerIndex).indexStart + primIndex];
			}

			[[nodiscard]] PrimitiveIndex getNbPrimitives() const noexcept { return mPrimitives.size(); }
			[[nodiscard]] PrimitiveIndex getNbPrimitives(const u32 containerIndex) const noexcept
			{
				const auto &container = derived().getContainer(containerIndex);

				return (containerIndex + 1 == mContainers.size() ? mPrimitives.size() : mContainers[containerIndex + 1].indexStart) - container.indexStart;
			}

			[[nodiscard]] CollisionResult traverse(const u32 containerIndex, const BasicRay &ray, Real distmax) const noexcept
			{
				CollisionResult res;

				VecP localOrigin;
				VecD localDir, localDirInv;
				const IObjectInstance *lastInstance = nullptr;
				const auto nbPrims = derived().getNbPrimitives(containerIndex);

				for(const auto i : mir(nbPrims))
				{
					const auto &pEntry = derived().getPrimitiveIndex(containerIndex, i);
					const auto &inst = derived().getInstance(pEntry.iIndex);
					const auto &object = *inst.instance->getObject();

					if(inst.instance != lastInstance)
					{
						localDir = dirSafeTransform(ray.dir, inst.worldToLocal);
						localDirInv = dirReciprocal(localDir);
						localOrigin = inst.worldToLocal * ray.origin;
						lastInstance = inst.instance;
					}

					if(const auto ores = object.intersectExt(pEntry.pIndex, localOrigin, localDir, localDirInv, std::numeric_limits<Real>::infinity(), true); ores)
					{
						const auto &collision = *ores;
						const auto localPos = localOrigin + localDir * collision.dist;
						const auto worldPos = inst.localToWorld * localPos;

						if(const auto colDistSqr = (worldPos - ray.origin) /osdot; colDistSqr < distmax)
						{
							distmax = colDistSqr;

							res.collision.emplace(CollisionData{
									.surface{
										.uv = collision.uv,
										.worldPos = worldPos,
										.localPos = localPos,
										.worldMatrix = inst.localToWorld,
										.inst = inst.instance,
										.objProps = selectPrimGroupProps(object.getClassName(), &object, inst.propMap),
										.minimalSafeInc = pl::plmax<Real>( {localOrigin.minimal_safe_increment(), worldPos.minimal_safe_increment(), object.getMinimalSafeIncrement(pEntry.pIndex)} ),
										.primIndex = pEntry.pIndex,
										.flags{std::make_pair(SurfaceData::EnumFlags::BackfaceHit, collision.backface)}
									},
									.distSqr = colDistSqr
								});
						}
					}
				}

				#ifdef PRT_EXT_STATS
					res.nbColTest += nbPrims;
				#endif

				return res;
			}
		};

		template <typename ObjectType_>
		class SetGeneric : public Set<SetGeneric<ObjectType_>>
		{
		public:
			using PrimitiveType = typename ObjectType_::PrimitiveType;
			using ObjectType = ObjectType_;

		};

		template <typename DerivedType_, typename ObjectType_>
		class SetCached : public PL_CRTP_CLASS_DECLARE(DerivedType_, SetCached<DerivedType_, ObjectType_>, Set)

			using PrimitiveType = typename ObjectType_::PrimitiveType;
			using ObjectType = ObjectType_;

		private:
			ContainerVectorType<PrimitiveType, PrimitiveIndex> mPData;

		public:

			SetCached(std::pmr::memory_resource *memRes = std::pmr::get_default_resource()) :
				crtp_base_type{memRes},
				mPData{typename decltype(mPData)::allocator_type{std::in_place, memRes}}
			{}

			void setMemoryResource(std::pmr::memory_resource *memRes)
			{
				base().setMemoryResource(memRes);

				pl::recreate(mPData,
					typename decltype(mPData)::allocator_type{std::in_place, memRes}
				);
			}

			[[nodiscard]] szt memory_capacity() const noexcept { return base().memory_capacity() + pl::container_memory_capacity(mPData); }

			void fill()
			{
				for(const auto i : mir(derived().getNbInstances()))
				{
					const auto &entry = derived().getInstance(i);
					const auto &object = static_cast<const ObjectType &>(*entry.instance->getObject());

					const auto nb = object.getNbPrimitives();
					mPData.reserve(mPData.size() + nb);

					for(const auto c : mir(nb))
						derived().addPrimitive({c, i}, entry.localToWorld);
				}
			}

			void clear()
			{
				base().clear();
				mPData.clear();
				mPData.shrink_to_fit();
			}

			void shrinkToFit()
			{
				base().shrinkToFit();
				mPData.shrink_to_fit();
			}

			void reserve(szt nbContainers, szt nbPrimitives)
			{
				base().reserve(nbContainers, nbPrimitives);
				mPData.reserve(nbPrimitives);
			}

			void swapPrimitives(const u32 containerIndex, const PrimitiveIndex primIndex1, const PrimitiveIndex primIndex2)
			{
				const auto &container = derived().getContainer(containerIndex);

				base().swapPrimitives(containerIndex, primIndex1, primIndex2);

				swap(mPData[container.indexStart + primIndex1], mPData[container.indexStart + primIndex2]);
			}

			using base_type::addPrimitive;
			void addPrimitive(const PrimitiveEntry entry, const Matrix &matrix)
			{
				auto p = derived().getPrimitive(entry);
				p *= matrix;

				derived().addPrimitive(entry, p);
			}

			void addPrimitive(PrimitiveEntry entry, PrimitiveType p)
			{
				base().addPrimitive(std::move(entry));

				mPData.push_back(std::move(p));
			}

			[[nodiscard]] auto getPrimitive(const PrimitiveEntry entry) const noexcept
			{
				const auto &inst = derived().getInstance(entry.iIndex);
				const auto &obj = static_cast<const ObjectType &>(*inst.instance->getObject());

				return obj.getPrimitive(entry.pIndex);
			}

			[[nodiscard]] auto getPrimitive(const u32 containerIndex, const PrimitiveIndex primIndex) const noexcept
			{
				return derived().getPrimitive( derived().getPrimitiveIndex(containerIndex, primIndex) );
			}

			[[nodiscard]] auto getPrimitiveCached(const u32 containerIndex, const PrimitiveIndex primIndex) const noexcept
			{
				const auto &container = derived().getContainer(containerIndex);

				return mPData[container.indexStart + primIndex];
			}

			[[nodiscard]] CollisionResult traverse(const u32 containerIndex, const BasicRay &ray, Real distmax) const noexcept
			{
				CollisionResult res;
				const auto &container = derived().getContainer(containerIndex);
				const auto nbPrims = derived().getNbPrimitives(containerIndex);

				for(const auto i : mir(nbPrims))
				{
					const auto &pEntry = derived().getPrimitiveIndex(containerIndex, i);
					const auto &inst = derived().getInstance(pEntry.iIndex);
					const auto &object = *inst.instance->getObject();
					const auto &primitive = mPData[container.indexStart + i];

					if(const auto ores = primitive.intersectExt(ray.origin, ray.dir, ray.dirInv, std::numeric_limits<Real>::infinity(), true); ores)
					{
						if(const auto colDistSqr = ores->dist /opow2; colDistSqr < distmax)
						{
							distmax = colDistSqr;
							const auto worldPos = ray.origin + ray.dir * ores->dist;

							res.collision.emplace(CollisionData{
									.surface{
										.uv = ores->uv,
										.worldPos = worldPos,
										.localPos = inst.worldToLocal * worldPos,
										.worldMatrix = inst.localToWorld,
										.inst = inst.instance,
										.objProps = selectPrimGroupProps(object.getClassName(), &object, inst.propMap),
										.minimalSafeInc = pl::plmax<Real>( {ray.origin.minimal_safe_increment(), worldPos.minimal_safe_increment(), primitive.getMinimalSafeIncrement()} ),
										.primIndex = pEntry.pIndex,
										.flags{std::make_pair(SurfaceData::EnumFlags::BackfaceHit, ores->backface)}
									},
									.distSqr = colDistSqr
								});

							plassert(pEntry.pIndex < object.getNbPrimitives());
						}
					}
				}

				#ifdef PRT_EXT_STATS
					res.nbColTest += nbPrims;
				#endif

				return res;
			}
		PL_CRTP_CLASS_END

		struct SubContainerPacked : public SubContainer
		{
			PrimitiveIndex packedDataIndex;
		};

		template <typename DerivedType_, typename PackedType_, typename ObjectType_>
		class SetPrepacked : public PL_CRTP_CLASS_DECLARE(DerivedType_, SetPrepacked<DerivedType_, PackedType_, ObjectType_>, Set, SubContainerPacked)

			using PackedType = PackedType_;
			using PrimitiveType = typename PackedType_::PrimitiveType;
			using ObjectType = ObjectType_;

			using point_type = typename PackedType_::point_type;
			using dir_type = typename PackedType_::dir_type;

			using MaskType = std::bitset<point_type::value_type::NbValues>;

		private:
			ContainerVectorType<PackedType_, PrimitiveIndex> mPackedData;
			//std::vector<PackedType_, AccelStructAllocator<PackedType_>> mPackedData;
			PrimitiveIndex mPackedDataIndex = 0;

		public:

			SetPrepacked(std::pmr::memory_resource *memRes = std::pmr::get_default_resource()) :
				crtp_base_type{memRes},
				mPackedData{typename decltype(mPackedData)::allocator_type{std::in_place, memRes}}
			{}

			void setMemoryResource(std::pmr::memory_resource *memRes)
			{
				base().setMemoryResource(memRes);

				pl::recreate(mPackedData,
					typename decltype(mPackedData)::allocator_type{std::in_place, memRes}
				);
			}

			[[nodiscard]] szt memory_capacity() const noexcept
			{
				return base().memory_capacity() + pl::container_memory_capacity(mPackedData);
			}

			void fill()
			{
				for(const auto i : mir(derived().getNbInstances()))
				{
					const auto &entry = derived().getInstance(i);
					const auto &object = static_cast<const ObjectType &>(*entry.instance->getObject());
					const auto nb = object.getNbPrimitives();
					mPackedData.reserve(mPackedData.size() + szt(nb / point_type::value_type::NbValues) + 1);

					for(const auto c : mir(nb))
						derived().addPrimitive({c, i}, entry.localToWorld);
				}
			}

			void newContainer()
			{
				base().newContainer();
				derived().getCurrentContainer().packedDataIndex = mPackedData.size();
				mPackedData.emplace_back();
				mPackedDataIndex = 0;
			}

			[[nodiscard]] MaskType getLastMask(const PrimitiveIndex nbPrims) const noexcept
			{
				return {
						!nbPrims ? u32{} :
						pl::fill_bits( ((nbPrims - 1) % point_type::value_type::NbValues) + 1, type_v<u32> )
					};
			}

			void clear()
			{
				base().clear();
				mPackedData.clear();
				mPackedData.shrink_to_fit();
				mPackedDataIndex = 0;
			}

			void shrinkToFit()
			{
				base().shrinkToFit();
				mPackedData.shrink_to_fit();
			}

			void reserve(u32 nbContainers, PrimitiveIndex nbPrimitives)
			{
				base().reserve(nbContainers, nbPrimitives);
				mPackedData.reserve(PrimitiveIndex(nbPrimitives / point_type::value_type::NbValues) + 1);
			}

			[[nodiscard]] Real getGeometricCost(const PrimitiveIndex nbPrimitives) const noexcept
			{
				return !nbPrimitives
					?
						0
					:
						Real(PrimitiveIndex((nbPrimitives - 1) / point_type::value_type::NbValues) + 1) * PrimitiveType::IntersectionCost +
						(point_type::value_type::NbValues - (((nbPrimitives - 1) % point_type::value_type::NbValues) + 1));
			}

			[[nodiscard]] const PackedType_ &getPackedData(const PrimitiveIndex index) const noexcept { return mPackedData[index]; }
			[[nodiscard]] PrimitiveIndex getPackedCount() const noexcept { return mPackedData.size(); }

			void swapPrimitives(const u32 containerIndex, const PrimitiveIndex primIndex1, const PrimitiveIndex primIndex2)
			{
				const auto &container = derived().getContainer(containerIndex);

				base().swapPrimitives(containerIndex, primIndex1, primIndex2);

				auto p1 = derived().getPrimitive(containerIndex, primIndex1);
				auto p2 = derived().getPrimitive(containerIndex, primIndex2);

				mPackedData[container.packedDataIndex + PrimitiveIndex(primIndex1 / point_type::value_type::NbValues)].pack(p2, primIndex1 % point_type::value_type::NbValues);
				mPackedData[container.packedDataIndex + PrimitiveIndex(primIndex2 / point_type::value_type::NbValues)].pack(p1, primIndex2 % point_type::value_type::NbValues);
			}

			using base_type::addPrimitive;
			void addPrimitive(const PrimitiveEntry entry, const Matrix &matrix)
			{
				auto p = derived().getPrimitive(entry);
				p *= matrix;

				derived().addPrimitive(entry, p);
			}

			void addPrimitive(const PrimitiveEntry entry, const PrimitiveType &p)
			{
				base().addPrimitive(entry);

				if(mPackedDataIndex >= point_type::value_type::NbValues)
				{
					mPackedDataIndex = 0;
					mPackedData.emplace_back();
				}

				mPackedData.back().pack(p, mPackedDataIndex);

				++mPackedDataIndex;
			}

			[[nodiscard]] auto getPrimitive(const PrimitiveEntry entry) const noexcept
			{
				const auto &inst = derived().getInstance(entry.iIndex);

				const auto &obj = static_cast<const ObjectType &>(*inst.instance->getObject());
				return obj.getPrimitive(entry.pIndex);
			}

			[[nodiscard]] auto getPrimitive(const u32 containerIndex, const PrimitiveIndex primIndex) const noexcept
			{
				return derived().getPrimitive( derived().getPrimitiveIndex(containerIndex, primIndex) );
			}

			[[nodiscard]] auto getPrimitivePacked(const PrimitiveIndex packedIndex, const PrimitiveIndex primIndex) const noexcept
				{ return mPackedData[packedIndex].unpack(primIndex); }

			[[nodiscard]] CollisionResult traverse(const u32 containerIndex, const BasicRay &ray, Real distmax) const noexcept
			{
				CollisionResult res;
				const auto &container = derived().getContainer(containerIndex);

				const auto nbPrims = derived().getNbPrimitives(containerIndex);
				if(!nbPrims)
					return res;

				using PackerP = VecPacker<typename point_type::value_type>;
				using PackerD = VecPacker<typename dir_type::value_type>;

				const point_type packedOrig = PackerP::pack(ray.origin);
				const dir_type packedDir = PackerD::pack(ray.dir);

				const int packedCount = PrimitiveIndex((nbPrims - 1) / point_type::value_type::NbValues) + 1;
				auto foundCollision = derived().findCollision(container, derived().getLastMask(nbPrims), packedCount, packedOrig, packedDir, distmax);

				#ifdef PRT_EXT_STATS
					res.nbColTest += packedCount;
				#endif

				distmax = res.update(std::move(foundCollision.collision), distmax, [&](auto &col)
				{
					const PrimitiveIndex pEntryIndex = ((PrimitiveIndex)foundCollision.packedIndex - container.packedDataIndex) * point_type::value_type::NbValues + (PrimitiveIndex)col.surface.primIndex;

					const PrimitiveIndex peIndex = container.indexStart + pEntryIndex;
					const auto &pEntry = derived().getPrimitiveEntry(peIndex);
					const auto &inst = derived().getInstance(pEntry.iIndex);
					const auto &object = *inst.instance->getObject();
					const auto p = derived().getPrimitivePacked(foundCollision.packedIndex, col.surface.primIndex);

					plassert(pEntry.pIndex < object.getNbPrimitives());

					col.surface.primIndex = pEntry.pIndex;
					col.surface.inst = inst.instance;
					col.surface.objProps = selectPrimGroupProps(object.getClassName(), &object, inst.propMap);
					col.surface.worldPos = ray.origin + ray.dir * (col.distSqr /oprtsqrt);
					col.surface.localPos = inst.worldToLocal * col.surface.worldPos;
					col.surface.worldMatrix = inst.localToWorld;
					col.surface.minimalSafeInc = pl::plmax<Real>( {ray.origin.minimal_safe_increment(), col.surface.worldPos.minimal_safe_increment(), p.getMinimalSafeIncrement()} );

					return true;
				});

				return res;
			}

		protected:

			struct FindcollisionResult
			{
				CollisionResult collision;
				int packedIndex;
			};

			[[nodiscard]] FindcollisionResult findCollision(const SubContainerPacked &container, const MaskType &lastMask, int packedCount, const point_type &packedOrig, const dir_type &packedDir, Real distmax) const noexcept
			{
				FindcollisionResult res;

				for(int i=container.packedDataIndex, last=container.packedDataIndex + packedCount - 1; i<=last; ++i)
				{
					distmax = res.collision.update(
						mPackedData[i].testCollision(
								i == last ? lastMask : MaskType{}.set(),
								packedOrig, packedDir,
								[&](const int, const auto, const auto distSqr)
								{
									if(distSqr < distmax)
									{
										res.packedIndex = i;
										return true;
									}

									return false;
								}
							),
						distmax
					);
				}

				return res;
			}

		PL_CRTP_CLASS_END

		template <typename DerivedType_, typename PackedType_, typename ObjectType_>
		class SetPacked : public PL_CRTP_CLASS_DECLARE(DerivedType_, SetPacked<DerivedType_, PackedType_, ObjectType_>, Set, SubContainer)

			using PackedType = PackedType_;
			using PrimitiveType = typename PackedType_::PrimitiveType;
			using ObjectType = ObjectType_;

			using point_type = typename PackedType_::point_type;
			using dir_type = typename PackedType_::dir_type;

			using MaskType = std::bitset<point_type::value_type::NbValues>;

			void fill()
			{
				for(const auto i : mir(derived().getNbInstances()))
				{
					const auto &entry = derived().getInstance(i);
					const auto &object = static_cast<const ObjectType &>(*entry.instance->getObject());

					for(const auto c : mir(object.getNbPrimitives()))
						derived().addPrimitive({c, i}, entry.localToWorld);
				}
			}

			[[nodiscard]] Real getGeometricCost(const PrimitiveIndex nbPrimitives) const noexcept
				{ return !nbPrimitives ? 0 : Real(PrimitiveIndex((nbPrimitives - 1) / point_type::value_type::NbValues) + 1) * PrimitiveType::IntersectionCost + 1; }

			[[nodiscard]] PrimitiveType getPrimitive(const u32 containerIndex, const PrimitiveIndex primIndex) const noexcept
			{
				const auto &pIndex = derived().getPrimitiveIndex(containerIndex, primIndex);
				const auto &inst = derived().getInstance(pIndex.iIndex);

				const auto &obj = static_cast<const ObjectType &>(*inst.instance->getObject());

				return obj.getPrimitive(pIndex.pIndex);
			}

			[[nodiscard]] CollisionResult traverse(const u32 containerIndex, const BasicRay &ray, Real distmax) const noexcept
			{
				CollisionResult res;
				const auto &container = derived().getContainer(containerIndex);

				const auto nbPrims = derived().getNbPrimitives(containerIndex);
				if(!nbPrims)
					return res;

				PackedType mPackedData;
				dir_type packedDir;
				point_type packedOrig;
				VecP localOrigin;
				VecD localDir;
				PrimitiveIndex packedCount = 0, packedStart = 0;
				MaskType lastMask;
				const IObjectInstance *lastInstance = nullptr;

				using PackerP = VecPacker<typename point_type::value_type>;
				using PackerD = VecPacker<typename dir_type::value_type>;

				const auto testPacked =
					[&](const PrimitiveIndex packStartIndex, const MaskType &mask)
					{
						distmax = res.update(
							mPackedData.testCollision(mask, packedOrig, packedDir, pl::true_pred),
							distmax,
							[&](auto &col)
							{
								const auto pEntryIndex = container.indexStart + packStartIndex + col.surface.primIndex;

								const auto &pEntry = derived().getPrimitiveEntry(pEntryIndex);
								const auto &inst = derived().getInstance(pEntry.iIndex);

								const auto localPos = localOrigin + localDir * col.distSqr;
								const auto worldPos = inst.localToWorld * localPos;
								const auto worldSqrDist = (worldPos - ray.origin) /osdot;

								if(worldSqrDist < distmax)
								{
									const auto &object = static_cast<const ObjectType &>(*inst.instance->getObject());
									const auto p = object.getPrimitive(pEntry.pIndex);

									col.distSqr = worldSqrDist;
									col.surface.inst = inst.instance;
									col.surface.objProps = selectPrimGroupProps(object.getClassName(), &object, inst.propMap);
									col.surface.primIndex = pEntry.pIndex;
									col.surface.localPos = localPos;
									col.surface.worldPos = worldPos;
									col.surface.worldMatrix = inst.localToWorld;
									col.surface.minimalSafeInc = pl::plmax<Real>( {ray.origin.minimal_safe_increment(), col.surface.worldPos.minimal_safe_increment(), p.getMinimalSafeIncrement()} );

									return true;
								}

								return false;
							}
						);
					};

				for(const auto i : mir(nbPrims))
				{
					const PrimitiveIndex pEntryIndex = container.indexStart + i;

					const auto &pEntry = derived().getPrimitiveEntry(pEntryIndex);
					const auto &inst = derived().getInstance(pEntry.iIndex);
					const auto &object = static_cast<const ObjectType &>(*inst.instance->getObject());

					if(lastInstance != inst.instance)
					{
						if(packedCount)
							testPacked(packedStart, lastMask);

						packedCount = 0;
						packedStart = i;
						lastMask.reset();

						localDir = dirSafeTransform(ray.dir, inst.worldToLocal);
						localOrigin = inst.worldToLocal * ray.origin;
						lastInstance = inst.instance;

						packedOrig = PackerP::pack(localOrigin);
						packedDir = PackerD::pack(localDir);
					}

					auto p = object.getPrimitive(pEntry.pIndex);
					lastMask.set(packedCount);
					mPackedData.pack(p, packedCount++);

					if(packedCount == point_type::value_type::NbValues)
					{
						testPacked(packedStart, MaskType{}.set());

						packedCount = 0;
						packedStart = i + 1;
						lastMask.reset();
					}
				}

				if(packedCount)
					testPacked(packedStart, lastMask);

				return res;
			}
		PL_CRTP_CLASS_END

	}



	template <typename ObjectType_> using SetGeneric = impl::SetGeneric<ObjectType_>;
	template <typename ObjectType_> using SetCached = impl::SetCached<void, ObjectType_>;
	template <typename PackedType_, typename ObjectType_> using SetPrepacked = impl::SetPrepacked<void, PackedType_, ObjectType_>;
	template <typename PackedType_, typename ObjectType_> using SetPacked = impl::SetPacked<void, PackedType_, ObjectType_>;
}
