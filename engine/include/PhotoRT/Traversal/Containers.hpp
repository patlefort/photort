/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/AccelStruct/AccelStructBVH.hpp>
#include <PhotoRT/Core/Intersection.hpp>

#include "ContainerSet.hpp"

#include <patlib/masked_container_array.hpp>

namespace PhotoRT::Traversal
{
	template <typename T_>
	using PrimitiveContainerFactoryType = pl::GenericFactoryPolymorphicAllocator< pl::GenericSmartFactoryAlloc, EntityContainer, T_ >;


	template <typename ObjectClass_>
	class EntityContainerGeneric : public EntityContainer, public pl::Cloneable<EntityContainer, EntityContainerGeneric<ObjectClass_>>, public IPrimitiveList
	{
	public:

		using EntityContainer::EntityContainer;

		using ObjectClass = ObjectClass_;

		struct ObjectEntry
		{
			Box aabox;
			const IObjectInstance *inst;
			Matrix worldMatrix, worldToLocalMatrix;
			PrimGroupPropertiesMapPtr propMap;
		};

		Box mAabox{pl::tag::nan};
		VectorWithStatsPMR<ObjectEntry, memtag::AccelStruct> mObjects;

		virtual EntityContainer &operator= (const EntityContainer &o) override
		{
			return (*this = static_cast<const EntityContainerGeneric &>(o));
		}

		virtual EntityContainer &operator= (EntityContainer &&o) noexcept override
		{
			return (*this = static_cast<EntityContainerGeneric &&>(o));
		}

		virtual void setMemoryResource(std::pmr::memory_resource *mr, std::pmr::memory_resource *monomr) override
		{
			EntityContainer::setMemoryResource(mr, monomr);

			pl::recreate(mObjects, typename decltype(mObjects)::allocator_type{std::in_place, mr});
		}

		virtual void finalize() override
		{
			mObjects.shrink_to_fit();
		}

		virtual void attachToInstance(const IObjectInstance &inst) override
		{
			mInstance = &inst;

			mObjects.clear();

			traverseRecursiveProperties(inst, [this](const IObjectInstance &inst, PrimGroupPropertiesMapPtr &newPropMap, const Matrix &curMatrix, const Matrix &curMatrixInv){
				if(const auto object = pl::point_to_const_cast(std::to_address(inst.getObject()));
					object && object->getType() == ObjectClass_::PrimitiveID && object->getNbPrimitives())
				{
					mObjects.reserve(1024);
					auto &entry = mObjects.emplace_back();

					entry.worldMatrix = curMatrix;
					entry.worldToLocalMatrix = curMatrixInv;
					entry.inst = &inst;
					entry.propMap = newPropMap;
				}

				return TraverseRecursiveState::Continue;
			});

			mObjects.shrink_to_fit();

			mAabox = rg::accumulate(
					mObjects,
					Box{pl::tag::inflowest},
					pl::accumulator_join,
					[&](auto &o){
						const auto b = o.inst->getObject()->getAABox();

						return (o.aabox = pl::plisinf(b) ? b : pl::assert_valid(b * o.worldMatrix));
					}
				);

			if(!pl::plisinf(mAabox))
				mAabox = safeIncrement(mAabox);
		}

		virtual void clearContainer() override
		{
			mObjects.clear();
		}

		[[nodiscard]] virtual Real judgeCost() const override
			{ return mObjects.size(); }

		[[nodiscard]] virtual Real judgeGeometryCost() const override
			{ return mObjects.size(); }

		virtual void dumpInfo(std::ostream &outputStream, if32 level) const override
		{
			EntityContainer::dumpInfo(outputStream, level);

			++level;

			outputStream << pl::io::indent(level) << "Nb: " << mObjects.size() << '\n';
			//outputStream << pl::io::indent(level) << "Type ID: " << ObjectClass::PrimitiveID << '\n';
		}

		[[nodiscard]] virtual PrimitiveIndex getNbPrimitives() const noexcept override { return mObjects.size(); }

		[[nodiscard]] virtual Box getAABox() const noexcept override
			{ return mAabox; }

		[[nodiscard]] virtual CollisionResult traverse(const BasicRay &ray, Real distmax) const noexcept override
		{
			CollisionResult res;

			for(const auto &entry : mObjects)
				distmax = res.update(traverseObject(entry, ray, distmax), distmax);

			return res;
		}

	protected:

		[[nodiscard]] CollisionResult traverseObject(const ObjectEntry &entry, const BasicRay &ray, Real distmax) const noexcept
		{
			CollisionResult res;

			const auto localOrigin = entry.worldToLocalMatrix * ray.origin;
			const auto localDir = dirSafeTransform(ray.dir, entry.worldToLocalMatrix);
			const auto localDirInv = dirReciprocal(localDir);

			const auto &object = *entry.inst->getObject();
			const auto [ores, nbt] = object.intersectExt(localOrigin, localDir, localDirInv, std::numeric_limits<Real>::infinity(), true);

			#ifdef PRT_EXT_STATS
				res.nbColTest += nbt;
			#endif

			if(ores)
			{
				const auto &collision = *ores;
				//distmax = collision.dist;
				const auto localPos = localOrigin + localDir * collision.dist;
				const auto worldPos = entry.worldMatrix * localPos;

				if(const auto colDistSqr = (worldPos - ray.origin) /osdot; colDistSqr < distmax)
				{
					distmax = colDistSqr;

					res.collision.emplace(CollisionData{
							.surface{
								.uv = collision.uv,
								.worldPos = worldPos,
								.localPos = localPos,
								.worldMatrix = entry.worldMatrix,
								.inst = entry.inst,
								.objProps = selectPrimGroupProps(object.getClassName(), &object, entry.propMap),
								.minimalSafeInc = pl::plmax<Real>( {localOrigin.minimal_safe_increment(), worldPos.minimal_safe_increment(), object.getMinimalSafeIncrement(collision.primIndex)} ),
								.primIndex = collision.primIndex,
								.flags{std::make_pair(SurfaceData::EnumFlags::BackfaceHit, collision.backface)}
							},
							.distSqr = colDistSqr
						});
				}
			}

			return res;
		}

	};


	template <typename T_>
	class EntityContainerCached : public EntityContainer, public pl::Cloneable<EntityContainer, EntityContainerCached<T_>>
	{
	public:

		using EntityContainer::EntityContainer;

		EntityContainerCached(const EntityContainerCached &o)
		{
			*this = o;
		}
		EntityContainerCached(EntityContainerCached &&) = default;

		EntityContainerCached &operator=(const EntityContainerCached &o)
		{
			mInstances = o.mInstances;
			mPackedData = o.mPackedData;
			mPackedData.setInstanceList(mInstances);
			mAabox = o.mAabox;

			return *this;
		}
		EntityContainerCached &operator=(EntityContainerCached &&) = default;

		virtual EntityContainer &operator= (const EntityContainer &o) override
		{
			return (*this = static_cast<const EntityContainerCached &>(o));
		}

		virtual EntityContainer &operator= (EntityContainer &&o) noexcept override
		{
			return (*this = static_cast<EntityContainerCached &&>(o));
		}

		VectorWithStatsPMR<InstanceEntry, memtag::AccelStruct> mInstances;
		T_ mPackedData;
		Box mAabox{pl::tag::nan};

		virtual void setMemoryResource(std::pmr::memory_resource *mr, std::pmr::memory_resource *monomr) override
		{
			EntityContainer::setMemoryResource(mr, monomr);

			pl::recreate(mInstances, typename decltype(mInstances)::allocator_type{std::in_place, mr});

			mPackedData.setMemoryResource(mr);
			mPackedData.newContainer();
		}

		virtual void dumpInfo(std::ostream &outputStream, if32 level) const override
		{
			EntityContainer::dumpInfo(outputStream, level);

			++level;

			outputStream << pl::io::indent(level) << "Nb prim: " << mPackedData.getNbPrimitives() << '\n';
			//outputStream << pl::io::indent(level) << "Nb packed prim: " << mPackedData.getPackedCount() << '\n';
		}

		virtual void clearContainer() override
		{
			mPackedData.clear();
		}

		[[nodiscard]] virtual PrimitiveIndex getNbPrimitives() const override
		{
			return mPackedData.getNbPrimitives();
		}

		[[nodiscard]] virtual Real judgeCost() const override
		{
			return judgeGeometryCost();
		}

		[[nodiscard]] virtual Real judgeGeometryCost() const override
		{
			return mPackedData.getGeometricCost(0);
		}

		virtual void finalize() override
		{
			mPackedData.endContainer();
			mPackedData.shrinkToFit();
		}

		[[nodiscard]] virtual Box getAABox() const noexcept override
		{
			return mAabox;
		}

		[[nodiscard]] virtual CollisionResult traverse(const BasicRay &ray, Real distmax) const noexcept override
		{
			return mPackedData.traverse(0, ray, distmax);
		}

		virtual void attachToInstance(const IObjectInstance &inst) override
		{
			mInstance = &inst;

			traverseRecursiveProperties(inst, [this](const IObjectInstance &inst, PrimGroupPropertiesMapPtr &newPropMap, const Matrix &curMatrix, const Matrix &curMatrixInv){
				if(const auto object = pl::point_to_const_cast(std::to_address(inst.getObject()));
					object && object->getType() == T_::ObjectType::PrimitiveID && object->getNbPrimitives())
				{
					mInstances.reserve(128);
					auto &entry = mInstances.emplace_back();
					entry.instance = &inst;
					entry.localToWorld = curMatrix;
					entry.worldToLocal = curMatrixInv;
					entry.propMap = newPropMap;
				}

				return TraverseRecursiveState::Continue;
			});

			mInstances.shrink_to_fit();
			mPackedData.setInstanceList(mInstances);
			mPackedData.fill();

			mAabox = rg::accumulate(
				mInstances,
				Box{pl::tag::inflowest},
				pl::accumulator_join,
				[&](const auto &i){ return pl::assert_valid(i.instance->getObject()->getAABox() * i.localToWorld); }
			);
		}

	};


	template <typename ContainerSetType_, typename BoundaryEntryType_, typename IndexType_>
	class AccelStructLeafContainerPrimitives
	{
	public:

		using ContainerSetType = ContainerSetType_;
		using ObjectType = typename ContainerSetType_::ObjectType;
		using BoundaryEntryType = BoundaryEntryType_;
		using IndexType = IndexType_;

		template <typename T_, szt> using BranchListVector = std::vector<T_>;
		using BranchListType = pl::masked_container_array<ContainerSetType_, IndexType, 10, BranchListVector>;

		BranchListType mContainerSet;
		IndexType mNbBranchesPerCore = 1;
		std::array<IndexType, BranchListType::NbBranches> mBranchIndexes;
		std::pmr::memory_resource *mMemRes;

		VectorWithStatsPMR<InstanceEntry, memtag::AccelStruct> mInstances;
		bool mDoClipping = true;

		IndexType mReserveSizeP, mReserveSizeC;

	private:
		IndexType mNbPrimitivesInLeaves = 0, mNbLeavesTotal = 0, mNbPrimitives = 0;

	public:

		AccelStructLeafContainerPrimitives(std::pmr::memory_resource *memRes = std::pmr::get_default_resource()) :
			mMemRes{memRes}, mInstances{typename decltype(mInstances)::allocator_type{std::in_place, memRes}} {}

		AccelStructLeafContainerPrimitives(const AccelStructLeafContainerPrimitives &o) :
			mContainerSet{o.mContainerSet},
			mMemRes{o.mMemRes},
			mInstances{o.mInstances},
			mDoClipping{o.mDoClipping},
			mNbPrimitivesInLeaves{o.mNbPrimitivesInLeaves},
			mNbLeavesTotal{o.mNbLeavesTotal},
			mNbPrimitives{o.mNbPrimitives}
		{
			for(auto &c : mContainerSet)
				c.setInstanceList(mInstances);
		}

		AccelStructLeafContainerPrimitives(AccelStructLeafContainerPrimitives &&o) = default;

		AccelStructLeafContainerPrimitives &operator=(const AccelStructLeafContainerPrimitives &o)
		{
			mContainerSet = o.mContainerSet;
			mInstances = o.mInstances;
			mDoClipping = o.mDoClipping;
			mNbPrimitivesInLeaves = o.mNbPrimitivesInLeaves;
			mNbLeavesTotal = o.mNbLeavesTotal;
			mNbPrimitives = o.mNbPrimitives;
			mMemRes = o.mMemRes;

			for(auto &c : mContainerSet)
				c.setInstanceList(mInstances);

			return *this;
		}
		AccelStructLeafContainerPrimitives &operator=(AccelStructLeafContainerPrimitives &&o) = default;

		void setMemoryResource(std::pmr::memory_resource *memRes)
		{
			mMemRes = memRes;
			pl::recreate(mInstances, typename decltype(mInstances)::allocator_type{std::in_place, memRes});

			for(auto &c : mContainerSet)
				c.setMemoryResource(memRes);
		}

		[[nodiscard]] IndexType getNbPrimitives() const noexcept { return mNbPrimitives; }
		[[nodiscard]] IndexType getNbPrimitivesInLeaves() const noexcept { return mNbPrimitivesInLeaves; }
		[[nodiscard]] IndexType getNbPrimitives(IndexType index) const noexcept { const auto brIndex = mContainerSet.build_index(index); return mContainerSet.m_branches[brIndex.low()].getNbPrimitives(brIndex.high()); }

		[[nodiscard]] bool leafIndexIsValid(IndexType index) const noexcept
			{ const auto brIndex = mContainerSet.build_index(index); return brIndex.high() < mContainerSet.m_branches[brIndex.low()].getNbContainers(); }

		[[nodiscard]] szt memory_capacity() const noexcept
		{
			return rg::accumulate(
					mContainerSet, sizeof(mContainerSet) + pl::container_memory_capacity(mInstances), rg::plus{},
					[](const auto &c){ return c.memory_capacity(); }
				);
		}

		void finalize()
		{
			mInstances.shrink_to_fit();

			mNbPrimitivesInLeaves = 0;
			mNbLeavesTotal = 0;
			for(auto &c : mContainerSet)
			{
				c.setInstanceList(mInstances);
				c.shrinkToFit();
				mNbPrimitivesInLeaves += c.getNbPrimitives();
				mNbLeavesTotal += c.getNbContainers();
			}
		}

		void clear()
		{
			for(auto &c : mContainerSet)
				c.clear();

			mInstances.clear();
			mInstances.shrink_to_fit();

			mNbLeavesTotal = 0;
			mNbPrimitivesInLeaves = 0;
			mNbPrimitives = 0;
		}

		void prepare(Real targetCost, IndexType nbThreads)
		{
			mContainerSet.m_branches.resize(BranchListType::NbBranches, ContainerSetType_{mMemRes});

			for(auto &c : mContainerSet)
				c.setInstanceList(mInstances);

			mNbBranchesPerCore = mContainerSet.NbBranches / nbThreads;
			mBranchIndexes | pl::zero_range;

			const auto primitiveCost = getGeometricCost(1);
			const auto nb = pl::plmax<IndexType>(1, pl::plmax<Real>(targetCost, primitiveCost) / primitiveCost);

			mReserveSizeP = mNbPrimitives / mContainerSet.NbBranches;
			mReserveSizeC = mReserveSizeP / nb;
		}

		void addInstance(const IObjectInstance &inst, PrimGroupPropertiesMapPtr &newPropMap, const Matrix &curMatrix, const Matrix &invMatrix)
		{
			mInstances.reserve(128);

			auto &entry = mInstances.emplace_back();
			entry.instance = &inst;
			entry.localToWorld = curMatrix;
			entry.worldToLocal = invMatrix;
			entry.propMap = newPropMap;

			mNbPrimitives += inst.getObject()->getNbPrimitives();
		}

		[[nodiscard]] IndexType getNbLeaves() const noexcept { return mNbLeavesTotal; }

		IndexType createLeaf(const auto &boundaries, IndexType threadNo)
		{
			auto branchIndex = threadNo * mNbBranchesPerCore + mBranchIndexes[threadNo];

			if(mContainerSet.branch(branchIndex).getNbContainers() == mContainerSet.MaxSize)
			{
				++mBranchIndexes[threadNo];
				if(mBranchIndexes[threadNo] == mNbBranchesPerCore)
					throw std::bad_alloc();
				++branchIndex;
			}

			auto &branch = mContainerSet.branch(branchIndex);

			if(!branch.getNbContainers())
				branch.reserve(mReserveSizeC, mReserveSizeP);

			branch.newContainer();

			for(const auto &e : boundaries)
			{
				const auto &inst = mInstances[e.getIndex().iIndex];
				branch.addPrimitive(e.getIndex(), inst.localToWorld);
			}

			branch.endContainer();

			const auto index = mContainerSet.build_index(branchIndex, branch.getNbContainers() - 1);
			plassert(index.low() < mContainerSet.m_branches.size());
			plassert(index.high() < branch.getNbContainers());

			return index.index();
		}

		[[nodiscard]] auto getPrimitive(const ObjectType &obj, const IndexType primIndex) const noexcept
			{ return obj.getPrimitiveMinimal(primIndex); }

		[[nodiscard]] auto createPrimitiveBoundaryList() const
		{
			#ifdef PL_LIB_HUGEPAGES
				std::pmr::vector<BoundaryEntryType> boundaries{mNbPrimitives, {&pl::hugepages_resource()}};
			#else
				std::vector<BoundaryEntryType> boundaries{mNbPrimitives};
			#endif

			if(boundaries.empty())
				return boundaries;

			for(IndexType indexStart = 0; const auto i : mir((IndexType)mInstances.size()))
			{
				const auto &instance = mInstances[i];
				const auto &obj = static_cast<const ObjectType &>(*instance.instance->getObject());
				const auto nbp = obj.getNbPrimitives();

				mir(nbp) | pl::for_each([&](const auto c)
				{
					auto p = getPrimitive(obj, c);
					p *= instance.localToWorld;

					auto &b = boundaries[indexStart + c];
					b.setBounds( pl::assert_valid(p.getAABox()) );
					b.setIndex({c, i});
				}, std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128));

				indexStart += nbp;
			}

			//std::cout << pl::io::range{pl::io::mode_text, '\n', boundaries | rgv::transform([&](const auto &b){ return b.getBounds(); })} << '\n';

			return boundaries;
		}

		void clipBoundaryList(auto&& boundaries, const auto &box, IndexType clipAxis, bool leftClip)
		{
			if(rg::empty(boundaries) || !mDoClipping)
				return;

			const auto clipPosition = leftClip ? box.v1[clipAxis] : box.v2[clipAxis];

			boundaries | pl::for_each([&](auto &b)
			{
				const auto &index = b.getIndex();
				const auto &inst = mInstances[index.iIndex];
				const auto &obj = static_cast<const ObjectType &>(*inst.instance->getObject());

				auto p = getPrimitive(obj, index.pIndex);
				p *= inst.localToWorld;

				b.setBounds( p.clipBoxToPlane(b.getBounds(), clipPosition, clipAxis, leftClip) );
			}, std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*16));
		}

		[[nodiscard]] auto getGeometricCost(IndexType nbPrimitives) const noexcept
		{
			return mContainerSet.m_branches[0].getGeometricCost(nbPrimitives);
		}
	};


	template <typename ContainerSetType_, typename IndexType_ = PrimitiveIndex>
	class EntityContainerBVH : public EntityContainer, public pl::Cloneable<EntityContainer, EntityContainerBVH<ContainerSetType_, IndexType_>>
	{
	public:
		using ContainerSetType = ContainerSetType_;
		using ObjectType = typename ContainerSetType_::ObjectType;

		using EntityContainer::EntityContainer;

		virtual EntityContainer &operator= (const EntityContainer &o) override
		{
			return (*this = static_cast<const EntityContainerBVH &>(o));
		}

		virtual EntityContainer &operator= (EntityContainer &&o) noexcept override
		{
			return (*this = static_cast<EntityContainerBVH &&>(o));
		}

		virtual void setMemoryResource(std::pmr::memory_resource *mr, std::pmr::memory_resource *monomr) override
		{
			EntityContainer::setMemoryResource(mr, monomr);

			mLeafContainer.setMemoryResource(mr);
		}

		virtual void setParameters(const ParameterTree &params) override
		{
			EntityContainer::setParameters(params);

			params.getValueIfExists("max_depth",      mOptions.maxDepth);
			params.getValueIfExists("target_cost",    mOptions.targetCost);
			params.getValueIfExists("sah_at_depth",   mOptions.useSahAtDepth);
			params.getValueIfExists("nb_sah_tests",   mOptions.maxSahTests);
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {EntityContainer::getParameters(deep)};

			params["max_depth"] =     mOptions.maxDepth;
			params["target_cost"] =   mOptions.targetCost;
			params["sah_at_depth"] =  mOptions.useSahAtDepth;
			params["nb_sah_tests"] =  mOptions.maxSahTests;

			return params;
		}

		virtual void dumpInfo(std::ostream &outputStream, if32 level) const override
		{
			EntityContainer::dumpInfo(outputStream, level);

			++level;

			outputStream << pl::io::indent(level) << "AABB: " << getAABox() << '\n';
			//mBvh.dump(outputStream, level);
		}

		virtual void clearContainer() override
		{
			mBvhData.reset();
			mLeafContainer.clear();
		}

		[[nodiscard]] virtual PrimitiveIndex getNbPrimitives() const override
		{
			return mLeafContainer.getNbPrimitivesInLeaves();
		}

		[[nodiscard]] virtual Real judgeCost() const override
		{
			return judgeGeometryCost();
		}

		[[nodiscard]] virtual Real judgeGeometryCost() const override
		{
			return 0;
		}

		virtual void finalize() override
		{
			mBvhData.reset();

			if(mLeafContainer.getNbPrimitives())
			{
				{
					BuilderType bvhBuilder{mLeafContainer, mOptions,
							{},
							#ifdef PL_LIB_HUGEPAGES
								{std::in_place, &pl::hugepages_resource()},
							#else
								{},
							#endif
							{std::in_place, mMemRes}
						};

					bvhBuilder.build(mLeafContainer.createPrimitiveBoundaryList(), mAabox);
					auto data = bvhBuilder.compile();

					#ifndef NDEBUG
						bvhBuilder.template validate<VecP>(mAabox, data);
					#endif

					mBvhData.emplace(std::move(data));
				}

				mLeafContainer.finalize();
			}
		}

		[[nodiscard]] virtual Box getAABox() const noexcept override
		{
			return mAabox;
		}

		[[nodiscard]] virtual CollisionResult traverse(const BasicRay &ray, Real distmax) const noexcept override
		{
			CollisionResult res;

			#ifdef PRT_EXT_STATS
				++res.nbAABBTest;
			#endif

			PRT_SCOPED_PROFILE(Profiler::raytrace, Profiler::tag::AccelStruct);

			const auto [mask, bdistmin, bdistmax] = Intersection::testAABBMinMax(mAabox.v1, mAabox.v2, ray.dirInv, ray.origin);

			if(mask.any())
			{
				AccelStruct::BVH<typename BuilderType::IndexType, VecP>
					bvh{ mBvhData->mNodes, mBvhData->mRootNode };

				bvh.traverse(ray.origin, ray.dir, ray.dirInv, bdistmin, bdistmax, distmax,
					[&](auto index, Real /*dmin*/, Real /*dmax*/, Real /*colDistSqr*/)
					{
						const auto brIndex = mLeafContainer.mContainerSet.build_index(index);

						distmax = res.update(
							mLeafContainer.mContainerSet.m_branches[brIndex.low()].traverse(brIndex.high(), ray, distmax),
							distmax
						);

						return distmax;
					}
				);
			}

			return res;
		}

		virtual void attachToInstance(const IObjectInstance &inst) override
		{
			mInstance = &inst;

			traverseRecursiveProperties(inst, [this](const IObjectInstance &inst, PrimGroupPropertiesMapPtr &newPropMap, const Matrix &curMatrix, const Matrix &curMatrixInv)
			{
				if(const auto object = pl::point_to_const_cast(std::to_address(inst.getObject()));
						object && object->getType() == ObjectType::PrimitiveID && object->getNbPrimitives())
				{
					if(object->isInfinite())
						throw Exception("Infinite primitives can't be used with a BVH.");

					mLeafContainer.addInstance(inst, newPropMap, curMatrix, curMatrixInv);
				}

				return TraverseRecursiveState::Continue;
			});

			if(!mLeafContainer.mInstances.empty())
			{
				mAabox = rg::accumulate(
					mLeafContainer.mInstances,
					Box{pl::tag::inflowest},
					pl::accumulator_join,
					[&](const auto &i){
						return pl::assert_valid(i.instance->getObject()->getAABox() * pl::assert_valid(i.localToWorld));
					}
				);

				pl::assert_valid(mAabox);
			}
			else
				mAabox.set_inflowest();
		}

	protected:

		using LeafContainerType = AccelStructLeafContainerPrimitives<ContainerSetType_, AccelStruct::BoundaryEntryMiddle<PrimitiveEntry, Point3Dr>, IndexType_>;
		using BuilderType = AccelStruct::BVHBuilderSAH<LeafContainerType, IndexType_, Point3Dr, ContainerVectorType, FinalContainerVectorType>;

		Box mAabox{pl::tag::nan};
		LeafContainerType mLeafContainer;
		pl::uninitialized_value<typename BuilderType::DataType> mBvhData;
		typename BuilderType::BuildOptions mOptions;
	};

}
