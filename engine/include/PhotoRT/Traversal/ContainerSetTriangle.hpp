/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>
#include "ContainerSet.hpp"
#include <PhotoRT/PrimitiveGroup/Triangle.hpp>

namespace PhotoRT::Traversal
{
	#ifdef PRT_FEATURE_PACKED_VECTOR

		struct PackedTriangle
		{
			using PrimitiveType = PrimitiveTriangle<>;
			static constexpr auto NbAxis = PrimitiveType::point_type::category::NbAxis;
			using StorageType = std::array<PackedVec::aligned_array_type, NbAxis>;
			using Packer = VecPackerAligned<StorageType::value_type>;
			using point_type = pl::simd::basic_geometric_vector<PackedVec, size_c<NbAxis>, pl::simd::categories::geometric_point_t, NbAxis>;
			using dir_type = pl::simd::basic_geometric_vector<PackedVec, size_c<NbAxis>, pl::simd::categories::geometric_dir_t, NbAxis>;

			std::array<StorageType, 3> mP;

			[[nodiscard]] CollisionResult testCollision(std::bitset<point_type::value_type::NbValues> mask, const point_type &packedOrig, const dir_type &packedDir, auto&& checkFc) const noexcept
			{
				CollisionResult res;

				const auto [resMask, tu, tv, tdist, bf] = Intersection::testTriangle(
						packedOrig, packedDir,
						point_type{pl::tag::array_init, mP[0].data()},
						dir_type{pl::tag::array_init, mP[1].data()},
						dir_type{pl::tag::array_init, mP[2].data()}
					);

				mask &= resMask;

				if(mask.none())
					return res;

				const auto [index, cdist] = plmin_index(tdist, mask);
				const auto distSqr = cdist /opow2;
				if(index < 0 || !PL_FWD(checkFc)(index, cdist, distSqr))
					return res;

				res.collision.emplace();
				res.collision->distSqr = distSqr;
				res.collision->surface.isBackfaceHit(bf[index]);
				res.collision->surface.uv = {tu[index], tv[index], 0};
				res.collision->surface.primIndex = index;

				return res;
			}

			template <typename... Ts_>
			void pack(const ImplPrimitives::PrimitiveTriangle<Ts_...> &t, uf32 index) noexcept
			{
				Packer::pack(t.mP[0], mP[0], index);
				Packer::pack(t.mP[1] - t.mP[0], mP[1], index);
				Packer::pack(t.mP[2] - t.mP[0], mP[2], index);
			}

			[[nodiscard]] PrimitiveType unpack(uf32 index) const noexcept
			{
				const auto p1 = Packer::unpack<VecP>(mP[0], index);

				return {
					p1,
					p1 + Packer::unpack<VecD>(mP[1], index),
					p1 + Packer::unpack<VecD>(mP[2], index)
				};
			}
		};

		namespace impl
		{
			template <typename DerivedType_ = void>
			class SetPrepackedTriangles : public PL_CRTP_CLASS_DECLARE(DerivedType_, SetPrepackedTriangles<DerivedType_>, SetPrepacked, PackedTriangle, PrimGroupTriangles)

				using ObjectType = typename base_type::ObjectType;

				using base_type::getPrimitive;
				[[nodiscard]] auto getPrimitive(const PrimitiveEntry entry) const noexcept
				{
					const auto &inst = derived().getInstance(entry.iIndex);
					const auto &obj = static_cast<const ObjectType &>(*inst.instance->getObject());

					return obj.getTriangle(entry.pIndex);
				}

			PL_CRTP_CLASS_END
		} // namespace impl

		using SetPrepackedTriangles = impl::SetPrepackedTriangles<>;

	#endif

	PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("container_factory-triangle_generic", Augmented_container_factory__triangle_generic, PrimitiveContainerFactoryType<EntityContainerGeneric<PrimGroupTriangles>>)
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("container_factory-triangle_generic_cached", Augmented_container_factory__triangle_generic_cached, PrimitiveContainerFactoryType<EntityContainerCached<SetCached<PrimGroupTriangles>>>)

	#ifdef PRT_FEATURE_PACKED_VECTOR
		PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("container_factory-triangle_prepacked", Augmented_container_factory__triangle_prepacked, PrimitiveContainerFactoryType<EntityContainerCached<SetPrepackedTriangles>>)
		PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("container_factory-triangle_bvh_cached", Augmented_container_factory__triangle_bvh_cached, PrimitiveContainerFactoryType<EntityContainerBVH<SetPrepackedTriangles>>)
		PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("container_factory-triangle_bvh_generic", Augmented_container_factory__triangle_bvh_generic, PrimitiveContainerFactoryType<EntityContainerBVH<SetPacked<PackedTriangle, PrimGroupTriangles>>>)
	#else
		PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("container_factory-triangle_bvh_cached", Augmented_container_factory__triangle_bvh_cached, PrimitiveContainerFactoryType<EntityContainerBVH<SetCached<PrimGroupTriangles>>>)
		PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("container_factory-triangle_bvh_generic", Augmented_container_factory__triangle_bvh_generic, PrimitiveContainerFactoryType<EntityContainerBVH<SetGeneric<PrimGroupTriangles>>>)
	#endif
}