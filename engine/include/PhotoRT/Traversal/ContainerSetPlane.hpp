/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>
#include "ContainerSet.hpp"
#include <PhotoRT/PrimitiveGroup/Plane.hpp>

namespace PhotoRT::Traversal
{
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("container_factory-plane_generic", Augmented_container_factory__plane_generic, PrimitiveContainerFactoryType<EntityContainerGeneric<PrimGroupPlanes>>)
}