/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Camera.hpp>
#include "Lens.hpp"

namespace PhotoRT
{
	class PHOTORT_API CameraControllerSingle : public CameraControllerBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("cameractl-single", CameraControllerSingle)

		void setCamera(EntityPtr<ICamera> camera) noexcept { mCamera = std::move(camera); mCameras.resize(1); mCameras[0] = mCamera; }
		[[nodiscard]] const auto &getCamera() const noexcept { return mCamera; }

		virtual void setFocus(Real dist) override { if(mCamera) mCamera->setFocus(dist); }
		virtual void setFocusPos(VecP pos) override { if(mCamera) mCamera->setFocusPos(pos); }
		virtual Ray getFocusDir(Point2Dr pos) const noexcept override { if(mCamera) return mCamera->getFocusDir(pos); return {}; }
		[[nodiscard]] virtual Real getFocus() const noexcept override { return mCamera ? mCamera->getFocus() : 0; }

		virtual void setAspectRatio(Real aspect) override { if(mCamera) mCamera->setAspectRatio(aspect); }
		virtual void setFilmSize(Dir2Dr s) override { if(mCamera) mCamera->setFilmSize(s); }
		[[nodiscard]] virtual Dir2Dr getFilmSize() const noexcept override { return mCamera ? mCamera->getFilmSize() : 0; }
		[[nodiscard]] virtual Lobe<> getCameraViewLobe() const noexcept override { return mCamera ? mCamera->getCameraViewLobe() : Lobe<>{}; }

		[[nodiscard]] virtual Real getAspectRatio() const noexcept override { return mCamera ? mCamera->getAspectRatio() : 0; }

		virtual void update() override;

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:
		EntityPtr<ICamera> mCamera;
	};

	class PHOTORT_API CameraController2Eyes : public CameraControllerBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("cameractl-2eyes", CameraController2Eyes)

		enum class CameraEyes : szt
		{
			LeftEye = 0,
			RightEye = 1
		};

		CameraController2Eyes();

		void setCamera(EntityPtr<ICamera> camera, CameraEyes eye) noexcept { mCamera[(szt)eye] = std::move(camera); mCameras.resize(2); mCameras[(szt)eye] = mCamera[(szt)eye]; }
		[[nodiscard]] const auto &getCamera(CameraEyes eye) const noexcept { return mCamera[(szt)eye]; }

		void setEyeDistance(Real dist) { mEyeDistance = dist; }
		[[nodiscard]] Real getEyeDistance() const noexcept { return mEyeDistance; }

		virtual void setFocus(Real dist) override { mReferenceCamera.setFocus(dist); }
		virtual void setFocusPos(VecP pos) override { mReferenceCamera.setFocusPos(pos); }
		virtual Ray getFocusDir(Point2Dr pos) const noexcept override { return mReferenceCamera.getFocusDir(pos); }
		[[nodiscard]] virtual Real getFocus() const noexcept override { return mReferenceCamera.getFocus(); }

		virtual void setAspectRatio(Real aspect) override;
		virtual void setFilmSize(Dir2Dr s) override;
		[[nodiscard]] virtual Dir2Dr getFilmSize() const noexcept override { return mReferenceCamera.getFilmSize(); }
		[[nodiscard]] virtual Lobe<> getCameraViewLobe() const noexcept override { return mReferenceCamera.getCameraViewLobe(); }

		[[nodiscard]] virtual Real getAspectRatio() const noexcept override { return mReferenceCamera.getAspectRatio(); }

		virtual void update() override;

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:
		std::array<EntityPtr<ICamera>, 2> mCamera;
		CameraLens mReferenceCamera;
		Real mEyeDistance = 1;

	};

}
