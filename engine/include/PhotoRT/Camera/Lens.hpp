/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/PrimitiveGroup_fwd.hpp>

#include "Pinhole.hpp"

#include <PhotoRT/Core/Distribution.hpp>

#include <patlib/marked_optional.hpp>

namespace PhotoRT
{
	class ILens
	{
	public:

		virtual ~ILens() = default;

		[[nodiscard]] virtual OptionalPDF<Real> PDF(const MaterialInteraction &lensSurface) const noexcept = 0;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(const Matrix &m, const MaterialInteraction &lensSurface) const noexcept = 0;
		[[nodiscard]] virtual pl::marked_optional<SampledSensor> sampleArea(RandomReals<2> p) const noexcept = 0;
		[[nodiscard]] virtual Real intersect(VecP origin, VecD dir, VecD dirInv) const noexcept = 0;
		[[nodiscard]] virtual bool hasArea() const noexcept = 0;
		virtual void update() = 0;
	};

	class PHOTORT_API LensCircle : public ILens, public RenderEntity
	{
	protected:
		[[nodiscard]] auto diskPdf() const noexcept { return pl::reciprocal(std::numbers::pi_v<Real> * (mDisk.mSize /opow2)); }

	public:
		PRT_ENTITY_FACTORY_DECLARE("lens-circle", LensCircle)

		LensCircle() noexcept { mDisk.setConcentricDisk(1); }

		[[nodiscard]] virtual OptionalPDF<Real> PDF(const MaterialInteraction &) const noexcept override { return diskPdf(); }
		[[nodiscard]] virtual OptionalPDF<Real> PDF(const Matrix &, const MaterialInteraction &lensSurface) const noexcept override { return PDF(lensSurface); }
		[[nodiscard]] virtual pl::marked_optional<SampledSensor> sampleArea(RandomReals<2> p) const noexcept override;
		[[nodiscard]] virtual Real intersect(VecP /*origin*/, VecD /*dir*/, VecD /*dirInv*/) const noexcept override { return -1; }
		[[nodiscard]] virtual bool hasArea() const noexcept override { return false; }
		virtual void update() override {}

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:
		DistributionMapperConcentricDisk<Real, VecP> mDisk;

	};

	class PHOTORT_API LensPrimitive : public ILens, public RenderEntity
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("lens-primitive", LensPrimitive)

		EntityPtr<IPrimitiveGroup> mPrimitiveGroup;

		[[nodiscard]] virtual OptionalPDF<Real> PDF(const MaterialInteraction &lensSurface) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(const Matrix &m, const MaterialInteraction &lensSurface) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledSensor> sampleArea(RandomReals<2> p) const noexcept override;
		[[nodiscard]] virtual Real intersect(VecP origin, VecD dir, VecD dirInv) const noexcept override;
		[[nodiscard]] virtual bool hasArea() const noexcept override { return true; }
		virtual void update() override;

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	};



	class PHOTORT_API CameraLens : public CameraPinHole
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("camera-lens", CameraLens)

		virtual ~CameraLens() = default;

		[[nodiscard]] virtual pl::marked_optional<SampledSensor> sampleArea(IRNGInt &rng, RandomReals<2> rndPos) const noexcept override;

		virtual void setFocus(Real dist) override;
		virtual void setFocusPos(VecP pos) override;
		[[nodiscard]] virtual Real getFocus() const noexcept override { return mFocusDist; }

		void setLens(EntityPtr<ILens> l) { mLens = std::move(l); }
		[[nodiscard]] const auto &getLens() const noexcept { return mLens; }

		virtual pl::marked_optional<CameraIntersection> intersectExt(const VecP origin, const VecD dir) const noexcept override;
		[[nodiscard]] virtual bool intersect(const VecP origin, const VecD dir) const noexcept override;

		[[nodiscard]] virtual bool hasArea() const noexcept override { return mLens->hasArea(); }

		virtual void update() override;

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

		[[nodiscard]] virtual OptionalPDF<Real> worldPDF(const MaterialInteraction &inter) const noexcept override { return mLens->PDF(mMatrix.world, inter); }
		[[nodiscard]] virtual OptionalPDF<Real> PDF(const MaterialInteraction &inter) const noexcept override { return mLens->PDF(inter); }

	protected:
		Real mFocusDist = 1, mFocusDistInv = 1, mFocusDistSquared = 1, mFocusDistInvSquared = 1;

		virtual pl::marked_optional<Point2Dr> getImagePosition(const VecP origin, const VecD dir, const VecP destination) const noexcept override;
		[[nodiscard]] virtual VecP getFocusPosition(const VecP localOrigin, const VecD localDir) const noexcept override;

		EntityPtr<ILens> mLens;
	};
}
