/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Camera.hpp>
#include <PhotoRT/Core/RenderEntity.hpp>
#include <PhotoRT/Core/Sensors.hpp>

namespace PhotoRT
{
	class PHOTORT_API CameraPinHole : public CameraBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("camera-pinhole", CameraPinHole)

		virtual void update() override;

		[[nodiscard]] virtual Lobe<> getCameraViewLobe() const noexcept override { return mImagePlaneLobe; }

		virtual void setPosition(VecP pos) override { mPosition = pos.as_point(); }
		[[nodiscard]] virtual VecP getPosition() const noexcept override { return mPosition; }

		virtual void setLookAt(VecP lookAt) override { mLookAt = lookAt; }
		[[nodiscard]] virtual VecP getLookAt() const noexcept override { return mLookAt; }

		virtual void setUp(VecP up) override { mUp = up; }
		[[nodiscard]] virtual VecP getUp() const noexcept override { return mUp; }

		virtual void setFocus(Real /*dist*/) override {}
		virtual void setFocusPos(VecP /*pos*/) override {}
		virtual Ray getFocusDir(Point2Dr pos) const noexcept override;
		[[nodiscard]] virtual Real getFocus() const noexcept override { return 1; }


		[[nodiscard]] virtual pl::marked_optional<SampledSensorOut> sampleImageDir(IRNGInt &rng, RandomReals<2> rndPos, Point2Dr pos) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledImagePhotonDir> sampleImagePhotonDir(const MaterialInteraction &surface, IRNGInt &rng, RandomReals<2> rndPos, const ISensor *scaleTo = nullptr) const noexcept override;

		[[nodiscard]] virtual OptionalPDF<Real> worldPDF(const MaterialInteraction &) const noexcept override { return 1; }
		[[nodiscard]] virtual OptionalPDF<Real> PDF(const MaterialInteraction &) const noexcept override { return 1; }
		[[nodiscard]] virtual OptionalPDF<Real> PDFA(const MaterialInteraction &sensorSurface, const MaterialInteraction &surface, VecD dir) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDFW(const MaterialInteraction &sensorSurface, VecD dir) const noexcept override;

		[[nodiscard]] virtual pl::marked_optional<SampledSensorOut> sampleOut(IRNGInt &rng, RandomReals<2> rndPos, RandomReals<2> rndDir) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledSensorOutDir> sampleOutDir(const SampledSensor &sensorSurface, IRNGInt &rng, RandomReals<2> rndDir) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledSensorIn> sampleIn(const MaterialInteraction &surface, IRNGInt &rng, RandomReals<2> rndPos) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledSensor> sampleArea(IRNGInt &rng, RandomReals<2> rndPos) const noexcept override;

		[[nodiscard]] virtual Color radiance(const MaterialInteraction &, VecD /*dir*/) const noexcept override { return White; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:

		virtual pl::marked_optional<Point2Dr> getImagePosition(const VecP origin, const VecD dir, const VecP destination) const noexcept;

		[[nodiscard]] virtual VecP getFocusPosition(const VecP localOrigin, const VecD localDir) const noexcept
		{
			return localOrigin + localDir;
		}

		Lobe<> mImagePlaneLobe;
		VecP mPosition{pl::tag::identity}, mLookAt{0, 0, 1, 1}, mUp{0, 1, 0, 1};
	};
}
