/*
	PhotoRT

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Random.hpp>

#include <boost/random/mersenne_twister.hpp>

#include <random>

#include <pcg_random.hpp>

namespace PhotoRT
{
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("rng-random", Augmented_rng_random, RNGStd<std::random_device>)
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("rng-mersenne_twister", Augmented_rng_mersenne_twister, RNGStdSeedable<boost::random::mt19937>)
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("rng-pcg32", Augmented_rng_pcg32, RNGStdSeedable<pcg32_fast>)
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("rng-pcg64", Augmented_rng_pcg64, RNGStdSeedable<pcg64_fast>)
}
