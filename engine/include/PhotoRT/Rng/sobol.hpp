/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Random.hpp>

#include <bit>

namespace PhotoRT::ImplSobol
{
	template <typename T_>
	struct SobolState
	{
		using value_type = T_;

		T_ seed = 0;
		T_ lastq[1111];
		T_ seed_save = -1;
	};

	using i4SobolState = SobolState<int>;
	using i8SobolState = SobolState<long long int>;

	int PHOTORT_API i4_bit_hi1 ( int n );
	int PHOTORT_API i4_bit_lo0 ( int n );
	int PHOTORT_API i4_max ( int i1, int i2 );
	int PHOTORT_API i4_min ( int i1, int i2 );
	void PHOTORT_API i4_sobol ( int dim_num, i4SobolState &state, float quasi[ ] );
	float PHOTORT_API *i4_sobol_generate ( int m, int n, i4SobolState &state );
	int PHOTORT_API i4_uniform ( int a, int b, int *seed );

	int PHOTORT_API i8_bit_hi1 ( long long int n );
	int PHOTORT_API i8_bit_lo0 ( long long int n );
	long long int PHOTORT_API i8_max ( long long int i1, long long int i2 );
	long long int PHOTORT_API i8_min ( long long int i1, long long int i2 );
	void PHOTORT_API i8_sobol ( int dim_num, i8SobolState &state, double quasi[ ] );
	double PHOTORT_API *i8_sobol_generate ( int m, int n, i8SobolState &state );
	long long int PHOTORT_API i8_uniform ( long long int a, long long int b, int *seed );

	float PHOTORT_API r4_abs ( float x );
	int PHOTORT_API r4_nint ( float x );
	float PHOTORT_API r4_uniform_01 ( int &seed );

	double PHOTORT_API r8_abs ( double x );
	int PHOTORT_API r8_nint ( double x );
	double PHOTORT_API r8_uniform_01 ( int &seed );

	void PHOTORT_API r8mat_write ( std::string output_filename, int m, int n, double table[] );

	int PHOTORT_API tau_sobol ( int dim_num );

	template <typename T_>
	struct SobolStateSelect {};

	template <> struct SobolStateSelect<float> { using type = i4SobolState; };
	template <> struct SobolStateSelect<int> { using type = i4SobolState; };
	template <> struct SobolStateSelect<double> { using type = i8SobolState; };
	template <> struct SobolStateSelect<long long int> { using type = i8SobolState; };
	template <typename T_> using SobolStateSelect_t = typename SobolStateSelect<T_>::type;

	template <typename T_> T_ uniform_01(int &seed)
	{
		if constexpr(std::same_as<T_, float>)
			return r4_uniform_01(seed);
		else if constexpr(std::same_as<T_, double>)
			return r8_uniform_01(seed);
		else if constexpr(std::same_as<T_, int>)
			return i4_uniform(std::numeric_limits<int>::min(), std::numeric_limits<int>::max(), &seed);
		else if constexpr(std::same_as<T_, long int> && sizeof(long long int) == sizeof(long int))
			return i8_uniform(std::numeric_limits<long long int>::min(), std::numeric_limits<long long int>::max(), &seed);
		else if constexpr(std::same_as<T_, long int> && sizeof(int) == sizeof(long int))
			return i8_uniform(std::numeric_limits<int>::min(), std::numeric_limits<int>::max(), &seed);
		else if constexpr(std::same_as<T_, long long int>)
			return i8_uniform(std::numeric_limits<long long int>::min(), std::numeric_limits<long long int>::max(), &seed);
		else
		{
			static_assert(pl::dependent_false<T_>, "Invalid type.");
			return {};
		}
	}

	inline void sobol(int nbDim, i4SobolState &state, float res[]) { i4_sobol(nbDim, state, res); }
	inline void sobol(int nbDim, i8SobolState &state, double res[]) { i8_sobol(nbDim, state, res); }
} // namespace PhotoRT::ImplSobol

namespace PhotoRT
{
	template <std::unsigned_integral T_>
	class RNGSobol
	{
	public:
		using result_type = T_;
		using param_type = int;
		static constexpr int default_seed = 1;

		void seed(int value = default_seed)
		{
			mSeedValue = pl::plmax<int>(value, 1);
		}

		template <typename Sseq_>
		void seed(Sseq_ &seq)
		{
			std::array<int, 1> seeds;

			seq.generate(seeds.begin(), seeds.end());

			seed(seeds[0]);
		}

		[[nodiscard]] param_type param() const noexcept { return mSeedValue; }
		void param(param_type params) noexcept { mSeedValue = params; }

		[[nodiscard]] static constexpr result_type min() noexcept { return 0; }
		[[nodiscard]] static constexpr result_type max() noexcept { return std::numeric_limits<result_type>::max(); }

		result_type operator() () noexcept
			{ return std::bit_cast<T_>(ImplSobol::uniform_01<std::make_signed_t<T_>>(mSeedValue)); }

	private:
		int mSeedValue = 1;
	};

	PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("rng-sobol", Augmented_rng_sobol, RNGStdSeedable<RNGSobol<RandomInt>>)
}
