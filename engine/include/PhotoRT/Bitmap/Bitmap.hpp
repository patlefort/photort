/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Bitmap_fwd.hpp"

#include <PhotoRT/Core/Base.hpp>

#include <PhotoRT/Core/Bitmap.hpp>
#include "impl/BitmapBuffer.hpp"
#include "BitmapAlgo.hpp"

//#include <boost/range/adaptor/sliced.hpp>

#include <patlib/box.hpp>
#include <patlib/rectangle.hpp>
#include <patlib/task.hpp>

namespace PhotoRT
{
	template <typename BitmapType_>
	[[nodiscard]] constexpr auto getBitmapRect(const BitmapType_ &b) noexcept
		{ return pl::rectangle<>{ {}, static_cast<pl::rectangle<>::point_type>(b.getSize()) }; }

	namespace ImplBitmap
	{
		template <typename Source_, typename Target_>
		auto &copyAndMoveImpl(const Source_ &other, Target_ &dest)
		{
			Target_ b;

			b.allocate(other.getSize());
			b.copyData(other);

			return (dest = std::move(b));
		}

		// pl::copy $other into a new buffer and move it to $dest if successful.
		template <typename Source_, typename Target_>
		auto &copyAndMove(const Source_ &other, Target_ &dest)
		{
			if(other.getNbPixels() == dest.getNbPixels())
				dest.copyData(other);
			else
				copyAndMoveImpl(other, dest);

			return dest;
		}

		// Cast $other to Target_ type if $other is of that type before invoking copyAndMove
		// to allow faster copy.
		template <typename Source_, typename Target_>
		auto &copyAndMoveCast(const Source_ &other, Target_ &dest)
		{
			if constexpr(std::same_as<Source_, Target_>)
			{
				plassert(other.hasBuffer());
				plassert(dest.hasBuffer());
				dest.buffer() = other.buffer();
			}
			else
			{
				if(const auto *casted = dynamic_cast<const Target_ *>(&other); casted)
				{
					plassert(casted->hasBuffer());
					plassert(dest.hasBuffer());
					dest.buffer() = casted->buffer();
				}
				else
					copyAndMove(other, dest);
			}

			return dest;
		}

		// Try to move $other in $dest if $other is of type Target_, else copy.
		template <typename Source_, typename Target_>
		auto &moveOrCopy(Source_ &other, Target_ &dest)
		{
			if constexpr(std::same_as<Source_, Target_>)
				dest = std::move(other);
			else
			{
				if(auto *casted = dynamic_cast<Target_ *>(&other); casted)
					dest = std::move(*casted);
				else
					copyAndMove(other, dest);
				//other.freeBitmap();
			}

			return dest;
		}
	} // namespace ImplBitmap


	template <typename BufferType_>
	class BitmapBuffer : public BufferType_
	{
	public:

		using BufferType = BufferType_;
		using typename BufferType::PixelType;
		static constexpr auto IsContiguous = BufferType::IsContiguous;

		using iterator = BitmapIterator1D<BitmapBuffer, PixelType>;
		using const_iterator = BitmapIterator1D<const BitmapBuffer, PixelType>;
		using reverse_iterator = std::reverse_iterator<iterator>;
		using const_reverse_iterator = std::reverse_iterator<const_iterator>;

		using iterator_2d = BitmapIterator2D<BitmapBuffer, PixelType>;
		using const_iterator_2d = BitmapIterator2D<const BitmapBuffer, PixelType>;
		using reverse_iterator_2d = std::reverse_iterator<iterator_2d>;
		using const_reverse_iterator_2d = std::reverse_iterator<const_iterator_2d>;

		using iterator_3d = BitmapIterator3D<BitmapBuffer, PixelType>;
		using const_iterator_3d = BitmapIterator3D<const BitmapBuffer, PixelType>;
		using reverse_iterator_3d = std::reverse_iterator<iterator_3d>;
		using const_reverse_iterator_3d = std::reverse_iterator<const_iterator_3d>;

		PL_CRTP_DERIVED_VIR(BufferType_)

		[[nodiscard]] auto begin() noexcept { return iterator{*this}; }
		[[nodiscard]] auto begin() const noexcept { return const_iterator{*this}; }
		[[nodiscard]] auto cbegin() const noexcept { return const_iterator{*this}; }
		[[nodiscard]] auto end() noexcept { return iterator{*this, true}; }
		[[nodiscard]] auto end() const noexcept { return const_iterator{*this, true}; }
		[[nodiscard]] auto cend() const noexcept { return const_iterator{*this, true}; }

		[[nodiscard]] auto rbegin() noexcept { return reverse_iterator{end()}; }
		[[nodiscard]] auto rbegin() const noexcept { return const_reverse_iterator{end()}; }
		[[nodiscard]] auto crbegin() const noexcept { return const_reverse_iterator{cend()}; }
		[[nodiscard]] auto rend() noexcept { return reverse_iterator{begin()}; }
		[[nodiscard]] auto rend() const noexcept { return const_reverse_iterator{begin()}; }
		[[nodiscard]] auto crend() const noexcept { return const_reverse_iterator{cbegin()}; }

		[[nodiscard]] auto begin2d(const pl::rectangle<> &r) noexcept { return iterator_2d{*this, r}; }
		[[nodiscard]] auto begin2d(const pl::rectangle<> &r) const noexcept { return const_iterator_2d{*this, r}; }
		[[nodiscard]] auto cbegin2d(const pl::rectangle<> &r) const noexcept { return const_iterator_2d{*this, r}; }
		[[nodiscard]] auto end2d(const pl::rectangle<> &r) noexcept { return iterator_2d{*this, r, true}; }
		[[nodiscard]] auto end2d(const pl::rectangle<> &r) const noexcept { return const_iterator_2d{*this, r, true}; }
		[[nodiscard]] auto cend2d(const pl::rectangle<> &r) const noexcept { return const_iterator_2d{*this, r, true}; }

		[[nodiscard]] auto rbegin2d(const pl::rectangle<> &r) noexcept { return reverse_iterator_2d{end2d(r)}; }
		[[nodiscard]] auto rbegin2d(const pl::rectangle<> &r) const noexcept { return const_reverse_iterator_2d{end2d(r)}; }
		[[nodiscard]] auto crbegin2d(const pl::rectangle<> &r) const noexcept { return const_reverse_iterator_2d{cend2d(r)}; }
		[[nodiscard]] auto rend2d(const pl::rectangle<> &r) noexcept { return reverse_iterator_2d{begin2d(r)}; }
		[[nodiscard]] auto rend2d(const pl::rectangle<> &r) const noexcept { return const_reverse_iterator_2d{begin2d(r)}; }
		[[nodiscard]] auto crend2d(const pl::rectangle<> &r) const noexcept { return const_reverse_iterator_2d{cbegin2d(r)}; }

		[[nodiscard]] auto range2d(const pl::rectangle<> &r) noexcept { return rg::make_subrange(begin2d(r), end2d(r)); }
		[[nodiscard]] auto range2d(const pl::rectangle<> &r) const noexcept { return rg::make_subrange(begin2d(r), end2d(r)); }
		[[nodiscard]] auto crange2d(const pl::rectangle<> &r) const noexcept { return rg::make_subrange(cbegin2d(r), cend2d(r)); }
		[[nodiscard]] auto rrange2d(const pl::rectangle<> &r) noexcept { return rg::make_subrange(rbegin2d(r), rend2d(r)); }
		[[nodiscard]] auto rrange2d(const pl::rectangle<> &r) const noexcept { return rg::make_subrange(rbegin2d(r), rend2d(r)); }
		[[nodiscard]] auto crrange2d(const pl::rectangle<> &r) const noexcept { return rg::make_subrange(crbegin2d(r), crend2d(r)); }

		[[nodiscard]] auto begin3d(const pl::box<VecP3i> &r) noexcept { return iterator_3d{*this, r}; }
		[[nodiscard]] auto begin3d(const pl::box<VecP3i> &r) const noexcept { return const_iterator_3d{*this, r}; }
		[[nodiscard]] auto cbegin3d(const pl::box<VecP3i> &r) const noexcept { return const_iterator_3d{*this, r}; }
		[[nodiscard]] auto end3d(const pl::box<VecP3i> &r) noexcept { return iterator_3d{*this, r, true}; }
		[[nodiscard]] auto end3d(const pl::box<VecP3i> &r) const noexcept { return const_iterator_3d{*this, r, true}; }
		[[nodiscard]] auto cend3d(const pl::box<VecP3i> &r) const noexcept { return const_iterator_3d{*this, r, true}; }

		[[nodiscard]] auto rbegin3d(const pl::box<VecP3i> &r) noexcept { return reverse_iterator_3d{end3d(r)}; }
		[[nodiscard]] auto rbegin3d(const pl::box<VecP3i> &r) const noexcept { return const_reverse_iterator_3d{end3d(r)}; }
		[[nodiscard]] auto crbegin3d(const pl::box<VecP3i> &r) const noexcept { return const_reverse_iterator_3d{cend3d(r)}; }
		[[nodiscard]] auto rend3d(const pl::box<VecP3i> &r) noexcept { return reverse_iterator_3d{begin3d(r)}; }
		[[nodiscard]] auto rend3d(const pl::box<VecP3i> &r) const noexcept { return const_reverse_iterator_3d{begin3d(r)}; }
		[[nodiscard]] auto crend3d(const pl::box<VecP3i> &r) const noexcept { return const_reverse_iterator_3d{cbegin3d(r)}; }

		[[nodiscard]] auto range3d(const pl::box<VecP3i> &r) noexcept { return rg::make_subrange(begin3d(r), end3d(r)); }
		[[nodiscard]] auto range3d(const pl::box<VecP3i> &r) const noexcept { return rg::make_subrange(begin3d(r), end3d(r)); }
		[[nodiscard]] auto crange3d(const pl::box<VecP3i> &r) const noexcept { return rg::make_subrange(cbegin3d(r), cend3d(r)); }
		[[nodiscard]] auto rrange3d(const pl::box<VecP3i> &r) noexcept { return rg::make_subrange(rbegin3d(r), rend3d(r)); }
		[[nodiscard]] auto rrange3d(const pl::box<VecP3i> &r) const noexcept { return rg::make_subrange(rbegin3d(r), rend3d(r)); }
		[[nodiscard]] auto crrange3d(const pl::box<VecP3i> &r) const noexcept { return rg::make_subrange(crbegin3d(r), crend3d(r)); }

		using BufferType_::BufferType_;

		PL_DEFAULT_COPYMOVE(BitmapBuffer)

		template <typename... Ts_>
		BitmapBuffer(const BitmapBuffer<Ts_...> &o)
		{
			allocate(o.getSize());
			copyData(o);
		}

		template <typename... Ts_>
		BitmapBuffer &operator=(const BitmapBuffer<Ts_...> &o)
		{
			if(int(o.getSize() != this->getSize()))
				allocate(o.getSize());

			return copyData(o);
		}

		void zeroBitmap() noexcept { this->set_zero(); }
		void freeBitmap() noexcept { this->free(); }

		[[nodiscard]] szt getBytesPerPixels() const noexcept { return sizeof(PixelType); }

		using BufferType_::operator[];

		[[nodiscard]] decltype(auto) getPixelRef(szt offset) noexcept { return (*this)[offset]; }
		[[nodiscard]] decltype(auto) getPixelRef(szt offset) const noexcept { return (*this)[offset]; }

		[[nodiscard]] auto getPixel(szt offset) const noexcept { return (PixelType)(*this)[offset]; }
		[[nodiscard]] auto getPixel(if32 u, if32 v) const noexcept { return (PixelType)this->get(Point2Di{u, v}); }
		[[nodiscard]] auto getPixel(if32 u, if32 v, if32 w) const noexcept { return (PixelType)this->get(Point3Di{u, v, w}); }

		void setPixel(const PixelType &color, szt offset) { (*this)[offset] = color; }
		void setPixel(const PixelType &color, if32 u, if32 v) { this->get(Point2Di{u, v}) = color; }
		void setPixel(const PixelType &color, if32 u, if32 v, if32 w) { this->get(Point3Di{u, v, w}) = color; }

		void copyIntoContiguousMemory(void *ptr) const noexcept
		{
			if constexpr(IsContiguous)
			{
				this->mPixels |
					pl::copy
					(
						static_cast<PixelType *>(ptr),
						pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
					);
			}
			else
			{
				*this |
					pl::copy
					(
						static_cast<PixelType *>(ptr),
						pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
					);
			}
		}

		template <typename BT2_>
		auto &copyData(const BitmapBuffer<BT2_> &other) noexcept
		{
			ImplBitmap::copy(other.base(), base());

			return *this;
		}

		template <typename T_>
		auto &operator+= (const BitmapBuffer<T_> &other) noexcept
		{
			ImplBitmap::op(base(), other.base(), base(), [](auto&& p1, auto&& p2){ return p1 + p2; });

			return *this;
		}

		template <typename T_>
		auto &operator-= (const BitmapBuffer<T_> &other) noexcept
		{
			ImplBitmap::op(base(), other.base(), base(), [](auto&& p1, auto&& p2){ return p1 - p2; });

			return *this;
		}

		template <typename T_>
		auto &operator*= (const BitmapBuffer<T_> &other) noexcept
		{
			ImplBitmap::op(base(), other.base(), base(), [](auto&& p1, auto&& p2){ return p1 * p2; });

			return *this;
		}

		template <typename T_>
		auto &operator/= (const BitmapBuffer<T_> &other) noexcept
		{
			ImplBitmap::op(base(), other.base(), base(), [](auto&& p1, auto&& p2){ return p1 / p2; });

			return *this;
		}

		template <typename T1_, typename T2_, typename OP_, bool GetIntoPixel_ = false, pl::CParallelPolicy Policy_ = pl::policy::default_policy_t>
		const auto &transform(const BitmapBuffer<T1_> &other, BitmapBuffer<T2_> &into, OP_&& fc, const bool_c<GetIntoPixel_> getIntoPixel = {}, const Policy_ pol = {}) const
		{
			ImplBitmap::op(base(), other.base(), into.base(), PL_FWD(fc), getIntoPixel, pol);

			return *this;
		}

		friend std::ostream& operator<<(std::ostream& os, const BitmapBuffer &b)
		{
			b.serialize(os);

			return os;
		}

		friend std::istream& operator>>(std::istream& is, BitmapBuffer &b)
		{
			b.unserialize(is);

			return is;
		}
	};

	/*template <typename BufferType_>
	struct BufferDesc
	{
		pl::memory_system_pointer<BitmapBuffer<BufferType_>, pl::IMemorySystem<VoidOffsetPtr>> buffer{};
		bool is_shared = false;
	};*/

	template <typename Derived_, typename BufferType_>
	class VirtualBitmapBase : public BitmapBase//, public pl::CloneableNewOrCopy<BitmapBase, Derived_>
	{
		template <typename, typename>
		friend class VirtualBitmapBase;

		PL_CRTP_BASE_VIR(Derived_)

	public:

		using BufferType = BufferType_;
		using PixelType = typename BufferType::PixelType;

		using reference = BitmapOffsetReference<VirtualBitmapBase>;
		using const_reference = BitmapOffsetReference<const VirtualBitmapBase>;

		using iterator = BitmapIterator1D<VirtualBitmapBase>;
		using const_iterator = BitmapIterator1D<const VirtualBitmapBase>;
		using reverse_iterator = std::reverse_iterator<iterator>;
		using const_reverse_iterator = std::reverse_iterator<const_iterator>;

		using iterator_2d = BitmapIterator2D<VirtualBitmapBase>;
		using const_iterator_2d = BitmapIterator2D<const VirtualBitmapBase>;
		using reverse_iterator_2d = std::reverse_iterator<iterator_2d>;
		using const_reverse_iterator_2d = std::reverse_iterator<const_iterator_2d>;

		using iterator_3d = BitmapIterator3D<VirtualBitmapBase>;
		using const_iterator_3d = BitmapIterator3D<const VirtualBitmapBase>;
		using reverse_iterator_3d = std::reverse_iterator<iterator_3d>;
		using const_reverse_iterator_3d = std::reverse_iterator<const_iterator_3d>;

		[[nodiscard]] auto begin() noexcept { return iterator{*this}; }
		[[nodiscard]] auto begin() const noexcept { return const_iterator{*this}; }
		[[nodiscard]] auto cbegin() const noexcept { return const_iterator{*this}; }
		[[nodiscard]] auto end() noexcept { return iterator{*this, true}; }
		[[nodiscard]] auto end() const noexcept { return const_iterator{*this, true}; }
		[[nodiscard]] auto cend() const noexcept { return const_iterator{*this, true}; }

		[[nodiscard]] auto rbegin() noexcept { return reverse_iterator{end()}; }
		[[nodiscard]] auto rbegin() const noexcept { return const_reverse_iterator{end()}; }
		[[nodiscard]] auto crbegin() const noexcept { return const_reverse_iterator{cend()}; }
		[[nodiscard]] auto rend() noexcept { return reverse_iterator{begin()}; }
		[[nodiscard]] auto rend() const noexcept { return const_reverse_iterator{begin()}; }
		[[nodiscard]] auto crend() const noexcept { return const_reverse_iterator{cbegin()}; }

		[[nodiscard]] auto begin2d(const pl::rectangle<> &r) noexcept { return iterator_2d{*this, r}; }
		[[nodiscard]] auto begin2d(const pl::rectangle<> &r) const noexcept { return const_iterator_2d{*this, r}; }
		[[nodiscard]] auto cbegin2d(const pl::rectangle<> &r) const noexcept { return const_iterator_2d{*this, r}; }
		[[nodiscard]] auto end2d(const pl::rectangle<> &r) noexcept { return iterator_2d{*this, r, true}; }
		[[nodiscard]] auto end2d(const pl::rectangle<> &r) const noexcept { return const_iterator_2d{*this, r, true}; }
		[[nodiscard]] auto cend2d(const pl::rectangle<> &r) const noexcept { return const_iterator_2d{*this, r, true}; }

		[[nodiscard]] auto rbegin2d(const pl::rectangle<> &r) noexcept { return reverse_iterator_2d{end2d(r)}; }
		[[nodiscard]] auto rbegin2d(const pl::rectangle<> &r) const noexcept { return const_reverse_iterator_2d{end2d(r)}; }
		[[nodiscard]] auto crbegin2d(const pl::rectangle<> &r) const noexcept { return const_reverse_iterator_2d{cend2d(r)}; }
		[[nodiscard]] auto rend2d(const pl::rectangle<> &r) noexcept { return reverse_iterator_2d{begin2d(r)}; }
		[[nodiscard]] auto rend2d(const pl::rectangle<> &r) const noexcept { return const_reverse_iterator_2d{begin2d(r)}; }
		[[nodiscard]] auto crend2d(const pl::rectangle<> &r) const noexcept { return const_reverse_iterator_2d{cbegin2d(r)}; }

		[[nodiscard]] auto range2d(const pl::rectangle<> &r) noexcept { return rg::make_subrange(begin2d(r), end2d(r)); }
		[[nodiscard]] auto range2d(const pl::rectangle<> &r) const noexcept { return rg::make_subrange(begin2d(r), end2d(r)); }
		[[nodiscard]] auto crange2d(const pl::rectangle<> &r) const noexcept { return rg::make_subrange(cbegin2d(r), cend2d(r)); }
		[[nodiscard]] auto rrange2d(const pl::rectangle<> &r) noexcept { return rg::make_subrange(rbegin2d(r), rend2d(r)); }
		[[nodiscard]] auto rrange2d(const pl::rectangle<> &r) const noexcept { return rg::make_subrange(rbegin2d(r), rend2d(r)); }
		[[nodiscard]] auto crrange2d(const pl::rectangle<> &r) const noexcept { return rg::make_subrange(crbegin2d(r), crend2d(r)); }

		[[nodiscard]] auto begin3d(const pl::box<VecP3i> &r) noexcept { return iterator_3d{*this, r}; }
		[[nodiscard]] auto begin3d(const pl::box<VecP3i> &r) const noexcept { return const_iterator_3d{*this, r}; }
		[[nodiscard]] auto cbegin3d(const pl::box<VecP3i> &r) const noexcept { return const_iterator_3d{*this, r}; }
		[[nodiscard]] auto end3d(const pl::box<VecP3i> &r) noexcept { return iterator_3d{*this, r, true}; }
		[[nodiscard]] auto end3d(const pl::box<VecP3i> &r) const noexcept { return const_iterator_3d{*this, r, true}; }
		[[nodiscard]] auto cend3d(const pl::box<VecP3i> &r) const noexcept { return const_iterator_3d{*this, r, true}; }

		[[nodiscard]] auto rbegin3d(const pl::box<VecP3i> &r) noexcept { return reverse_iterator_3d{end3d(r)}; }
		[[nodiscard]] auto rbegin3d(const pl::box<VecP3i> &r) const noexcept { return const_reverse_iterator_3d{end3d(r)}; }
		[[nodiscard]] auto crbegin3d(const pl::box<VecP3i> &r) const noexcept { return const_reverse_iterator_3d{cend3d(r)}; }
		[[nodiscard]] auto rend3d(const pl::box<VecP3i> &r) noexcept { return reverse_iterator_3d{begin3d(r)}; }
		[[nodiscard]] auto rend3d(const pl::box<VecP3i> &r) const noexcept { return const_reverse_iterator_3d{begin3d(r)}; }
		[[nodiscard]] auto crend3d(const pl::box<VecP3i> &r) const noexcept { return const_reverse_iterator_3d{cbegin3d(r)}; }

		[[nodiscard]] auto range3d(const pl::box<VecP3i> &r) noexcept { return rg::make_subrange(begin3d(r), end3d(r)); }
		[[nodiscard]] auto range3d(const pl::box<VecP3i> &r) const noexcept { return rg::make_subrange(begin3d(r), end3d(r)); }
		[[nodiscard]] auto crange3d(const pl::box<VecP3i> &r) const noexcept { return rg::make_subrange(cbegin3d(r), cend3d(r)); }
		[[nodiscard]] auto rrange3d(const pl::box<VecP3i> &r) noexcept { return rg::make_subrange(rbegin3d(r), rend3d(r)); }
		[[nodiscard]] auto rrange3d(const pl::box<VecP3i> &r) const noexcept { return rg::make_subrange(rbegin3d(r), rend3d(r)); }
		[[nodiscard]] auto crrange3d(const pl::box<VecP3i> &r) const noexcept { return rg::make_subrange(crbegin3d(r), crend3d(r)); }

		//PL_DEFAULT_COPYMOVE(VirtualBitmapBase)

		/*VirtualBitmapBase() = default;
		VirtualBitmapBase(const VirtualBitmapBase &o)
			: BitmapBase(o)
		{
			if(o.hasBuffer() && hasBuffer())
			{
				allocate(o.buffer().getSize());
				buffer() = o.buffer();
			}
		}
		VirtualBitmapBase(VirtualBitmapBase &&o) = default;

		VirtualBitmapBase(BitmapBase &&o)
		{
			ImplBitmap::moveOrCopy(o, *this);
		}*/

		using BitmapBase::operator=;

		virtual BitmapBase &operator= (const BitmapBase &other) override
		{
			return ImplBitmap::copyAndMoveCast(other, derived());
		}
		virtual BitmapBase &operator= (BitmapBase &&other) override
		{
			return ImplBitmap::moveOrCopy(other, derived());
		}

		/*Bitmap &operator= (const Bitmap &other)
		{
			ImplBitmap::copyAndMove(other, derived());
			return *this;
		}
		Bitmap &operator= (Bitmap &&other) noexcept
		{
			buffer() = std::move(other.buffer());
			BitmapBase::operator=(std::move(other));
			return *this;
		}*/

	public:

		[[nodiscard]] virtual void *getRawData() const noexcept final { return derived().buffer().getRawData(); }
		[[nodiscard]] virtual bool isContiguous() const noexcept final { return BufferType::IsContiguous; }
		[[nodiscard]] virtual std::any getUnderlyingPixelType() const noexcept final { return type_v<PixelType>; }

		[[nodiscard]] decltype(auto) at(szt offset) { if(offset >= size()) throw std::out_of_range("Offset out of bounds of bitmap."); return derived().buffer()[offset]; }
		[[nodiscard]] decltype(auto) at(szt offset) const { if(offset >= size()) throw std::out_of_range("Offset out of bounds of bitmap."); return derived().buffer()[offset]; }
		[[nodiscard]] decltype(auto) front() noexcept { return derived().buffer()[0]; }
		[[nodiscard]] decltype(auto) front() const noexcept { return derived().buffer()[0]; }
		[[nodiscard]] decltype(auto) back() noexcept { return derived().buffer()[size() - 1]; }
		[[nodiscard]] decltype(auto) back() const noexcept { return derived().buffer()[size() - 1]; }

		[[nodiscard]] auto size() const noexcept { return derived().buffer().size(); }
		[[nodiscard]] bool empty() const noexcept { return derived().buffer().empty(); }

		virtual void copyIntoContiguousMemory(void *ptr) const final
		{
			derived().buffer().copyIntoContiguousMemory(ptr);
		}

		virtual BitmapBase &operator+= (const BitmapBase &other) final
		{
			if(const auto *casted = dynamic_cast<const Derived_ *>(&other); casted)
				derived().buffer() += casted->buffer();
			else
				BitmapBase::operator+=(other);

			return *this;
		}

		auto &operator+= (const Derived_ &other)
		{
			derived().buffer() += other.buffer();

			return *this;
		}

		virtual BitmapBase &operator-= (const BitmapBase &other) final
		{
			if(const auto *casted = dynamic_cast<const Derived_ *>(&other); casted)
				derived().buffer() -= casted->buffer();
			else
				BitmapBase::operator-=(other);

			return *this;
		}

		auto &operator-= (const Derived_ &other)
		{
			derived().buffer() -= other.buffer();

			return *this;
		}

		virtual BitmapBase &operator*= (const BitmapBase &other) final
		{
			if(const auto *casted = dynamic_cast<const Derived_ *>(&other); casted)
				derived().buffer() *= casted->buffer();
			else
				BitmapBase::operator*=(other);

			return *this;
		}

		auto &operator*= (const Derived_ &other)
		{
			derived().buffer() *= other.buffer();

			return *this;
		}

		virtual BitmapBase &operator/= (const BitmapBase &other) final
		{
			if(const auto *casted = dynamic_cast<const Derived_ *>(&other); casted)
				derived().buffer() /= casted->buffer();
			else
				BitmapBase::operator/=(other);

			return *this;
		}

		auto &operator/= (const Derived_ &other)
		{
			derived().buffer() /= other.buffer();

			return *this;
		}

		template <typename OP_, bool GetIntoPixel_ = false, pl::CParallelPolicy Policy_ = pl::policy::default_policy_t>
		const auto &transform(const BitmapBase &other, BitmapBase &into, OP_&& fc, const bool_c<GetIntoPixel_> getIntoPixel = {}, const Policy_ pol = {}) const
		{
			const auto *castedOther = dynamic_cast<const Derived_ *>(&other);
			auto *castedInto = dynamic_cast<Derived_ *>(&into);

			if(castedOther && castedInto)
				derived().buffer().transform(castedOther->buffer(), castedInto->buffer(), PL_FWD(fc), getIntoPixel, pol);
			else
				BitmapBase::transform(other, into, PL_FWD(fc), getIntoPixel, pol);

			return *this;
		}

		template <typename... T1_, typename... T2_, typename OP_, bool GetIntoPixel_ = false, pl::CParallelPolicy Policy_ = pl::policy::default_policy_t>
		const auto &transform(const VirtualBitmapBase<T1_...> &other, VirtualBitmapBase<T2_...> &into, OP_&& fc, const bool_c<GetIntoPixel_> getIntoPixel = {}, const Policy_ pol = {}) const
		{
			derived().buffer().transform(other.derived().buffer(), into.derived().buffer(), PL_FWD(fc), getIntoPixel, pol);

			return *this;
		}

		[[nodiscard]] virtual Dir3Di getSize() const noexcept final { return derived().hasBuffer() ? derived().buffer().getSize() : Dir3Di{}; }
		[[nodiscard]] virtual szt getNbPixels() const noexcept final { return derived().hasBuffer() ? derived().buffer().getNbPixels() : 0; }
		[[nodiscard]] virtual szt getMemSize() const noexcept final { return derived().hasBuffer() ? derived().buffer().getMemSize() : 0; }
		[[nodiscard]] virtual if32 getWidth() const noexcept final { return derived().hasBuffer() ? derived().buffer().getSize()[0] : 0; }
		[[nodiscard]] virtual if32 getHeight() const noexcept final { return derived().hasBuffer() ? derived().buffer().getSize()[1] : 0; }
		[[nodiscard]] virtual if32 getDepth() const noexcept final { return derived().hasBuffer() ? derived().buffer().getSize()[2] : 0; }

		[[nodiscard]] virtual szt getBytesPerPixels() const noexcept final { return sizeof(PixelType); }

		[[nodiscard]] virtual Color getPixel(szt offset) const noexcept final { return convertToColor<Color>((PixelType)derived().buffer()[offset]); }
		[[nodiscard]] virtual Color getPixel(if32 u, if32 v) const noexcept final { return convertToColor<Color>((PixelType)derived().buffer().get(Point2Di{u, v})); }
		[[nodiscard]] virtual Color getPixel(if32 u, if32 v, if32 w) const noexcept final { return convertToColor<Color>((PixelType)derived().buffer().get(Point3Di{u, v, w})); }

		virtual void setPixel(Color color, szt offset) final
		{
			PixelType p; convertFromColor(color, p);
			derived().buffer()[offset] = p;
		}
		virtual void setPixel(Color color, if32 u, if32 v) final
		{
			PixelType p; convertFromColor(color, p);
			derived().buffer().get(Point2Di{u, v}) = p;
		}
		virtual void setPixel(Color color, if32 u, if32 v, if32 w) final
		{
			PixelType p; convertFromColor(color, p);
			derived().buffer().get(Point3Di{u, v, w}) = p;
		}

		[[nodiscard]] auto getPixelRef(szt offset) noexcept { return reference{*this, offset}; }
		[[nodiscard]] auto getPixelRef(szt offset) const noexcept { return const_reference{*this, offset}; }

		virtual BitmapBase &copyData(const BitmapBase &bitmap) override
		{
			if(const auto *casted = dynamic_cast<const Derived_ *>(&bitmap); casted)
				derived().buffer().copyData(casted->buffer());
			else
				BitmapBase::copyData(bitmap);

			return *this;
		}

		auto &copyData(const Derived_ &bitmap)
		{
			derived().buffer().copyData(bitmap.buffer());

			return *this;
		}

		auto &copyData(const BitmapBuffer<BufferType> &bitmap)
		{
			derived().buffer().copyData(bitmap);

			return *this;
		}

		template <typename... Ts_>
		auto &copyData(const BitmapBuffer<Ts_...> &bitmap)
		{
			ImplBitmap::op(bitmap, derived().buffer().base(), derived().buffer().base(),
				[&](auto&& c1, auto &&){
					PixelType p; convertFromColor(convertToColor<Color>(c1), p);
					return p;
				}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
			);

			return *this;
		}

		template <typename... Ts_>
		auto &copyData(const VirtualBitmapBase<Ts_...> &bitmap)
		{
			ImplBitmap::op(bitmap.derived().buffer().base(), derived().buffer().base(), derived().buffer().base(),
				[&](auto&& c1, auto &&){
					PixelType p; convertFromColor(convertToColor<Color>(c1), p);
					return p;
				}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
			);

			return *this;
		}



		auto &operator= (const IBitmap1DRead &other)
		{
			copyData(other); return *this;
		}

		auto &operator= (const IBitmap2DRead &other)
		{
			copyData(other); return *this;
		}

		auto &operator= (const IBitmap3DRead &other)
		{
			copyData(other); return *this;
		}

		virtual BitmapBase &copyData(const IBitmap1DRead &bitmap) override
		{
			if(const auto *casted = dynamic_cast<const Derived_ *>(&bitmap); casted)
				copyData(*casted);
			else
				BitmapBase::copyData(bitmap);

			return *this;
		}

		virtual BitmapBase &copyData(const IBitmap2DRead &bitmap) override
		{
			if(const auto *casted = dynamic_cast<const Derived_ *>(&bitmap); casted)
				copyData(*casted);
			else
				BitmapBase::copyData(bitmap);

			return *this;
		}

		virtual BitmapBase &copyData(const IBitmap3DRead &bitmap) override
		{
			if(const auto *casted = dynamic_cast<const Derived_ *>(&bitmap); casted)
				copyData(*casted);
			else
				BitmapBase::copyData(bitmap);

			return *this;
		}

		virtual void zeroBitmap() override
		{
			derived().buffer().set_zero();
		}

		friend auto &operator<<(std::ostream &os, const VirtualBitmapBase &b)
		{
			if(b.derived().hasBuffer())
				b.derived().buffer().serialize(os);
			else
				os.put('\0');

			return os;
		}

		friend auto &operator>>(std::istream &is, VirtualBitmapBase &b)
		{
			if(b.derived().hasBuffer())
				b.derived().buffer().unserialize(is);
			else
				is >> pl::io::skip_one;

			return is;
		}

		virtual void shrink_to_fit() override
		{
			if(derived().hasBuffer())
				derived().buffer().shrink_to_fit();
		}

	};

	template <typename BufferType_>
	class VirtualBitmapView : public VirtualBitmapBase<VirtualBitmapView<BufferType_>, BufferType_>
	{
	protected:
		BitmapBuffer<BufferType_> *mBuffer = nullptr;

	public:

		VirtualBitmapView() = default;
		VirtualBitmapView(BitmapBuffer<BufferType_> *buf) :
			mBuffer{buf} {}

		VirtualBitmapView(const VirtualBitmapView &) = default;

		[[nodiscard]] auto &buffer() noexcept { return *mBuffer; }
		[[nodiscard]] const auto &buffer() const noexcept { return *mBuffer; }
		[[nodiscard]] bool hasBuffer() const noexcept { return mBuffer != nullptr; }

		void buffer(BitmapBuffer<BufferType_> *buf) noexcept { mBuffer = buf; }

	protected:

		virtual void allocMemory(const Dir3Di dim) override
		{
			if(hasBuffer())
				buffer().allocate(dim);
		}

	};

	template <typename BufferType_>
	class VirtualBitmap : public VirtualBitmapBase<VirtualBitmap<BufferType_>, BufferType_>
	{
	protected:
		BitmapBuffer<BufferType_> mBuffer;

	public:

		VirtualBitmap() = default;
		VirtualBitmap(BitmapBuffer<BufferType_> buf) :
			mBuffer{std::move(buf)} {}

		PL_DEFAULT_COPYMOVE(VirtualBitmap)

		[[nodiscard]] auto &buffer() noexcept { return mBuffer; }
		[[nodiscard]] const auto &buffer() const noexcept { return mBuffer; }
		[[nodiscard]] constexpr bool hasBuffer() const noexcept { return true; }

	protected:

		virtual void allocMemory(const Dir3Di dim) override
		{
			buffer().allocate(dim);
		}
	};

	template <typename BufferType_, typename MemoryTag_ = memtag::Texture>
	class BitmapEntity final : public RenderEntityAugment<VirtualBitmapBase<BitmapEntity<BufferType_, MemoryTag_>, BufferType_>>
	{
	protected:
		using RenderEntityAugment<VirtualBitmapBase<BitmapEntity<BufferType_, MemoryTag_>, BufferType_>>::base;

		virtual void allocMemory(const Dir3Di dim) override
		{
			if(!hasBuffer())
				throw Exception("Cannot allocate memory for a non-existant buffer.");

			buffer().allocate(dim);
		}

	private:

		pl::memory_system_pointer<BitmapBuffer<BufferType_>, pl::IMemorySystem<VoidOffsetPtr>> mBuffer{};

		void allocateBuffer()
		{
			auto &memSys = *getMemorySystem<MemoryTag_>();

			std::string buffId = this->entityCollection ? this->entityCollection->getShmID() + this->getEntityId() : std::string{this->getEntityId()};

			mBuffer.find_or_construct(memSys, std::move(buffId), boost::indeterminate);
		}

	public:

		PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE_INCLASS(BitmapEntity<BufferType_, MemoryTag_>)

		[[nodiscard]] auto &buffer() noexcept { return *mBuffer; }
		[[nodiscard]] const auto &buffer() const noexcept { return *mBuffer; }
		[[nodiscard]] bool hasBuffer() const noexcept { return mBuffer != nullptr; }

		virtual void serialize(std::ostream &os) const override
		{
			if(!getMemorySystem<MemoryTag_>()->is_shared() || !pl::io::is_ipc(os))
				os << base();
			else
				os.put('\0');
		}

		virtual void unserialize(std::istream &is) override
		{
			if(hasBuffer())
			{
				if(!getMemorySystem<MemoryTag_>()->is_shared() || !pl::io::is_ipc(is))
					is >> base();
				else
					is >> pl::io::skip_one;

				if((buffer().getSize() /ohmul)[0] == 0)
					this->log() << "0 sized bitmap detected.";
			}
			else
				is >> pl::io::skip_one;
		}

		virtual void setEntityId(std::string id) override
		{
			RenderEntity::setEntityId(std::move(id));

			allocateBuffer();
		}

		[[nodiscard]] virtual bool hasData(bool ipcStream = false) const noexcept override { return !getMemorySystem<MemoryTag_>()->is_shared() || !ipcStream; }
		[[nodiscard]] virtual bool canHaveData() const noexcept override { return true; }

		virtual void setParameters(const ParameterTree &params) override
		{
			RenderEntity::setParameters(params);

			params.getValueIfExistsBind("tiled", PL_LAMBDA_FORWARD_THIS(setTiled));
			params.getValueIfExistsBind("linear_filter", PL_LAMBDA_FORWARD_THIS(setLinearFilter));

			if(const auto p = params.findParam("copy_from_1d"); p)
			{
				auto otherBitmap = this->entityCollection->createEntity(type_v<IBitmap1DRead>, *p);

				base() = *otherBitmap;
			}
			else if(const auto p = params.findParam("copy_from_2d"); p)
			{
				auto otherBitmap = this->entityCollection->createEntity(type_v<IBitmap2DRead>, *p);

				base() = *otherBitmap;
			}
			else if(const auto p = params.findParam("copy_from_3d"); p)
			{
				auto otherBitmap = this->entityCollection->createEntity(type_v<IBitmap3DRead>, *p);

				base() = *otherBitmap;
			}
			else if(const auto popt = params.findParam("fit"); popt && popt->childParams)
			{
				szt maxNbPix = 0;
				if32 w = 0, h = 0;
				const auto &p = *popt;

				if(const auto p2 = p.findParam("width"); p2)
					w = *p2;
				if(const auto p2 = p.findParam("height"); p2)
					h = *p2;
				if(const auto p2 = p.findParam("max_nbpixels"); p2)
					maxNbPix = *p2;

				this->fitToAspectRatio(w, h, maxNbPix);
			}
			else
			{
				if32 w = 0, h = 0, d = 1, hasDim = 0;
				if(const auto p = params.findParam("width"); p)
					{ w = *p; ++hasDim; }
				if(const auto p = params.findParam("height"); p)
					{ h = *p; ++hasDim; }
				if(const auto p = params.findParam("depth"); p)
					d = *p;

				if(hasDim == 2)
				{
					this->allocate(Dir3Di{w, h, d});

					if(const auto p = params.findParam("dist1d"); p)
					{
						if(this->empty())
							throw Exception("The bitmap cannot be filled by the given distribution because it has no given dimension.");

						auto dist = this->entityCollection->createEntity(type_v<IColorDistribution1D>, *p);

						szt startOffset = 0, endOffset = this->getNbPixels();
						params.getValueIfExists("start", startOffset);
						params.getValueIfExists("end", endOffset);

						startOffset = pl::plclamp<szt>(startOffset, 0, this->getNbPixels());
						endOffset = pl::plclamp<szt>(endOffset, 0, this->getNbPixels());

						this->zeroBitmap();

						BitmapAlgo::fillFromColorDistribution(*this, *this | rgv::slice(startOffset, endOffset), *dist, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*16));
					}
					else if(const auto p = params.findParam("dist2d"); p)
					{
						if(this->empty())
							throw Exception("The bitmap cannot be filled by the given distribution because it has no given dimension.");

						auto dist = this->entityCollection->createEntity(type_v<IColorDistribution2D>, *p);

						auto rect = this->getRect();
						params.getValueIfExists("rect", rect);

						this->zeroBitmap();

						BitmapAlgo::fillFromColorDistribution(*this, this->range2d(rect), *dist, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*16));
					}
					else if(const auto p = params.findParam("dist3d"); p)
					{
						if(this->empty())
							throw Exception("The bitmap cannot be filled by the given distribution because it has no given dimension.");

						auto dist = this->entityCollection->createEntity(type_v<IColorDistribution3D>, *p);

						auto box = this->getBox();
						params.getValueIfExists("box", box);

						this->zeroBitmap();

						BitmapAlgo::fillFromColorDistribution(*this, this->range3d(box), *dist, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*16));
					}
				}
			}
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {RenderEntity::getParameters(deep)};

			params.insert({
				{"linear_filter", this->mLinearFilter},
				{"tiled", this->mTiled}
			});

			return params;
		}
	};

	template <typename PixelType_, typename Alloc_ = PmrAllocatorGlobal<PixelType_, memtag::Texture>>
	using TextureBitmapBuffer = ImplBitmap::BitmapBuffer<PixelType_, pl::allocator_noinit_if_trivial<PixelType_, pl::allocator_with_stats<PixelType_, memtag::Texture, Alloc_>>>;

	template <typename PixelType_, typename Alloc_ = PmrAllocatorGlobal<PixelType_, memtag::Texture>>
	using TextureBitmap = VirtualBitmap<TextureBitmapBuffer<PixelType_, Alloc_>>;

	template <typename PixelType_, typename IndexType_, szt paletteSize_ = 256, typename Alloc_ = PmrAllocatorGlobal<PixelType_, memtag::Texture>>
	using TextureBitmapPaletteBuffer = ImplBitmap::BitmapPaletteBuffer<PixelType_, IndexType_, size_c<paletteSize_>, pl::allocator_noinit_if_trivial<PixelType_, pl::allocator_with_stats<PixelType_, memtag::Texture, Alloc_>>>;

	template <typename PixelType_, typename IndexType_, szt paletteSize_ = 256, typename Alloc_ = PmrAllocatorGlobal<PixelType_, memtag::Texture>>
	using TextureBitmapPalette = VirtualBitmap<TextureBitmapPaletteBuffer<PixelType_, IndexType_, paletteSize_, Alloc_>>;


	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(BitmapEntity<TextureBitmapBuffer<u8>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(BitmapEntity<TextureBitmapPaletteBuffer<u32, u8, 256>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(BitmapEntity<TextureBitmapBuffer<u16>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(BitmapEntity<TextureBitmapBuffer<ColorInt24>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(BitmapEntity<TextureBitmapBuffer<u32>>)

	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(BitmapEntity<TextureBitmapBuffer<Color3_32>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(BitmapEntity<TextureBitmapBuffer<Color32>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(BitmapEntity<TextureBitmapBuffer<Color3_64>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(BitmapEntity<TextureBitmapBuffer<Color64>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(BitmapEntity<TextureBitmapBuffer<float>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(BitmapEntity<TextureBitmapBuffer<double>>)
}
