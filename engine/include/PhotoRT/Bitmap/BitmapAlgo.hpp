/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>

#include <PhotoRT/Core/Bitmap.hpp>

#include <patlib/rectangle.hpp>
#include <patlib/range.hpp>

namespace PhotoRT::BitmapAlgo
{
	[[nodiscard]] constexpr pl::dir2d<if32> fitSizeToAspectRatio(if32 width, if32 height, szt maxNbPixels) noexcept
	{
		const auto totalNbPixels = (szt)width * height;

		if(totalNbPixels <= maxNbPixels)
			return {width, height};

		const double
			ratio = (double)height / (double)width,
			targetWidth = std::sqrt((double)maxNbPixels / ratio),
			targetHeight = targetWidth * ratio;

		return {(if32)targetWidth, (if32)targetHeight};
	}

	template <typename Bitmap_, pl::CParallelPolicy Policy_ = pl::policy::default_policy_t>
	void gammaCorrect(Bitmap_ &b, CReal fromGamma, CReal toGamma, const Policy_ pol = {}) noexcept
	{
		const auto gammaInv = 1 / toGamma;

		b |
			pl::transform
			(
				[&](auto&& c)
				{
					typename Bitmap_::PixelType res;
					convertFromColor(gammaCorrectColor(convertToColor<Color>((typename Bitmap_::PixelType)c), fromGamma, gammaInv), res);
					return res;
				},
				std::identity{}, pol
			);
	}

	template <pl::COutputValueRange<Color> Range_, pl::CParallelPolicy Policy_ = pl::policy::default_policy_t>
		requires rg::sized_range<Range_>
	void fillFromColorDistribution(const auto &/*bitmap*/, Range_ &&range, const IColorDistribution1D &dist, const Policy_ pol = {}) noexcept
	{
		const auto start = PL_FWD(range).begin();
		const Real rangeSize = PL_FWD(range).size();

		PL_FWD(range) | pl::iterator_view |
			pl::for_each
			(
				[&](auto c){ *c = dist(Real(c - start) / rangeSize); },
				std::identity{}, pol
			);
	}

	template <pl::COutputValueRange<Color> Range_, pl::CParallelPolicy Policy_ = pl::policy::default_policy_t>
		requires CBitmapIterator<rg::iterator_t<Range_>>
	void fillFromColorDistribution(const auto &bitmap, Range_ &&range, const IColorDistribution2D &dist, const Policy_ pol = {}) noexcept
	{
		const auto startOffset = range.begin().getOffset();
		const auto endOffset = (range.end() - 1).getOffset();
		const auto size = bitmap.getSize();
		const auto posStart = getPosFromOffset(startOffset, size);
		const auto posEnd = getPosFromOffset(endOffset, size);
		const auto rangeSize = posEnd - posStart + 1;

		PL_FWD(range) | pl::iterator_view |
			pl::for_each
			(
				[&](auto c)
				{
					auto pos = getPosFromOffset(c.getOffset(), size) - posStart;
					*c = dist(Real(pos[0]) / rangeSize[0], Real(pos[1]) / rangeSize[1]);
				},
				std::identity{}, pol
			);
	}

	template <pl::COutputValueRange<Color> Range_, pl::CParallelPolicy Policy_ = pl::policy::default_policy_t>
		requires CBitmapIterator<rg::iterator_t<Range_>>
	void fillFromColorDistribution(const auto &bitmap, Range_ &&range, const IColorDistribution3D &dist, const Policy_ pol = {}) noexcept
	{
		const auto startOffset = range.begin().getOffset();
		const auto endOffset = (range.end() - 1).getOffset();
		const auto size = bitmap.getSize();
		const auto posStart = getPosFromOffset(startOffset, size);
		const auto posEnd = getPosFromOffset(endOffset - 1, size);
		const auto rangeSize = posEnd - posStart + 1;

		PL_FWD(range) | pl::iterator_view |
			pl::for_each
			(
				[&](auto c)
				{
					auto pos = getPosFromOffset(c.getOffset(), size) - posStart;
					*c = dist(Real(pos[0]) / rangeSize[0], Real(pos[1]) / rangeSize[1], Real(pos[2]) / rangeSize[2]);
				},
				std::identity{}, pol
			);
	}

	[[nodiscard]] Color getPixelLinearFilter(const auto &bitmap, Real u, bool tiled = true) noexcept
	{
		const auto width = static_cast<if32>(bitmap.getWidth());
		if32 imageX, imageX2;

		if(tiled)
		{
			u = (u - pl::plfloor(u)) * width;

			imageX = u;

			imageX2 = imageX - 1;
			if(imageX2 < 0)
				imageX2 = width - 1;

			imageX %= width;
			imageX2 %= width;
		}
		else
		{
			u *= width;
			imageX = u;

			imageX2 = imageX - 1;

			if(u < 0 || imageX == 0 || imageX2 >= (width - 1))
				return 0;
		}

		u -= pl::plfloor(u);

		return blend2Colors(
			bitmap.getPixel(imageX2),
			bitmap.getPixel(imageX),
			u
		);
	}

	[[nodiscard]] Color getPixelLinearFilter(const auto &bitmap, Real u, Real v, bool tiled = true) noexcept
	{
		const auto size = static_cast<Point2Di>(bitmap.getSize());
		Point2Di p1, p2;

		// 4 3
		// 2 1
		if(tiled)
		{
			u = (u - pl::plfloor(u)) * size[0];
			v = (v - pl::plfloor(v)) * size[1];

			Point2Di i1{static_cast<Point2Di::value_type>(u), static_cast<Point2Di::value_type>(v)}, i2 = i1 - 1;

			if(i2[0] < 0)
				i2[0] = size[0] - 1;
			if(i2[1] < 0)
				i2[1] = size[1] - 1;

			p1 = i1 % size;
			p2 = i2 % size;
		}
		else
		{
			u *= size[0];
			v *= size[1];

			const Point2Di i1{(Point2Di::value_type)u, (Point2Di::value_type)v}, i2 = i1 - 1;

			if(u < 0 || v < 0 || pl::mask_any(i1 == 0) || pl::mask_any(i2 >= size - 1))
				return 0;

			p1 = i1;
			p2 = i2;
		}

		u -= pl::plfloor(u);
		v -= pl::plfloor(v);

		return blend4Colors(
			bitmap.getPixel(p2[0], p2[1]),
			bitmap.getPixel(p1[0], p2[1]),
			bitmap.getPixel(p2[0], p1[1]),
			bitmap.getPixel(p1[0], p1[1]),
			u, v);
	}

	[[nodiscard]] Color filterPixelBounded(const auto &bitmap, const ColorFilter2D &filter, if32 x, if32 y) noexcept
	{
		const auto fSize = filter.mSize;
		if32
			sfx = 0, sfy = 0,
			lfx = fSize[0] - 1, lfy = fSize[1] - 1;

		if32
			bufferX1 = r_(x) + 0.5 - fSize[0],
			bufferY1 = r_(y) + 0.5 - fSize[1],
			bufferX2, bufferY2;

		Color colorTotal{};

		bufferX2 = bufferX1 + lfx;
		bufferY2 = bufferY1 + lfy;

		if(bufferX1 < 0)
		{
			sfx = -bufferX1;
			bufferX1 = 0;
		}

		if(bufferY1 < 0)
		{
			sfy = -bufferY1;
			bufferY1 = 0;
		}

		const auto size = bitmap.getSize();
		if(bufferX2 >= size[0])
		{
			if32 dif = bufferX2 - size[0] + 1;
			lfx -= dif;
			bufferX2 -= dif;
		}

		if(bufferY2 >= size[1])
		{
			if32 dif = bufferY2 - size[1] + 1;
			lfy -= dif;
			bufferY2 -= dif;
		}

		for(if32 fy = sfy, iy = bufferY1; fy <= lfy; ++fy, ++iy)
			for(if32 fx = sfx, ix = bufferX1; fx <= lfx; ++fx, ++ix)
				colorTotal += bitmap.getPixel(ix, iy) * filter[fy * fSize[0] + fx];

		return colorTotal;
	}

	[[nodiscard]] Color filterPixelUnbounded(const auto &bitmap, const ColorFilter2D &filter, if32 x, if32 y) noexcept
	{
		pl::rectangle<> r{
			{
				static_cast<pl::rectangle<>::value_type>(r_(x) + 0.5 - filter.mSize[0]),
				static_cast<pl::rectangle<>::value_type>(r_(y) + 0.5 - filter.mSize[1])
			},
			pl::size_p{filter.mSize}
		};

		Color colorTotal{};

		plassert(bool(r.p1 >= 0));
		plassert(bool(r.p2 < bitmap.getSize()));

		r.loop_local_offset([&](const auto p, const auto offset){
			colorTotal += bitmap.getPixel(p[0], p[1]) * filter[offset];
		});

		return colorTotal;
	}

	void splatColor(const ColorFilter2D &filter, Dir2Dr filterSize, Point2Dr p, std::invocable<Point2Di, ColorFilter2D::ColorType> auto &&fc) noexcept
	{
		const auto fCellSize = filterSize * 2 * filter.getSizeRec();
		const auto st = p - filterSize + fCellSize * 0.5;

		pl::rectangle<>{{}, pl::size_p{filter.mSize}}.loop_local_offset([&](const auto p, const auto offset){
			PL_FWD(fc)(
				Point2Di{st + p * fCellSize},
				filter[offset]
			);
		});
	}

	template <pl::CParallelPolicy Policy_ = pl::policy::default_policy_t>
	void applyFilter(const auto &bitmap, const ColorFilter2D &filter, auto &intoBitmap, const pl::rectangle<> &rect, const Point2Di dest, bool checkBounds = true, const Policy_ pol = {}) noexcept
	{
		if(checkBounds)
		{
			bitmap.range2d(rect) | pl::iterator_view |
				pl::for_each
				(
					[&](auto&& p)
					{
						const auto thispos = p.getPosition();
						const auto pos = p.getLocalPosition() + dest;
						intoBitmap.setPixel(filterPixelBounded(bitmap, filter, thispos[0], thispos[1]), pos[0], pos[1]);
					},
					std::identity{}, pol
				);
		}
		else
		{
			bitmap.range2d(rect) | pl::iterator_view |
				pl::for_each
				(
					[&](auto&& p)
					{
						const auto thispos = p.getPosition();
						const auto pos = p.getLocalPosition() + dest;
						intoBitmap.setPixel(filterPixelUnbounded(bitmap, filter, thispos[0], thispos[1]), pos[0], pos[1]);
					},
					std::identity{}, pol
				);
		}
	}

	template <pl::CParallelPolicy Policy_ = pl::policy::default_policy_t>
	void applyTonemap(const auto &bitmap, const auto &toneMapper, auto &intoBitmap, const pl::rectangle<> &rect, const Point2Di dest, const Policy_ pol = {}) noexcept
	{
		const auto &tm = toneMapper.colorInter();

		bitmap.range2d(rect) | pl::iterator_view |
			pl::for_each
			(
				[&](auto&& p)
				{
					const auto pos = p.getLocalPosition() + dest;
					intoBitmap.setPixel(tm((Color)*p), pos[0], pos[1]);
				},
				std::identity{}, pol
			);
	}

	template <pl::CParallelPolicy Policy_ = pl::policy::default_policy_t>
	void applyFilterAndTonemap(const auto &bitmap, const ColorFilter2D &filter, const auto &toneMapper, auto &intoBitmap, const pl::rectangle<> &rect, const Point2Di dest, bool checkBounds = true, const Policy_ pol = {}) noexcept
	{
		const auto &tm = toneMapper.colorInter();

		if(checkBounds)
		{
			bitmap.range2d(rect) | pl::iterator_view |
				pl::for_each
				(
					[&](auto&& p)
					{
						const auto thispos = p.getPosition();
						const auto pos = p.getLocalPosition() + dest;
						intoBitmap.setPixel(tm(filterPixelBounded(bitmap, filter, thispos[0], thispos[1])), pos[0], pos[1]);
					},
					std::identity{}, pol
				);
		}
		else
		{
			bitmap.range2d(rect) | pl::iterator_view |
				pl::for_each
				(
					[&](auto&& p)
					{
						const auto thispos = p.getPosition();
						const auto pos = p.getLocalPosition() + dest;
						intoBitmap.setPixel(tm(filterPixelUnbounded(bitmap, filter, thispos[0], thispos[1])), pos[0], pos[1]);
					},
					std::identity{}, pol
				);
		}
	}
}
