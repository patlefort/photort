/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "BitmapBuffer_fwd.hpp"

#include <PhotoRT/Core/Base.hpp>

#include <patlib/task.hpp>

#ifdef PRT_IPC
	#include <boost/interprocess/containers/vector.hpp>
#endif

namespace PhotoRT::ImplBitmap
{
	#ifdef PRT_IPC
		template <typename T_, typename Alloc_>
		using BitmapVector = boost::interprocess::vector<T_, Alloc_>;
	#else
		template <typename T_, typename Alloc_>
		using BitmapVector = std::vector<T_, Alloc_>;
	#endif

	// Bitmap buffer implementations
	template <typename T_>
	class BitmapBufferEmpty
	{
	public:
		using PixelType = T_;

		using value_type = T_;
		using size_type = szt;
		using difference_type = if32;
		using reference = T_&;
		using const_reference = const T_&;
		using allocator_type = std::allocator<T_>;

		void allocate(Dir3Di) {}
		void allocate(Dir2Di) {}
		void free() noexcept {}
		void shrink_to_fit() {}

		[[nodiscard]] PixelType at(szt) { return {}; }
		[[nodiscard]] PixelType at(szt) const { return {}; }
		[[nodiscard]] PixelType front() noexcept { return {}; }
		[[nodiscard]] PixelType front() const noexcept { return {}; }
		[[nodiscard]] PixelType back() noexcept { return {}; }
		[[nodiscard]] PixelType back() const noexcept { return {}; }
		[[nodiscard]] size_type size() const noexcept { return 0; }
		[[nodiscard]] bool empty() const noexcept { return false; }

		[[nodiscard]] Dir3Di getSize() const noexcept { return {}; }
		[[nodiscard]] szt getMemSize() const noexcept { return 0; }

		static constexpr bool IsContiguous = true;

		void set_zero() noexcept {}

		[[nodiscard]] PixelType operator[] (const szt) const noexcept { return PixelType{}; }

		[[nodiscard]] auto get(Point3Di) const noexcept { return PixelType{}; }

		[[nodiscard]] void *getRawData() const noexcept { return nullptr; }

		void serialize(std::ostream &) const {}
		void unserialize(std::istream &) {}

	};

	[[nodiscard]] inline szt pointToOffset(Point2Di p, Point2Di mSize) noexcept
		{ return mSize[0] * p[1] + p[0]; }

	[[nodiscard]] inline Point2Di offsetToPoint(szt offset, Point2Di mSize) noexcept
	{
		return {if32(offset % mSize[0]), if32(offset / mSize[0])};
	}

	[[nodiscard]] inline szt pointToOffset(Point3Di p, Dir3Di mSize, szt mSize2d) noexcept
		{ return mSize2d * p[2] + mSize[0] * p[1] + p[0]; }

	[[nodiscard]] inline Point3Di offsetToPoint(szt offset, Dir3Di mSize, szt mSize2d) noexcept
	{
		return {if32(offset % mSize[0]), if32(offset % mSize2d) / mSize[0], if32(offset / mSize2d)};
	}

	void readHeaders(auto &buf, std::istream &is)
	{
		Dir3Di dim{};
		is >> dim >> pl::io::skip_one;
		buf.allocate(dim);
	}

	void writeHeaders(const auto &buf, std::ostream &os)
	{
		os << buf.getSize() << '\n';
	}

	auto pixelSerializer(auto &pixels)
	{
		return pl::io::range{pl::io::mode_binary, pl::io::empty_delim, rgv::all(pixels)};
	}

	template <typename T_, typename Alloc_ = std::allocator<T_>>
	class BitmapBuffer
	{
	private:
		szt mMemSize = 0, mNbPixels = 0, mSize2d = 0;
		Dir3Di mSize{};

	public:
		using PixelType = T_;
		using AllocatorType = Alloc_;
		using BitmapVectorType = BitmapVector<T_, Alloc_>;
		static constexpr bool IsContiguous = true;

		using value_type = T_;
		using size_type = szt;
		using difference_type = if32;
		using reference = T_&;
		using const_reference = const T_&;
		using allocator_type = AllocatorType;

		BitmapVectorType mPixels;

		/*static constexpr auto AllocIndex = upstream_allocator_count<typename BitmapVectorType::allocator_type> - upstream_allocator_count<Alloc_>;

		explicit BitmapBuffer(const Alloc_ &alloc = {}) noexcept
			: mPixels{ pl::make_allocator(type_v<typename BitmapVectorType::allocator_type>, std::in_place_index<AllocIndex>, alloc) } {}

		[[nodiscard]] auto getAllocator() const { return pl::get_allocator(mPixels.get_allocator(), std::in_place_index<AllocIndex>); }*/

		explicit BitmapBuffer(const Alloc_ &alloc = {}) noexcept
			: mPixels{alloc} {}

		[[nodiscard]] auto getAllocator() const { return mPixels.get_allocator(); }

		void allocate(Dir3Di dim)
		{
			mSize = dim;
			mSize2d = dim[0] * dim[1];
			mNbPixels = mSize2d * dim[2];
			mMemSize = mNbPixels * sizeof(T_);
			mPixels.resize(mNbPixels);
		}

		void allocate(Dir2Di dim)
		{
			Dir3Di newDim{dim};
			newDim.set(1, size_v<2>);
			allocate(newDim);
		}

		void free() noexcept
		{
			mPixels.clear();

			mSize = 0;
			mMemSize = 0;
			mNbPixels = 0;
			mSize2d = 0;
		}

		void shrink_to_fit() { mPixels.shrink_to_fit(); }

		[[nodiscard]] auto getSize() const noexcept { return mSize; }
		[[nodiscard]] auto getMemSize() const noexcept { return mMemSize; }
		[[nodiscard]] auto getNbPixels() const noexcept { return mNbPixels; }

		[[nodiscard]] decltype(auto) at(szt offset) { return mPixels.at(offset); }
		[[nodiscard]] decltype(auto) at(szt offset) const { return mPixels.at(offset); }
		[[nodiscard]] decltype(auto) front() noexcept { return mPixels.front(); }
		[[nodiscard]] decltype(auto) front() const noexcept { return mPixels.front(); }
		[[nodiscard]] decltype(auto) back() noexcept { return mPixels.back(); }
		[[nodiscard]] decltype(auto) back() const noexcept { return mPixels.back(); }
		[[nodiscard]] size_type size() const noexcept { return getNbPixels(); }
		[[nodiscard]] bool empty() const noexcept { return mPixels.empty(); }

		[[nodiscard]] void *getRawData() const noexcept { return (void *)mPixels.data(); }

		void set_zero() noexcept
		{
			mPixels | pl::zero_range;
		}

		[[nodiscard]] const auto &operator[] (const szt offset) const noexcept { return mPixels[offset]; }
		[[nodiscard]] auto &operator[] (const szt offset) noexcept { return mPixels[offset]; }

		[[nodiscard]] const auto &get(Point2Di p) const noexcept
			{ return (*this)[pointToOffset(p, static_cast<Point2Di>(mSize))]; }
		[[nodiscard]] auto &get(Point2Di p) noexcept
			{ return (*this)[pointToOffset(p, static_cast<Point2Di>(mSize))]; }

		[[nodiscard]] const auto &get(Point3Di p) const noexcept
			{ return (*this)[pointToOffset(p, mSize, mSize2d)]; }
		[[nodiscard]] auto &get(Point3Di p) noexcept
			{ return (*this)[pointToOffset(p, mSize, mSize2d)]; }

		void serialize(std::ostream &outputStream) const
		{
			writeHeaders(*this, outputStream);

			if(!mPixels.empty())
				outputStream << pixelSerializer(mPixels);
		}

		void unserialize(std::istream &inputStream)
		{
			readHeaders(*this, inputStream);

			if(!mPixels.empty())
				inputStream >> pixelSerializer(mPixels);
		}
	};

	template <typename T_, typename Alloc_ = std::allocator<T_>, if32 TileSize_ = 4>
	class BitmapBufferTiled
	{
	private:
		szt mMemSize = 0, mNbPixels = 0, mSize2d = 0, mRealNbPixels = 0, mRealSize2d = 0;
		Dir3Di mSize{};
		Dir2Di mNbTiles{}, mRealSize{};

	public:
		using PixelType = T_;
		static constexpr if32 tileSize = TileSize_;
		using AllocatorType = Alloc_;
		static constexpr bool IsContiguous = false;

		using value_type = T_;
		using size_type = szt;
		using difference_type = if32;
		using reference = T_&;
		using const_reference = const T_&;
		using allocator_type = AllocatorType;

		BitmapVector<T_, Alloc_> mPixels;

		BitmapBufferTiled() = default;
		explicit BitmapBufferTiled(const Alloc_ &alloc) noexcept
			: mPixels{alloc} {}

		[[nodiscard]] auto getAllocator() const { return mPixels.get_allocator(); }

		void allocate(Dir3Di dim)
		{
			mSize = dim;
			mNbTiles = static_cast<Dir2Di>(dim);
			mNbTiles = mNbTiles / TileSize_ + pl::select(Dir2Di{1}, Dir2Di{}, (mNbTiles % TileSize_ > 0));
			mRealSize = mNbTiles * TileSize_;
			mRealSize2d = mRealSize[0] * mRealSize[1];
			mSize2d = dim[0] * dim[1];
			mNbPixels = mSize2d * dim[2];
			mRealNbPixels = mRealSize2d * dim[2];
			mMemSize = mRealNbPixels * sizeof(T_);
			mPixels.resize(mRealNbPixels);
		}

		void allocate(Dir2Di dim)
		{
			Dir3Di newDim{dim};
			newDim.set(1, size_v<2>);
			allocate(newDim);
		}

		void free() noexcept
		{
			mPixels.clear(mRealNbPixels);
			mSize = 0;
			mMemSize = 0;
			mNbPixels = 0;
			mSize2d = 0;
			mRealNbPixels = 0;
			mNbTiles = 0;
			mRealSize = 0;
			mRealSize2d = 0;
		}

		void shrink_to_fit() { mPixels.shrink_to_fit(); }

		[[nodiscard]] auto getSize() const noexcept { return mSize; }
		[[nodiscard]] auto getMemSize() const noexcept { return mMemSize; }
		[[nodiscard]] auto getNbPixels() const noexcept { return mNbPixels; }

		[[nodiscard]] decltype(auto) at(szt offset) { return mPixels.at(calcOffset(offsetToPoint(offset, mSize, mSize2d))); }
		[[nodiscard]] decltype(auto) at(szt offset) const { return mPixels.at(calcOffset(offsetToPoint(offset, mSize, mSize2d))); }
		[[nodiscard]] decltype(auto) front() noexcept { return mPixels.front(); }
		[[nodiscard]] decltype(auto) front() const noexcept { return mPixels.front(); }
		[[nodiscard]] decltype(auto) back() noexcept { return mPixels.back(); }
		[[nodiscard]] decltype(auto) back() const noexcept { return mPixels.back(); }
		[[nodiscard]] size_type size() const noexcept { return getNbPixels(); }
		[[nodiscard]] bool empty() const noexcept { return mPixels.empty(); }

		[[nodiscard]] void *getRawData() const noexcept { return (void *)mPixels.data(); }

		void set_zero() noexcept
		{
			mPixels | pl::zero_range;
		}

		[[nodiscard]] const auto &operator[] (const szt offset) const noexcept { return mPixels[calcOffset(offsetToPoint(offset, mSize, mSize2d))]; }
		[[nodiscard]] auto &operator[] (const szt offset) noexcept { return mPixels[calcOffset(offsetToPoint(offset, mSize, mSize2d))]; }

		[[nodiscard]] const auto &get(Point2Di p) const noexcept
			{ return (*this)[calcOffset(p)]; }
		[[nodiscard]] auto &get(Point2Di p) noexcept
			{ return (*this)[calcOffset(p)]; }

		[[nodiscard]] const auto &get(Point3Di p) const noexcept
			{ return (*this)[calcOffset(p)]; }
		[[nodiscard]] auto &get(Point3Di p) noexcept
			{ return (*this)[calcOffset(p)]; }

		[[nodiscard]] auto getPixel(const szt offset) const noexcept
			{ return (*this)[offset]; }
		void setPixel(PixelType color, const szt offset) noexcept
			{ return mPixels[calcOffset(offsetToPoint(offset, mSize, mSize2d))] = color; }

		void serialize(std::ostream &outputStream) const
		{
			writeHeaders(*this, outputStream);

			if(!mPixels.empty())
				outputStream << pixelSerializer(mPixels);
		}

		void unserialize(std::istream &inputStream)
		{
			readHeaders(*this, inputStream);

			if(!mPixels.empty())
				inputStream >> pixelSerializer(mPixels);
		}

	private:

		[[nodiscard]] szt calcOffset(Point2Di p) const noexcept
		{
			const Point2Di tile {p[0] / TileSize_, p[1] / TileSize_}, tilePos {p[0] % TileSize_, p[1] % TileSize_};
			const szt offset = (tile[1] * mNbTiles[0] + tile[0]) * (TileSize_ /opow2) + tilePos[1] * TileSize_ + tilePos[0];

			return offset;
		}

		[[nodiscard]] szt calcOffset(Point3Di p) const noexcept
		{
			const Point2Di tile {p[0] / TileSize_, p[1] / TileSize_}, tilePos {p[0] % TileSize_, p[1] % TileSize_};
			const szt offset = (tile[1] * mNbTiles[0] + tile[0]) * (TileSize_ /opow2) + tilePos[1] * TileSize_ + tilePos[0] + p[2] * mRealSize2d;

			return offset;
		}
	};

	template <typename T_, typename PackedType_, typename Alloc_ = std::allocator<T_>>
	class BitmapAlignedBuffer
	{
	private:
		szt mMemSize = 0, mNbPixels = 0, mSize2d = 0;
		Dir3Di mSize{};
		szt mAlignedSize = 0;

		using AlignAlloc = pl::aligned_allocator<T_, Alloc_>;

	public:

		using PixelType = T_;
		using PackedType = PackedType_;
		using AllocatorType = Alloc_;
		static constexpr bool IsContiguous = true;

		using value_type = T_;
		using size_type = szt;
		using difference_type = if32;
		using reference = T_&;
		using const_reference = const T_&;
		using allocator_type = AllocatorType;

		BitmapVector<T_, AlignAlloc> mPixels;

		explicit BitmapAlignedBuffer(const Alloc_ &alloc = {}) noexcept
			: mPixels(typename decltype(mPixels)::allocator_type{AlignAlloc{alloc, alignof(PackedType_)}}) {}

		[[nodiscard]] auto getAllocator() const { return mPixels.get_allocator().upstream_allocator(); }

		void allocate(Dir3Di dim)
		{
			mSize = dim;

			mSize2d = dim[0] * dim[1];
			mNbPixels = mSize2d * dim[2];
			mMemSize = mNbPixels * sizeof(T_);
			mAlignedSize = mMemSize / sizeof(PackedType_) + (mMemSize % sizeof(PackedType_) > 0 ? 1 : 0);
			mMemSize = mAlignedSize * sizeof(PackedType_);
			mPixels.resize(mAlignedSize * PackedType_::NbValues);
		}

		void allocate(Dir2Di dim)
		{
			Dir3Di newDim{dim};
			newDim.set(1, size_v<2>);
			allocate(newDim);
		}

		void free() noexcept
		{
			mPixels.clear();
			mSize = 0;
			mMemSize = 0;
			mAlignedSize = 0;
			mNbPixels = 0;
			mSize2d = 0;
		}

		void shrink_to_fit() { mPixels.shrink_to_fit(); }

		[[nodiscard]] auto getSize() const noexcept { return mSize; }
		[[nodiscard]] auto getNbPixels() const noexcept { return mNbPixels; }
		[[nodiscard]] auto getMemSize() const noexcept { return mMemSize; }
		[[nodiscard]] auto getAlignedSize() const noexcept { return mAlignedSize; }

		[[nodiscard]] decltype(auto) at(szt offset) { return mPixels.at(offset); }
		[[nodiscard]] decltype(auto) at(szt offset) const { return mPixels.at(offset); }
		[[nodiscard]] decltype(auto) front() noexcept { return mPixels.front(); }
		[[nodiscard]] decltype(auto) front() const noexcept { return mPixels.front(); }
		[[nodiscard]] decltype(auto) back() noexcept { return mPixels.back(); }
		[[nodiscard]] decltype(auto) back() const noexcept { return mPixels.back(); }
		[[nodiscard]] size_type size() const noexcept { return getNbPixels(); }
		[[nodiscard]] bool empty() const noexcept { return mPixels.empty(); }

		[[nodiscard]] void *getRawData() const noexcept { return (void *)mPixels.data(); }

		void set_zero() noexcept
		{
			mPixels | pl::zero_range;
		}

		[[nodiscard]] const auto &operator[] (const szt offset) const noexcept { return mPixels[offset]; }
		[[nodiscard]] auto &operator[] (const szt offset) noexcept { return mPixels[offset]; }

		[[nodiscard]] const auto &get(Point2Di p) const noexcept
			{ return (*this)[pointToOffset(p, static_cast<Point2Di>(mSize))]; }
		[[nodiscard]] auto &get(Point2Di p) noexcept
			{ return (*this)[pointToOffset(p, static_cast<Point2Di>(mSize))]; }

		[[nodiscard]] const auto &get(Point3Di p) const noexcept
			{ return (*this)[pointToOffset(p, mSize, mSize2d)]; }
		[[nodiscard]] auto &get(Point3Di p) noexcept
			{ return (*this)[pointToOffset(p, mSize, mSize2d)]; }

		void serialize(std::ostream &outputStream) const
		{
			writeHeaders(*this, outputStream);

			if(!mPixels.empty())
				outputStream << pixelSerializer(mPixels);
		}

		void unserialize(std::istream &inputStream)
		{
			readHeaders(*this, inputStream);

			if(!mPixels.empty())
				inputStream >> pixelSerializer(mPixels);
		}
	};

	template <typename T_, typename PackedType_, typename nbChannels_ = size_c<4>, typename Alloc_ = std::allocator<T_>>
	class BitmapPackedBuffer
	{
	private:
		Dir3Di mSize{};
		szt mMemSize = 0, mSizePacked = 0, mSizePackedPixels = 0, mNbPixels = 0, mSize2d = 0;

		using OffsetReferenceType = BitmapOffsetReference<BitmapPackedBuffer, T_>;

	public:

		using PixelType = T_;
		static constexpr auto nbChannels = nbChannels_::value;
		using PackedType = PackedType_;
		static constexpr bool IsContiguous = false;

		using PixelValueType = typename PackedType_::value_type;
		using AllocatorType = Alloc_;
		using AlignAlloc = pl::aligned_allocator<PixelValueType, Alloc_>;

		using value_type = T_;
		using size_type = szt;
		using difference_type = if32;
		using reference = OffsetReferenceType;
		using const_reference = value_type;
		using allocator_type = AllocatorType;

		BitmapVector<PixelValueType, AlignAlloc> mPixels;

		explicit BitmapPackedBuffer(const Alloc_ &alloc = {}) noexcept
			: mPixels(typename decltype(mPixels)::allocator_type{AlignAlloc{alloc, alignof(PackedType_)}}) {}

		[[nodiscard]] decltype(auto) getAllocator() const { return mPixels.get_allocator().upstream_allocator(); }

		void allocate(Dir3Di dim)
		{
			mSize = dim;
			mSize2d = dim[0] * dim[1];
			mNbPixels = mSize2d * dim[2];
			mSizePackedPixels = mNbPixels / PackedType_::NbValues + (mNbPixels % PackedType_::NbValues > 0 ? 1 : 0);
			mSizePacked = mSizePackedPixels * nbChannels;
			mMemSize = mSizePacked * sizeof(PackedType_);

			mPixels.resize(mSizePacked * PackedType_::NbValues);
		}

		void allocate(Dir2Di dim)
		{
			Dir3Di newDim{dim};
			newDim.set(1, size_v<2>);
			allocate(newDim);
		}

		void free() noexcept
		{
			mPixels.clear();
			mSize = 0;
			mMemSize = 0;
			mSizePacked = 0;
			mSizePackedPixels = 0;
			mSize2d = 0;
			mNbPixels = 0;
		}

		void shrink_to_fit() { mPixels.shrink_to_fit(); }

		[[nodiscard]] auto getSize() const noexcept { return mSize; }
		[[nodiscard]] auto getNbPixels() const noexcept { return mNbPixels; }
		[[nodiscard]] auto getMemSize() const noexcept { return mMemSize; }
		[[nodiscard]] auto getSizePacked() const noexcept { return mSizePacked; }
		[[nodiscard]] auto getSizePackedPixels() const noexcept { return mSizePackedPixels; }

		[[nodiscard]] decltype(auto) at(szt offset) { if(offset >= size()) throw std::out_of_range("Offset out of bounds of bitmap."); return (*this)[offset]; }
		[[nodiscard]] decltype(auto) at(szt offset) const { if(offset >= size()) throw std::out_of_range("Offset out of bounds of bitmap."); return (*this)[offset]; }
		[[nodiscard]] decltype(auto) front() noexcept { return (*this)[0]; }
		[[nodiscard]] decltype(auto) front() const noexcept { return (*this)[0]; }
		[[nodiscard]] decltype(auto) back() noexcept { return (*this)[size() - 1]; }
		[[nodiscard]] decltype(auto) back() const noexcept { return (*this)[size() - 1]; }
		[[nodiscard]] size_type size() const noexcept { return getNbPixels(); }
		[[nodiscard]] bool empty() const noexcept { return mPixels.empty(); }

		[[nodiscard]] void *getRawData() const noexcept { return (void *)mPixels.data(); }

		void set_zero() noexcept
		{
			mPixels | pl::zero_range;
		}

		[[nodiscard]] constexpr szt getOffsetStart(szt offset) const noexcept
			{ return (offset / PackedType_::NbValues) * (nbChannels * PackedType_::NbValues) + (offset % PackedType_::NbValues); }

		[[nodiscard]] auto operator[] (const szt offset) const noexcept
		{
			const auto offsetStart = getOffsetStart(offset);
			PixelType res;

			{
				pl::simd::scoped_vector_modify mres{res};

				pl::index_sequence_loop(size_v<nbChannels>, [&](auto&& i){
					mres[i] = mPixels[offsetStart + i*PackedType_::NbValues];
				});
			}

			return res;
		}
		[[nodiscard]] auto operator[] (const szt offset) noexcept { return OffsetReferenceType{*this, offset}; }

		[[nodiscard]] auto get(Point2Di p) const noexcept
			{ return (*this)[pointToOffset(p, static_cast<Point2Di>(mSize))]; }
		[[nodiscard]] auto get(Point2Di p) noexcept
			{ return (*this)[pointToOffset(p, static_cast<Point2Di>(mSize))]; }

		[[nodiscard]] auto get(Point3Di p) const noexcept
			{ return (*this)[pointToOffset(p, mSize, mSize2d)]; }
		[[nodiscard]] auto get(Point3Di p) noexcept
			{ return (*this)[pointToOffset(p, mSize, mSize2d)]; }

		[[nodiscard]] auto getPixel(const szt offset) const noexcept
			{ return (*this)[offset]; }
		void setPixel(PixelType color, const szt offset) noexcept
		{
			const auto offsetStart = getOffsetStart(offset);
			const auto &car = pl::simd::vector_access(color);

			pl::index_sequence_loop(size_v<nbChannels>, [&](auto&& i){
				mPixels[offsetStart + i*PackedType_::NbValues] = car[i];
			});
		}

		void serialize(std::ostream &outputStream) const
		{
			writeHeaders(*this, outputStream);

			if(!mPixels.empty())
				outputStream << pixelSerializer(mPixels);
		}

		void unserialize(std::istream &inputStream)
		{
			readHeaders(*this, inputStream);

			if(!mPixels.empty())
				inputStream >> pixelSerializer(mPixels);
		}

	};

	template <typename T_, typename IndexType_, typename paletteSize_ = size_c<256>, typename Alloc_ = std::allocator<T_>>
	class BitmapPaletteBuffer
	{
	private:
		Dir3Di mSize{};
		szt mMemSize = 0, mNbPixels = 0, mSize2d = 0;

		using OffsetReferenceType = BitmapOffsetReference<BitmapPaletteBuffer, T_>;

	public:
		using PixelType = T_;
		using IndexType = IndexType_;
		static constexpr szt paletteSize = paletteSize_{};
		using AllocatorType = typename std::allocator_traits<Alloc_>::template rebind_alloc<IndexType_>;
		using PaletteAllocatorType = Alloc_;
		static constexpr bool IsContiguous = true;

		using value_type = T_;
		using size_type = szt;
		using difference_type = if32;
		using reference = OffsetReferenceType;
		using const_reference = value_type;
		using allocator_type = AllocatorType;

		BitmapVector<IndexType_, AllocatorType> mPixels;
		BitmapVector<T_, PaletteAllocatorType> mPalette;

		explicit BitmapPaletteBuffer(const Alloc_ &alloc = {}) noexcept
			: mPixels{typename decltype(mPixels)::allocator_type{alloc}}, mPalette{alloc} {}
		BitmapPaletteBuffer(const BitmapPaletteBuffer &other) = default;
		BitmapPaletteBuffer(BitmapPaletteBuffer &&other) = default;

		BitmapPaletteBuffer &operator=(const BitmapPaletteBuffer &other)
		{
			BitmapPaletteBuffer n{mPixels.get_allocator()};

			n.allocate(other.getSize());

			if(!other.mPixels.empty() && &other.mPixels[0] != &n.mPixels[0])
				rg::copy(other.mPixels, n.mPixels.begin());
			if(!other.mPalette.empty() && &other.mPalette[0] != &n.mPalette[0])
				rg::copy(other.mPalette, n.mPalette.begin());

			return (*this = std::move(n));
		}

		BitmapPaletteBuffer &operator=(BitmapPaletteBuffer &&other) = default;

		void allocate(Dir3Di dim)
		{
			mSize = dim;
			mSize2d = dim[0] * dim[1];
			mNbPixels = mSize2d * dim[2];
			mMemSize = mNbPixels * sizeof(IndexType_);
			mPixels.resize(mNbPixels);
			mPalette.resize(paletteSize);
		}

		[[nodiscard]] decltype(auto) getAllocator() const { return mPixels.get_allocator(); }
		[[nodiscard]] decltype(auto) getAllocatorPalette() const { return mPalette.get_allocator(); }

		void allocate(Dir2Di dim)
		{
			Dir3Di newDim{dim};
			newDim.set(1, size_v<2>);
			allocate(newDim);
		}

		void free() noexcept
		{
			mPixels.clear();
			mPalette.clear();
			mSize = 0;
			mMemSize = 0;
			mSize2d = 0;
			mNbPixels = 0;
		}

		void shrink_to_fit() { mPixels.shrink_to_fit(); mPalette.shrink_to_fit(); }

		[[nodiscard]] auto getSize() const noexcept { return mSize; }
		[[nodiscard]] auto getNbPixels() const noexcept { return mNbPixels; }
		[[nodiscard]] auto getMemSize() const noexcept { return mMemSize; }

		[[nodiscard]] decltype(auto) at(szt offset) { if(offset >= size()) throw std::out_of_range("Offset out of bounds of bitmap."); return (*this)[offset]; }
		[[nodiscard]] decltype(auto) at(szt offset) const { if(offset >= size()) throw std::out_of_range("Offset out of bounds of bitmap."); return (*this)[offset]; }
		[[nodiscard]] decltype(auto) front() noexcept { return (*this)[0]; }
		[[nodiscard]] decltype(auto) front() const noexcept { return (*this)[0]; }
		[[nodiscard]] decltype(auto) back() noexcept { return (*this)[size() - 1]; }
		[[nodiscard]] decltype(auto) back() const noexcept { return (*this)[size() - 1]; }
		[[nodiscard]] size_type size() const noexcept { return getNbPixels(); }
		[[nodiscard]] bool empty() const noexcept { return mPixels.empty(); }

		[[nodiscard]] void *getRawData() const noexcept { return (void *)mPixels.data(); }

		void set_zero() noexcept
		{
			mPixels | pl::zero_range;
		}

		[[nodiscard]] auto operator[] (const szt offset) const noexcept { return mPalette[mPixels[offset]]; }
		[[nodiscard]] auto operator[] (const szt offset) noexcept { return OffsetReferenceType{*this, offset}; }

		[[nodiscard]] auto get(Point2Di p) const noexcept
			{ return (*this)[pointToOffset(p, static_cast<Point2Di>(mSize))]; }
		[[nodiscard]] auto get(Point2Di p) noexcept
			{ return (*this)[pointToOffset(p, static_cast<Point2Di>(mSize))]; }

		[[nodiscard]] auto get(Point3Di p) const noexcept
			{ return (*this)[pointToOffset(p, mSize, mSize2d)]; }
		[[nodiscard]] auto get(Point3Di p) noexcept
			{ return (*this)[pointToOffset(p, mSize, mSize2d)]; }

		[[nodiscard]] auto getPixel(const szt offset) const noexcept
			{ return (*this)[offset]; }
		void setPixel(PixelType, const szt) noexcept
			{ /*mPixels[offset] = findClosestColor(color);*/ }

		void serialize(std::ostream &outputStream) const
		{
			writeHeaders(*this, outputStream);

			if(!mPixels.empty())
			{
				outputStream << pixelSerializer(mPixels);
				outputStream << pixelSerializer(mPalette);
			}
		}

		void unserialize(std::istream &inputStream)
		{
			readHeaders(*this, inputStream);

			if(!mPixels.empty())
			{
				inputStream >> pixelSerializer(mPixels);
				inputStream >> pixelSerializer(mPalette);
			}
		}

		/*IndexType_ findClosestColor(const PixelType &color) const noexcept
		{
			typename CT_::value_type minDif = pl::Constants<typename CT_::value_type>::infinity(), curDif;
			IndexType_ index = 0;

			for(uf32 i=0; i<mPalette.size(); ++i)
			{
				curDif = ((color - convertToColor<CT_>(mPalette[i])) /oabs /ohadd)[size_v<0>];

				if(curDif < minDif)
				{
					index = i;
					curDif = minDif;
				}
			}

			return index;
		}*/
	};


	template <typename T1_, typename T2_, typename T3_, typename OP_, bool GetIntoPixel_ = false, pl::CParallelPolicy Policy_ = pl::policy::default_policy_t>
	void op(const T1_ &b1, const T2_ &b2, T3_ &into, OP_&& fc, const bool_c<GetIntoPixel_> = {}, const Policy_ pol = {}) noexcept
	{
		mir( pl::plmin(b1.getNbPixels(), b2.getNbPixels()) ) |
			pl::for_each
			(
				[&](const auto i)
				{
					if constexpr(GetIntoPixel_)
						into[i] = PL_FWD(fc)(into[i], b1[i], b2[i]);
					else
						into[i] = PL_FWD(fc)(b1[i], b2[i]);
				},
				std::identity{}, pol
			);
	}

	template <typename T1_, typename T2_>
	void copy(const T1_ &from, T2_ &into) noexcept
	{
		from.mPixels | rgv::take_exactly(pl::plmin(into.getNbPixels(), from.getNbPixels())) |
			pl::copy
			(
				into.mPixels.begin(),
				pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
			);
	}

	template <typename... T_>
	void copy(const BitmapPaletteBuffer<T_...> &from, BitmapPaletteBuffer<T_...> &into) noexcept
	{
		from.mPixels | rgv::take_exactly(pl::plmin(into.getNbPixels(), from.getNbPixels())) |
			pl::copy
			(
				into.mPixels.begin(),
				pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
			);

		std::copy(from.mPalette.begin(), from.mPalette.end(), into.mPalette.begin());
	}

	template <typename... T_>
	void copy(const BitmapAlignedBuffer<T_...> &from, BitmapAlignedBuffer<T_...> &into) noexcept
	{
		using PT = typename BitmapAlignedBuffer<T_...>::PackedType;

		const PT *packedColors1 = reinterpret_cast<const PT *>(from.mPixels.data());
		PT *packedColorsInto = reinterpret_cast<PT *>(into.mPixels.data());

		std::span{packedColors1, pl::plmin(from.getAlignedSize(), into.getAlignedSize())} |
			pl::copy
			(
				packedColorsInto,
				pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
			);
	}


	template <typename... T_, typename OP_, bool GetIntoPixel_ = false, pl::CParallelPolicy Policy_ = pl::policy::default_policy_t>
	void op(const BitmapAlignedBuffer<T_...> &b1, const BitmapAlignedBuffer<T_...> &b2, BitmapAlignedBuffer<T_...> &into, OP_&& fc, const bool_c<GetIntoPixel_> = {}, const Policy_ pol = {}) noexcept
	{
		using PT = typename BitmapAlignedBuffer<T_...>::PackedType;

		const PT *packedColors1 = reinterpret_cast<const PT *>(b1.mPixels.data());
		const PT *packedColors2 = reinterpret_cast<const PT *>(b2.mPixels.data());
		PT *packedColorsInto = reinterpret_cast<PT *>(into.mPixels.data());

		if constexpr(GetIntoPixel_)
		{
			auto ospan = rgv::zip( std::span{packedColors2, b2.getAlignedSize()}, std::span{packedColorsInto, into.getAlignedSize()} );

			std::span{packedColors1, b2.getAlignedSize()} |
				pl::transform
				(
					ospan,
					packedColorsInto,
					[&](auto&& p1, auto&& p2)
					{
						return PL_FWD(fc)(
							std::get<1>(PL_FWD(p2)),
							PL_FWD(p1),
							std::get<0>(PL_FWD(p2))
						);
					},
					std::identity{},
					std::identity{},
					pol
				);
		}
		else
		{
			std::span{packedColors1, pl::plmin(b1.getAlignedSize(), b2.getAlignedSize())} |
				pl::transform
				(
					std::span{packedColors2, b2.getAlignedSize()},
					packedColorsInto,
					PL_FWD(fc),
					std::identity{},
					std::identity{},
					pol
				);
		}
	}


	template <typename... T_>
	void copy(const BitmapPackedBuffer<T_...> &from, BitmapPackedBuffer<T_...> &into) noexcept
	{
		using PT = typename BitmapPackedBuffer<T_...>::PackedType;

		const PT *packedColors1 = reinterpret_cast<const PT *>(from.mPixels.data());
		PT *packedColorsInto = reinterpret_cast<PT *>(into.mPixels.data());

		std::span{packedColors1, pl::plmin(from.getSizePackedPixels(), into.getSizePackedPixels())} |
			pl::copy
			(
				packedColorsInto,
				pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128)
			);
	}

	template <typename... T_, typename OP_, bool GetIntoPixel_ = false, pl::CParallelPolicy Policy_ = pl::policy::default_policy_t>
	void op(const BitmapPackedBuffer<T_...> &b1, const BitmapPackedBuffer<T_...> &b2, BitmapPackedBuffer<T_...> &into, OP_&& fc, const bool_c<GetIntoPixel_> = {}, const Policy_ pol = {}) noexcept
	{
		using PT = pl::simd::basic_vector<typename BitmapPackedBuffer<T_...>::PackedType, size_c<BitmapPackedBuffer<T_...>::nbChannels>, typename BitmapPackedBuffer<T_...>::PixelType::tag>;

		const PT *packedColors1 = reinterpret_cast<const PT *>(b1.mPixels.data());
		const PT *packedColors2 = reinterpret_cast<const PT *>(b2.mPixels.data());
		PT *packedColorsInto = reinterpret_cast<PT *>(into.mPixels.data());

		if constexpr(GetIntoPixel_)
		{
			auto ospan = rgv::zip( std::span{packedColors2, b2.getSizePackedPixels()}, std::span{packedColorsInto, into.getSizePackedPixels()} );

			std::span{packedColors1, b1.getSizePackedPixels()} |
				pl::transform
				(
					ospan,
					packedColorsInto,
					[&](auto&& p1, auto&& p2)
					{
						return PL_FWD(fc)( std::get<1>(PL_FWD(p2)), PL_FWD(p1), std::get<0>(PL_FWD(p2)) );
					},
					std::identity{},
					std::identity{},
					pol
				);
		}
		else
		{
			std::span{packedColors1, b1.getSizePackedPixels()} |
				pl::transform
				(
					std::span{packedColors2, b2.getSizePackedPixels()},
					packedColorsInto,
					PL_FWD(fc),
					std::identity{},
					std::identity{},
					pol
				);
		}
	}
}
