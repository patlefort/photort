/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Emitter.hpp>

namespace PhotoRT
{
	class PHOTORT_API EmitterObjectInstance final : public EmitterBase
	{
	public:

		[[nodiscard]] virtual bool hasArea() const noexcept override { return true; }
		[[nodiscard]] virtual bool isDiracDelta() const noexcept override { return mIsDirac; }

		[[nodiscard]] virtual pl::marked_optional<SampledSensorOut> sampleOut(IRNGInt &rng, RandomReals<2> rndPos, RandomReals<2> rndDir) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledSensorOutDir> sampleOutDir(const SampledSensor &sensorSurface, IRNGInt &rng, RandomReals<2> rndDir) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledSensorIn> sampleIn(const MaterialInteraction &surface, IRNGInt &rng, RandomReals<2> rndPos) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledSensor> sampleArea(IRNGInt &rng, RandomReals<2> rndPos) const noexcept override;
		[[nodiscard]] virtual Color radiance(const MaterialInteraction &sensorSurface, VecD dir) const noexcept override;

		[[nodiscard]] virtual OptionalPDF<Real> worldPDF(const MaterialInteraction &sensorSurface) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(const MaterialInteraction &sensorSurface) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDFA(const MaterialInteraction &sensorSurface, const MaterialInteraction &surface, VecD dir) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDFW(const MaterialInteraction &sensorSurface, VecD dir) const noexcept override;

		void setInstance(EntityPtr<IObjectInstance> inst, const Matrix &worldMatrix);
		[[nodiscard]] const auto &getInstance() const noexcept { return mInstance; }

		[[nodiscard]] auto getInstanceProps() const noexcept { return mInstanceProps; }
		void setInstanceProps(const IPrimitiveGroupProperties *p) noexcept { mInstanceProps = p; }

		[[nodiscard]] virtual VecD worldToLocal(VecD v) const noexcept override { return mWorldToLocalMatrix * v; }
		[[nodiscard]] virtual VecD localToWorld(VecD v) const noexcept override { return mWorldMatrix * v; }
		[[nodiscard]] virtual VecP worldToLocal(VecP v) const noexcept override { return mWorldToLocalMatrix * v; }
		[[nodiscard]] virtual VecP localToWorld(VecP v) const noexcept override { return mWorldMatrix * v; }

		virtual void init(const Scene &scene) override;

		[[nodiscard]] virtual VecP getMiddle() const noexcept override;

		[[nodiscard]] virtual Color averagePower() const noexcept override;

		[[nodiscard]] bool isLightPortal() const noexcept { return mLightPortal; }
		void setLightPortal(bool lp) noexcept { mLightPortal = lp; }

	protected:
		EntityPtr<IObjectInstance> mInstance;
		const IPrimitiveGroupProperties *mInstanceProps = nullptr;
		Matrix mWorldMatrix, mWorldToLocalMatrix;
		PrimitiveIndex mNbPrimitives = 0;
		bool mIsDirac = true, mHasFlatSurfaces = true, mLightPortal = false;

	};
}
