/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/PrimitiveGroup_fwd.hpp>

#include <PhotoRT/Core/Base.hpp>

#include <PhotoRT/Core/Emitter.hpp>

namespace PhotoRT
{
	class PHOTORT_API EmitterPoint final : public EmitterEntityBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("light-point", EmitterPoint)

		[[nodiscard]] virtual pl::marked_optional<SampledSensorOut> sampleOut(IRNGInt &rng, RandomReals<2> rndPos, RandomReals<2> rndDir) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledSensorOutDir> sampleOutDir(const SampledSensor &sensorSurface, IRNGInt &rng, RandomReals<2> rndDir) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledSensorIn> sampleIn(const MaterialInteraction &surface, IRNGInt &rng, RandomReals<2> rndPos) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledSensor> sampleArea(IRNGInt &rng, RandomReals<2> rndPos) const noexcept override;
		[[nodiscard]] virtual Color radiance(const MaterialInteraction &sensorSurface, VecD dir) const noexcept override;

		[[nodiscard]] virtual OptionalPDF<Real> PDFW(const MaterialInteraction &,  VecD /*dir*/) const noexcept override
			{ return pl::constants::sphere_pdf<Real>; }

		void setDiffuseColor(Color color) { mDiffuseColor = color; }
		[[nodiscard]] const Color &getDiffuseColor() const noexcept { return mDiffuseColor; }

		void setPosition(VecP position) { mPosition = position; }
		[[nodiscard]] VecP getPosition() const noexcept { return mPosition; }

		[[nodiscard]] virtual bool isDiracDelta() const noexcept override { return true; }

		void setAttenuationConstant(Real mAttenuationConstant) { this->mAttenuationConstant = mAttenuationConstant; }
		[[nodiscard]] Real getAttenuationConstant() const noexcept { return mAttenuationConstant; }
		void setAttenuationLinear(Real mAttenuationLinear) { this->mAttenuationLinear = mAttenuationLinear; }
		[[nodiscard]] Real getAttenuationLinear() const noexcept { return mAttenuationLinear; }
		void setAttenuationQuadratic(Real mAttenuationQuadratic) { this->mAttenuationQuadratic = mAttenuationQuadratic; }
		[[nodiscard]] Real getAttenuationQuadratic() const noexcept { return mAttenuationQuadratic; }

		void setAttenuation(Real vattenuationConstant, Real vattenuationLinear, Real vattenuationQuadratic) noexcept
		{
			mAttenuationConstant = vattenuationConstant;
			mAttenuationLinear = vattenuationLinear;
			mAttenuationQuadratic = vattenuationQuadratic;
		}

		[[nodiscard]] virtual VecP getMiddle() const noexcept override { return mPosition; }

		[[nodiscard]] virtual Color averagePower() const noexcept override { return mDiffuseColor; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:
		VecP mPosition{pl::tag::identity};
		Color mDiffuseColor{};
		Real mAttenuationConstant = 0, mAttenuationLinear = 0, mAttenuationQuadratic = 0;

	};
}
