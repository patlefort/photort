/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/DistributionSampling_fwd.hpp>
#include <PhotoRT/Core/Transform_fwd.hpp>

#include <PhotoRT/Core/Base.hpp>

#include <PhotoRT/Core/Emitter.hpp>
#include <PhotoRT/Core/Bitmap.hpp>

namespace PhotoRT
{
	class PHOTORT_API EmitterPanoramic final : public EmitterEntityBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("light-panoramic", EmitterPanoramic)

		EntityPtr<const IColorDistribution2D> mLightMap;
		EntityPtr<const IPDFSamplerPoint2Dr> mLightSampler;
		EntityPtr<const ITransform> mOrientation;

		[[nodiscard]] virtual pl::marked_optional<SampledSensorOut> sampleOut(IRNGInt &rng, RandomReals<2> rndPos, RandomReals<2> rndDir) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledSensorOutDir> sampleOutDir(const SampledSensor &sensorSurface, IRNGInt &rng, RandomReals<2> rndDir) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledSensorIn> sampleIn(const MaterialInteraction &surface, IRNGInt &rng, RandomReals<2> rndPos) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledSensor> sampleArea(IRNGInt &rng, RandomReals<2> rndPos) const noexcept override;
		[[nodiscard]] virtual Color radiance(const MaterialInteraction &sensorSurface, VecD dir) const noexcept override;

		[[nodiscard]] virtual bool isDiracDelta() const noexcept override { return false; }
		[[nodiscard]] virtual bool isInfinite() const noexcept override { return true; }
		[[nodiscard]] virtual bool hasArea() const noexcept override { return true; }

		[[nodiscard]] virtual OptionalPDF<Real> PDF(const MaterialInteraction &sensorSurface) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDFA(const MaterialInteraction &sensorSurface, const MaterialInteraction &surface, VecD dir) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDFW(const MaterialInteraction &sensorSurface, VecD dir) const noexcept override;

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

		virtual void init(const Scene &scene) override;

		[[nodiscard]] virtual Color averagePower() const noexcept override;

	protected:

		Matrix mOriMatrix{pl::tag::identity}, mOriMatrixInv{pl::tag::identity};
		Real mArea = 0, mAreaPdf = 0, mAreaRadius = 0, mAreaRadius2 = 0;
		VecP mMiddle;
		VecD mRadiusVec;

		static constexpr auto mConstDirPdf = 1 / (std::numbers::pi_v<Real> /opow2 * 2);

		[[nodiscard]] OptionalPDF<Real> panDirPdf(VecD odir) const noexcept;

		[[nodiscard]] Point2Dr getUV(VecD dir) const noexcept
			{ return *CoordinatePosition{tag::cartesian, dir}.to(tag::polar, type_v<Point2Dr>); }

		struct DirSample
		{
			VecD dir;
			Sample<Point2Dr> sample;

			constexpr void reset() noexcept { sample.reset(); }
			[[nodiscard]] constexpr bool has_value() const noexcept { return sample.has_value(); }
		};

		[[nodiscard]] pl::marked_optional<DirSample> sampleDir(RandomReals<2> rnd) const noexcept;

	};

}
