/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>

#include <PhotoRT/Core/Medium.hpp>
#include <PhotoRT/Core/Interaction.hpp>
#include <PhotoRT/Core/SurfaceMapper.hpp>

namespace PhotoRT
{
	class PHOTORT_API MediumHenyeyGreenstein : public MediumBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("medium-henyey_greenstein", MediumHenyeyGreenstein)

		EntityPtr<const IColorMapper> mSigmaA, mSigmaS;
		EntityPtr<const IRealMapper> mG;

		[[nodiscard]] SampledMedium sampleScattering(RandomReals<2> rnd, const MaterialInteraction &inter, const VecD odir, Real distance, IRNGInt &rng, EnumRayType typeMask = EnumRayType::All) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> dirPDF(const MediumInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual Color transmittance(const MaterialInteraction &inter, const VecD odir, Real scatterDist, IRNGInt &rng) const noexcept override;

		[[nodiscard]] Real getScale() const noexcept { return mScale; }
		void setScale(Real s) { mScale = s; mScaleInv = 1 / s; }

		[[nodiscard]] Real getMaximumDistance() const noexcept { return mMaximumDistance; }
		void setMaximumDistance(Real s) { mMaximumDistance = s; }

		[[nodiscard]] Real getStepSize() const noexcept { return mStepSize; }
		void setStepSize(Real s) { mStepSize = s; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:

		[[nodiscard]] SampledMedium tryScatter(const Color sigmaA, const Color sigmaS, RandomReals<2> rnd, const MaterialInteraction &inter, const VecD odir, Real distance, IRNGInt &rng) const noexcept;

		[[nodiscard]] Color phase(const Color &sigmaT, Real distance) const noexcept
			{ return (-sigmaT * distance) /oexp; }

		Real mScale = 1, mScaleInv = 1, mMaximumDistance = std::numeric_limits<Real>::infinity(), mStepSize = 1;

		[[nodiscard]] Color getSigmaA(const MaterialInteraction &inter, const VecD odir) const noexcept
		{
			return pl::assert_valid( mSigmaA ? mSigmaA->map(inter, odir) : Black );
		}

		[[nodiscard]] Color getSigmaS(const MaterialInteraction &inter, const VecD odir) const noexcept
		{
			return pl::assert_valid( mSigmaS ? mSigmaS->map(inter, odir) : Black );
		}

		[[nodiscard]] Real getG(const MaterialInteraction &inter, const VecD odir) const noexcept
		{
			return pl::assert_valid( mG ? mG->map(inter, odir) : 0, valid::Weight );
		}

		template <bool worldCoord_ = true>
		Color walkMedium(
			const MaterialInteraction &inter, const VecD dir, Real maxDist, Real step, Real rndStart,
			pl::CInvocableConvertibleR<Color, Color, Color, const MaterialInteraction &, Real, Real, bool &> auto&& fc,
			bool_c<worldCoord_> worldCoord = {}) const
		{
			Color color{pl::tag::one}, lastSigmaA, lastSigmaS;
			Real totalDist = 0;
			bool doContinue;

			surfaceWalk(inter, dir, maxDist, step, rndStart,
				[&](auto /*i*/, const auto dist, const auto distToNext, const auto &i)
				{
					const Color
						sigmaA{this->getSigmaA(i, dir)},
						sigmaS{this->getSigmaS(i, dir)};

					if(dist > 0 && (pl::mask_any(lastSigmaA != sigmaA) || pl::mask_any(lastSigmaS != sigmaS)))
					{
						doContinue = true;
						color *= PL_FWD(fc)(lastSigmaA, lastSigmaS, i, dist, totalDist, doContinue);
						totalDist = 0;

						if(!doContinue || isAlmostBlack(color))
							return false;
					}

					lastSigmaA = sigmaA;
					lastSigmaS = sigmaS;
					totalDist += distToNext;

					return true;
				},

				worldCoord
			);

			if(totalDist)
				color *= PL_FWD(fc)(lastSigmaA, lastSigmaS, inter, maxDist, totalDist, doContinue);

			return pl::assert_valid(color);
		}
	};
}
