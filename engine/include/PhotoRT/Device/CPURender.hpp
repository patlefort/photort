/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>

#include "CPU.hpp"

#include <patlib/thread_one_master.hpp>
#include <patlib/chrono.hpp>
#include <patlib/task.hpp>
#include <patlib/scoped.hpp>

#include <shared_access/shared.hpp>

#include <fmt/format.h>
#include <fmt/std.h>

#ifdef PL_NUMA
	#include <patlib/cpu.hpp>
	#include <patlib/numa.hpp>
#endif

namespace PhotoRT
{
	template <typename Master_, typename Worker_, typename WorkUnit_>
	class RenderDevice : public RenderEntity, public virtual IRenderDeviceCPU, public pl::ThreadDelegate<pl::ThreadWorkerPoolFirstMaster<Master_, Worker_, WorkUnit_>>
	{
	public:
		PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE_INCLASS(RenderDevice<Master_, Worker_, WorkUnit_>)

		using ThreadMasterClass = pl::ThreadDelegate<pl::ThreadWorkerPoolFirstMaster<Master_, Worker_, WorkUnit_>>;
		using ThreadMasterClass::mOtherThread;

		[[nodiscard]] virtual std::vector<EntityPtr<ContributionBuffer>> getContributionBuffers() const override { return mOtherThread.mMaster.mContributionBuffers; }

		[[nodiscard]] virtual std::string_view getDeviceName() const override { return getEntityId(); }

		virtual void getStats(Statistics &stats, const std::locale &loc) const override
		{
			mOtherThread.mMaster.getStats(stats, loc);

			stats["waits-rt-threads-total"] = fmt::format(loc, "{:L}    ", mOtherThread.wait_count());
		}

		virtual void setRenderContext(const EntityPtr<RenderContext> &context) override
			{ mOtherThread.mMaster.mRenderContext = context; }

		virtual void init(Scene &mScene) override
		{
			mOtherThread.mMaster.mDeviceName = getEntityId();
			mOtherThread.mMaster.init(mScene);
		}

		virtual void setParameters(const ParameterTree &params) override
		{
			RenderEntity::setParameters(params);

			if(const auto p = params.findParam("nb_threads"); p && p->value != "-1")
				mOtherThread.nb_threads(*p);
			else
				mOtherThread.nb_threads(pl::plmax<uf32>(pl::tasks.max_threads(), 1));

			params.getValueIfExistsBind("work_list_size", PL_LAMBDA_FORWARD_OBJ(work_list_size, this->mOtherThread));

			mOtherThread.mMaster.setParameters(*this, params);
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep) const override
		{
			ParameterTree params {RenderEntity::getParameters(deep)};

			params["nb_threads"] = mOtherThread.nb_threads();
			params["work_list_size"] = mOtherThread.work_list_size();

			return params + mOtherThread.mMaster.getParameters(*this, deep);
		}

		virtual void notifyEntityUpdate(IRenderEntity &) override {}

		[[nodiscard]] virtual bool isReady() const override { return mOtherThread.mMaster.getRayTracerEngine() && mOtherThread.mMaster.getRayTracerEngine()->isReady(); }

		virtual void dumpInfo(Scene &mScene, const std::filesystem::path &folder, const std::string &name) override
		{
			mOtherThread.mMaster.dumpInfo(mScene, folder, name);
		}

		[[nodiscard]] virtual bool hasData(bool ipcStream = false) const noexcept override { return mOtherThread.mMaster.hasData(ipcStream); }
		[[nodiscard]] virtual bool canHaveData() const noexcept override { return mOtherThread.mMaster.canHaveData(); }
		virtual void serialize(std::ostream &outputStream) const override { mOtherThread.mMaster.serialize(outputStream); }
		virtual void unserialize(std::istream &inputStream) override { mOtherThread.mMaster.unserialize(inputStream); }

		virtual void setRenderRect(std::string_view filmID, const pl::rectangle<> &rect) override
		{
			//if(mOtherThread.mMaster.setRenderRect(filmID, rect))
				//mOtherThread.flip();

			RenderDeviceExecutionLockGuard g{*this};

			if(mOtherThread.mMaster.setRenderRect(filmID, rect))
				mOtherThread.flip();
		}
		[[nodiscard]] virtual pl::rectangle<> getRenderRect() const override { return mOtherThread.mMaster.getRenderRect(); }

		virtual void name(std::string n) override { mOtherThread.name(std::move(n)); }
		[[nodiscard]] const auto &name() const noexcept { return mOtherThread.name(); }
	};

	class PHOTORT_API RenderDeviceCPU : public RenderEntity, public virtual IRenderDeviceCPU, public pl::ThreadDelegate<EntityPtr<IRenderDeviceCPU>>
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("device-cpu", RenderDeviceCPU)

		using ThreadDel = pl::ThreadDelegate<EntityPtr<IRenderDeviceCPU>>;

		virtual void start() override { if(mOtherThread) ThreadDel::start(); }
		virtual void end() override { if(mOtherThread) ThreadDel::end(); }
		virtual void pause() override { if(mOtherThread) ThreadDel::pause(); }
		virtual void restart() override { if(mOtherThread) ThreadDel::restart(); }
		[[nodiscard]] virtual bool started() const override { return mOtherThread && ThreadDel::started(); }
		[[nodiscard]] virtual bool waiting() const override { return mOtherThread && ThreadDel::waiting(); }
		[[nodiscard]] virtual bool paused() const override { return mOtherThread && ThreadDel::paused(); }
		[[nodiscard]] virtual bool running() const override { return mOtherThread && ThreadDel::running(); }
		virtual void join() override { if(mOtherThread) ThreadDel::join(); }
		virtual void priority(pl::EnumThreadPriority p) override { mPriority = p; if(mOtherThread) ThreadDel::priority(p); }
		[[nodiscard]] virtual pl::EnumThreadPriority priority() const override { return mPriority; }

		[[nodiscard]] virtual std::vector<EntityPtr<ContributionBuffer>> getContributionBuffers() const override { if(mOtherThread) return mOtherThread->getContributionBuffers(); return {}; }

		[[nodiscard]] virtual std::string_view getDeviceName() const override { return mOtherThread ? mOtherThread->getDeviceName() : ""; }

		virtual void lock_execution() override { if(mOtherThread) ThreadDel::lock_execution(); }
		virtual void unlock_execution() override { if(mOtherThread) ThreadDel::unlock_execution(); }

		virtual void getStats(Statistics &stats, const std::locale &loc) const override
			{ if(mOtherThread) mOtherThread->getStats(stats, loc); }

		virtual void setRenderContext(const EntityPtr<RenderContext> &context) override;

		virtual void init(Scene &mScene) override
			{ if(mOtherThread) mOtherThread->init(mScene); }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep) const override;

		virtual void notifyEntityUpdate(IRenderEntity &entity) override { if(mOtherThread) mOtherThread->notifyEntityUpdate(entity); }

		[[nodiscard]] virtual bool isReady() const override { return mOtherThread && mOtherThread->isReady(); }

		virtual void dumpInfo(Scene &mScene, const std::filesystem::path &folder, const std::string &name) override
			{ if(mOtherThread) mOtherThread->dumpInfo(mScene, folder, name); }

		[[nodiscard]] virtual bool hasData(bool ipcStream = false) const noexcept override { return mOtherThread && DynEntityCast<IRenderEntity>(mOtherThread)->hasData(ipcStream); }
		[[nodiscard]] virtual bool canHaveData() const noexcept override { return mOtherThread && DynEntityCast<IRenderEntity>(mOtherThread)->canHaveData(); }
		virtual void serialize(std::ostream &outputStream) const override { if(mOtherThread) DynEntityCast<IRenderEntity>(mOtherThread)->serialize(outputStream); }
		virtual void unserialize(std::istream &inputStream) override { if(mOtherThread) DynEntityCast<IRenderEntity>(mOtherThread)->unserialize(inputStream); }

		virtual void setRenderRect(std::string_view filmID, const pl::rectangle<> &rect) override { if(mOtherThread) mOtherThread->setRenderRect(filmID, rect); }
		[[nodiscard]] virtual pl::rectangle<> getRenderRect() const override { if(mOtherThread) return mOtherThread->getRenderRect(); return {}; }

		virtual void name(std::string n) override { mName = std::move(n); }
		[[nodiscard]] const auto &name() const noexcept { return mName; }

	protected:
		ParameterTree mDeviceParams;
		pl::EnumThreadPriority mPriority = pl::EnumThreadPriority::Normal;
		std::string mName;
	};


	template <typename WorkUnit_>
	class RenderDeviceWorkerBase
	{
	public:
		RenderDeviceWorkerBase()
		{
			mEntityList.mFactories = std::make_unique<EntityFactoriesProxy>(factories);
			mStatsTimer.duration(1s);
		}

		virtual ~RenderDeviceWorkerBase() = default;

		using WorkUnitType = WorkUnit_;

		virtual void resetStats()
		{
			mRayTracer->resetStats();

			mStatsTimer.start();
		}

		std::unique_ptr<IRayTracer> mRayTracer;
		EntityPtr<IIntegratorFactory> mIntegratorFactory;
		Parameter mRngEngineParams;
		std::string mDeviceName;
		EntityPtr<IRayTracerEngine> mRayTracerEngine;
		const Scene *mScene = nullptr;

		[[nodiscard]] virtual StatsAccumulatorList getStats() const
		{
			// Return a cloned list of stats
			StatsAccumulatorList cloned;

			mStats | sa::mode::shared |
				[&](const auto &sts)
				{
					for(const auto &s : sts)
						cloned.emplace(s.first, s.second->clone_copy());
				};

			return cloned;
		}

		virtual void init()
		{
			/*mEntityList.clearEntities();
			mSampler = mEntityList.createEntity(type_v<Sampler>, mRngEngineParams);
			mEntityList.clearEntities();*/

			mRayTracer = mRayTracerEngine->createTracer();
		}

		virtual void start(std::vector<pl::work_unit<WorkUnit_>> &/*workUnits1*/, std::vector<pl::work_unit<WorkUnit_>> &/*workUnits2*/)
		{
			#ifdef PL_LIB_NUMA
				if(numa_available() > -1)
					pl::log(mDeviceName, "Worker #{} started on node {}.", std::this_thread::get_id(), numa_node_of_cpu(pl::get_cpu()));
				else
			#endif
					pl::log(mDeviceName, "Worker #{} started.", std::this_thread::get_id());

			init();
			restart();
		}

		virtual void end()
		{
			pl::log(mDeviceName, "Worker #{} ended.", std::this_thread::get_id());
		}

		virtual void pause()
		{
			pl::log(mDeviceName, "Worker #{} paused.", std::this_thread::get_id());
			mStatsTimer.pause();
		}

		virtual void unpause()
		{
			pl::log(mDeviceName, "Worker #{} unpaused.", std::this_thread::get_id());
			mStatsTimer.unpause();
		}

		virtual void restart()
		{
			mSampler.seed();

			resetStats();
		}

		virtual pl::EnumWorkUnitState process(WorkUnit_ &unit) = 0;

		virtual void idle()
		{
			if(mStatsTimer.check())
			{
				// Accumulate statistics
				mStats | sa::mode::excl |
					[&](auto &sts)
					{
						sts.clear();
						accumulateStats(sts);

						auto st = mRayTracer->getStats();
						mergeStats(sts, st);

						#ifdef PRT_PROFILER
							auto profilerst = Profiler::raytrace.getStats();
							mergeStats(sts, profilerst);
						#endif
					};

			}
		}

	protected:

		pl::stop_watch<> mStatsTimer;
		Sampler mSampler;
		EntityCollection mEntityList;
		sa::shared<StatsAccumulatorList, std::shared_mutex> mStats;

		virtual void accumulateStats(StatsAccumulatorList &s) = 0;
	};

	template <typename Worker_, typename WorkUnit_>
	class RenderDeviceMasterBase : public pl::io::ISerializable<>
	{
	public:
		RenderDeviceMasterBase()
		{
			mStatsTimer.duration(10s);
		}

		virtual ~RenderDeviceMasterBase() = default;

		struct TimedStats
		{
			u64 nbRays = 0, nbSamples = 0;

			void reset()
			{
				nbRays = 0;
				nbSamples = 0;
			}

			void update(pl::stop_watch<> &timer, u64 curNbRays, u64 curNbSamples, TimedStats &resStats)
			{
				const auto period = timer.period();

				resStats.nbRays = (curNbRays - nbRays) / period;
				resStats.nbSamples = (curNbSamples - nbSamples) / period;

				nbRays = curNbRays;
				nbSamples = curNbSamples;
			}

		};

		EntityPtr<RenderContext> mRenderContext;
		std::vector<EntityPtr<ContributionBuffer>> mContributionBuffers;
		std::string mDeviceName;
		Parameter mRngEngineParams;

		virtual void setRayTracerEngine(EntityPtr<IRayTracerEngine> rt) { mRayTracerEngine = std::move(rt); }
		[[nodiscard]] virtual const EntityPtr<IRayTracerEngine> &getRayTracerEngine() const noexcept { return mRayTracerEngine; }

		virtual void start()
		{
			pl::log(mDeviceName) << "Master started.";

			if(mIntegratorFactory)
			{
				//EntityCollection entityCollection;
				//entityCollection.mFactories = std::make_unique<EntityFactoriesProxy>(factories);
				//auto mSampler = entityCollection.createEntity(type_v<Sampler>, mRngEngineParams);
				//mSampler->seed();
				Sampler sampler;
				sampler.seed();
				auto rayTracer = mRayTracerEngine->createTracer();
				auto tracer = TracerParams{*rayTracer, sampler};

				mIntegratorFactory->init(*mScene, tracer);
			}

			restart();
		}

		virtual void end()
		{
			pl::log(mDeviceName) << "Master ended.";
		}

		virtual void restart()
		{
			resetStats();
			zeroBuffers();
		}

		virtual void pause()
		{
			pl::log(mDeviceName) << "Master paused.";
			mStatsTimer.pause();
		}

		virtual void unpause()
		{
			pl::log(mDeviceName) << "Master unpaused.";
			mStatsTimer.unpause();
		}

		virtual void zeroBuffers()
		{
			for(auto &t : mContributionBuffers)
				t->zeroBuffers();
		}

		virtual void prepareWorker(Worker_ &worker, pl::back_buffered_work_units<WorkUnit_> &)
		{
			mWorkers.push_back(&worker);

			updateWorker(worker);
		}

		virtual void deleteWorker(Worker_ &worker)
		{
			pl::erase_one(mWorkers, &worker);
		}

		virtual bool prepare(WorkUnit_ &unit) = 0;
		virtual pl::EnumWorkUnitState process(WorkUnit_ &unit) = 0;

		virtual void init(const Scene &scene)
		{
			mScene = &scene;
			mIntegratorFactory = nullptr;

			if(mEntity && mRenderContext)
			{
				//mRenderContext->mRenderParams.print(std::cout);
				auto &intParam = mRenderContext->mRenderParams["integrator"];
				Parameter intParamRef;
				intParamRef.toEntity("int_" + intParam["int_type"].value + '-', "");

				const pl::scoped_swap ss{intParamRef.childParams, intParam.childParams};

				//intParamRef.childParams->print(std::cout);

				//mEntity->addParamReferenceTyped(mIntegratorFactory, intParamRef, true);
				//auto integrator = mEntity->getEntityCollection()->createEntity(intParamRef);
				mEntity->getEntityCollection()->addEntityReferenceInto(*mEntity, mIntegratorFactory, intParamRef);
			}

			/*if(mIntegratorFactory)
			{
				EntityCollection entityCollection;
				auto mSampler = entityCollection.createEntity<Sampler>(mRngEngineParams);
				mSampler->seed();
				auto rayTracer = mRayTracerEngine->createTracer();
				auto tracer = TracerParams{*rayTracer, *mSampler};

				mIntegratorFactory->init(mScene, tracer);
			}*/

			for(auto &worker : mWorkers)
				worker->mScene = &scene;
				//worker->init(mScene);
		}

		virtual void work()
		{
			if(mStatsTimer.check())
			{
				// Accumulate statistics
				mRayTraceStats | sa::mode::excl |
					[&](auto &sts)
					{
						sts.clear();

						for(auto &w : mWorkers)
						{
							auto st = w->getStats();
							mergeStats(sts, st);
						}

						u64 nbRays = 0, nbSamples = 0;

						if(auto rayStats = sts.find("rays"); rayStats != sts.end())
							nbRays = static_cast<const stats::AccumulatorRay &>(*rayStats->second).extract(stats::StatSum_c);

						if(auto sampleStats = sts.find("samples"); sampleStats != sts.end())
							nbSamples = static_cast<const stats::AccumulatorSample &>(*sampleStats->second).extract(stats::StatSum_c);

						mTimedStats.update(mStatsTimer, nbRays, nbSamples, mCurrentTimedStats);
					};
			}
		}

		[[nodiscard]] virtual bool workAllDone() const { return mNbSamplesMax && mNbSamplesDone >= mNbSamplesMax; }

		virtual void resetStats()
		{
			mNbSamplesDone = 0;

			mTimedStats.reset();
			mCurrentTimedStats.reset();

			mRayTraceStats.access(sa::mode::excl)->clear();

			mStatsTimer.start();
		}

		virtual void getStats(Statistics &stats, const std::locale &loc) const
		{
			mRayTraceStats | sa::mode::shared |
				[&](auto &sts)
				{
					if(sts.empty())
						return;

					// Build statistics map
					stats.clear();

					for(auto &s : sts)
						pl::concatenate(stats, s.second->toMap(loc, s.first + ": "));

					stats.insert({
						{"rays: total-per-sec",    fmt::format(loc, "{:L}    ", mCurrentTimedStats.nbRays)},
						{"samples: total-per-sec", fmt::format(loc, "{:L}    ", mCurrentTimedStats.nbSamples)}
					});

					#ifdef PRT_PROFILER
						std::map<std::string, MReal, std::less<>> profileSums;
						const auto totalRunningTime = std::chrono::duration_cast<std::chrono::duration<MReal, std::milli>>(mStatsTimer.total_running_time()).count();

						for(const auto &s : sts)
						{
							if(s.first.starts_with("profile: "))
							{
								std::string_view v{s.first};
								v.remove_prefix(9);

								profileSums.emplace(std::string{v}, static_cast<const stats::AccumulatorTimed &>(*s.second).extract(stats::StatSum_c) / mWorkers.size());
							}
						}

						for(const auto &s : profileSums)
							stats.emplace("profile: %: " + s.first, fmt::format(loc, "{:.2f}%", static_cast<double>(s.second) / totalRunningTime * 100.0));
					#endif
				};
		}

		virtual void setStatsRefreshRate(uf32 nbMilliSeconds)
		{
			mStatsTimer.duration(std::chrono::milliseconds(nbMilliSeconds));
		}

		[[nodiscard]] uf32 getStatsRefreshRate() const
		{
			return std::chrono::duration_cast<std::chrono::milliseconds>(mStatsTimer.duration()).count();
		}

		[[nodiscard]] u64 getMaxSamples() const noexcept { return mNbSamplesMax; }
		virtual void setMaxSamples(u64 nb) { mNbSamplesMax = nb; }

		virtual void setParameters(IRenderEntity &entity, const ParameterTree &params)
		{
			mEntity = &entity;

			//auto p = params.findParam("rng_engine_params");
			//mRngEngineParams = p ? *p : Parameter{};

			params.getValueIfExistsBind("max_samples", PL_LAMBDA_FORWARD_THIS(setMaxSamples));

			//entity.addParamReferenceTyped(mIntegratorFactory, params, "integrator");
			if(auto p = params.findParam("raytracer"); p && !p->value.empty())
				setRayTracerEngine(entity.getEntityCollection()->addEntityReferenceCreate(type_v<IRayTracerEngine>, entity, *p));
			else
				setRayTracerEngine(nullptr);

			updateWorkers();
		}

		[[nodiscard]] virtual ParameterTree getParameters(const IRenderEntity &, bool deep = false) const
		{
			ParameterTree params;

			//params["rng_engine_params"] = mRngEngineParams;

			params["max_samples"] = getMaxSamples();

			//if(mIntegratorFactory)
				//params["integrator"].fromEntityRef(*mIntegratorFactory, deep);

			if(getRayTracerEngine())
				params["raytracer"].fromEntityRef(*getRayTracerEngine(), deep);

			return params;
		}

		[[nodiscard]] virtual bool hasData(bool /*ipcStream*/ = false) const { return false; }
		[[nodiscard]] virtual bool canHaveData() const noexcept { return false; }
		virtual void serialize(std::ostream &) const override {}
		virtual void unserialize(std::istream &) override {}

		virtual void dumpInfo(Scene &/*scene*/, const std::filesystem::path &/*folder*/, const std::string &/*name*/) {}

		virtual bool setRenderRect(std::string_view filmID, const pl::rectangle<> &rect) { mRenderRect = rect; mRenderFilmID = filmID; return false; }
		[[nodiscard]] virtual pl::rectangle<> getRenderRect() const { return mRenderRect; }

	protected:
		EntityPtr<IRayTracerEngine> mRayTracerEngine;
		EntityPtr<IIntegratorFactory> mIntegratorFactory;
		std::vector<Worker_ *> mWorkers;
		TimedStats mTimedStats, mCurrentTimedStats;
		sa::shared<StatsAccumulatorList, std::shared_mutex> mRayTraceStats;
		pl::stop_watch<> mStatsTimer;
		u64 mNbSamplesMax = 0, mNbSamplesDone = 0;
		const Scene *mScene = nullptr;
		pl::rectangle<> mRenderRect{};
		std::string mRenderFilmID;
		IRenderEntity *mEntity = nullptr;

		virtual void updateWorker(Worker_ &worker)
		{
			worker.mRayTracerEngine = mRayTracerEngine;
			worker.mIntegratorFactory = mIntegratorFactory;
			//worker.mRngEngineParams = mRngEngineParams;
			worker.mDeviceName = mDeviceName;
			worker.mScene = mScene;

			//if(mScene)
				//worker.init(*mScene);
		}

		virtual void updateWorkers()
		{
			for(auto &t : mWorkers)
				updateWorker(*t);
		}

	};

}
