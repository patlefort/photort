/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>

#include "CPURender.hpp"

namespace PhotoRT
{
	class PHOTORT_API RenderDeviceWorkerLightTrace : public RenderDeviceWorkerBase<PhotonSampleList>
	{
	public:

		virtual void init() override;
		virtual void resetStats() override;

		virtual void start(std::vector<pl::work_unit<PhotonSampleList>> &workUnits1, std::vector<pl::work_unit<PhotonSampleList>> &workUnits2) override;
		virtual void restart() override;

		[[nodiscard]] EntityPtr<IIntegratorPhoton> getIntegrator() const { return mIntegrator; }

		virtual pl::EnumWorkUnitState process(PhotonSampleList &unit) override;

		virtual void initLightTrace(std::span<EntityPtr<ContributionBufferLight>> lightBuffers);

	protected:
		EntityPtr<IIntegratorPhoton> mIntegrator;
		std::span<EntityPtr<ContributionBufferLight>> mLightBuffers;

		virtual void accumulateStats(StatsAccumulatorList &s) override;
	};

	class PHOTORT_API RenderDeviceMasterLightTrace : public RenderDeviceMasterBase<RenderDeviceWorkerLightTrace, PhotonSampleList>
	{
	public:

		RenderDeviceMasterLightTrace();

		virtual void restart() override;
		virtual void prepareWorker(RenderDeviceWorkerLightTrace &worker, pl::back_buffered_work_units<PhotonSampleList> &workUnits) override;
		virtual bool prepare(PhotonSampleList &unit) override;
		virtual pl::EnumWorkUnitState process(PhotonSampleList &unit) override;

		virtual void init(const Scene &scene) override;

		virtual void end() override;

		virtual void dumpInfo(Scene &scene, const std::filesystem::path &folder, const std::string &name) override;

		virtual void setParameters(IRenderEntity &entity, const ParameterTree &params) override
		{
			RenderDeviceMasterBase<RenderDeviceWorkerLightTrace, PhotonSampleList>::setParameters(entity, params);
		}

		[[nodiscard]] virtual ParameterTree getParameters(const IRenderEntity &entity, bool deep = false) const override
		{
			ParameterTree params{RenderDeviceMasterBase<RenderDeviceWorkerLightTrace, PhotonSampleList>::getParameters(entity, deep)};

			return params;
		}

		virtual void work() override;

		virtual bool setRenderRect(std::string_view filmID, const pl::rectangle<> &rect) override;

	protected:
		EntityCollection mEntityList;
		bool mRestoredState = false;

		u64 mNbSamplesGenerated = 0;

		std::vector<EntityPtr<ContributionBufferLight>> mLightBuffers;

	};

	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(RenderDevice<RenderDeviceMasterLightTrace, RenderDeviceWorkerLightTrace, RenderDeviceWorkerLightTrace::WorkUnitType>)
}
