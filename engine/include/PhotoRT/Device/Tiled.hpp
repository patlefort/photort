/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>

#include "CPURender.hpp"

namespace PhotoRT
{
	class IPixelSampler;

	class PHOTORT_API RenderDeviceWorkerTiled : public RenderDeviceWorkerBase<PixelSampleTileLayers>
	{
	public:

		virtual void init() override;
		virtual void resetStats() override;
		virtual void restart() override;

		[[nodiscard]] EntityPtr<IIntegratorTile> getIntegrator() const { return mIntegrator; }

		virtual pl::EnumWorkUnitState process(PixelSampleTileLayers &unit) override;

		virtual void initTiled(
			std::span<EntityPtr<IPixelSampler>> samplers,
			std::span<EntityPtr<ContributionBufferLight>> lightBuffers,
			std::span<EntityPtr<ContributionBufferPixel>> pixelBuffers);

	protected:
		EntityPtr<IIntegratorTile> mIntegrator;
		std::vector<Sampler> mRngEngines;

		std::span<EntityPtr<IPixelSampler>> mSamplers;
		std::span<EntityPtr<ContributionBufferLight>> mLightBuffers;
		std::span<EntityPtr<ContributionBufferPixel>> mPixelBuffers;

		virtual void accumulateStats(StatsAccumulatorList &s) override;
	};

	class PHOTORT_API RenderDeviceMasterTiled : public RenderDeviceMasterBase<RenderDeviceWorkerTiled, PixelSampleTileLayers>
	{
	public:

		RenderDeviceMasterTiled();

		virtual void restart() override;
		virtual void prepareWorker(RenderDeviceWorkerTiled &worker, pl::back_buffered_work_units<PixelSampleTileLayers> &workUnits) override;
		virtual bool prepare(PixelSampleTileLayers &unit) override;
		virtual pl::EnumWorkUnitState process(PixelSampleTileLayers &unit) override;

		virtual void init(const Scene &scene) override;

		virtual void end() override;

		virtual void dumpInfo(Scene &scene, const std::filesystem::path &folder, const std::string &name) override;

		virtual void setParameters(IRenderEntity &entity, const ParameterTree &params) override
		{
			auto p = params.findParam("pixel_sampler_params");
			mPixelSamplerParams = p ? *p : Parameter{};

			params.getValueIfExists("use_pixel_buffer", mUsePixelBuffer);
			params.getValueIfExists("use_light_buffer", mUseLightBuffer);

			RenderDeviceMasterBase<RenderDeviceWorkerTiled, PixelSampleTileLayers>::setParameters(entity, params);
		}

		[[nodiscard]] virtual ParameterTree getParameters(const IRenderEntity &entity, bool deep = false) const override
		{
			ParameterTree params{RenderDeviceMasterBase<RenderDeviceWorkerTiled, PixelSampleTileLayers>::getParameters(entity, deep)};

			params["use_pixel_buffer"] = mUsePixelBuffer;
			params["use_light_buffer"] = mUseLightBuffer;

			params["pixel_sampler_params"] = mPixelSamplerParams;

			return params;
		}

		virtual void work() override;
		[[nodiscard]] virtual bool workAllDone() const override;

		virtual bool setRenderRect(std::string_view filmID, const pl::rectangle<> &rect) override;

	protected:
		RNGStdSeedable<pcg32_fast> mRng;
		std::vector<EntityPtr<IPixelSampler>> mPixelSamplers;
		EntityCollection mEntityList;
		bool mRestoredState = false;
		IPixelSampler *mTargetPs = nullptr;
		uf32 mTargetPsIndex = 0;
		Parameter mPixelSamplerParams;

		uf32 mPixelSampleIndex = 0;
		u64 mNbSamplesGenerated = 0, mNbTilesGenerated = 0, mNbTilesDone = 0;
		bool mUsePixelBuffer = true, mUseLightBuffer = false;

		std::vector<EntityPtr<ContributionBufferLight>> mLightBuffers;
		std::vector<EntityPtr<ContributionBufferPixel>> mPixelBuffers;

	};

	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(RenderDevice<RenderDeviceMasterTiled, RenderDeviceWorkerTiled, RenderDeviceWorkerTiled::WorkUnitType>)
}
