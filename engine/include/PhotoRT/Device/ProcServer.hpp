/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifdef PRT_IPC

#include <PhotoRT/Core/Base.hpp>

#include "CPU.hpp"

// Workaround bug in boost.process on MingW
#ifndef __kernel_entry
	#define __kernel_entry
#endif

#include <boost/process.hpp>

namespace PhotoRT
{
	class IProcServerCommandFilter
	{
	public:
		virtual ~IProcServerCommandFilter() = default;

		virtual void filterCommand(std::string &command) = 0;
	};

	class PHOTORT_API RenderDeviceProcServer : public RenderDeviceServer
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("device-procserver", RenderDeviceProcServer)

		struct Client;

			class PHOTORT_API ModuleException : public Exception
			{
			public:

				[[nodiscard]] virtual const char *module() const noexcept override;

			};

		using CommandHandler = std::function<void(Client &client, std::string_view command)>;

		std::vector<EntityPtr<ContributionBuffer>> contributionBuffers;

		virtual ~RenderDeviceProcServer();

		virtual void start() override;

		virtual void getStats(Statistics &stats, const std::locale &loc) const override;
		[[nodiscard]] virtual std::vector<EntityPtr<ContributionBuffer>> getContributionBuffers() const override { return contributionBuffers; }

		virtual void init(Scene &scene) override { mScene = &scene; }

		virtual void setRenderContext(const EntityPtr<RenderContext> &context) override { mRenderContext = context; }

		[[nodiscard]] virtual std::string_view getDeviceName() const override { return getEntityId(); }

		virtual void dumpInfo(Scene &, const std::filesystem::path &/*folder*/, const std::string &/*name*/) override {}

		[[nodiscard]] virtual bool isReady() const override { return true; }

		virtual void notifyEntityUpdate(IRenderEntity &) override {}

		virtual void setRenderRect(std::string_view filmID, const pl::rectangle<> &rect) override { mRenderRect = rect; mRenderFilmID = filmID; }
		[[nodiscard]] virtual pl::rectangle<> getRenderRect() const override { return mRenderRect; }

		void setModulePath(std::string_view p) { mModulePath = p; }
		[[nodiscard]] const std::string &getModulePath() const noexcept { return mModulePath; }

		void setClientConfig(ParameterTree c) { mClientConfig = std::move(c); }
		[[nodiscard]] const ParameterTree &getClientConfig() const noexcept { return mClientConfig; }

		void setCommandFilter(IProcServerCommandFilter *cf) noexcept { mCommandFilter = cf; }
		[[nodiscard]] const IProcServerCommandFilter *getCommandFilter() const noexcept { return mCommandFilter; }
		[[nodiscard]] IProcServerCommandFilter *getCommandFilter() noexcept { return mCommandFilter; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:

		Scene *mScene = nullptr;
		EntityPtr<RenderContext> mRenderContext;
		pl::rectangle<> mRenderRect{};
		std::string mRenderFilmID;
		std::list<Client> mClients;
		boost::process::group mProcGroup;
		std::string mModulePath;
		ParameterTree mClientDeviceParams, mClientConfig;
		IProcServerCommandFilter *mCommandFilter = nullptr;

		mutable std::mutex mStatsMutex;
		Statistics mClientStats;

		virtual void on_start() override;
		virtual void on_restart() override;
		virtual void on_end() override;

		[[nodiscard]] virtual bool prepare_to_pause() override;
		[[nodiscard]] virtual bool prepare_to_end() override;

		using RenderDeviceServer::process_loop;
		virtual void process_loop() override;

		void sendCommand(std::string_view command, Client &client, CommandHandler handler = nullptr);
		void broadcastCommand(std::string_view command, CommandHandler handler = nullptr);
		void getBufferTypes(Client &client);

		void sendData(Client &client);
		void clearClientBuffers(Client &client);

		void launchClient(Client &client);

	};
}

#endif