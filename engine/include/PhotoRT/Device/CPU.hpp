/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>

#include <PhotoRT/Core/RenderDevices.hpp>

#include <PhotoRT/Core/Distribution.hpp>
#include <PhotoRT/Core/Film.hpp>
#include <PhotoRT/Core/PixelSampler.hpp>
#include <PhotoRT/Core/RayTracer.hpp>
#include <PhotoRT/Core/RenderEntity.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/Scene.hpp>
#include <PhotoRT/Core/Sampling.hpp>
#include <PhotoRT/Core/Statistics.hpp>
#include <PhotoRT/Core/Integrator.hpp>

#include <patlib/thread.hpp>
#include <patlib/rectangle.hpp>

namespace PhotoRT
{
	class IRenderDeviceCPU : public virtual IRenderDeviceBuffered
	{
	public:
		virtual void name(std::string n) = 0;
	};

	class RenderDeviceServer : public RenderEntity, public virtual IRenderDeviceBuffered, public pl::ThreadBase
	{
	public:

		std::vector<EntityPtr<ContributionBuffer>> mContributionBuffers;

		virtual void getStats(Statistics &, const std::locale &) const override {}
		[[nodiscard]] virtual std::vector<EntityPtr<ContributionBuffer>> getContributionBuffers() const override { return mContributionBuffers; }

		virtual void init(Scene &scene) override
		{
			mScene = &scene;
		}

		virtual void setRenderContext(const EntityPtr<RenderContext> &context) override
		{
			mRenderContext = context;
		}

		[[nodiscard]] virtual std::string_view getDeviceName() const override { return {}; }

		virtual void dumpInfo(Scene &, const std::filesystem::path &/*folder*/, const std::string &/*name*/) override {}

		virtual void setRenderRect(std::string_view filmID, const pl::rectangle<> &rect) override { mRenderRect = rect; mRenderFilmID = filmID; }
		[[nodiscard]] virtual pl::rectangle<> getRenderRect() const override { return mRenderRect; }

		virtual void setEntityId(std::string v) override
		{
			RenderEntity::setEntityId(v);
			name(std::move(v));
		}

	protected:

		std::map<std::string, std::map<std::string, EntityPtr<ContributionBuffer>>> mReceiveBuffers, mCompileBuffers;

		Scene *mScene = nullptr;
		EntityPtr<RenderContext> mRenderContext;
		pl::rectangle<> mRenderRect{};
		std::string mRenderFilmID;

	};
}
