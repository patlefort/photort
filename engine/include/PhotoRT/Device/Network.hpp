/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifdef PRT_NETWORKING

#include <PhotoRT/Core/Base.hpp>

#if __has_include(<winsock2.h>)
	#include <winsock2.h>
#endif

#include "CPU.hpp"

#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>

#include <optional>

namespace PhotoRT
{
	namespace bio = boost::asio;

	template <typename SocketType_, typename MutableBufferType_, typename Delim_>
	bool readUntilTimeout(SocketType_ &socket, const MutableBufferType_ &buffer, Delim_ /*delim*/, bio::deadline_timer::duration_type duration)
	{
		auto &mIoService = socket.get_io_service();
		bio::deadline_timer timer(mIoService);
		std::optional<boost::system::error_code> timerRes, readRes;

		timer.expires_from_now(duration);
		timer.async_wait([&](const boost::system::error_code &e){ timerRes = e; });

		bio::async_read(socket, buffer, [&](const boost::system::error_code &e, szt /*bytes_transferred*/){ readRes = e; });

		mIoService.reset();

		while(mIoService.run_one())
		{
			if(timerRes)
				socket.cancel();
			else if(readRes)
				timer.cancel();
		}

		if(*readRes)
			throw boost::system::system_error(*readRes);

		return !timerRes;
	}


	class PHOTORT_API NetworkedDevice : public ClientDevice
	{
	public:

		virtual void setPort(const std::string &p) { mPort = p; }
		virtual const std::string &getPort() const { return mPort; }

	protected:

		bio::io_context mIoService;
		std::string mPort {BOOST_STRINGIZE(PRT_DEFAULT_PORT)};

		void setSocketOptions(auto &socket, u32 durationMilli)
		{
			socket.set_option(bio::socket_base::enable_connection_aborted(true));
			//socket.set_option(bio::socket_base::keep_alive(true));
			//socket.set_option(bio::ip::tcp::no_delay(true));

			const auto nativeHandle = socket.native_handle();

			#ifdef _WIN32
				int32_t timeout = durationMilli;

				setsockopt(nativeHandle, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof(timeout));
				setsockopt(nativeHandle, SOL_SOCKET, SO_SNDTIMEO, (const char*)&timeout, sizeof(timeout));

				//BOOL one = 1;
				//setsockopt(nativeHandle, IPPROTO_TCP, TCP_NODELAY, (const char*)&one, sizeof(one));
			#else
				struct timeval tv;

				tv.tv_sec  = durationMilli / 1000;
				tv.tv_usec = durationMilli % 1000 * 1000;

				setsockopt(nativeHandle, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
				setsockopt(nativeHandle, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv));

				int one = 1, ten = 10;

				if(
					setsockopt(nativeHandle, SOL_SOCKET, SO_KEEPALIVE, &one, sizeof(one)) ||
					setsockopt(nativeHandle, SOL_TCP, TCP_KEEPINTVL, &one, sizeof(one)) ||
					setsockopt(nativeHandle, SOL_TCP, TCP_KEEPCNT, &ten, sizeof(ten))
				)
				{
					pl::log("NetworkedDevice", pl::logtag::error) << "Error on setting keep-alive on socket.";
				}

				//int one = 1;
				//setsockopt(socket.native_handle(), SOL_TCP, TCP_NODELAY, &one, sizeof(one));

			#endif
		}

	};



	class PHOTORT_API RenderDeviceServerIOStream : public RenderDeviceServer, public NetworkedDevice
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("device-networkserver", RenderDeviceServerIOStream)

			struct Client
			{
				std::unique_ptr<bio::ip::tcp::iostream> stream;
				bio::ip::tcp::socket::endpoint_type remote_endpoint;
				bool valid = true;
				ParameterTree finalOptions;
			};

			using Clock = std::chrono::high_resolution_clock;
			using Duration = std::chrono::duration<u64, Clock::period>;
			using CommandHandler = std::function<void(Client &client, std::string_view command, boost::system::error_code er)>;

			class PHOTORT_API ModuleException : public Exception
			{
			public:
				ModuleException(std::string_view message, const boost::system::error_code &er, const std::experimental::source_location &sl = std::experimental::source_location::current()) noexcept;

				[[nodiscard]] virtual const char *module() const noexcept override;
				[[nodiscard]] const boost::system::error_code &getErrorCode() const noexcept { return er; }

			protected:
				boost::system::error_code er;
			};


		RenderDeviceServerIOStream()
		{
			mRefreshRate = std::chrono::duration_cast<Duration>(std::chrono::seconds(30));
		}

		virtual void start() override;

		void setRefreshRate(u64 nbMilliSeconds)
		{
			mRefreshRate = std::chrono::duration_cast<Duration>(std::chrono::milliseconds(nbMilliSeconds));
		}

		[[nodiscard]] u64 getRefreshRate() const
		{
			return std::chrono::duration_cast<std::chrono::milliseconds>(mRefreshRate).count();
		}

		virtual void setRenderRect(std::string_view filmID, const pl::rectangle<> &rect) override;

		[[nodiscard]] uf32 getNbClients() const noexcept { return mClients.size(); }

		void setMaxNbClients(uf32 nb) noexcept { mMaxNbClients = nb; }
		[[nodiscard]] uf32 getMaxNbClients() const noexcept { return mMaxNbClients; }

		void setBinary(bool v) noexcept { mIoBinary = v; }
		[[nodiscard]] bool getBinary() const noexcept { return mIoBinary; }

		void setCompressor(std::string v) noexcept { mIoCompressor = std::move(v); }
		[[nodiscard]] const auto &getCompressor() const noexcept { return mIoCompressor; }

		virtual void notifyEntityUpdate(IRenderEntity &entity) override;

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

		[[nodiscard]] virtual bool isReady() const override { return true; }

	protected:

		std::shared_ptr<bio::ip::tcp::acceptor> mAcceptor;
		std::shared_ptr<bio::steady_timer> mTimer;

		Duration mRefreshRate;

		bool mIoBinary = true;
		std::string mIoCompressor;
		uf32 mMaxNbClients = 10;
		std::list<Client> mClients;
		mutable std::mutex mRenderParamMutex;

		std::string mRenderGUID;

		void getBufferTypes(Client &client);

		virtual void on_end() override;
		[[nodiscard]] virtual bool prepare_to_end() override;
		[[nodiscard]] virtual bool prepare_to_pause() override;
		virtual void on_restart() override;

		using RenderDeviceServer::process_loop;
		virtual void process_loop() override;

		virtual void acceptClient();
		virtual void getClientData();
		virtual void getClientDataAsync();

		virtual void sendCommand(std::string_view command, Client &client, CommandHandler handler = nullptr);
		virtual void broadcastCommand(std::string_view command, CommandHandler handler = nullptr);

	};


}

#endif
