/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>
#include "AccelStruct.hpp"

#include <patlib/masked_container_array.hpp>
#include <patlib/task.hpp>
#include <patlib/box.hpp>
#include <patlib/io.hpp>

#include <span>
#include <bit>

namespace PhotoRT::AccelStruct
{
	constexpr szt BVHMaxDepth = 100;

	template <typename R_>
	concept CBvhBoundaryRange =
		rg::sized_range<R_> &&
		rg::indirectly_swappable<rg::iterator_t<R_>, rg::iterator_t<R_>> &&
		rg::random_access_range<R_> &&
		CBoundaryEntry<rg::range_value_t<R_>>;

	template <std::unsigned_integral IndexType_, pl::CArithmetic SplitType_, szt NbAxis_>
		requires (NbAxis_ > 0)
	struct BVHTreeNode
	{
		static constexpr IndexType_ NbAxisBits = std::bit_width(NbAxis_ - 1);
		using IndexUnionType = pl::split_value<IndexType_, NbAxisBits>;

		static constexpr IndexType_ LeafValue = IndexUnionType::LowSize - 1;
		static constexpr IndexType_ EmptyLeafValue = IndexUnionType::HighSize - 1;

		std::array<SplitType_, 2> split;
		IndexUnionType index;

		[[nodiscard]] auto childIndex() const noexcept { return index.high(); }
		auto &childIndex(IndexType_ i) noexcept { index.high(i); return *this; }
		[[nodiscard]] auto axis() const noexcept { return index.low(); }
		auto &axis(IndexType_ i) noexcept { index.low(i); return *this; }

		void setLeaf() noexcept { axis(LeafValue); }
		void setEmptyLeaf() noexcept { childIndex(EmptyLeafValue); }
		[[nodiscard]] bool isEmptyLeaf() const noexcept { return childIndex() == EmptyLeafValue; }
		[[nodiscard]] bool isLeaf() const noexcept { return axis() == LeafValue; }

		[[nodiscard]] auto childIndexLeft() const noexcept { return childIndex(); }
		[[nodiscard]] auto childIndexRight() const noexcept { return childIndex() + 1; }
	};

	template <
		std::unsigned_integral IndexType_ = u32,
		pl::simd::CNeutralOrPointGeometricVector PointType_ = VecP
	>
	class BVH
	{
	public:

		using IndexType = IndexType_;
		using point_type = PointType_;
		using dir_type = pl::simd::rebind_to_dir<point_type>;
		using BoxType = pl::box<point_type>;
		using RealType = typename point_type::value_type;

		static constexpr auto NbAxis = point_type::category::NbAxis;

		using TreeNode = BVHTreeNode<IndexType, RealType, NbAxis>;

		BVH(std::span<const TreeNode> nodes, IndexType rootIndex)
			: mNodes{nodes}, mRootNode{rootIndex} {}

		/*void dump(std::ostream &outputStream, szt level) const
		{
			szt treeMaxDepth{};
			IndexType_ maxNbPrim{}, minNbPrim{std::numeric_limits<IndexType_>::max()};
			u64 totalLeafCost{};
			RealType smallestNodeSize{pl::constants::infhighest<RealType>};
			IndexType_ nbEmptyLeaves{};

			if(mTotalNbNodes > 0)
			{
				selectTree(
					[&](const auto &nodes)
					{
						const auto fc = [&](const TreeNode &node, szt level, const BoxType &box)
							{
								treeMaxDepth = pl::plmax<IndexType_>(treeMaxDepth, level);

								if(node.isLeaf())
								{
									IndexType_ nbp = node.isEmptyLeaf() ? 0 : mPrimitiveSource.getNbPrimitives(node.childIndex());
									maxNbPrim = maxNbPrim /omax/ nbp;
									minNbPrim = minNbPrim /omin/ nbp;

									totalLeafCost += mPrimitiveSource.getGeometricCost(nbp);

									if(nbp)
										smallestNodeSize = smallestNodeSize /omin/ box.dims().min3();
									else
										++nbEmptyLeaves;
								}
							};

						traverseRecursive(fc, 0, nodes[mRootNode], mAabox);
					}
				);
			}

			std::locale loc = pl::io::try_get_locale("");

			outputStream << pl::io::indent(level) << "Config:" << '\n';
			outputStream << pl::io::indent(level) << " Max depth: " << getMaxDepth() << '\n';
			outputStream << pl::io::indent(level) << " Index size: " << sizeof(IndexType_) << '\n';
			outputStream << pl::io::indent(level) << " Node size: " << sizeof(TreeNode) << '\n';
			outputStream << pl::io::indent(level) << " Node alignment: " << alignof(TreeNode) << '\n';
			outputStream << pl::io::indent(level) << " Target cost: " << getTargetCost() << '\n';
			outputStream << pl::io::indent(level) << " Use SAH at depth: " << getUseSahAtDepth() << '\n';
			outputStream << pl::io::indent(level) << " Nb. SAH tests: " << getNbSahTests() << '\n';
			outputStream << pl::io::indent(level) << " Traversal cost: " << TraversalCost << '\n';
			outputStream << pl::io::indent(level) << " Optimize tree: " << getOptimizeTree() << '\n';

			outputStream << '\n';
			outputStream << pl::io::indent(level) << "Stats:" << '\n';

			auto ms = pl::container_memory_capacity(mFinalTree);
			for(const auto &b : mBranches)
				ms += pl::container_memory_capacity(b);

			outputStream << pl::io::indent(level) << " Nodes memory size: " << pl::io::format_size(ms, loc) << '\n';
			outputStream << pl::io::indent(level) << " Leaves memory size: " << pl::io::format_size(mPrimitiveSource.memory_capacity(), loc) << '\n';
			outputStream << pl::io::indent(level) << " Time to build: " << std::chrono::duration_cast<std::chrono::duration<MReal, std::chrono::seconds::period>>(mBuildTime).count() << " seconds\n";
			outputStream << pl::io::indent(level) << " Max depth: " << treeMaxDepth << '\n';
			outputStream << pl::io::indent(level) << " Smallest node size: " << smallestNodeSize << '\n';
			outputStream << pl::io::indent(level) << " Number of nodes: " << mTotalNbNodes << '\n';
			outputStream << pl::io::indent(level) << " Number of non-empty leaves: " << mPrimitiveSource.getNbLeaves() << '\n';
			outputStream << pl::io::indent(level) << " Number of empty leaves: " << nbEmptyLeaves << '\n';
			outputStream << pl::io::indent(level) << " Nb primitives: " << mPrimitiveSource.getNbPrimitivesInLeaves() << '\n';
			outputStream << pl::io::indent(level) << " Min/max/avg primitives per leaf: " << minNbPrim << '/' << maxNbPrim << '/' << ((RealType)mPrimitiveSource.getNbPrimitivesInLeaves() / (RealType)mPrimitiveSource.getNbLeaves()) << '\n';
			outputStream << pl::io::indent(level) << " Total/avg leaf cost: " << totalLeafCost << '/' << ((MReal)totalLeafCost / (MReal)mPrimitiveSource.getNbLeaves()) << '\n';
		}*/

		void traverse(
			point_type origin,
			dir_type dir,
			dir_type dirInv,
			RealType distmin,
			RealType distmax,
			RealType colDistSqr,
			pl::CInvocableConvertibleR<RealType, IndexType_, RealType, RealType, RealType> auto&& leafTest) const noexcept
		{
			std::array<TraversalStack, BVHMaxDepth> stack;

			auto exitp = stack.begin();
			auto nodeIndex = mRootNode;

			const auto dirPositiveMask = pl::mask(dir >= pl::constants::zero<dir_type>);
			const auto dirNaN = pl::mask(dirInv /oisnan);

			distmin = pl::plmax<RealType>(distmin, 0);

			const auto aorigin = pl::make_array_sequence(size_v<NbAxis>, [a = pl::simd::vector_access(origin)](auto&& i) { return point_type{a[i]}; });
			const auto adirInv = pl::make_array_sequence(size_v<NbAxis>, [a = pl::simd::vector_access(dirInv)](auto&& i) { return dir_type{a[i]}; });

			while(true)
			{
				while(true)
				{
					const auto &node = mNodes[nodeIndex];
					const auto axis = node.index.low();
					const auto cIndex = node.index.high();

					// Hit a leaf node
					if(axis == TreeNode::LeafValue) [[unlikely]]
					{
						if(cIndex != TreeNode::EmptyLeafValue && colDistSqr > distmin /opow2)
							colDistSqr = PL_FWD(leafTest)(cIndex, distmin, distmax, colDistSqr);
						break;
					}

					point_type vt{pl::tag::array_init, node.split.data(), pl::tag::unaligned_init};

					vt -= aorigin[axis];

					// Direction is parallel to the split axis
					if(dirNaN[axis]) [[unlikely]]
					{
						// Branchless node selection
						//const std::array<RealType, 2> t { vt[size_v<0>], vt[size_v<1>] };
						const auto &t = pl::simd::vector_access(vt);

						const bool tDistMin = t[0] < 0;
						const bool tDistMax = t[1] > 0;

						if(tDistMin && tDistMax)
							break;

						const bool storeNode = !(tDistMin | tDistMax);

						nodeIndex = pl::select( cIndex + 1, cIndex, tDistMin );
						auto &ep = *exitp;
						ep.otherIndex = pl::select( cIndex + 1, cIndex, storeNode );
						ep.tNear = pl::select( distmin, ep.tNear, storeNode );
						ep.tFar = pl::select( distmax, ep.tFar, storeNode );
						exitp = pl::select( exitp+1, exitp, storeNode );

						/*if(t[0] < 0)
						{
							// Neither nodes are traversed
							if(t[1] > 0)
								break;

							// Traverse right node only
							nodeIndex = cIndex + (1 << mFinalTree.BranchSize);
						}
						else if(t[1] > 0)
						{
							// Traverse left node only
							nodeIndex = cIndex;
						}
						else
						{
							// Traverse left node and store right node into the stack for later
							exitp->otherIndex = cIndex + (1 << mFinalTree.BranchSize);
							nodeIndex = cIndex;
							exitp->tNear = distmin;
							exitp->tFar = distmax;

							++exitp;
						}*/

						continue;
					}

					vt *= adirInv[axis];

					// Branchless node selection
					//const std::array<RealType, 2> t { vt[size_v<0>], vt[size_v<1>] };
					const auto &t = pl::simd::vector_access(vt);

					const IndexType_ dp = dirPositiveMask[axis], dn = !dp;

					const bool tDistMin = t[dn] < distmin;
					const bool tDistMax = t[dp] > distmax;

					if(tDistMin && tDistMax)
						break;

					const auto nextDistMin = t[dp] /omax/ distmin;
					const auto nextDistMax = t[dn] /omin/ distmax;
					const bool storeNode = !(tDistMin | tDistMax);

					auto &ep = *exitp;
					ep.otherIndex = pl::select( cIndex + dp, ep.otherIndex, storeNode );
					ep.tNear = pl::select( nextDistMin, ep.tNear, storeNode );
					ep.tFar = pl::select( distmax, ep.tFar, storeNode );
					exitp = pl::select( exitp+1, exitp, storeNode );

					nodeIndex = pl::select( cIndex + dp, cIndex + dn, tDistMin );
					distmin = pl::select( nextDistMin, distmin, tDistMin );
					distmax = pl::select( distmax, nextDistMax, tDistMin );

					/*if(t[dn] < distmin)
					{
						// Neither nodes are traversed
						if(t[dp] > distmax)
							break;

						// Traverse right node only
						nodeIndex = childNodeIndex(pl::make_type_c(nodes), cIndex, dp);
						distmin = t[dp] /omax/ distmin;
					}
					else if(t[dp] > distmax)
					{
						// Traverse left node only
						nodeIndex = childNodeIndex(pl::make_type_c(nodes), cIndex, dn);
						distmax = t[dn] /omin/ distmax;
					}
					else
					{
						// Traverse left node and store right node into the stack for later
						exitp->otherIndex = childNodeIndex(pl::make_type_c(nodes), cIndex, dp);
						nodeIndex = childNodeIndex(pl::make_type_c(nodes), cIndex, dn);
						exitp->tNear = t[dp] /omax/ distmin;
						exitp->tFar = distmax;

						distmax = t[dn] /omin/ distmax;
						++exitp;
					}*/
				}

				// Go down the stack until a node that can be intersected
				while(true)
				{
					if(exitp == stack.begin()) [[unlikely]]
						return;

					--exitp;

					if(colDistSqr <= exitp->tNear /opow2)
						continue;

					nodeIndex = exitp->otherIndex;
					distmin = exitp->tNear;
					distmax = exitp->tFar;
					break;
				}
			}
		}

		void traverseRecursive(std::invocable<TreeNode &, szt, const BoxType &> auto&& fc, szt level, const IndexType nodeIndex, const BoxType &box) const
		{
			++level;

			auto &node = mNodes[nodeIndex];
			PL_FWD(fc)(node, level, box);

			if(!node.isLeaf())
			{
				const auto axis = node.axis();
				auto nbox = box;
				nbox.v2.set(node.split[0], axis);
				this->traverseRecursive(PL_FWD(fc), level, node.childIndexLeft(), nbox);

				nbox = box;
				nbox.v1.set(node.split[1], axis);
				this->traverseRecursive(PL_FWD(fc), level, node.childIndexRight(), nbox);
			}
		}

	private:

		struct TraversalStack
		{
			RealType tNear, tFar;
			IndexType_ otherIndex;
		};

		std::span<const TreeNode> mNodes;
		IndexType_ mRootNode = 0;

		/*void dumpRecursive(std::ostream &outputStream, szt level, const TreeNode &node, const BoxType &box) const
		{
			++level;

			const auto minSize = box.dims().min3();

			if(node.isLeaf())
			{
				IndexType_ nb = node.isEmptyLeaf() ? 0 : mPrimitiveSource.getNbPrimitives(node.childIndex());
				outputStream << pl::io::indent(level) << "l: " << nb << ' ' << minSize << '\n';
			}
			else
			{
				const auto axis = node.axis();
				outputStream << pl::io::indent(level) << "n: " << axis << ' ' << node.split[0] << ' ' << node.split[1] << ' ' << minSize << '\n';
				auto nbox = box;
				nbox.v2.set(node.split[0], axis);
				dumpRecursive(outputStream, level, nodes[childNodeIndex(pl::make_type_c(nodes), node, false)], nbox);

				nbox = box;
				nbox.v1.set(node.split[1], axis);
				dumpRecursive(outputStream, level, nodes[childNodeIndex(pl::make_type_c(nodes), node, true)], nbox);
			}
		}*/

		[[nodiscard]] IndexType_ getNodeFromPosition(point_type v)
		{
			IndexType_ index = 0;

			const auto &var = pl::simd::vector_access(v);

			do
			{
				const auto &node = mNodes[index];
				if(node.isLeaf())
					break;

				index = node.childIndex() + (var[node.axis()] > node.split);
			}
			while(true);

			return index;
		}
	};


	template <
		typename PrimitiveSourceType_,
		std::unsigned_integral IndexType_,
		pl::simd::CNeutralOrPointGeometricVector BoxPointType_,
		template <typename T_> typename BuildVectorType_,
		template <typename T_> typename FinalVectorType_ = std::vector,
		bool Point_ = false
	>
	class BVHBuilderSAH : pl::copymove_initialize_only
	{
	public:
		static constexpr szt NbAxis = BoxPointType_::category::NbAxis;

		using IndexType = IndexType_;
		using BoxType = pl::box<BoxPointType_>;
		using RealType = typename BoxPointType_::value_type;
		using TreeNode = BVHTreeNode<IndexType, RealType, NbAxis>;
		using BuildVectorType = BuildVectorType_<TreeNode>;
		using NodeVectorType = FinalVectorType_<TreeNode>;

		struct BuildOptions
		{
			szt maxDepth = 100, useSahAtDepth = 0;
			IndexType maxSahTests = 100;
			RealType emptyFactor = 0.5, targetCost = 0;
		};

	private:
		template <typename T_, szt> using BranchListVector = std::vector<T_>;
		using BranchListType = pl::masked_container_array<BuildVectorType, IndexType, 10, BranchListVector>;

		typename BuildVectorType::block_allocator_type mBlockAllocator;
		typename BuildVectorType::allocator_type mAllocator;
		typename NodeVectorType::allocator_type mFinalAllocator;
		BranchListType mBranches;
		IndexType mNbBranchesPerCore = 1;
		std::array<IndexType, BranchListType::NbBranches> mBranchIndexes;

		IndexType mTotalNbPrimitives = 0, mTotalNbNodes = 0, mRootNode = 0;
		szt mReserveSize = 0;

		BuildOptions mOptions;

		/*[[nodiscard]] constexpr IndexType childNodeIndex(IndexType index, IndexType right) const noexcept
			{ return index + (right << BranchListType::BranchSize); }

		[[nodiscard]] constexpr IndexType childNodeIndex(const TreeNode &node, bool right) const noexcept
			{ return childNodeIndex(node.childIndex(), (IndexType)right); }*/

	public:

		struct DataType
		{
			NodeVectorType mNodes;
			IndexType mRootNode;
		};

		PrimitiveSourceType_ &mPrimitiveSource;

		static constexpr bool Point = Point_;

		BVHBuilderSAH(const BVHBuilderSAH &) = default;
		BVHBuilderSAH(BVHBuilderSAH &&) = default;
		BVHBuilderSAH(PrimitiveSourceType_ &primitiveSource, BuildOptions opt = {}, const typename BuildVectorType::block_allocator_type &ba = {}, const typename BuildVectorType::allocator_type &a = {}, const typename NodeVectorType::allocator_type &fa = {}) :
				mBlockAllocator{ba}, mAllocator{a}, mFinalAllocator{fa},
				mBranches{ {BranchListType::NbBranches, BuildVectorType{mBlockAllocator, mAllocator}} },
				mOptions{std::move(opt)}, mPrimitiveSource{primitiveSource}
			{}

		void setOptions(BuildOptions opt)
		{
			mOptions = std::move(opt);
			mOptions.maxDepth = pl::plmin(mOptions.maxDepth, BVHMaxDepth);
		}

		[[nodiscard]] const auto &getOptions() const noexcept { return mOptions; }

		void clear()
		{
			for(auto &b : mBranches)
			{
				b.clear();
				b.shrink_to_fit();
			}

			mTotalNbPrimitives = 0;
			mTotalNbNodes = 0;
		}

		void build(CBvhBoundaryRange auto&& boundaries, BoxType aabox)
		{
			pl::assert_valid(aabox);

			clear();

			mTotalNbPrimitives = rg::size(boundaries);

			if(!mTotalNbPrimitives)
				return;

			prepare();
			buildImpl(boundaries, aabox);
		}

		[[nodiscard]] DataType compile()
		{
			DataType data{.mNodes = NodeVectorType{mFinalAllocator}, .mRootNode = mRootNode};

			std::array<IndexType, decltype(mBranches)::NbBranches> startIndexes;

			for(auto&& [br, si] : rgv::zip(mBranches, startIndexes))
			{
				si = mTotalNbNodes;
				mTotalNbNodes += br.size();
			}

			plassert(mTotalNbNodes > 0);

			data.mNodes.reserve(mTotalNbNodes);
			pl::append_move(mBranches, data.mNodes);

			for(auto &n : data.mNodes | rgv::remove_if([](const auto &n){ return n.isLeaf(); }))
			{
				const auto childIndex = n.childIndex();
				const auto brIndex = mBranches.build_index(childIndex);
				n.childIndex(brIndex.high() + startIndexes[brIndex.low()]);
			}

			const auto brIndex = mBranches.build_index(data.mRootNode);
			data.mRootNode = brIndex.high() + startIndexes[brIndex.low()];

			return data;
		}

		template <pl::simd::CNeutralOrPointGeometricVector PointType_>
		void validate(BoxType aabox, DataType &data)
		{
			if(mTotalNbNodes > 0)
			{
				const auto fc = [&](const TreeNode &node, [[maybe_unused]] szt level, const BoxType &)
					{
						plassert(level <= BVHMaxDepth && level <= mOptions.maxDepth);

						if(node.isLeaf())
						{
							if(!node.isEmptyLeaf())
								plassert( mPrimitiveSource.leafIndexIsValid(node.childIndex()) );
						}
						else
						{
							plassert( pl::contains_key(data.mNodes, node.childIndexLeft()) );
							plassert( pl::contains_key(data.mNodes, node.childIndexRight()) );
						}
					};

				BVH<IndexType, PointType_> bvh{data.mNodes, data.mRootNode};
				bvh.traverseRecursive(fc, 0, data.mRootNode, aabox);
			}
		}

	private:

		void prepare()
		{
			const auto primitiveCost = mPrimitiveSource.getGeometricCost(1);
			mReserveSize = pl::plmax<IndexType>(1, (mPrimitiveSource.getNbPrimitivesInLeaves() / pl::plmax<IndexType>(1, pl::plmax<RealType>(mOptions.targetCost, primitiveCost) / primitiveCost) * 2) / mBranches.NbBranches);

			//for(auto &b : mBranches)
				//pl::recreate(b, mBlockAllocator, mAllocator);

			const auto nbThreads = pl::plmin<IndexType>(pl::tasks.max_threads(), mBranches.NbBranches);
			mNbBranchesPerCore = mBranches.NbBranches / nbThreads;
			mBranchIndexes | pl::zero_range;

			mPrimitiveSource.prepare(mOptions.targetCost, nbThreads);
		}

		void buildImpl(CBvhBoundaryRange auto&& boundaries, BoxType aabox)
		{
			const auto thNo = pl::tasks.thread_num();
			const auto branchIndex = selectBranch(thNo);
			auto &branch = mBranches.branch(branchIndex);

			branch.emplace_back();
			mRootNode = mBranches.build_index(branchIndex, IndexType{}).index();

			buildRecursive(branch.back(), aabox, boundaries, 1);
		}

		struct Split
		{
			IndexType axis, binIndex;
			RealType cost, posLeft, posRight;
			IndexType nbLeft, nbRight;
		};

		static constexpr RealType TraversalCost = 1;

		[[nodiscard]] RealType splitGeometricCost(const Split &split, RealType bmin, RealType bmax, RealType area, RealType totalAreaInv) const noexcept
		{
			const RealType
				sizeLeft = split.posLeft - bmin, sizeRight = bmax - split.posRight,
				ef = (!split.nbLeft || !split.nbRight) ? mOptions.emptyFactor : 1,
				cl = !split.nbLeft ? 0 : mPrimitiveSource.getGeometricCost(split.nbLeft) * (sizeLeft * totalAreaInv),
				cr = !split.nbRight ? 0 : mPrimitiveSource.getGeometricCost(split.nbRight) * (sizeRight * totalAreaInv);

			if((!split.nbLeft && sizeRight >= area) || (!split.nbRight && sizeLeft >= area))
				return pl::constants::infhighest<RealType>;

			return TraversalCost + ef * (cl + cr);
		}

		class SAH
		{
		protected:

			IndexType nbTests, axis;
			BoxType box;
			IndexType nbBins;
			RealType bmin, bmax, nodeSize, sizeInv, binSize;

		public:

			SAH(IndexType nbTests_, IndexType axis_, const BoxType &box_) noexcept
				: nbTests(nbTests_), axis(axis_), box(box_)
			{
				bmin = box.v1[axis];
				bmax = box.v2[axis];

				nodeSize = bmax - bmin;
				nbBins = nbTests + 1;

				binSize = nodeSize / (RealType)nbBins;
				sizeInv = RealType(1) / binSize;
			}

			SAH(const SAH &o) = default;

			[[nodiscard]] IndexType index(RealType p) const noexcept
				{ return impl::getBinIndex(p, nbBins - 1, bmin, sizeInv); }

			[[nodiscard]] Split find(const CBvhBoundaryRange auto &boundaries, auto &&geometricCostFc) const noexcept
			{
				if(nbBins <= 1 || binSize < constants::epsilon<RealType>) [[unlikely]]
					return Split{
						.axis = axis,
						.binIndex = 0,
						.cost = pl::constants::infhighest<RealType>,
						.posLeft = std::numeric_limits<RealType>::quiet_NaN(),
						.posRight = std::numeric_limits<RealType>::quiet_NaN(),
						.nbLeft = 0,
						.nbRight = 0
					};

				thread_local std::vector<IndexType> binCounters;
				thread_local std::vector<RealType> leftPos, rightPos;

				binCounters.resize(nbBins);
				leftPos.resize(nbBins);
				rightPos.resize(nbBins);

				MiddleLeftRightBins<RealType, IndexType> bins{
					nbBins,
					hn::make_pair(middle, pl::range_data(binCounters)),
					hn::make_pair(left,   pl::range_data(leftPos)),
					hn::make_pair(right,  pl::range_data(rightPos))
				};

				impl::fillBins(boundaries, axis, bins, bmin, sizeInv, bool_v<Point>);

				plassert(rg::accumulate(bins.range(middle), IndexType{}) == boundaries.size());

				Split split, potentialSplit;
				const auto totalAreaInv = RealType{1} / nodeSize;

				plassert(!pl::plisnan(totalAreaInv));

				split.cost = pl::constants::infhighest<RealType>;

				potentialSplit.axis = axis;

				potentialSplit.nbRight = rg::size(boundaries);
				potentialSplit.nbLeft = 0;

				const auto *leftPosPtr = bins.data(left);
				const auto *rightPosPtr = bins.data(right) + 1;
				IndexType i=0;
				for(const auto *binPtr = bins.data(middle), *binLast = bins.data(middle) + (bins.size() - 1); binPtr < binLast && potentialSplit.nbRight; ++binPtr, ++leftPosPtr, ++rightPosPtr, ++i)
				{
					potentialSplit.nbLeft += *binPtr;
					potentialSplit.nbRight -= *binPtr;
					potentialSplit.posLeft = *leftPosPtr;
					potentialSplit.posRight = *rightPosPtr;

					plassert(potentialSplit.nbLeft + potentialSplit.nbRight == (IndexType)boundaries.size());

					potentialSplit.cost = PL_FWD(geometricCostFc)(potentialSplit, bmin, bmax, nodeSize, totalAreaInv);

					plassert(!pl::plisnan(potentialSplit.cost));

					if(potentialSplit.cost < split.cost)
					{
						split = potentialSplit;
						split.binIndex = i;
					}
				}

				return split;
			}
		};

		[[nodiscard]] auto selectBranch(IndexType threadNo)
		{
			auto branchIndex = threadNo * mNbBranchesPerCore + mBranchIndexes[threadNo];

			if(mBranches.branch(branchIndex).size() + 2 > mBranches.MaxSize) [[unlikely]]
			{
				++mBranchIndexes[threadNo];
				if(mBranchIndexes[threadNo] == mNbBranchesPerCore)
					throw std::bad_alloc();
				++branchIndex;
			}

			plassert(branchIndex < mBranches.m_branches.size());

			return branchIndex;
		}

		std::pair<TreeNode &, TreeNode &> splitNode(TreeNode &node, const Split &split, IndexType threadNo)
		{
			const auto branchIndex = selectBranch(threadNo);
			auto &branch = mBranches.branch(branchIndex);
			branch.reserve(mReserveSize);

			const auto childIndex = mBranches.build_index(branchIndex, branch.size());

			plassert(branch.empty() || mBranches.build_index(branchIndex, branch.size() + 1).high() > mBranches.build_index(branchIndex, branch.size() - 1).high());

			auto &nodeLeft = branch.emplace_back();
			auto &nodeRight = branch.emplace_back();

			node.childIndex(childIndex.index());
			node.axis(split.axis);
			node.split = {
				split.nbLeft ? split.posLeft : pl::constants::inflowest<RealType>,
				split.nbRight ? split.posRight : pl::constants::infhighest<RealType>
			};

			return {nodeLeft, nodeRight};
		}

		void setLeaf(TreeNode &node, CBvhBoundaryRange auto &&boundaries, IndexType threadNo)
		{
			node.setLeaf();

			if(rg::empty(boundaries))
				node.setEmptyLeaf();
			else
				node.childIndex(mPrimitiveSource.createLeaf(boundaries, threadNo));
		}

		void buildRecursive(
			TreeNode &node,
			BoxType box,
			CBvhBoundaryRange auto&& boundaries,
			szt depth = 1
		)
		{
			const IndexType nbPrimitives = rg::size(boundaries);
			const auto lastCost = mPrimitiveSource.getGeometricCost(nbPrimitives);

			// Create leaf node if max depth or target cost is reached.
			if(depth >= mOptions.maxDepth || lastCost <= mOptions.targetCost || nbPrimitives <= 1)
			{
				setLeaf(node, boundaries, pl::tasks.thread_num());
			}
			else
			{
				Split split;

				pl::assert_valid(box);

				const auto isBetterCost = [&](const RealType c) { return c < lastCost; };

				const IndexType nbTests =
					depth < mOptions.useSahAtDepth
					?
						1
					:
						nbPrimitives <= mTotalNbPrimitives / 4
						?
							pl::plmin<IndexType>(mOptions.maxSahTests / 4, 2)
						:
							mOptions.maxSahTests;

				auto sah = pl::make_array_sequence(size_v<NbAxis>, [&](auto&& i){ return SAH{nbTests, (IndexType)i.value, box}; });
				const auto geometricCostFc = PL_LAMBDA_FORWARD_THIS(splitGeometricCost);

				// Split in the middle at the largest axis until mOptions.useSahAtDepth is reached.
				if(depth < mOptions.useSahAtDepth)
				{
					split.axis = box.largest_axis();
					split = sah[split.axis].find(boundaries, geometricCostFc);
				}
				// Test splits with SAH
				else
				{
					split.cost = pl::constants::infhighest<RealType>;

					for(const auto i : mir(NbAxis))
					{
						auto pSplit = sah[i].find(boundaries, geometricCostFc);

						if(pSplit.cost < split.cost)
							split = pSplit;
					}
				}

				// Create leaf node if no good split is found.
				if(!isBetterCost(split.cost))
				{
					setLeaf(node, boundaries, pl::tasks.thread_num());
				}
				else
				{
					plassert(split.nbLeft + split.nbRight > 0);
					plassert(split.nbLeft + split.nbRight == nbPrimitives);
					plassert(split.nbLeft + split.nbRight <= mTotalNbPrimitives);

					// Execute the split
					auto childs = splitNode(node, split, pl::tasks.thread_num());

					BoxType boxLeft{box};
					boxLeft.v2.set(split.posLeft, split.axis);

					BoxType boxRight{box};
					boxRight.v1.set(split.posRight, split.axis);

					auto &sahUsed = sah[split.axis];

					pl::partition_nb(boundaries, split.nbLeft,
						[&split, &sahUsed](const auto &p)
							{ return sahUsed.index(p.getMiddle(split.axis)) <= split.binIndex; }
					);

					const auto splitAndContinueLeft = [&]
						{ buildRecursive(childs.first, boxLeft, boundaries | rgv::take_exactly(split.nbLeft), depth + 1); };

					const auto splitAndContinueRight = [&]
						{ buildRecursive(childs.second, boxRight, boundaries | rgv::drop_exactly(split.nbLeft), depth + 1); };

					pl::tasks.task_group([&](auto &tg)
					{
						const bool finalTask = split.nbLeft <= 1024*128;
						tg.run(std::ref(splitAndContinueLeft), finalTask, !finalTask);
						splitAndContinueRight();
					});
					//splitAndContinueLeft();
					//splitAndContinueRight();
				}
			}
		}
	};
}
