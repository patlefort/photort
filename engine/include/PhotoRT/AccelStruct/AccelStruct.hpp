/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>

#include <patlib/box.hpp>
#include <patlib/multi_tagged_span.hpp>

namespace PhotoRT::AccelStruct
{
	template <typename T_>
	concept CBoundaryEntry =
		std::swappable<T_> &&
		std::regular<T_> &&
		requires
		{
			typename T_::EntryType;
			typename T_::point_type;
			typename T_::BoundType;
		} &&
		std::constructible_from<T_, typename T_::EntryType, typename T_::BoundType> &&
		requires(T_ e, const T_ ce, typename T_::EntryType index, typename T_::BoundType bounds, u32 axis)
		{
			{ ce.getLeft(axis) } noexcept -> pl::CArithmetic;
			{ ce.getRight(axis) } noexcept -> pl::CArithmetic;
			{ ce.getMiddle(axis) } noexcept -> pl::CArithmetic;
			{ ce.getBounds() } noexcept -> std::convertible_to<typename T_::BoundType>;
			{ e.setBounds(bounds) } noexcept;
			{ ce.getIndex() } noexcept -> std::convertible_to<typename T_::EntryType>;
			{ e.setIndex(index) } noexcept;
		};

	template <typename EntryType_, typename PointType_>
	struct BoundaryEntry
	{
	public:

		using EntryType = EntryType_;
		using point_type = PointType_;
		using BoundType = pl::box<PointType_>;

	private:
		EntryType_ mIndex;
		BoundType mBounds;

	public:

		BoundaryEntry() = default;
		BoundaryEntry(EntryType_ index, const BoundType &bounds) noexcept { setIndex(index); setBounds(bounds); }

		[[nodiscard]] auto getLeft(u32 axis) const noexcept { return mBounds.v1[axis]; }
		[[nodiscard]] auto getRight(u32 axis) const noexcept { return mBounds.v2[axis]; }
		[[nodiscard]] auto getMiddle(u32 axis) const noexcept { return pl::pllerp(mBounds.v1[axis], mBounds.v2[axis], typename point_type::value_type{0.5}); }
		[[nodiscard]] const auto &getBounds() const noexcept { return mBounds; }
		template <typename T_> void setBounds(const T_ &b) noexcept { mBounds = b; }
		[[nodiscard]] const auto &getIndex() const noexcept { return mIndex; }
		void setIndex(EntryType_ i) noexcept { mIndex = i; }

		[[nodiscard]] bool operator==(const BoundaryEntry &o) const noexcept
		{
			return mIndex == o.mIndex && mBounds == o.mBounds;
		}
	};

	template <typename EntryType_, typename PointType_>
	struct BoundaryEntryMiddle
	{
	public:

		using EntryType = EntryType_;
		using point_type = PointType_;
		using BoundType = pl::box<PointType_>;

	private:
		EntryType_ mIndex;
		BoundType mBounds;
		PointType_ middle;

	public:

		BoundaryEntryMiddle() = default;
		BoundaryEntryMiddle(EntryType_ index, const BoundType &bounds) noexcept { setIndex(index); setBounds(bounds); }

		[[nodiscard]] auto getLeft(u32 axis) const noexcept { return mBounds.v1[axis]; }
		[[nodiscard]] auto getRight(u32 axis) const noexcept { return mBounds.v2[axis]; }
		[[nodiscard]] auto getMiddle(u32 axis) const noexcept { return middle[axis]; }
		[[nodiscard]] const auto &getBounds() const noexcept { return mBounds; }
		template <typename T_> void setBounds(const T_ &b) noexcept { mBounds = b; middle = b.middle(); }
		[[nodiscard]] const auto &getIndex() const noexcept { return mIndex; }
		void setIndex(EntryType_ i) noexcept { mIndex = i; }

		[[nodiscard]] bool operator==(const BoundaryEntryMiddle &o) const noexcept
		{
			return mIndex == o.mIndex && mBounds == o.mBounds;
		}
	};

	template <typename EntryType_, typename PointType_>
	struct BoundaryEntryPoint
	{
	public:

		using EntryType = EntryType_;
		using point_type = PointType_;
		using BoundType = PointType_;

	private:
		EntryType_ mIndex;
		BoundType mBounds;

	public:

		BoundaryEntryPoint() = default;
		BoundaryEntryPoint(EntryType_ index_, const BoundType &bounds_) noexcept { setIndex(index_); setBounds(bounds_); }

		[[nodiscard]] auto getLeft(u32 axis) const noexcept { return mBounds[axis]; }
		[[nodiscard]] auto getRight(u32 axis) const noexcept { return mBounds[axis]; }
		[[nodiscard]] auto getMiddle(u32 axis) const noexcept { return mBounds[axis]; }
		[[nodiscard]] const auto &getBounds() const noexcept { return mBounds; }
		template <typename T_> void setBounds(const T_ &b) { mBounds = b; }
		[[nodiscard]] const auto &getIndex() const noexcept { return mIndex; }
		void setIndex(EntryType_ i) noexcept { mIndex = i; }

		[[nodiscard]] bool operator==(const BoundaryEntryPoint &o) const noexcept
		{
			return mIndex == o.mIndex && mBounds == o.mBounds;
		}
	};

	PL_MAKE_MULTITAGGEDSPAN_TAG(middle)
	PL_MAKE_MULTITAGGEDSPAN_TAG(left)
	PL_MAKE_MULTITAGGEDSPAN_TAG(right)

	template <std::unsigned_integral SizeT_>
	using LeftRightBins = pl::dynamic_multi_tagged_span<
		hn::pair<type_c<left_t>, type_c<SizeT_>>,
		hn::pair<type_c<right_t>, type_c<SizeT_>>
	>;

	template <pl::CArithmetic PosType_, std::unsigned_integral SizeT_>
	using MiddleLeftRightBins = pl::dynamic_multi_tagged_span<
		hn::pair<type_c<middle_t>, type_c<SizeT_>>,
		hn::pair<type_c<left_t>, type_c<PosType_>>,
		hn::pair<type_c<right_t>, type_c<PosType_>>
	>;


	namespace impl
	{
		template <typename EntryType_>
		struct BoundaryListStack
		{
			using ItemType = EntryType_;
			using StackAllocatorType = pl::stack_allocator<ItemType, size_c<256>>;
			using ListType = std::vector<ItemType, StackAllocatorType>;

		private:
			typename StackAllocatorType::stack_data data;

		public:
			BoundaryListStack() : list(StackAllocatorType{{}, &data}) {}
			BoundaryListStack(const szt size) : list(size, StackAllocatorType{{}, &data}) {}

			ListType list;
		};

		template <typename EntryType_, typename Alloc_ = std::allocator<EntryType_>>
		struct BoundaryList
		{
			using ItemType = EntryType_;
			using ListType = std::vector<ItemType, Alloc_>;

			BoundaryList() = default;
			BoundaryList(const szt size) : list(size) {}
			BoundaryList(const szt size, const Alloc_ &alloc) : list(size, alloc) {}
			BoundaryList(const Alloc_ &alloc) : list(alloc) {}

			ListType list;
		};

		template <typename RealType_, typename SizeT_>
		constexpr SizeT_ getBinIndex(RealType_ pos, SizeT_ indexMax, RealType_ bmin, RealType_ binSizeInv)
		{
			return pl::plmin<SizeT_>((pos - bmin) * binSizeInv, indexMax);
		}

		template <typename RealType_, typename CountType_>
		constexpr void splitCount(
			const auto &source,
			RealType_ position,
			CountType_ & BOOST_RESTRICT nbLeft, CountType_ & BOOST_RESTRICT nbRight,
			RealType_ epsilon,
			auto &&getLeftFc, auto &&getRightFc) noexcept
		{
			nbLeft = 0;
			nbRight = 0;

			const auto pLeft = position + epsilon, pRight = position - epsilon;

			for(const auto &e : source)
			{
				if(PL_FWD(getLeftFc)(e) < pLeft)
					++nbLeft;
				if(PL_FWD(getRightFc)(e) >= pRight)
					++nbRight;
			}
		}

		template <typename RealType_, typename CountType_>
		constexpr void splitCount(
			const auto &source,
			RealType_ position,
			CountType_ & BOOST_RESTRICT nbLeft, CountType_ & BOOST_RESTRICT nbRight,
			RealType_ & BOOST_RESTRICT posLeft, RealType_ & BOOST_RESTRICT posRight,
			auto &&getMidFc, auto &&getLeftFc, auto &&getRightFc) noexcept
		{
			nbLeft = 0;
			nbRight = 0;

			posLeft = pl::constants::inflowest<RealType_>;
			posRight = pl::constants::infhighest<RealType_>;

			for(const auto &e : source)
			{
				if(PL_FWD(getMidFc)(e) < position)
				{
					++nbLeft;
					posLeft = posLeft /omax/ PL_FWD(getRightFc)(e);
				}
				else
				{
					++nbRight;
					posRight = posRight /omin/ PL_FWD(getLeftFc)(e);
				}
			}
		}

		template <typename RangeType_>
		constexpr void splitBoundaryListCopy(
			const RangeType_ &source,
			RangeType_ &list,
			auto &&checkFc) noexcept
		{
			if(list.empty())
				return;

			auto itTarget = list.begin();
			auto end = list.end();
			for(const auto &e : source)
			{
				if(PL_FWD(checkFc)(e))
				{
					*itTarget = e;
					if(++itTarget == end)
						break;
				}
			}

			plassert(itTarget == end);
		}

		constexpr void splitBoundaryList(
			auto &list,
			auto nb,
			auto &&checkFc) noexcept
		{
			if(nb)
			{
				auto itEnd = list.end() - 1;
				auto itFirst = list.begin();
				auto itStart = itFirst;
				while(itStart != itEnd)
				{
					if(PL_FWD(checkFc)(*itStart))
					{
						++itStart;
						if(std::distance(itFirst, itStart) >= nb)
							break;
					}
					else
					{
						*itStart = *itEnd;
						--itEnd;
					}
				}
			}

			list.resize(nb);
		}

		template <typename RealType_, typename SizeT_>
		constexpr void fillBins(const rg::input_range auto &list, u32 axis, LeftRightBins<SizeT_> &bins, RealType_ bmin, RealType_ binSizeInv) noexcept
		{
			plassert(!bins.empty());

			std::memset(bins.data(left), 0, bins.size() * sizeof(*bins.data(left)));
			std::memset(bins.data(right), 0, bins.size() * sizeof(*bins.data(right)));

			for(const auto indexMax = bins.size() - 1; const auto &e : list)
			{
				const auto binLeft = getBinIndex(e.getLeft(axis), indexMax, bmin, binSizeInv);
				const auto binRight = getBinIndex(e.getRight(axis), indexMax, bmin, binSizeInv);

				++bins[left][binLeft];
				++bins[right][binRight];
			}
		}

		template <typename RealType_, typename SizeT_>
		constexpr void fillBins(const rg::input_range auto &list, u32 axis, std::span<SizeT_> bins, RealType_ bmin, RealType_ binSizeInv) noexcept
		{
			std::memset(bins.data(), 0, pl::container_memory_size(bins));

			for(const auto indexMax = static_cast<if32>(bins.size()) - 1; const auto &e : list)
			{
				const auto bin = getBinIndex(e.getMiddle(axis), indexMax, bmin, binSizeInv);

				++bins[bin];
			}
		}

		template <typename RealType_, typename SizeT_, bool PointSet_ = false>
		constexpr void fillBins(const rg::input_range auto &list, u32 axis, MiddleLeftRightBins<RealType_, SizeT_> &bins, RealType_ bmin, RealType_ binSizeInv, bool_c<PointSet_> = {}) noexcept
		{
			plassert(!bins.empty());

			std::memset(bins.data(middle), 0, bins.size() * sizeof(*bins.data(middle)));
			for(const auto i : mir(bins.size()))
			{
				bins[left][i] = pl::constants::inflowest<RealType_>;
				bins[right][i] = pl::constants::infhighest<RealType_>;
			}

			for(const auto indexMax = bins.size() - 1; const auto &e : list)
			{
				const auto bin = getBinIndex(e.getMiddle(axis), indexMax, bmin, binSizeInv);

				++bins[middle][bin];

				bins[left][bin] = bins[left][bin] /omax/ e.getRight(axis);
				bins[right][bin] = bins[right][bin] /omin/ e.getLeft(axis);
			}

			if constexpr(!PointSet_)
			{
				for(const auto i : mir(bins.size()) | rgv::tail)
					bins[left][i] = bins[left][i] /omax/ bins[left][i - 1];

				for(const auto i : mir(bins.size()) | rgv::tail | rgv::reverse)
					bins[right][i - 1] = bins[right][i - 1] /omin/ bins[right][i];
			}
		}

		constexpr auto partitionBinsLeft(const rg::input_range auto bins, const auto mIndex) noexcept
			{ return rg::accumulate(bins | rgv::take_exactly(mIndex), rg::range_value_t<std::decay_t<decltype(bins)>>{}); }

		constexpr auto partitionBinsRight(const rg::input_range auto bins, const auto mIndex) noexcept
			{ return rg::accumulate(bins | rgv::drop_exactly(mIndex), rg::range_value_t<std::decay_t<decltype(bins)>>{}); }

		constexpr auto partitionSortedListLeft(const auto position, const auto &sorted, u32 axis, const auto &r) noexcept
		{
			const auto split = rg::upper_bound(r, position, [axis](const auto p, auto &&a) { return p < a.getLeft(axis); });

			return std::distance(sorted.list.cbegin(), split);
		}

		constexpr auto partitionSortedListRight(const auto position, const auto &sorted, u32 axis, const auto &r) noexcept
		{
			const auto split = rg::lower_bound(r, position, [axis](auto &&a, const auto p) { return a.getRight(axis) < p; });

			return std::distance(split, sorted.list.cend());
		}
	}
}
