/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/RayTracer_fwd.hpp>

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/BxDF.hpp>

namespace PhotoRT
{
	class PHOTORT_API FresnelDielectric : public FresnelBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("fresnel-dielectric", FresnelDielectric)

		[[nodiscard]] virtual Color fresnel(EnumTransportMode mode, const VecD normal, const VecD odir, const SurfaceInteraction &inter) const noexcept override;
	};

	class PHOTORT_API FresnelSchlickDielectric : public FresnelBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("fresnel-dielectric_schlick", FresnelSchlickDielectric)

		[[nodiscard]] virtual Color fresnel(EnumTransportMode mode, const VecD normal, const VecD odir, const SurfaceInteraction &inter) const noexcept override;
	};

	class PHOTORT_API FresnelConductor : public FresnelBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("fresnel-conductor", FresnelConductor)

		Color mExtinctionCoef{};

		[[nodiscard]] virtual Color fresnel(EnumTransportMode mode, const VecD normal, const VecD odir, const SurfaceInteraction &inter) const noexcept override;

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;
	};

	class PHOTORT_API FresnelSchlickConductor : public FresnelConductor
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("fresnel-conductor_schlick", FresnelSchlickConductor)

		[[nodiscard]] virtual Color fresnel(EnumTransportMode mode, const VecD normal, const VecD odir, const SurfaceInteraction &inter) const noexcept override;
	};

}
