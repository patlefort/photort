/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/RayTracer_fwd.hpp>
#include <PhotoRT/Core/DistributionSampling_fwd.hpp>

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/BxDF.hpp>

namespace PhotoRT
{
	class PHOTORT_API BxDFInvert : public BxDFChainBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("bxdf-invert", BxDFInvert)

		[[nodiscard]] virtual bool isHemisphereValid(EnumHemisphere hemisphere) const noexcept override { return mBxdf->isHemisphereValid(hemisphere == EnumHemisphere::North ? EnumHemisphere::South : EnumHemisphere::North); }

	protected:

		virtual SurfaceInteraction getSurfaceInfo(const SurfaceInteraction &inter, const VecD /*odir*/) const noexcept override
		{
			auto newInter = inter;

			newInter.lobe().normal = -newInter.lobe().normal;
			newInter.surface().shadingNormal = newInter.lobe().normal;

			return newInter;
		}
	};

	class PHOTORT_API BxDFNormalMap : public BxDFChainBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("bxdf-normalmap", BxDFNormalMap)

		EntityPtr<const INormalMapper> mNormalMapper;

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:

		virtual SurfaceInteraction getSurfaceInfo(const SurfaceInteraction &inter, const VecD odir) const noexcept override
		{
			if(!mNormalMapper)
				return inter;

			auto newInter = inter;
			newInter.lobe().calcLobeDirs(mNormalMapper->map(inter, odir));

			newInter.surface().shadingNormal = newInter.lobe().normal;

			return newInter;
		}
	};

	class PHOTORT_API BxDFMulti : public BxDFBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("bxdf-multi", BxDFMulti)

		struct BxDFEntry
		{
			EntityPtr<const IBxDF> bxdf;
			EnumRayType type;
			Real power = 1;
			bool diracDelta = false, mAnisotropic = false;
		};

		std::vector<BxDFEntry> mBxdfs;

		void addBxDF(EntityPtr<const IBxDF> bxdf, Real power = 1);
		void clearBxDFs() noexcept;

		[[nodiscard]] virtual pl::marked_optional<SampledBxDF> sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask = EnumRayType::All) const noexcept override;
		[[nodiscard]] virtual BxDFRadianceResult radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual Color alpha(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual EnumRayType getType() const noexcept final { return mType; }
		[[nodiscard]] virtual bool isDiracDelta() const noexcept final { return mDiracDelta; }

		[[nodiscard]] virtual bool isHemisphereValid(EnumHemisphere hemisphere) const noexcept override;
		[[nodiscard]] virtual bool isAnisotropic() const noexcept final { return mAnisotropic; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:
		Real mInvSamplersCount = 0;
		EnumRayType mType{};
		bool mDiracDelta = true, mAnisotropic = false;

		[[nodiscard]] uf32 pickBxDF(uf32 index, uf32 nbValid, EnumRayType typeMask = EnumRayType::All) const noexcept
		{
			if(nbValid == mBxdfs.size())
				return index;

			uf32 chosenBxDF = 0;
			for(++index;
				!static_cast<bool>(mBxdfs[chosenBxDF].type & typeMask) || --index;
				++chosenBxDF);

			plassert(chosenBxDF < mBxdfs.size());

			return chosenBxDF;
		}

		[[nodiscard]] uf32 getNbMasked(EnumRayType typeMask) const noexcept
		{
			return rg::accumulate(mBxdfs, uf32{}, rg::plus{}, [&](const auto &v) -> uf32 { return static_cast<bool>(v.type & typeMask); });
		}

	};

	class PHOTORT_API BxDFAlphaBlendMulti : public BxDFMulti
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("bxdf-blend_multi", BxDFAlphaBlendMulti)

		[[nodiscard]] virtual pl::marked_optional<SampledBxDF> sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask = EnumRayType::All) const noexcept override;
		[[nodiscard]] virtual BxDFRadianceResult radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual Color alpha(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
	};

	class PHOTORT_API BxDFVoid final : public RenderEntity, public virtual IBxDF
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("bxdf-void", BxDFVoid)

		virtual pl::marked_optional<SampledBxDF> sampleDir(EnumTransportMode, RandomReals<2> /*rnd*/, const SurfaceInteraction &, const VecD /*odir*/, IRNGInt &, EnumRayType /*typeMask*/ = EnumRayType::All) const noexcept override
			{ return SampledBxDF::NullOpaque(); }

		[[nodiscard]] virtual BxDFRadianceResult radiance(EnumTransportMode, const SurfaceInteraction &, const VecD /*odir*/, const VecD /*idir*/) const noexcept override
			{ return { {Black, White}, 0 }; }

		[[nodiscard]] virtual OptionalPDF<Real> PDF(EnumTransportMode, const SurfaceInteraction &, const VecD /*odir*/, const VecD /*idir*/) const noexcept override
			{ return pl::nullopt; }

		[[nodiscard]] virtual Color alpha(EnumTransportMode, const SurfaceInteraction &, const VecD /*odir*/, const VecD /*idir*/) const noexcept override
			{ return White; }

		[[nodiscard]] virtual EnumRayType getType() const noexcept override { return EnumRayType::Empty; }

		[[nodiscard]] virtual bool isDiracDelta() const noexcept override { return true; }

		[[nodiscard]] virtual bool isHemisphereValid(EnumHemisphere) const noexcept override { return false; }
		[[nodiscard]] virtual bool isAnisotropic() const noexcept override { return false; }
	};

	class PHOTORT_API BxDFLambertian : public BxDFBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("bxdf-lambertian", BxDFLambertian)

		[[nodiscard]] virtual pl::marked_optional<SampledBxDF> sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask = EnumRayType::All) const noexcept override;
		[[nodiscard]] virtual BxDFRadianceResult radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;

		[[nodiscard]] virtual EnumRayType getType() const noexcept final { return EnumRayType::Diffuse; }
		[[nodiscard]] virtual bool isHemisphereValid(EnumHemisphere hemisphere) const noexcept final { return hemisphere == EnumHemisphere::North; }

	};

	class PHOTORT_API BxDFOrenNayar : public BxDFLambertian
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("bxdf-orennayar", BxDFOrenNayar)

		EntityPtr<const IRealMapper> mSigma;

		[[nodiscard]] virtual pl::marked_optional<SampledBxDF> sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask = EnumRayType::All) const noexcept override;
		[[nodiscard]] virtual BxDFRadianceResult radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:

		[[nodiscard]] std::pair<Real, Real> getAB(Real sigma) const noexcept
		{
			const auto sigma2 = pl::degree_to_radian(sigma) /opow2;
			return {
					1 - (sigma2 * pl::reciprocal(2 * (sigma2 + 0.33))),
					0.45 * sigma2 * pl::reciprocal(sigma2 + 0.09)
				};
		}

		[[nodiscard]] Real compute(const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept;
	};

	class PHOTORT_API BxDFSpecularBase : public BxDFBase
	{
	public:

		virtual void setFresnel(EntityPtr<const IFresnel> f) { mFresnel = std::move(f); }
		[[nodiscard]] const auto &getFresnel() const noexcept { return mFresnel; }

		virtual void setMicrofacet(EntityPtr<const IMicrofacetsDistribution> f) { mMicrofacets = std::move(f); }
		[[nodiscard]] const auto &getMicrofacet() const noexcept { return mMicrofacets; }

		[[nodiscard]] virtual EnumRayType getType() const noexcept override { return mMicrofacets ? EnumRayType::SpecularGlossy : EnumRayType::Specular; }
		[[nodiscard]] virtual bool isDiracDelta() const noexcept override { return mMicrofacets ? false : true; }

		[[nodiscard]] virtual bool isAnisotropic() const noexcept override { return mMicrofacets && mMicrofacets->isAnisotropic(); }

		[[nodiscard]] virtual bool isDualLayer() const noexcept { return mDualLayer; }
		virtual void setDualLayer(bool v) { mDualLayer = v; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:

		VecD getHalfVector(const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept;

		EntityPtr<const IFresnel> mFresnel;
		EntityPtr<const IMicrofacetsDistribution> mMicrofacets;
		bool mDualLayer = false;
	};


	class PHOTORT_API BxDFSpecularReflection : public BxDFSpecularBase
	{
		friend class BxDFSpecularCombined;

	public:
		PRT_ENTITY_FACTORY_DECLARE("bxdf-specular_reflect", BxDFSpecularReflection)

		[[nodiscard]] virtual BxDFRadianceResult radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledBxDF> sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask = EnumRayType::All) const noexcept override;
		[[nodiscard]] virtual Color alpha(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;

		[[nodiscard]] virtual EnumRayType getType() const noexcept final { return BxDFSpecularBase::getType(); }

		[[nodiscard]] virtual bool isFresnelAsAlpha() const noexcept { return mFresnelAsAlpha; }
		virtual void setFresnelAsAlpha(bool a) noexcept { mFresnelAsAlpha = a; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

		[[nodiscard]] Color specRadiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD &hv, bool isMicrofacet, Real dp_on, Real D) const noexcept;
		[[nodiscard]] Real specPDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD &hv) const noexcept;

	protected:
		bool mFresnelAsAlpha = false;
	};

	class PHOTORT_API BxDFSpecularTransmission : public BxDFSpecularBase
	{
		friend class BxDFSpecularCombined;

	public:
		PRT_ENTITY_FACTORY_DECLARE("bxdf-specular_refract", BxDFSpecularTransmission)

		[[nodiscard]] virtual BxDFRadianceResult radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledBxDF> sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask = EnumRayType::All) const noexcept override;
		[[nodiscard]] virtual Color alpha(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;

		[[nodiscard]] virtual EnumRayType getType() const noexcept final { return BxDFSpecularBase::getType(); }
		[[nodiscard]] virtual bool isHemisphereValid(EnumHemisphere hemisphere) const noexcept final { return hemisphere == EnumHemisphere::South; }

		[[nodiscard]] pl::marked_optional<VecD> refract(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD halfVector) const noexcept;
		[[nodiscard]] Color specRadiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD &hv, bool isMicrofacet, Real dp_on, Real D, Real *pdf = nullptr) const noexcept;

		[[nodiscard]] Real specPDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD &hv) const noexcept;
	};

	class PHOTORT_API BxDFSpecularCombined : public BxDFSpecularBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("bxdf-combined", BxDFSpecularCombined)

		EntityPtr<const IBxDF> mBottom;
		BxDFSpecularTransmission mTrans;
		BxDFSpecularReflection mRefl;

		virtual void setFresnel(EntityPtr<const IFresnel> f) override
		{
			BxDFSpecularBase::setFresnel(f);
			mTrans.setFresnel(f);
			mRefl.setFresnel(std::move(f));
		}

		virtual void setMicrofacet(EntityPtr<const IMicrofacetsDistribution> f) override
		{
			BxDFSpecularBase::setMicrofacet(f);
			mTrans.setMicrofacet(f);
			mRefl.setMicrofacet(std::move(f));
		}

		[[nodiscard]] virtual BxDFRadianceResult radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledBxDF> sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask = EnumRayType::All) const noexcept override;
		[[nodiscard]] virtual Color alpha(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;

		[[nodiscard]] virtual EnumRayType getType() const noexcept final { return (mMicrofacets && mBottom) || (!mMicrofacets && !mBottom) ? EnumRayType::Specular : (mBottom ? mBottom->getType() : EnumRayType::SpecularGlossy); }
		[[nodiscard]] virtual bool isDiracDelta() const noexcept final { return getType() == EnumRayType::Specular; }
		[[nodiscard]] virtual bool isAnisotropic() const noexcept final { return (mBottom && mBottom->isAnisotropic()) || (mMicrofacets && mMicrofacets->isAnisotropic()); }
		[[nodiscard]] virtual bool isHemisphereValid(EnumHemisphere hemisphere) const noexcept final { return mBottom && mFresnel ? mBottom->isHemisphereValid(hemisphere) : true; }

		virtual void setDualLayer(bool v) override
		{
			BxDFSpecularBase::setDualLayer(v);
			mTrans.setDualLayer(v);
			mRefl.setDualLayer(v);
		}

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:

		[[nodiscard]] static Real reflectProb(const Color F) noexcept { return alphaProbability(F); }

	};

	class PHOTORT_API BxDFAlphaBlend : public BxDFBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("bxdf-blend", BxDFAlphaBlend)

		EntityPtr<const IBxDF> mTop, mBottom;

		[[nodiscard]] virtual BxDFRadianceResult radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledBxDF> sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask = EnumRayType::All) const noexcept override;
		[[nodiscard]] virtual Color alpha(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;

		[[nodiscard]] virtual EnumRayType getType() const noexcept override { return mTop->getType() | (mBottom ? mBottom->getType() : EnumRayType::Empty); }
		[[nodiscard]] virtual bool isDiracDelta() const noexcept override { return mTop->isDiracDelta() && (mBottom ? mBottom->isDiracDelta() : false); }

		[[nodiscard]] virtual bool isHemisphereValid(EnumHemisphere hemisphere) const noexcept override { return mTop->isHemisphereValid(hemisphere) || (mBottom ? mBottom->isHemisphereValid(hemisphere) : false); }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:

		[[nodiscard]] static Real firstSelectProb(const Color a) noexcept { return alphaProbability(a); }
	};

	class PHOTORT_API BxDFTabulated : public BxDFBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("bxdf-tab", BxDFTabulated)

		EntityPtr<IPDFSamplerPoint2Dr> mSampler;
		EntityPtr<IColorDistribution2D> mRadianceMap;

		[[nodiscard]] virtual BxDFRadianceResult radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledBxDF> sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask = EnumRayType::All) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;

		virtual void createFromBxDF(const IBxDF &bxdf);

		[[nodiscard]] virtual bool isHemisphereValid(EnumHemisphere) const noexcept override { return true; }
		[[nodiscard]] virtual bool isAnisotropic() const noexcept override { return true; }

		[[nodiscard]] Real getOrientation() const noexcept { return mOrientation; }
		void setOrientation(Real o)
		{
			mOrientation = o;
			mRotCos = o /ocos;
			mRotSin = o /osin;
		}

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:

		Real mOrientation = 0, mRotCos = 1, mRotSin = 0;
	};

}
