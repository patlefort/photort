/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/RayTracer_fwd.hpp>

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/BxDF.hpp>

namespace PhotoRT
{
	class PHOTORT_API MicrofacetsGGX : public MicrofacetsBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("microfacets-ggx", MicrofacetsGGX)

		[[nodiscard]] virtual Real distribution(const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD halfVector) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledHalfVector> sampleHalfVector(RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir) const noexcept override;
		[[nodiscard]] virtual Color shadowMasking(const SurfaceInteraction &inter, const VecD dir, const VecD halfVector) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD halfVector) const noexcept override;
		[[nodiscard]] virtual bool isAnisotropic() const noexcept override { return false; }

	protected:

		[[nodiscard]] Real getD(Real r, Real tang2, Real cosTheta) const noexcept
		{
			if(!pl::is_valid(tang2, valid::Weight)) [[unlikely]]
				return 0;

			const auto term = r /opow2 + tang2, term2 = std::numbers::pi_v<Real> * (cosTheta /opow2 /opow2) * (term /opow2);
			const auto D = term2 > 0 ? (r /opow2 * cosTheta) * prtreciprocal(term2) : 0;

			return pl::assert_valid(D, valid::Weight);
		}
	};

	class PHOTORT_API MicrofacetsTrowbridgeReitz : public RenderEntity, public IMicrofacetsDistribution
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("microfacets-trowbridge", MicrofacetsTrowbridgeReitz)

		EntityPtr<const IRealMapper> mParamu, mParamv;

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

		[[nodiscard]] Real getOrientation() const noexcept { return mOrientation; }
		void setOrientation(Real o)
		{
			mOrientation = o;
			mRotCos = pl::plcos(o);
			mRotSin = pl::plsin(o);
		}

		[[nodiscard]] virtual Real distribution(const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD halfVector) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledHalfVector> sampleHalfVector(RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir) const noexcept override;
		[[nodiscard]] virtual Color shadowMasking(const SurfaceInteraction &inter, const VecD dir, const VecD halfVector) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD halfVector) const noexcept override;

		[[nodiscard]] virtual Color shadowMasking2(const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD halfVector) const noexcept override
		{
			return pl::reciprocal(shadowMasking(inter, odir, halfVector) + shadowMasking(inter, -idir, halfVector) + White);
		}

		[[nodiscard]] virtual bool isAnisotropic() const noexcept override { return mParamv != nullptr; }

	protected:

		Real mOrientation = 0, mRotCos = 1, mRotSin = 0;

		[[nodiscard]] Real paramToAlpha(const Real p) const noexcept
		{
			return p;
		}

		[[nodiscard]] Real getD(Real ru, Real rv, Real cosPhi, Real sinPhi, Real tang2, Real cosTheta) const noexcept
		{
			if(!pl::is_valid(tang2, valid::Weight)) [[unlikely]]
				return 0;

			const auto e = ((ru ? cosPhi /opow2 / (ru /opow2) : 0) + (rv ? sinPhi /opow2 / (rv /opow2) : 0)) * tang2;
			const auto D = (std::numbers::pi_v<Real> * (ru ? ru : rv) * (rv ? rv : ru) * (cosTheta /opow2 /opow2) * pl::pow2(e + 1)) /oprtreciprocal;

			return pl::assert_valid(D, valid::Weight);
		}

	};

}
