/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>

#include <PhotoRT/Core/RenderEntity.hpp>
#include <PhotoRT/Core/Edf.hpp>
#include <PhotoRT/Core/SurfaceMapper.hpp>

namespace PhotoRT
{
	class PHOTORT_API EDFBase : public RenderEntity, public IEDF
	{
	public:

		EntityPtr<const IColorMapper> mColorMapper;

		[[nodiscard]] virtual Color radianceDiracDelta(const SurfaceInteraction &, EnumHemisphere) const noexcept override { return Black; }

		[[nodiscard]] virtual pl::marked_optional<EDFSample> sampleDir(RandomReals<2> rnd, const SurfaceInteraction &inter) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(const SurfaceInteraction &inter, VecD idir) const noexcept override;

		[[nodiscard]] virtual bool isDiracDelta() const noexcept override { return false; }
		[[nodiscard]] virtual bool isHemisphereValid(EnumHemisphere hemisphere) const noexcept override
			{ return (hemisphere == EnumHemisphere::North && isEmittingFront()) || (hemisphere == EnumHemisphere::South && isEmittingBack()); }

		virtual void setPower(Real p) { mPower = p; }
		[[nodiscard]] Real getPower() const noexcept { return mPower; }

		virtual void setEmitFront(bool e) { mEmitFront = e; updateSidePdf(); }
		[[nodiscard]] bool isEmittingFront() const noexcept { return mEmitFront; }

		virtual void setEmitBack(bool e) { mEmitBack = e; updateSidePdf(); }
		[[nodiscard]] bool isEmittingBack() const noexcept { return mEmitBack; }

		[[nodiscard]] virtual bool isAnisotropic() const noexcept override { return false; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:
		bool mEmitFront = true, mEmitBack = true;
		Real mSidePdf = 0.5;
		Real mPower = 1;

		void updateSidePdf() noexcept
		{
			const uf32 nb = (uf32)isEmittingBack() + (uf32)isEmittingFront();

			mSidePdf = nb ? 1.0 / nb : 0;
		}

		[[nodiscard]] Color getColor(const SurfaceInteraction &inter, const VecD idir) const noexcept;
		[[nodiscard]] EnumHemisphere getHemisphere(const SurfaceInteraction &inter, const VecD idir) const noexcept;
	};

	class EDFDiffuse : public EDFBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("edf-diffuse", EDFDiffuse)

		[[nodiscard]] virtual pl::marked_optional<EDFSample> sampleDir(RandomReals<2> rnd, const SurfaceInteraction &inter) const noexcept override;
		[[nodiscard]] virtual Color radiance(const SurfaceInteraction &inter, VecD idir) const noexcept override;
	};

	class EDFProjector : public EDFBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("edf-projector", EDFProjector)

		[[nodiscard]] virtual pl::marked_optional<EDFSample> sampleDir(RandomReals<2> rnd, const SurfaceInteraction &inter) const noexcept override;
		[[nodiscard]] virtual Color radiance(const SurfaceInteraction &inter, VecD idir) const noexcept override;
		[[nodiscard]] virtual Color radianceDiracDelta(const SurfaceInteraction &inter, EnumHemisphere hemisphere) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(const SurfaceInteraction &inter, VecD idir) const noexcept override;

		[[nodiscard]] virtual bool isDiracDelta() const noexcept override { return true; }
	};

}
