/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Base.hpp"

namespace PhotoRT
{
	namespace ImplPrimitives
	{
		template <typename DerivedType_ = void, pl::simd::CNeutralOrPointGeometricVector VT_ = VecP>
		PL_CRTP_CLASS(PrimitivePlane, PrimitiveBase, VT_)

			using typename base_type::point_type;
			using typename base_type::dir_type;
			using typename base_type::RealType;
			static constexpr bool Infinite = true;
			static constexpr uf32 TypeID = 3;
			static constexpr RealType IntersectionCost = 40;
			static constexpr bool AlwaysFlat = true;

			point_type mCenter;
			std::array<dir_type, 2> mDir;

			PrimitivePlane() = default;

			PrimitivePlane(std::initializer_list<pl::zero_init_t>) noexcept
				: mCenter{pl::tag::identity}, mDir{pl::tag::identity} {}

			PrimitivePlane(point_type center, dir_type dir1 = {1, 0, 0}, dir_type dir2 = {0, 0, -1}) noexcept
				: mCenter{center}, mDir{dir1, dir2} {}

			explicit PrimitivePlane(bool /*flat*/) noexcept
				{ setFlat(); }

			template <pl::simd::CNeutralOrPointGeometricVector OVT_>
			[[nodiscard]] explicit operator PrimitivePlane<DerivedType_, OVT_>() const noexcept
			{
				using OT = PrimitivePlane<DerivedType_, OVT_>;
				using OVT = typename OT::point_type;
				using ODirVT = typename OT::dir_type;

				return OT{
					static_cast<OVT>(mCenter), static_cast<ODirVT>(mDir[0]), static_cast<ODirVT>(mDir[1])
				};
			}

			void setFlat() noexcept
			{
				mCenter.set_identity();
				mDir[0] = {1, 0, 0};
				mDir[1] = {0, 0, -1};
			}

			template <pl::CMatrix4x4 MatrixType_>
			[[nodiscard]] MatrixType_ getTangentSpace(point_type pos, type_c<MatrixType_> = {}) const noexcept
			{
				return MatrixType_{
					mDir[0],
					mDir[1],
					derived().getSurfaceDir(pos),
					mCenter
				}.transpose();
			}

			[[nodiscard]] dir_type getSurfaceNormal(point_type position) const noexcept
				{ return derived().getSurfaceDir(position) /oprtnormalize; }

			[[nodiscard]] RealType getSurfaceArea() const noexcept
				{ return pl::constants::infhighest<RealType>; }

			[[nodiscard]] dir_type getSurfaceDir(point_type ) const noexcept
				{ return mDir[0] /ocross/ mDir[1]; }

			[[nodiscard]] pl::box<point_type> getAABox() const noexcept
				{ return pl::box<point_type>(pl::tag::infhighest); }

			[[nodiscard]] point_type getMiddle() const noexcept
				{ return mCenter; }

			[[nodiscard]] point_type getPointOnSurface() const noexcept
				{ return mCenter; }

			[[nodiscard]] bool intersect(point_type rayOrigin, dir_type rayDir, dir_type /*rayDirReciprocal*/) const noexcept
			{
				return std::get<0>(Intersection::testPlane(rayOrigin, rayDir, mCenter, mDir[0], mDir[1])).any();
			}

			[[nodiscard]] pl::marked_optional<Collision<point_type, RealType>> intersectExt(point_type rayOrigin, dir_type rayDir, dir_type /*rayDirReciprocal*/, RealType dist = pl::constants::infhighest<RealType>, bool /*calcUv*/ = false) const noexcept
			{
				const auto [mask, u, v, resDist, bf] = Intersection::testPlane(rayOrigin, rayDir, mCenter, mDir[0], mDir[1]);

				if(mask.any() && resDist <= dist)
				{
					return Collision<point_type, RealType>{
							.uv{u, v, 0},
							.dist = resDist,
							.backface = bf.any()
						};
				}

				return pl::nullopt;
			}

			[[nodiscard]] bool intersect(const pl::box<point_type> &) const noexcept
			{
				return false;
			}

			[[nodiscard]] static auto buildSerializer(auto &p)
			{
				return pl::io::range{pl::io::mode_select, ' ', std::make_tuple(
					std::ref(p.mCenter),
					pl::io::range{pl::io::mode_select, ' ', rgv::all(p.mDir)}
				)};
			}

			void serialize(std::ostream &os) const
			{
				os << buildSerializer(*this);
			}

			void unserialize(std::istream &is)
			{
				is >> buildSerializer(*this);

				mDir[0] = mDir[0].as_dir() /oprtnormalize;
				mDir[1] = mDir[1].as_dir() /oprtnormalize;
			}

			auto &operator*= (const pl::CMatrix4x4TransformPointAndDir<point_type, dir_type> auto &m) noexcept
			{
				mCenter = m * mCenter;
				mDir[0] = dirSafeTransform(mDir[0], m);
				mDir[1] = dirSafeTransform(mDir[1], m);
				return *this;
			}
		PL_CRTP_CLASS_END
	} // namespace ImplPrimitives

	template <typename VT_ = VecP> using PrimitivePlane = ImplPrimitives::PrimitivePlane<void, VT_>;
}
