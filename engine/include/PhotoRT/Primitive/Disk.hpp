/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Plane.hpp"

namespace PhotoRT
{
	namespace ImplPrimitives
	{
		template <pl::simd::CNeutralOrPointGeometricVector VT_ = VecP>
		PL_CRTP_CLASS_FINAL(PrimitiveDisk, PrimitivePlane, VT_)

			using typename base_type::point_type;
			using typename base_type::dir_type;
			using typename base_type::RealType;
			static constexpr bool Infinite = false;
			static constexpr uf32 TypeID = 4;
			static constexpr RealType IntersectionCost = 60;
			using base_type::AlwaysFlat;

			PrimitiveDisk() = default;
			PrimitiveDisk(point_type center, dir_type dir1 = {1, 0, 0}, dir_type dir2 = {0, 0, -1}, const RealType radiusOuter = 1, const RealType radiusInner = 0) noexcept
			 : crtp_base_type{center, dir1, dir2}, mRadiusOuter{radiusOuter}, mRadiusInner{radiusInner} {}

			PrimitiveDisk(const RealType radiusOuter, const RealType radiusInner) noexcept
				: mRadiusOuter{radiusOuter}, mRadiusInner{radiusInner} { this->setFlat(); }

			PrimitiveDisk(std::initializer_list<pl::zero_init_t>) noexcept
				: crtp_base_type({}), mRadiusOuter{}, mRadiusInner{} {}

			using base_type::mCenter; using base_type::mDir;
			RealType mRadiusOuter, mRadiusInner;

			using base_type::operator*=;

			template <pl::simd::CNeutralOrPointGeometricVector OVT_>
			[[nodiscard]] explicit operator PrimitiveDisk<OVT_>() const noexcept
			{
				using OT = PrimitiveDisk<OVT_>;
				using OVT = typename OT::point_type;
				using ODirVT = typename OT::dir_type;
				using ORealType = typename OT::RealType;

				return OT{
					static_cast<OVT>(mCenter), static_cast<ODirVT>(mDir[0]), static_cast<ODirVT>(mDir[1]),
					static_cast<ORealType>(mRadiusOuter), static_cast<ORealType>(mRadiusInner)
				};
			}

			[[nodiscard]] OptionalPDF<RealType> PDF() const noexcept { return this->getSurfaceArea() /oprtreciprocal; }
			[[nodiscard]] OptionalPDF<RealType> PDF(point_type point, dir_type dir, dir_type dirInv, pl::marked_optional<const SampledAreaResult<point_type> &> sampled = pl::nullopt) const noexcept
				{ return samplePDF(*this, point, dir, dirInv, sampled); }

			[[nodiscard]] RealType getSurfaceArea() const noexcept
				{ return (mRadiusOuter /opow2 - mRadiusInner /opow2) * (0.5 * std::numbers::pi_v<RealType>); }

			[[nodiscard]] pl::marked_optional<SampledAreaResult<point_type>> sampleArea(RandomReals<2> rnd) const noexcept
			{
				const auto dif = mRadiusOuter - mRadiusInner;
				const auto [rad, costheta, sintheta] = uniformSampleDisk(static_cast<pl::values<RealType, 2>>(rnd));
				const auto dir = Dir2Dr{costheta, sintheta};
				const auto s = dir * (dif * rad) + dir * mRadiusInner;
				const auto n = derived().getSurfaceNormal({});

				return SampledAreaResult<point_type>{
					.pos = mCenter + (mDir[0] * s[0]) + (mDir[1] * s[1]),
					.uv = static_cast<point_type>(s.as_point()),
					.normal = n,
					.shadingNormal = n,
					.pdf = PDF().value()
				};
			}

			[[nodiscard]] pl::marked_optional<SampledAreaResultBackface<point_type>> sampleVisibleSurfaceArea(RandomReals<2> rnd, point_type point) const noexcept
				{ return samplePrimitiveArea(*this, rnd, point); }

			[[nodiscard]] pl::box<point_type> getAABox() const noexcept
			{
				return pl::box<point_type>{(mCenter - mRadiusOuter).as_point(), (mCenter + mRadiusOuter).as_point()};
			}

			[[nodiscard]] bool intersect(point_type rayOrigin, dir_type rayDir, dir_type /*rayDirReciprocal*/) const noexcept
			{
				const auto [mask, u, v, resDist, bf] = Intersection::testPlane(rayOrigin, rayDir, mCenter, mDir[0], mDir[1]);

				if(mask.any())
				{
					const auto dist2 = ((rayOrigin + rayDir * resDist) - mCenter) /osdot;

					return dist2 <= mRadiusOuter /opow2 && dist2 >= mRadiusInner /opow2;
				}

				return false;
			}

			[[nodiscard]] pl::marked_optional<Collision<point_type, RealType>> intersectExt(point_type rayOrigin, dir_type rayDir, dir_type /*rayDirReciprocal*/, RealType dist = pl::constants::infhighest<RealType>, bool /*calcUv*/ = false) const noexcept
			{
				const auto [mask, u, v, resDist, bf] = Intersection::testPlane(rayOrigin, rayDir, mCenter, mDir[0], mDir[1]);

				if(mask.any() && resDist <= dist)
				{
					const auto dist2 = ((rayOrigin + rayDir * resDist) - mCenter) /osdot;

					if(dist2 <= mRadiusOuter /opow2 && dist2 >= mRadiusInner /opow2)
					{
						return Collision<point_type, RealType>{
								.uv{u, v, 0},
								.dist = resDist,
								.backface = bf.any()
							};
					}
				}

				return pl::nullopt;
			}

			[[nodiscard]] bool intersect(const pl::box<point_type> &box) const noexcept
			{
				return this->getAABox().overlap(box);
			}

			void serialize(std::ostream &os) const
			{
				base().serialize(os);

				if(!pl::io::is_binary(os))
					os << ' ';
				os << pl::io::range{pl::io::mode_select, ' ', std::forward_as_tuple(mRadiusOuter, mRadiusInner)};
			}

			void unserialize(std::istream &is)
			{
				base().unserialize(is);

				is >> pl::io::range{pl::io::mode_select, ' ', std::forward_as_tuple(mRadiusOuter, mRadiusInner)};
			}

			auto &operator*= (const pl::CMatrix4x4TransformPointAndDir<point_type, dir_type> auto &m) noexcept
			{
				base() *= m;
				return *this;
			}
		PL_CRTP_CLASS_END
	} // namespace ImplPrimitives

	template <typename VT_ = VecP> using PrimitiveDisk = ImplPrimitives::PrimitiveDisk<VT_>;
}
