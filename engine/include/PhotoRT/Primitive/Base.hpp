/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Primitive.hpp>
#include <PhotoRT/Core/Intersection.hpp>

#include <patlib/crtp.hpp>
#include <patlib/marked_optional.hpp>

namespace PhotoRT::ImplPrimitives
{
	template <typename Primitive_>
	[[nodiscard]] constexpr auto samplePrimitiveArea(const Primitive_ &p, RandomReals<2> rnd, typename Primitive_::point_type point) noexcept
		-> pl::marked_optional<SampledAreaResultBackface<typename Primitive_::point_type>>
	{
		return p.sampleArea(rnd)
			.and_then([&](auto areaSp) -> pl::marked_optional<SampledAreaResultBackface<typename Primitive_::point_type>>
			{
				auto dir = static_cast<typename Primitive_::dir_type>(areaSp.pos - point);
				const auto dist = dir /oprtlength;
				if(!dist)
					return pl::nullopt;

				dir = dir /olnormalize/ dist;
				auto dp = areaSp.normal /odot/ dir;
				const bool isBackface = dp > 0;

				dp = dp /oabs;
				if(dp < constants::epsilon<decltype(dp)>)
					return pl::nullopt;

				areaSp.pdf *= geometricTermPos(dist) * (dp /oprtreciprocal);
				if(!pl::is_valid(areaSp.pdf, valid::Weight))
					return pl::nullopt;

				return SampledAreaResultBackface<typename Primitive_::point_type>{areaSp, isBackface};
			});
	}

	template <typename Primitive_>
	[[nodiscard]] constexpr OptionalPDF<typename Primitive_::RealType> samplePDF(
		const Primitive_ &p,
		typename Primitive_::point_type point,
		typename Primitive_::dir_type dir,
		typename Primitive_::dir_type dirInv,
		pl::marked_optional<const SampledAreaResult<typename Primitive_::point_type> &> sampled = pl::nullopt) noexcept
	{
		struct DistNormal
		{
			typename Primitive_::RealType distSqr = 0;
			typename Primitive_::dir_type normal;

			constexpr void reset() noexcept { distSqr = 0; }
			[[nodiscard]] constexpr bool has_value() const noexcept { return distSqr != 0; }
		};

		return sampled
			.and_then([&](const auto &sd) -> pl::marked_optional<DistNormal>
			{
				const auto distSqr = (sd.pos - point) /osdot;
				if(!distSqr)
					return pl::nullopt;

				return DistNormal{
						.distSqr = distSqr,
						.normal = static_cast<typename Primitive_::dir_type>(sd.normal)
					};
			})
			.or_else([&]
			{
				return p.intersectExt(point, dir, dirInv)
					.transform([&](const auto &collision)
					{
						return DistNormal{
								.distSqr = collision.dist /opow2,
								.normal = p.getSurfaceNormal(point + dir * collision.dist)
							};
					});
			})
			.and_then([&](const auto &dn) -> OptionalPDF<typename Primitive_::RealType>
			{
				const auto pdf = pdfAtoW(p.PDF().value(), dn.distSqr, dn.normal /odot/ dir /oabs);
				if(!pl::is_valid(pdf, valid::Weight))
					return pl::nullopt;

				return pdf;
			});
	}

	template <typename DerivedType_, pl::simd::CNeutralOrPointGeometricVector VT_ = VecP>
	class PrimitiveBase : public pl::crtp_base<DerivedType_>
	{
	public:

		using pl::crtp_base<DerivedType_>::derived;

		using point_type = VT_;
		using dir_type = pl::simd::rebind_to_dir<point_type>;
		using RealType = typename point_type::value_type;

		[[nodiscard]] constexpr RealType getMinimalSafeIncrement() const noexcept
		{
			const auto aabb = derived().getAABox();

			return (aabb.v1 /oabs /omax/ (aabb.v2 /oabs)).minimal_safe_increment();
		}

		[[nodiscard]] constexpr point_type getMiddle() const noexcept
			{ return derived().getAABox().middle(); }

		[[nodiscard]] constexpr pl::marked_optional<pl::box<point_type>> getAABoxClipped(const pl::box<point_type> boxClip) const noexcept
		{
			const auto pbox = derived().getAABox();

			if(!pbox.overlap(boxClip))
				return pl::nullopt;

			return pbox.clip(boxClip);
		}

		[[nodiscard]] constexpr pl::box<point_type> clipBoxToPlane(pl::box<point_type> box, RealType position, u32 axis, bool left) const noexcept
		{
			auto &p = left ? box.v1 : box.v2;
			p.set( pl::select(p[axis], position, left ? p[axis] > position : p[axis] < position) , axis);

			return box;
		}

		friend std::ostream &operator<<(std::ostream &os, const PrimitiveBase &o)
		{
			o.derived().serialize(os);
			return os;
		}

		friend std::istream &operator>>(std::istream &is, PrimitiveBase &o)
		{
			o.derived().unserialize(is);
			return is;
		}
	};
}
