/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Base.hpp"

namespace PhotoRT
{
	namespace ImplPrimitives
	{
		template <typename DerivedType_ = void, pl::simd::CNeutralOrPointGeometricVector VT_ = VecP>
		PL_CRTP_CLASS(PrimitiveSphere, PrimitiveBase, VT_)

			using typename base_type::point_type;
			using typename base_type::dir_type;
			using typename base_type::RealType;
			static constexpr bool Infinite = false;
			static constexpr uf32 TypeID = 2;
			static constexpr RealType IntersectionCost = 160;
			static constexpr bool AlwaysFlat = false;

			PrimitiveSphere() = default;
			PrimitiveSphere(const point_type pos, const RealType radius = 1) noexcept : mPos{pos}, mRadius{radius} {}

			PrimitiveSphere(std::initializer_list<pl::zero_init_t>) noexcept
				: mPos{pl::tag::identity}, mRadius{pl::tag::identity} {}

			point_type mPos;
			RealType mRadius;

			template <pl::simd::CNeutralOrPointGeometricVector OVT_>
			[[nodiscard]] explicit operator PrimitiveSphere<DerivedType_, OVT_>() const noexcept
			{
				using OT = PrimitiveSphere<DerivedType_, OVT_>;
				using OVT = typename OT::point_type;
				using ORealType = typename OT::RealType;

				return OT{
					static_cast<OVT>(mPos), static_cast<ORealType>(mRadius)
				};
			}

			template <pl::CMatrix4x4 MatrixType_>
			[[nodiscard]] MatrixType_ getTangentSpaceFromNormal(point_type p, dir_type n) const noexcept
			{
				MatrixType_ mat;

				std::tie(mat[0], mat[1]) = lobeDirsAnisotropic(n);

				mat[2] = n;
				mat[3] = p;

				return mat.transpose();
			}

			template <pl::CMatrix4x4 MatrixType_>
			[[nodiscard]] MatrixType_ getTangentSpace(point_type p, type_c<MatrixType_> = {}) const noexcept
			{
				return derived().template getTangentSpaceFromNormal<MatrixType_>(p, static_cast<dir_type>(p - mPos) /oprtnormalize);
			}

			[[nodiscard]] RealType getSurfaceArea() const noexcept
			{
				return mRadius /opow2 * (std::numbers::pi_v<RealType> * 4);
			}

			[[nodiscard]] dir_type getSurfaceNormal(point_type position) const noexcept
			{
				return static_cast<dir_type>(position - mPos) /oprtnormalize;
			}

			[[nodiscard]] dir_type getSurfaceDir(point_type position) const noexcept
			{
				return static_cast<dir_type>(position - mPos);
			}

			[[nodiscard]] pl::marked_optional<SampledAreaResult<point_type>> sampleArea(RandomReals<2> rnd) const noexcept
			{
				const auto dir = static_cast<dir_type>(uniformSampleSphere(static_cast<pl::values<RealType, 2>>(rnd)));

				return SampledAreaResult<point_type>{
					.pos = dir * mRadius + mPos,
					.uv = static_cast<point_type>(rnd),
					.normal = dir,
					.shadingNormal = dir,
					.pdf = derived().PDF().value()
				};
			}

			[[nodiscard]] OptionalPDF<RealType> PDF() const noexcept { return derived().getSurfaceArea() /oprtreciprocal; }
			[[nodiscard]] OptionalPDF<RealType> PDF(point_type point, dir_type /*dir*/, dir_type /*dirInv*/, pl::marked_optional<const SampledAreaResult<point_type> &> = pl::nullopt) const noexcept
			{
				const auto distance2 = (mPos - point) /osdot, radius2 = mRadius /opow2;

				if(distance2 < radius2)
					return PDF();

				const auto cosalphamax2 = radius2 / distance2;
				const auto sinalphamax = pl::plmax<RealType>(0, 1 - cosalphamax2) /oprtsqrt;

				return uniformConePdf(sinalphamax);
			}

			[[nodiscard]] pl::marked_optional<SampledAreaResultBackface<point_type>> sampleVisibleSurfaceArea(RandomReals<2> rnd, point_type point) const noexcept
			{
				const auto n = (point - mPos).as_dir();

				const auto distance2 = n /osdot;
				const auto radius2 = mRadius /opow2;

				if(distance2 < radius2)
					return samplePrimitiveArea(*this, rnd, point);

				const auto dc = distance2 /oprtsqrt;
				const Lobe<dir_type> coneDir{n /olnormalize/ dc};

				const auto cosalphamax2 = radius2 / distance2;
				const auto sinalphamax = pl::plmax<RealType>(0, 1 - cosalphamax2) /oprtsqrt;
				const auto dir = coneDir.sampleConeDir(static_cast<pl::values<RealType, 2>>(rnd), sinalphamax, cosalphamax2);

				return SampledAreaResultBackface<point_type>{{
						.pos = mPos + dir * mRadius,
						.uv = static_cast<point_type>(sphereThetaPhi(dir)),
						.normal = dir,
						.shadingNormal = dir,
						.pdf = uniformConePdf(sinalphamax)
					}, false};
			}

			[[nodiscard]] pl::box<point_type> getAABox() const noexcept
			{
				return pl::box<point_type>((mPos - mRadius).as_point(), (mPos + mRadius).as_point());
			}

			[[nodiscard]] point_type getMiddle() const noexcept
				{ return mPos; }

			[[nodiscard]] point_type getPointOnSurface() const noexcept
				{ auto res = mPos; res.set(mPos[size_v<0>] + mRadius, size_v<0>); return res; }

			[[nodiscard]] bool intersect(point_type rayOrigin, dir_type rayDir, dir_type /*rayDirReciprocal*/) const noexcept
			{
				return std::get<0>(Intersection::testSphere(rayOrigin, rayDir, mPos, mRadius * mRadius)).any();
			}

			/*void getShadingInfo(SurfaceData &surfaceData) const noexcept
			{
				surfaceData.shadingNormal = surfaceData.surfaceNormal;
				surfaceData.medium = nullptr;
				surfaceData.isDelta(false);

				surfaceData.uv = CoordinatePosition{tag::cartesian, surfaceData.surfaceNormal}.to(tag::polar)->as_point();

				surfaceData.pMat = derived().template getTangentSpaceFromNormal<Matrix>(surfaceData.localPos, surfaceData.surfaceNormal);
			}*/

			[[nodiscard]] pl::marked_optional<Collision<point_type, RealType>> intersectExt(point_type rayOrigin, dir_type rayDir, dir_type /*rayDirReciprocal*/, RealType dist = pl::constants::infhighest<RealType>, bool calcUv = false) const noexcept
			{
				const auto [mask, resDist, bf] = Intersection::testSphere(rayOrigin, rayDir, mPos, mRadius /opow2);

				if(mask.any() && resDist <= dist)
				{
					return Collision<point_type, RealType>{
							.uv = calcUv ? CoordinatePosition{tag::cartesian, derived().getSurfaceNormal(rayOrigin + rayDir * resDist)}.to(tag::polar)->as_point() : point_type{},
							.dist = resDist,
							.backface = bf.any()
						};
				}

				return pl::nullopt;
			}

			[[nodiscard]] bool intersect(const pl::box<point_type> &box) const noexcept
			{
				RealType distSquared = mRadius /opow2;
				point_type boxSphereDist1 = mPos - box.v1, boxSphereDist2 = mPos - box.v2;

				boxSphereDist1 *= boxSphereDist1;
				boxSphereDist2 *= boxSphereDist2;

				boxSphereDist1 = pl::select(boxSphereDist1, point_type{}, mPos < box.v1);
				boxSphereDist2 = pl::select(boxSphereDist2, point_type{}, mPos > box.v2);

				distSquared = (boxSphereDist1 /ohadd + boxSphereDist2 /ohadd)[0];

				return distSquared > 0;
			}

			void serialize(std::ostream &os) const
			{
				os << pl::io::range{pl::io::mode_select, ' ', std::forward_as_tuple(mPos, mRadius)};
			}

			void unserialize(std::istream &is)
			{
				is >> pl::io::range{pl::io::mode_select, ' ', std::forward_as_tuple(mPos, mRadius)};
			}

			auto &operator*= (const pl::CMatrix4x4TransformPointAndDir<point_type, dir_type> auto &m) noexcept
			{
				mPos = m * mPos;
				return *this;
			}
		PL_CRTP_CLASS_END
	} // namespace ImplPrimitives

	template <typename VT_ = VecP> using PrimitiveSphere = ImplPrimitives::PrimitiveSphere<void, VT_>;
}
