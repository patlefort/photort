/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Base.hpp"

#include <boost/container/static_vector.hpp>

namespace PhotoRT
{
	namespace ImplPrimitives
	{
		template <typename DerivedType_ = void, pl::simd::CNeutralOrPointGeometricVector VT_ = VecP>
		PL_CRTP_CLASS(PrimitiveTriangle, PrimitiveBase, VT_)

			using typename base_type::point_type;
			using typename base_type::dir_type;
			using typename base_type::RealType;
			static constexpr bool Infinite = false;
			static constexpr uf32 TypeID = 1;
			static constexpr RealType IntersectionCost = 80;
			static constexpr bool AlwaysFlat = true;

			PrimitiveTriangle() = default;
			PrimitiveTriangle(std::array<point_type, 3> p) noexcept
				: mP{p} {}
			PrimitiveTriangle(point_type p1, point_type p2 = pl::tag::identity, point_type p3 = pl::tag::identity) noexcept
				: mP{p1, p2, p3} {}

			PrimitiveTriangle(std::initializer_list<pl::zero_init_t>) noexcept
				: mP{pl::tag::identity, pl::tag::identity, pl::tag::identity} {}

			std::array<point_type, 3> mP;

			template <pl::simd::CNeutralOrPointGeometricVector OVT_>
			[[nodiscard]] explicit operator PrimitiveTriangle<DerivedType_, OVT_>() const noexcept
			{
				using OT = PrimitiveTriangle<DerivedType_, OVT_>;
				using OVT = typename OT::point_type;

				return OT{
					static_cast<OVT>(mP[0]), static_cast<OVT>(mP[1]), static_cast<OVT>(mP[2])
				};
			}

			template <pl::CMatrix4x4 MatrixType_>
			[[nodiscard]] MatrixType_ getTangentSpace(point_type pos, type_c<MatrixType_> = {}) const noexcept
			{
				return MatrixType_{
					mP[1] - mP[0],
					mP[2] - mP[0],
					derived().getSurfaceDir(pos),
					mP[0]
				}.transpose();
			}

			[[nodiscard]] RealType getSurfaceArea() const noexcept
				{ return (mP[1] - mP[0]) /ocross/ (mP[2] - mP[0]) /oprtlength * 0.5; }

			[[nodiscard]] dir_type getSurfaceNormal(point_type position) const noexcept
				{ return derived().getSurfaceDir(position) /oprtnormalize; }

			[[nodiscard]] dir_type getSurfaceDir(point_type) const noexcept
				{ return static_cast<dir_type>((mP[1] - mP[0]) /ocross/ (mP[2] - mP[0])); }

			[[nodiscard]] OptionalPDF<RealType> PDF() const noexcept { return derived().getSurfaceArea() /oprtreciprocal; }
			[[nodiscard]] OptionalPDF<RealType> PDF(point_type point, dir_type dir, dir_type dirInv, pl::marked_optional<const SampledAreaResult<point_type> &> sampled = pl::nullopt) const noexcept
				{ return samplePDF(*this, point, dir, dirInv, sampled); }

			[[nodiscard]] pl::marked_optional<SampledAreaResult<point_type>> sampleArea(RandomReals<2> rnd) const noexcept
			{
				const auto p = uniformSampleTriangle(static_cast<pl::values<RealType, 2>>(rnd));
				const auto n = derived().getSurfaceNormal({});

				return SampledAreaResult<point_type>{
					.pos = mP[0] + (mP[1] - mP[0]) * p[0] + (mP[2] - mP[0]) * p[1],
					.uv = static_cast<point_type>(p),
					.normal = n,
					.shadingNormal = n,
					.pdf = derived().PDF().value()
				};
			}

			[[nodiscard]] pl::marked_optional<SampledAreaResultBackface<point_type>> sampleVisibleSurfaceArea(RandomReals<2> rnd, point_type point) const noexcept
				{ return samplePrimitiveArea(*this, rnd, point); }

			[[nodiscard]] auto getAABox() const noexcept
			{
				return pl::box<point_type>{mP[0] /omin/ mP[1] /omin/ mP[2], mP[0] /omax/ mP[1] /omax/ mP[2]};
			}

			[[nodiscard]] auto clip(const pl::box<point_type> boxClip) const noexcept
				-> boost::container::static_vector<point_type, 10>
			{
				const auto &bar1 = pl::simd::vector_access(boxClip.v1);
				const auto &bar2 = pl::simd::vector_access(boxClip.v2);

				boost::container::static_vector<point_type, 10> vl2, *cvp, *ovp, vts{
					mP[0],
					mP[1],
					mP[2],
					mP[0]
				};

				cvp = &vts;
				ovp = &vl2;

				const auto clipAxis =
					[&cvp, &ovp]
					(RealType pos, szt axis, szt side)
					{
						const auto &cv = *cvp;
						auto &ov = *ovp;

						for(szt i=0, ms = cv.size() - 1; i<ms; ++i)
						{
							szt mni = pl::min_index(cv[i][axis], cv[i+1][axis]);
							szt mxi = !mni;
							mni += i;
							mxi += i;

							RealType mn = cv[mni][axis];
							RealType mx = cv[mxi][axis];

							if(mn < pos && mx > pos)
								ov.push_back(cv[mni] + (cv[mxi] - cv[mni]) * ((pos - mn) / (mx - mn)));

							if(side == 0 && cv[i+1][axis] < pos) {}
							else if(side == 1 && cv[i+1][axis] > pos) {}
							else
								ov.push_back(cv[i+1]);
						}

						ov.push_back(ov[0]);
					};

				for(const auto i : mir<szt>(3))
				{
					clipAxis(bar1[i], i, 0);
					if(ovp->size() == 1)
						return {};

					pl::adl_swap(cvp, ovp);
					ovp->clear();

					clipAxis(bar2[i], i, 1);
					if(ovp->size() == 1)
						return {};

					pl::adl_swap(cvp, ovp);
					ovp->clear();
				}

				vts.pop_back();

				return vts;
			}

			[[nodiscard]] pl::box<point_type> clipBoxToPlane(pl::box<point_type> box, RealType position, u32 axis, bool left) const noexcept
			{
				std::array<point_type, 4> vts;
				auto vtsIt = vts.begin();

				const auto testSegment = [&](point_type v1, point_type v2)
					{
						const auto v1a = v1[axis], v2a = v2[axis];
						const bool allLeft = v1a <= position && v2a <= position;
						const bool allRight = v1a >= position && v2a >= position;

						if((allRight && left) || (allLeft && !left))
						{
							*vtsIt++ = v2;
							return;
						}

						if((allLeft && left) || (allRight && !left))
							return;

						const auto clipped = pl::assert_valid( v1 + (v2 - v1) * (position - v1a) / (v2a - v1a) );

						*vtsIt++ = clipped;

						if((v2a >= position && left) || (v2a <= position && !left))
							*vtsIt++ = v2;
					};

				testSegment(mP[0], mP[1]);
				testSegment(mP[1], mP[2]);
				testSegment(mP[2], mP[0]);

				const auto nb = std::distance(vts.begin(), vtsIt);

				plassert(nb <= 4);

				if(nb == 4)
				{
					box = box.join( { pl::tag::range_init, rg::make_subrange(vts.begin(), vtsIt) } );

					pl::assert_valid(box);
					plassert(bool(box.v1 <= box.v2));
				}

				return box;
			}

			[[nodiscard]] pl::marked_optional<pl::box<point_type>> getAABoxClipped(const pl::box<point_type> boxClip) const noexcept
			{
				const auto clipped = derived().clip(boxClip);

				if(clipped.empty())
					return pl::nullopt;

				return pl::box<point_type>{pl::tag::range_init, clipped};
			}

			[[nodiscard]] point_type getMiddle() const noexcept
				{ return (mP[0] + mP[1] + mP[2]) / 3; }

			[[nodiscard]] point_type getPointOnSurface() const noexcept
				{ return getMiddle(); }

			[[nodiscard]] bool intersect(point_type rayOrigin, dir_type rayDir, dir_type /*rayDirReciprocal*/) const noexcept
			{
				return std::get<0>(Intersection::testTriangle(rayOrigin, rayDir, mP[0], static_cast<dir_type>(mP[1] - mP[0]), static_cast<dir_type>(mP[2] - mP[0]))).any();
			}

			[[nodiscard]] pl::marked_optional<Collision<point_type, RealType>> intersectExt(point_type rayOrigin, dir_type rayDir, dir_type /*rayDirReciprocal*/, RealType dist = pl::constants::infhighest<RealType>, bool /*calcUv*/ = false) const noexcept
			{
				const auto [mask, u, v, resDist, bf] = Intersection::testTriangle(rayOrigin, rayDir, mP[0], static_cast<dir_type>(mP[1] - mP[0]), static_cast<dir_type>(mP[2] - mP[0]));

				if(mask.any() && resDist <= dist)
				{
					return Collision<point_type, RealType>{
							.uv{u, v, 0},
							.dist = resDist,
							.backface = bf.any()
						};
				}

				return pl::nullopt;
			}

			[[nodiscard]] bool intersect(const pl::box<point_type> &box) const noexcept
			{
				return Intersection::triInCollisionWithAAbox(mP[0], mP[1], mP[2], box.middle(), box.dims() * 0.5);
			}

			[[nodiscard]] RealType getMinimalSafeIncrement() const noexcept
			{
				return (mP[0] /oabs /omax/ (mP[1] /oabs) /omax/ (mP[2] /oabs)).minimal_safe_increment();
			}

			void serialize(std::ostream &os) const
			{
				os << pl::io::range{pl::io::mode_select, ' ', rgv::all(mP)};
			}

			void unserialize(std::istream &is)
			{
				is >> pl::io::range{pl::io::mode_select, ' ', rgv::all(mP)};
			}

			auto &operator*= (const pl::CMatrix4x4TransformPointAndDir<point_type, dir_type> auto &m) noexcept
			{
				for(auto &p : mP)
					p = m * p;
				return *this;
			}
		PL_CRTP_CLASS_END

		template <typename DerivedType_ = void, pl::simd::CNeutralOrPointGeometricVector VT_ = VecP>
		PL_CRTP_CLASS(PrimitiveTriangleEx, PrimitiveTriangle, VT_)

			using typename base_type::point_type;
			using typename base_type::dir_type;
			using typename base_type::RealType;
			using base_type::TypeID;
			using base_type::IntersectionCost;
			using base_type::AlwaysFlat;

			using base_type::mP;

			PrimitiveTriangleEx() = default;

			template <typename D_>
			PrimitiveTriangleEx(const PrimitiveTriangle<D_, point_type> &o)
				: crtp_base_type{o.mP}
			{
				mNormal = derived().getSurfaceNormal({});
			}

			PrimitiveTriangleEx(std::initializer_list<pl::zero_init_t>) noexcept
				: crtp_base_type({}), mNormal{pl::tag::identity}, mVn{pl::tag::identity} {}

			PrimitiveTriangleEx(
				point_type p1, point_type p2 = pl::tag::identity, point_type p3 = pl::tag::identity,
				dir_type normal = pl::tag::identity,
				dir_type n1 = pl::tag::identity, dir_type n2 = pl::tag::identity, dir_type n3 = pl::tag::identity) noexcept
				: crtp_base_type{p1, p2, p3}, mNormal{normal}, mVn{n1, n2, n3} {}

			dir_type mNormal;
			std::array<dir_type, 3> mVn;

			template <pl::simd::CNeutralOrPointGeometricVector OVT_>
			[[nodiscard]] explicit operator PrimitiveTriangleEx<DerivedType_, OVT_>() const noexcept
			{
				using OT = PrimitiveTriangleEx<DerivedType_, OVT_>;
				using OVT = typename OT::point_type;
				using ODirVT = typename OT::dir_type;

				return OT{
					static_cast<OVT>(mP[0]), static_cast<OVT>(mP[1]), static_cast<OVT>(mP[2]),
					static_cast<ODirVT>(mNormal),
					static_cast<ODirVT>(mVn[0]), static_cast<ODirVT>(mVn[1]), static_cast<ODirVT>(mVn[2])
				};
			}

			template <pl::simd::CNeutralOrPointGeometricVector OVT_>
			[[nodiscard]] explicit operator PrimitiveTriangle<DerivedType_, OVT_>() const noexcept
			{
				using OT = PrimitiveTriangle<DerivedType_, OVT_>;
				using OVT = typename OT::point_type;

				return OT{
					static_cast<OVT>(mP[0]), static_cast<OVT>(mP[1]), static_cast<OVT>(mP[2])
				};
			}

			[[nodiscard]] dir_type getSurfaceNormal(point_type) const noexcept
				{ return mNormal; }

			[[nodiscard]] dir_type getSurfaceDir(point_type) const noexcept
				{ return mNormal; }

			/*void getShadingInfo(SurfaceData &surfaceData) const noexcept
			{
				base().getShadingInfo(surfaceData);

				surfaceData.shadingNormal = derived().getInterpolatedNormal(static_cast<point_type>(surfaceData.uv));
			}*/

			using base_type::sampleArea;
			[[nodiscard]] pl::marked_optional<SampledAreaResult<point_type>> sampleArea(RandomReals<2> rnd) const noexcept
			{
				const auto p = uniformSampleTriangle(static_cast<pl::values<RealType, 2>>(rnd));
				const auto uv = static_cast<point_type>(p);

				return SampledAreaResult<point_type>{
					.pos = mP[0] + (mP[1] - mP[0]) * p[0] + (mP[2] - mP[0]) * p[1],
					.uv = uv,
					.normal = mNormal,
					.shadingNormal = derived().getInterpolatedNormal(uv),
					.pdf = derived().PDF().value()
				};
			}

			[[nodiscard]] dir_type getInterpolatedNormal(point_type uv) const noexcept
			{
				#ifdef PL_LIB_VECTOR_SIMD
					if constexpr(pl::simd::CSimdVector<point_type>)
					{
						const auto uvDir = uv.as_dir();
						return mVn[0] + (mVn[1] - mVn[0]) * (uvDir /osshuffle<0, 0, 0, 3>) +
						                (mVn[2] - mVn[0]) * (uvDir /osshuffle<1, 1, 1, 3>);
					}
					else
				#endif
						return mVn[0] + (mVn[1] - mVn[0]) * (uv[size_v<0>]) +
					                  (mVn[2] - mVn[0]) * (uv[size_v<1>]);
			}

			auto &operator*= (const pl::CMatrix4x4TransformPointAndDir<point_type, dir_type> auto &m) noexcept
			{
				base() *= m;

				mNormal = dirSafeTransform(mNormal, m);
				for(auto &vn : mVn)
					vn = dirSafeTransform(vn, m);

				return *this;
			}

			void serialize(std::ostream &os) const
			{
				base().serialize(os);

				if(!pl::io::is_binary(os))
					os << ' ';

				os << pl::io::range{pl::io::mode_select, ' ', std::make_tuple(
						std::ref(mNormal),
						pl::io::range{pl::io::mode_select, ' ', rgv::all(mVn)}
					)};
			}

			void unserialize(std::istream &is)
			{
				base().unserialize(is);

				is >> pl::io::range{pl::io::mode_select, ' ', std::make_tuple(std::ref(mNormal), pl::io::range{pl::io::mode_select, ' ', rgv::all(mVn)})};
			}
		PL_CRTP_CLASS_END
	} // namespace ImplPrimitives

	template <typename VT_ = VecP> using PrimitiveTriangle = ImplPrimitives::PrimitiveTriangle<void, VT_>;
	template <typename VT_ = VecP> using PrimitiveTriangleEx = ImplPrimitives::PrimitiveTriangleEx<void, VT_>;

}
