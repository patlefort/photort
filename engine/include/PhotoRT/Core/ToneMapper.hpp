/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Bitmap_fwd.hpp"
#include "ColorProfile_fwd.hpp"
#include "ToneMapper_fwd.hpp"

#include "Base.hpp"
#include "Distribution.hpp"
#include "FilmBitmap.hpp"

namespace PhotoRT
{
	class IToneMapper : public IDistributionMapper<Color, Color>
	#ifdef PRT_FEATURE_PACKED_COLOR
		, public IDistributionMapper<PackedColor, PackedColor>
	#endif
	{
	public:

		[[nodiscard]] auto &colorInter() noexcept { return static_cast<IDistributionMapper<Color, Color> &>(*this); }
		[[nodiscard]] const auto &colorInter() const noexcept { return static_cast<const IDistributionMapper<Color, Color> &>(*this); }

		#ifdef PRT_FEATURE_PACKED_COLOR
			[[nodiscard]] auto &colorPackedInter() noexcept { return static_cast<IDistributionMapper<PackedColor, PackedColor> &>(*this); }
			[[nodiscard]] const auto &colorPackedInter() const noexcept { return static_cast<const IDistributionMapper<PackedColor, PackedColor> &>(*this); }
		#endif

		virtual void prepare(const FilmBitmap &bitmap, const IColorProfile &colorProfile) = 0;
	};

	class ToneMapperBase : public IToneMapper
	{
	public:

		virtual void prepare(const FilmBitmap &/*bitmap*/, const IColorProfile &colorProfile) override
		{
			mColorProfile = &colorProfile;
		}

	protected:

		const IColorProfile *mColorProfile = nullptr;
	};

}
