/*
	PhotoRT

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "RenderEntityBase_fwd.hpp"

#include "Base.hpp"

#include <boost/bimap/bimap.hpp>
#include <boost/bimap/multiset_of.hpp>
#include <boost/bimap/list_of.hpp>

#include <filesystem>

namespace PhotoRT
{
	using RenderEntityRelationMap = boost::bimaps::bimap<
		boost::bimaps::multiset_of<std::string, std::less<>>,
		boost::bimaps::multiset_of<std::string, std::less<>>,
		boost::bimaps::list_of_relation
	>;

	using ExtensionMimeTypeMap = boost::bimaps::bimap<
		boost::bimaps::multiset_of<std::string, std::less<>>,
		boost::bimaps::multiset_of<std::string, std::less<>>,
		boost::bimaps::list_of_relation
	>;

	class PHOTORT_API ExceptionEntityCollection : public Exception
	{
	public:

		using Exception::Exception;

		[[nodiscard]] virtual const char *module() const noexcept override;
	};

	class IMimeType
	{
	public:
		virtual ~IMimeType() = default;

		[[nodiscard]] virtual std::string getMimeType(const std::filesystem::path &filename) const = 0;
		[[nodiscard]] virtual std::string getMimeExtension(std::string_view mimeType) const = 0;
	};

	struct RenderEntitySocketDesc
	{
		std::string type;
	};

	class IExportHook
	{
	public:
		virtual ~IExportHook() = default;
	};

	using RenderEntitySocketDescMap = std::map<std::string, RenderEntitySocketDesc, std::less<>>;
}
