/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Scene_fwd.hpp"
#include "Material_fwd.hpp"
#include "Emitter_fwd.hpp"
#include "PrimitiveGroup_fwd.hpp"
#include "Interaction_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"
#include "Transform.hpp"
#include "Primitive.hpp"
#include "SurfaceData.hpp"

#include <patlib/string.hpp>
#include <patlib/marked_optional.hpp>

#ifdef PRT_IPC
	#include <boost/interprocess/containers/vector.hpp>
#endif

#include <boost/container/flat_map.hpp>
#include <boost/static_string/static_string.hpp>

namespace PhotoRT
{
	using PrimGroupPropertiesMapEntryShared = PrimGroupPropertiesMapEntry<EntityPtr<IPrimitiveGroupProperties>>;
	using PrimGroupPropertiesMapEntryPtr = PrimGroupPropertiesMapEntry<IPrimitiveGroupProperties *>;

	using PrimGroupPropertiesMapShared = std::vector<PrimGroupPropertiesMapEntryShared>;
	using PrimGroupPropertiesMapPtr = std::vector<PrimGroupPropertiesMapEntryPtr>;

	#ifdef PRT_IPC
		template <typename T_, typename Alloc_>
		using PrimitiveVectorTypeNoInit = boost::interprocess::vector<T_, pl::allocator_noinit<T_, pl::allocator_with_stats<T_, memtag::Primitive, Alloc_>>>;

		template <typename T_, typename Alloc_>
		using PrimitiveVectorType = boost::interprocess::vector<T_, pl::allocator_with_stats<T_, memtag::Primitive, Alloc_>>;
	#else
		template <typename T_, typename Alloc_>
		using PrimitiveVectorTypeNoInit = std::vector<T_, pl::allocator_noinit<T_, pl::allocator_with_stats<T_, memtag::Primitive, Alloc_>>>;

		template <typename T_, typename Alloc_>
		using PrimitiveVectorType = std::vector<T_, pl::allocator_with_stats<T_, memtag::Primitive, Alloc_>>;
	#endif

	struct PrimitiveMaterial
	{
		EntityPtr<const Material> material;
	};

	class IPrimitiveGroupProperties
	{
	public:

		virtual ~IPrimitiveGroupProperties() = default;

		[[nodiscard]] virtual std::string_view getObjectID() const noexcept = 0;

		virtual void setClassName(std::string_view s) = 0;
		[[nodiscard]] virtual const std::pair<std::string, szt> &getClassName() const noexcept = 0;

		virtual void setEmitter(bool emitter = true) = 0;
		[[nodiscard]] virtual bool isEmitter() const noexcept = 0;

		virtual void setLightPortal(bool lp) = 0;
		[[nodiscard]] virtual bool isLightPortal() const noexcept = 0;

		virtual void setEmitterGroup(std::string_view g) = 0;
		[[nodiscard]] virtual std::string_view getEmitterGroup() const noexcept = 0;

		[[nodiscard]] virtual pl::marked_optional<const PrimitiveMaterial &> getMaterial(PrimitiveIndex primitiveIndex) const noexcept = 0;
		virtual void setMaterial(EntityPtr<const Material> mat, PrimitiveIndex start) = 0;
		virtual void setMaterial(EntityPtr<const Material> mat) = 0;
	};

	class PHOTORT_API PrimitiveGroupProperties : public RenderEntity, public virtual IPrimitiveGroupProperties
	{
	protected:

		bool mEmitter = false, mLightPortal = false;
		std::pair<std::string, szt> mClassName;
		std::string mEmitterGroup;

	public:
		PRT_ENTITY_FACTORY_DECLARE("object_props-", PrimitiveGroupProperties)

		boost::container::flat_map<PrimitiveIndex, PrimitiveMaterial> mMaterials;

		virtual void setClassName(std::string_view s) override { mClassName.first = s; mClassName.second = std::hash<std::string>{}(mClassName.first); }
		[[nodiscard]] virtual const std::pair<std::string, szt> &getClassName() const noexcept override { return mClassName; }

		[[nodiscard]] virtual std::string_view getObjectID() const noexcept override { return getEntityId(); }

		virtual void setEmitter(bool emitter = true) override { mEmitter = emitter; }
		[[nodiscard]] virtual bool isEmitter() const noexcept override { return mEmitter; }

		virtual void setEmitterGroup(std::string_view g) override { mEmitterGroup = g; }
		[[nodiscard]] virtual std::string_view getEmitterGroup() const noexcept override { return mEmitterGroup; }

		virtual void setLightPortal(bool lp) override { mLightPortal = lp; }
		[[nodiscard]] virtual bool isLightPortal() const noexcept override { return mLightPortal; }

		[[nodiscard]] virtual pl::marked_optional<const PrimitiveMaterial &> getMaterial(PrimitiveIndex primitiveIndex) const noexcept override;
		virtual void setMaterial(EntityPtr<const Material> mat, PrimitiveIndex start) override;
		virtual void setMaterial(EntityPtr<const Material> mat) override;

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	};

	template <typename PtrType_>
	struct PrimGroupPropertiesMapEntry
	{
		PrimGroupPropertiesMapEntry() = default;

		PrimGroupPropertiesMapEntry(const PrimGroupPropertiesMapEntry &o) = default;
		PrimGroupPropertiesMapEntry(PrimGroupPropertiesMapEntry &&o) = default;

		template <typename PT_>
		PrimGroupPropertiesMapEntry(const PrimGroupPropertiesMapEntry<PT_> &o) noexcept
			: id{o.id}, props{&(*o.props)} {}

		PrimGroupPropertiesMapEntry &operator=(const PrimGroupPropertiesMapEntry &o) = default;
		PrimGroupPropertiesMapEntry &operator=(PrimGroupPropertiesMapEntry &&o) = default;

		template <typename PT_>
		PrimGroupPropertiesMapEntry &operator=(const PrimGroupPropertiesMapEntry<PT_> &o) noexcept
		{
			id = o.id;
			props = &(*o.props);
			return *this;
		}

		PrimGroupPropertiesMapEntry(const std::pair<std::string, szt> &id_, const PtrType_ &props_)
			: id{ {id_.first.begin(), id_.first.end()}, id_.second }, props{props_} {}

		std::pair<boost::static_strings::static_string<50>, szt> id;
		PtrType_ props;
	};

	using UVDirPair = pl::simd::basic_vector<UVdir, size_c<2>>;

	struct UVMapping
	{
		PrimitiveVectorTypeNoInit<UV, PmrAllocatorGlobal<UV, memtag::Primitive>> uv;
		PrimitiveVectorTypeNoInit<UVDirPair, PmrAllocatorGlobal<UVDirPair, memtag::Primitive>> tangents;
	};

	struct SampledPrimitive
	{
		PrimitiveIndex index;
		Real pdf;
		RandomReal offset;
	};

	struct PrimGroupCollision : public Collision<>
	{
		PrimitiveIndex primIndex;
	};


	class IPrimitiveList
	{
	public:
		virtual ~IPrimitiveList() = default;

		virtual PrimitiveIndex getNbPrimitives() const noexcept = 0;
	};

	class IPrimitiveGroup : public virtual IPrimitiveList, public virtual IPrimitiveGroupProperties, public virtual IAABox
	{
	public:
		virtual ~IPrimitiveGroup() = default;

		[[nodiscard]] virtual bool isAlwaysFlat() const noexcept = 0;
		[[nodiscard]] virtual bool isInfinite() const noexcept = 0;
		[[nodiscard]] virtual uf32 getType() const noexcept = 0;

		[[nodiscard]] virtual Box getPrimitiveAABox(PrimitiveIndex primitiveIndex = 0) const noexcept = 0;
		[[nodiscard]] virtual VecP getPrimitiveMiddle(PrimitiveIndex primitiveIndex = 0) const noexcept = 0;
		[[nodiscard]] virtual VecP getPointOnSurface(PrimitiveIndex primitiveIndex = 0) const noexcept = 0;

		virtual Box computeAABox() noexcept = 0;

		virtual void buildCdf() = 0;
		[[nodiscard]] virtual bool isCdfBuilt() const noexcept = 0;
		virtual void clearCdf() noexcept = 0;

		[[nodiscard]] virtual VecD getSurfaceNormal(VecP position, PrimitiveIndex primitiveIndex = 0) const noexcept = 0;
		[[nodiscard]] virtual VecD getSurfaceDir(VecP position, PrimitiveIndex primitiveIndex = 0) const noexcept = 0;
		[[nodiscard]] virtual Real PMF(PrimitiveIndex primitiveIndex = 0) const noexcept = 0;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(PrimitiveIndex primitiveIndex = 0) const noexcept = 0;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(const Matrix &m, PrimitiveIndex primitiveIndex = 0) const noexcept = 0;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(VecP point, VecD dir, VecD dirInv, PrimitiveIndex primitiveIndex = 0, pl::marked_optional<const SampledAreaResult<VecP> &> sampled = pl::nullopt) const noexcept = 0;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(const Matrix &m, VecP point, VecD dir, VecD dirInv, PrimitiveIndex primitiveIndex = 0, pl::marked_optional<const SampledAreaResult<VecP> &> surfaceData = pl::nullopt) const noexcept = 0;
		[[nodiscard]] virtual SampledPrimitive samplePrimitive(RandomReal r) const noexcept = 0;
		[[nodiscard]] virtual pl::marked_optional<SampledAreaResultBackface<VecP>> sampleVisibleSurfaceArea(RandomReals<2> rnd, VecP point, PrimitiveIndex primitiveIndex = 0) const noexcept = 0;
		[[nodiscard]] virtual Real getSurfaceArea(PrimitiveIndex primitiveIndex = 0) const noexcept = 0;
		[[nodiscard]] virtual pl::marked_optional<SampledAreaResult<VecP>> sampleArea(RandomReals<2> rnd, PrimitiveIndex primitiveIndex = 0) const noexcept = 0;
		[[nodiscard]] virtual std::pair<bool, StatCounter> intersect(const VecP rayOrigin, const VecD rayDir, const VecD rayDirReciprocal) const noexcept = 0;
		[[nodiscard]] virtual std::pair<pl::marked_optional<PrimGroupCollision>, StatCounter> intersectExt(const VecP rayOrigin, const VecD rayDir, const VecD rayDirReciprocal, Real dist = std::numeric_limits<Real>::infinity(), bool calcUv = false) const noexcept = 0;
		[[nodiscard]] virtual pl::marked_optional<PrimGroupCollision> intersectExt(PrimitiveIndex primitiveIndex, const VecP rayOrigin, const VecD rayDir, const VecD rayDirReciprocal, Real dist = std::numeric_limits<Real>::infinity(), bool calcUv = false) const noexcept = 0;
		[[nodiscard]] virtual bool intersect(const Box &box) const noexcept = 0;

		virtual void getShadingInfo(SurfaceData &surfaceData, PrimitiveIndex primIndex = 0) const noexcept = 0;
		[[nodiscard]] virtual Matrix getTextureTangentSpace(const SurfaceData &surfaceData, const UVMapping *tcMaps, const Matrix &texSpaceTransform) const noexcept = 0;
		[[nodiscard]] virtual const UVMapping &getUVmap(u32 mapIndex) const noexcept = 0;

		[[nodiscard]] virtual Real getMinimalSafeIncrement(PrimitiveIndex primIndex = 0) const noexcept = 0;

	};

	template <typename MapType_>
	[[nodiscard]] const IPrimitiveGroupProperties *selectPrimGroupProps(const std::pair<std::string, szt> &id, const IPrimitiveGroupProperties *obj, const MapType_ &matList)
	{
		auto it = rg::find_if(matList,
			[&](const auto &p){ return p.id.second == id.second && pl::to_string_view(p.id.first) == id.first; }
		);

		return it == matList.end() ? obj : &(*it->props);
	}

	class PrimitiveGroupBase : public PrimitiveGroupProperties, public IPrimitiveGroup {};
}
