/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "SurfaceMapper_fwd.hpp"
#include "Interaction_fwd.hpp"
#include "Instance_fwd.hpp"

#include "Base.hpp"

namespace PhotoRT
{
	template <typename OutType_>
	class ISurfaceMapper
	{
	public:
		virtual ~ISurfaceMapper() = default;

		using OutType = OutType_;

		[[nodiscard]] virtual OutType_ map(const MaterialInteraction &inter, const VecD odir, type_c<OutType_> = {}) const noexcept = 0;
	};
}
