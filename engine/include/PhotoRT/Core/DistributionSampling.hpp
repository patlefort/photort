/*
	PhotoRT

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "DistributionSampling_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"
#include "Distribution.hpp"
#include "Sampling.hpp"
#include "Bitmap.hpp"

namespace PhotoRT
{
	class PDFSamplerPoint2Dr : public PDFSamplerPoint2DrBase
	{
	public:
		virtual ~PDFSamplerPoint2Dr() = default;

		[[nodiscard]] virtual pl::marked_optional<Sample<Point2Dr>> operator() (RandomReals<2> rnd) const noexcept override
			{ return Sample<Point2Dr>{Point2Dr{rnd}, 1}; }

	};

	class PHOTORT_API PDFSamplerColorDistImportanceTiled : public PDFSamplerPoint2DrBase
	{
	public:
		virtual ~PDFSamplerColorDistImportanceTiled() = default;

		virtual void init() override;

		void setDist(EntityPtr<const IColorDistribution2D> d);
		[[nodiscard]] const auto &getDist() const noexcept { return mDist; }

		[[nodiscard]] virtual pl::marked_optional<Sample<Point2Dr>> operator() (RandomReals<2> rnd) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(Point2Dr v) const noexcept override;

		[[nodiscard]] auto getTileSize() const noexcept { return mTileSize; }
		void setTileSize(u32 s) noexcept { mTileSize = s; }

		[[nodiscard]] auto getDimensions() const noexcept { return mDims; }
		void setDimensions(pl::point2d<u32> v) noexcept { mDims = v; }

		[[nodiscard]] bool getSpherical() const noexcept { return mSpherical; }
		void setSpherical(bool v) noexcept { mSpherical = v; }

	protected:

		template <typename T_>
		[[nodiscard]] static auto getCdfXImpl(T_ &this_, u32 p) noexcept
		{
			const auto offsetC = p * (this_.mNbTilesX+2);
			const auto offsetD = p * this_.mNbTilesX;

			return CDFDistribution{
				this_.mNbTilesX+2,
				&this_.mDistrib[offsetD],
				&this_.mCdfXdata[offsetC]
			};
		}

		[[nodiscard]] auto getCdfX(u32 p) noexcept { return getCdfXImpl(*this, p); }
		[[nodiscard]] auto getCdfX(u32 p) const noexcept { return getCdfXImpl(*this, p); }

		template <typename T_>
		[[nodiscard]] static auto getCdfYImpl(T_ &this_) noexcept
		{
			return CDFDistribution{
				this_.mCdfYdata.size(),
				this_.mDistribY.data(),
				this_.mCdfYdata.data()
			};
		}

		[[nodiscard]] auto getCdfY() noexcept { return getCdfYImpl(*this); }
		[[nodiscard]] auto getCdfY() const noexcept { return getCdfYImpl(*this); }

		EntityPtr<const IColorDistribution2D> mDist;

		std::vector<Real> mCdfXdata;
		std::vector<Real> mDistrib, mDistribY;
		std::vector<Real> mCdfYdata;
		u32 mTileSize = 4, mNbTiles, mNbTilesX;
		Dir2Dr mMaxSize;
		pl::point2d<u32> mDims;
		bool mSpherical = false;
	};

}
