/*
	PhotoRT

	Copyright (C) 2021 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Memory.hpp"

#include <vector>

namespace PhotoRT
{
	template <typename T_, typename Alloc_ = std::allocator<T_>>
	using VectorNoInit = std::vector<T_, pl::allocator_noinit<T_, Alloc_>>;

	template <typename T_, typename Category_, typename Alloc_ = std::allocator<T_>>
	using VectorWithStats = std::vector<T_, pl::allocator_with_stats<T_, Category_, Alloc_>>;

	template <typename T_, typename Category_, typename Alloc_ = std::allocator<T_>>
	using VectorWithStatsNoInit = std::vector<T_, pl::allocator_noinit<T_, pl::allocator_with_stats<T_, Category_, Alloc_>>>;

	template <typename T_, typename Category_>
	using VectorWithStatsPMR = VectorWithStats<T_, Category_, std::pmr::polymorphic_allocator<T_>>;

	template <typename T_, typename Category_>
	using VectorWithStatsPMRnoInit = VectorWithStats<T_, Category_, pl::allocator_noinit<T_, std::pmr::polymorphic_allocator<T_>>>;
}