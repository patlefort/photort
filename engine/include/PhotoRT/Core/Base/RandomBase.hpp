/*
	PhotoRT

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "RandomBase_fwd.hpp"

#include "Defs.hpp"

#include <any>

namespace PhotoRT
{
	using RandomInt = u64;
	using RandomReal = MReal;

	template <typename T_, szt NB_>
	using RandomValues = pl::values<T_, NB_>;

	template <szt NB_>
	using RandomInts = RandomValues<RandomInt, NB_>;

	template <szt NB_>
	using RandomReals = RandomValues<RandomReal, NB_>;

	namespace ImplRng
	{
		template <typename T_>
		[[nodiscard]] constexpr T_ clampRng(const T_ value) noexcept
		{
			using T = pl::underlying_type_t<T_>;
			constexpr auto na = std::nextafter(T{1}, T{});
			return pl::plmin(value, T_{na});
		}

		template <std::unsigned_integral T_>
		struct ConstantGenerator
		{
			using result_type = T_;

			T_ mValue, mMin, mMax;

			constexpr auto operator() () const noexcept { return mValue; }

			[[nodiscard]] constexpr auto min() const noexcept { return mMin; }
			[[nodiscard]] constexpr auto max() const noexcept { return mMax; }

			void discard(unsigned long long) {}
		};
	} // namespace ImplRng

	template <std::floating_point RT_ = RandomReal>
	[[nodiscard]] constexpr auto uniformRealRng() noexcept { return std::uniform_real_distribution<RT_>{0, 1}; }

	template <std::unsigned_integral T_ = std::size_t>
	[[nodiscard]] constexpr auto uniformIndexRng(T_ nb) noexcept { return std::uniform_int_distribution<T_>{0, nb - 1}; }

	template <std::floating_point T_, std::unsigned_integral ST_>
	[[nodiscard]] constexpr std::pair<T_, ST_> remapRng(T_ r1, ST_ size) noexcept
	{
		T_ ip;
		const auto fp = std::modf(r1 * size, &ip);
		return {fp, pl::plmin((ST_)ip, size - 1)};
	}

	template <std::unsigned_integral T_>
	[[nodiscard]] constexpr std::pair<ImplRng::ConstantGenerator<T_>, T_> remapRng(ImplRng::ConstantGenerator<T_> cg, T_ size) noexcept
	{
		const auto index = std::uniform_int_distribution<T_>{0, size - 1}(cg);
		const auto newSize = (cg.max() - cg.min()) / size - 1;

		return {{.mValue = std::uniform_int_distribution<T_>{0, newSize}(cg), .mMin = 0, .mMax = newSize}, index};
	}

	template <std::unsigned_integral T_ = RandomInt>
	class IRNG
	{
	public:
		using result_type = T_;
		using param_type = std::any;

		virtual ~IRNG() = default;

		virtual void seed() = 0;
		virtual T_ generate() noexcept = 0;
		virtual void generate(std::span<T_> values) noexcept = 0;
		virtual void setPosition(u64 sampleNo, const Point2Di &p) = 0;
		virtual void setBounds(const Point2Di &p) = 0;
		[[nodiscard]] static constexpr T_ min() noexcept { return 0; }
		[[nodiscard]] static constexpr T_ max() noexcept { return std::numeric_limits<T_>::max(); }

		[[nodiscard]] virtual std::any param() const = 0;
		virtual void param(const std::any &param) = 0;

		auto operator() () noexcept { return generate(); }
		void operator() (std::span<T_> values) noexcept { generate(values); }
	};

	template <typename RNGT_>
	class RNGWrappable : public RNGT_
	{
		[[nodiscard]] constexpr auto &typecast() noexcept { return static_cast<RNGT_ &>(*this); }
		[[nodiscard]] constexpr const auto &typecast() const noexcept { return static_cast<const RNGT_ &>(*this); }

	public:

		using RNGT_::RNGT_;

		constexpr void setStream(typename RNGT_::state_type s)
		{
			mStream = s;
			typecast() = {(typename RNGT_::state_type)0, s};
		}

		constexpr auto operator() () noexcept
		{
			const auto res = typecast()();

			if(typecast().wrapped())
				setStream(mStream + 1);

			return res;
		}

	protected:
		typename RNGT_::state_type mStream{};
	};

	template <typename RNGT_>
	class RNGRemapped
	{
	public:
		using result_type = typename RNGT_::result_type;

	private:
		RNGT_ *mRng = nullptr;
		std::uniform_int_distribution<result_type> mDistrib;

	public:

		constexpr RNGRemapped(RNGT_ &rng, std::uniform_int_distribution<result_type> distrib) :
			mRng{&rng}, mDistrib{distrib} {}

		constexpr auto operator() () noexcept
		{
			return mDistrib(*mRng);
		}

		[[nodiscard]] static constexpr auto min() noexcept { return RNGT_::min(); }
		[[nodiscard]] static constexpr auto max() noexcept { return RNGT_::max(); }

	};

	using IRNGInt = IRNG<RandomInt>;
}
