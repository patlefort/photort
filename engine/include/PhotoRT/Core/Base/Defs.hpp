/*
	PhotoRT

	Copyright (C) 2021 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#if (__cplusplus <= 201703L)
	#error "PhotoRT Engine requires at least a C++20 compiler"
#endif

#include <patlib/base.hpp>
#include <patlib/concepts.hpp>
#include <patlib/utility.hpp>
#include <patlib/memory.hpp>
#include <patlib/type.hpp>
#include <patlib/pointer.hpp>
#include <patlib/io.hpp>
#include <patlib/parallel/algorithm.hpp>
#include <patlib/exception.hpp>
#include <patlib/string.hpp>
#include <patlib/log.hpp>
#include <patlib/asserter.hpp>
#include <patlib/math.hpp>
#include <patlib/matrix.hpp>

#include <cstring>
#include <iostream>
#include <string>
#include <type_traits>
#include <cstdint>
#include <vector>
#include <limits>
#include <algorithm>
#include <assert.h>
#include <functional>
#include <iterator>
#include <tuple>
#include <typeinfo>
#include <cstdint>
#include <cstddef>
#include <utility>
#include <string_view>
#include <stdexcept>
#include <array>
#include <exception>
#include <span>
#include <numbers>
//#include <ranges>

#include <range/v3/all.hpp>

#include <boost/predef.h>

#ifdef PhotoRT_Core_SHARED
	#ifdef PhotoRT_Core_EXPORTS
		#define PHOTORT_API BOOST_SYMBOL_EXPORT
	#else
		#define PHOTORT_API BOOST_SYMBOL_IMPORT
	#endif
#else
	#define PHOTORT_API
#endif

#ifdef PRT_PROFILER

	#define PRT_PROFILE_START(profiler, tag) profiler.start(tag)
	#define PRT_PROFILE_STOP(profiler, tag) profiler.stop(tag)
	#define PRT_SCOPED_PROFILE(profiler, tag) ScopedProfilePhase PL_ANONYMOUS(scopedProfile)(profiler, tag)

#else

	#define PRT_PROFILE_START(profiler, tag)
	#define PRT_PROFILE_STOP(profiler, tag)
	#define PRT_SCOPED_PROFILE(profiler, tag)

#endif

namespace boost::hana {}
namespace ranges::views {}
namespace shared_access {}

namespace PhotoRT
{
	namespace pl = patlib;
	namespace sa = shared_access;
	using namespace pl::base_types;
	using namespace pl::string_view_op;
	using pl::string_view_op::operator+;
	using namespace pl::loop;
	using namespace pl::math_tags_op;
	using pl::size_c;
	using pl::size_v;
	using pl::type_c;
	using pl::type_v;
	using pl::bool_c;
	using pl::bool_v;

	constexpr bool useDouble =
		#ifdef PRT_DOUBLE
			true;
		#else
			false;
		#endif

	constexpr bool use64BitsIndex =
		#ifdef PRT_64BITS_INDEX
			true;
		#else
			false;
		#endif

	inline namespace base_types
	{
		using namespace pl::base_types;

		#ifdef PRT_DOUBLE
			using Real = double;
		#else
			using Real = float;
		#endif

		[[nodiscard]] constexpr auto r_(auto&& v) noexcept { return static_cast<Real>(PL_FWD(v)); }

		using MReal = long double;

		#ifdef PRT_64BITS_INDEX
			using PrimitiveIndex = u64;
			using PrimitiveIndexSigned = i64;
		#else
			using PrimitiveIndex = u32;
			using PrimitiveIndexSigned = i32;
		#endif

		using StatCounter = smint;
	}

	enum class EnumHemisphere : u8
	{
		Invalid = 0, North = 1, South = 2
	};

	[[nodiscard]] constexpr EnumHemisphere swapHemisphere(const EnumHemisphere hemi) noexcept
	{
		return hemi == EnumHemisphere::North ? EnumHemisphere::South :
			(hemi == EnumHemisphere::South ? EnumHemisphere::North :
				EnumHemisphere::Invalid);
	}

	[[nodiscard]] constexpr auto hemisphereIndex(EnumHemisphere hemi) noexcept
		{ return pl::underlying_value(hemi) - 1; }

	#define PRT_IMPORT_NS \
		namespace prt = PhotoRT; \
		namespace pl = patlib; \
		using namespace prt::base_types; \
		using namespace pl::loop; \
		using namespace pl::string_view_op; \
		using pl::string_view_op::operator+; \
		using pl::size_c; \
		using pl::size_v; \
		using pl::type_c; \
		using pl::type_v; \
		using pl::bool_c; \
		using pl::bool_v;

	using namespace std::chrono_literals;
	using namespace std::literals::string_literals;

	namespace hn = boost::hana;
	namespace rg = ranges;
	namespace rgv = rg::views;

	// Generic exception class for the library
	class PHOTORT_API Exception : public pl::Exception
	{
		using pl::Exception::Exception;

		[[nodiscard]] virtual const char *module() const noexcept override;
	};

	#ifdef PL_LIB_ASSERTS
		template <typename T_, typename... Asserters_>
		using Asserted = pl::asserter<T_, Asserters_...>;
	#else
		template <typename T_, typename... Asserters_>
		using Asserted = T_;
	#endif
} // namespace PhotoRT
