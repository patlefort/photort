/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Defs.hpp"

#include <patlib/math.hpp>
#include <patlib/simd/all.hpp>
#include <patlib/box.hpp>
#include <patlib/marked_optional.hpp>

namespace PhotoRT
{
	using namespace pl::simd::vector_op;

	#ifdef PRT_DOUBLE

		using Vec4D = pl::simd::best_gvec_t<double, size_c<4>>;

		#ifdef PL_LIB_VECTOR_DOUBLE_SIMD

			#define PRT_FEATURE_PACKED_VECTOR

			using PackedVec = pl::simd::best_vector_t<double, size_c<pl::simd::max_double_simd_size>>;

		#endif

	#else

		using Vec4D = pl::simd::best_gvec_t<float, size_c<4>>;

		#ifdef PL_LIB_VECTOR_FLOAT_SIMD

			#define PRT_FEATURE_PACKED_VECTOR

			using PackedVec = pl::simd::best_vector_t<float, size_c<pl::simd::max_float_simd_size>>;

		#endif

	#endif

	#ifdef PRT_FASTSQRT
		inline constexpr auto oprtnormalize = onormalizeapprox;
		inline constexpr auto oprtlength = olengthapprox;
	#else
		inline constexpr auto oprtnormalize = onormalize;
		inline constexpr auto oprtlength = olength;
	#endif

	using VecD = pl::simd::rebind_vector_tag<pl::simd::categories::geometric_t<pl::simd::categories::geometric_dir_t>, Vec4D>;
	using VecP = pl::simd::rebind_vector_tag<pl::simd::categories::geometric_t<pl::simd::categories::geometric_point_t>, Vec4D>;

	// Always basic vectors.
	using Point2Dr = pl::point2d<Real>;
	using Point2Di = pl::point2d<if32>;
	using Dir2Dr = pl::dir2d<Real>;
	using Dir2Di = pl::dir2d<if32>;

	using Point3Dr = pl::point3d<Real>;
	using Point3Di = pl::point3d<if32>;
	using Dir3Dr = pl::dir3d<Real>;
	using Dir3Di = pl::dir3d<if32>;

	// Vec type is based on pl::simd::best_gvec_t.
	using VecP3r = pl::simd::best_gvec_t<Real, size_c<3>, pl::simd::categories::geometric_point_t, 3>;
	using VecD3r = pl::simd::best_gvec_t<Real, size_c<3>, pl::simd::categories::geometric_dir_t, 3>;

	using VecP3i = pl::simd::best_gvec_t<if32, size_c<3>, pl::simd::categories::geometric_point_t, 3>;
	using VecD3i = pl::simd::best_gvec_t<if32, size_c<3>, pl::simd::categories::geometric_dir_t, 3>;

	using UV = VecP3r;
	using UVdir = VecD3r;

	using Matrix = pl::matrix4x4<Vec4D>;

	using Box = pl::box<VecP>;

	template <pl::simd::CGeometricVector T_>
	using ValidGVec = Asserted<T_, pl::tag_t<pl::valid::not_nan>, pl::tag_t<pl::valid::not_inf>>;

	template <pl::CNumberPack T_>
	using ValidWeight = Asserted<T_, pl::tag_t<pl::valid::not_nan>, pl::tag_t<pl::valid::not_inf>, pl::tag_t<pl::valid::not_negative>>;

	template <pl::CNumberPack T_>
	using ValidLength = Asserted<T_, pl::tag_t<pl::valid::not_nan>, pl::tag_t<pl::valid::not_negative>>;

	template <pl::CNumberPack T_>
	using ValidPDF = Asserted<T_, pl::tag_t<pl::valid::not_nan>, pl::tag_t<pl::valid::not_inf>, pl::tag_t<pl::valid::not_negative>, pl::tag_t<pl::valid::not_zero>>;

	template <pl::CNumberPack T_>
	using ValidRatio = Asserted<T_, pl::tag_t<pl::valid::not_nan>, pl::tag_t<pl::valid::not_inf>, pl::tag_t<pl::valid::not_negative>, pl::tag_t<pl::valid::not_above_one>>;

	namespace valid
	{
		struct ValidTag {};

		PL_MAKE_DERIVED_TAG(Weight, ValidTag)
		PL_MAKE_DERIVED_TAG(Length, ValidTag)
		PL_MAKE_DERIVED_TAG(PDF, ValidTag)
		PL_MAKE_DERIVED_TAG(Ratio, ValidTag)

		[[nodiscard]] constexpr bool tag_invoke(pl::tag_t<pl::is_valid>, const pl::CNumberPack auto &v, Weight_t) noexcept
			{ return pl::mask_none(pl::plisnan(v) | pl::plisinf(v) | (v < 0)); }

		[[nodiscard]] constexpr bool tag_invoke(pl::tag_t<pl::is_valid>, const pl::CNumberPack auto &v, Length_t) noexcept
			{ return pl::mask_none(pl::plisnan(v) | (v < 0)); }

		[[nodiscard]] constexpr bool tag_invoke(pl::tag_t<pl::is_valid>, const pl::CNumberPack auto &v, PDF_t) noexcept
			{ return pl::mask_none(pl::plisnan(v) | pl::plisinf(v) | (v <= 0)); }

		[[nodiscard]] constexpr bool tag_invoke(pl::tag_t<pl::is_valid>, const pl::CNumberPack auto &v, Ratio_t) noexcept
			{ return pl::mask_none(pl::plisnan(v) | pl::plisinf(v) | (v < 0) | (v > 1)); }
	}

	namespace constants
	{
		template <pl::CBitField T_>
		inline constexpr auto noIndex = pl::constants::all_ones<T_>;

		template <pl::CNumberPackFloat T_>
		inline constexpr auto epsilon = std::numeric_limits<T_>::epsilon() * 100;

		template <typename... Ts_>
			requires (!pl::simd::vector<Ts_...>::ConstExpr)
		inline auto epsilon<pl::simd::vector<Ts_...>> = std::numeric_limits<pl::simd::vector<Ts_...>>::epsilon() * 100;

		template <typename... Ts_>
			requires pl::simd::vector<Ts_...>::ConstExpr
		inline constexpr auto epsilon<pl::simd::vector<Ts_...>> = std::numeric_limits<pl::simd::vector<Ts_...>>::epsilon() * 100;
	} // namespace constants

	#ifdef PRT_FASTSQRT
		inline constexpr auto oprtsqrt = osqrtapprox;
	#else
		inline constexpr auto oprtsqrt = osqrt;
	#endif

	#ifdef PRT_FASTRECIPROCAL
		inline constexpr auto oprtreciprocal = oreciprocalapprox;
	#else
		inline constexpr auto oprtreciprocal = oreciprocal;
	#endif

	#ifdef PRT_FASTEXP2
		inline constexpr auto oprtexp2 = oexp2approx;
	#else
		inline constexpr auto oprtexp2 = oexp2;
	#endif

	#ifdef PRT_FASTLOG2
		inline constexpr auto oprtlog2 = olog2approx;
	#else
		inline constexpr auto oprtlog2 = olog2;
	#endif

	template <typename T_>
	[[nodiscard]] constexpr auto prtsqrt(T_&& v) noexcept
		{ return PL_FWD(v) <oprtsqrt; }

	template <typename T_>
	[[nodiscard]] constexpr auto prtreciprocal(T_&& v) noexcept
		{ return PL_FWD(v) <oprtreciprocal; }

	template <typename T_>
	[[nodiscard]] constexpr auto prtexp2(T_&& v) noexcept
		{ return PL_FWD(v) <oprtexp2; }

	template <typename T_>
	[[nodiscard]] constexpr auto prtlog2(T_&& v) noexcept
		{ return PL_FWD(v) <oprtlog2; }


	namespace impl_optional
	{
		template <pl::CNumberFloat T_>
		struct optional_pdf_policy : public pl::marked_optional_default_storage<T_>
		{
			using type = T_;

			PL_DEFAULT_COPYMOVE(optional_pdf_policy)
			using pl::marked_optional_default_storage<type>::marked_optional_default_storage;
			using pl::marked_optional_default_storage<type>::value;

			constexpr optional_pdf_policy() noexcept : pl::marked_optional_default_storage<type>(std::in_place, T_{}) {}

			template <typename NewT_>
			using rebind = optional_pdf_policy<NewT_>;

			constexpr void set(T_ v) noexcept { value() = v; }

			constexpr void mark() noexcept
				{ value() = T_{}; }

			[[nodiscard]] constexpr bool is_marked() const noexcept
				{ return value() == T_{}; }
		};
	}

	template <pl::CNumberFloat T_>
	using OptionalPDF = pl::marked_optional<T_, impl_optional::optional_pdf_policy<T_>>;


	template <pl::simd::CNeutralOrPointGeometricVector VT1_, pl::simd::CNeutralOrDirGeometricVector VT2_>
	[[nodiscard]] constexpr auto safeIncrement(const VT1_ point, const VT2_ dir, const typename VT2_::value_type minimalDist) noexcept
	{
		const VT2_ inc { pl::plmax(minimalDist * (typename VT1_::value_type)PRT_MINIMAL_SAFE_INC_FACTOR, constants::epsilon<pl::underlying_type_t<VT1_>>) };

		return point +
			pl::select(
				VT2_{},
				pl::select(inc, -inc, dir > 0),
				!dir
			);
	}

	template <typename VT_>
	[[nodiscard]] constexpr auto safeIncrement(const pl::box<VT_> &b) noexcept
	{
		const auto minSafeInc = b.minimal_safe_increment();

		return pl::box<VT_>{
			safeIncrement(b.v1, pl::simd::rebind_to_dir<VT_>{-1}, minSafeInc),
			safeIncrement(b.v2, pl::simd::rebind_to_dir<VT_>{1}, minSafeInc)
		};
	}

	template <pl::CNumberPackFloat T_>
	[[nodiscard]] constexpr auto rotate2Dvector(const pl::point2d<T_> p, const T_ angle) noexcept
	{
		const auto [sinphi, cosphi] = pl::sincos(angle);

		return pl::point2d<T_>{
			cosphi * p[size_v<0>] - sinphi * p[size_v<1>],
			sinphi * p[size_v<0>] + cosphi * p[size_v<1>]
		};
	}


	class IAABox
	{
	public:
		virtual ~IAABox() = default;

		[[nodiscard]] virtual Box getAABox() const noexcept = 0;
	};


	template <pl::CAlignedType VT_>
	struct VecPackerAligned
	{
		using VT = VT_;

		template <pl::simd::CGeometricVector Vec_>
		[[nodiscard]] static constexpr auto pack(const Vec_ p) noexcept
		{
			const auto &vs = pl::simd::vector_access(p);
			std::array<VT_, Vec_::category::NbAxis> res;

			for(const auto i : mir(Vec_::category::NbAxis))
				for(auto &v : res[i]())
					v = vs[i];

			return res;
		}

		template <pl::simd::CGeometricVector Vec_>
		static constexpr void pack(const Vec_ p, std::array<VT_, Vec_::category::NbAxis> &v, const szt index) noexcept
		{
			const auto &vs = pl::simd::vector_access(p);

			for(const auto i : mir(Vec_::category::NbAxis))
				v[i]()[index] = vs[i];
		}

		template <pl::simd::CGeometricVector Vec_>
		[[nodiscard]] static constexpr auto unpack(const std::array<VT_, Vec_::category::NbAxis> &v, const szt index) noexcept
		{
			return pl::construct_from_index_sequence<Vec_>(size_v<Vec_::NbValues>, [&]<szt I>(size_c<I> i){
				if constexpr(I < Vec_::category::NbAxis)
					return v[i]()[index];
				else if constexpr(I == Vec_::category::NbAxis)
					return pl::simd::get_gvec_last_value<typename Vec_::category::type, typename Vec_::value_type>();
				else
					return typename Vec_::value_type{};
			});
		}
	};

	template <pl::simd::CVector VT_>
	struct VecPacker
	{
		using VT = VT_;

		template <pl::simd::CGeometricVector Vec_>
		[[nodiscard]] static constexpr auto pack(const Vec_ p) noexcept
		{
			return static_cast<pl::simd::basic_geometric_vector<VT_, size_c<Vec_::category::NbAxis>, typename Vec_::category::type, Vec_::category::NbAxis>>(p);
		}

		template <pl::simd::CGeometricVector Vec_>
		static constexpr void pack(const Vec_ p, pl::simd::basic_geometric_vector<VT_, size_c<Vec_::category::NbAxis>, typename Vec_::category::type, Vec_::category::NbAxis> &v, const szt index) noexcept
		{
			const auto &pac = pl::simd::vector_access(p);
			for(const auto i : mir(Vec_::category::NbAxis))
				v[i].set(pac[i], index);
		}

		template <pl::simd::CGeometricVector Vec_>
		[[nodiscard]] static constexpr auto unpack(const pl::simd::basic_geometric_vector<VT_, size_c<Vec_::category::NbAxis>, typename Vec_::category::type, Vec_::category::NbAxis> &v, const szt index) noexcept
		{
			return pl::construct_from_index_sequence<Vec_>(size_v<Vec_::NbValues>, [&]<szt I>(size_c<I> i){
				if constexpr(I < Vec_::category::NbAxis)
					return v[i][index];
				else if constexpr(I == Vec_::category::NbAxis)
					return pl::simd::get_gvec_last_value<typename Vec_::category::type, typename Vec_::value_type>();
				else
					return typename Vec_::value_type{};
			});
		}
	};


	[[nodiscard]] constexpr auto uniformConePdf(pl::CNumberPackFloat auto cosThetaMax) noexcept
	{
		return (std::numbers::pi_v<decltype(cosThetaMax)> * 2 * (1 - cosThetaMax)) /oprtreciprocal;
	}

	template <pl::CNumberPackFloat T_>
	[[nodiscard]] constexpr pl::dir3d<T_> uniformSampleSphere(pl::values<T_, 2> p) noexcept
	{
		const auto z2 = 1 - 2 * p[1];
		const auto r = T_{} <omax> (1 - z2 /opow2) <oprtsqrt;
		const auto phi = std::numbers::pi_v<T_> * 2 * p[0];
		const auto [sinphi, cosphi] = pl::sincos(phi);

		return {r * cosphi, r * sinphi, z2};
	}

	template <pl::simd::CNeutralOrDirGeometricVector VT_>
	[[nodiscard]] constexpr auto sphereThetaPhi(VT_ v) noexcept
		requires (VT_::category::NbAxis == 3 && std::floating_point<typename VT_::value_type>)
	{
		using T = typename VT_::value_type;

		pl::point2d<T> res;

		res.set(std::acos(pl::plclamp<T>(v[size_v<2>], -1, 1)), size_v<1>);

		if(pl::simd::mask_nb(size_v<2>, !v).all())
			res.set(0, size_v<0>);
		else
		{
			const T p = std::atan2(pl::plclamp<T>(v[size_v<1>], -1, 1), pl::plclamp<T>(v[size_v<0>], -1, 1));
			res.set((p < 0) ? p + std::numbers::pi_v<T> * 2 : p, size_v<0>);
		}

		return pl::assert_valid(res);
	}

	template <pl::CNumberPackFloat T_>
	[[nodiscard]] constexpr pl::dir3d<T_> sphericalDirection(pl::point2d<T_> p) noexcept
	{
		const auto phi = p[0] * std::numbers::pi_v<T_> * 2;
		const auto theta = p[1] * std::numbers::pi_v<T_>;
		const auto sinTheta = theta <osin;
		const auto [sinphi, cosphi] = pl::sincos(phi);

		return {cosphi * sinTheta, sinphi * sinTheta, pl::plcos(theta)};
	}

	template <pl::simd::CGeometricVector VT_>
	[[nodiscard]] constexpr auto sphereUV(VT_ v) noexcept
		requires (VT_::NbValues >= 3)
	{
		return pl::assert_valid(
			sphereThetaPhi(v) *
				pl::point2d<typename VT_::value_type>{
					1 / (std::numbers::pi_v<pl::underlying_type_t<VT_>> * 2),
					std::numbers::inv_pi_v<pl::underlying_type_t<VT_>>
				}
		);
	}

	template <pl::CNumberPackFloat T_>
	[[nodiscard]] constexpr auto uniformSampleTriangle(pl::values<T_, 2> p) noexcept
	{
		p[0] = p[0] <oprtsqrt;

		return pl::point2d<T_>{1 - p[0], p[1] * p[0]};
	}

	template <std::floating_point T_>
	[[nodiscard]] constexpr std::tuple<T_, T_, T_> uniformSampleDisk(pl::values<T_, 2> p) noexcept
	{
		const auto offset = pl::tag::two * p - pl::tag::one;

		if(!offset[0] && !offset[1])
			return {};

		T_ theta, r;
		if(offset[0] /oabs > offset[1] /oabs)
		{
			r = offset[0];
			theta = (std::numbers::pi_v<pl::underlying_type_t<T_>> * 0.25) * (offset[1] / offset[0]);
		}
		else
		{
			r = offset[1];
			theta = (std::numbers::pi_v<pl::underlying_type_t<T_>> * 0.5) - (std::numbers::pi_v<pl::underlying_type_t<T_>> * 0.25) * (offset[0] / offset[1]);
		}

		const auto [sintheta, costheta] = pl::sincos(theta);

		pl::assert_valid(r);
		pl::assert_valid(costheta);
		pl::assert_valid(sintheta);

		return {r, costheta, sintheta};
	}

	template <std::floating_point T_>
	[[nodiscard]] constexpr auto uniformSampleDiskPoint(pl::values<T_, 2> p) noexcept
	{
		const auto [r, costheta, sintheta] = uniformSampleDisk(p);

		return pl::assert_valid( pl::point2d<T_>{r * costheta, r * sintheta} );
	}

	template <pl::simd::CNeutralOrDirGeometricVector VT_>
	[[nodiscard]] constexpr std::pair<VT_, VT_> lobeDirs(VT_ n) noexcept
	{
		const auto y = n <ofind_orthogonal <oprtnormalize;

		return {
			pl::assert_valid(n <ocross> y <oprtnormalize),
			pl::assert_valid(y)
		};
	}

	// https://www.semanticscholar.org/paper/Building-an-Orthonormal-Basis-from-a-3D-Unit-Frisvad/36dd42635aa896245b039c1334f30b5ab5b4299c/pdf
	/*template <typename VT_>
	void lobeDirs(const VT_ &n, VT_ &x, VT_ &y) noexcept
	{
		const auto &nv = pl::simd::vector_access(n);

		if(nv[2] < -1 + constants::epsilon<pl::underlying_type_t<VT_>> * 1000)
		{
			x = {0, -1, 0, 0};
			y = {-1, 0, 0, 0};
			return;
		}

		const auto a = 1 / (nv[2] + 1);
		const auto b = -nv[0] * nv[1] * a;
		x = {1 - nv[0] /opow2 * a, b, -nv[0], 0};
		y = {b, 1 - nv[1] /opow2 * a, -nv[1], 0};

		pl::assert_valid(y);
		pl::assert_valid(x);
	}*/

	template <pl::simd::CNeutralOrDirGeometricVector VT_>
	[[nodiscard]] constexpr std::pair<VT_, VT_> lobeDirsAnisotropic(VT_ n) noexcept
		requires (VT_::NbValues >= 3)
	{
		VT_ x, y;

		if(n[size_v<0>] /oabs < constants::epsilon<pl::underlying_type_t<VT_>> && n[size_v<2>] /oabs < constants::epsilon<pl::underlying_type_t<VT_>>)
			x = VT_{0, 0, 1}.as_dir();
		else
			x = VT_{n[size_v<2>], 0, -(n[size_v<0>])}.as_dir() /oprtnormalize;

		y = x <ocross> n <oprtnormalize;

		if(n[size_v<1>] < 0)
			y = -y;

		return {pl::assert_valid(x), pl::assert_valid(y)};
	}

	template <pl::simd::CNeutralOrDirGeometricVector VT_>
	[[nodiscard]] constexpr auto checkAndInvertNormal(VT_ odir, VT_ n) noexcept
	{
		const auto dp = std::invoke([&]
			{
				#ifdef PL_LIB_VECTOR_SIMD
					if constexpr(pl::simd::CSimdVector<VT_>)
						return n.dot_m(pl::simd::masked{size_v<0xFF>, odir});
					else
				#endif
						return VT_{n <odot> odir};
			});

		return pl::assert_valid( pl::select(-n, n, dp > 0) );
	}

	template <pl::simd::CNeutralOrDirGeometricVector VT_>
	[[nodiscard]] constexpr auto checkAndInvertDirReflect(VT_ n, VT_ idir) noexcept
	{
		const auto dp = std::invoke([&]
			{
				#ifdef PL_LIB_VECTOR_SIMD
					if constexpr(pl::simd::CSimdVector<VT_>)
						return n.dot_m(pl::simd::masked{size_v<0xFF>, idir});
					else
				#endif
						return VT_{n <odot> idir};
			});

		return pl::assert_valid( idir - (n * pl::select(dp, dp < 0) * pl::tag::two) );
	}

	template <pl::simd::CNeutralOrDirGeometricVector VT_>
	[[nodiscard]] constexpr auto checkAndInvertDirTrans(VT_ n, VT_ idir) noexcept
	{
		const auto dp = std::invoke([&]
			{
				#ifdef PL_LIB_VECTOR_SIMD
					if constexpr(pl::simd::CSimdVector<VT_>)
						return n.dot_m(pl::simd::masked{size_v<0xFF>, idir});
					else
				#endif
						return VT_{n <odot> idir};
			});

		return pl::assert_valid( idir - (n * pl::select(dp, dp > 0) * pl::tag::two) );
	}

	template <pl::CNumberPackFloat T_>
	[[nodiscard]] constexpr pl::dir3d<T_> uniformSampleLobe(pl::values<T_, 2> p) noexcept
	{
		const auto sintheta = p[0];
		const auto phi = p[1] * (std::numbers::pi_v<T_> * 2);
		const auto costheta = (1 - sintheta /opow2) <oprtsqrt;

		if(costheta <= 0)
			return {};

		const auto [sinphi, cosphi] = pl::sincos(phi);

		return {cosphi * sintheta, sinphi * sintheta, costheta};
	}

	[[nodiscard]] constexpr auto geometricTermPos(const pl::CNumberPackFloat auto dist) noexcept
	{
		return dist /opow2;
	}

	template <pl::simd::CNeutralOrPointGeometricVector VT_>
	[[nodiscard]] constexpr auto geometricTermPos(VT_ p1, VT_ p2) noexcept
	{
		return (p2 - p1) <osdot;
	}

	template <pl::simd::CNeutralOrPointGeometricVector VT_>
	[[nodiscard]] constexpr auto geometricTermPosEpsilon(VT_ p1, VT_ p2) noexcept
	{
		return geometricTermPos(p1, p2) + constants::epsilon<pl::underlying_type_t<VT_>>;
	}

	template <pl::CNumberPackFloat T_>
	[[nodiscard]] constexpr auto pdfA(const T_ distSqr, const T_ cosTheta) noexcept
	{
		return cosTheta * (distSqr <oprtreciprocal);
	}

	template <pl::CNumberPackFloat T_>
	[[nodiscard]] constexpr auto pdfW(const T_ distSqr, const T_ cosTheta) noexcept
	{
		return distSqr * (cosTheta <oprtreciprocal);
	}

	template <pl::CNumberPackFloat T_>
	[[nodiscard]] constexpr auto pdfAtoW(const T_ pdf, const T_ distSqr, const T_ cosTheta) noexcept
	{
		return pdf * pdfW(distSqr, cosTheta);
	}

	template <pl::CNumberPackFloat T_>
	[[nodiscard]] constexpr auto pdfWtoA(const T_ pdf, const T_ distSqr, const T_ cosTheta) noexcept
	{
		return pdf * pdfA(distSqr, cosTheta);
	}

	template <pl::CNumberPackFloat T_>
	[[nodiscard]] constexpr auto pdfWtoAzero(const T_ pdf, const T_ distSqr, const T_ cosTheta) noexcept
	{
		return !distSqr ? pdf : pdfWtoA(pdf, distSqr, cosTheta);
	}

	template <pl::simd::CNeutralOrDirGeometricVector T_>
	[[nodiscard]] constexpr auto reflect(T_ odir, T_ normal) noexcept
	{
		return odir - normal * (odir <odot> normal) * pl::tag::two;
	}

	template <pl::simd::CNeutralOrDirGeometricVector T_>
	[[nodiscard]] constexpr auto dirReciprocal(T_ dir) noexcept
	{
		auto r = dir <oprtreciprocal;

		if constexpr(T_::category::NbAxis < T_::NbValues)
		{
			pl::simd::vector_modify(r, [](auto &v){
				for(const auto i : mir(T_::category::NbAxis, T_::NbValues))
					v[i] = {};
			});
		}

		return r;
	}

	template <pl::simd::CNeutralOrDirGeometricVector T_, pl::CMatrix4x4Transform<T_> MT_>
	[[nodiscard]] constexpr auto dirSafeTransform(T_ dir, const MT_ &matrix) noexcept
	{
		return (matrix * dir) <oprtnormalize;
	}

	// Coordinate systems

	namespace tag
	{
		struct PositionTag {};

		template <typename T_>
		concept CPositionTag = std::derived_from<T_, PositionTag>;

		PL_MAKE_DERIVED_TAG(cartesian, PositionTag)
		PL_MAKE_DERIVED_TAG(polar, PositionTag)
	} // namespace tag

	namespace ImplCoord
	{
		template <pl::simd::CGeometricVector VTInto_, tag::CPositionTag Type_>
		[[nodiscard]] constexpr auto prtadl_coordinateMap(Type_, Type_, const auto &v, type_c<VTInto_>) noexcept
			{ return static_cast<VTInto_>(v); }

		template <pl::simd::CGeometricVector VTInto_>
		[[nodiscard]] constexpr auto prtadl_coordinateMap(tag::cartesian_t, tag::polar_t, const auto &v, type_c<VTInto_>) noexcept
			{ return static_cast<VTInto_>(sphereUV(v)); }

		template <pl::simd::CGeometricVector VTInto_>
		[[nodiscard]] constexpr auto prtadl_coordinateMap(tag::polar_t, tag::cartesian_t, const auto &v, type_c<VTInto_>) noexcept
			{ return static_cast<VTInto_>(sphericalDirection(v)); }
	} // namespace ImplCoord

	template <tag::CPositionTag Type_, pl::simd::CGeometricVector VT_>
	class CoordinatePosition
	{
		VT_ v{};

	public:
		using VertexType = VT_;
		using value_type = Type_;

		constexpr CoordinatePosition() = default;
		constexpr CoordinatePosition(VT_ v_) noexcept : v{v_} {}
		constexpr CoordinatePosition(Type_, VT_ v_) noexcept : v{v_} {}
		constexpr CoordinatePosition(const CoordinatePosition &) = default;

		[[nodiscard]] constexpr const auto &operator* () const noexcept { return v; }
		[[nodiscard]] constexpr auto &operator* () noexcept { return v; }

		[[nodiscard]] constexpr const auto *operator-> () const noexcept { return &v; }
		[[nodiscard]] constexpr auto *operator-> () noexcept { return &v; }

		template <typename T_, typename OVT_>
		[[nodiscard]] explicit constexpr operator CoordinatePosition<T_, OVT_>() const noexcept
		{
			using ImplCoord::prtadl_coordinateMap;
			return CoordinatePosition<T_, OVT_>{ prtadl_coordinateMap(Type_{}, T_{}, v, type_v<OVT_>) };
		}

		template <tag::CPositionTag IntoType_, pl::simd::CGeometricVector OVT_ = VertexType>
		[[nodiscard]] constexpr auto to(IntoType_ = {}, type_c<OVT_> = {}) const noexcept
			{ return static_cast<CoordinatePosition<IntoType_, OVT_>>(*this); }
	};


	// Sampling lobe

	template <typename T_>
	struct anisotropic_p : public pl::param<T_> { using pl::param<T_>::param; };
		//template <typename T_> anisotropic_p(T_) -> anisotropic_p<pl::remove_cvref_t<T_>>;

	template <pl::simd::CNeutralOrDirGeometricVector VT_ = VecD>
	class Lobe
	{
	public:
		using R_ = typename VT_::value_type;

		constexpr Lobe() = default;
		constexpr Lobe(VT_ n) noexcept
		{
			calcLobeDirs(n);
		}
		constexpr Lobe(anisotropic_p<VT_> n) noexcept
		{
			calcLobeDirsAnisotropic(n);
		}
		constexpr Lobe(VT_ lx, VT_ ly, VT_ n) noexcept
			: lobeX{lx}, lobeY{ly}, normal{n} {}

		VT_
			lobeX{pl::tag::identity},
			lobeY{pl::tag::identity},
			normal{pl::tag::identity};

		template <pl::simd::CNeutralOrDirGeometricVector OVT_>
		[[nodiscard]] constexpr explicit operator Lobe<OVT_>() const noexcept
			{ return Lobe<OVT_>{static_cast<OVT_>(lobeX), static_cast<OVT_>(lobeY), static_cast<OVT_>(normal)}; }

		constexpr void reverse() noexcept
		{
			lobeX = -lobeX;
			lobeY = -lobeY;
			normal = -normal;
		}

		constexpr void calcLobeDirs(VT_ n) noexcept
		{
			std::tie(lobeX, lobeY) = lobeDirs(n);
			normal = n;
		}

		constexpr void calcLobeDirsAnisotropic(VT_ n) noexcept
		{
			std::tie(lobeX, lobeY) = lobeDirsAnisotropic(n);
			normal = n;
		}

		constexpr void rotateOnNormal(const R_ a) noexcept
		{
			const auto [sinphi, cosphi] = pl::sincos(a);
			rotateOnNormal(cosphi, sinphi);
		}

		constexpr void rotateOnNormal(const R_ cosa, const R_ sina) noexcept
		{
			const auto lobeXcopy = lobeX;
			lobeX = pl::assert_valid(lobeXcopy * cosa + lobeY * sina);
			lobeY = pl::assert_valid(lobeY * cosa - lobeXcopy * sina);
		}

		[[nodiscard]] constexpr auto sphericalDirection(pl::point2d<R_> p) const noexcept
		{
			return toWorld(CoordinatePosition{tag::polar, p}.to(tag::cartesian, type_v<VT_>)->as_dir());
		}

		[[nodiscard]] constexpr auto sphericalDirection(R_ cosTheta, R_ sinTheta, R_ phi) const noexcept
		{
			const auto [sinphi, cosphi] = pl::sincos(phi);

			return pl::assert_valid(
				lobeX * (cosphi * sinTheta) +
				lobeY * (sinphi * sinTheta) +
				normal * cosTheta
			);
		}

		[[nodiscard]] constexpr auto toLocal(const VT_ v) const noexcept
		{
			#ifdef PL_LIB_VECTOR_SIMD
				if constexpr(pl::simd::CSimdVector<VT_>)
					return pl::assert_valid( (v.dot_m(pl::simd::masked{size_v<0xF1>, lobeX}) + v.dot_m(pl::simd::masked{size_v<0xF2>, lobeY}) + v.dot_m(pl::simd::masked{size_v<0xF4>, normal})) <oblend<0b1000>> v );
				else
			#endif
					return pl::assert_valid( VT_{v <odot> lobeX, v <odot> lobeY, v <odot> normal, v[size_v<3>]} );
		}

		[[nodiscard]] constexpr auto toWorld(const VT_ v) const noexcept
		{
			#ifdef PL_LIB_VECTOR_SIMD
				if constexpr(pl::simd::CSimdVector<VT_>)
					return pl::assert_valid( (v /osshuffle<0, 0, 0, 3> * lobeX + v /osshuffle<1, 1, 1, 3> * lobeY + v /osshuffle<2, 2, 2, 3> * normal) <oblend<0b1000>> v );
				else
			#endif
					return pl::assert_valid( (v[size_v<0>] * lobeX + v[size_v<1>] * lobeY + v[size_v<2>] * normal).set(v[size_v<3>], size_v<3>) );
		}

		[[nodiscard]] constexpr auto toWorld(R_ x, R_ y, R_ z) const noexcept
		{
			return pl::assert_valid( lobeX * x + lobeY * y + normal * z );
		}

		[[nodiscard]] constexpr auto sphereUV(VT_ v) const noexcept
		{
			return pl::assert_valid( *CoordinatePosition{tag::cartesian, toLocal(v)}.to(tag::polar, type_v<pl::point2d<R_>>) );
		}

		[[nodiscard]] constexpr std::pair<R_, VT_> sampleUniformDir(pl::values<R_, 2> p) const noexcept
		{
			const auto sintheta = p[0];
			const auto phi = p[1] * (std::numbers::pi_v<R_> * 2);
			const auto costheta = (1 - sintheta /opow2) <oprtsqrt;

			if(costheta <= 0)
				return {};

			return {costheta, sphericalDirection(costheta, sintheta, phi)};
		}

		[[nodiscard]] constexpr auto sampleCosineDir(pl::values<R_, 2> p) const noexcept
		{
			const auto dsp = uniformSampleDiskPoint(p);
			const auto z = (pl::plmax<R_>(0, 1 - dsp /osdot)) <oprtsqrt;

			return std::make_pair(z, toWorld(dsp[0], dsp[1], z));
		}

		[[nodiscard]] constexpr auto sampleConeDir(pl::values<R_, 2> p, R_ costhetamax, R_ sinthetamax2) const noexcept
		{
			auto costheta = (1 - p[0]) + p[0] * costhetamax;
			auto sintheta2 = pl::plmax<R_>(0, 1 - costheta /opow2);

			if(sinthetamax2 < 0.00068523) // sin(1.5 deg)^2
			{
				sintheta2 = sinthetamax2 * p[0];
				costheta = (1 - sintheta2) <oprtsqrt;
			}

			const auto sintheta = sintheta2 <oprtsqrt;
			const auto phi = p[1] * (std::numbers::pi_v<R_> * 2);

			return sphericalDirection(costheta, sintheta, phi);
		}

		[[nodiscard]] constexpr auto reflect(VT_ odir) const noexcept
		{
			return PhotoRT::reflect(odir, normal);
		}

		[[nodiscard]] constexpr EnumHemisphere getHemisphere(VT_ idir) const noexcept
		{
			return (idir <odot> normal) > 0 ? EnumHemisphere::North : EnumHemisphere::South;
		}

		friend auto &operator<<(std::ostream &os, const Lobe &l)
		{
			return (os << pl::io::range{pl::io::mode_select, '\n', std::forward_as_tuple(l.normal, l.lobeX, l.lobeY)});
		}

		friend auto &operator>>(std::istream &is, Lobe &l)
		{
			return (is >> pl::io::range{pl::io::mode_select, '\n', std::forward_as_tuple(l.normal, l.lobeX, l.lobeY)});
		}
	};
}
