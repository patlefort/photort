/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Defs.hpp"

namespace PhotoRT
{
	using Color32 = pl::simd::best_color_t<float, size_c<4>>;
	using Color64 = pl::simd::best_color_t<double, size_c<4>>;

	using Color3_32 = pl::simd::best_color_t<float, size_c<3>>;
	using Color3_64 = pl::simd::best_color_t<double, size_c<3>>;

	#ifdef PL_LIB_VECTOR_DOUBLE_SIMD
		using PackedColorValue64 = pl::simd::best_color_t<double, size_c<pl::simd::max_double_simd_size>>;
	#endif

	#ifdef PL_LIB_VECTOR_FLOAT_SIMD
		using PackedColorValue32 = pl::simd::best_color_t<float, size_c<pl::simd::max_float_simd_size>>;
	#endif

	#ifdef PRT_DOUBLE
		using Color = Color64;

		#ifdef PL_LIB_VECTOR_DOUBLE_SIMD
			#define PRT_FEATURE_PACKED_COLOR

			using PackedColorValue = PackedColorValue64;
			using PackedColor = pl::simd::best_color_t<PackedColorValue, size_c<4>>;
		#endif

	#else
		using Color = Color32;

		#ifdef PL_LIB_VECTOR_FLOAT_SIMD
			#define PRT_FEATURE_PACKED_COLOR

			using PackedColorValue = PackedColorValue32;
			using PackedColor = pl::simd::best_color_t<PackedColorValue, size_c<4>>;
		#endif

	#endif

	using CReal = Color::value_type;
	using ColorInt24 = pl::simd::best_color_t<u8, size_c<3>>;


	namespace constants
	{
		template <pl::simd::CColorVector T_>
		inline auto black = T_{};
		template <pl::simd::CColorVector T_>
		inline auto white = T_{pl::tag::one};

		template <pl::simd::CColorVector T_>
		inline auto red = T_{1, 0, 0};
		template <pl::simd::CColorVector T_>
		inline auto green = T_{0, 1, 0};
		template <pl::simd::CColorVector T_>
		inline auto blue = T_{0, 0, 1};

	} // namespace constants

	template <typename T_>
	using ValidColor = ValidWeight<T_>;

	inline auto Black = constants::black<Color>;
	inline auto White = constants::white<Color>;

	[[nodiscard]] constexpr bool isAlmostBlack(pl::simd::CColorVector auto c) noexcept
		{ return pl::simd::mask_color( c <= static_cast<typename decltype(c)::value_type>(1e-7) ).all(); }

	[[nodiscard]] constexpr bool isBlack(pl::simd::CColorVector auto c) noexcept
		{ return pl::simd::mask_color( c <= constants::black<decltype(c)> ).all(); }

	[[nodiscard]] constexpr bool isAlmostWhite(pl::simd::CColorVector auto c) noexcept
		{ return pl::simd::mask_color( c >= constants::white<decltype(c)> - static_cast<typename decltype(c)::value_type>(1e-7) ).all(); }

	[[nodiscard]] constexpr bool isWhite(pl::simd::CColorVector auto c) noexcept
		{ return pl::simd::mask_color( c >= constants::white<decltype(c)> ).all(); }

	[[nodiscard]] constexpr auto clampColor(pl::simd::CColorVector auto c) noexcept
		{ return c <omin> constants::white<decltype(c)> <omax> constants::black<decltype(c)>; }

	template <pl::simd::CColorVector CT_>
	[[nodiscard]] constexpr CT_ preserveAlpha(CT_ sc, CT_ tc) noexcept
	{
		if constexpr(CT_::NbValues > 3)
			return tc <oblend<0b1000>> sc;
		else
			return tc;
	}

	[[nodiscard]] constexpr auto gammaCorrectColor(const pl::simd::CColorVector auto sourceColor, const auto fromGamma, const auto toGammaInv) noexcept
		{ return preserveAlpha(sourceColor, sourceColor <opow> fromGamma <opow> toGammaInv); }

	[[nodiscard]] constexpr auto invertColor(pl::simd::CColorVector auto c) noexcept
		{ return preserveAlpha(c, constants::white<decltype(c)> - c); }

	template <pl::simd::CColorVector CT_>
	[[nodiscard]] auto averageAndAdjustSaturationColors(const std::span<const CT_> colors) noexcept
	{
		return rg::accumulate(colors, CT_{}, rg::plus{}, [](const auto &c){
				const auto maximum = c.max3();
				return preserveAlpha(c, c * (maximum > 1 ? 1 / maximum : 1));
			}) * (1 / static_cast<typename CT_::value_type>(colors.size()));
	}

	[[nodiscard]] constexpr auto averageColorComponents(const pl::simd::CColorVector auto c) noexcept
	{
		return (c[size_v<0>] + c[size_v<1>] + c[size_v<2>]) * (1.0 / 3.0);
	}

	template <pl::simd::CColorVector CT_>
	[[nodiscard]] constexpr auto averageColors(const std::span<const CT_> colors) noexcept
		{ return rg::accumulate(colors, CT_{}) * (1 / static_cast<typename CT_::value_type>(colors.size())); }


	[[nodiscard]] constexpr auto fillAlphaColor(const pl::simd::CColorVector auto c) noexcept
	{
		return c <osshuffle<3, 3, 3, 3>;
	}

	template <pl::simd::CColorVector CT_>
	[[nodiscard]] constexpr auto blendAlpha(const CT_ source, const CT_ dest) noexcept
	{
		const auto w = constants::white<CT_>;
		return w - ((w - dest) * (w - source));
	}

	template <pl::simd::CColorVector CT_>
	[[nodiscard]] constexpr auto blendColorsAlpha(const CT_ source, const CT_ dest) noexcept
	{
		const auto w = constants::white<CT_>;

		const auto alpha = blendAlpha(source, dest);
		const auto alphaSource = fillAlphaColor(source);

		return (dest * (w - alphaSource) + source * alphaSource) <oblend<0b1000>> alpha;
	}

	template <pl::simd::CColorVector CT_>
	[[nodiscard]] constexpr auto blend2Colors(const CT_ cx1, const CT_ cx2, const typename CT_::value_type x) noexcept
	{
		return pl::pllerp(cx1, cx2, CT_{x});
	}

	template <pl::simd::CColorVector CT_>
	[[nodiscard]] constexpr auto blend4Colors(
		const CT_ cx1y1, const CT_ cx2y1, const CT_ cx1y2, const CT_ cx2y2,
		const typename CT_::value_type x, const typename CT_::value_type y) noexcept
	{
		const auto cx = CT_{x};
		return pl::pllerp( pl::pllerp(cx1y1, cx2y1, cx), pl::pllerp(cx1y2, cx2y2, cx), CT_{y} );
	}

	template <pl::simd::CColorVector CT_>
	[[nodiscard]] constexpr auto reduceColor(const CT_ c) noexcept
	{
		const auto maximum = c.max3();

		return c * pl::select(maximum <oprtreciprocal, pl::constants::one<std::decay_t<decltype(maximum)>>, maximum > pl::constants::one<std::decay_t<decltype(maximum)>>);
	}


	// Dithering

	namespace ImplColor
	{
		template <pl::simd::CColorVector CT_>
		[[nodiscard]] constexpr auto ditherThreshold(const CT_ c, const typename CT_::value_type distance, const typename CT_::value_type threshold) noexcept
		{
			const auto c2 = c / distance;
			const auto c2floor = c2 /ofloor;
			const auto cthreshold = c2 - c2floor;
			const auto cstart = c2floor * distance;
			const auto cnext = cstart + distance;

			return pl::select(cstart, cnext, (cthreshold < threshold) | (cnext > constants::white<std::decay_t<decltype(c)>>));
		}


	}

	template <if32 Size_>
	struct ditherOrdered
	{
		static constexpr if32 Size = Size_;

		static_assert(pl::dependent_value_false<Size_>, "Unsupported dither size.");

		template <pl::simd::CColorVector CT_>
		[[nodiscard]] constexpr static auto dither(const CT_ c, const typename CT_::value_type /*distance*/, Point2Di) noexcept
		{
			return c;
		}
	};

	template <>
	struct ditherOrdered<2>
	{
		static constexpr if32 Size = 2;

		template <pl::simd::CColorVector CT_>
		[[nodiscard]] constexpr static auto dither(const CT_ c, const typename CT_::value_type distance, Point2Di p) noexcept
		{
			constexpr typename CT_::value_type matrix[Size][Size] = {
					{1.0/5.0, 3.0/5.0},
					{4.0/5.0, 2.0/5.0}
				};

			return ImplColor::ditherThreshold(c, distance, matrix[p[0] % Size][p[1] % Size]);
		}
	};

	template <>
	struct ditherOrdered<3>
	{
		static constexpr if32 Size = 3;

		template <pl::simd::CColorVector CT_>
		[[nodiscard]] constexpr static auto dither(const CT_ c, const typename CT_::value_type distance, Point2Di p) noexcept
		{
			constexpr typename CT_::value_type matrix[Size][Size] = {
					{0.1, 0.8, 0.4},
					{0.7, 0.6, 0.3},
					{0.5, 0.2, 0.9}
				};

			return ImplColor::ditherThreshold(c, distance, matrix[p[0] % Size][p[1] % Size]);
		}
	};

	template <>
	struct ditherOrdered<4>
	{
		static constexpr if32 Size = 4;

		template <pl::simd::CColorVector CT_>
		[[nodiscard]] constexpr static auto dither(const CT_ c, const typename CT_::value_type distance, Point2Di p) noexcept
		{
			constexpr typename CT_::value_type matrix[Size][Size] = {
					{1.0/17.0, 9.0/17.0, 3.0/17.0, 11.0/17.0},
					{13.0/17.0, 5.0/17.0, 15.0/17.0, 7.0/17.0},
					{4.0/17.0, 12.0/17.0, 2.0/17.0, 10.0/17.0},
					{16.0/17.0, 8.0/17.0, 14.0/17.0, 6.0/17.0}
				};

			return ImplColor::ditherThreshold(c, distance, matrix[p[0] % Size][p[1] % Size]);
		}
	};


	// Color conversion. single u8, float or double are considered grayscale color.

	template <pl::simd::CColorVector CT_, typename T_>
	[[nodiscard]] constexpr CT_ convertToColor(const T_ c) noexcept
	{
		return static_cast<CT_>(c);
	}

	template <pl::simd::CColorVector CT_>
	[[nodiscard]] constexpr CT_ convertToColor(const u16 c) noexcept
	{
		using CR = typename CT_::value_type;
		const CT_ factor{static_cast<CR>(1.0 / 31.0)};
		const CT_ res{
			static_cast<CR>(c&0x1F),
			static_cast<CR>((c>>5)&0x1F),
			static_cast<CR>((c>>10)&0x1F)};

		return preserveAlpha(res, res * factor);
	}

	template <pl::simd::CColorVector CT_>
	[[nodiscard]] constexpr CT_ convertToColor(const u8 *c) noexcept
	{
		using CR = typename CT_::value_type;
		const CT_ factor{static_cast<CR>(1.0 / 255.0)};
		const CT_ res{
				static_cast<CR>(c[0]),
				static_cast<CR>(c[1]),
				static_cast<CR>(c[2])
			};

		return preserveAlpha(res, res * factor);
	}

	template <pl::simd::CColorVector CT_>
	[[nodiscard]] constexpr CT_ convertToColor(const ColorInt24 c) noexcept
	{
		using CR = typename CT_::value_type;
		const CT_ factor{static_cast<CR>(1.0 / 255.0)};
		const CT_ res{
				static_cast<CR>(c[0]),
				static_cast<CR>(c[1]),
				static_cast<CR>(c[2])
			};

		return preserveAlpha(res, res * factor);
	}

	template <pl::simd::CColorVector CT_>
	[[nodiscard]] constexpr CT_ convertToColor(const u32 c) noexcept
	{
		using CR = typename CT_::value_type;
		const CT_ factor{static_cast<CR>(1.0 / 255.0)};

		return CT_{
				static_cast<CR>(c&0xff),
				static_cast<CR>((c>>8)&0xff),
				static_cast<CR>((c>>16)&0xff),
				static_cast<CR>((c>>24)&0xff)
			} * factor;
	}

	template <pl::simd::CColorVector CT_>
	[[nodiscard]] constexpr CT_ convertToColor(const u8 c) noexcept
	{
		using CR = typename CT_::value_type;
		return preserveAlpha(constants::white<CT_>, CT_{static_cast<CR>(c) * static_cast<CR>(1.0 / 255.0)});
	}

	template <pl::simd::CColorVector CT_>
	[[nodiscard]] constexpr CT_ convertToColor(const float c) noexcept
	{
		using CR = typename CT_::value_type;
		return preserveAlpha(constants::white<CT_>, CT_{static_cast<CR>(c)});
	}

	template <pl::simd::CColorVector CT_>
	[[nodiscard]] constexpr CT_ convertToColor(const double c) noexcept
	{
		using CR = typename CT_::value_type;
		return preserveAlpha(constants::white<CT_>, CT_{static_cast<CR>(c)});
	}


	template <pl::simd::CColorVector CTFrom_, pl::simd::CColorVector CTInto_>
	constexpr void convertFromColor(const CTFrom_ c, CTInto_ &into) noexcept
		{ pl::static_cast_into(c, into); }

	template <pl::simd::CColorVector CT_>
	constexpr void convertFromColor(const CT_ c, u16 &into) noexcept
	{
		const auto colorTmp = c * 32 <omin> CT_{31};
		const auto &ar = pl::simd::vector_access(colorTmp);

		into = static_cast<u16>(ar[0]) & 0x1F;

		for(const auto i : mir(1, pl::plmin<int>(CT_::NbValues, 3)))
			into |= ((static_cast<u16>(ar[i]) & 0x1F) << (i*5));
	}

	template <pl::simd::CColorVector CT_>
	constexpr void convertFromColor(const CT_ c, u8 *into) noexcept
	{
		const auto colorTmp = c * 256 <omin> CT_{255};
		const auto &ar = pl::simd::vector_access(colorTmp);

		for(const auto i : mir(pl::plmin<int>(CT_::NbValues, 3)))
			into[i] = static_cast<u8>(ar[i]);

		for(const auto i : mir(pl::plmin<int>(CT_::NbValues, 3), 3))
			into[i] = 0;
	}

	template <pl::simd::CColorVector CT_>
	constexpr void convertFromColor(const CT_ c, ColorInt24 &into) noexcept
	{
		const auto colorTmp = c * 256 <omin> CT_{255};
		const auto &ar = pl::simd::vector_access(colorTmp);

		for(const auto i : mir(pl::plmin<int>(CT_::NbValues, 3)))
			into[i] = static_cast<u8>(ar[i]);

		for(const auto i : mir(pl::plmin<int>(CT_::NbValues, 3), 3))
			into[i] = 0;
	}

	template <pl::simd::CColorVector CT_>
	constexpr void convertFromColor(const CT_ c, u32 &into) noexcept
	{
		const auto colorTmp = c * 256 <omin> CT_{255};
		const auto &ar = pl::simd::vector_access(colorTmp);

		into = static_cast<u32>(ar[0]);
		for(const auto i : mir(1, pl::plmin<int>(CT_::NbValues, 4)))
			into |= static_cast<u32>(ar[i]) << (i*8);
	}

	template <typename OutType_, typename InType_>
	constexpr OutType_ convertFromColor(const InType_ &c) noexcept
	{
		OutType_ oc; convertFromColor(c, oc); return oc;
	}

	constexpr void convertFromColor(pl::simd::CColorVector auto c, u8 &into) noexcept
	{
		into = (c <oblend<0b1000>> constants::black<decltype(c)> <omin> constants::white<decltype(c)> <ohadd)[size_v<0>] * (255.0 / 3.0);
	}

	constexpr void convertFromColor(pl::simd::CColorVector auto c, std::floating_point auto &into) noexcept
	{
		into = (c <oblend<0b1000>> constants::black<decltype(c)> <ohadd)[size_v<0>] * (1.0 / 3.0);
	}

	// Assume SRGB color profile
	[[nodiscard]] constexpr auto xyzToRGB(const pl::simd::CColorVector auto c) noexcept
	{
		using T = std::decay_t<decltype(c)>;
		using CR = typename T::value_type;

		const auto &ca = pl::simd::vector_access(c);
		return preserveAlpha(c, T{
				static_cast<CR>(3.240479) * (ca[0]) - static_cast<CR>(1.537150) * (ca[1]) - static_cast<CR>(0.498535) * (ca[2]),
				static_cast<CR>(-0.969256) * (ca[0]) + static_cast<CR>(1.875991) * (ca[1]) + static_cast<CR>(0.041556) * (ca[2]),
				static_cast<CR>(0.055648) * (ca[0]) - static_cast<CR>(0.204043) * (ca[1]) + static_cast<CR>(1.057311) * (ca[2])
			});
	}

	[[nodiscard]] constexpr auto rgbToXYZ(const pl::simd::CColorVector auto c) noexcept
	{
		using T = std::decay_t<decltype(c)>;
		using CR = typename T::value_type;

		const auto &ca = pl::simd::vector_access(c);
		return preserveAlpha(c, T{
				static_cast<CR>(0.412453) * (ca[0]) + static_cast<CR>(0.357580) * (ca[1]) + static_cast<CR>(0.180423) * (ca[2]),
				static_cast<CR>(0.212671) * (ca[0]) + static_cast<CR>(0.715160) * (ca[1]) + static_cast<CR>(0.072169) * (ca[2]),
				static_cast<CR>(0.019334) * (ca[0]) + static_cast<CR>(0.119193) * (ca[1]) + static_cast<CR>(0.950227) * (ca[2])
			});
	}

	[[nodiscard]] constexpr auto getColorY(const pl::simd::CColorVector auto c) noexcept
	{
		using T = std::decay_t<decltype(c)>;
		using CR = typename T::value_type;

		return preserveAlpha(
				T{},
				T{static_cast<CR>(0.212671), static_cast<CR>(0.715160), static_cast<CR>(0.072169)}
			) /odot/ c;
	}

	[[nodiscard]] constexpr auto getColorAlbedo(const pl::simd::CColorVector auto c) noexcept
	{
		return pl::plmin<typename decltype(c)::value_type>(getColorY(c), 1);
	}
}
