/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstring>
#include <iostream>
#include <string>
#include <type_traits>
#include <map>
#include <unordered_map>
#include <vector>
#include <list>
#include <algorithm>
#include <limits>
#include <iomanip>
#include <random>
#include <forward_list>
#include <cstdarg>
#include <memory>
#include <cmath>
#include <sstream>
#include <assert.h>
#include <fstream>
#include <cctype>
#include <functional>
#include <set>
#include <mutex>
#include <chrono>
#include <iterator>
#include <tuple>
#include <ctime>
#include <typeinfo>
#include <cstdint>
#include <cstddef>
#include <utility>
#include <future>
#include <atomic>
#include <shared_mutex>
#include <string_view>
#include <charconv>
#include <optional>
#include <any>
#include <stdexcept>
#include <span>
//#include <ranges>
#include <filesystem>
#include <thread>
#include <queue>
#include <deque>
#include <compare>
#include <numbers>
#include <experimental/source_location>
#include <numeric>
#include <bit>
#include <variant>
#include <bitset>

#include <boost/predef.h>

#if __has_include(<winsock2.h>)
	#include <winsock2.h>
#endif

#include <boost/container/flat_map.hpp>

#include <boost/iostreams/device/array.hpp>
#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/copy.hpp>

#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>

#include <boost/signals2.hpp>

#include <boost/lockfree/spsc_queue.hpp>
#include <boost/lockfree/queue.hpp>
#include <boost/lockfree/stack.hpp>

#include <boost/lexical_cast.hpp>

#include <boost/bimap/bimap.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/bimap/multiset_of.hpp>
#include <boost/bimap/list_of.hpp>
#include <boost/bimap/support/lambda.hpp>

#include <boost/proto/proto.hpp>

#include <boost/hana.hpp>

#include <boost/type_index.hpp>

#include <boost/logic/tribool.hpp>
#include <boost/logic/tribool_io.hpp>

#include <boost/container/static_vector.hpp>
#include <boost/container/small_vector.hpp>

#include <boost/static_string/static_string.hpp>

#ifdef PRT_IPC
	#include <boost/interprocess/managed_shared_memory.hpp>
	#include <boost/interprocess/mapped_region.hpp>
	#include <boost/interprocess/shared_memory_object.hpp>
	#include <boost/interprocess/managed_heap_memory.hpp>
	#include <boost/interprocess/managed_mapped_file.hpp>
	#include <boost/interprocess/sync/interprocess_sharable_mutex.hpp>
	#include <boost/interprocess/ipc/message_queue.hpp>
	#include <boost/interprocess/containers/vector.hpp>

	#include <boost/interprocess/smart_ptr/shared_ptr.hpp>
	#include <boost/interprocess/smart_ptr/weak_ptr.hpp>
	#include <boost/interprocess/smart_ptr/unique_ptr.hpp>
#endif

#include <boost/endian/conversion.hpp>
#include <boost/endian/buffers.hpp>
#include <boost/endian/arithmetic.hpp>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <boost/dynamic_bitset.hpp>

#include <memory_resource>
#include <experimental/type_traits>
#include <experimental/propagate_const>
#include <experimental/iterator>

#ifdef __has_include(<sched.h>)
	#include <sched.h>
#endif

#ifdef __has_include(<unistd.h>)
	#include <unistd.h>
#endif

#ifdef _OPENMP
	#include <omp.h>
#endif

#include <half/half.hpp>
#include <pcg_random.hpp>

#include <range/v3/all.hpp>

#include <shared_access/shared.hpp>

#include <patlib.hpp>

#include <fmt/format.h>
#include <fmt/ranges.h>
#include <fmt/ostream.h>
#include <fmt/chrono.h>
#include <fmt/std.h>