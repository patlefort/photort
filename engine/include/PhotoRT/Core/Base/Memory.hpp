/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Defs.hpp"

#include <patlib/memory.hpp>
#include <patlib/memory_system.hpp>

namespace PhotoRT
{
	#ifdef PRT_IPC
		template <typename T_>
		using OffsetPtr = pl::offset_ptr<T_>;
	#else
		template <typename T_>
		using OffsetPtr = T_*;
	#endif

	using VoidOffsetPtr = OffsetPtr<void>;

	template <typename T_>
	using PmrAllocatorOffset = pl::pmr_allocator<T_, OffsetPtr<T_>>;

	namespace memtag
	{
		struct Texture {};
		struct Film {};
		struct ContBuffer {};
		struct Primitive {};
		struct AccelStruct {};
		struct Entity {};
		struct Other {};

		#define PRT_ALLOC_STATS(x) \
			PHOTORT_API pl::allocation_stats &alloc_stat(x);

		#define PRT_ALLOC_STATS_DEF(x) \
			PHOTORT_API pl::allocation_stats &alloc_stat(x) \
			{ \
				static pl::allocation_stats stats; \
				return stats; \
			}

		PRT_ALLOC_STATS(Texture)
		PRT_ALLOC_STATS(ContBuffer)
		PRT_ALLOC_STATS(Film)
		PRT_ALLOC_STATS(Primitive)
		PRT_ALLOC_STATS(AccelStruct)
		PRT_ALLOC_STATS(Entity)
		PRT_ALLOC_STATS(Other)
	} // namespace memtag

	template <typename Category_ = void>
	std::shared_ptr<pl::IMemorySystem<VoidOffsetPtr>> &getMemorySystem()
	{
		static std::shared_ptr<pl::IMemorySystem<VoidOffsetPtr>> ms;
		return ms;
	}

	template <>
	PL_CONST_FC( std::shared_ptr<pl::IMemorySystem<VoidOffsetPtr>> PHOTORT_API &getMemorySystem<void>() );

	template <>
	PL_CONST_FC( std::shared_ptr<pl::IMemorySystem<VoidOffsetPtr>> PHOTORT_API &getMemorySystem<memtag::Primitive>() );

	template <>
	PL_CONST_FC( std::shared_ptr<pl::IMemorySystem<VoidOffsetPtr>> PHOTORT_API &getMemorySystem<memtag::Texture>() );

	template <>
	PL_CONST_FC( std::shared_ptr<pl::IMemorySystem<VoidOffsetPtr>> PHOTORT_API &getMemorySystem<memtag::Film>() );

	template <>
	PL_CONST_FC( std::shared_ptr<pl::IMemorySystem<VoidOffsetPtr>> PHOTORT_API &getMemorySystem<memtag::ContBuffer>() );

	PL_CONST_FC( bool PHOTORT_API &getMemoryOwnership() );

	template <typename T_, typename Category_>
	class PmrAllocatorGlobal
	{
	public:

		using value_type = T_;
		using size_type = szt;
		using pointer = OffsetPtr<T_>;
		using difference_type = typename std::pointer_traits<pointer>::difference_type;
		using const_pointer = typename std::pointer_traits<pointer>::template rebind<const value_type>;
		using void_pointer = typename std::pointer_traits<pointer>::template rebind<void>;

		using offset_memory_resource_type = pl::IMemoryResource<void_pointer>;

		using is_always_equal = std::true_type;

		using propagate_on_container_move_assignment = std::false_type;
		using propagate_on_container_copy_assignment = std::false_type;
		using propagate_on_container_swap = std::false_type;

		/*template <typename OT_>
		struct rebind
		{
			using other = PmrAllocatorGlobal<OT_, Category_>;
		};*/

		PmrAllocatorGlobal() = default;
		template <typename... TA_>
		PmrAllocatorGlobal(const PmrAllocatorGlobal<TA_...> &) {}

		[[nodiscard]] bool operator==(const PmrAllocatorGlobal &) const
		{
			return true;
		}

		[[nodiscard]] auto allocate_bytes(size_type nbytes, size_type alignment = alignof(std::max_align_t))
		{
			auto &memSys = getMemorySystem<Category_>();
			return pl::assert_valid(memSys)->allocate(nbytes, alignment);
		}

		void deallocate_bytes(void_pointer p, size_type nbytes, size_type alignment = alignof(std::max_align_t))
		{
			auto &memSys = getMemorySystem<Category_>();
			pl::assert_valid(memSys)->deallocate(p, nbytes, alignment);
		}

		template<typename OT_>
		auto allocate_object(size_type n = 1)
		{
			using PT = typename std::pointer_traits<pointer>::template rebind<OT_>;

			if(std::numeric_limits<szt>::max() / sizeof(OT_) < n)
				throw std::length_error("Size too big to allocate objects.");

			return static_cast<PT>( allocate_bytes(n * sizeof(OT_), alignof(OT_)) );
		}

		template<typename OT_>
		void deallocate_object(typename std::pointer_traits<pointer>::template rebind<T_> p, size_type n = 1)
		{
			deallocate_bytes(static_cast<void_pointer>(p), n * sizeof(OT_), alignof(OT_));
		}

		template<typename OT_, typename... CtorArgs_>
		auto new_object(CtorArgs_&&... ctor_args)
		{
			auto p = allocate_object<OT_>();

			try {
				std::construct_at(std::to_address(p), PL_FWD(ctor_args)...);
			} catch (...) {
				deallocate_object(p);
				throw;
			}

			return p;
		}

		template<typename OT_>
		void delete_object(typename std::pointer_traits<pointer>::template rebind<OT_> p)
		{
			std::destroy_at(std::to_address(p));
			deallocate_object(p);
		}

		[[nodiscard]] auto allocate(const size_type n)
		{
			return static_cast<pointer>( allocate_bytes(n * sizeof(value_type), alignof(value_type)) );
		}

		void deallocate(pointer p, const size_type) noexcept
		{
			deallocate_bytes(static_cast<void_pointer>(p), sizeof(value_type), alignof(value_type));
		}

	};
}
