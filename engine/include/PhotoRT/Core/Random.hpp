/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Random_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"

#include <random>

#include <pcg_extras.hpp>

namespace PhotoRT
{
	namespace ImplRng
	{
		template <typename T_>
		void seedFromRandom(T_ &rng)
		{
			pcg_extras::seed_seq_from<std::random_device> seedSource;
			rng.seed(seedSource);
		}

		template <typename T_>
		concept CRngHasParam =
			requires
			{
				typename T_::param_type;
			} &&
			requires(T_ r, const T_ cr, typename T_::param_type p)
			{
				{ cr.param() } -> pl::CNotVoid;
				{ r.param(p) };
			};

	} // namespace ImplRng


	template <typename RNGT_>
	class VirtualRNGBase : public IRNGInt
	{
	public:
		VirtualRNGBase() = default;
		VirtualRNGBase(RNGT_ r) requires pl::CMoveContructible<RNGT_> : mRng{std::move(r)} {}

		virtual result_type generate() noexcept final { return std::uniform_int_distribution<result_type>{IRNGInt::min(), IRNGInt::max()}(mRng); }
		virtual void generate(std::span<result_type> values) noexcept override
		{
			for(auto &v : values)
				v = generate();
		}
		virtual void setPosition(u64 /*sampleNo*/, const Point2Di &/*p*/) override {}
		virtual void setBounds(const Point2Di &/*p*/) override {}

		[[nodiscard]] virtual std::any param() const final
		{
			if constexpr(ImplRng::CRngHasParam<RNGT_>)
				return mRng.param();
			else
				return pl::tag::nothing;
		}
		virtual void param([[maybe_unused]] const std::any &param) final
		{
			if constexpr(ImplRng::CRngHasParam<RNGT_>)
				mRng.param(std::any_cast<typename RNGT_::param_type>(param));
		}

	protected:
		[[no_unique_address]] RNGT_ mRng;
	};

	template <typename RNGT_>
	class RNGStd : public VirtualRNGBase<RNGT_>
	{
	public:

		virtual void seed() override {}
	};

	template <typename RNGT_>
	class RNGStdSeedable : public VirtualRNGBase<RNGT_>
	{
	public:

		virtual void seed() override
			{ ImplRng::seedFromRandom(this->mRng); }
	};

	template <typename RNGT_>
	class RNGStdPositionable : public RNGStdSeedable<RNGT_>
	{
	public:

		virtual void setPosition(u64 sampleNo, const Point2Di &p) override { this->mRng.setPosition(sampleNo, p); }
		virtual void setBounds(const Point2Di &p) override { this->mRng.setBounds(p); }
	};


	template <typename RNGT_>
	class RNGStdAdaptorStreams : public VirtualRNGBase<RNGT_>
	{
	public:
		virtual void seed() override
		{
			std::array<int, 1> seeds;

			pcg_extras::seed_seq_from<std::random_device> seedSource;

			seedSource.generate(seeds.begin(), seeds.end());

			this->mRng.set_stream(seeds[0]);
			this->mRng.seed(seedSource);
		}
	};
}
