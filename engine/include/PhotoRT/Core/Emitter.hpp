/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Transform_fwd.hpp"
#include "Material_fwd.hpp"
#include "Scene_fwd.hpp"
#include "Emitter_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"
#include "Sensors.hpp"
#include "Interaction.hpp"
#include "SubstanceStack.hpp"

namespace PhotoRT
{
	class IEmitter : public ISensor
	{
	public:
		virtual ~IEmitter() = default;

		virtual void init(const Scene &scene) = 0;

		virtual void setGroupName(std::string_view name) = 0;
		[[nodiscard]] virtual std::string_view getGroupName() const noexcept = 0;

		virtual void setGroupIndex(szt index) = 0;
		[[nodiscard]] virtual szt getGroupIndex() const noexcept = 0;
	};

	class PHOTORT_API EmitterBase : public IEmitter
	{
	public:

		virtual void init(const Scene &) override {}

		[[nodiscard]] virtual bool isDiracDelta() const noexcept override { return false; }
		[[nodiscard]] virtual bool isInfinite() const noexcept override { return false; }
		[[nodiscard]] virtual bool hasArea() const noexcept override { return false; }

		[[nodiscard]] virtual const SubstanceStack &getSubstanceStack() const noexcept override { return mSubstanceStack; }
		[[nodiscard]] virtual SubstanceStack &getSubstanceStack() noexcept override { return mSubstanceStack; }

		[[nodiscard]] virtual OptionalPDF<Real> worldPDF(const MaterialInteraction &sensorSurface) const noexcept override { return PDF(sensorSurface); }
		[[nodiscard]] virtual OptionalPDF<Real> PDF(const MaterialInteraction &/*sensorSurface*/) const noexcept override { return pl::nullopt; }
		[[nodiscard]] virtual OptionalPDF<Real> PDFA(const MaterialInteraction &/*sensorSurface*/, const MaterialInteraction &, VecD /*dir*/) const noexcept override { return pl::nullopt; }
		[[nodiscard]] virtual OptionalPDF<Real> PDFW(const MaterialInteraction &/*sensorSurface*/, VecD /*dir*/) const noexcept override { return pl::nullopt; }

		[[nodiscard]] virtual VecP getMiddle() const noexcept override { return {}; }

		[[nodiscard]] virtual VecD worldToLocal(VecD v) const noexcept override { return v; }
		[[nodiscard]] virtual VecD localToWorld(VecD v) const noexcept override { return v; }
		[[nodiscard]] virtual VecP worldToLocal(VecP v) const noexcept override { return v; }
		[[nodiscard]] virtual VecP localToWorld(VecP v) const noexcept override { return v; }

		virtual void setGroupName(std::string_view name) override { mGroupName = name; }
		[[nodiscard]] virtual std::string_view getGroupName() const noexcept override { return mGroupName; }

		virtual void setGroupIndex(szt index) override { mGroupIndex = index; }
		[[nodiscard]] virtual szt getGroupIndex() const noexcept override { return mGroupIndex; }

	protected:

		SubstanceStack mSubstanceStack;
		std::string mGroupName;
		szt mGroupIndex = 0;

	};

	class PHOTORT_API EmitterEntityBase : public RenderEntity, public EmitterBase
	{
	public:
		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;
	};
}
