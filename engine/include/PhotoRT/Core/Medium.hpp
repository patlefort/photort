/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Medium_fwd.hpp"
#include "Interaction_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"
#include "Ray.hpp"

namespace PhotoRT
{
	struct SampledMedium
	{
		VecD dir;
		Color color;
		Real scatterDist, pdfDir, pdfScatter;
		EnumRayType type;
	};

	class IMedium
	{
	public:
		virtual ~IMedium() = default;

		[[nodiscard]] virtual SampledMedium sampleScattering(RandomReals<2> rnd, const MaterialInteraction &inter, const VecD odir, Real distance, IRNGInt &rng, EnumRayType typeMask = EnumRayType::All) const noexcept = 0;
		[[nodiscard]] virtual OptionalPDF<Real> dirPDF(const MediumInteraction &inter, const VecD odir, const VecD idir) const noexcept = 0;
		[[nodiscard]] virtual Color transmittance(const MaterialInteraction &inter, const VecD odir, Real scatterDist, IRNGInt &rng) const noexcept = 0;

		[[nodiscard]] virtual EnumRayType getType() const noexcept = 0;
		[[nodiscard]] virtual bool isDiracDelta() const noexcept = 0;
		[[nodiscard]] virtual bool isHomogeneous() const noexcept = 0;

		[[nodiscard]] auto operator() (const MaterialInteraction &inter, const VecD odir, Real scatterDist, IRNGInt &rng) const noexcept
			{ return transmittance(inter, odir, scatterDist, rng); }
	};

	class MediumBase : public RenderEntity, public IMedium
	{
	public:

		[[nodiscard]] virtual EnumRayType getType() const noexcept override { return EnumRayType::Diffuse; }
		[[nodiscard]] virtual bool isDiracDelta() const noexcept override { return false; }
		virtual void setHomogeneous(bool v) { mHomogeneous = v; }
		[[nodiscard]] virtual bool isHomogeneous() const noexcept override { return mHomogeneous; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:
		bool mHomogeneous = true;
	};

}
