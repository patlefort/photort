/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Scene_fwd.hpp"
#include "Sensors_fwd.hpp"
#include "SubstanceStack_fwd.hpp"

#include "Base.hpp"
//#include "RayTracer.hpp"
#include "Sampling.hpp"
#include "Interaction.hpp"

#include <patlib/marked_optional.hpp>

namespace PhotoRT
{
	struct SampledSensor
	{
		MaterialInteraction interaction;
		Real pdf = 0;

		constexpr void reset() noexcept { pdf = 0; }
		[[nodiscard]] constexpr bool has_value() const noexcept { return pdf != 0; }
	};

	struct SampledSensorOutDir
	{
		Color color;
		VecD dir;
		VecP origin;
		Real pdf = 0;
		EnumHemisphere hemisphere = EnumHemisphere::North;
		bool delta = false;

		constexpr void reset() noexcept { pdf = 0; }
		[[nodiscard]] constexpr bool has_value() const noexcept { return pdf != 0; }
	};

	struct SampledSensorOut
	{
		SampledSensor sensor;
		SampledSensorOutDir dir;

		constexpr void reset() noexcept { sensor.reset(); }
		[[nodiscard]] constexpr bool has_value() const noexcept { return sensor.has_value(); }
	};

	struct SampledSensorIn
	{
		SampledSensor sensor;
		Color color;
		VecD dir;
		Real pdf = 0;
		bool delta = false;

		constexpr void reset() noexcept { pdf = 0; }
		[[nodiscard]] constexpr bool has_value() const noexcept { return pdf != 0; }
	};

	class ISensor
	{
	public:
		virtual ~ISensor() = default;

		[[nodiscard]] virtual bool isDiracDelta() const noexcept = 0;
		[[nodiscard]] virtual bool isInfinite() const noexcept = 0;
		[[nodiscard]] virtual bool hasArea() const noexcept = 0;

		[[nodiscard]] virtual VecP getMiddle() const noexcept = 0;

		[[nodiscard]] virtual const SubstanceStack &getSubstanceStack() const noexcept = 0;
		[[nodiscard]] virtual SubstanceStack &getSubstanceStack() noexcept = 0;

		[[nodiscard]] virtual VecD worldToLocal(VecD v) const noexcept = 0;
		[[nodiscard]] virtual VecD localToWorld(VecD v) const noexcept = 0;
		[[nodiscard]] virtual VecP worldToLocal(VecP v) const noexcept = 0;
		[[nodiscard]] virtual VecP localToWorld(VecP v) const noexcept = 0;

		[[nodiscard]] virtual OptionalPDF<Real> PDF(const MaterialInteraction &sensorSurface) const noexcept = 0;
		[[nodiscard]] virtual OptionalPDF<Real> worldPDF(const MaterialInteraction &sensorSurface) const noexcept = 0;
		[[nodiscard]] virtual OptionalPDF<Real> PDFA(const MaterialInteraction &sensorSurface, const MaterialInteraction &surface, VecD dir) const noexcept = 0;
		[[nodiscard]] virtual OptionalPDF<Real> PDFW(const MaterialInteraction &sensorSurface, VecD dir) const noexcept = 0;

		[[nodiscard]] virtual pl::marked_optional<SampledSensorOut> sampleOut(IRNGInt &rng, RandomReals<2> rndPos, RandomReals<2> rndDir) const noexcept = 0;
		[[nodiscard]] virtual pl::marked_optional<SampledSensorOutDir> sampleOutDir(const SampledSensor &sensorSurface, IRNGInt &rng, RandomReals<2> rndDir) const noexcept = 0;
		[[nodiscard]] virtual pl::marked_optional<SampledSensorIn> sampleIn(const MaterialInteraction &surface, IRNGInt &rng, RandomReals<2> rndPos) const noexcept = 0;
		[[nodiscard]] virtual pl::marked_optional<SampledSensor> sampleArea(IRNGInt &rng, RandomReals<2> rndPos) const noexcept = 0;

		[[nodiscard]] virtual Color radiance(const MaterialInteraction &sensorSurface, VecD dir) const noexcept = 0;

		[[nodiscard]] virtual Color averagePower() const noexcept = 0;

		[[nodiscard]] auto operator() (const MaterialInteraction &sensorSurface, VecD dir) const noexcept
			{ return radiance(sensorSurface, dir); }
	};

	template <typename SensorType_ = ISensor>
	class ISensorSampler
	{
	public:
		virtual ~ISensorSampler() = default;

		using SampleFunctor = std::function<void(const SensorType_ &sensor, uf32 index, Real weight, Real pdf, RandomReals<2> rnd)>;

		virtual void setList(std::span<const std::experimental::propagate_const<SensorType_ *>> sensors) = 0;
		[[nodiscard]] virtual std::span<const std::experimental::propagate_const<SensorType_ *>> getList() const noexcept = 0;

		virtual void init(const Scene &scene, TracerParams &tracer) = 0;

		virtual void sample(const MaterialInteraction &inter, uf32 nbSamples, uf32 nbSamplesPerSensors, Sampler &rng, const SampleFunctor &sampleFc) const noexcept = 0;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(const MaterialInteraction &inter, const SensorType_ &sensor) const noexcept = 0;

		[[nodiscard]] virtual Sample<const SensorType_ *> sampleOne(const MaterialInteraction &inter, IRNGInt &rng) const noexcept = 0;
	};

	template <typename SensorType_ = ISensor>
	class SensorSamplerBase : public ISensorSampler<SensorType_>
	{
	public:
		using SampleFunctor = typename ISensorSampler<SensorType_>::SampleFunctor;

		virtual void setList(std::span<const std::experimental::propagate_const<SensorType_ *>> sensors) override { mSensors = sensors; }
		[[nodiscard]] virtual std::span<const std::experimental::propagate_const<SensorType_ *>> getList() const noexcept override { return mSensors; }

		[[nodiscard]] virtual OptionalPDF<Real> PDF(const MaterialInteraction &, const SensorType_ &) const noexcept override { return mPdf; }

		virtual void init(const Scene &, TracerParams &) override
		{
			mPdf = mSensors.empty() ? 0 : 1.0 / mSensors.size();
		}

		[[nodiscard]] virtual Sample<const SensorType_ *> sampleOne(const MaterialInteraction &, IRNGInt &rng) const noexcept override
		{
			const auto nbs = mSensors.size();

			if(!nbs)
				return {nullptr, 0};

			const auto index = uniformIndexRng(nbs)(rng);

			return {std::to_address(mSensors[index]), mPdf};
		}

	protected:
		std::span<const std::experimental::propagate_const<SensorType_ *>> mSensors;
		Real mPdf = 0;
	};

	template <typename SensorType_ = ISensor>
	class SensorSamplerOne : public SensorSamplerBase<SensorType_>
	{
		PL_MAKE_STATIC_MULTITAGGEDSPAN_TAG(left)
		PL_MAKE_STATIC_MULTITAGGEDSPAN_TAG(right)

	public:
		using SampleFunctor = typename SensorSamplerBase<SensorType_>::SampleFunctor;

		virtual void sample(const MaterialInteraction &, uf32 nbSamples, uf32 nbSamplesPerSensors, Sampler &rng, const SampleFunctor &sampleFc) const noexcept override
		{
			const auto ss = this->mSensors;
			const auto nbs = ss.size();

			if(!nbs)
				return;

			const auto nbTotal = pl::plmin<uf32>(PRT_MAX_NBSAMPLES, nbSamples * nbSamplesPerSensors);
			const auto w = Real(nbTotal) /oprtreciprocal;
			RandomInt rndl[PRT_MAX_NBSAMPLES];
			const std::span rndlSpan{rndl, nbSamples};
			RandomReal rndx[PRT_MAX_NBSAMPLES], rndy[PRT_MAX_NBSAMPLES];

			const pl::dynamic_multi_tagged_span<
					hn::pair<type_c<left_t>, type_c<RandomReal>>,
					hn::pair<type_c<right_t>, type_c<RandomReal>>
				> rndSpan{nbTotal, hn::make_pair(left, pl::range_data(rndx)), hn::make_pair(right, pl::range_data(rndy))};

			rng.requestSamples(rndlSpan, std::uniform_int_distribution<RandomInt>{0, static_cast<RandomInt>(nbs) - 1}, true);
			rng.requestSamples(rndSpan[left], uniformRealRng<>(), true);
			rng.requestSamples(rndSpan[right], uniformRealRng<>(), true);

			rg::shuffle(rndSpan[left], rng.mRng);
			rg::shuffle(rndSpan[right], rng.mRng);

			rgv::for_each(
					rgv::zip(rndlSpan, rgv::chunk(rndSpan[left], nbSamplesPerSensors), rgv::chunk(rndSpan[right], nbSamplesPerSensors)),
					pl::applier([&](const auto i, const auto chunkx, const auto chunky){
						return rg::yield_from(rgv::zip(rgv::repeat_n(i, nbSamplesPerSensors), chunkx, chunky));
					})
				)
				| pl::for_each(pl::applier([&](const auto i, const auto rndx, const auto rndy){
					sampleFc(*ss[i], i, w, this->mPdf, {rndx, rndy});
				}));
		}
	};

	template <typename SensorType_ = ISensor>
	class SensorSamplerAll : public SensorSamplerBase<SensorType_>
	{
		PL_MAKE_STATIC_MULTITAGGEDSPAN_TAG(left)
		PL_MAKE_STATIC_MULTITAGGEDSPAN_TAG(right)

	public:
		using SampleFunctor = typename SensorSamplerBase<SensorType_>::SampleFunctor;

		virtual void sample(const MaterialInteraction &, uf32 nbSamples, uf32 nbSamplesPerSensors, Sampler &rng, const SampleFunctor &sampleFc) const noexcept override
		{
			const auto ss = this->mSensors;
			const auto nbs = ss.size();

			if(!nbs)
				return;

			const auto nbTotal = pl::plmin<uf32>(PRT_MAX_NBSAMPLES, nbSamples * nbSamplesPerSensors);
			const auto w = Real(nbs * nbTotal) /oprtreciprocal;
			RandomReal rndx[PRT_MAX_NBSAMPLES], rndy[PRT_MAX_NBSAMPLES];

			const pl::dynamic_multi_tagged_span<
					hn::pair<type_c<left_t>, type_c<RandomReal>>,
					hn::pair<type_c<right_t>, type_c<RandomReal>>
				> rndSpan{nbTotal, hn::make_pair(left, pl::range_data(rndx)), hn::make_pair(right, pl::range_data(rndy))};

			for(const auto l : mir(nbs))
			{
				rng.requestSamples(rndSpan[left], uniformRealRng<>(), true);
				rng.requestSamples(rndSpan[right], uniformRealRng<>(), true);

				rg::shuffle(rndSpan[right], rng.mRng);

				const auto &sensor = *ss[l];

				for(const auto [rndx, rndy] : rndSpan.range())
					sampleFc(sensor, l, w, this->mPdf, {rndx, rndy});
			}
		}
	};

}
