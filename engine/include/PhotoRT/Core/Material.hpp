/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Edf_fwd.hpp"
#include "Medium_fwd.hpp"
#include "BxDF_fwd.hpp"
#include "Material_fwd.hpp"
#include "Normals_fwd.hpp"
#include "Interaction_fwd.hpp"
#include "RayTracer_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"
//#include "Sampling.hpp"

namespace PhotoRT
{
	class MaterialLayer
	{
	protected:

		EntityPtr<const IBxDF> mBxdf, mOptimizedBxdf;
		EntityPtr<const IEDF> mEdf;
		Real mRefractionIndex = 1, mRefractionIndexInvert = 1;
		Material *mMaterial = nullptr;
		bool mSubstance = false;

	public:

		MaterialLayer() = default;
		MaterialLayer(Material *m) : mMaterial{m} {}

		[[nodiscard]] auto &getMaterial() noexcept { return *mMaterial; }
		[[nodiscard]] const auto &getMaterial() const noexcept { return *mMaterial; }

		void setBxDF(EntityPtr<const IBxDF> b) { mBxdf = std::move(b); }
		[[nodiscard]] const auto &getBxDF() const noexcept { return mBxdf; }

		void setOptimizedBxDF(EntityPtr<const IBxDF> b) { mOptimizedBxdf = std::move(b); }
		[[nodiscard]] const auto &getOptimizedBxDF() const noexcept { return mOptimizedBxdf; }

		void setEmission(EntityPtr<const IEDF> e) { mEdf = std::move(e); }
		[[nodiscard]] const auto &getEmission() const noexcept { return mEdf; }

		void setRefractionIndex(Real refrind)
		{
			mRefractionIndex = refrind;
			mRefractionIndexInvert = 1 / refrind;
		}
		[[nodiscard]] auto getRefractionIndex() const noexcept { return mRefractionIndex; }
		[[nodiscard]] auto getRefractionIndexInvert() const noexcept { return mRefractionIndexInvert; }

		[[nodiscard]] bool isEmissive() const noexcept { return mEdf != nullptr; }
		[[nodiscard]] bool isReflective() const noexcept { return mBxdf != nullptr; }

		void setSubstance(bool s) { mSubstance = s; }
		[[nodiscard]] bool isSubstance() const noexcept { return mSubstance; }
	};

	struct LayerAlpha
	{
		Color alpha;
		Real pdf;
	};

	class PHOTORT_API Material : public RenderEntity
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("material-", Material)

		Material();
		virtual ~Material() override;

	protected:

		Color
			mColor{pl::tag::one},
			mAbsorbance{},
			mAbsorbanceInf{pl::tag::one};

		u32 mRefractionPriority = 1;

		Real mSamplingImportance = 1;

		bool mTransparent = false, mAbsorbant = false, mAnisotropy = false, mSampleDirectLightMedium = true;

		EntityPtr<const INormalMapper> mNormalMap;
		EntityPtr<const IMedium> mMedium;

		void oneIfEmpty()
		{
			if(mLayers.empty())
				mLayers.emplace_back(this);
		}

	public:

		std::vector<MaterialLayer> mLayers;

		[[nodiscard]] auto &getFrontLayer(bool backfaceHit = false) noexcept { plassert(!mLayers.empty()); return backfaceHit ? mLayers.back() : mLayers.front(); }
		[[nodiscard]] const auto &getFrontLayer(bool backfaceHit = false) const noexcept { plassert(!mLayers.empty()); return backfaceHit ? mLayers.back() : mLayers.front(); }
		[[nodiscard]] if32 getFrontLayerIndex(bool backfaceHit = false) const noexcept { return backfaceHit ? mLayers.size() - 1 : 0; }

		[[nodiscard]] auto &getBackLayer(bool backfaceHit = false) noexcept { plassert(!mLayers.empty()); return backfaceHit ? mLayers.front() : mLayers.back(); }
		[[nodiscard]] const auto &getBackLayer(bool backfaceHit = false) const noexcept { plassert(!mLayers.empty()); return backfaceHit ? mLayers.front() : mLayers.back(); }
		[[nodiscard]] if32 getBackLayerIndex(bool backfaceHit = false) const noexcept { return backfaceHit ? 0 : mLayers.size() - 1; }

		[[nodiscard]] LayerAlpha getLayerAlpha(if32 layerIndex, EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir, EnumHemisphere hemisphere, bool ignoreSelf = true) const noexcept;

		void setBxDF(EntityPtr<const IBxDF> bxdf) { oneIfEmpty(); mLayers[0].setBxDF(std::move(bxdf)); }
		[[nodiscard]] const auto &getBxDF() const noexcept { return mLayers[0].getBxDF(); }

		void setOptimizedBxDF(EntityPtr<const IBxDF> bxdf) { oneIfEmpty(); mLayers[0].setOptimizedBxDF(std::move(bxdf)); }
		[[nodiscard]] const auto &getOptimizedBxDF() const noexcept { return mLayers[0].getOptimizedBxDF(); }

		[[nodiscard]] Color absorbColor(Real distance) const noexcept;

		void setEmission(EntityPtr<const IEDF> emission) { oneIfEmpty(); mLayers[0].setEmission(std::move(emission)); }
		[[nodiscard]] const auto &getEmission() const noexcept { return mLayers[0].getEmission(); }

		[[nodiscard]] bool isEmissive() const noexcept { return mLayers.empty() ? false : mLayers[0].isEmissive(); }
		[[nodiscard]] bool isReflective() const noexcept { return mLayers.empty() ? false : mLayers[0].isReflective(); }

		void setColor(CReal cr, CReal cg, CReal cb, CReal ca);
		void setColor(u32 col);
		void setColor(Color col);
		[[nodiscard]] Color getColor() const noexcept { return mColor; }

		void setRefractionIndex(Real refrind) { oneIfEmpty(); mLayers.back().setRefractionIndex(refrind); }
		[[nodiscard]] auto getRefractionIndex() const noexcept { return mLayers.empty() ? 1 : mLayers.back().getRefractionIndex(); }

		void setRefractionPriority(u32 priority) { mRefractionPriority = priority; }
		[[nodiscard]] auto getRefractionPriority() const noexcept { return mRefractionPriority; }
		[[nodiscard]] auto getRefractionIndexInvert() const noexcept { return mLayers.empty() ? 1 : mLayers.back().getRefractionIndexInvert(); }

		void setNormalMapper(EntityPtr<const INormalMapper> normalMap) { mNormalMap = std::move(normalMap); }
		[[nodiscard]] const auto &getNormalMapper() const noexcept { return mNormalMap; }

		[[nodiscard]] auto getSamplingImportance() const noexcept { return mSamplingImportance; }
		void setSamplingImportance(Real nb) { mSamplingImportance = nb; }

		void setTransparent(bool transparent) { mTransparent = transparent; }
		[[nodiscard]] bool isTransparent() const noexcept { return mTransparent; }
		void setSubstance(bool substance) { oneIfEmpty(); mLayers.back().setSubstance(substance); }
		[[nodiscard]] bool isSubstance() const noexcept { return mLayers.empty() ? false : mLayers.back().isSubstance(); }
		void setAbsorbant(bool absorbant) { mAbsorbant = absorbant; }
		[[nodiscard]] bool isAbsorbant() const noexcept { return mAbsorbant; }
		void setSampleDirectLightMedium(bool s) { mSampleDirectLightMedium = s; }
		[[nodiscard]] bool isSamplingDirectLightMedium() const noexcept { return mSampleDirectLightMedium; }

		void setMedium(EntityPtr<const IMedium> m) { mMedium = std::move(m); }
		[[nodiscard]] const auto &getMedium() const noexcept { return mMedium; }
		[[nodiscard]] bool isMedium() const noexcept { return mMedium != nullptr; }

		[[nodiscard]] bool isAnisotropic() const noexcept { return mAnisotropy; }

		void setAbsorbance(Color absorb)
		{
			mAbsorbance = absorb;
			mAbsorbanceInf = {
				CReal(mAbsorbance[0] > 0 ? 0 : 1),
				CReal(mAbsorbance[1] > 0 ? 0 : 1),
				CReal(mAbsorbance[2] > 0 ? 0 : 1),
				1
			};
		}
		[[nodiscard]] auto getAbsorbance() const noexcept { return mAbsorbance; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

		void initForRendering();

	};
}
