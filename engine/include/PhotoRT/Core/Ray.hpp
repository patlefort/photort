/*
	PhotoRT

	Copyright (C) 2022 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Ray_fwd.hpp"
#include "Instance_fwd.hpp"
#include "PrimitiveGroup_fwd.hpp"
#include "Medium_fwd.hpp"
#include "Scene_fwd.hpp"
#include "Sampling_fwd.hpp"
#include "RayTracer_fwd.hpp"
#include "RayTraversalContainers_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"
#include "Statistics.hpp"

#include <patlib/bitset.hpp>

#include <boost/container/static_vector.hpp>

namespace PhotoRT
{
	enum class EnumRayType : u32
	{
		Empty =           0,
		Diffuse =         0b1,
		Specular =        0b1 << 1,
		Shadow =          0b1 << 2,
		Primary =         0b1 << 3,
		SpecularGlossy =  0b1 << 4,
		Photon =          0b1 << 5,
		Transparency =    0b1 << 6,
		Passthrough =     0b1 << 7,
		Portal =          0b1 << 8,
		All =             pl::constants::all_ones<u32>
	};

	constexpr EnumRayType operator|(EnumRayType lhs, EnumRayType rhs) noexcept
		{ return static_cast<EnumRayType>( pl::underlying_value(lhs) | pl::underlying_value(rhs) ); }

	constexpr EnumRayType operator&(EnumRayType lhs, EnumRayType rhs) noexcept
		{ return static_cast<EnumRayType>( pl::underlying_value(lhs) & pl::underlying_value(rhs) ); }

	template <typename T_>
	constexpr bool isType(const T_ &b, EnumRayType type) noexcept
		{ return static_cast<bool>(b.getType() & type); }


	struct BasicRay
	{
		VecP origin;
		VecD dir, dirInv;
	};

	class Ray : public BasicRay
	{
	public:

		static constexpr auto MaxDistance = std::numeric_limits<Real>::infinity();

		Real length;
		EnumRayType type;

		Ray() = default;
		Ray(const VecP origin_, const VecD dir_, EnumRayType type_ = EnumRayType::Primary, Real length_ = MaxDistance) noexcept
			: BasicRay{origin_, dir_, {}}, length{length_}, type{type_} {}

		[[nodiscard]] constexpr EnumRayType getType() const noexcept { return type; }
		[[nodiscard]] constexpr bool isType(EnumRayType t) const noexcept { return PhotoRT::isType(*this, t); }

		void setupPathRay() noexcept
		{
			length = Ray::MaxDistance;
		}

		void prepareForTracing() noexcept
		{
			dirInv = dirReciprocal(dir);
		}

		void setupPhoton() noexcept
		{
			type = EnumRayType::Photon;
			length = Ray::MaxDistance;
			//mode = EnumTransportMode::Importance;
		}

		void setupPrimary() noexcept
		{
			type = EnumRayType::Primary;
			length = Ray::MaxDistance;
			//mode = EnumTransportMode::Radiance;
		}

		void setupNull() noexcept
		{
			length = 0;
		}

		void setupShadow() noexcept
		{
			type = EnumRayType::Shadow;
		}

		void setupTransparent() noexcept
		{
			type = EnumRayType::Transparency;
			length = Ray::MaxDistance;
		}

		void setupPassthrough() noexcept
		{
			type = EnumRayType::Passthrough;
			length = Ray::MaxDistance;
		}

		void setupPortal() noexcept
		{
			type = EnumRayType::Portal;
			length = Ray::MaxDistance;
		}

		void ptp(VecP start, VecP dest) noexcept
		{
			origin = start;
			setDestination(dest);
		}

		void setDestination(VecP dest) noexcept
		{
			dir = pl::assert_valid( (dest - origin).as_dir() );
			length = pl::assert_valid( dir /olength, valid::Length );
			dir = pl::assert_valid( dir /olnormalize/ length );
		}

		void operator *= (const Matrix &m) noexcept
		{
			origin = pl::assert_valid( m * origin );
			dir = dirSafeTransform(dir, m);
		}
	};
}
