/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Scene_fwd.hpp"
#include "RayTracer_fwd.hpp"
#include "Film_fwd.hpp"
#include "Sampling_fwd.hpp"
#include "Integrator_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"
#include "Statistics.hpp"

namespace PhotoRT
{
	namespace stats
	{
		using AccumulatorPath = StatsAccumulator<
			StatCounter,
			StatMin, StatMax, StatAvg, StatCount
		>;

		using AccumulatorShadowRays = StatsAccumulator<
			StatCounter,
			StatSum
		>;

		using AccumulatorShadowRaysOccluded = StatsAccumulator<
			StatCounter,
			StatSum
		>;

		using AccumulatorZeroContrib = StatsAccumulator<
			StatCounter,
			StatCount
		>;

		namespace collection
		{
			PL_MAKE_HANA_TAG(Sample)
			PL_MAKE_HANA_TAG(ShadowRays)
			PL_MAKE_HANA_TAG(Path)
			PL_MAKE_HANA_TAG(ShadowRaysOccluded)
			PL_MAKE_HANA_TAG(ZeroContribPath)
		} // namespace collection
	} // namespace stats


	class IIntegrator
	{
	public:
		virtual ~IIntegrator() = default;

		virtual StatsAccumulatorList getStats() const = 0;
		virtual void resetStats() = 0;
		virtual void init(const Scene &scene) = 0;
		virtual void initRNG(Sampler &sampler) = 0;
		virtual void setPhotonBuffers(std::span<const EntityPtr<ContributionBufferLight>> photonBuffers) = 0;
	};

	class IIntegratorFactory
	{
	public:
		virtual ~IIntegratorFactory() = default;

		virtual void init(const Scene &scene, TracerParams &tracer) = 0;
		virtual std::shared_ptr<IIntegrator> createIntegrator() const = 0;
	};

	class IIntegratorTile : public virtual IIntegrator
	{
	public:
		virtual ~IIntegratorTile() = default;

		virtual void integrate(PixelSampleTileLayers &samples, TracerParams &tracer) = 0;


	};

	class IIntegratorPhoton : public virtual IIntegrator
	{
	public:
		virtual ~IIntegratorPhoton() = default;

		virtual void integrate(Real rayImportance, Real rayDistributionFactor, u32 nbSamples, PhotonSampleList &photonSamples, TracerParams &tracer) = 0;

	};


	class IntegratorBase : public virtual IIntegrator
	{
	public:
		virtual ~IntegratorBase() = default;

		virtual StatsAccumulatorList getStats() const override { return {}; }
		virtual void resetStats() override {}

		virtual void init(const Scene &scene) override { mScene = &scene; }
		virtual void initRNG(Sampler &) override {}

	protected:
		const Scene *mScene = nullptr;
	};

}
