/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "RenderEntityImportExport_fwd.hpp"

#include "Base.hpp"
#include "RenderEntityBase.hpp"
#include "Parameter.hpp"

#include <fstream>

namespace PhotoRT
{
	class IEntityImportExport
	{
	public:
		virtual ~IEntityImportExport() = default;

		[[nodiscard]] virtual std::string_view getImportExporterName() const noexcept = 0;

		[[nodiscard]] virtual ParameterTree getConfig() const = 0;
		virtual void setConfig(const ParameterTree &params) = 0;

		[[nodiscard]] virtual bool canImport(std::string_view mimeType) const noexcept = 0;
		[[nodiscard]] virtual bool canExport(std::string_view mimeType) const noexcept = 0;
		[[nodiscard]] virtual std::string getEntityType(std::string_view mimeType) const = 0;

		[[nodiscard]] virtual bool isStreamable(std::string_view mimeType) const noexcept = 0;

		[[nodiscard]] virtual ExtensionMimeTypeMap getMimeTypesImport() const = 0;
		[[nodiscard]] virtual ExtensionMimeTypeMap getMimeTypesExport() const = 0;
		[[nodiscard]] virtual std::vector<std::string> getGenericTypes() const = 0;

		[[nodiscard]] virtual EntityPtrBase importEntity(EntityCollection &entityCollection, std::istream &inputStream, std::string_view genericType, std::string_view mimeType, std::string id, const ParameterTree &params = {}) = 0;
		[[nodiscard]] virtual EntityPtrBase importEntity(EntityCollection &entityCollection, std::span<const std::byte> data, std::string_view genericType, std::string_view mimeType, std::string id, const ParameterTree &params = {}) = 0;
		virtual void exportEntity(EntityCollection &entityCollection, const IExportHook &entity, std::ostream &outputStream, std::string_view mimeType, const ParameterTree &params = {}) = 0;
		virtual std::vector<std::byte> exportEntity(EntityCollection &entityCollection, const IExportHook &entity, std::string_view mimeType, const ParameterTree &params = {}) = 0;

		[[nodiscard]] virtual EntityPtrBase importEntity(EntityCollection &entityCollection, const std::filesystem::path &filename, std::string_view genericType, std::string_view mimeType, std::string id, const ParameterTree &params = {}) = 0;
		virtual void exportEntity(EntityCollection &entityCollection, const IExportHook &entity, const std::filesystem::path &filename, std::string_view mimeType, const ParameterTree &params = {}) = 0;
	};

	class EntityImportExportBase : public virtual IEntityImportExport
	{
	public:
		[[nodiscard]] virtual ParameterTree getConfig() const override { return {}; }
		virtual void setConfig(const ParameterTree &) override {}

		[[nodiscard]] virtual EntityPtrBase importEntity(EntityCollection &/*entityCollection*/, std::istream &/*inputStream*/, std::string_view /*genericType*/, std::string_view /*mimeType*/, std::string /*id*/, const ParameterTree &/*params*/ = {}) override
		{
			throw Exception("Streaming is not supported.");
		}

		virtual void exportEntity(EntityCollection &/*entityCollection*/, const IExportHook &/*entity*/, std::ostream &/*outputStream*/, std::string_view /*mimeType*/, const ParameterTree &/*params*/ = {}) override
		{
			throw Exception("Streaming is not supported.");
		}

		[[nodiscard]] virtual EntityPtrBase importEntity(EntityCollection &entityCollection, std::span<const std::byte> data, std::string_view genericType, std::string_view mimeType, std::string id, const ParameterTree &params = {}) override
		{
			std::span<const char> cData{reinterpret_cast<const char *>(data.data()), data.size()};

			return pl::io::stream_from(cData, pl::io::throw_on_failure, [&](auto&& is, auto&&){
				is.imbue(Parameter::loc);
				return importEntity(entityCollection, is, genericType, mimeType, std::move(id), params);
			});
		}

		[[nodiscard]] virtual EntityPtrBase importEntity(EntityCollection &entityCollection, const std::filesystem::path &filename, std::string_view genericType, std::string_view mimeType, std::string id, const ParameterTree &params = {}) override
		{
			std::ifstream fstream(filename, std::ios_base::binary | std::ios_base::in);

			if(!fstream.good())
				throw Exception("Error while trying to open file \"" + filename.string() + "\".");

			fstream.imbue(Parameter::loc);

			return importEntity(entityCollection, fstream, genericType, mimeType, std::move(id), params);
		}

		virtual void exportEntity(EntityCollection &entityCollection, const IExportHook &entity, const std::filesystem::path &filename, std::string_view mimeType, const ParameterTree &params = {}) override
		{
			std::ofstream fstream(filename, std::ios_base::binary | std::ios_base::in);

			if(!fstream.good())
				throw Exception("Error while trying to open file \"" + filename.string() + "\".");

			fstream.imbue(Parameter::loc);

			exportEntity(entityCollection, entity, fstream, mimeType, params);
		}

		virtual std::vector<std::byte> exportEntity(EntityCollection &entityCollection, const IExportHook &entity, std::string_view mimeType, const ParameterTree &params = {}) override
		{
			std::vector<char> data;

			pl::io::stream_into(data, pl::io::throw_on_failure, [&](auto&& os, auto&&){
				os.imbue(Parameter::loc);
				exportEntity(entityCollection, entity, os, mimeType, params);
			});

			std::vector<std::byte> bytes(data.size());
			auto trRange = data | pl::static_cast_view<std::byte>;

			trRange | pl::copy(bytes.begin());

			return bytes;
		}
	};
}
