/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "ColorProfile_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"
#include "RenderEntityFactory.hpp"
#include "Distribution.hpp"

namespace PhotoRT
{
	class IColorProfile : public IDistributionMapper<Color, Color>
	#ifdef PRT_FEATURE_PACKED_COLOR
		, public IDistributionMapper<PackedColor, PackedColor>
	#endif
	{
	public:

		virtual ~IColorProfile() = default;

		template <typename T_>
		[[nodiscard]] auto &getInter() noexcept { return static_cast<IDistributionMapper<T_, T_> &>(*this); }

		template <typename T_>
		[[nodiscard]] const auto &getInter() const noexcept { return static_cast<const IDistributionMapper<T_, T_> &>(*this); }

		[[nodiscard]] auto &colorInter() noexcept { return static_cast<IDistributionMapper<Color, Color> &>(*this); }
		[[nodiscard]] const auto &colorInter() const noexcept { return static_cast<const IDistributionMapper<Color, Color> &>(*this); }

		[[nodiscard]] virtual Color inverse(Color color) const noexcept = 0;

		[[nodiscard]] virtual Color toXYZ(const Color linearRgb) const noexcept = 0;
		[[nodiscard]] virtual Color toLinearRGB(const Color xyz) const noexcept = 0;
		[[nodiscard]] virtual Color::value_type getY(const Color linearRgb) const noexcept = 0;

		#ifdef PRT_FEATURE_PACKED_COLOR
			[[nodiscard]] auto &colorPackedInter() noexcept { return static_cast<IDistributionMapper<PackedColor, PackedColor> &>(*this); }
			[[nodiscard]] const auto &colorPackedInter() const noexcept { return static_cast<const IDistributionMapper<PackedColor, PackedColor> &>(*this); }

			[[nodiscard]] virtual PackedColor inverse(const PackedColor color) const noexcept = 0;

			[[nodiscard]] virtual PackedColor toXYZ(const PackedColor linearRgb) const noexcept = 0;
			[[nodiscard]] virtual PackedColor toLinearRGB(const PackedColor xyz) const noexcept = 0;
			[[nodiscard]] virtual PackedColor::value_type getY(const PackedColor linearRgb) const noexcept = 0;
		#endif
	};

	template <template <typename> typename ProfileType_>
	class ColorProfileVirtual : public IColorProfile
	{
	public:
		ProfileType_<Color> mCp;

		[[nodiscard]] virtual Color operator() (Color color) const noexcept override
		{
			return mCp(color);
		}

		[[nodiscard]] virtual Color inverse(Color color) const noexcept override
		{
			return mCp.inverse(color);
		}

		[[nodiscard]] virtual Color toXYZ(const Color linearRgb) const noexcept override { return mCp.toXYZ(linearRgb); }
		[[nodiscard]] virtual Color toLinearRGB(const Color xyz) const noexcept override { return mCp.toLinearRGB(xyz); }
		[[nodiscard]] virtual Color::value_type getY(const Color linearRgb) const noexcept override { return mCp.getY(linearRgb); }

		#ifdef PRT_FEATURE_PACKED_COLOR
			ProfileType_<PackedColor> mCpp;
			[[nodiscard]] virtual PackedColor operator() (PackedColor color) const noexcept override
			{
				return mCpp(color);
			}

			[[nodiscard]] virtual PackedColor toXYZ(const PackedColor linearRgb) const noexcept override { return mCpp.toXYZ(linearRgb); }
			[[nodiscard]] virtual PackedColor toLinearRGB(const PackedColor xyz) const noexcept override { return mCpp.toLinearRGB(xyz); }
			[[nodiscard]] virtual PackedColor::value_type getY(const PackedColor linearRgb) const noexcept override { return mCpp.getY(linearRgb); }

			[[nodiscard]] virtual PackedColor inverse(const PackedColor color) const noexcept override
			{
				return mCpp.inverse(color);
			}
		#endif
	};

	template <typename ColorType_>
	class ColorProfileSRGB
	{
	public:
		[[nodiscard]] ColorType_ operator() (const ColorType_ color) const noexcept
		{
			auto res = pl::select(color * 12.92, color /opow/ (1 / 2.4) * 1.055 - 0.055, color <= 0.0031308);

			if constexpr(ColorType_::NbValues > 3)
				res = res /oblend<0b1000>/ color;

			return res;
		}

		[[nodiscard]] ColorType_ inverse(const ColorType_ color) const noexcept
		{
			auto res = pl::select(color * (1.0 / 12.92), ((color + 0.055) * (1.0 / 1.055)) /opow/ 2.4, color <= 0.04045);

			if constexpr(ColorType_::NbValues > 3)
				res = res /oblend<0b1000>/ color;

			return res;
		}

		[[nodiscard]] ColorType_ toXYZ(const ColorType_ linearRgb) const noexcept
		{
			return rgbToXYZ(linearRgb);
		}

		[[nodiscard]] ColorType_ toLinearRGB(const ColorType_ xyz) const noexcept
		{
			return xyzToRGB(xyz);
		}

		[[nodiscard]] typename ColorType_::value_type getY(const ColorType_ linearRgb) const noexcept
		{
			return getColorY(linearRgb);
		}
	};

	template <typename ColorType_>
	class ColorProfileAdobeRGB
	{
	public:
		using CR = typename ColorType_::value_type;

		[[nodiscard]] ColorType_ operator() (const ColorType_ color) const noexcept
		{
			auto res = pl::select(color * 12.92, color /opow/ (1 / 2.4) * 1.055 - 0.055, color <= 0.0031308);

			if constexpr(ColorType_::NbValues > 3)
				res = res /oblend<0b1000>/ color;

			return res;
		}

		[[nodiscard]] ColorType_ inverse(const ColorType_ color) const noexcept
		{
			auto res = pl::select(color * (1.0 / 12.92), ((color + 0.055) * (1.0 / 1.055)) /opow/ 2.4, color <= 0.04045);

			if constexpr(ColorType_::NbValues > 3)
				res = res /oblend<0b1000>/ color;

			return res;
		}

		[[nodiscard]] ColorType_ toXYZ(const ColorType_ linearRgb) const noexcept
		{
			const auto &ca = pl::simd::vector_access(linearRgb);
			return ColorType_{
				(CR)0.57667 * (ca[0]) + (CR)0.18556 * (ca[1]) + (CR)0.18823 * (ca[2]),
				(CR)0.29734 * (ca[0]) + (CR)0.62736 * (ca[1]) + (CR)0.07529 * (ca[2]),
				(CR)0.02703 * (ca[0]) + (CR)0.07069 * (ca[1]) + (CR)0.99134 * (ca[2]),
				(ca[3])
			};
		}

		[[nodiscard]] ColorType_ toLinearRGB(const ColorType_ xyz) const noexcept
		{
			const auto &ca = pl::simd::vector_access(xyz);
			return ColorType_{
				(CR)2.04159 * (ca[0]) - (CR)0.56501 * (ca[1]) - (CR)0.34473 * (ca[2]),
				(CR)-0.96924 * (ca[0]) + (CR)1.87597 * (ca[1]) + (CR)0.04156 * (ca[2]),
				(CR)0.01344 * (ca[0]) - (CR)0.11836 * (ca[1]) + (CR)1.01517 * (ca[2]),
				(ca[3])
			};
		}

		[[nodiscard]] CR getY(const ColorType_ linearRgb) const noexcept
		{
			return ColorType_{(CR)0.29734, (CR)0.62736, (CR)0.07529, 0} /odot/ linearRgb;
		}
	};

	PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("colorprofile-srgb", AugmentedColorProfileSRGB, ColorProfileVirtual<ColorProfileSRGB>)
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("colorprofile-adobergb", AugmentedColorProfileAdobeRGB, ColorProfileVirtual<ColorProfileAdobeRGB>)
}
