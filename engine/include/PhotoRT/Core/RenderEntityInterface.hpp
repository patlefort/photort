/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "RenderEntityInterface_fwd.hpp"

#include "Base.hpp"
#include "RenderEntityBase.hpp"
#include "Parameterized.hpp"

#include <experimental/propagate_const>

#include <patlib/io.hpp>

namespace PhotoRT
{
	template <typename T_>
	using EntityPtr = std::shared_ptr<T_>;

	using EntityPtrBase = EntityPtr<IRenderEntity>;

	template <typename T_>
	using EntityPtrPC = std::experimental::propagate_const<std::shared_ptr<T_>>;

	// Base interface for most objects in PhotoRT
	class IRenderEntity : public pl::io::ISerializable<>, public IParameterized, public virtual IExportHook
	{
	public:

		virtual ~IRenderEntity() = default;

		[[nodiscard]] virtual std::string_view getEntityType() const noexcept = 0;
		//virtual void setEntityType(std::string v) = 0;

		[[nodiscard]] virtual std::string_view getEntityId() const noexcept = 0;
		virtual void setEntityId(std::string v) = 0;

		[[nodiscard]] virtual std::string_view getEntityName() const noexcept = 0;
		virtual void setEntityName(std::string v) = 0;

		[[nodiscard]] virtual EntityCollection *getEntityCollection() const noexcept = 0;
		virtual void setEntityCollection(EntityCollection *v) = 0;

		[[nodiscard]] virtual RenderEntitySocketDescMap getSockets() const = 0;
		virtual bool connect(std::string_view socket, const EntityPtrBase &entity) = 0;
		[[nodiscard]] virtual EntityPtrBase getConnected(std::string_view socket) = 0;

		[[nodiscard]] virtual bool hasData(bool ipcStream = false) const noexcept = 0;
		[[nodiscard]] virtual bool canHaveData() const noexcept = 0;
	};

	template <typename ToType_, typename T_>
	auto DynEntityCast(const EntityPtr<T_> &e)
		{ return std::dynamic_pointer_cast<ToType_>(e); }

	template <typename ToType_, typename T_>
	auto StaticEntityCast(const EntityPtr<T_> &e)
		{ return std::static_pointer_cast<ToType_>(e); }
}