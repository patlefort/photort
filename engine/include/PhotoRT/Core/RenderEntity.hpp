/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Parser_fwd.hpp"
#include "RenderEntity_fwd.hpp"

#include "Base.hpp"
#include "RenderEntityBase.hpp"
#include "RenderEntityInterface.hpp"
#include "RenderEntityCollection.hpp"
#include "Parameter.hpp"

#include <patlib/crtp.hpp>

namespace PhotoRT
{
	// Base class for most objects in PhotoRT
	class PHOTORT_API RenderEntity : public IRenderEntity
	{
	protected:
		std::string entityId, entityName;
		EntityCollection *entityCollection = nullptr;

	public:

		RenderEntity();
		virtual ~RenderEntity();

		[[nodiscard]] virtual std::string_view getEntityId() const noexcept final { return entityId; }
		virtual void setEntityId(std::string v) override { entityId = std::move(v); }

		[[nodiscard]] virtual std::string_view getEntityName() const noexcept final { return entityName; }
		virtual void setEntityName(std::string v) override { entityName = std::move(v); }

		[[nodiscard]] virtual EntityCollection *getEntityCollection() const noexcept final { return entityCollection; }
		virtual void setEntityCollection(EntityCollection *v) override { entityCollection = v; }

		virtual void setParameters(const ParameterTree &params) override
		{
			if(entityCollection)
				entityCollection->releaseAllEntityReferences(*this);

			params.getValueIfExists("name", entityName);
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool /*deep*/ = false) const override
		{
			ParameterTree params;

			if(!getEntityName().empty())
				params["name"].value = entityName;

			return params;
		}

		[[nodiscard]] virtual RenderEntitySocketDescMap getSockets() const override
		{
			return {};
		}

		virtual bool connect(std::string_view /*socket*/, const EntityPtrBase &) override
		{
			return false;
		}

		[[nodiscard]] virtual EntityPtrBase getConnected(std::string_view /*socket*/) override { return nullptr; }

		template <typename EntityType_, typename... Args_>
		[[nodiscard]] EntityPtr<EntityType_> addReference(type_c<EntityType_> type, Args_&&... args)
			{ return entityCollection->addEntityReferenceCreate(type, *this, PL_FWD(args)...); }

		template <typename EntityType_, typename... Args_>
		void addReferenceInto(EntityPtr<EntityType_> &ent, Args_&&... args)
			{ entityCollection->addEntityReferenceInto(*this, ent, PL_FWD(args)...); }

		void removeReference(const EntityPtrBase &entity)
			{ entityCollection->releaseEntityReference(*this, entity); }

		template <typename EntityType_>
		void removeReference(const EntityPtr<EntityType_> &entity)
			{ entityCollection->releaseEntityReference(*this, DynEntityCast<IRenderEntity>(entity)); }

		template <typename EntityType_>
		void addParamReference(EntityPtr<EntityType_> &ref, const ParameterTree &params, std::string_view paramName)
		{
			if(const auto p = params.findParam(paramName); p && !p->value.empty())
				addReferenceInto(ref, *p);
			else
				ref = nullptr;
		}

		template <typename EntityType_>
		[[nodiscard]] EntityPtr<EntityType_> addParamReference(type_c<EntityType_> type, const ParameterTree &params, std::string_view paramName)
		{
			if(const auto p = params.findParam(paramName); p && !p->value.empty())
				return addReference(type, *p);
			else
				return nullptr;
		}

		virtual void serialize(std::ostream &) const override {}
		virtual void unserialize(std::istream &) override {}

		[[nodiscard]] virtual bool hasData(bool /*ipcStream*/ = false) const noexcept override { return false; }
		[[nodiscard]] virtual bool canHaveData() const noexcept override { return false; }

		template <pl::CLogTag Category_ = pl::logtag::message_t>
		[[nodiscard]] auto log(const hn::basic_type<Category_> tag = {}) const
			{ return pl::log(entityId, tag); }

		template <pl::CLogTag Category_, typename... Fargs_>
		void log(const hn::basic_type<Category_> tag, fmt::format_string<Fargs_...> format, Fargs_&&... args) const
			{ pl::log(entityId, tag, format, PL_FWD(args)...); }

		template <typename... Fargs_>
		void log(fmt::format_string<Fargs_...> format, Fargs_&&... args) const
			{ pl::log(entityId, format, PL_FWD(args)...); }
	};

	[[nodiscard]] inline uf32 getSocketIndex(std::string_view socket, std::string_view prefix)
	{
		return pl::io::from_string_noloc<uf32>(socket.substr(0, prefix.size()));
	}

	namespace ImplEntity
	{
		void doConnect(auto &inputEntity, std::string_view socket, const EntityPtrBase &entity)
		{
			const auto e = inputEntity.getConnected(socket);
			const auto collection = pl::assert_valid(inputEntity.getEntityCollection());

			if(!inputEntity.connect(socket, entity))
				throw ExceptionEntityCollection(
					"Invalid socket connection with "
					+ (entity ? "entity \"" + entity->getEntityType() + "/" + entity->getEntityId() + "\"" : " no entity")
					+ " on socket \"" + socket + "\" in entity \"" + inputEntity.getEntityType() + "/" + inputEntity.getEntityId() + "\".");

			if(entity)
				pl::log("PhotoRT", "Connected entity {}/{} to socket {} on entity {}/{}.",
					pl::io::quoted{entity->getEntityId()},
					pl::io::quoted{entity->getEntityType()},
					pl::io::quoted{socket},
					pl::io::quoted{inputEntity.getEntityType()},
					pl::io::quoted{inputEntity.getEntityId()});
			else
				pl::log("PhotoRT", "Disconnected entity from socket {} on entity {}/{}.",
					pl::io::quoted{socket},
					pl::io::quoted{inputEntity.getEntityType()},
					pl::io::quoted{inputEntity.getEntityId()});

			if(e)
				collection->releaseEntityReference(inputEntity, e);

			if(entity)
				collection->addEntityReference(inputEntity, entity);
		}

		void doConnect(auto &inputEntity, std::string_view socket, const Parameter &param)
		{
			if(!param.isEntity())
				throw ExceptionEntityCollection("Invalid parameter given for entity reference.");

			auto [type, id] = param.getEntityType();

			doConnect( inputEntity, socket, pl::assert_valid(inputEntity.getEntityCollection())->createEntity(type, std::move(id), param.childParams.value_or(ParameterTree{})) );
		}

		void doConnect(auto &inputEntity, std::string_view socket, std::string_view type, std::string id, const ParameterTree &params)
		{
			doConnect( inputEntity, socket, pl::assert_valid(inputEntity.getEntityCollection())->createEntity(type, std::move(id), params) );
		}

		void doConnect(auto &inputEntity, std::string_view socket, const ParameterTree &params, std::string_view paramName)
		{
			if(const auto p = params.findParam(paramName); p)
				doConnect(inputEntity, socket, *p);
			else
				doConnect(inputEntity, socket, EntityPtrBase{});
		}

		void doConnect(auto &inputEntity, std::string_view socket, const ParameterTree &params)
		{
			doConnect(inputEntity, socket, params, socket);
		}

		auto doConnectChain(auto &inputEntity, auto&&... args)
		{
			doConnect(inputEntity, PL_FWD(args)...);

			return [&](auto&&... cargs){ return doConnectChain(inputEntity, PL_FWD(cargs)...); };
		}
	} // namespace ImplEntity

	[[nodiscard]] auto connectEntity(auto &inputEntity)
	{
		return [&](auto&&... args){ return ImplEntity::doConnectChain(inputEntity, PL_FWD(args)...); };
	}

	#define PRT_TRY_CONNECT_SOCKET(socketStr, fc, type) \
		if(socket == socketStr) \
		{ \
			fc(DynEntityCast<type>(entity)); \
			return true; \
		}

	#define PRT_TRY_GETCONNECTED_SOCKET(socketStr, fc) \
		if(socket == socketStr) \
			return DynEntityCast<IRenderEntity>(fc());

	// Augment a class to implement RenderEntity
	template <typename BaseClass_>
	class RenderEntityAugment : public BaseClass_, public RenderEntity
	{
	protected:

		PL_CRTP_DERIVED_VIR(BaseClass_)

	public:
		using BaseClass = BaseClass_;

		using BaseClass_::BaseClass_;
	};
}
