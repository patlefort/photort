/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Distribution_fwd.hpp"

#include "Base.hpp"
#include "RenderEntityInterface.hpp"

#include <patlib/multi_tagged_span.hpp>

#include <bit>

namespace PhotoRT
{
	template <typename InType_, typename OutType_>
	class IDistributionMapper
	{
	public:
		virtual ~IDistributionMapper() = default;

		using InType = InType_;
		using OutType = OutType_;

		[[nodiscard]] virtual OutType_ operator() (InType_ u) const noexcept = 0;
	};

	template <typename InType_, typename OutType_>
	class IDistributionMapper2D
	{
	public:
		virtual ~IDistributionMapper2D() = default;

		using InType = InType_;
		using OutType = OutType_;

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v) const noexcept = 0;
	};

	template <typename InType_, typename OutType_>
	class IDistributionMapper3D
	{
	public:
		virtual ~IDistributionMapper3D() = default;

		using InType = InType_;
		using OutType = OutType_;

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v, InType_ w) const noexcept = 0;
	};

	template <typename T_>
	class DistributionMapperVirtual : public T_, public virtual IDistributionMapper<typename T_::InType, typename T_::OutType>
	{
		[[nodiscard]] auto &typecast() noexcept { return static_cast<T_ &>(*this); }
		[[nodiscard]] const auto &typecast() const noexcept { return static_cast<const T_ &>(*this); }

	public:
		using T_::T_;

		[[nodiscard]] virtual typename T_::OutType operator() (typename T_::InType u) const noexcept override
			{ return typecast()(u); }
	};

	template <typename T_>
	class DistributionMapperVirtual2D : public T_, public virtual IDistributionMapper2D<typename T_::InType, typename T_::OutType>
	{
		[[nodiscard]] auto &typecast() noexcept { return static_cast<T_ &>(*this); }
		[[nodiscard]] const auto &typecast() const noexcept { return static_cast<const T_ &>(*this); }

	public:
		using T_::T_;

		[[nodiscard]] virtual typename T_::OutType operator() (typename T_::InType u, typename T_::InType v) const noexcept override
			{ return typecast()(u, v); }
	};

	template <typename T_>
	class DistributionMapperVirtual3D : public T_, public virtual IDistributionMapper3D<typename T_::InType, typename T_::OutType>
	{
		[[nodiscard]] auto &typecast() noexcept { return static_cast<T_ &>(*this); }
		[[nodiscard]] const auto &typecast() const noexcept { return static_cast<const T_ &>(*this); }

	public:
		using T_::T_;

		[[nodiscard]] virtual typename T_::OutType operator() (typename T_::InType u, typename T_::InType v, typename T_::InType w) const noexcept override
			{ return typecast()(u, v, w); }
	};

	template <typename InType_, typename OutType_>
	class DistributionMapper1Dto2D : public virtual IDistributionMapper<InType_, OutType_>, public virtual IDistributionMapper2D<InType_, OutType_>
	{
	public:
		virtual ~DistributionMapper1Dto2D() = default;

		using IDistributionMapper<InType_, OutType_>::operator ();

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v) const noexcept override
		{
			return (*this)(u) * (*this)(v);
		}
	};

	template <typename InType_, typename OutType_>
	class DistributionMapper1Dto3D : public virtual IDistributionMapper<InType_, OutType_>, public IDistributionMapper3D<InType_, OutType_>
	{
	public:
		virtual ~DistributionMapper1Dto3D() = default;

		using IDistributionMapper<InType_, OutType_>::operator ();

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v, InType_ w) const noexcept override
		{
			return (*this)(u) * (*this)(v) * (*this)(w);
		}
	};


	template <typename Type_>
	class DistributionMapperMultiBase
	{
	public:

		virtual ~DistributionMapperMultiBase() = default;

		std::vector<EntityPtr<const Type_>> mMappers;

		auto &operator<< (EntityPtr<const Type_> mapper)
		{
			mMappers.push_back(std::move(mapper));

			return *this;
		}
	};

	template <typename InType_, typename OutType_, typename Operator_>
	class DistributionMapperMulti :
		public DistributionMapperMultiBase<IDistributionMapper<InType_, OutType_>>,
		public DistributionMapperMultiBase<IDistributionMapper2D<InType_, OutType_>>,
		public DistributionMapperMultiBase<IDistributionMapper3D<InType_, OutType_>>,
		public virtual IDistributionMapper<InType_, OutType_>,
		public virtual IDistributionMapper2D<InType_, OutType_>,
		public virtual IDistributionMapper3D<InType_, OutType_>
	{
	public:

		using maps1d = DistributionMapperMultiBase<IDistributionMapper<InType_, OutType_>>;
		using maps2d = DistributionMapperMultiBase<IDistributionMapper2D<InType_, OutType_>>;
		using maps3d = DistributionMapperMultiBase<IDistributionMapper3D<InType_, OutType_>>;

		using maps1d::operator<<;
		using maps2d::operator<<;
		using maps3d::operator<<;

		virtual ~DistributionMapperMulti() = default;

		[[nodiscard]] virtual OutType_ operator() (InType_ u) const noexcept override
		{
			OutType_ value{};

			auto it = maps1d::mMappers.begin();
			if(it != maps1d::mMappers.end())
			{
				value = (**it)(u);
				++it;
			}

			for(;it != maps1d::mMappers.end(); ++it)
				value = mOp(value, (**it)(u));

			return value;
		}

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v) const noexcept override
		{
			OutType_ value{};

			auto it = maps2d::mMappers.begin();
			if(it != maps2d::mMappers.end())
			{
				value = (**it)(u, v);
				++it;
			}

			for(;it != maps2d::mMappers.end(); ++it)
				value = mOp(value, (**it)(u, v));

			return value;
		}

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v, InType_ w) const noexcept override
		{
			OutType_ value{};

			auto it = maps3d::mMappers.begin();
			if(it != maps3d::mMappers.end())
			{
				value = (**it)(u, v, w);
				++it;
			}

			for(;it != maps3d::mMappers.end(); ++it)
				value = mOp(value, (**it)(u, v, w));

			return value;
		}

	protected:
		Operator_ mOp;
	};

	template <typename Type_>
	class DistributionMapperMultiIO : public DistributionMapperMultiBase<IDistributionMapper<Type_, Type_>>, public virtual IDistributionMapper<Type_, Type_>, public virtual IDistributionMapper2D<Type_, Type_>, public virtual IDistributionMapper3D<Type_, Type_>
	{
	public:
		using DistributionMapperMultiBase<IDistributionMapper<Type_, Type_>>::DistributionMapperMultiBase;
		using DistributionMapperMultiBase<IDistributionMapper<Type_, Type_>>::mMappers;
		using DistributionMapperMultiBase<IDistributionMapper<Type_, Type_>>::operator<<;

		virtual ~DistributionMapperMultiIO() = default;

		[[nodiscard]] virtual Type_ operator() (Type_ u) const noexcept override
		{
			for(const auto &m : mMappers)
				u = (*m)(u);

			return u;
		}

		[[nodiscard]] virtual Type_ operator() (Type_ u, Type_ v) const noexcept override
		{
			for(const auto &m : mMappers)
			{
				u = (*m)(u);
				v = (*m)(v);
			}

			return u * v;
		}

		[[nodiscard]] virtual Type_ operator() (Type_ u, Type_ v, Type_ w) const noexcept override
		{
			for(const auto &m : mMappers)
			{
				u = (*m)(u);
				v = (*m)(v);
				w = (*m)(w);
			}

			return u * v * w;
		}
	};

	template <typename InType_, typename OutType_, template<typename, typename> class DistType_, template<typename, typename> class DistTypeNext_>
	class DistributionMapperChainBase
	{
	public:
		DistributionMapperChainBase() = default;
		DistributionMapperChainBase(EntityPtr<const DistType_<InType_, OutType_>> map, EntityPtr<const DistTypeNext_<InType_, OutType_>> next) :
			mMap(std::move(map)), mNext(std::move(next))
		{}

		EntityPtr<const DistType_<InType_, OutType_>> mMap;
		EntityPtr<const DistTypeNext_<OutType_, OutType_>> mNext;
	};

	template <typename InType_, typename OutType_>
	class DistributionMapperChain1D : public DistributionMapperChainBase<InType_, OutType_, IDistributionMapper, IDistributionMapper>, public virtual IDistributionMapper<InType_, OutType_>
	{
	public:

		[[nodiscard]] virtual OutType_ operator() (InType_ u) const noexcept override
		{
			const auto res = (*this->mMap)(u);
			return this->mNext ? (*this->mNext)(res) : res;
		}

	};

	template <typename InType_, typename OutType_>
	class DistributionMapperChain2D : public DistributionMapperChainBase<InType_, OutType_, IDistributionMapper2D, IDistributionMapper>, public virtual IDistributionMapper2D<InType_, OutType_>
	{
	public:

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v) const noexcept override
		{
			const auto res = (*this->mMap)(u, v);
			return this->mNext ? (*this->mNext)(res) : res;
		}

	};

	template <typename InType_, typename OutType_>
	class DistributionMapperChain3D : public DistributionMapperChainBase<InType_, OutType_, IDistributionMapper3D, IDistributionMapper>, public virtual IDistributionMapper3D<InType_, OutType_>
	{
	public:

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v, InType_ w) const noexcept override
		{
			const auto res = (*this->mMap)(u, v, w);
			return this->mNext ? (*this->mNext)(res) : res;
		}

	};

	template <typename InType_, typename OutType_>
	class DistributionMapperChainIO : public DistributionMapperChainBase<InType_, OutType_, IDistributionMapper2D, IDistributionMapper2D>, public virtual IDistributionMapper<InType_, OutType_>, public virtual IDistributionMapper2D<InType_, OutType_>
	{
	public:

		[[nodiscard]] virtual OutType_ operator() (InType_ u) const noexcept override
		{
			const auto res = (*this->mMap)({}, u);
			return this->mNext ? (*this->mNext)(u, res) : res;
		}

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v) const noexcept override
		{
			const auto res = (*this->mMap)(u, v);
			return this->mNext ? (*this->mNext)(v, res) : res;
		}

	};

	template <typename InType_, typename OutType_, typename Operator_>
	class DistributionMapperAdaptor : public DistributionMapperChainBase<InType_, OutType_, IDistributionMapper, IDistributionMapper>, public virtual IDistributionMapper<InType_, OutType_>, public virtual IDistributionMapper2D<InType_, OutType_>, public virtual IDistributionMapper3D<InType_, OutType_>
	{
	public:

		[[nodiscard]] virtual OutType_ operator() (InType_ u) const noexcept override
		{
			const auto res = (OutType_)(*this->mMap)(u);
			return this->mNext ? (*this->mNext)(res) : res;
		}

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v) const noexcept override
		{
			const auto res = (OutType_)op((*this->mMap)(u), (*this->mMap)(v));
			return this->mNext ? (*this->mNext)(res) : res;
		}

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v, InType_ w) const noexcept override
		{
			const auto res = (OutType_)op((OutType_)op((*this->mMap)(u), (*this->mMap)(v)), (*this->mMap)(w));
			return this->mNext ? (*this->mNext)(res) : res;
		}

	protected:
		Operator_ op;
	};

	template <typename InType_, typename OutType_>
	class DistributionMapperScaler1D : public virtual IDistributionMapper<InType_, OutType_>
	{
	public:

		DistributionMapperScaler1D() = default;
		DistributionMapperScaler1D(InType_ scaleu) noexcept
			: mScaleu{scaleu} {}

		EntityPtr<const IDistributionMapper<InType_, OutType_>> mMap;
		InType_ mScaleu{1};

		[[nodiscard]] virtual OutType_ operator() (InType_ u) const noexcept override
		{
			return (*this->mMap)(u * mScaleu);
		}

	};

	template <typename InType_, typename OutType_>
	class DistributionMapperScaler2D : public virtual IDistributionMapper2D<InType_, OutType_>
	{
	public:

		DistributionMapperScaler2D() = default;
		DistributionMapperScaler2D(InType_ scaleu, InType_ scalev = 1) noexcept
			: mScaleu{scaleu}, mScalev{scalev} {}

		EntityPtr<const IDistributionMapper2D<InType_, OutType_>> mMap;
		InType_ mScaleu{1}, mScalev{1};

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v) const noexcept override
		{
			return (*this->mMap)(u * mScaleu, v * mScalev);
		}

	};

	template <typename InType_, typename OutType_>
	class DistributionMapperScaler3D : public virtual IDistributionMapper3D<InType_, OutType_>
	{
	public:

		DistributionMapperScaler3D() = default;
		DistributionMapperScaler3D(InType_ scaleu, InType_ scalev = 1, InType_ scalew = 1) noexcept
			: mScaleu{scaleu}, mScalev{scalev}, mScalew{scalew} {}

		EntityPtr<const IDistributionMapper3D<InType_, OutType_>> mMap;
		InType_ mScaleu{1}, mScalev{1}, mScalew{1};

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v, InType_ w) const noexcept override
		{
			return (*this->mMap)(u * mScaleu, v * mScalev, w * mScalew);
		}

	};

	template <typename InType_, typename OutType_>
	class DistributionMapperClamper : public virtual IDistributionMapper<InType_, OutType_>, public DistributionMapper1Dto2D<InType_, OutType_>, public DistributionMapper1Dto3D<InType_, OutType_>
	{
	public:

		InType_ mMinimum{0}, mMaximum{1};

		DistributionMapperClamper() = default;
		DistributionMapperClamper(InType_ minimum, InType_ maximum) noexcept
		{
			mMinimum = minimum;
			mMaximum = maximum;
		}

		[[nodiscard]] virtual OutType_ operator() (InType_ u) const noexcept override
		{
			return pl::plclamp(u, mMinimum, mMaximum);
		}
	};

	template <typename InType_, typename OutType_>
	class DistributionMapper2D : public IDistributionMapper2D<InType_, OutType_>
	{
	public:
		DistributionMapper2D() = default;
		DistributionMapper2D(EntityPtr<const IDistributionMapper<InType_, OutType_>> map1, EntityPtr<const IDistributionMapper<InType_, OutType_>> map2)
			: mMap1{std::move(map1)}, mMap2{std::move(map2)} {}

		EntityPtr<const IDistributionMapper<InType_, OutType_>> mMap1, mMap2;

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v) const noexcept override
		{
			return (*mMap1)(u) * (*mMap2)(v);
		}
	};

	template <typename InType_, typename OutType_>
	class DistributionMapper3D : public IDistributionMapper3D<InType_, OutType_>
	{
	public:
		DistributionMapper3D() = default;
		DistributionMapper3D(EntityPtr<const IDistributionMapper<InType_, OutType_>> map1, EntityPtr<const IDistributionMapper<InType_, OutType_>> map2, EntityPtr<const IDistributionMapper<InType_, OutType_>> map3)
			: mMap1{std::move(map1)}, mMap2{std::move(map2)}, mMap3{std::move(map3)} {}

		EntityPtr<const IDistributionMapper<InType_, OutType_>> mMap1, mMap2, mMap3;

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v, InType_ w) const noexcept override
		{
			return (*mMap1)(u) * (*mMap2)(v) * (*mMap3)(w);
		}
	};


	template <typename InType_, typename OutType_>
	class DistributionMapperLinear : public virtual IDistributionMapper<InType_, OutType_>, public DistributionMapper1Dto2D<InType_, OutType_>, public DistributionMapper1Dto3D<InType_, OutType_>
	{
	public:
		DistributionMapperLinear() = default;
		DistributionMapperLinear(InType_ size) noexcept
		{
			setConstant(size);
		}

		InType_ mSize, mStart{};

		[[nodiscard]] virtual OutType_ operator() (InType_ u) const noexcept override
		{
			return u * mSize + mStart;
		}

		void setConstant(InType_ size, InType_ start = {}) noexcept
		{
			mSize = size;
			mStart = start;
		}
	};

	template <typename InType_, typename OutType_>
	class DistributionMapperConstant : public virtual IDistributionMapper<InType_, OutType_>, public virtual IDistributionMapper2D<InType_, OutType_>, public virtual IDistributionMapper3D<InType_, OutType_>
	{
	public:
		DistributionMapperConstant() = default;
		DistributionMapperConstant(OutType_ v) noexcept
			: mValue{v}
		{}

		InType_ mValue;

		[[nodiscard]] virtual OutType_ operator() (InType_) const noexcept override
		{
			return mValue;
		}

		[[nodiscard]] virtual OutType_ operator() (InType_, InType_) const noexcept override
		{
			return mValue;
		}

		[[nodiscard]] virtual OutType_ operator() (InType_, InType_, InType_) const noexcept override
		{
			return mValue;
		}
	};

	template <typename InType_, typename OutType_>
	class DistributionMapperGaussian : public virtual IDistributionMapper<InType_, OutType_>, public DistributionMapper1Dto2D<InType_, OutType_>, public DistributionMapper1Dto3D<InType_, OutType_>
	{
	public:
		DistributionMapperGaussian() = default;
		DistributionMapperGaussian(InType_ width, InType_ height, InType_ position, InType_ alpha) noexcept
		{
			setGaussian(width, height, position, alpha);
		}

		[[nodiscard]] virtual OutType_ operator() (InType_ u) const noexcept override
		{
			return mHeight * (( -mAlpha * pl::pow2(u - mPosition) ) /oexp - mPrecalcWidth );
		}

		void setGaussian(InType_ width, InType_ height, InType_ position, InType_ alpha) noexcept
		{
			mHeight = height;
			mPosition = position;
			mWidth = width;
			mAlpha = alpha;

			mPrecalcWidth = (-alpha * width * width) /oexp;
		}

		[[nodiscard]] auto getHeight() const noexcept { return mHeight; }
		[[nodiscard]] auto getPosition() const noexcept { return mPosition; }
		[[nodiscard]] auto getWidth() const noexcept { return mWidth; }
		[[nodiscard]] auto getAlpha() const noexcept { return mAlpha; }

	protected:
		InType_ mHeight, mPosition{}, mWidth, mAlpha;
		InType_ mPrecalcWidth;
	};

	template <typename InType_, typename OutType_>
	class DistributionMapperChecker : public virtual IDistributionMapper<InType_, OutType_>, public virtual IDistributionMapper2D<InType_, OutType_>, public virtual IDistributionMapper3D<InType_, OutType_>
	{
	public:

		[[nodiscard]] virtual OutType_ operator() (InType_ u) const noexcept override
		{
			//return pl::mod(u * InType_(2.0), InType_(2.0)) >= InType_(1.0) ? 1.0 : 0.0;
			return u /oround >= u ? 1 : 0;
		}

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v) const noexcept override
		{
			return (*this)( (*this)(u) * InType_(0.5) + v );
		}

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v, OutType_ w) const noexcept override
		{
			return (*this)(
				(*this)( (*this)(u) * InType_(0.5) + v ) * InType_(0.5) + w
			);
		}
	};

	template <typename InType_, typename OutType_, typename Operator_>
	class DistributionMapperCompare : public virtual IDistributionMapper<InType_, OutType_>, public DistributionMapper1Dto2D<InType_, OutType_>, public DistributionMapper1Dto3D<InType_, OutType_>
	{
	public:

		InType_ mCompare;
		OutType_ mTrue, mFalse;

		[[nodiscard]] virtual OutType_ operator() (InType_ u) const noexcept override
		{
			return mOp(u, mCompare) ? mTrue : mFalse;
		}

	protected:
		Operator_ mOp;
	};

	template <typename InType_, typename OutType_>
	class DistributionMapperConcentricDisk : public IDistributionMapper2D<InType_, OutType_>
	{
	public:
		DistributionMapperConcentricDisk() = default;
		DistributionMapperConcentricDisk(InType_ size) noexcept : mSize(size) {}

		InType_ mSize{1};

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v) const noexcept override
		{
			const auto s = uniformSampleDiskPoint(pl::values<InType_, 2>{u, v}) * mSize;

			return OutType_{s[0], s[1], 0, 0};
		}

		void setConcentricDisk(InType_ size) noexcept
		{
			mSize = size;
		}

	};

	template <typename InType_, typename OutType_>
	class DistributionMapperLanczosSinc : public virtual IDistributionMapper<InType_, OutType_>, public DistributionMapper1Dto2D<InType_, OutType_>, public DistributionMapper1Dto3D<InType_, OutType_>
	{
	public:
		DistributionMapperLanczosSinc() = default;
		DistributionMapperLanczosSinc(InType_ width, InType_ height, InType_ position, InType_ tau) noexcept
		{
			setLanczos(width, height, position, tau);
		}

		[[nodiscard]] virtual OutType_ operator() (InType_ u) const noexcept override
		{
			u = (u - mPosition) * mWidthInv;

			if(u < constants::epsilon<InType_>)
				return mHeight;
			if(u > 1)
				return 0;

			u *= std::numbers::pi_v<InType_>;

			return mHeight * pl::plsin(u * mTau) / (u * mTau) * pl::plsin(u) / u;
		}

		void setLanczos(InType_ width, InType_ height, InType_ position, InType_ tau) noexcept
		{
			mHeight = height;
			mPosition = position;
			mWidth = width;
			mTau = tau;

			mWidthInv = InType_(1) / width;
		}

		[[nodiscard]] auto getHeight() const noexcept { return mHeight; }
		[[nodiscard]] auto getPosition() const noexcept { return mPosition; }
		[[nodiscard]] auto getWidth() const noexcept { return mWidth; }
		[[nodiscard]] auto getTau() const noexcept { return mTau; }

	protected:
		InType_ mWidth, mHeight, mPosition{}, mTau;
		InType_ mWidthInv;
	};

	template <typename InType_, typename OutType_>
	class DistributionMapperCosWave : public virtual IDistributionMapper<InType_, OutType_>, public DistributionMapper1Dto2D<InType_, OutType_>, public DistributionMapper1Dto3D<InType_, OutType_>
	{
	public:
		DistributionMapperCosWave() = default;

		DistributionMapperCosWave(InType_ strength, InType_ start, InType_ position) noexcept :
			mStrength{strength}, mStart{start}, mPosition{position} {}

		InType_ mStrength{1}, mStart{0}, mPosition{0};

		[[nodiscard]] virtual OutType_ operator() (InType_ u) const noexcept override
		{
			return pl::plcos(u + mPosition) * mStrength + mStart;
		}

	};


	// From http://www.flipcode.com/archives/Perlin_Distribution_Class.shtml and modified. Link no longer exists.
	template <typename InType_, typename OutType_>
	class DistributionMapperPerlin3D : public virtual IDistributionMapper<InType_, OutType_>, public virtual IDistributionMapper2D<InType_, OutType_>, public virtual IDistributionMapper3D<InType_, OutType_>
	{
	public:

		using IType = if32;//typename IntType<sizeof(InType_)>::TypeSigned;

		struct GradientContainer
		{
			std::array<OutType_, 3> g;
		};

		DistributionMapperPerlin3D() = default;

		DistributionMapperPerlin3D(IType tableSize, EntityPtr<IRNGInt> randomiz)
		{
			init(tableSize, std::move(randomiz));
		}

		DistributionMapperPerlin3D(const DistributionMapperPerlin3D &other)
		{
			initFromOther(other);
		}

		DistributionMapperPerlin3D(DistributionMapperPerlin3D &&other) = default;

		DistributionMapperPerlin3D &operator=(const DistributionMapperPerlin3D &other)
		{
			initFromOther(other);
			return *this;
		}

		DistributionMapperPerlin3D &operator=(DistributionMapperPerlin3D &&other) = default;

		[[nodiscard]] virtual OutType_ operator() (InType_ u) const noexcept override
		{
			return (*this)(u, 0, 0);
		}

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v) const noexcept override
		{
			return (*this)(u, v, 0);
		}

		[[nodiscard]] virtual OutType_ operator() (InType_ u, InType_ v, InType_ w) const noexcept override
		{
			if(!mGradientTab)
				return 0;

			IType bx0, bx1, by0, by1, bz0, bz1, b00, b10, b01, b11;
			OutType_ rx0, rx1, ry0, ry1, rz0, rz1, *q, sy, sz, a, b, c, d, t, u2, v2;
			IType i, j;

			const auto perlinLerp = [](auto t, auto a, auto b){ return a + t * (b - a); };
			const auto perlinLerpCurve = [](auto t){ return t * t * (3 - 2 * t); };
			const auto at3 = [&q](auto rx, auto ry, auto rz){ return rx * q[0] + ry * q[1] + rz * q[2]; };
			const auto perlinSetup =
				[this](auto val, auto &b0, auto &b1, auto &r0, auto &r1)
				{
					const auto tf = val /ofloor;
					b0 = IType(tf) % mTabSize;
					b0 = b0 < 0 ? mTabSize + b0 : b0;
					b1 = b0 == mTabSize2 ? 0 : b0 + 1;
					r0 = val - tf;
					r1 = r0 - 1;
				};

			perlinSetup(u, bx0,bx1, rx0,rx1);
			perlinSetup(v, by0,by1, ry0,ry1);
			perlinSetup(w, bz0,bz1, rz0,rz1);

			i = mPerms[ bx0 ];
			j = mPerms[ bx1 ];

			b00 = mPerms[ i + by0 ];
			b10 = mPerms[ j + by0 ];
			b01 = mPerms[ i + by1 ];
			b11 = mPerms[ j + by1 ];

			t  = perlinLerpCurve(rx0);
			sy = perlinLerpCurve(ry0);
			sz = perlinLerpCurve(rz0);

			q = mGradientTab[ b00 + bz0 ].g.data() ; u2 = at3(rx0,ry0,rz0);
			q = mGradientTab[ b10 + bz0 ].g.data() ; v2 = at3(rx1,ry0,rz0);
			a = perlinLerp(t, u2, v2);

			q = mGradientTab[ b01 + bz0 ].g.data() ; u2 = at3(rx0,ry1,rz0);
			q = mGradientTab[ b11 + bz0 ].g.data() ; v2 = at3(rx1,ry1,rz0);
			b = perlinLerp(t, u2, v2);

			c = perlinLerp(sy, a, b);

			q = mGradientTab[ b00 + bz1 ].g.data() ; u2 = at3(rx0,ry0,rz1);
			q = mGradientTab[ b10 + bz1 ].g.data() ; v2 = at3(rx1,ry0,rz1);
			a = perlinLerp(t, u2, v2);

			q = mGradientTab[ b01 + bz1 ].g.data() ; u2 = at3(rx0,ry1,rz1);
			q = mGradientTab[ b11 + bz1 ].g.data() ; v2 = at3(rx1,ry1,rz1);
			b = perlinLerp(t, u2, v2);

			d = perlinLerp(sy, a, b);

			return perlinLerp(sz, c, d) * mStrength + mStart;
		}

		void setStrength(InType_ str) noexcept { mStrength = str; }
		[[nodiscard]] InType_ getStrength() const noexcept { return mStrength; }

		void setStart(InType_ st) noexcept { mStart = st; }
		[[nodiscard]] InType_ getStart() const noexcept { return mStart; }

		[[nodiscard]] const auto &getRandomizer() const noexcept { return mRandomizer; }

		[[nodiscard]] IType getTableSize() const noexcept { return mTabSize; }

		void initFromOther(const DistributionMapperPerlin3D &other)
		{
			mTabSize = other.mTabSize;
			mTabSize2 = other.mTabSize2;
			mTabSizef = other.mTabSizef;

			mRandomizer = other.mRandomizer;

			const szt tabCount = mTabSize + mTabSize + 2;
			mGradientTab = std::make_unique<GradientContainer[]>(tabCount);
			mPerms = std::make_unique<IType[]>(tabCount);

			std::memcpy(mGradientTab.get(), other.mGradientTab.get(), sizeof(GradientContainer) * tabCount);
			std::memcpy(mPerms.get(), other.mPerms.get(), sizeof(IType) * tabCount);

			mStrength = other.mStrength;
			mStart = other.mStart;
		}

		void init(IType tableSize, EntityPtr<IRNGInt> randomiz)
		{
			IType i, k, j;

			mRandomizer = std::move(randomiz);
			mTabSize = tableSize;
			mTabSize2 = mTabSize - 1;
			mTabSizef = mTabSize;

			mRandomizer->seed();

			const szt tabCount = mTabSize + mTabSize + 2;
			mGradientTab = std::make_unique<GradientContainer[]>(tabCount);
			mPerms = std::make_unique<IType[]>(tabCount);

			GradientContainer *tval = mGradientTab.get();
			OutType_ z, r, theta;

			std::uniform_int_distribution<IType> intDist{0, mTabSize2 - 1};

			for(i=0; i<mTabSize; ++i)
			{
				mPerms[i] = i;
				z = 1.0 - 2.0 * (intDist(*mRandomizer) /ofloor / mTabSize);
				r = std::sqrt(1.0 - z * z);

				theta = 2 * std::numbers::pi_v<OutType_> * (static_cast<OutType_>(intDist(*mRandomizer)) /ofloor / mTabSize);
				tval->g[0] = r * pl::plcos(theta);
				tval->g[1] = r * pl::plsin(theta);
				tval->g[2] = z;
				tval++;
			}

			while(--i)
			{
				k = mPerms[i];
				mPerms[i] = mPerms[(j = intDist(*mRandomizer))];
				mPerms[j] = k;
			}

			for(i = 0 ; i < mTabSize + 2 ; ++i)
			{
				mPerms[mTabSize + i] = mPerms[i];
				mGradientTab[mTabSize + i] = mGradientTab[i];
			}
		}
	protected:
		InType_ mStrength{1}, mStart{}, mTabSizef;
		EntityPtr<IRNGInt> mRandomizer;
		std::unique_ptr<IType[]> mPerms;
		IType mTabSize, mTabSize2;
		std::unique_ptr<GradientContainer[]> mGradientTab;
	};

	namespace tag
	{
		PL_MAKE_MULTITAGGEDSPAN_TAG(cdf_dist)
		PL_MAKE_MULTITAGGEDSPAN_TAG(cdf_result)
	}

	template <typename T_>
	class CDFDistribution
	{
	private:
		using DataType = pl::dynamic_multi_tagged_span<
			hn::pair<type_c<tag::cdf_dist_t>,   type_c<const T_>>,
			hn::pair<type_c<tag::cdf_result_t>, type_c<T_>>
		>;

	public:

		template <typename>
		friend class CDFDistribution;

		 // Has to be size of distribution data + 2. Distribution data will never be accessed pass its size, so only the
		 // result data need to have allocated distribution data size + 2.
		DataType mData;

		using value_type = std::remove_const_t<T_>;
		using size_type = typename DataType::size_type;

		static constexpr bool IsConst = std::is_const_v<T_>;

		constexpr CDFDistribution() = default;

		constexpr CDFDistribution(DataType data) :
			mData{data} {}

		constexpr CDFDistribution(size_type nb, const T_ *data, T_ *result) :
			mData{
				nb,
				hn::pair{tag::cdf_dist,   data},
				hn::pair{tag::cdf_result, result}
			} {}

		template <typename OT_>
		constexpr CDFDistribution(const CDFDistribution<OT_> &o)
				requires ((IsConst || !std::is_const_v<OT_>) && std::same_as<std::remove_const_t<T_>, std::remove_const_t<OT_>>)
			: mData{o.mData} {}

		[[nodiscard]] constexpr size_type size() const noexcept { return mData.size() - 2; }
		[[nodiscard]] constexpr value_type integral() const noexcept { return mData[tag::cdf_result][mData.size() - 1]; }

		[[nodiscard]] constexpr value_type operator[] (const size_type index) const noexcept
			{ return mData[tag::cdf_result][index]; }

		constexpr void createDistribution() const
			requires (!IsConst)
		{
			const auto cdf = mData[tag::cdf_result];
			plassert(cdf.size() >= 2);

			auto &integralValue = cdf[cdf.size() - 1];
			integralValue = 0;

			if(!size())
				return;

			cdf[0] = 0;

			const auto nbSamples = static_cast<value_type>(size());

			rg::partial_sum(
					mData.range(tag::cdf_dist) | rgv::drop_last(2),
					cdf.begin() + 1,
					std::plus{},
					[&](const auto &v){ return v / nbSamples; }
				);

			if(const auto fcInt = cdf[size()]; fcInt > 0)
			{
				for(auto &c : cdf | rgv::tail | rgv::drop_last(1))
					pl::assert_valid(c /= fcInt, valid::Weight);

				integralValue = 1 / fcInt;
			}
		}

		[[nodiscard]] constexpr size_type operator () (const value_type x) const noexcept
			{ return sample(x); }

		[[nodiscard]] constexpr size_type sample(const value_type x) const noexcept
		{
			auto cdfr = mData.range(tag::cdf_result);
			auto it = std::upper_bound(cdfr.begin() + 1, cdfr.end(), x);

			return it == cdfr.end() ? size() - 1 : static_cast<size_type>(std::distance(cdfr.begin(), it)) - 1;
		}

		[[nodiscard]] constexpr value_type getProbability(const size_type index) const noexcept
		{
			return mData[tag::cdf_result][index + 1] - mData[tag::cdf_result][index];
		}

		[[nodiscard]] constexpr value_type getSampleLocalOffset(const value_type x, const size_type index) const noexcept
		{
			auto dx = x - mData[tag::cdf_result][index];
			const auto dc = mData[tag::cdf_result][index + 1] - mData[tag::cdf_result][index];

			if(dc > 0)
				dx /= dc;

			return dx;
		}

		[[nodiscard]] constexpr value_type getSampleOffset(const value_type x, const size_type index) const noexcept
		{
			return ((value_type)index + getSampleLocalOffset(x, index)) / (value_type)size();
		}

		[[nodiscard]] constexpr value_type getWeight(const value_type x) const noexcept
		{
			value_type cdfIndexf = x * size();
			size_type cdfIndex = static_cast<size_type>(cdfIndexf);

			if(cdfIndex != 0 && cdfIndexf - cdfIndex == 0)
				--cdfIndex;

			return getProbability(cdfIndex) * size();
		}

		[[nodiscard]] constexpr value_type getDistribution(const size_type index) const noexcept
			{ return mData[tag::cdf_dist][index]; }

		// Probability mass function
		[[nodiscard]] constexpr value_type PMF(const size_type index) const noexcept
			{ return mData[tag::cdf_dist][index] * integral(); }

		// Probability density function
		[[nodiscard]] constexpr value_type PDF(const value_type x) const noexcept
			{ return mData[tag::cdf_dist][pl::plmin<size_type>(x * size(), size() - 1)] * integral(); }

		[[nodiscard]] constexpr value_type joinPMF(const value_type a, const value_type b) const noexcept
			{ return (a + b) * (static_cast<value_type>(size() - 1) / size()); }

		[[nodiscard]] friend constexpr auto tag_invoke(pl::tag_t<pl::as_const_view>, const CDFDistribution &v) noexcept
			{ return CDFDistribution<const T_>{v}; }
	};
	template <typename ST_, typename T1_, typename T2_>
	CDFDistribution(ST_, T1_ *, T2_ *) -> CDFDistribution<T2_>;
	template <typename TD_, typename TR_>
	CDFDistribution(pl::dynamic_multi_tagged_span<hn::pair<type_c<tag::cdf_dist_t>, type_c<TD_>>, hn::pair<type_c<tag::cdf_result_t>, type_c<TR_>>>) -> CDFDistribution<TR_>;


	// https://en.wikipedia.org/wiki/Hilbert_curve
	template <typename T_ = if32>
	class HilbertCurve2D
	{
	public:

		using value_type = T_;

		constexpr pl::point2d<T_> advance() noexcept
		{
			pl::point2d<T_> p{};

			do
			{
				if(mDistance == mDistMax)
					reset();

				p = indexToPos(mNbDims, mDistance++);
			}
			while(pl::mask_any(p >= mBounds));

			return p;
		}

		constexpr void setBounds(pl::dir2d<T_> b) noexcept
		{
			mBounds = b;
			mNbDims = static_cast<T_>( std::bit_ceil(static_cast<std::make_unsigned_t<T_>>(b.hmax())) );
			mDistMax = mNbDims /opow2;

			reset();
		}

		[[nodiscard]] constexpr auto getBounds() const noexcept { return mBounds; }

		constexpr void reset() noexcept
		{
			mDistance = 0;
		}

	protected:
		T_ mDistance = 0, mNbDims = 1, mDistMax = 0;
		pl::dir2d<T_> mBounds{};

		[[nodiscard]] constexpr T_ posToIndex(T_ n, pl::point2d<T_> p) const noexcept
		{
			pl::point2d<T_> rp;
			T_ d=0;

			for(T_ s=n/2; s>0; s/=2)
			{
				rp = (p & s) > 0;
				d += s * s * ((3 * rp[0]) ^ rp[1]);
				p = rot(s, p, rp);
			}

			return d;
		}

		[[nodiscard]] constexpr pl::point2d<T_> indexToPos(T_ n, T_ d) const noexcept
		{
			pl::point2d<T_> rp, p{};
			T_ t=d;

			for(T_ s=1; s<n; s*=2)
			{
				rp[0] = 1 & (t/2);
				rp[1] = 1 & (t ^ rp[0]);
				p = rot(s, p, rp) + (rp * s);
				t /= 4;
			}

			return p;
		}

		[[nodiscard]] constexpr pl::point2d<T_> rot(T_ n, pl::point2d<T_> p, pl::point2d<T_> rp) const noexcept
		{
			if(!rp[1])
			{
				if(rp[0] == 1)
					return {n - 1 - p[0], n - 1 - p[1]};

				return {p[1], p[0]};
			}

			return p;
		}

	};
}
