/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Base.hpp"
#include "Integration.hpp"

#include <patlib/back_buffered.hpp>
#include <patlib/scoped.hpp>

namespace PhotoRT
{
	class PathWalker
	{
	private:
		uf32 mNbSamplesPath = 4, mMaxPathLength = 25;
		uf32 mNbLayerBouncesMax = 5;
		bool mDoRR = true;
		Real mRrFactor = 1, mRrMin = 0.2;
		bool mMediumScatter = true;

		template <typename PathState_>
		void absorb(PathState_ &ps, Color absoption) const noexcept
		{
			ps.throughput *= absoption;
			ps.importance *= getColorAlbedo(absoption);
		}

		template <typename PathState_>
		void passthrough(PathState_ &ps, RayBatchEntry &rbEntry, TraversalEntry &trEntry, VecD dir, Color tr = White, Real pdf = 1) const noexcept
		{
			trEntry.color = tr;
			trEntry.pdf = pdf;
			trEntry.contProb = 1;
			trEntry.segment.insideLayer = false;
			trEntry.albedo = getColorAlbedo(tr);
			rbEntry.ray.dir = dir;
			const auto &inter = std::get<SurfaceInteraction>(*trEntry.segment.start);
			rbEntry.ray.origin = inter.safeIncrement(inter.worldPos(), inter.worldPos(), EnumHemisphere::South);
			rbEntry.ray.setupPathRay();

			trEntry.subsEntryEvent = ps.substanceStack.checkSubstanceEntry(EnumHemisphere::South);

			rbEntry.ray.setupPassthrough();
		}

	public:

		void setNbSamplesPath(uf32 s) noexcept { mNbSamplesPath = pl::plclamp<uf32>(s, 0, RayBatchMaxSize); }
		[[nodiscard]] auto getNbSamplesPath() const noexcept { return mNbSamplesPath; }

		void setMaxPathLength(uf32 m) noexcept { mMaxPathLength = m; }
		[[nodiscard]] auto getMaxPathLength() const noexcept { return mMaxPathLength; }

		void setLayerBouncesMax(uf32 m) noexcept { mNbLayerBouncesMax = pl::plclamp<uf32>(m, 0, 256); }
		[[nodiscard]] auto getLayerBouncesMax() const noexcept { return mNbLayerBouncesMax; }

		void setUseRR(bool v) noexcept { mDoRR = v; }
		[[nodiscard]] bool isUsingRR() const noexcept { return mDoRR; }

		void setRRfactor(Real f) noexcept { mRrFactor = f; }
		[[nodiscard]] auto getRRfactor() const noexcept { return mRrFactor; }

		void setRRmin(Real f) noexcept { mRrMin = f; }
		[[nodiscard]] auto getRRmin() const noexcept { return mRrMin; }

		void mediumScatterEnabled(bool v) noexcept { mMediumScatter = v; }
		[[nodiscard]] auto mediumScatterEnabled() const noexcept { return mMediumScatter; }

		[[nodiscard]] Real getContinuationProb(CReal albedo) const noexcept { return isUsingRR() ? pl::plclamp<Real>(albedo * getRRfactor(), mRrMin, 1) : 1; }

		PathSegment traceLine(auto &pathState, Ray &ray, PathSegment segment, TracerParams &tracer, auto&& createInteractionFc, auto&& onCollisionFc) const noexcept
		{
			Ray *oray = &ray;
			auto &rng = tracer.sampler;
			pl::back_buffered<RayBatchEntry> rbEntry;
			pl::back_buffered<TraversalEntry> trEntry;
			const auto destination = ray.origin + ray.dir * ray.length;

			trEntry.back().segment = segment;

			do
			{
				// Update substance stack

				auto &backSegment = trEntry.back().segment;

				const auto curInter = [&]() -> auto & { return std::get<SurfaceInteraction>(*backSegment.end); };
				const auto &enteredSub = pathState.substanceStack.getEnteredSub()->getMaterial();

				// Color absorption
				if(!enteredSub.isMedium())
				{
					absorb(pathState, enteredSub.absorbColor(backSegment.end ? curInter().colDist : oray->length));
				}
				else
				{
					auto absoption = enteredSub.getMedium()->transmittance(*backSegment.start, oray->dir, backSegment.end ? curInter().colDist : oray->length, rng);
					absorb(pathState, absoption);
					absoption.set(0, size_v<3>);
					segment.pdfTr *= (absoption /ohadd)[size_v<0>] * (1.0 / 3.0);
				}

				if(!PL_FWD(onCollisionFc)(pathState, backSegment))
					break;

				pl::assert_valid(pathState.throughput);

				// End path if current throughput is zero or no collision is
				if(!backSegment.end || isAlmostBlack(pathState.throughput))
					break;

				if(curInter().surface().matEntry && !curInter().isSafeDir(oray->dir))
				{
					pathState.throughput.set_zero();
					break;
				}

				// Get the next substance if current surface can be entered
				pathState.substanceStack.interaction(curInter());

				const bool shouldPassthrough = curInter().shouldPassthrough();

				if(!shouldPassthrough && !curInter().surface().matEntry)
				{
					pathState.throughput.set_zero();
					break;
				}

				if(!shouldPassthrough)
				{
					const auto &mat = *curInter().surface().matEntry->material;
					const auto bxdfAlpha = mat.getLayerAlpha(mat.getFrontLayerIndex(curInter().surface().isBackfaceHit()), pathState.mode, curInter(), oray->dir, oray->dir, EnumHemisphere::South, false);

					pathState.throughput *= White - bxdfAlpha.alpha;
					segment.pdfTr *= 1 - bxdfAlpha.pdf;
				}

				if(isAlmostBlack(pathState.throughput))
					break;

				trEntry.front().segment = {backSegment, nullptr};
				//trEntry.front().prevSegment = backSegment;

				auto &nray = rbEntry.front().ray;
				nray.type = EnumRayType::Passthrough;
				nray.origin = curInter().safeIncrement(oray->origin, curInter().worldPos(), EnumHemisphere::South);

				if(ray.length != Ray::MaxDistance)
				{
					nray.setDestination(destination);
				}
				else
				{
					nray.length = Ray::MaxDistance;
					nray.dir = oray->dir;
				}

				trEntry.front().subsEntryEvent = pathState.substanceStack.checkSubstanceEntry(EnumHemisphere::South);

				traceOne(tracer.rayTracer, rbEntry.front().ray, trEntry.front().segment, 1, 1, PL_FWD(createInteractionFc), &backSegment);

				oray = &rbEntry.front().ray;

				pathState.substanceStack.updateSubstanceStack(trEntry.front().subsEntryEvent);

				rbEntry.flip();
				trEntry.flip();
			}
			while(true);

			segment.end = trEntry.back().segment.end;

			return segment;
		}

		/*
			This function handle most of the work for walking a path. It handles bxdf, layers, medium,
			absorbance, substance entry and exit with refraction priority, path splitting, russian roulette
			and light portals. This function walk the path iteratively and handle path splitting and some other
			functions with recursion.

			It assume that the first ray given with $ray has already been traced and $segment.end contain information
			about the collided surface if any.
		*/
		template <typename PathState_>
		void integrateAndTracePath(
			const Contributor &contrib,
			PathState_ &pathState,
			const Ray &ray,
			PathSegment segment,
			TracerParams &tracer,
			auto &&createInteractionFc,
			auto &&pathPrepareFc,          // void(PathState_ &pathState, const Ray &ray)
			auto &&pathSplitFc,            // void(PathState_ &newPathState, const Ray &ray, const PathSegment &segment, const std::function<void()> &fc)
			auto &&pathPassthroughFc,      // bool(PathState_ &pathState, const Ray &ray, const PathSegment &segment)
			auto &&pathContributionFc,     // bool(const Contributor &contrib, PathState_ &pathState, const Ray &ray, const PathSegment &segment, const auto &interaction)
			auto &&pathEndFc               // void(const Contributor &contrib, PathState_ &pathState, const Ray &ray, const PathSegment &segment)
		) const noexcept
		{
			const Ray *oray = &ray;
			auto &rng = tracer.sampler;
			pl::back_buffered<RayBatch> batch;
			pl::back_buffered<TraversalData> traversalEntries;

			#ifndef NDEBUG
				szt debugIterationCounter = 0;
			#endif

			PL_FWD(pathPrepareFc)(pathState, ray);

			const auto doRecursion = [&](PathState_ &newPathState, const Ray &ray, const PathSegment &newSegment)
				{
					integrateAndTracePath(
							contrib,
							newPathState,
							ray,
							newSegment,
							tracer,
							PL_FWD(createInteractionFc),
							PL_FWD(pathPrepareFc),
							PL_FWD(pathSplitFc),
							PL_FWD(pathPassthroughFc),
							PL_FWD(pathContributionFc),
							PL_FWD(pathEndFc)
						);
				};

			const auto updateThroughput = [&](PathState_ &ps, const auto &trEntry)
				{
					pl::assert_valid( ps.throughput *= trEntry.color * (trEntry.pdf /oprtreciprocal) );
					ps.importance *= trEntry.albedo;
					ps.rrFactor *= trEntry.contProb /oprtreciprocal;
				};

			const auto doRecursionTrace = [&](const PathState_ &ps, auto &rbEntry, auto &trEntry)
				{
					traceOne(tracer.rayTracer, rbEntry.ray, trEntry.segment, trEntry.pdf, trEntry.contProb, PL_FWD(createInteractionFc), &trEntry.prevSegment);

					PathState_ newPathState{ps};

					PL_FWD(pathSplitFc)(newPathState, rbEntry.ray, trEntry.segment, [&]
						{
							newPathState.substanceStack.updateSubstanceStack(trEntry.subsEntryEvent);
							updateThroughput(newPathState, trEntry);
							doRecursion(newPathState, rbEntry.ray, trEntry.segment);
						});
				};

			const auto traceAndRecursion = [&](PathState_ &ps, auto &rbEntry, auto &trEntry)
				{
					traceOne(tracer.rayTracer, rbEntry.ray, trEntry.segment, trEntry.pdf, trEntry.contProb, PL_FWD(createInteractionFc), &trEntry.prevSegment);

					doRecursion(ps, rbEntry.ray, trEntry.segment);
				};

			const auto doPathSplit = [&](const PathState_ &ps, auto &rbEntry, auto &trEntry, auto&& fc)
				{
					PathState_ newPathState{ps};

					PL_FWD(pathSplitFc)(newPathState, rbEntry.ray, trEntry.segment, [&]
						{
							if(PL_FWD(fc)(newPathState))
							{
								newPathState.substanceStack.updateSubstanceStack(trEntry.subsEntryEvent);
								traceAndRecursion(newPathState, rbEntry, trEntry);
							}
						});
				};

			const auto newRayEntry = [&] [[nodiscard]]
				{
					auto &tre = traversalEntries.front().emplace_back();
					tre.segment = {segment, nullptr};

					auto &be = batch.front().emplace_back();

					return std::forward_as_tuple(be, tre);
				};

			const auto sampleRays = [&](uf32 nb, auto&& fc)
				{
					nb = pl::plmin<uf32>(PRT_MAX_NBSAMPLES, nb);
					const auto weight = r_(nb) /oprtreciprocal;
					RandomReal rndx[PRT_MAX_NBSAMPLES], rndy[PRT_MAX_NBSAMPLES];
					const std::span rndSpanX{rndx, nb}, rndSpanY{rndy, nb};

					pathState.weight *= weight;
					pathState.distributionFactor *= weight;

					tracer.sampler.requestSamples(rndSpanX, uniformRealRng<>(), true);
					tracer.sampler.requestSamples(rndSpanY, uniformRealRng<>(), true);

					rg::shuffle(rndSpanY, tracer.sampler.mRng);

					for(const auto i : mir(nb))
					{
						if(auto&& [rbEntry, trEntry] = newRayEntry(); !fc(i, {rndSpanX[i], rndSpanY[i]}, rbEntry, trEntry))
						{
							batch.front().pop_back();
							traversalEntries.front().pop_back();
						}
					}
				};

			const auto doPassthrough = [&](PathState_ &ps, auto &rbEntry, auto &trEntry, Color tr = White, Real pdf = 1)
				{
					passthrough(ps, rbEntry, trEntry, oray->dir, tr, pdf);

					PL_FWD(pathPassthroughFc)(ps, *oray, segment);
				};

			const auto getContProbFromRay = [&] [[nodiscard]] (const auto &fromRay, const auto albedo) -> Real
				{
					return !(fromRay.isType(EnumRayType::Transparency | EnumRayType::Passthrough)) ?
						getContinuationProb(albedo) : 1;
				};

			const auto doRR = [&] [[nodiscard]] (const auto contProb) -> bool
				{
					// Russian roulette
					return contProb >= 1 || uniformRealRng<Real>()(rng.mRng) < contProb;
				};

			const auto terminatePath = [&](auto&& ps, const auto &fromRay, const auto &fromSegment)
				{
					if(!ps.terminated)
					{
						ps.terminated = true;
						const auto c = contrib.scale({.weight = ps.throughput * ps.weight, .rrFactor = ps.rrFactor});
						PL_FWD(pathEndFc)(c, ps, fromRay, fromSegment);
					}
				};

			do
			{
				if(!oray->isType(EnumRayType::Transparency | EnumRayType::Passthrough))
					++pathState.nbBounces;

				pl::assert_valid(pathState.throughput);

				// End path if current throughput is zero
				if(isAlmostBlack(pathState.throughput))
					break;

				const auto curInter = [&]() -> auto & { return std::get<SurfaceInteraction>(*segment.end); };

				const auto &enteredSub = pathState.substanceStack.getEnteredSub()->getMaterial();
				const bool samplingMedium = mMediumScatter && enteredSub.isMedium();
				const auto colDist = segment.end ? curInter().colDist : pl::constants::infhighest<Real>;

				// Color absorption
				if(!enteredSub.isMedium())
				{
					absorb(pathState, enteredSub.absorbColor(colDist));
				}
				else if(!mMediumScatter)
				{
					auto absoption = enteredSub.getMedium()->transmittance(*segment.start, oray->dir, colDist, rng);
					absorb(pathState, absoption);
					absoption.set(0, size_v<3>);
					segment.pdfTr *= (absoption /ohadd)[size_v<0>] * (1.0 / 3.0);
				}

				if(segment.end)
				{
					if(!curInter().isSafeDir(oray->dir))
						break;

					pathState.substanceStack.interaction(curInter());
				}
				else if(!samplingMedium)
					break;

				const bool shouldPassthrough = segment.end ? curInter().shouldPassthrough() : false;
				const bool isLightPortal = segment.end ? curInter().surface().objProps && curInter().surface().objProps->isLightPortal() : false;

				if(isLightPortal)
				{
					segment.lpSurface = segment.end;
					segment.infPdfTr = 1;
				}

				// Passthrough if we hit a substance with lower priority index or a light portal but only if not inside a medium
				if(shouldPassthrough && !samplingMedium)
				{
					auto&& [rbEntry, trEntry] = newRayEntry();
					trEntry.prevSegment = segment;

					doPassthrough(pathState, rbEntry, trEntry);
				}
				else
				{
					const bool hasMaterial = segment.end && curInter().surface().matEntry;

					// End path if no material or medium
					if(!samplingMedium && !hasMaterial)
						break;

					// Prepare surface layer sampling

					const MaterialLayer *frontSubstance = nullptr;
					const IBxDF *bxdf = nullptr;
					if32 frontLayerIndex = -1, backLayerIndex;
					auto layerOrient = EnumHemisphere::North;

					if(hasMaterial)
					{
						auto &inter = curInter();
						frontLayerIndex = inter.surface().layerIndex;

						const auto &material = inter.getMaterial();
						const auto &frontLayer = material.mLayers[frontLayerIndex];

						backLayerIndex = material.getBackLayerIndex(inter.surface().isBackfaceHit());
						layerOrient = backLayerIndex - frontLayerIndex > 0 ? EnumHemisphere::South : EnumHemisphere::North;
						frontSubstance = &material.mLayers[frontLayerIndex];

						if(frontSubstance == inter.enteredSub)
						{
							if(frontLayerIndex == backLayerIndex)
								frontSubstance = inter.nextSubstance;
							else
								frontSubstance = &material.mLayers[frontLayerIndex + (layerOrient == EnumHemisphere::South ? 1 : -1)];
						}

						// Get contribution at vertex but only if not inside a medium since we have to sample the medium first
						if(!samplingMedium)
						{
							const pl::fast_scoped_assignment b1{inter.nextSubstance, frontSubstance};
							const auto c = contrib.scale({.weight = pathState.throughput * pathState.weight, .rrFactor = pathState.rrFactor});

							if(PRT_SCOPED_PROFILE(Profiler::raytrace, Profiler::tag::Contribution);
									!PL_FWD(pathContributionFc)(c, pathState, *oray, segment, inter))
								break;

							if(pathState.nbBounces + 1 >= getMaxPathLength())
								break;
						}

						bxdf = std::to_address(frontLayer.getBxDF());
					}

					// Generate path samples
					const bool isTransparent = hasMaterial && ((!samplingMedium && bxdf && !isType(*bxdf, pl::flip_bits(EnumRayType::Transparency)) && curInter().surface().matEntry->material->mLayers.size() == 1));
					const Real samplingImportance = !isTransparent * (hasMaterial ? curInter().surface().matEntry->material->getSamplingImportance() : enteredSub.getSamplingImportance()) * pathState.distributionFactor;
					const auto curNbSamples = pl::plmax<uf32>(mNbSamplesPath * samplingImportance, 1);

					// Medium have to be sampled from the start of the last ray, not the current segment
					Real maxMediumDist = std::numeric_limits<Real>::infinity();
					auto scatterDir = oray->dir;
					auto scatterOrigin = oray->origin;
					if(segment.end && samplingMedium)
					{
						const auto destp = curInter().safeIncrement(oray->origin, curInter().worldPos(), EnumHemisphere::North);
						scatterDir = static_cast<VecD>(destp - oray->origin);
						if(scatterDir /odot/ oray->dir < 0)
							maxMediumDist = 0;
						else
						{
							maxMediumDist = scatterDir /oprtlength;
							scatterDir = scatterDir /olnormalize/ maxMediumDist;
						}
					}

					pl::assert_valid(oray->dir);

					//////////////////
					// Direction sample functions
					//////////////////

						const auto sampleMedium = [&] [[nodiscard]] (PathState_ &ps, auto &rbEntry, auto &trEntry, Real &mediumPdf, RandomReals<2> rv) -> bool
							{
								if(maxMediumDist <= 0)
									return false;

								const auto &medium = *enteredSub.getMedium();

								auto sp = medium.sampleScattering(rv, *segment.end, scatterDir, maxMediumDist, rng);
								if(sp.pdfScatter)
									ps.throughput *= sp.color * (sp.pdfScatter /oprtreciprocal);
								mediumPdf = sp.pdfScatter;

								if(sp.pdfDir > 0)
								{
									if(isAlmostBlack(ps.throughput))
									{
										ps.throughput.set_zero();
										return true;
									}

									// We have scattering

									++ps.nbBounces;
									rbEntry.ray.length = Ray::MaxDistance;
									ps.importance *= getColorAlbedo(sp.color);
									rbEntry.ray.origin = scatterOrigin + scatterDir * sp.scatterDist;

									trEntry.color = sp.pdfDir;
									trEntry.pdf = sp.pdfDir;
									trEntry.albedo = getColorAlbedo(sp.color);
									rbEntry.ray.type = sp.type;
									rbEntry.ray.dir = sp.dir;

									// Segment will start at a new location
									trEntry.segment.start = &PL_FWD(createInteractionFc)(MediumInteraction{
											.mWorldPos = rbEntry.ray.origin,
											.mLocalPos = rbEntry.ray.origin,
											.colDist = sp.scatterDist,
											.mInst = pl::visit_one(*segment.start, [&](const auto &i){ return i.instance(); }),
											.mMedium = &medium,
											.enteredSub = ps.substanceStack.getEnteredSub()
										});

									trEntry.subsEntryEvent = ps.substanceStack.checkSubstanceEntry(EnumHemisphere::North);

									{
										PathSegment s{segment};
										s.end = trEntry.segment.start;
										s.pdfTr *= mediumPdf;

										const auto c = contrib.scale({.weight = ps.throughput * ps.weight, .rrFactor = ps.rrFactor});

										PRT_SCOPED_PROFILE(Profiler::raytrace, Profiler::tag::Contribution);
										if(!PL_FWD(pathContributionFc)(c, ps, *oray, s, std::get<MediumInteraction>(*s.end)))
											ps.throughput.set_zero();
									}

									trEntry.contProb = 1;
									if(ps.nbBounces + 1 >= getMaxPathLength() || !doRR(trEntry.contProb))
										ps.throughput.set_zero();

									return true;
								}

								return false;
							};

						const auto sampleDirection = [&] [[nodiscard]] (const PathState_ &ps, auto &rbEntry, auto &trEntry, const RandomReals<2> rv, const SurfaceInteraction &interaction, const VecD odir) -> EnumHemisphere
							{
								auto hemisphere = EnumHemisphere::Invalid;

								if(const auto sp = sampleLayer(ps.mode, rv, odir, interaction, rng, ps.importance); sp && !isAlmostBlack(sp->color.color))
								{
									hemisphere = sp->hemisphere;
									trEntry.pdf = sp->pdf;
									trEntry.color = sp->color.color;
									trEntry.albedo = sp->albedo;
									rbEntry.ray.dir = sp->dir;
									rbEntry.ray.type = sp->type;
								}

								return hemisphere;
							};

					//////////////////
					// End - Direction sample functions
					//////////////////

					// Sample material and medium

					if(!samplingMedium && hasMaterial && curInter().getMaterial().mLayers.size() == 1)
					{
						// Simple case with no medium and one layer
						sampleRays(curNbSamples,
								[&](uf32, const RandomReals<2> rnd, auto &rbEntry, auto &trEntry) -> bool
								{
									trEntry.prevSegment = segment;
									trEntry.segment.insideLayer = false;

									const auto dirHemisphere = sampleDirection(pathState, rbEntry, trEntry, rnd, std::get<SurfaceInteraction>(*segment.end), oray->dir);
									if(dirHemisphere == EnumHemisphere::Invalid)
										return false;

									trEntry.contProb = getContProbFromRay(rbEntry.ray, trEntry.albedo);
									if(!doRR(trEntry.contProb))
										return false;

									rbEntry.ray.setupPathRay();
									pl::visit_one(*trEntry.segment.start, [&](const auto &i){
											rbEntry.ray.origin = i.safeIncrement(oray->origin, i.worldPos(), dirHemisphere);
										});

									trEntry.subsEntryEvent = pathState.substanceStack.checkSubstanceEntry(dirHemisphere);

									if(rbEntry.ray.isType(EnumRayType::Transparency))
										trEntry.segment.infPdfTr = segment.infPdfTr * trEntry.pdf;

									return true;
								}
							);
					}
					else
					{
						// Select between recursion or continue with current loop. Only the last sample should continue with current
						// loop as it might alter the current path state.
						const auto selectRecursion = [&] [[nodiscard]] (uf32 i, auto &rbEntry, auto &trEntry, auto&& fc) -> bool
							{
								if(i < curNbSamples - 1)
								{
									doPathSplit(pathState, rbEntry, trEntry, [&](PathState_ &ps) -> bool
										{
											const auto res = PL_FWD(fc)(ps, true);

											if(res)
												updateThroughput(ps, trEntry);
											else
												terminatePath(ps, *oray, trEntry.prevSegment);

											return res;
										});

									return false;
								}

								return PL_FWD(fc)(pathState, false);
							};

						sampleRays(curNbSamples, [&](uf32 i, const RandomReals<2> rnd, auto &rbEntry, auto &trEntry) -> bool
							{
								return selectRecursion(i, rbEntry, trEntry, [&](auto &ps, bool /*recursion*/) -> bool
									{
										Real mediumPdf = 1;

										trEntry.prevSegment = segment;

										// Possibly sample medium
										if(samplingMedium && sampleMedium(ps, rbEntry, trEntry, mediumPdf, rnd))
											return !isAlmostBlack(ps.throughput);

										if(shouldPassthrough)
										{
											doPassthrough(ps, rbEntry, trEntry, mediumPdf, mediumPdf);
											return true;
										}

										if(!hasMaterial)
											return false;

										const auto &material = curInter().getMaterial();

										uf32 nbLayerBounces = 0;

										constexpr u8 FlagFirst = 0, FlagValid = 1, FlagReversed = 2;
										auto flags = pl::make_bitset(size_v<3>, std::make_pair(FlagFirst, true));

										rbEntry.ray = *oray;

										const std::array subs{curInter().enteredSub, frontSubstance};
										auto layerSegment = segment;
										auto layerLobe = curInter().lobe();
										auto odir = oray->dir;

										layerSegment.pdfTr *= mediumPdf;
										if(!isLightPortal)
											layerSegment.infPdfTr *= mediumPdf;
										std::get<SurfaceInteraction>(*layerSegment.end).surface().layerIndex = frontLayerIndex;
										std::get<SurfaceInteraction>(*layerSegment.end).nextSubstance = frontSubstance;

										rbEntry.ray.origin = std::get<SurfaceInteraction>(*layerSegment.end).worldPos();
										trEntry.segment.insideLayer = true;

										// Sample layers
										while(true)
										{
											++nbLayerBounces;

											std::get<SurfaceInteraction>(*layerSegment.end).lobe() = layerLobe;

											// Add contribution at layer
											if(!flags[FlagFirst] || samplingMedium)
											{
												const auto c = contrib.scale({.weight = ps.throughput * ps.weight, .rrFactor = ps.rrFactor});

												PRT_SCOPED_PROFILE(Profiler::raytrace, Profiler::tag::Contribution);
												if(!PL_FWD(pathContributionFc)(c, ps, rbEntry.ray, layerSegment, std::get<SurfaceInteraction>(*layerSegment.end)))
													break;
											}

											// Sample a new direction
											trEntry.prevSegment = layerSegment;
											const auto dirHemisphere = sampleDirection(ps, rbEntry, trEntry, flags[FlagFirst] ? rnd : RandomReals<2>{uniformRealRng<>()(rng), uniformRealRng<>()(rng)}, std::get<SurfaceInteraction>(*layerSegment.end), odir);
											if(dirHemisphere == EnumHemisphere::Invalid)
												break;

											// Russian roulette
											trEntry.contProb = getContProbFromRay(rbEntry.ray, trEntry.albedo);
											if(!doRR(trEntry.contProb))
												break;

											const auto bxdfHemisphere = flags[FlagReversed] ? swapHemisphere(dirHemisphere) : dirHemisphere;
											const auto hemisphere = layerOrient == EnumHemisphere::North ? swapHemisphere(bxdfHemisphere) : bxdfHemisphere;

											// Check if the ray is exiting the material
											if((hemisphere == EnumHemisphere::South && std::get<SurfaceInteraction>(*layerSegment.end).surface().layerIndex == (if32)material.mLayers.size() - 1) || (hemisphere == EnumHemisphere::North && std::get<SurfaceInteraction>(*layerSegment.end).surface().layerIndex == 0))
											{
												if(ps.nbBounces + 1 >= this->getMaxPathLength())
													break;

												flags.set(FlagValid, true);

												trEntry.segment.insideLayer = false;

												rbEntry.ray.setupPathRay();
												pl::visit_one(*trEntry.segment.start, [&](const auto &i){
														rbEntry.ray.origin = i.safeIncrement(oray->origin, i.worldPos(), dirHemisphere);
													});

												trEntry.subsEntryEvent = ps.substanceStack.checkSubstanceEntry(bxdfHemisphere);

												break;
											}

											if(nbLayerBounces >= mNbLayerBouncesMax)
												break;

											const auto lastLayerIndex = std::get<SurfaceInteraction>(*layerSegment.end).surface().layerIndex;
											layerSegment.next(&PL_FWD(createInteractionFc)(*layerSegment.end));
											trEntry.segment = {layerSegment, nullptr};
											trEntry.segment.insideLayer = true;

											const auto &lastLayer = material.mLayers[lastLayerIndex];
											const if32 ldir = hemisphere == EnumHemisphere::South ? 1 : -1;
											SurfaceInteraction &interaction = std::get<SurfaceInteraction>(*layerSegment.end);
											interaction.surface().layerIndex = lastLayerIndex + ldir;

											updateThroughput(ps, trEntry);

											odir = rbEntry.ray.dir;

											if(isAlmostBlack(ps.throughput))
												break;

											const auto &curLayer = material.mLayers[interaction.surface().layerIndex];

											// Adjust entered and next substance
											if(dirHemisphere == EnumHemisphere::South)
											{
												if(hemisphere == EnumHemisphere::South)
												{
													interaction.enteredSub = &lastLayer;
													interaction.nextSubstance = &curLayer;
												}
												else
												{
													const auto nextLayer = interaction.surface().layerIndex + ldir;

													interaction.enteredSub = &curLayer;
													interaction.nextSubstance =
														nextLayer < 0 || nextLayer == (if32)material.mLayers.size() ?
															subs[hemisphereIndex(bxdfHemisphere)] :
															&material.mLayers[nextLayer];
												}

												if(!interaction.enteredSub->isSubstance())
													interaction.enteredSub = subs[0];
											}
											else
											{
												interaction.surface().reverse();
												layerLobe.reverse();
												flags.flip(FlagReversed);

												const auto nextLayer = interaction.surface().layerIndex + ldir;

												interaction.nextSubstance =
													hemisphere == EnumHemisphere::South ? &curLayer :
													(nextLayer < 0 || nextLayer == (if32)material.mLayers.size() ?
														subs[hemisphereIndex(bxdfHemisphere)] :
														&material.mLayers[nextLayer]);
											}

											flags.set(FlagFirst, false);
										}

										return flags[FlagValid];
									});

							});
					}

				}


				// End the path if no rays has been generated
				if(batch.front().empty())
					break;

				// Trace batch of rays
				traceBatch(tracer.rayTracer, batch.front(), traversalEntries.front(), PL_FWD(createInteractionFc));

				// Split the path with recursion for all but the last one
				for(auto&& [collision, trEntry] : rgv::zip(batch.front(), traversalEntries.front()) | rgv::drop_last(1))
					doRecursionTrace(pathState, collision, trEntry);

				{
					auto &collision = batch.front().back();
					auto &trEntry = traversalEntries.front().back();

					updateThroughput(pathState, trEntry);
					segment = trEntry.segment;
					oray = &collision.ray;

					pathState.substanceStack.updateSubstanceStack(trEntry.subsEntryEvent);
				}

				batch.flip();
				batch.front().clear();
				traversalEntries.flip();
				traversalEntries.front().clear();

				#ifndef NDEBUG
					++debugIterationCounter;
				#endif
			}
			while(true);

			terminatePath(pathState, *oray, segment);
		}

	};
}
