/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Camera_fwd.hpp"
#include "PixelSampler_fwd.hpp"
#include "Film_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"
#include <PhotoRT/Bitmap/Bitmap.hpp>
#include "Filter.hpp"
#include "ToneMapper.hpp"
#include "ColorProfile.hpp"
#include "FilmBitmap.hpp"

#include <patlib/mutex.hpp>
#include <patlib/clone.hpp>
#include <patlib/rectangle.hpp>
#include <patlib/task.hpp>

#include <shared_access/shared.hpp>

#include <experimental/propagate_const>

#include <boost/atomic/atomic.hpp>

#ifdef PRT_IPC
	#include <boost/interprocess/sync/interprocess_sharable_mutex.hpp>
	#include <boost/interprocess/containers/vector.hpp>
#endif

namespace PhotoRT
{
	using ContBufferBitmap = BitmapBuffer<
		/*#if PL_MAX_XMM_SIZE_FLOAT
			ImplBitmap::BitmapPackedBuffer<
				FilmColor, PackedColorValue32, size_c<FilmColor::NbValues>,
				pl::allocator_with_stats<FilmColor, memtag::Film, PmrAllocatorGlobal<FilmColor, memtag::ContBuffer>>
			>
		#else*/
			ImplBitmap::BitmapBuffer<
				FilmColor, pl::allocator_with_stats<FilmColor, memtag::Film, PmrAllocatorGlobal<FilmColor, memtag::ContBuffer>>
			>
		//#endif
	>;

	#ifdef PRT_IPC
		template <typename T_>
		using ContBufferVector = boost::interprocess::vector<T_, pl::allocator_with_stats<T_, memtag::Film, PmrAllocatorGlobal<T_, memtag::ContBuffer>>>;
	#else
		template <typename T_>
		using ContBufferVector = std::vector<T_, pl::allocator_with_stats<T_, memtag::Film, PmrAllocatorGlobal<T_, memtag::ContBuffer>>>;
	#endif

	struct PixelSample
	{
		Point2Dr p;
		Real weight;
		Color color;
		szt layerIndex;
	};

	class IndividualPixelSampleList
	{
	public:
		VectorWithStats<PixelSample, memtag::ContBuffer> individualSamples;

		void addSample(Real x, Real y, Real weight, Color color, szt layerIndex)
		{
			individualSamples.push_back({
				{x, y},
				pl::assert_valid(weight, valid::Weight),
				pl::assert_valid(color),
				layerIndex
			});
		}

		void addSample(const PixelSample &sp)
		{
			pl::assert_valid(sp.color);
			pl::assert_valid(sp.weight, valid::Weight);

			individualSamples.push_back(sp);
		}

		void clear()
			{ individualSamples.clear(); }
	};

	struct PhotonSamples
	{
		uf32 nbSamples = 0;

		void clear() noexcept
			{ nbSamples = 0; }

	};

	struct PhotonSampleList
	{
		VectorWithStats<PhotonSamples, memtag::ContBuffer> list;

		void clear() noexcept
		{
			for(auto &l : list)
				l.clear();
		}
	};

	struct PixelSampleTile
	{
		VectorWithStats<Color, memtag::ContBuffer> color;

		void addContribution(const if32 offset, const Color c) noexcept
			{ color[offset] += pl::assert_valid(c); }

		void zero() noexcept
			{ color.assign(color.size(), pl::tag::zero); }
	};

	struct PixelTile
	{
		Real rayImportance, rayDistributionFactor;
		Point2Di bufferStart, tilePos;
		Dir2Di bufferSize;
		pl::rectangle<> tileDims;
		uf32 samplerIndex, tileSampleCount, tileIndex;

		IPixelSampler *pixelSampler;
		const ICamera *camera;

		Dir2Dr filterSize;
		const ColorFilter2D *filter;
	};

	struct PixelSampleTileLayers : public PixelTile
	{
		VectorWithStats<PixelSampleTile, memtag::ContBuffer> tiles;
		Real weight = 0;

		void addSample(Real w = 1) noexcept
			{ weight += w; }

		void addContribution(const Point2Dr p, const Color c, szt index = 0) noexcept
		{
			if(filter)
				BitmapAlgo::splatColor(*filter, filterSize, p + bufferStart,
					[&](auto&& pos, auto&& weight)
						{ tiles[index].addContribution(pos[1] * bufferSize[0] + pos[0], c * weight); }
				);
			else
				tiles[index].addContribution(getOffset(p), c);
		}

		[[nodiscard]] if32 getOffset(const Point2Di p) const noexcept
			{ return (p[1] + bufferStart[1]) * bufferSize[0] + p[0] + bufferStart[0]; }

		[[nodiscard]] Color getContribution(const if32 offset) const noexcept
		{
			Color res{};

			for(const auto &t : tiles)
				res += t.color[offset];

			return res;
		}

		[[nodiscard]] Color getContribution(const Point2Di p) const noexcept
			{ return getContribution(getOffset(p)); }

		void zero() noexcept
		{
			weight = 0;

			for(auto &t : tiles)
				t.zero();
		}
	};


	class PHOTORT_API ContributionBuffer : public RenderEntity
	{
	public:

		[[nodiscard]] virtual std::string_view getType() const noexcept = 0;
		virtual void zeroBuffers() = 0;
		virtual void merge(const ContributionBuffer &contributionBuffer) = 0;
		virtual void mergeAndZero(ContributionBuffer &contributionBuffer) = 0;
		virtual void copyFrom(const ContributionBuffer &contributionBuffer) = 0;

		virtual void normalize(FilmBitmap &into, szt index) const = 0;

		virtual void setRenderRect(pl::rectangle<> r) = 0;
		[[nodiscard]] virtual pl::rectangle<> getRenderRect() const noexcept = 0;

		void setCamera(EntityPtr<const ICamera> c) noexcept { mCamera = std::move(c); }
		[[nodiscard]] const auto &getCamera() const noexcept { return mCamera; }

		[[nodiscard]] std::string_view getFilmID() const noexcept { return mFilmID; }
		void setFilmID(std::string id) { mFilmID = std::move(id); }

		[[nodiscard]] bool is_owner() const noexcept { return mOwner; }
		void setOwner(bool v) { mOwner = v; }

		[[nodiscard]] virtual szt getNbLayers() const = 0;
		virtual void setNbLayers(szt v) = 0;

		void setFilm(const Film *f) noexcept { mFilm = f; }
		[[nodiscard]] const Film *getFilm() const noexcept { return mFilm; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

		[[nodiscard]] virtual bool hasData(bool ipcStream = false) const noexcept override { return !ipcStream; }
		[[nodiscard]] virtual bool canHaveData() const noexcept override { return true; }

	protected:

		std::string mFilmID;
		const Film *mFilm = nullptr;
		EntityPtr<const ICamera> mCamera;
		bool mOwner = true;

	};


	class PHOTORT_API ContributionBufferPixel : public ContributionBuffer
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("contribution_buffer-pixel", ContributionBufferPixel)

		using SampleWeightBitmap = BitmapBuffer<
			#if PL_MAX_XMM_SIZE_FLOAT
				ImplBitmap::BitmapAlignedBuffer<
					FilmWeight, PackedColorValue32, pl::allocator_with_stats<FilmWeight, memtag::ContBuffer, PmrAllocatorGlobal<FilmWeight, memtag::ContBuffer>>
				>
			#else
				ImplBitmap::BitmapBuffer<
					FilmWeight, pl::allocator_with_stats<FilmWeight, memtag::ContBuffer, PmrAllocatorGlobal<FilmWeight, memtag::ContBuffer>>
				>
			#endif
		>;

		[[nodiscard]] virtual std::string_view getType() const noexcept override { return "pixel"; }

		virtual void setEntityId(std::string v) override;

		virtual void setRenderRect(pl::rectangle<> rect) override;
		[[nodiscard]] virtual pl::rectangle<> getRenderRect() const noexcept override { return mData->rect; }

		virtual void zeroBuffers() override;

		[[nodiscard]] virtual szt getNbLayers() const override { return mData->nbLayers; }
		virtual void setNbLayers(szt v) override;

		void addContributionTile(PixelSampleTileLayers &tile);

		virtual void merge(const ContributionBuffer &contributionBuffer) override;
		virtual void mergeAndZero(ContributionBuffer &contributionBuffer) override;
		virtual void copyFrom(const ContributionBuffer &contributionBuffer) override;

		virtual void normalize(FilmBitmap &into, szt index) const override;

		virtual void serialize(std::ostream &outputStream) const override;
		virtual void unserialize(std::istream &inputStream) override;

		[[nodiscard]] auto getTileSize() const noexcept { return mData->tileSize; }
		[[nodiscard]] auto getPadding() const noexcept { return static_cast<Dir2Di>(mData->tileStart); }
		[[nodiscard]] auto getNbTiles() const noexcept { return mData->nbTiles; }

	protected:

		struct BufferData
		{
			struct Buffers
			{
				ContBufferVector<ContBufferBitmap> hdrBuffers;
				SampleWeightBitmap samplesWeight;
			};

			using SharedBufferType =
				sa::shared<Buffers,
					#ifdef PRT_IPC
						pl::boost_mutex_adaptor<boost::interprocess::interprocess_sharable_mutex>
					#else
						std::shared_mutex
					#endif
				>;

			Dir2Di size{}, nbTiles{}, tileSize{};
			Point2Di tileStart{};
			pl::rectangle<> rect{}, bufferRect{};
			szt nbLayers = 0;

			SharedBufferType buffers;
		};

		pl::memory_system_pointer<BufferData, pl::IMemorySystem<VoidOffsetPtr>> mData{};

	};


	class PHOTORT_API ContributionBufferLight : public ContributionBuffer
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("contribution_buffer-light", ContributionBufferLight)

		[[nodiscard]] virtual std::string_view getType() const noexcept override { return "light"; }

		virtual void setEntityId(std::string v) override;

		virtual void setRenderRect(pl::rectangle<> rect) override;
		[[nodiscard]] virtual pl::rectangle<> getRenderRect() const noexcept override { return mData->rect; }

		virtual void zeroBuffers() override;

		[[nodiscard]] const auto &getBuffer(szt index = 0) const noexcept { return mData->hdrBuffers[index]; }
		[[nodiscard]] auto &getBuffer(szt index = 0) noexcept { return mData->hdrBuffers[index]; }

		[[nodiscard]] virtual szt getNbLayers() const override { return mData->hdrBuffers.size(); }
		virtual void setNbLayers(szt v) override;

		void addSamples(double nb = 1) noexcept { mData->nbSamples.fetch_add(nb, std::memory_order::relaxed); }

		void addContribution(const PixelSample &sp);

		void addIndividualContributionList(const IndividualPixelSampleList &list)
		{
			for(const auto &s : list.individualSamples)
				addContribution(s);
		}

		virtual void merge(const ContributionBuffer &contributionBuffer) override;
		virtual void mergeAndZero(ContributionBuffer &contributionBuffer) override;
		virtual void copyFrom(const ContributionBuffer &contributionBuffer) override;

		virtual void normalize(FilmBitmap &into, szt index) const override;

		virtual void serialize(std::ostream &outputStream) const override;
		virtual void unserialize(std::istream &inputStream) override;

	protected:

		struct AtomicColor
		{
			std::array<pl::atomic_wrapper<std::atomic<CReal>>, 3> c;

			[[nodiscard]] auto &operator[] (szt i) noexcept { return c[i]; }
			[[nodiscard]] const auto &operator[] (szt i) const noexcept { return c[i]; }

			auto &operator+= (const AtomicColor &o) noexcept
			{
				for(const auto i : mir(c.size()))
					c[i].fetch_add(o[i].load(std::memory_order::relaxed), std::memory_order::relaxed);
				return *this;
			}

			auto &operator-= (const AtomicColor &o) noexcept
			{
				for(const auto i : mir(c.size()))
					c[i].fetch_sub(o[i].load(std::memory_order::relaxed), std::memory_order::relaxed);
				return *this;
			}

			auto &operator+= (const std::array<CReal, 3> &o) noexcept
			{
				for(const auto i : mir(c.size()))
					c[i].fetch_add(o[i], std::memory_order::relaxed);
				return *this;
			}

			auto &operator-= (const std::array<CReal, 3> &o) noexcept
			{
				for(const auto i : mir(c.size()))
					c[i].fetch_sub(o[i], std::memory_order::relaxed);
				return *this;
			}

			auto &operator+= (const Color &o) noexcept
			{
				const auto &ca = pl::simd::vector_access(o);

				for(const auto i : mir(c.size()))
					c[i].fetch_add(ca[i], std::memory_order::relaxed);
				return *this;
			}

			auto &operator-= (const Color &o) noexcept
			{
				const auto &ca = pl::simd::vector_access(o);

				for(const auto i : mir(c.size()))
					c[i].fetch_sub(ca[i], std::memory_order::relaxed);
				return *this;
			}

			auto &operator*= (const AtomicColor &o) noexcept
			{
				for(const auto i : mir(c.size()))
					c[i].exchange(c[i].load(std::memory_order::relaxed) * o[i].load(std::memory_order::relaxed), std::memory_order::relaxed);
				return *this;
			}

			auto &operator/= (const AtomicColor &o) noexcept
			{
				for(const auto i : mir(c.size()))
					c[i].exchange(c[i].load(std::memory_order::relaxed) / o[i].load(std::memory_order::relaxed), std::memory_order::relaxed);
				return *this;
			}

			auto &operator*= (const std::array<CReal, 3> &o) noexcept
			{
				for(const auto i : mir(c.size()))
					c[i].exchange(c[i].load(std::memory_order::relaxed) * o[i], std::memory_order::relaxed);
				return *this;
			}

			auto &operator/= (const std::array<CReal, 3> &o) noexcept
			{
				for(const auto i : mir(c.size()))
					c[i].exchange(c[i].load(std::memory_order::relaxed) / o[i], std::memory_order::relaxed);
				return *this;
			}

			auto &operator*= (const Color &o) noexcept
			{
				const auto &ca = pl::simd::vector_access(o);

				for(const auto i : mir(c.size()))
					c[i].exchange(c[i].load(std::memory_order::relaxed) * ca[i], std::memory_order::relaxed);
				return *this;
			}

			auto &operator/= (const Color &o) noexcept
			{
				const auto &ca = pl::simd::vector_access(o);

				for(const auto i : mir(c.size()))
					c[i].exchange(c[i].load(std::memory_order::relaxed) / ca[i], std::memory_order::relaxed);
				return *this;
			}

			[[nodiscard]] auto begin() { return c.begin(); }
			[[nodiscard]] auto begin() const { return c.begin(); }
			[[nodiscard]] auto cbegin() const { return c.cbegin(); }

			[[nodiscard]] auto rbegin() { return c.rbegin(); }
			[[nodiscard]] auto rbegin() const { return c.rbegin(); }
			[[nodiscard]] auto crbegin() const { return c.crbegin(); }

			[[nodiscard]] auto end() { return c.end(); }
			[[nodiscard]] auto end() const { return c.end(); }
			[[nodiscard]] auto cend() const { return c.cend(); }

			[[nodiscard]] auto rend() { return c.rend(); }
			[[nodiscard]] auto rend() const { return c.rend(); }
			[[nodiscard]] auto crend() const { return c.crend(); }

			[[nodiscard]] bool empty() const noexcept { return c.empty(); }
			[[nodiscard]] auto size() const noexcept { return c.size(); }
			[[nodiscard]] auto max_size() const noexcept { return c.max_size(); }

			friend std::ostream &operator<< (std::ostream &os, const AtomicColor &v) noexcept
			{
				return (os << pl::io::atomic_range(v.c, pl::io::mode_text));
			}

			friend std::ostream &operator<= (std::ostream &os, const AtomicColor &v) noexcept
			{
				return (os << pl::io::atomic_range(v.c, pl::io::mode_binary));
			}

			friend std::istream &operator>>(std::istream &is, AtomicColor &v)
			{
				return (is >> pl::io::atomic_range(v.c, pl::io::mode_text));
			}

			friend std::istream &operator>=(std::istream &is, AtomicColor &v)
			{
				return (is >> pl::io::atomic_range(v.c, pl::io::mode_binary));
			}
		};

		using AtomicBitmap = BitmapBuffer<
			ImplBitmap::BitmapBuffer<
				AtomicColor,
				pl::allocator_with_stats<AtomicColor, memtag::ContBuffer, PmrAllocatorGlobal<AtomicColor, memtag::ContBuffer>>
			>
		>;

		struct BufferData
		{
			pl::atomic_wrapper<std::atomic<double>> nbSamples;
			ContBufferVector<AtomicBitmap> hdrBuffers;

			Dir2Di size{}, filterTotalSize{};
			if32 nbColors{};
			pl::rectangle<> rect{}, bufferRect{};
		};

		pl::memory_system_pointer<BufferData, pl::IMemorySystem<VoidOffsetPtr>> mData{};
	};


	class PHOTORT_API ContributionBufferAggregator
	{
	public:

		std::vector<EntityPtr<ContributionBuffer>> mBuffers;
		EntityPtr<ContributionBuffer> mFinalBuffer;
		std::vector<FilmBitmap> mNormalizedBuffers;

		void compile();
		void normalize();
		void zero();

	};

	struct LayerParams
	{
		CReal power = 1;
	};

	class PHOTORT_API Film : public RenderEntity
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("film-", Film)

		virtual void zero();

		void setSize(Dir2Di s);
		[[nodiscard]] auto getSize() const noexcept { return mSize; }

		[[nodiscard]] auto getBufferSize() const noexcept { return mBufferSize; }

		[[nodiscard]] const auto &getBitmap() const noexcept { return mHdrBitmap; }
		[[nodiscard]] auto &getBitmap() noexcept { return mHdrBitmap; }

		[[nodiscard]] virtual ContBufferBitmap getCroppedBitmap() const;

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

		virtual void setToneMapper(EntityPtr<IToneMapper> tm) noexcept { mToneMapper = std::move(tm); }
		[[nodiscard]] const auto &getToneMapper() const noexcept { return mToneMapper; }

		void setFilterSize(const Dir2Dr s) noexcept { mFilter.mSize = s; }
		[[nodiscard]] auto getFilterSize() const noexcept { return mFilter.mSize; }

		void setFilterTargetSize(const Dir2Dr s) noexcept { mFilterTargetSize = s; calcBufferSize(); }
		[[nodiscard]] auto getFilterTargetSize() const noexcept { return mFilterTargetSize; }

		virtual void setFilterDist(EntityPtr<const IColorFilterDistribution2D> filterDist) noexcept;
		[[nodiscard]] const auto &getFilterDist() const noexcept { return mFilter.mDistribution; }

		[[nodiscard]] const auto &getFilter() const noexcept { return mFilter; }
		void initFilter();

		virtual void setCamera(EntityPtr<ICamera> c) { mCamera = std::move(c); }
		[[nodiscard]] const auto &getCamera() const noexcept { return mCamera; }

		virtual void setColorProfile(EntityPtr<const IColorProfile> cp) { mColorProfile = std::move(cp); }
		[[nodiscard]] const auto &getColorProfile() const noexcept { return mColorProfile; }

		virtual void initFilmBuffer();

		[[nodiscard]] pl::rectangle<> getFilmBounds() const { return mFilmBounds; }

		void setNbLayers(i32 nb) { mNbLayers = nb; }
		[[nodiscard]] auto getNbLayers() const noexcept { return mNbLayers; }

		template <typename Bitmap_>
		void applyToneMap(Bitmap_ &intoBitmap) const
		{
			mToneMapper->prepare(mHdrBitmap, *mColorProfile);
			BitmapAlgo::applyTonemap(mHdrBitmap, *mToneMapper, intoBitmap, mFilmBounds, {}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*16));
		}

	protected:

		void calcBufferSize();

		EntityPtr<const IColorProfile> mColorProfile;
		Dir2Di mSize{}, mBufferSize{};
		Dir2Dr mFilterTargetSize{};
		pl::rectangle<> mFilmBounds{};
		if32 mNbLayers = 1;

		ColorFilter2D mFilter;
		EntityPtr<IToneMapper> mToneMapper;

		EntityPtr<ICamera> mCamera;

		FilmBitmap mHdrBitmap;
	};

	class PHOTORT_API ContributionToFilm
	{
	public:
		sa::shared<std::experimental::propagate_const<EntityPtr<Film>>> mFilm;

		std::map<std::string, ContributionBufferAggregator, std::less<>> mAggregators;

		void registerBuffer(EntityPtr<ContributionBuffer> contribBuffer);
		void unregisterBuffer(const EntityPtr<ContributionBuffer> &contribBuffer);
		void compile(i32 index = -1);
		void zeroBuffers();
		void zeroFilm();
		void initBuffers();

		void setNbLayers(i32 nb);
		[[nodiscard]] i32 getNbLayers() const noexcept { return mNbLayers; }

		void setLayerParams(const std::vector<LayerParams> *p) { mLayerParams = p; }
		[[nodiscard]] const std::vector<LayerParams> *getLayerParams() const noexcept { return mLayerParams; }

		friend PHOTORT_API std::istream &operator>> (std::istream &is, ContributionToFilm &cf);
		friend PHOTORT_API std::ostream &operator<< (std::ostream &os, const ContributionToFilm &cf);

	private:
		void initAggregator(ContributionBufferAggregator &ag, std::string_view type);

		i32 mNbLayers = 1;
		const std::vector<LayerParams> *mLayerParams = nullptr;
	};


	class PHOTORT_API RenderContext : public RenderEntity
	{
	protected:
		i32 mNbLayers = 1;

	public:
		PRT_ENTITY_FACTORY_DECLARE("rendercontext-", RenderContext)

		std::map<std::string, ContributionToFilm, std::less<>> mFilms;
		ParameterTree mRenderParams;
		std::vector<LayerParams> mLayerParams;

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

		void setNbLayers(i32 nb);
		[[nodiscard]] i32 getNbLayers() const noexcept { return mNbLayers; }

		virtual void zeroFilms();
		virtual void zeroBuffers();
		virtual void initBuffers();
		virtual void initFilters();
		virtual void initFilmBuffers();

		virtual void initCamera();

		virtual void registerBuffer(EntityPtr<ContributionBuffer> contribBuffer);
		virtual void unregisterBuffer(const EntityPtr<ContributionBuffer> &contribBuffer);
		virtual void deleteBuffers();
	};

}
