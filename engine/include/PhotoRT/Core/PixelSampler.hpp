/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Film_fwd.hpp"
#include "Camera_fwd.hpp"
#include "PixelSampler_fwd.hpp"
#include "Scene_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"
//#include "RayTracer.hpp"
#include "Distribution.hpp"
#include "ColorHalf.hpp"

namespace PhotoRT
{
	class IPixelSampler
	{
	public:
		virtual ~IPixelSampler() = default;

		virtual void setDimensions(Dir2Di tileSize, Dir2Di nbTiles, Dir2Di padding, Dir2Di size) = 0;

		virtual void setCamera(const ICamera &c) = 0;
		[[nodiscard]] virtual const ICamera &getCamera() const = 0;

		virtual bool generateTile(PixelTile &tile, IRNGInt &numberSequence) = 0;
		virtual void tileDone(PixelSampleTileLayers &tile) = 0;

		[[nodiscard]] virtual bool done() const = 0;

		virtual void dumpInfo(Scene &scene, const std::filesystem::path &folder, const std::string &name) = 0;

		virtual void reset() = 0;

		virtual void init() = 0;
	};


	class PHOTORT_API PixelSamplerBase : public RenderEntity, public IPixelSampler
	{
	public:

		virtual void setDimensions(Dir2Di /*tileSize*/, Dir2Di /*nbTiles*/, Dir2Di /*padding*/, Dir2Di size) override
		{
			mSize = size;
		}

		virtual void setCamera(const ICamera &c) override { mCamera = &c; }
		[[nodiscard]] virtual const ICamera &getCamera() const override { return *mCamera; }

		virtual void tileDone(PixelSampleTileLayers &) override {}

		[[nodiscard]] if32 getSPP() const noexcept { return mSpp; }
		void setSPP(if32 nb) noexcept { mSpp = nb; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

		virtual void dumpInfo(Scene &/*scene*/, const std::filesystem::path &/*folder*/, const std::string &/*name*/) override {}

		virtual void reset() override { mSequenceCount = 0; }

		virtual void init() override;

		//[[nodiscard]] virtual bool hasData(bool ipcStream = false) const noexcept { return true; }
		virtual void serialize(std::ostream &outputStream) const override;
		virtual void unserialize(std::istream &inputStream) override;

	protected:

		umint mNbSamplesTotal = 0;
		if32 mSequenceCount = 0, mSpp = 0;
		const ICamera *mCamera = nullptr;
		Dir2Di mSize;

	};



	class PHOTORT_API PixelSamplerUniform : public PixelSamplerBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("pixelsampler-uniform", PixelSamplerUniform)

		virtual void setDimensions(Dir2Di tileSize, Dir2Di nbTiles, Dir2Di padding, Dir2Di size) override;

		virtual bool generateTile(PixelTile &tile, IRNGInt &numberSequence) override;

		[[nodiscard]] virtual bool done() const override { return mTileSampleCount >= mSpp; }

		virtual void reset() override;
		virtual void init() override;

	protected:

		HilbertCurve2D<if32> mSpaceFillingCurve;

		Point2Di advanceCurve()
		{
			return (mSpaceFillingCurve.advance() + mTileMid) % mNbTiles;
		}

		void generateTileIndex(PixelTile &samples, if32 mSequenceIndex);
		void generateTileIndex(PixelTile &samples, const Point2Di pos);

		Dir2Di mTileMid, mNbTiles, mTileSize, mPadding, mTileSizePad;
		if32 mNbTilesTotal = 0, mTileSampleCount = 0;

	};


	class PHOTORT_API PixelSamplerVariance : public PixelSamplerUniform
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("pixelsampler-variance", PixelSamplerVariance)

		virtual bool generateTile(PixelTile &tile, IRNGInt &numberSequence) override;
		virtual void tileDone(PixelSampleTileLayers &tile) override;

		void setWeightCap(u8 cap) noexcept { mWeightCap = cap; }
		[[nodiscard]] u8 getWeightCap() const noexcept { return mWeightCap; }

		void setNbWeightCalculatedCap(umint cap) noexcept { mNbWeightCalculatedCap = cap; }
		[[nodiscard]] umint getNbWeightCalculatedCap() const noexcept { return mNbWeightCalculatedCap; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

		virtual void reset() override;

		virtual void init() override;

		virtual void dumpInfo(Scene &scene, const std::filesystem::path &folder, const std::string &name) override;

		[[nodiscard]] virtual bool canHaveData() const noexcept override { return true; }
		virtual void serialize(std::ostream &outputStream) const override;
		virtual void unserialize(std::istream &inputStream) override;

	protected:

		std::vector<if32> mTilesSampleCount;
		std::vector<Real> mTileVariance;
		std::vector<u8> mTileWeights;
		std::vector<Color3_16> mRecordedTiles;
		std::vector<bool> mTileRecorded;
		umint mNbVarianceReported = 0, mNbWeightCalculated = 0, mNbWeightCalculatedCap = 20;
		umint mNbTilesWeighted = 0;
		u8 mWeightCap = 10;
		if32 mTileSeqIndex = 0, mSequenceIndex = 0;

		void calculateTileVariance();
	};
}
