/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "RayTracer_fwd.hpp"
#include "BxDF_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"
#include "SurfaceMapper.hpp"
#include "Normals.hpp"
#include "Distribution.hpp"
#include "Interaction.hpp"
#include "Ray.hpp"

#include <patlib/marked_optional.hpp>

namespace PhotoRT
{
	template <pl::simd::CColorVector ColorT_>
	[[nodiscard]] constexpr Real alphaProbability(ColorT_ alpha) noexcept
		{ return isAlmostBlack(alpha) ? 0 : (isAlmostWhite(alpha) ? 1 : pl::plclamp<Real>(getColorY(alpha), 0.4, 0.6)); }

	template <pl::CNumberPackFloat T_>
	[[nodiscard]] constexpr T_ alphaPDF(const T_ ap) noexcept
		{ return ap * pl::reciprocal(1 - ap); }

	template <pl::CNumberPackFloat T_>
	[[nodiscard]] constexpr T_ transparencyPDF(const T_ ap) noexcept
		{ return (1 - ap) * pl::reciprocal(ap); }

	template <pl::simd::CColorVector ColorT_>
	[[nodiscard]] constexpr ColorT_ dualLayerRefr(const ColorT_ r) noexcept
	{
		const auto t = constants::white<ColorT_> - r /opow2;
		return (constants::white<ColorT_> - r) /opow2 / pl::select(t, constants::white<ColorT_>, t > 0);
	}

	template <pl::simd::CColorVector ColorT_>
	[[nodiscard]] constexpr ColorT_ dualLayerRefl(const ColorT_ r) noexcept
		{ return r + r * dualLayerRefr(r); }

	template <pl::simd::CColorVector ColorT_>
	[[nodiscard]] constexpr ColorT_ dualLayerRefl(const ColorT_ r, const ColorT_ bottomColor, const ColorT_ tr) noexcept
	{
		const auto t = constants::white<ColorT_> - r;
		const auto bt = bottomColor * t * tr;

		return bt +
			bt /opow2 * (
				r + r * bottomColor * (pl::select(t /oprtreciprocal, constants::white<ColorT_>, t > 0) - constants::white<ColorT_>)
			);
	}

	struct SampledHalfVector
	{
		VecD dir;
		Real D, pdf = 0;

		constexpr void reset() noexcept { pdf = 0; }
		[[nodiscard]] constexpr bool has_value() const noexcept { return pdf != 0; }
	};

	// Microfacets distribution
	class IMicrofacetsDistribution
	{
	public:
		virtual ~IMicrofacetsDistribution() = default;

		[[nodiscard]] virtual Real distribution(const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD halfVector) const noexcept = 0;
		[[nodiscard]] virtual pl::marked_optional<SampledHalfVector> sampleHalfVector(RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir) const noexcept = 0;
		[[nodiscard]] virtual Color shadowMasking(const SurfaceInteraction &inter, const VecD dir, const VecD halfVector) const noexcept = 0;
		[[nodiscard]] virtual Color shadowMasking2(const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD halfVector) const noexcept = 0;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD halfVector) const noexcept = 0;
		[[nodiscard]] virtual bool isAnisotropic() const noexcept = 0;

		[[nodiscard]] auto operator() (const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD halfVector) const noexcept
			{ return distribution(inter, odir, idir, halfVector); }

	};

	class PHOTORT_API MicrofacetsBase : public RenderEntity, public IMicrofacetsDistribution
	{
	public:
		EntityPtr<const IRealMapper> mParam;

		[[nodiscard]] virtual Color shadowMasking2(const SurfaceInteraction &inter, const VecD odir, const VecD idir, const VecD halfVector) const noexcept override
		{
			return shadowMasking(inter, odir, halfVector) * shadowMasking(inter, -idir, halfVector);
		}

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;
	};

	// Fresnel term

	template <typename T_>
	[[nodiscard]] constexpr T_ schlickR0toIndex(const T_ R0) noexcept
	{
		const auto
			R0sqr = pl::plsqrt(R0),
			v1 = -2 / (R0sqr - 1) - 1,
			v2 = 2 / (R0sqr + 1) - 1;

		return v1 /oisnan ? v2 : (v2 /oisnan ? v1 : v1 /omax/ v2);
	}

	class IFresnel
	{
	public:
		virtual ~IFresnel() = default;

		[[nodiscard]] virtual Color fresnel(EnumTransportMode mode, const VecD normal, const VecD odir, const SurfaceInteraction &inter) const noexcept = 0;
		[[nodiscard]] virtual pl::marked_optional<VecD> refract(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD halfVector) const noexcept = 0;

		[[nodiscard]] auto operator() (EnumTransportMode mode, const VecD normal, const VecD odir, const SurfaceInteraction &inter) const noexcept
			{ return fresnel(mode, normal, odir, inter); }
	};

	class PHOTORT_API FresnelBase : public RenderEntity, public IFresnel
	{
	public:

		[[nodiscard]] Real getIOR() const noexcept { return ior; }
		void setIOR(Real i) { ior = pl::plmax<Real>(i, 0); iorInv = !ior ? 0 : 1.0 / ior; }
		[[nodiscard]] virtual pl::marked_optional<VecD> refract(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD halfVector) const noexcept override;

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:
		Real ior = 1, iorInv = 1;
	};

	template <pl::simd::CColorVector ColorT_>
	struct ColorPair
	{
		ColorT_ color, alpha;
	};

	template <typename ColorT_>
	[[nodiscard]] constexpr ColorPair<ColorT_> operator* (const ColorPair<ColorT_> c1, const ColorPair<ColorT_> c2) noexcept
		{ return {c1.color * c2.color, c1.alpha * c2.alpha}; }

	template <typename ColorT_>
	[[nodiscard]] constexpr ColorPair<ColorT_> operator+ (const ColorPair<ColorT_> c1, const ColorPair<ColorT_> c2) noexcept
		{ return {c1.color + c2.color, c1.alpha + c2.alpha}; }

	template <typename ColorT_>
	[[nodiscard]] constexpr bool tag_invoke(pl::tag_t<pl::is_valid>, const ColorPair<ColorT_> &c, const pl::tag::nothing_t) noexcept
		{ return pl::is_valid(c.color) && pl::is_valid(c.alpha); }

	template <typename ColorT_>
	[[nodiscard]] constexpr bool isnan(const ColorPair<ColorT_> c) noexcept
		{ return pl::mask_any(pl::plisnan(c.color) | pl::plisnan(c.alpha)); }

	template <typename ColorT_>
	[[nodiscard]] constexpr bool isinf(const ColorPair<ColorT_> c) noexcept
		{ return pl::mask_any(pl::plisinf(c.color) | pl::plisinf(c.alpha)); }

	template <typename ColorT_>
	[[nodiscard]] constexpr bool isBlack(const ColorPair<ColorT_> c) noexcept
		{ return isBlack(c.color) && isBlack(c.alpha); }

	template <typename ColorT_>
	[[nodiscard]] constexpr bool isAlmostBlack(const ColorPair<ColorT_> c) noexcept
		{ return isAlmostBlack(c.color) && isAlmostBlack(c.alpha); }

	template <typename ColorT_>
	[[nodiscard]] constexpr bool isWhite(const ColorPair<ColorT_> c) noexcept
		{ return isWhite(c.color) && isWhite(c.alpha); }

	template <typename ColorT_>
	[[nodiscard]] constexpr bool isAlmostWhite(const ColorPair<ColorT_> c) noexcept
		{ return isAlmostWhite(c.color) && isAlmostWhite(c.alpha); }

	struct SampledBxDF
	{
		VecD dir;
		ColorPair<Color> color;
		CReal albedo;
		Real pdf = 0;
		EnumRayType type;
		EnumHemisphere hemisphere;

		[[nodiscard]] static auto NullOpaque() noexcept
		{
			SampledBxDF sp;
			sp.color.alpha = White;
			return sp;
		}

		[[nodiscard]] static auto NullTransparent(const Color alpha) noexcept
		{
			SampledBxDF sp;
			sp.color.alpha = alpha;
			return sp;
		}

		constexpr void reset() noexcept { pdf = 0; }
		[[nodiscard]] constexpr bool has_value() const noexcept { return pdf != 0; }
	};

	struct BxDFRadianceResult
	{
		ColorPair<Color> color;
		CReal albedo;
	};

	// BxDF
	class IBxDF
	{
	public:
		virtual ~IBxDF() = default;

		[[nodiscard]] virtual pl::marked_optional<SampledBxDF> sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask = EnumRayType::All) const noexcept = 0;
		[[nodiscard]] virtual BxDFRadianceResult radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept = 0;
		[[nodiscard]] virtual Color alpha(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept = 0;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept = 0;
		[[nodiscard]] virtual EnumRayType getType() const noexcept = 0;
		[[nodiscard]] virtual bool isDiracDelta() const noexcept = 0;
		[[nodiscard]] virtual bool isHemisphereValid(EnumHemisphere hemisphere) const noexcept = 0;
		[[nodiscard]] virtual bool isAnisotropic() const noexcept = 0;

		[[nodiscard]] auto operator() (EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept
			{ return radiance(mode, inter, odir, idir); }
	};

	class PHOTORT_API BxDFBase : public RenderEntity, public virtual IBxDF
	{
	public:
		BxDFBase() = default;
		BxDFBase(EntityPtr<const IColorMapper> colorMapper) : mColorMapper{std::move(colorMapper)} {}

		EntityPtr<const IColorMapper> mColorMapper, mAlphaMapper;

		[[nodiscard]] virtual pl::marked_optional<SampledBxDF> sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask = EnumRayType::All) const noexcept override;
		[[nodiscard]] virtual BxDFRadianceResult radiance(EnumTransportMode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual Color alpha(EnumTransportMode, const SurfaceInteraction &inter, const VecD odir, const VecD /*idir*/) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(EnumTransportMode, const SurfaceInteraction &inter, const VecD /*odir*/, const VecD idir) const noexcept override;

		[[nodiscard]] virtual EnumRayType getType() const noexcept override { return EnumRayType::Diffuse; }
		[[nodiscard]] virtual bool isDiracDelta() const noexcept override { return false; }

		[[nodiscard]] virtual bool isHemisphereValid(EnumHemisphere hemisphere) const noexcept override { return hemisphere == EnumHemisphere::North; }
		[[nodiscard]] virtual bool isAnisotropic() const noexcept override { return false; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:

		[[nodiscard]] ColorPair<Color> getColor(const SurfaceInteraction &inter, const VecD odir) const noexcept;

		[[nodiscard]] Color getColorAlpha(const SurfaceInteraction &inter, const VecD odir) const noexcept
			{ return getColor(inter, odir).alpha; }
	};

	class PHOTORT_API BxDFChainBase : public RenderEntity, public virtual IBxDF
	{
	public:
		BxDFChainBase() = default;
		BxDFChainBase(EntityPtr<const IBxDF> other) : mBxdf{std::move(other)} {}

		EntityPtr<const IBxDF> mBxdf;

		[[nodiscard]] virtual BxDFRadianceResult radiance(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual pl::marked_optional<SampledBxDF> sampleDir(EnumTransportMode mode, RandomReals<2> rnd, const SurfaceInteraction &inter, const VecD odir, IRNGInt &rng, EnumRayType typeMask = EnumRayType::All) const noexcept override;
		[[nodiscard]] virtual Color alpha(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(EnumTransportMode mode, const SurfaceInteraction &inter, const VecD odir, const VecD idir) const noexcept override;

		[[nodiscard]] virtual bool isHemisphereValid(EnumHemisphere hemisphere) const noexcept override { return mBxdf->isHemisphereValid(hemisphere); }
		[[nodiscard]] virtual EnumRayType getType() const noexcept override { return mBxdf->getType(); }
		[[nodiscard]] virtual bool isDiracDelta() const noexcept override { return mBxdf->isDiracDelta(); }
		[[nodiscard]] virtual bool isAnisotropic() const noexcept override { return mBxdf->isAnisotropic(); }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:

		virtual SurfaceInteraction getSurfaceInfo(const SurfaceInteraction &inter, const VecD /*odir*/) const noexcept;
	};

}
