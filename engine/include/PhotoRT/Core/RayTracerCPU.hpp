/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Base.hpp"
#include "RayTracer.hpp"
#include "RayTraversalContainers.hpp"

#include <boost/container/pmr/monotonic_buffer_resource.hpp>

#ifdef PL_NUMA
	#include <patlib/numa.hpp>
#endif

namespace PhotoRT
{
	class PHOTORT_API CPURayTracerEngine : public RenderEntity, public IRayTracerEngine
	{
		template <bool>
		friend class CPURayTracer;

	protected:

		const Scene *mScene = nullptr;
		std::atomic<bool> mReady{false};

		struct ContainerFactoryEntry
		{
			EntityPtr<pl::IGenericSmartFactory<EntityContainer>> factory;
			ParameterTree params;
		};

		struct ContainerData
		{
			//std::vector<EntityContainerInstanceTree *> trees;

			std::vector<pl::UniquePtr<EntityContainer>> accelStructures;
			szt rootAccelStructureIndex = 0;

			void clear()
			{
				//trees.clear();
				//trees.shrink_to_fit();
				accelStructures.clear();
				accelStructures.shrink_to_fit();
			}
		};

		struct ContainerGenerators
		{
			#ifdef PL_LIB_NUMA
				pl::numa::MemoryResource numaMemRes{
					0
					#ifdef PL_LIB_HUGEPAGES
						,pl::hugepages_resource()
					#endif
				};
				std::pmr::memory_resource *defaultMemRes{&numaMemRes};
			#else
				std::pmr::memory_resource *defaultMemRes{
					#ifdef PL_LIB_HUGEPAGES
						&pl::hugepages_resource()
					#else
						std::pmr::get_default_resource()
					#endif
				};
			#endif

			/*pl::MemoryResourceAdaptor<std::pmr::memory_resource, boost::container::pmr::memory_resource> defaultMemResAdapted{*defaultMemRes};
			std::unique_ptr<boost::container::pmr::monotonic_buffer_resource> monoMemResBoost;
			pl::MemoryResourceAdaptor<boost::container::pmr::monotonic_buffer_resource> monoMemRes;*/
			pl::MonotonicBufferResource monoMemRes{*defaultMemRes};
			pl::smart_factory<EntityContainerMulti, std::pmr::polymorphic_allocator<EntityContainerMulti>> containerMultiFactory{ {&monoMemRes} };
			pl::smart_factory<EntityContainerInstanceTree, std::pmr::polymorphic_allocator<EntityContainerInstanceTree>> containerTreeFactory{ {&monoMemRes} };

			std::map<u32, ContainerFactoryEntry> containerFactories;

			void clear()
			{
				/*if(monoMemResBoost)
					monoMemResBoost->release();*/
				monoMemRes.free();
			}
		};

		std::vector<ContainerGenerators> mContainerGenerators;
		std::vector<ContainerData> mContainerData;
		ContainerData *mFirstContainer = nullptr;
		szt mRootIndex = 0;

		void buildContainer(ContainerData &data, ContainerGenerators &generators);
		void copyContainer(const ContainerData &from, ContainerData &to, ContainerGenerators &generators);

	public:
		PRT_ENTITY_FACTORY_DECLARE("raytracer-cpu", CPURayTracerEngine)

		CPURayTracerEngine();

		virtual void clearAccelStructures();

		[[nodiscard]] virtual const Scene *getScene() const override { return mScene; }

		virtual void init(const Scene &scene) override;

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

		virtual void printInfo(std::ostream &outputStream) const override;

		[[nodiscard]] virtual bool isReady() const override { return mReady.load(); }

		[[nodiscard]] virtual std::unique_ptr<IRayTracer> createTracer(int index = -1, bool withStats = true) override;

	};

	namespace CPURayTracerImpl
	{
		template <bool Stats_>
		struct Stats {};

		template <>
		struct Stats<true>
		{
			stats::AccumulatorRay mStatsRay;

			#ifdef PRT_EXT_STATS
				stats::AccumulatorColTest mStatsColTest;
				stats::AccumulatorAABBTest mStatsAABB;
			#endif
		};
	} // namespace CPURayTracerImpl

	template <bool Stats_ = true>
	class CPURayTracer : public CPURayTracerImpl::Stats<Stats_>
	{
		friend class CPURayTracerEngine;

	private:
		CPURayTracerEngine *mEngine = nullptr;
		const Scene *mScene = nullptr;
		CPURayTracerEngine::ContainerData *mData = nullptr;

	public:

		CPURayTracer(CPURayTracerEngine &engine)
			: mEngine{&engine}, mScene{engine.getScene()} {}

		[[nodiscard]] pl::marked_optional<CollisionData> traceRay(const Ray &ray) noexcept
		{
			PRT_SCOPED_PROFILE(Profiler::raytrace, Profiler::tag::Rays);

			const BasicRay bray{
					.origin = ray.origin,
					.dir = ray.dir,
					.dirInv = dirReciprocal(ray.dir)
				};

			pl::assert_valid(ray.origin);
			pl::assert_valid(ray.dir);

			const auto accelStruct = std::to_address(mData->accelStructures[mData->rootAccelStructureIndex]);
			const auto res = pl::assert_valid(accelStruct)->traverse(bray, pl::plisinf(ray.length) ? ray.length : ray.length /opow2);

			if constexpr(Stats_)
			{
				this->mStatsRay(1);

				#ifdef PRT_EXT_STATS
					this->mStatsColTest(res.nbColTest);
					this->mStatsAABB(res.nbAABBTest);
				#endif
			}

			return res.collision;
		}

		RayBatchMask traceRayBatch(RayBatch &batch) noexcept
		{
			PRT_SCOPED_PROFILE(Profiler::raytrace, Profiler::tag::RayBatches);

			plassert(batch.size() <= RayBatchMaxSize);

			const auto accelStruct = std::to_address(mData->accelStructures[mData->rootAccelStructureIndex]);
			const auto mask = pl::assert_valid(accelStruct)->traverseBatch(batch, prepareRayBatch(batch));

			if constexpr(Stats_)
			{
				this->mStatsRay(batch.size());

				#ifdef PRT_EXT_STATS
					for(const auto &d : batch)
					{
						this->mStatsColTest(d.result.nbColTest);
						this->mStatsAABB(d.result.nbAABBTest);
					}
				#endif
			}

			return mask;
		}

		[[nodiscard]] StatsAccumulatorList getStats() const
		{
			StatsAccumulatorList list;

			if constexpr(Stats_)
			{
				list.emplace("rays", pl::clone(this->mStatsRay));

				#ifdef PRT_EXT_STATS
					list.emplace("coltest", pl::clone(this->mStatsColTest));
					list.emplace("aabbtest", pl::clone(this->mStatsAABB));
				#endif
			}

			return list;
		}

		void resetStats()
		{
			if constexpr(Stats_)
			{
				this->mStatsRay.reset();

				#ifdef PRT_EXT_STATS
					this->mStatsColTest.reset();
					this->mStatsAABB.reset();
				#endif
			}
		}

		[[nodiscard]] const Scene *getScene() const noexcept { return mScene; }

	};

}
