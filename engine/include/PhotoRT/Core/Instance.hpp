/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Transform_fwd.hpp"
#include "Emitter_fwd.hpp"
#include "Instance_fwd.hpp"

#include "Base.hpp"
#include "PrimitiveGroup.hpp"
#include "RenderEntity.hpp"

#include <patlib/pointer.hpp>
#include <patlib/bitset.hpp>

namespace PhotoRT
{
	struct InstanceNode
	{
		Matrix mat{pl::tag::identity}, matInv{pl::tag::identity};
		EntityPtrPC<IObjectInstance> instance;
	};

	enum class InstanceFlagEnum : szt
	{
		Group = 0,
		TreeGroup,
		AabbCheck
	};

	using InstanceFlags = pl::enum_bitset<InstanceFlagEnum, 3>;

	class IObjectInstance : public IAABox
	{
	public:
		virtual ~IObjectInstance() = default;

		[[nodiscard]] virtual std::string_view getInstanceID() const = 0;

		[[nodiscard]] virtual const EntityPtr<IPrimitiveGroup> &getObject() const noexcept = 0;
		virtual void setObject(EntityPtr<IPrimitiveGroup> object) = 0;

		[[nodiscard]] virtual const EntityPtr<ITransform> &getTransform() const noexcept = 0;
		virtual void setTransform(EntityPtr<ITransform> t) = 0;

		[[nodiscard]] virtual std::span<InstanceNode> getChildNodes() noexcept = 0;
		[[nodiscard]] virtual std::span<const InstanceNode> getChildNodes() const noexcept = 0;
		virtual InstanceNode &addNode(InstanceNode node) = 0;

		[[nodiscard]] virtual const PrimGroupPropertiesMapShared &getPrimGroupProperties() const noexcept = 0;
		[[nodiscard]] virtual PrimGroupPropertiesMapShared &getPrimGroupProperties() noexcept = 0;

		[[nodiscard]] virtual const IEmitter *getEmitter() const noexcept = 0;
		virtual void setEmitter(const IEmitter *light) = 0;

		// Hints for building acceleration structures
		[[nodiscard]] virtual InstanceFlags getFlags() const noexcept = 0;
		virtual void setFlags(InstanceFlags flags) = 0;
	};

	class PHOTORT_API ObjectInstance : public RenderEntity, public IObjectInstance
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("instance-", ObjectInstance)

		ObjectInstance() = default;
		ObjectInstance(EntityPtr<IPrimitiveGroup> obj) noexcept
			: mObject{std::move(obj)} {}

		[[nodiscard]] virtual std::string_view getInstanceID() const noexcept override { return getEntityId(); }

		[[nodiscard]] virtual const EntityPtr<IPrimitiveGroup> &getObject() const noexcept override { return mObject; }
		virtual void setObject(EntityPtr<IPrimitiveGroup> o) override { mObject = std::move(o); }

		[[nodiscard]] virtual const EntityPtr<ITransform> &getTransform() const noexcept override { return mTransform; }
		virtual void setTransform(EntityPtr<ITransform> t) override { mTransform = std::move(t); }

		[[nodiscard]] virtual std::span<InstanceNode> getChildNodes() noexcept override { return mNodes; }
		[[nodiscard]] virtual std::span<const InstanceNode> getChildNodes() const noexcept override { return mNodes; }
		virtual InstanceNode &addNode(InstanceNode node) override;

		[[nodiscard]] virtual const PrimGroupPropertiesMapShared &getPrimGroupProperties() const noexcept override { return mPrimGroupProperties; }
		[[nodiscard]] virtual PrimGroupPropertiesMapShared &getPrimGroupProperties() noexcept override { return mPrimGroupProperties; }

		[[nodiscard]] virtual InstanceFlags getFlags() const noexcept override { return mFlags; }
		virtual void setFlags(InstanceFlags flags) override { mFlags = flags; }

		[[nodiscard]] virtual Box getAABox() const noexcept override;

		[[nodiscard]] virtual const IEmitter *getEmitter() const noexcept override { return mLight; }
		virtual void setEmitter(const IEmitter *light) override { mLight = light; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:
		EntityPtr<ITransform> mTransform;
		EntityPtr<IPrimitiveGroup> mObject;

		PrimGroupPropertiesMapShared mPrimGroupProperties;
		const IEmitter *mLight = nullptr;

		InstanceFlags mFlags{InstanceFlagEnum::AabbCheck};

		// Lightweight nodes
		VectorWithStats<InstanceNode, memtag::Entity> mNodes;

		friend auto buildObjectInstanceNodesSerializer(auto& instance) noexcept;
	};

	class PHOTORT_API ObjectInstancePtr : public RenderEntity, public IObjectInstance
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("instance-ptr", ObjectInstancePtr)

		ObjectInstancePtr() = default;
		ObjectInstancePtr(EntityPtr<IObjectInstance> inst) noexcept
			: mInstance{std::move(inst)} {}

		[[nodiscard]] virtual std::string_view getInstanceID() const noexcept override { return getEntityId(); }

		[[nodiscard]] virtual const EntityPtr<IPrimitiveGroup> &getObject() const noexcept override { return mInstance->getObject(); }
		virtual void setObject(EntityPtr<IPrimitiveGroup> object) override { mInstance->setObject(std::move(object)); }

		[[nodiscard]] virtual const EntityPtr<ITransform> &getTransform() const noexcept override { return mTransform; }
		virtual void setTransform(EntityPtr<ITransform> t) override { mTransform = std::move(t); }

		[[nodiscard]] virtual std::span<InstanceNode> getChildNodes() noexcept override { return mInstance->getChildNodes(); }
		[[nodiscard]] virtual std::span<const InstanceNode> getChildNodes() const noexcept override { return mInstance->getChildNodes(); }
		virtual InstanceNode &addNode(InstanceNode node) override { return mInstance->addNode(std::move(node)); }

		[[nodiscard]] virtual const PrimGroupPropertiesMapShared &getPrimGroupProperties() const noexcept override { return mPrimGroupProperties; }
		[[nodiscard]] virtual PrimGroupPropertiesMapShared &getPrimGroupProperties() noexcept override { return mPrimGroupProperties; }

		[[nodiscard]] virtual InstanceFlags getFlags() const noexcept override { return mInstance->getFlags(); }
		virtual void setFlags(InstanceFlags flags) override { mInstance->setFlags(flags); }

		[[nodiscard]] virtual Box getAABox() const noexcept override { return mInstance->getAABox(); }

		void setPointer(EntityPtr<IObjectInstance> i) { mInstance = std::move(i); }
		[[nodiscard]] const auto &getPointer() const noexcept { return mInstance; }

		[[nodiscard]] virtual const IEmitter *getEmitter() const noexcept override { return mLight; }
		virtual void setEmitter(const IEmitter *light) override { mLight = light; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:
		EntityPtr<ITransform> mTransform;
		EntityPtr<IObjectInstance> mInstance;
		PrimGroupPropertiesMapShared mPrimGroupProperties;
		const IEmitter *mLight = nullptr;

	};


	namespace InstImpl
	{
		template <typename InstanceType_>
		[[nodiscard]] constexpr auto &instanceType(auto &inst) noexcept
			{ return *inst; }

		template <pl::CPointerLike InstanceType_>
		[[nodiscard]] constexpr auto &instanceType(auto &inst) noexcept
			{ return inst; }
	} // namespace InstImpl

	enum struct TraverseRecursiveState
	{
		Continue = 0,
		Stop
	};

	template <typename InstanceType_, typename ChildsDoneOp_ = pl::do_nothing_t, pl::CParallelPolicy Policy_ = pl::policy::default_policy_t>
	void traverseRecursiveProperties(InstanceType_ &inst, auto &&fc, ChildsDoneOp_ &&childsDoneFc = {}, const PrimGroupPropertiesMapPtr &propertyMap = {}, const Matrix &curMatrix = pl::tag::identity, const Matrix &curMatrixInv = pl::tag::identity, const Policy_ policy = {})
	{
		const auto &spm = pl::to_ref(inst).getPrimGroupProperties();
		PrimGroupPropertiesMapPtr newMap{spm.begin(), spm.end()};

		for(const auto &i : propertyMap)
		{
			if(auto it = rg::find_if(newMap, [&](const auto &p){ return p.id.second == i.id.second && p.id.first == i.id.first; });
				it != newMap.end()
			)
				*it = i;
			else
				newMap.push_back(i);
		}

		if(PL_FWD(fc)(inst, newMap, curMatrix, curMatrixInv) == TraverseRecursiveState::Continue)
		{
			pl::to_ref(inst).getChildNodes() | pl::for_each([&](auto &child)
			{
				const auto tr = std::to_address(child.instance->getTransform());

				traverseRecursiveProperties(
						InstImpl::instanceType<InstanceType_>(child.instance),
						PL_FWD(fc),
						PL_FWD(childsDoneFc),
						newMap,
						tr ? tr->getMatrix() * child.mat * curMatrix : child.mat * curMatrix,
						tr ? curMatrixInv * child.matInv * tr->inverse() : curMatrixInv * child.matInv,
						policy
					);

			}, std::identity{}, policy);

			PL_FWD(childsDoneFc)(inst, newMap, curMatrix, curMatrixInv);
		}
	}

	template <typename InstanceType_, typename ChildsDoneOp_ = pl::do_nothing_t, pl::CParallelPolicy Policy_ = pl::policy::default_policy_t>
	void traverseRecursiveTransform(InstanceType_ &inst, auto &&fc, ChildsDoneOp_ &&childsDoneFc = {}, const Matrix &curMatrix = pl::tag::identity, const Matrix &curMatrixInv = pl::tag::identity, const Policy_ policy = {})
	{
		if(PL_FWD(fc)(inst, curMatrix, curMatrixInv) == TraverseRecursiveState::Continue)
		{
			pl::to_ref(inst).getChildNodes() | pl::for_each([&](auto &child)
			{
				const auto tr = std::to_address(child.instance->getTransform());

				traverseRecursiveTransform(
						InstImpl::instanceType<InstanceType_>(child.instance),
						PL_FWD(fc),
						PL_FWD(childsDoneFc),
						tr ? tr->getMatrix() * child.mat * curMatrix : child.mat * curMatrix,
						tr ? curMatrixInv * child.matInv * tr->inverse() : curMatrixInv * child.matInv,
						policy
					);

			}, std::identity{}, policy);

			PL_FWD(childsDoneFc)(inst, curMatrix, curMatrixInv);
		}
	}

	template <typename InstanceType_, typename ChildsDoneOp_ = pl::do_nothing_t, pl::CParallelPolicy Policy_ = pl::policy::default_policy_t>
	void traverseRecursive(InstanceType_ &inst, auto &&fc, ChildsDoneOp_ &&childsDoneFc = {}, const Policy_ policy = {})
	{
		if(PL_FWD(fc)(inst) == TraverseRecursiveState::Continue)
		{
			pl::to_ref(inst).getChildNodes() | pl::for_each([&](auto &child)
			{
				traverseRecursive(InstImpl::instanceType<InstanceType_>(child.instance), PL_FWD(fc), PL_FWD(childsDoneFc), policy);
			}, std::identity{}, policy);

			PL_FWD(childsDoneFc)(inst);
		}
	}
}
