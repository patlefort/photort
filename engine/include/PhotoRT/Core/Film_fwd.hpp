/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Bitmap/impl/BitmapBuffer_fwd.hpp>
#include <PhotoRT/Bitmap/Bitmap_fwd.hpp>

namespace PhotoRT
{
	struct PixelSample;
	class IndividualPixelSampleList;
	struct PhotonSamples;
	struct PhotonSampleList;
	struct PixelSampleTile;
	struct PixelTile;
	struct PixelSampleTileLayers;

	using PhotonSamplesList = std::vector<PhotonSamples>;

	class ContributionBuffer;
	class ContributionBufferPixel;
	class ContributionBufferLight;

	class ContributionBufferAggregator;

	struct LayerParams;

	class Film;
	class RenderContext;
}
