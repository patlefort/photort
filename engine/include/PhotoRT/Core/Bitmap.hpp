/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Bitmap_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"
#include "Distribution.hpp"
#include "Filter.hpp"

#include <patlib/clone.hpp>
#include <patlib/box.hpp>
#include <patlib/rectangle.hpp>

namespace PhotoRT
{
	[[nodiscard]] inline Point3Di getPosFromOffset(if32 offset, const Dir3Di size) noexcept
	{
		const auto area2D = size[0] * size[1];
		return {offset % size[0], (offset % area2D) / size[0], offset / area2D};
	}

	class IBitmap1DWrite
	{
	public:
		virtual ~IBitmap1DWrite() = default;

		virtual void setPixel(Color color, szt offset) = 0;
		virtual void zeroBitmap() = 0;
	};

	class IBitmap1DRead
	{
	public:
		virtual ~IBitmap1DRead() = default;

		[[nodiscard]] virtual Color getPixel(szt offset) const noexcept = 0;
		[[nodiscard]] virtual Dir3Di getSize() const noexcept = 0;
		[[nodiscard]] virtual szt getNbPixels() const noexcept = 0;
		[[nodiscard]] virtual if32 getWidth() const noexcept = 0;

		[[nodiscard]] Color operator[] (const szt index) const noexcept
			{ return getPixel(index); }
	};

	class IBitmap2DWrite
	{
	public:
		virtual ~IBitmap2DWrite() = default;

		virtual void setPixel(Color color, if32 u, if32 v) = 0;
		virtual void setPixel(Color color, szt offset) = 0;
		virtual void zeroBitmap() = 0;
	};

	class IBitmap2DRead
	{
	public:
		virtual ~IBitmap2DRead() = default;

		[[nodiscard]] virtual Color getPixel(if32 u, if32 v) const noexcept = 0;
		[[nodiscard]] virtual Color getPixel(szt offset) const noexcept = 0;
		[[nodiscard]] virtual if32 getWidth() const noexcept = 0;
		[[nodiscard]] virtual if32 getHeight() const noexcept = 0;
		[[nodiscard]] virtual Dir3Di getSize() const noexcept = 0;
		[[nodiscard]] virtual pl::rectangle<> getRect() const noexcept = 0;
		[[nodiscard]] virtual szt getNbPixels() const noexcept = 0;
	};

	class IBitmap3DWrite
	{
	public:
		virtual ~IBitmap3DWrite() = default;

		virtual void setPixel(Color color, if32 u, if32 v, if32 w) = 0;
		virtual void setPixel(Color color, szt offset) = 0;
		virtual void zeroBitmap() = 0;
	};

	class IBitmap3DRead
	{
	public:
		virtual ~IBitmap3DRead() = default;

		[[nodiscard]] virtual Color getPixel(if32 u, if32 v, if32 w) const noexcept = 0;
		[[nodiscard]] virtual Color getPixel(szt offset) const noexcept = 0;
		[[nodiscard]] virtual if32 getWidth() const noexcept = 0;
		[[nodiscard]] virtual if32 getHeight() const noexcept = 0;
		[[nodiscard]] virtual if32 getDepth() const noexcept = 0;
		[[nodiscard]] virtual Dir3Di getSize() const noexcept = 0;
		[[nodiscard]] virtual pl::box<VecP3i> getBox() const noexcept = 0;
		[[nodiscard]] virtual szt getNbPixels() const noexcept = 0;
	};

	struct BitmapIterator {};

	template <typename T_> constexpr bool isBitmapIterator_v = std::is_base_of_v<BitmapIterator, pl::decay_t<T_>>;

	template <typename BitmapType_, typename ColorType_ = Color>
	struct BitmapOffsetReference
	{
		BitmapType_ *mB;
		szt mOffset;

		BitmapOffsetReference(const BitmapOffsetReference &) = default;
		BitmapOffsetReference(BitmapType_ &b, szt offset) noexcept : mB(&b), mOffset(offset) {}

		template <typename IT_, typename std::enable_if_t<isBitmapIterator_v<IT_>>>
		BitmapOffsetReference(IT_ it) noexcept : mB(&it.getBitmap()), mOffset(it.getOffset()) {}

		template <typename BT_, typename CT_>
		auto &operator =(const BitmapOffsetReference<BT_, CT_> &o) const requires (!std::is_const_v<BitmapType_>) { mB->setPixel(*o, mOffset); return *this; }

		auto &operator =(auto v) const requires (!std::is_const_v<BitmapType_>) { mB->setPixel(v, mOffset); return *this; }
		auto &operator +=(auto v) const requires (!std::is_const_v<BitmapType_>) { mB->setPixel(get() + v, mOffset); return *this; }
		auto &operator -=(auto v) const requires (!std::is_const_v<BitmapType_>) { mB->setPixel(get() - v, mOffset); return *this; }
		auto &operator /=(auto v) const requires (!std::is_const_v<BitmapType_>) { mB->setPixel(get() / v, mOffset); return *this; }
		auto &operator *=(auto v) const requires (!std::is_const_v<BitmapType_>) { mB->setPixel(get() * v, mOffset); return *this; }

		[[nodiscard]] auto operator +(auto v) const noexcept { return get() + v; }
		[[nodiscard]] auto operator -(auto v) const noexcept { return get() - v; }
		[[nodiscard]] auto operator /(auto v) const noexcept { return get() / v; }
		[[nodiscard]] auto operator *(auto v) const noexcept { return get() * v; }

		[[nodiscard]] ColorType_ get() const noexcept { return mB->getPixel(mOffset); }
		[[nodiscard]] auto getOffset() const noexcept { return mOffset; }

		[[nodiscard]] auto operator *() const noexcept { return get(); }

		[[nodiscard]] operator ColorType_() const noexcept { return get(); }
	};

	namespace ImplBitmap
	{
		template <typename BitmapType_>
		class BitmapIteratorBase : public BitmapIterator
		{
		public:

			using BitmapType = BitmapType_;

			BitmapIteratorBase() = default;
			BitmapIteratorBase(BitmapType_ &bitmap) noexcept : mBitmap{&bitmap} {}

			[[nodiscard]] auto getBitmap() const noexcept { return mBitmap; }

		protected:
			BitmapType_ *mBitmap{};
		};

	} // namespace ImplBitmap

	template <typename T_>
	concept CBitmapIterator = std::is_base_of_v<BitmapIterator, T_> &&
		rg::input_or_output_iterator<T_> &&
		requires(const T_ i) {
			{ i.getOffset() } -> std::convertible_to<szt>;
		};

	template <typename BitmapType_, typename ColorType_ = Color>
	class BitmapIterator1D : public ImplBitmap::BitmapIteratorBase<BitmapType_>
	{
	public:
		using value_type = ColorType_;
		using pointer = ColorType_ *;
		using reference = std::conditional_t<std::is_const_v<BitmapType_>, typename pl::remove_cvref_t<BitmapType_>::const_reference, typename pl::remove_cvref_t<BitmapType_>::reference>;
		using iterator_category = std::random_access_iterator_tag;
		using difference_type = if32;

		using typename ImplBitmap::BitmapIteratorBase<BitmapType_>::BitmapType;

	private:
		using ImplBitmap::BitmapIteratorBase<BitmapType_>::mBitmap;
		difference_type mIndex{};

	public:

		BitmapIterator1D() = default;
		template <typename OBT_>
		BitmapIterator1D(const BitmapIterator1D<OBT_, ColorType_> &other) noexcept
			requires (std::is_const_v<BitmapType_> && !std::is_const_v<OBT_>)
			: ImplBitmap::BitmapIteratorBase<BitmapType_>(*other.mBitmap), mIndex{other.mIndex} {}
		BitmapIterator1D(BitmapType_ &bitmap) noexcept : ImplBitmap::BitmapIteratorBase<BitmapType_>(bitmap) {}
		BitmapIterator1D(BitmapType_ &bitmap, bool /*startsAtEnd*/) noexcept : ImplBitmap::BitmapIteratorBase<BitmapType_>(bitmap) { mIndex = mBitmap->getNbPixels(); }

		[[nodiscard]] reference operator *() const noexcept { return mBitmap->getPixelRef(getOffset()); }

		[[nodiscard]] szt getOffset() const noexcept { return (szt)mIndex; }
		[[nodiscard]] auto getBitmap() const noexcept { return mBitmap; }

		auto &operator-- () noexcept
		{
			--mIndex;
			return *this;
		}

		auto &operator++ () noexcept
		{
			++mIndex;
			return *this;
		}

		auto operator-- (int) noexcept
		{
			auto cp = *this;
			--(*this);
			return cp;
		}

		auto operator++ (int) noexcept
		{
			auto cp = *this;
			++(*this);
			return cp;
		}

		[[nodiscard]] auto operator [] (const difference_type i) const noexcept { return *(*this + i); }

		[[nodiscard]] auto operator<=> (const CBitmapIterator auto &other) const noexcept { return getOffset() <=> other.getOffset(); }
		[[nodiscard]] bool operator== (const CBitmapIterator auto &other) const noexcept { return getOffset() == other.getOffset(); }
		/*[[nodiscard]] auto operator<=> (difference_type o) const noexcept { return getOffset() <=> static_cast<szt>(o); }
		[[nodiscard]] bool operator== (difference_type o) const noexcept { return getOffset() == static_cast<szt>(o); }*/

		auto &operator += (const difference_type n) noexcept { mIndex += n; return *this; }
		auto &operator -= (const difference_type n) noexcept { mIndex -= n; return *this; }

		[[nodiscard]] auto operator -(const difference_type n) const noexcept { auto nit = *this; return (nit -= n); }

		[[nodiscard]] auto operator -(const BitmapIterator1D &other) const noexcept { return mIndex - other.mIndex; }

		[[nodiscard]] explicit operator difference_type() const noexcept { return mIndex; }
	};

	template <typename BitmapType_, typename ColorType_>
	[[nodiscard]] auto operator +(BitmapIterator1D<BitmapType_, ColorType_> it, const typename BitmapIterator1D<BitmapType_, ColorType_>::difference_type n) noexcept { return (it += n); }

	template <typename BitmapType_, typename ColorType_>
	[[nodiscard]] auto operator +(const typename BitmapIterator1D<BitmapType_, ColorType_>::difference_type n, BitmapIterator1D<BitmapType_, ColorType_> it) noexcept { return (it += n); }


	template <typename BitmapType_, typename ColorType_ = Color>
	class BitmapIterator2D : public ImplBitmap::BitmapIteratorBase<BitmapType_>
	{
	public:
		using value_type = ColorType_;
		using pointer = ColorType_ *;
		using reference = std::conditional_t<std::is_const_v<BitmapType_>, typename pl::remove_cvref_t<BitmapType_>::const_reference, typename pl::remove_cvref_t<BitmapType_>::reference>;
		using iterator_category = std::random_access_iterator_tag;
		using difference_type = if32;

		using typename ImplBitmap::BitmapIteratorBase<BitmapType_>::BitmapType;

	private:
		using ImplBitmap::BitmapIteratorBase<BitmapType_>::mBitmap;
		pl::rectangle<> mR;
		difference_type mWidth;
		Point2Di mP;

	public:

		BitmapIterator2D() = default;
		template <typename OBT_>
		BitmapIterator2D(const BitmapIterator2D<OBT_, ColorType_> &other) noexcept
			requires (std::is_const_v<BitmapType_> && !std::is_const_v<OBT_>)
			: ImplBitmap::BitmapIteratorBase<BitmapType_>(*other.mBitmap), mR{other.mR}, mWidth{other.mWidth}, mP{other.mP} {}
		BitmapIterator2D(BitmapType_ &bitmap, const pl::rectangle<> &r) noexcept :
			ImplBitmap::BitmapIteratorBase<BitmapType_>(bitmap), mR{r}
		{
			const auto sz = mBitmap->getSize();
			mWidth = sz[0];
			mP = mR.p1;

			if(!mWidth || !sz[1])
				mWidth = 0;
		}

		BitmapIterator2D(BitmapType_ &bitmap, const pl::rectangle<> &r, bool /*startsAtEnd*/) noexcept :
			ImplBitmap::BitmapIteratorBase<BitmapType_>(bitmap), mR{r}
		{
			// Offset for end of range will be the offset mR.p1[0], mR.p2[1]
			const auto sz = mBitmap->getSize();
			mWidth = sz[0];

			if(!mWidth || !sz[1])
			{
				mWidth = 0;
				mP = mR.p1;
			}
			else
				mP = {mR.p1[0], mR.p2[1]};
		}

		[[nodiscard]] auto getBitmap() const noexcept { return mBitmap; }

		[[nodiscard]] reference operator *() const noexcept { return mBitmap->getPixelRef(getOffset()); }

		[[nodiscard]] szt getOffset() const noexcept { return mP[1] * mWidth + mP[0]; }
		[[nodiscard]] const auto &getPosition() const noexcept { return mP; }
		[[nodiscard]] auto getLocalPosition() const noexcept { return mP - mR.p1; }

		[[nodiscard]] difference_type area() const noexcept { return mR.area(); }

		auto &operator-- () noexcept
		{
			if(!mWidth)
				return *this;

			if(--mP[0] < mR.p1[0])
			{
				mP[0] = mR.p2[0] - 1;
				--mP[1];
			}

			return *this;
		}

		auto &operator++ () noexcept
		{
			if(!mWidth)
				return *this;

			if(++mP[0] >= mR.p2[0])
			{
				mP[0] = mR.p1[0];
				++mP[1];
			}

			return *this;
		}

		auto operator-- (int) noexcept
		{
			auto cp = *this;
			--(*this);
			return cp;
		}

		auto operator++ (int) noexcept
		{
			auto cp = *this;
			++(*this);
			return cp;
		}

		[[nodiscard]] auto operator [] (const difference_type i) const noexcept { if(!mWidth) return **this; return *(*this + i); }

		[[nodiscard]] auto operator<=> (const CBitmapIterator auto &other) const noexcept { return getOffset() <=> other.getOffset(); }
		[[nodiscard]] bool operator== (const CBitmapIterator auto &other) const noexcept { return getOffset() == other.getOffset(); }
		/*[[nodiscard]] auto operator<=> (difference_type o) const noexcept { return getOffset() <=> static_cast<szt>(o); }
		[[nodiscard]] bool operator== (difference_type o) const noexcept { return getOffset() == static_cast<szt>(o); }*/

		[[nodiscard]] bool operator == (const BitmapIterator2D &other) const noexcept { return bool(mP == other.mP); }

		auto &operator += (const difference_type n) noexcept
		{
			if(!mWidth)
				return *this;

			const difference_type lwidth = mR.p2[0] - mR.p1[0];
			const difference_type newoffset = getLocalOffset(mP) + n;

			mP = {
				mR.p1[0] + newoffset % lwidth,
				mR.p1[1] + newoffset / lwidth
			};

			return *this;
		}

		auto &operator -= (const difference_type n) noexcept { return *this += -n; }

		[[nodiscard]] auto operator -(const difference_type n) const noexcept { auto nit = *this; return (nit -= n); }

		[[nodiscard]] difference_type operator -(const BitmapIterator2D &other) const noexcept
		{
			if(!mWidth)
				return 0;

			const auto opos = getPosFromOffset(other.getOffset(), mBitmap->getSize());

			return getLocalOffset(mP) - getLocalOffset(static_cast<Point2Di>(opos));
		}

		[[nodiscard]] explicit operator difference_type() const noexcept { return getOffset(); }

	protected:

		[[nodiscard]] auto getLocalOffset(Point2Di pt) const noexcept
		{
			const Point2Di lp = pt - mR.p1;
			const difference_type lwidth = mR.p2[0] - mR.p1[0];

			return lp[1] * lwidth + lp[0];
		}

	};

	template <typename BitmapType_, typename ColorType_>
	[[nodiscard]] auto operator +(BitmapIterator2D<BitmapType_, ColorType_> it, const typename BitmapIterator2D<BitmapType_, ColorType_>::difference_type n) noexcept { return (it += n); }

	template <typename BitmapType_, typename ColorType_>
	[[nodiscard]] auto operator +(const typename BitmapIterator2D<BitmapType_, ColorType_>::difference_type n, BitmapIterator2D<BitmapType_, ColorType_> it) noexcept { return (it += n); }

	template <typename BitmapType_, typename ColorType_ = Color>
	class BitmapIterator3D : public ImplBitmap::BitmapIteratorBase<BitmapType_>
	{
	public:
		using value_type = ColorType_;
		using pointer = ColorType_ *;
		using reference = std::conditional_t<std::is_const_v<BitmapType_>, typename pl::remove_cvref_t<BitmapType_>::const_reference, typename pl::remove_cvref_t<BitmapType_>::reference>;
		using iterator_category = std::random_access_iterator_tag;
		using difference_type = if32;

		using typename ImplBitmap::BitmapIteratorBase<BitmapType_>::BitmapType;

	private:
		using ImplBitmap::BitmapIteratorBase<BitmapType_>::mBitmap;
		pl::box<VecP3i> mR;
		difference_type mWidth, mArea2d;
		VecP3i mPos;

	public:

		BitmapIterator3D() = default;
		template <typename OBT_>
		BitmapIterator3D(const BitmapIterator3D<OBT_, ColorType_> &other) noexcept
			requires (std::is_const_v<BitmapType_> && !std::is_const_v<OBT_>)
			: ImplBitmap::BitmapIteratorBase<BitmapType_>(*other.mBitmap), mR{other.mR}, mWidth{other.mWidth}, mArea2d{other.mArea2d}, mPos{other.mPos} {}
		BitmapIterator3D(BitmapType_ &bitmap, const pl::box<VecP3i> &r) noexcept :
			ImplBitmap::BitmapIteratorBase<BitmapType_>(bitmap), mR{r}
		{
			const auto sz = mBitmap->getSize();
			mWidth = sz[0];
			mArea2d = sz[0] * sz[1];
			mPos = mR.v1;

			if(!mArea2d || !sz[2])
				mWidth = 0;
		}

		BitmapIterator3D(BitmapType_ &bitmap, const pl::box<VecP3i> &r, bool /*startsAtEnd*/) noexcept :
			ImplBitmap::BitmapIteratorBase<BitmapType_>(bitmap), mR{r}
		{
			// Offset for end of range will be the offset mR.v1[0}, mR.v1[1] and mR.v2[2]
			const auto sz = mBitmap->getSize();
			mWidth = sz[0];
			mArea2d = sz[0] * sz[1];

			if(!mArea2d || !sz[2])
			{
				mWidth = 0;
				mPos = mR.v1;
				mPos[2] = mR.v2[2];
			}
			else
				mPos = mR.v1;
		}

		[[nodiscard]] auto getBitmap() const noexcept { return mBitmap; }

		[[nodiscard]] reference operator *() const noexcept { return mBitmap->getPixelRef(getOffset()); }

		[[nodiscard]] szt getOffset() const noexcept { return mPos[2] * mArea2d + mPos[1] * mWidth + mPos[0]; }
		[[nodiscard]] auto getPosition() const noexcept { return mPos; }
		[[nodiscard]] auto getLocalPosition() const noexcept { return mPos - mR.v1; }

		[[nodiscard]] difference_type area() const noexcept { return mR.area(); }

		auto &operator-- () noexcept
		{
			if(!mWidth)
				return *this;

			if(--mPos[0] < mR.v1[0])
			{
				mPos[0] = mR.v2[0] - 1;

				if(--mPos[1] < mR.v1[1])
				{
					mPos[1] = mR.v2[1] - 1;
					--mPos[2];
				}
			}

			return *this;
		}

		auto &operator++ () noexcept
		{
			if(!mWidth)
				return *this;

			if(++mPos[0] >= mR.v2[0])
			{
				mPos[0] = mR.v1[0];

				if(++mPos[1] >= mR.v2[1])
				{
					mPos[1] = mR.v1[1];
					++mPos[2];
				}
			}

			return *this;
		}

		auto operator-- (int) noexcept
		{
			auto cp = *this;
			--(*this);
			return cp;
		}

		auto operator++ (int) noexcept
		{
			auto cp = *this;
			++(*this);
			return cp;
		}

		[[nodiscard]] auto operator [] (const difference_type i) const noexcept { if(!mWidth) return **this; return *(*this + i); }

		[[nodiscard]] auto operator<=> (const CBitmapIterator auto &other) const noexcept { return getOffset() <=> other.getOffset(); }
		[[nodiscard]] bool operator== (const CBitmapIterator auto &other) const noexcept { return getOffset() == other.getOffset(); }
		/*[[nodiscard]] auto operator<=> (difference_type o) const noexcept { return getOffset() <=> static_cast<szt>(o); }
		[[nodiscard]] bool operator== (difference_type o) const noexcept { return getOffset() == static_cast<szt>(o); }*/

		[[nodiscard]] bool operator == (const BitmapIterator3D &other) const noexcept { return bool(mPos == other.mPos); }

		auto &operator += (const difference_type n) noexcept
		{
			if(!mWidth)
				return *this;

			const auto lwidth = mR.v2[0] - mR.v1[0];
			const auto larea = lwidth * (mR.v2[1] - mR.v1[1]);
			const auto newoffset = getLocalOffset(mPos) + n;

			mPos = {
				mR.v1[0] + newoffset % lwidth,
				mR.v1[1] + (newoffset % larea) / lwidth,
				mR.v1[2] + newoffset / larea
			};

			return *this;
		}

		auto &operator -= (const difference_type n) noexcept { return *this += -n; }

		[[nodiscard]] auto operator -(const difference_type n) const noexcept { auto nit = *this; return (nit -= n); }

		[[nodiscard]] difference_type operator -(const BitmapIterator3D &other) const noexcept
		{
			if(!mWidth)
				return 0;

			const auto opos = (VecP3i)getPosFromOffset(other.getOffset(), mBitmap->getSize());

			return getLocalOffset(mPos) - getLocalOffset(opos);
		}

		[[nodiscard]] explicit operator difference_type() const noexcept { return getOffset(); }

	protected:

		[[nodiscard]] auto getLocalOffset(VecP3i pt) const
		{
			const auto relPos = pt - mR.v1;
			const auto lwidth = mR.v2[0] - mR.v1[0];
			const auto larea = lwidth * (mR.v2[1] - mR.v1[1]);

			return relPos[2] * larea + relPos[1] * lwidth + relPos[0];
		}
	};

	template <typename BitmapType_, typename ColorType_>
	[[nodiscard]] auto operator +(BitmapIterator3D<BitmapType_, ColorType_> it, const typename BitmapIterator3D<BitmapType_, ColorType_>::difference_type n) noexcept { return (it += n); }

	template <typename BitmapType_, typename ColorType_>
	[[nodiscard]] auto operator +(const typename BitmapIterator3D<BitmapType_, ColorType_>::difference_type n, BitmapIterator3D<BitmapType_, ColorType_> it) noexcept { return (it += n); }



	class PHOTORT_API BitmapBase : public virtual IExportHook,
		public virtual IBitmap1DWrite, public virtual IBitmap1DRead,
		public virtual IBitmap2DWrite, public virtual IBitmap2DRead,
		public virtual IBitmap3DWrite, public virtual IBitmap3DRead,
		public virtual IColorDistribution1D, public virtual IColorDistribution2D, public virtual IColorDistribution3D/*,
		public virtual pl::ICloneableNewOrCopy<BitmapBase>*/
	{
	public:
		BitmapBase() = default;

		virtual ~BitmapBase() = default;

		BitmapBase(const BitmapBase &o);
		BitmapBase(BitmapBase &&o);

		virtual BitmapBase &operator= (const BitmapBase &o);
		virtual BitmapBase &operator= (BitmapBase &&o);

		using PixelType = Color;

		// Iterators
		using value_type = PixelType;
		using size_type = szt;
		using difference_type = if32;
		using reference = BitmapOffsetReference<BitmapBase>;
		using const_reference = BitmapOffsetReference<const BitmapBase>;

		using iterator = BitmapIterator1D<BitmapBase>;
		using const_iterator = BitmapIterator1D<const BitmapBase>;
		using reverse_iterator = std::reverse_iterator<iterator>;
		using const_reverse_iterator = std::reverse_iterator<const_iterator>;

		using iterator_2d = BitmapIterator2D<BitmapBase>;
		using const_iterator_2d = BitmapIterator2D<const BitmapBase>;
		using reverse_iterator_2d = std::reverse_iterator<iterator_2d>;
		using const_reverse_iterator_2d = std::reverse_iterator<const_iterator_2d>;

		using iterator_3d = BitmapIterator3D<BitmapBase>;
		using const_iterator_3d = BitmapIterator3D<const BitmapBase>;
		using reverse_iterator_3d = std::reverse_iterator<iterator_3d>;
		using const_reverse_iterator_3d = std::reverse_iterator<const_iterator_3d>;

		template <typename T_>
			requires rg::sentinel_for<T_, T_>
		struct A
		{};

		static constexpr A<iterator_2d> sdfsdf{};

		[[nodiscard]] auto begin() noexcept { return iterator{*this}; }
		[[nodiscard]] auto begin() const noexcept { return const_iterator{*this}; }
		[[nodiscard]] auto cbegin() const noexcept { return const_iterator{*this}; }
		[[nodiscard]] auto end() noexcept { return iterator{*this, true}; }
		[[nodiscard]] auto end() const noexcept { return const_iterator{*this, true}; }
		[[nodiscard]] auto cend() const noexcept { return const_iterator{*this, true}; }

		[[nodiscard]] auto rbegin() noexcept { return reverse_iterator{end()}; }
		[[nodiscard]] auto rbegin() const noexcept { return const_reverse_iterator{end()}; }
		[[nodiscard]] auto crbegin() const noexcept { return const_reverse_iterator{cend()}; }
		[[nodiscard]] auto rend() noexcept { return reverse_iterator{begin()}; }
		[[nodiscard]] auto rend() const noexcept { return const_reverse_iterator{begin()}; }
		[[nodiscard]] auto crend() const noexcept { return const_reverse_iterator{cbegin()}; }

		[[nodiscard]] auto begin2d(const pl::rectangle<> &r) noexcept { return iterator_2d{*this, r}; }
		[[nodiscard]] auto begin2d(const pl::rectangle<> &r) const noexcept { return const_iterator_2d{*this, r}; }
		[[nodiscard]] auto cbegin2d(const pl::rectangle<> &r) const noexcept { return const_iterator_2d{*this, r}; }
		[[nodiscard]] auto end2d(const pl::rectangle<> &r) noexcept { return iterator_2d{*this, r, true}; }
		[[nodiscard]] auto end2d(const pl::rectangle<> &r) const noexcept { return const_iterator_2d{*this, r, true}; }
		[[nodiscard]] auto cend2d(const pl::rectangle<> &r) const noexcept { return const_iterator_2d{*this, r, true}; }

		[[nodiscard]] auto rbegin2d(const pl::rectangle<> &r) noexcept { return reverse_iterator_2d{end2d(r)}; }
		[[nodiscard]] auto rbegin2d(const pl::rectangle<> &r) const noexcept { return const_reverse_iterator_2d{end2d(r)}; }
		[[nodiscard]] auto crbegin2d(const pl::rectangle<> &r) const noexcept { return const_reverse_iterator_2d{cend2d(r)}; }
		[[nodiscard]] auto rend2d(const pl::rectangle<> &r) noexcept { return reverse_iterator_2d{begin2d(r)}; }
		[[nodiscard]] auto rend2d(const pl::rectangle<> &r) const noexcept { return const_reverse_iterator_2d{begin2d(r)}; }
		[[nodiscard]] auto crend2d(const pl::rectangle<> &r) const noexcept { return const_reverse_iterator_2d{cbegin2d(r)}; }

		[[nodiscard]] auto range2d(const pl::rectangle<> &r) noexcept { return rg::make_subrange(begin2d(r), end2d(r)); }
		[[nodiscard]] auto range2d(const pl::rectangle<> &r) const noexcept { return rg::make_subrange(begin2d(r), end2d(r)); }
		[[nodiscard]] auto crange2d(const pl::rectangle<> &r) const noexcept { return rg::make_subrange(cbegin2d(r), cend2d(r)); }
		[[nodiscard]] auto rrange2d(const pl::rectangle<> &r) noexcept { return rg::make_subrange(rbegin2d(r), rend2d(r)); }
		[[nodiscard]] auto rrange2d(const pl::rectangle<> &r) const noexcept { return rg::make_subrange(rbegin2d(r), rend2d(r)); }
		[[nodiscard]] auto crrange2d(const pl::rectangle<> &r) const noexcept { return rg::make_subrange(crbegin2d(r), crend2d(r)); }

		[[nodiscard]] auto begin3d(const pl::box<VecP3i> &r) noexcept { return iterator_3d{*this, r}; }
		[[nodiscard]] auto begin3d(const pl::box<VecP3i> &r) const noexcept { return const_iterator_3d{*this, r}; }
		[[nodiscard]] auto cbegin3d(const pl::box<VecP3i> &r) const noexcept { return const_iterator_3d{*this, r}; }
		[[nodiscard]] auto end3d(const pl::box<VecP3i> &r) noexcept { return iterator_3d{*this, r, true}; }
		[[nodiscard]] auto end3d(const pl::box<VecP3i> &r) const noexcept { return const_iterator_3d{*this, r, true}; }
		[[nodiscard]] auto cend3d(const pl::box<VecP3i> &r) const noexcept { return const_iterator_3d{*this, r, true}; }

		[[nodiscard]] auto rbegin3d(const pl::box<VecP3i> &r) noexcept { return reverse_iterator_3d{end3d(r)}; }
		[[nodiscard]] auto rbegin3d(const pl::box<VecP3i> &r) const noexcept { return const_reverse_iterator_3d{end3d(r)}; }
		[[nodiscard]] auto crbegin3d(const pl::box<VecP3i> &r) const noexcept { return const_reverse_iterator_3d{cend3d(r)}; }
		[[nodiscard]] auto rend3d(const pl::box<VecP3i> &r) noexcept { return reverse_iterator_3d{begin3d(r)}; }
		[[nodiscard]] auto rend3d(const pl::box<VecP3i> &r) const noexcept { return const_reverse_iterator_3d{begin3d(r)}; }
		[[nodiscard]] auto crend3d(const pl::box<VecP3i> &r) const noexcept { return const_reverse_iterator_3d{cbegin3d(r)}; }

		[[nodiscard]] auto range3d(const pl::box<VecP3i> &r) noexcept { return rg::make_subrange(begin3d(r), end3d(r)); }
		[[nodiscard]] auto range3d(const pl::box<VecP3i> &r) const noexcept { return rg::make_subrange(begin3d(r), end3d(r)); }
		[[nodiscard]] auto crange3d(const pl::box<VecP3i> &r) const noexcept { return rg::make_subrange(cbegin3d(r), cend3d(r)); }
		[[nodiscard]] auto rrange3d(const pl::box<VecP3i> &r) noexcept { return rg::make_subrange(rbegin3d(r), rend3d(r)); }
		[[nodiscard]] auto rrange3d(const pl::box<VecP3i> &r) const noexcept { return rg::make_subrange(rbegin3d(r), rend3d(r)); }
		[[nodiscard]] auto crrange3d(const pl::box<VecP3i> &r) const noexcept { return rg::make_subrange(crbegin3d(r), crend3d(r)); }

		using IBitmap1DRead::operator[];

		template<szt I>
		[[nodiscard]] decltype(auto) operator[] (const size_c<I>) const noexcept { return (*this)[I]; }
		template<szt I>
		[[nodiscard]] decltype(auto) operator[] (const size_c<I>) noexcept { return (*this)[I]; }

		[[nodiscard]] virtual Color operator() (Real u) const noexcept override;
		[[nodiscard]] virtual Color operator() (Real u, Real v) const noexcept override;
		[[nodiscard]] virtual Color operator() (Real u, Real v, Real w) const noexcept override;

		[[nodiscard]] reference getPixelRef(szt offset) noexcept { return reference{*this, offset}; }
		[[nodiscard]] const_reference getPixelRef(szt offset) const noexcept { return const_reference{*this, offset}; }

		virtual void setLinearFilter(bool filter) { mLinearFilter = filter; }
		virtual bool isLinearFilter() const noexcept { return mLinearFilter; }

		virtual void setTiled(bool tiled) { mTiled = tiled; }
		virtual bool isTiled() const noexcept { return mTiled; }

		virtual BitmapBase &operator-= (const BitmapBase &other);
		virtual BitmapBase &operator+= (const BitmapBase &other);
		virtual BitmapBase &operator/= (const BitmapBase &other);
		virtual BitmapBase &operator*= (const BitmapBase &other);

		virtual void fitToAspectRatio(if32 width, if32 height, szt maxNbPixels);

		virtual void allocate(const Dir3Di dim);
		virtual void shrink_to_fit() = 0;

		void allocate(const Dir2Di dim)
		{
			Dir3Di newDim{dim};
			newDim.set(1, size_v<2>);
			allocate(newDim);
		}

		void freeBitmap() noexcept { allocate(Dir3Di{}); }

		[[nodiscard]] bool isEmpty() const noexcept { return !getNbPixels(); }

		[[nodiscard]] decltype(auto) at(szt offset) { if(offset >= getNbPixels()) throw std::out_of_range("Offset out of bounds of bitmap."); return getPixelRef(offset); }
		[[nodiscard]] decltype(auto) at(szt offset) const { if(offset >= getNbPixels()) throw std::out_of_range("Offset out of bounds of bitmap."); return getPixelRef(offset); }
		[[nodiscard]] decltype(auto) front() noexcept { return getPixelRef(0); }
		[[nodiscard]] decltype(auto) front() const noexcept { return getPixelRef(0); }
		[[nodiscard]] decltype(auto) back() noexcept { return getPixelRef(getNbPixels() - 1); }
		[[nodiscard]] decltype(auto) back() const noexcept { return getPixelRef(getNbPixels() - 1); }
		[[nodiscard]] auto size() const noexcept { return getNbPixels(); }
		[[nodiscard]] bool empty() const noexcept { return isEmpty(); }

		virtual void zeroBitmap() override = 0;

		[[nodiscard]] virtual void *getRawData() const noexcept = 0;
		[[nodiscard]] virtual bool isContiguous() const noexcept = 0;
		[[nodiscard]] virtual std::any getUnderlyingPixelType() const noexcept = 0;
		virtual void copyIntoContiguousMemory(void *ptr) const = 0;

		[[nodiscard]] virtual Color getPixel(if32 u, if32 v, if32 w) const noexcept override { const auto sz = getSize(); return getPixel(w * (sz[0] * sz[1]) + v * sz[0] + u); }
		[[nodiscard]] virtual Color getPixel(if32 u, if32 v) const noexcept override { return getPixel(v * getWidth() + u); }
		[[nodiscard]] virtual Color getPixel(szt) const noexcept override { return Black; }

		virtual void setPixel(Color color, szt offset) override = 0;
		virtual void setPixel(Color color, if32 u, if32 v) override { setPixel(color, v * getWidth() + u); }
		virtual void setPixel(Color color, if32 u, if32 v, if32 w) override { const auto sz = getSize(); setPixel(color, w * (sz[0] * sz[1]) + v * sz[0] + u); }

		using IBitmap1DRead::getNbPixels;
		using IBitmap1DRead::getSize;
		using IBitmap1DRead::getWidth;
		using IBitmap2DRead::getHeight;
		using IBitmap3DRead::getDepth;

		[[nodiscard]] virtual szt getMemSize() const noexcept = 0;

		[[nodiscard]] virtual pl::rectangle<> getRect() const noexcept final { const auto sz = getSize(); return {0, 0, sz[0], sz[1]}; }
		[[nodiscard]] virtual pl::box<VecP3i> getBox() const noexcept final { const auto sz = getSize(); return {{0, 0, 0}, (VecP3i)sz}; }

		[[nodiscard]] IBitmap1DRead &get1Dread() noexcept { return *this; }
		[[nodiscard]] const IBitmap1DRead &get1Dread() const noexcept { return *this; }

		[[nodiscard]] IBitmap2DRead &get2Dread() noexcept { return *this; }
		[[nodiscard]] const IBitmap2DRead &get2Dread() const noexcept { return *this; }

		[[nodiscard]] IBitmap3DRead &get3Dread() noexcept { return *this; }
		[[nodiscard]] const IBitmap3DRead &get3Dread() const noexcept { return *this; }

		[[nodiscard]] IBitmap1DWrite &get1Dwrite() noexcept { return *this; }
		[[nodiscard]] const IBitmap1DWrite &get1Dwrite() const noexcept { return *this; }

		[[nodiscard]] IBitmap2DWrite &get2Dwrite() noexcept { return *this; }
		[[nodiscard]] const IBitmap2DWrite &get2Dwrite() const noexcept { return *this; }

		[[nodiscard]] IBitmap3DWrite &get3Dwrite() noexcept { return *this; }
		[[nodiscard]] const IBitmap3DWrite &get3Dwrite() const noexcept { return *this; }

		BitmapBase &operator= (const IBitmap1DRead &other)
		{
			return copyData(other);
		}

		BitmapBase &operator= (const IBitmap2DRead &other)
		{
			return copyData(other);
		}

		BitmapBase &operator= (const IBitmap3DRead &other)
		{
			return copyData(other);
		}

		virtual BitmapBase &copyData(const BitmapBase &bitmap);
		virtual BitmapBase &copyData(const IBitmap1DRead &bitmap);
		virtual BitmapBase &copyData(const IBitmap2DRead &bitmap);
		virtual BitmapBase &copyData(const IBitmap3DRead &bitmap);

		template <typename OP_, bool GetIntoPixel_ = false, pl::CParallelPolicy Policy_ = pl::policy::default_policy_t>
		const BitmapBase &transform(const IBitmap1DRead &other, IBitmap1DWrite &into, OP_&& fc, const bool_c<GetIntoPixel_> = {}, const Policy_ pol = {}) const
		{
			if constexpr(GetIntoPixel_)
			{
				auto &intoRead = dynamic_cast<const IBitmap1DRead &>(into);

				mir( pl::plmin(other.getNbPixels(), getNbPixels()) ) |
					pl::for_each
					(
						[&](auto i){
							into.setPixel(intoRead.getPixel(i), PL_FWD(fc)(getPixel(i), other.getPixel(i)), i);
						},
						std::identity{}, pol
					);
			}
			else
			{
				mir( pl::plmin(other.getNbPixels(), getNbPixels()) ) |
					pl::for_each
					(
						[&](auto i){
							into.setPixel(PL_FWD(fc)(getPixel(i), other.getPixel(i)), i);
						},
						std::identity{}, pol
					);
			}

			return *this;
		}

		[[nodiscard]] virtual szt getBytesPerPixels() const noexcept = 0;

		/*[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;
		virtual void setParameters(const ParameterTree &params) override;*/

	protected:
		bool mLinearFilter = true, mTiled = true;

		virtual void allocMemory(const Dir3Di dim) = 0;

		void setDimensions(const Dir3Di dim);

	};



	template <typename BitmapType_>
	class BitmapReadChainBase : public RenderEntity, public virtual BitmapType_
	{
	public:
		BitmapReadChainBase() = default;
		BitmapReadChainBase(EntityPtr<const BitmapType_> bitmap) : mBitmap(std::move(bitmap)) {}

		void setBitmap(EntityPtr<const BitmapType_> map) { mBitmap = std::move(map); }
		[[nodiscard]] const auto &getBitmap() const noexcept { return mBitmap; }

		virtual void setParameters(const ParameterTree &params) override
		{
			RenderEntity::setParameters(params);

			setBitmap(addParamReference(type_v<BitmapType_>, params, "bitmap"));
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {RenderEntity::getParameters(deep)};

			if(mBitmap)
				params["bitmap"].fromEntityRef(*mBitmap, deep);

			return params;
		}

		[[nodiscard]] virtual if32 getWidth() const noexcept final { return mBitmap->getWidth(); }
		[[nodiscard]] virtual szt getNbPixels() const noexcept final { return mBitmap->getNbPixels(); }
		[[nodiscard]] virtual Dir3Di getSize() const noexcept final { return mBitmap->getSize(); }

	protected:
		EntityPtr<const BitmapType_> mBitmap;
	};

	class BitmapReadChainBase1D : public BitmapReadChainBase<IBitmap1DRead>, public virtual IColorDistribution1D
	{
	public:

		[[nodiscard]] virtual Color getPixel(szt offset) const noexcept override
			{ return mBitmap->getPixel(offset); }

		[[nodiscard]] virtual Color operator() (Real u) const noexcept override
			{ return getPixel(u * mBitmap->getNbPixels()); }
	};

	class BitmapReadChainBase2D : public BitmapReadChainBase<IBitmap2DRead>, public virtual IColorDistribution2D
	{
	public:
		[[nodiscard]] virtual Color getPixel(szt offset) const noexcept override { return mBitmap->getPixel(offset); }
		[[nodiscard]] virtual Color getPixel(if32 u, if32 v) const noexcept override { return mBitmap->getPixel(u, v); }

		[[nodiscard]] virtual if32 getHeight() const noexcept final { return mBitmap->getHeight(); }
		[[nodiscard]] virtual pl::rectangle<> getRect() const noexcept final { return mBitmap->getRect(); }

		[[nodiscard]] virtual Color operator() (Real u, Real v) const noexcept override
			{ return getPixel(u * mBitmap->getWidth(), v * mBitmap->getHeight()); }
	};

	class BitmapReadChainBase3D : public BitmapReadChainBase<IBitmap3DRead>, public virtual IColorDistribution3D
	{
	public:
		[[nodiscard]] virtual Color getPixel(szt offset) const noexcept override { return mBitmap->getPixel(offset); }
		[[nodiscard]] virtual Color getPixel(if32 u, if32 v, if32 w) const noexcept override { return mBitmap->getPixel(u, v, w); }
		[[nodiscard]] virtual if32 getHeight() const noexcept final { return mBitmap->getHeight(); }
		[[nodiscard]] virtual if32 getDepth() const noexcept final { return mBitmap->getDepth(); }
		[[nodiscard]] virtual pl::box<VecP3i> getBox() const noexcept final { return mBitmap->getBox(); }

		[[nodiscard]] virtual Color operator() (Real u, Real v, Real w) const noexcept override
			{ return getPixel(u * mBitmap->getWidth(), v * mBitmap->getHeight(), w * mBitmap->getDepth()); }
	};

	class BitmapReadInvert1D : public BitmapReadChainBase1D
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("bitmapread-invert1d", BitmapReadInvert1D)

		[[nodiscard]] virtual Color getPixel(szt offset) const noexcept override
			{ return invertColor(mBitmap->getPixel(offset)); }
	};

	class BitmapReadInvert2D : public BitmapReadChainBase2D
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("bitmapread-invert2d", BitmapReadInvert2D)

		[[nodiscard]] virtual Color getPixel(szt offset) const noexcept override
			{ return invertColor(mBitmap->getPixel(offset)); }
		[[nodiscard]] virtual Color getPixel(if32 u, if32 v) const noexcept override
			{ return invertColor(mBitmap->getPixel(u, v)); }
	};

	class BitmapReadInvert3D : public BitmapReadChainBase3D
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("bitmapread-invert3d", BitmapReadInvert3D)

		[[nodiscard]] virtual Color getPixel(szt offset) const noexcept override
			{ return invertColor(mBitmap->getPixel(offset)); }
		[[nodiscard]] virtual Color getPixel(if32 u, if32 v, if32 w) const noexcept override
			{ return invertColor(mBitmap->getPixel(u, v, w)); }
	};


	class PHOTORT_API BitmapReadFiltered2D : public BitmapReadChainBase2D
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("bitmapread-filter2d", BitmapReadFiltered2D)

		using BitmapReadChainBase2D::BitmapReadChainBase2D;

		void setFilter(EntityPtr<const IColorFilterDistribution2D> f) { mFilter.mDistribution = std::move(f); }
		[[nodiscard]] const auto &getFilter() const noexcept { return mFilter.mDistribution; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

		[[nodiscard]] virtual Color getPixel(if32 u, if32 v) const noexcept override;
		[[nodiscard]] virtual Color getPixel(szt offset) const noexcept override { return getPixel(offset % mBitmap->getWidth(), offset / mBitmap->getWidth()); }

	protected:
		ColorFilter2D mFilter;

	};


	template <typename Bitmap_, typename VTP_, typename VTD_, typename FC_>
	void bitmapWalk(const Bitmap_ &bmp, const VTP_ &start, const VTD_ &dir, typename VTD_::value_type maxDist, typename VTD_::value_type step, FC_&& fc)
	{
		static constexpr auto NbDim = VTD_::NbValues > 3 ? 3 : VTD_::NbValues;

		auto pos = start;
		auto stepInc = dir * step;
		auto dist = 0;
		for(const auto i : mir( if32(maxDist / step) ))
		{
			if(!PL_FWD(fc)(i, dist, pos, pl::index_sequence_call(size_v<NbDim>, [&](auto&&... index){ return bmp(pos[index]...); })))
				break;

			pos += stepInc;
			dist += step;
		}
	}

}
