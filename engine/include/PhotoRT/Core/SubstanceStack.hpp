/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Material_fwd.hpp"
#include "Interaction_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"
#include "Statistics.hpp"

#include <boost/container/static_vector.hpp>

namespace PhotoRT
{
	struct SubstanceEntryEvent
	{
		const MaterialLayer *enterSubsEvent, *exitSubsEvent;
	};

	class PHOTORT_API SubstanceStack
	{
	public:
		struct SubstanceItem
		{
			const MaterialLayer *mat;
			u32 priority;
		};

		using ArrayType = boost::container::static_vector<SubstanceItem, PRT_MAX_SUBSTANCES>;

		void print(std::ostream &os) const;

		void pop() noexcept { mSubstances.pop_back(); }

		void push(const MaterialLayer &subs, u32 priority) noexcept
		{
			SubstanceItem item{&subs, priority};

			if(mSubstances.empty())
			{
				mSubstances.push_back(item);
				return;
			}

			mSubstances.emplace(
				rg::find_if(rgv::all(mSubstances) | rgv::reverse, [&](auto&& e){ return priority >= e.priority; }).base(),
				item
			);
		}

		[[nodiscard]] const MaterialLayer &back() const noexcept { return *mSubstances.back().mat; }
		[[nodiscard]] const MaterialLayer &previous() const noexcept { plassert(mSubstances.size() > 1); return *mSubstances[mSubstances.size() - 2].mat; }

		void reset()
		{
			mSubstances.clear();
			//std::cout << "Substance stack reset\n";
		}

		[[nodiscard]] auto size() const noexcept { return mSubstances.size(); }

		[[nodiscard]] bool full() const noexcept
			{ return mSubstances.size() == mSubstances.capacity(); }

		[[nodiscard]] bool hasSubstance(const MaterialLayer &subs) const noexcept
		{
			return rg::find_if(mSubstances, [&subs](const auto &e) { return e.mat == &subs; }) != mSubstances.end();
		}

		[[nodiscard]] auto findSubstance(const MaterialLayer &subs) const noexcept
		{
			return rg::find_if(mSubstances, [&subs](const auto &e) { return e.mat == &subs; });
		}

		[[nodiscard]] auto findSubstance(const MaterialLayer &subs) noexcept
		{
			return rg::find_if(mSubstances, [&subs](const auto &e) { return e.mat == &subs; });
		}

		void remove(const MaterialLayer &subs) noexcept
			{ auto it = findSubstance(subs); plassert(it != mSubstances.end()); mSubstances.erase(it); }

		[[nodiscard]] SubstanceEntryEvent checkSubstanceEntry(EnumHemisphere hemisphere) const noexcept;
		void interaction(SurfaceInteraction &surface) noexcept;
		void updateSubstanceStack(const SubstanceEntryEvent &evt) noexcept;
		void initialSubstance(const MaterialLayer &substance) noexcept;

		[[nodiscard]] const MaterialLayer *getEnteredSub() const noexcept { return mSubstances.empty() ? nullptr : &back(); }

	private:

		ArrayType mSubstances;
		const MaterialLayer *mNextSubstance = nullptr, *mNextEnterSubsEvent = nullptr, *mNextExitSubsEvent = nullptr;

		friend std::ostream& operator<<(std::ostream& os, const SubstanceStack &s)
		{
			s.print(os); return os;
		}
	};
}