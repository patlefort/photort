/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Material_fwd.hpp"
#include "Instance_fwd.hpp"
#include "PrimitiveGroup_fwd.hpp"
#include "Medium_fwd.hpp"
#include "Scene_fwd.hpp"
#include "Sampling_fwd.hpp"
#include "RayTracer_fwd.hpp"
#include "RayTraversalContainers_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"
#include "Statistics.hpp"
#include "Ray.hpp"
#include "SurfaceData.hpp"

#include <patlib/bitset.hpp>

#include <boost/container/static_vector.hpp>

namespace PhotoRT
{
	// Rendering statistics
	namespace stats
	{

		using AccumulatorRay = StatsAccumulator<
			StatCounter,
			StatSum
		>;

		using AccumulatorSample = StatsAccumulator<
			StatCounter,
			StatSum
		>;

		using AccumulatorColTest = StatsAccumulator<
			StatCounter,
			StatSum, StatMin, StatMax, StatAvg
		>;

		using AccumulatorAABBTest = StatsAccumulator<
			StatCounter,
			StatSum, StatMin, StatMax, StatAvg
		>;

	}

	[[nodiscard]] constexpr EnumTransportMode switchTransportMode(EnumTransportMode mode) noexcept
		{ return mode == EnumTransportMode::Importance ? EnumTransportMode::Radiance : EnumTransportMode::Importance; }


	struct RayBatchEntry
	{
		Ray ray;
		CollisionResult result;
		Real distmax;
	};

	constexpr auto RayBatchMaxSize = sizeof(umint) * CHAR_BIT;
	using RayBatchMask = std::bitset<RayBatchMaxSize>;

	using RayBatch = boost::container::static_vector<RayBatchEntry, RayBatchMaxSize>;

	[[nodiscard]] inline RayBatchMask initRayBatchMask(const RayBatch &rb) noexcept
		{ return std::numeric_limits<umint>::max() >> (RayBatchMaxSize - rb.size()); }

	inline RayBatchMask prepareRayBatch(RayBatch &rb) noexcept
	{
		for(auto &d : rb)
		{
			d.ray.prepareForTracing();
			d.distmax = pl::plisinf(d.ray.length) ? d.ray.length : d.ray.length /opow2;
		}

		return initRayBatchMask(rb);
	}


	class IRayTracer
	{
	public:
		virtual ~IRayTracer() = default;

		virtual pl::marked_optional<CollisionData> traceRay(const Ray &ray) noexcept = 0;
		virtual RayBatchMask traceRayBatch(RayBatch &batch) noexcept = 0;

		[[nodiscard]] auto traceRay(const VecP origin, const VecD dir, Real length = Ray::MaxDistance) noexcept
		{
			return traceRay({origin, dir, EnumRayType::Primary, length});
		}

		[[nodiscard]] virtual StatsAccumulatorList getStats() const = 0;
		virtual void resetStats() = 0;

		[[nodiscard]] virtual const Scene *getScene() const = 0;
	};

	class IRayTracerEngine
	{
	public:
		virtual ~IRayTracerEngine() = default;

		[[nodiscard]] virtual const Scene *getScene() const = 0;
		virtual void init(const Scene &scene) = 0;

		virtual void printInfo(std::ostream &outputStream) const = 0;

		[[nodiscard]] virtual bool isReady() const = 0;

		[[nodiscard]] virtual std::unique_ptr<IRayTracer> createTracer(int index = -1, bool withStats = true) = 0;
	};

	template <typename T_>
	class RayTracerVirtual : public T_, public IRayTracer
	{
		[[nodiscard]] auto &typecast() { return static_cast<T_ &>(*this); }
		[[nodiscard]] const auto &typecast() const { return static_cast<const T_ &>(*this); }

	public:

		using T_::T_;

		virtual pl::marked_optional<CollisionData> traceRay(const Ray &ray) noexcept override
			{ return typecast().traceRay(ray); }
		virtual RayBatchMask traceRayBatch(RayBatch &batch) noexcept override
			{ return typecast().traceRayBatch(batch); }

		[[nodiscard]] virtual StatsAccumulatorList getStats() const override
			{ return typecast().getStats(); }
		virtual void resetStats() override
			{ typecast().resetStats(); }

		[[nodiscard]] virtual const Scene *getScene() const override { return typecast().getScene(); }
	};

	struct TracerParams
	{
		IRayTracer &rayTracer;
		Sampler &sampler;
	};

}
