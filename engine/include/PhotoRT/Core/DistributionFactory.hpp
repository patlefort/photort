/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Base.hpp"
#include "RenderEntity.hpp"
#include "RenderEntityFactory.hpp"
#include "Distribution.hpp"

namespace PhotoRT::DistFactoriesImpl
{
	template <typename InType_, typename OutType_, typename OperatorType_, typename TypeName_>
	struct RegisterDistributionCompare final : public RenderEntityAugment<DistributionMapperCompare<InType_, OutType_, OperatorType_>>
	{
		[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(TypeName_::name); }

		virtual void setParameters(const ParameterTree &params) final
		{
			params.getValueIfExists
				("to", this->mCompare)
				("true", this->mTrue)
				("false", this->mFalse);
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool /*deep*/ = false) const final
		{
			ParameterTree params;

			params["to"] = this->mCompare;
			params["true"] = this->mTrue;
			params["false"] = this->mFalse;

			return params;
		}
	};

	template <typename InType_, typename OutType_, typename TypeName_>
	struct RegisterDistributionsCompare
	{
		struct GreaterEqual { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_greater_equal"); };
		struct Greater { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_greater"); };
		struct Equal { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_equal"); };
		struct Less { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_less"); };
		struct LessEqual { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_less_equal"); };

		EntityFactoryProxy<EntityFactory< RegisterDistributionCompare<InType_, OutType_, pl::ops::op_greater_equal_t, GreaterEqual> >> f_greater_equal{GreaterEqual::name};
		EntityFactoryProxy<EntityFactory< RegisterDistributionCompare<InType_, OutType_, pl::ops::op_greater_t, Greater> >> f_greater{Greater::name};
		EntityFactoryProxy<EntityFactory< RegisterDistributionCompare<InType_, OutType_, pl::ops::op_equal_to_t, Equal> >> f_equal{Equal::name};
		EntityFactoryProxy<EntityFactory< RegisterDistributionCompare<InType_, OutType_, pl::ops::op_less_t, Less> >> f_less{Less::name};
		EntityFactoryProxy<EntityFactory< RegisterDistributionCompare<InType_, OutType_, pl::ops::op_less_equal_t, LessEqual> >> f_less_equal{LessEqual::name};
	};

	template <typename InType_, typename OutType_, typename OperatorType_, typename TypeName_>
	struct RegisterDistributionAdaptor : public RenderEntityAugment<DistributionMapperAdaptor<InType_, OutType_, OperatorType_>>
	{
		[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(TypeName_::name); }

		virtual void setParameters(const ParameterTree &params) final
		{
			this->addParamReference(this->mMap, params, "map");
			this->addParamReference(this->mNext, params, "next");
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const final
		{
			ParameterTree params;

			if(this->mMap)
				params["map"].fromEntityRef(*this->mMap, deep);

			if(this->mNext)
				params["next"].fromEntityRef(*this->mNext, deep);

			return params;
		}
	};

	template <typename InType_, typename OutType_, typename TypeName_>
	struct RegisterDistributionsAdaptor
	{
		struct Plus { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_plus"); };
		struct Minus { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_minus"); };
		struct Multiply { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_multiply"); };
		struct Divide { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_divide"); };
		struct Min { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_min"); };
		struct Max { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_max"); };
		struct Mod { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_mod"); };

		struct GreaterEqual { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_greater_equal"); };
		struct Greater { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_greater"); };
		struct Equal { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_equal"); };
		struct Less { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_less"); };
		struct LessEqual { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_less_equal"); };

		EntityFactoryProxy<EntityFactory< RegisterDistributionAdaptor<InType_, OutType_, std::plus<>, Plus> >> f_plus{Plus::name};
		EntityFactoryProxy<EntityFactory< RegisterDistributionAdaptor<InType_, OutType_, std::minus<>, Minus> >> f_minus{Minus::name};
		EntityFactoryProxy<EntityFactory< RegisterDistributionAdaptor<InType_, OutType_, std::multiplies<>, Multiply> >> f_multiply{Multiply::name};
		EntityFactoryProxy<EntityFactory< RegisterDistributionAdaptor<InType_, OutType_, std::divides<>, Divide> >> f_divide{Divide::name};
		EntityFactoryProxy<EntityFactory< RegisterDistributionAdaptor<InType_, OutType_, pl::ops::op_min_t, Min> >> f_min{Min::name};
		EntityFactoryProxy<EntityFactory< RegisterDistributionAdaptor<InType_, OutType_, pl::ops::op_max_t, Max> >> f_max{Max::name};
		EntityFactoryProxy<EntityFactory< RegisterDistributionAdaptor<InType_, OutType_, pl::ops::op_mod_t, Mod> >> f_mod{Mod::name};

		EntityFactoryProxy<EntityFactory< RegisterDistributionAdaptor<InType_, OutType_, pl::ops::op_greater_equal_t, GreaterEqual> >> f_greater_equal{GreaterEqual::name};
		EntityFactoryProxy<EntityFactory< RegisterDistributionAdaptor<InType_, OutType_, pl::ops::op_greater_t, Greater> >> f_greater{Greater::name};
		EntityFactoryProxy<EntityFactory< RegisterDistributionAdaptor<InType_, OutType_, pl::ops::op_equal_to_t, Equal> >> f_equal{Equal::name};
		EntityFactoryProxy<EntityFactory< RegisterDistributionAdaptor<InType_, OutType_, pl::ops::op_less_t, Less> >> f_less{Less::name};
		EntityFactoryProxy<EntityFactory< RegisterDistributionAdaptor<InType_, OutType_, pl::ops::op_less_equal_t, LessEqual> >> f_less_equal{LessEqual::name};
	};

	template <typename InType_, typename OutType_, typename TypeName_>
	struct RegisterDistributionsAdaptorWithBinary : public RegisterDistributionsAdaptor<InType_, OutType_, TypeName_>
	{
		struct And { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_and"); };
		struct Or { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_or"); };
		struct Xor { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_xor"); };

		EntityFactoryProxy<EntityFactory< RegisterDistributionAdaptor<InType_, OutType_, std::bit_and<>, And> >> f_and{And::name};
		EntityFactoryProxy<EntityFactory< RegisterDistributionAdaptor<InType_, OutType_, std::bit_or<>, Or> >> f_or{And::name};
		EntityFactoryProxy<EntityFactory< RegisterDistributionAdaptor<InType_, OutType_, std::bit_xor<>, Xor> >> f_xor{And::name};
	};


	template <typename Type_, typename TypeName_>
	struct RegisterIOdistributions
	{
		using ObjectType = DistributionMapperMultiIO<Type_>;

		struct Multiio { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "-multiio"); };

		class AugmentedClass final : public RenderEntityAugment<ObjectType>
		{
		public:
			[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(Multiio::name); }

			using ObjectType::operator<<;

			virtual void setParameters(const ParameterTree &params) final
			{
				this->mMappers.clear();

				if(const auto p = params.findParam("maps"); p && p->childParams)
					for(const auto &param : *p->childParams)
					{
						if(!param.second.value.empty())
							*this << this->template addReference(type_v<IDistributionMapper<Type_, Type_>>, param.second);
					}
			}

			[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const final
			{
				ParameterTree params;

				uf32 i = 0;
				for(auto &map : this->mMappers)
				{
					params["maps"][i].fromEntityRef(*map, deep);
					i++;
				}

				return params;
			}
		};

		EntityFactoryProxy<EntityFactory< AugmentedClass >> f_multi{Multiio::name};
	};

	template <typename InType_, typename OutType_, class OperatorType_, typename TypeName_>
	struct RegisterMultiDistribution
	{
		using ObjectType = DistributionMapperMulti<InType_, OutType_, OperatorType_>;

		class AugmentedClass final : public RenderEntityAugment<ObjectType>
		{
		public:
			[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(TypeName_::name); }

			using maps1d = typename ObjectType::maps1d;
			using maps2d = typename ObjectType::maps2d;
			using maps3d = typename ObjectType::maps3d;

			using maps1d::operator<<;
			using maps2d::operator<<;
			using maps3d::operator<<;

			virtual void setParameters(const ParameterTree &params) final
			{
				maps1d::mMappers.clear();

				if(const auto p = params.findParam("maps1d"); p && p->childParams)
					for(const auto &param : *p->childParams)
					{
						if(!param.second.value.empty())
							*this << this->addReference(type_v<IDistributionMapper<InType_, OutType_>>, param.second);
					}

				maps2d::mMappers.clear();

				if(const auto p = params.findParam("maps2d"); p && p->childParams)
					for(const auto &param : *p->childParams)
					{
						if(!param.second.value.empty())
							*this << this->addReference(type_v<IDistributionMapper2D<InType_, OutType_>>, param.second);
					}

				maps3d::mMappers.clear();

				if(const auto p = params.findParam("maps3d"); p && p->childParams)
					for(const auto &param : *p->childParams)
					{
						if(!param.second.value.empty())
							*this << this->addReference(type_v<IDistributionMapper3D<InType_, OutType_>>, param.second);
					}
			}

			[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const final
			{
				ParameterTree params;

				uf32 i = 0;
				for(const auto &map : this->maps1d::mMappers)
				{
					params["maps1d"][i].fromEntityRef(*map, deep);
					i++;
				}

				i = 0;
				for(const auto &map : this->maps2d::mMappers)
				{
					params["maps2d"][i].fromEntityRef(*map, deep);
					i++;
				}

				i = 0;
				for(const auto &map : this->maps3d::mMappers)
				{
					params["maps3d"][i].fromEntityRef(*map, deep);
					i++;
				}

				return params;
			}
		};

		EntityFactoryProxy<EntityFactory< AugmentedClass >> f_multi{TypeName_::name};
	};

	template <typename InType_, typename OutType_, typename TypeName_>
	struct RegisterMultiDistributions
	{
		struct Plus { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_plus"); };
		struct Minus { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_minus"); };
		struct Multiply { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_multiply"); };
		struct Divide { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_divide"); };
		struct Min { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_min"); };
		struct Max { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_max"); };
		struct Mod { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_mod"); };

		struct GreaterEqual { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_greater_equal"); };
		struct Greater { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_greater"); };
		struct Equal { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_equal"); };
		struct Less { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_less"); };
		struct LessEqual { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_less_equal"); };

		RegisterMultiDistribution<InType_, OutType_, std::plus<>, Plus> f_plus;
		RegisterMultiDistribution<InType_, OutType_, std::minus<>, Minus> f_minus;
		RegisterMultiDistribution<InType_, OutType_, std::multiplies<>, Multiply> f_multiply;
		RegisterMultiDistribution<InType_, OutType_, std::divides<>, Divide> f_divide;
		RegisterMultiDistribution<InType_, OutType_, pl::ops::op_min_t, Min> f_min;
		RegisterMultiDistribution<InType_, OutType_, pl::ops::op_max_t, Max> f_max;
		RegisterMultiDistribution<InType_, OutType_, pl::ops::op_mod_t, Mod> f_mod;

		RegisterMultiDistribution<InType_, OutType_, pl::ops::op_greater_equal_t, GreaterEqual> f_greater_equal;
		RegisterMultiDistribution<InType_, OutType_, pl::ops::op_greater_t, Greater> f_greater;
		RegisterMultiDistribution<InType_, OutType_, pl::ops::op_equal_to_t, Equal> f_equal;
		RegisterMultiDistribution<InType_, OutType_, pl::ops::op_less_t, Less> f_less;
		RegisterMultiDistribution<InType_, OutType_, pl::ops::op_less_equal_t, LessEqual> f_less_equal;

	};

	template <typename InType_, typename OutType_, typename TypeName_>
	struct RegisterMultiDistributionsWithBinary : public RegisterMultiDistributions<InType_, OutType_, TypeName_>
	{
		struct And { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_and"); };
		struct Or { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_or"); };
		struct Xor { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "_xor"); };

		RegisterMultiDistribution<InType_, OutType_, std::bit_and<>, And> f_and;
		RegisterMultiDistribution<InType_, OutType_, std::bit_or<>, Or> f_or;
		RegisterMultiDistribution<InType_, OutType_, std::bit_xor<>, Xor> f_xor;
	};

	template <typename InType_, typename OutType_, typename TypeName_>
	struct RegisterDistributionMappers
	{
		struct Scaler1d { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "-scaler1d"); };
		struct Scaler2d { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "-scaler2d"); };
		struct Scaler3d { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "-scaler3d"); };
		struct Clamper { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "-clamper"); };
		struct Linear { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "-linear"); };
		struct Constant { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "-constant"); };
		struct Gaussian { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "-gaussian"); };
		struct Lanczos { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "-lanczos"); };
		struct Coswave { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "-coswave"); };
		struct Checker { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "-checker"); };
		struct Chain1d { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "-chain1d"); };
		struct Chain2d { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "-chain2d"); };
		struct Chain3d { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "-chain3d"); };
		struct Chainio { static inline constexpr auto name = pl::concat_char_arrays(TypeName_::name, "-chainio"); };

		class AugmentedClassScaler1D final : public RenderEntityAugment<DistributionMapperScaler1D<InType_, OutType_>>
		{
		public:
			[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(Scaler1d::name); }

			virtual void setParameters(const ParameterTree &params) final
			{
				params.getValueIfExists("u", this->mScaleu);
				this->addParamReference(this->mMap, params, "map");
			}

			[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const final
			{
				ParameterTree params;

				params["u"] = this->mScaleu;
				if(this->mMap)
					params["map"].fromEntityRef(*this->mMap, deep);

				return params;
			}
		};

		class AugmentedClassScaler2D final : public RenderEntityAugment<DistributionMapperScaler2D<InType_, OutType_>>
		{
		public:
			[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(Scaler2d::name); }

			virtual void setParameters(const ParameterTree &params) final
			{
				params.getValueIfExists
					("u", this->mScaleu)
					("v", this->mScalev);
				this->addParamReference(this->mMap, params, "map");
			}

			[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const final
			{
				ParameterTree params;

				params["u"] = this->mScaleu;
				params["v"] = this->mScalev;
				if(this->mMap)
					params["map"].fromEntityRef(*this->mMap, deep);

				return params;
			}
		};

		class AugmentedClassScaler3D final : public RenderEntityAugment<DistributionMapperScaler3D<InType_, OutType_>>
		{
		public:
			[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(Scaler3d::name); }

			virtual void setParameters(const ParameterTree &params) final
			{
				params.getValueIfExists
					("u", this->mScaleu)
					("v", this->mScalev)
					("w", this->mScalew);
				this->addParamReference(this->mMap, params, "map");
			}

			[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const final
			{
				ParameterTree params;

				params["u"] = this->mScaleu;
				params["v"] = this->mScalev;
				params["w"] = this->mScalew;
				if(this->mMap)
					params["map"].fromEntityRef(*this->mMap, deep);

				return params;
			}
		};

		class AugmentedClassClamper final : public RenderEntityAugment<DistributionMapperClamper<InType_, OutType_>>
		{
		public:
			[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(Clamper::name); }

			virtual void setParameters(const ParameterTree &params) final
			{
				params.getValueIfExists("min", this->mMinimum) ("max", this->mMaximum);
			}

			[[nodiscard]] virtual ParameterTree getParameters(bool /*deep*/ = false) const final
			{
				ParameterTree params;

				params["min"] = this->mMinimum;
				params["max"] = this->mMaximum;

				return params;
			}
		};

		class AugmentedClassLinear final : public RenderEntityAugment<DistributionMapperLinear<InType_, OutType_>>
		{
		public:
			[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(Linear::name); }

			virtual void setParameters(const ParameterTree &params) final
			{
				InType_ size{1}, start{};

				params.getValueIfExists("size", size) ("start", start);

				this->setConstant(size, start);
			}

			[[nodiscard]] virtual ParameterTree getParameters(bool /*deep*/ = false) const final
			{
				ParameterTree params;

				params["size"] = this->mSize;
				params["start"] = this->mStart;

				return params;
			}
		};

		class AugmentedClassConstant final : public RenderEntityAugment<DistributionMapperConstant<InType_, OutType_>>
		{
		public:
			[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(Constant::name); }

			virtual void setParameters(const ParameterTree &params) final
			{
				params.getValueIfExists("v", this->mValue);
			}

			[[nodiscard]] virtual ParameterTree getParameters(bool /*deep*/ = false) const final
			{
				ParameterTree params;

				params["v"] = this->mValue;

				return params;
			}
		};

		class AugmentedClassGaussian final : public RenderEntityAugment<DistributionMapperGaussian<InType_, OutType_>>
		{
		public:
			[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(Gaussian::name); }

			virtual void setParameters(const ParameterTree &params) final
			{
				InType_ width{1}, height{1}, position{}, alpha{1};

				params.getValueIfExists
					("width", width)
					("height", height)
					("position", position)
					("alpha", alpha);

				this->setGaussian(width, height, position, alpha);
			}

			[[nodiscard]] virtual ParameterTree getParameters(bool /*deep*/ = false) const final
			{
				ParameterTree params;

				params["width"] = this->mWidth;
				params["height"] = this->mHeight;
				params["position"] = this->mPosition;
				params["alpha"] = this->mAlpha;

				return params;
			}
		};

		class AugmentedClassLanczos final : public RenderEntityAugment<DistributionMapperLanczosSinc<InType_, OutType_>>
		{
		public:
			[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(Lanczos::name); }

			virtual void setParameters(const ParameterTree &params) final
			{
				InType_ width{1}, height{1}, position{}, tau{1};

				params.getValueIfExists
					("width", width)
					("height", height)
					("position", position)
					("tau", tau);

				this->setLanczos(width, height, position, tau);
			}

			[[nodiscard]] virtual ParameterTree getParameters(bool /*deep*/ = false) const final
			{
				ParameterTree params;

				params["width"] = this->mWidth;
				params["height"] = this->mHeight;
				params["position"] = this->mPosition;
				params["tau"] = this->mTau;

				return params;
			}
		};

		class AugmentedClassCosWave final : public RenderEntityAugment<DistributionMapperCosWave<InType_, OutType_>>
		{
		public:
			[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(Coswave::name); }

			virtual void setParameters(const ParameterTree &params) final
			{
				params.getValueIfExists
					("strength", this->mStrength)
					("start", this->mStart)
					("position", this->mPosition);
			}

			[[nodiscard]] virtual ParameterTree getParameters(bool /*deep*/ = false) const final
			{
				ParameterTree params;

				params["strength"] = this->mStrength;
				params["start"] = this->mStart;
				params["position"] = this->mPosition;

				return params;
			}
		};

		class AugmentedClassChecker final : public RenderEntityAugment<DistributionMapperChecker<InType_, OutType_>>
		{
			[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(Checker::name); }
		};

		class AugmentedClassChain final : public RenderEntityAugment<DistributionMapperChain1D<InType_, OutType_>>
		{
		public:
			[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(Chain1d::name); }

			virtual void setParameters(const ParameterTree &params) final
			{
				this->addParamReference(this->mMap, params, "map");
				this->addParamReference(this->mNext, params, "next");
			}

			[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const final
			{
				ParameterTree params;

				if(this->mMap)
					params["map"].fromEntityRef(*this->mMap, deep);

				if(this->mNext)
					params["next"].fromEntityRef(*this->mNext, deep);

				return params;
			}
		};

		class AugmentedClassChain2D final : public RenderEntityAugment<DistributionMapperChain2D<InType_, OutType_>>
		{
		public:
			[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(Chain2d::name); }

			virtual void setParameters(const ParameterTree &params) final
			{
				this->addParamReference(this->mMap, params, "map");
				this->addParamReference(this->mNext, params, "next");
			}

			[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const final
			{
				ParameterTree params;

				if(this->mMap)
					params["map"].fromEntityRef(*this->mMap, deep);

				if(this->mNext)
					params["next"].fromEntityRef(*this->mNext, deep);

				return params;
			}
		};

		class AugmentedClassChain3D final : public RenderEntityAugment<DistributionMapperChain3D<InType_, OutType_>>
		{
		public:
			[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(Chain3d::name); }

			virtual void setParameters(const ParameterTree &params) final
			{
				this->addParamReference(this->mMap, params, "map");
				this->addParamReference(this->mNext, params, "next");
			}

			[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const final
			{
				ParameterTree params;

				if(this->mMap)
					params["map"].fromEntityRef(*this->mMap, deep);

				if(this->mNext)
					params["next"].fromEntityRef(*this->mNext, deep);

				return params;
			}
		};

		class AugmentedClassChainIO final : public RenderEntityAugment<DistributionMapperChainIO<InType_, OutType_>>
		{
		public:
			[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return pl::to_string_view(Chainio::name); }

			virtual void setParameters(const ParameterTree &params) final
			{
				this->addParamReference(this->mMap, params, "map");
				this->addParamReference(this->mNext, params, "next");
			}

			[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const final
			{
				ParameterTree params;

				if(this->mMap)
					params["map"].fromEntityRef(*this->mMap, deep);

				if(this->mNext)
					params["next"].fromEntityRef(*this->mNext, deep);

				return params;
			}
		};

		EntityFactoryProxy<EntityFactory< AugmentedClassScaler1D >> f_scaler1d{Scaler1d::name};
		EntityFactoryProxy<EntityFactory< AugmentedClassScaler2D >> f_scaler2d{Scaler2d::name};
		EntityFactoryProxy<EntityFactory< AugmentedClassScaler3D >> f_scaler3d{Scaler3d::name};
		EntityFactoryProxy<EntityFactory< AugmentedClassClamper >> f_clamper{Clamper::name};
		EntityFactoryProxy<EntityFactory< AugmentedClassLinear >> f_linear{Linear::name};
		EntityFactoryProxy<EntityFactory< AugmentedClassConstant >> f_constant{Constant::name};
		EntityFactoryProxy<EntityFactory< AugmentedClassGaussian >> f_gaussian{Gaussian::name};
		EntityFactoryProxy<EntityFactory< AugmentedClassLanczos >> f_lanczos{Lanczos::name};
		EntityFactoryProxy<EntityFactory< AugmentedClassCosWave >> f_coswave{Coswave::name};
		EntityFactoryProxy<EntityFactory< AugmentedClassChecker >> f_checker{Checker::name};
		EntityFactoryProxy<EntityFactory< AugmentedClassChain >> f_chain{Chain1d::name};
		EntityFactoryProxy<EntityFactory< AugmentedClassChain2D >> f_chain2d{Chain2d::name};
		EntityFactoryProxy<EntityFactory< AugmentedClassChain3D >> f_chain3d{Chain3d::name};
		EntityFactoryProxy<EntityFactory< AugmentedClassChainIO >> f_chainio{Chainio::name};
	};

}
