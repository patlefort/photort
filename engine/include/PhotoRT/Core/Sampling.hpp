/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Sampling_fwd.hpp"

#include "Base.hpp"
#include "Random.hpp"
#include "Distribution.hpp"
#include "RenderEntity.hpp"

#include <pcg_random.hpp>

#include <patlib/marked_optional.hpp>

namespace PhotoRT
{
	template <typename SampleType_, typename PDFType_ = Real>
	struct Sample
	{
		SampleType_ value;
		PDFType_ pdf{};

		constexpr void reset() noexcept { pdf = {}; }
		[[nodiscard]] constexpr bool has_value() const noexcept { return pdf != PDFType_{}; }
	};

	template <typename T_, typename RndType_>
	class IPDFSampler
	{
	public:
		virtual ~IPDFSampler() = default;

		using value_type = T_;
		using RndType = RndType_;

		[[nodiscard]] virtual pl::marked_optional<Sample<T_>> operator() (RndType_ rnd) const noexcept = 0;
		virtual void requestSamples(std::span<pl::marked_optional<Sample<T_>>> samples, const RndType_ *rnd) const noexcept = 0;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(T_ v) const noexcept = 0;

		virtual void init() = 0;
	};

	template <typename T_, typename RndType_>
	class PDFSamplerBase : public IPDFSampler<T_, RndType_>
	{
	public:

		virtual void requestSamples(std::span<pl::marked_optional<Sample<T_>>> samples, const RndType_ *rnd) const noexcept override
		{
			for(const auto i : mir(samples.size()))
				samples[i] = (*this)(rnd[i]);
		}

		[[nodiscard]] virtual OptionalPDF<Real> PDF(T_) const noexcept override { return 1; }

		virtual void init() override {}
	};

	template <typename T_, typename RndType_>
	class PDFSampler : public PDFSamplerBase<T_, RndType_>
	{
	public:

		[[nodiscard]] virtual pl::marked_optional<Sample<T_>> operator() (RndType_ rnd) const noexcept override
		{
			return Sample<T_>{
				.value = rnd * mDimension,
				.pdf = 1
			};
		}

		[[nodiscard]] T_ getDimension() const noexcept { return mDimension; }
		void setDimension(T_ d) noexcept { mDimension = d; }

	protected:
		T_ mDimension{1};
	};



	/*template <typename T_>
	class ISampler
	{
	public:
		virtual ~ISampler() = default;

		using value_type = T_;

		virtual void requestSamples(std::span<T_> samples, const T_ *rng) noexcept = 0;
	};

	template <typename T_>
	class SamplerBasic : public virtual ISampler<T_>
	{
	public:

		virtual void requestSamples(std::span<T_> samples, const T_ *rng) noexcept override
		{
			std::copy_n(rng, samples.size(), samples.begin());
		}
	};

	class PHOTORT_API SamplerStratified1D : public Sampler1D
	{
	public:

		virtual void requestSamples(std::span<Real> samples, const Real *rng) noexcept override;
	};

	class PHOTORT_API SamplerStratified2D : public Sampler2D
	{
	public:

		virtual void requestSamples(std::span<Point2Dr> samples, const Point2Dr *rng) noexcept override;
	};*/

	class IMutator
	{
	public:
		virtual ~IMutator() = default;

		virtual RandomInt generate() noexcept = 0;
		virtual void newPath(std::vector<RandomInt> &sp, IRNGInt &rng1d) = 0;
		virtual void newIteration() = 0;
		virtual void accept() = 0;
		virtual void reject() = 0;

		RandomInt operator() () noexcept { return generate(); }
	};

	template <typename Mutator_>
	class MutatorFactory :
		public RenderEntity,
		public virtual IMutatorFactory,
		public pl::GenericSmartFactoryAllocBase<IMutator, Mutator_, pl::allocator_with_stats<Mutator_, memtag::Entity>>
	{};

	struct MutatorFactoryEntry
	{
		uf32 nbSamples = 4;
		EntityPtr<IMutatorFactory> factory;
	};

	struct MutatorEntry
	{
		uf32 nbSamples = 4;
		EntityPtr<IMutator> mutator;
	};

	/*class MutatorMetropolis : public IMutator
	{
	public:

		MutatorMetropolis(const MutatorFactoryMetropolis &factory)
			: mFactory(factory) {}

		virtual Real get1d() override;
		virtual Point2Dr get2d() override;
		virtual void newPath(std::vector<Real> &sp1d, std::vector<Point2Dr> &sp2d, IRNGInt &rng1d, IRNG2D &rng2d) override;
		virtual void newIteration() override;
		virtual void accept() override;
		virtual void reject() override;

	protected:
		IRNGInt *mRng1d = nullptr;
		IRNG2D *mRng2d = nullptr;

		const MutatorFactoryMetropolis &mFactory;

		Real mLargeStep;
		uf32 mIteration = 0, mLastLargeStepIteration = 0;
		uf32 mIndexSp1d = 0, mIndexSp2d = 0;
		//std::vector<uf32> lastModificationIteration1d, lastModificationIteration2d, bklmi1d, bklmi2d;
		std::vector<Real> mBksp1d, *mSp1d = nullptr;
		std::vector<Point2Dr> mBksp2d, *mSp2d = nullptr;
	};

	class MutatorFactoryMetropolis : public MutatorFactory<MutatorMetropolis>
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE(MutatorFactoryMetropolis)

		virtual EntityPtr<IMutator> create_shared() override
		{
			return construct_shared(*this);
		}

		virtual pl::UniquePtr<IMutator> create_unique() override
		{
			return construct_unique(type_v<IMutator>, *this);
		}

		virtual void setParameters(const ParameterTree &params) override
		{
			RenderEntity::setParameters(params);

			params.getValueIfExists
				("large_step_prob", mLargeStepProbability)
				("sigma", mSigma)
			;
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {RenderEntity::getParameters(deep)};

			params["large_step_prob"] = mLargeStepProbability;
			params["sigma"] = mSigma;

			return params;
		}

		Real mLargeStepProbability = 0.3, mSigma = 0.01;
	};*/

	class PHOTORT_API Sampler final : public IRNGInt
	{
	public:

		/*EntityPtr<IRNGInt> rng1d;
		EntityPtr<IRNG2D> rng2d;
		EntityPtr<Sampler1D> mSampler1D;
		EntityPtr<Sampler2D> mSampler2D;*/

		//RNGStdAdaptorStreams<RNGWrappable<pcg64>> mRng;
		RNGStdAdaptorStreams<pcg64> mRng;
		/*SamplerStratified1D mSampler1D;
		SamplerStratified2D mSampler2D;*/

		std::vector<RandomInt> mSamples;
		uf32 mSampleIndex = 0;

		std::vector<std::vector<RandomInt>> mRecordedsp;

		/*virtual void setParameters(const ParameterTree &params) final
		{
			addParamReference(mRng1d, params, "rng1d");
			addParamReference(mRng2d, params, "rng2d");

			addParamReference(mSampler1D, params, "sampler1d");
			addParamReference(mSampler2D, params, "sampler2d");
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const final
		{
			ParameterTree params;

			if(mRng1d)
				params["rng1d"].fromEntityRef(*mRng1d, deep);
			if(mRng2d)
				params["rng2d"].fromEntityRef(*mRng2d, deep);
			if(mSampler1D)
				params["sampler1d"].fromEntityRef(*mSampler1D, deep);
			if(mSampler2D)
				params["sampler2d"].fromEntityRef(*mSampler2D, deep);

			return params;
		}*/

		virtual RandomInt generate() noexcept final
		{
			/*if(mMutator)
				return mMutator->get1d();*/

			return mRng();

			/*Real v;
			requestSamples1D(1, &v);
			recordSample(v);
			return v;*/
		}

		virtual void generate(std::span<RandomInt> values) noexcept final
		{
			/*if(mMutator)
			{
				for(auto &s : values)
					s = mMutator->get1d();
			}
			else
			{*/
				mRng(values);
			//}
		}

		/*void movePath(if32 i) { pathIndex += i; }
		void setPath(uf32 i) { pathIndex = i; }
		void nextPath() { ++pathIndex; }
		void prevPath() { --pathIndex; }
		void pathSplit()
		{
			nextPath();
			mRecordedsp1d[pathIndex].clear();
			mRecordedsp2d[pathIndex].clear();
			std::copy(mRecordedsp1d[pathIndex - 1].begin(), mRecordedsp1d[pathIndex - 1].end(), std::inserter(mRecordedsp1d[pathIndex], mRecordedsp1d[pathIndex].begin()));
			std::copy(mRecordedsp2d[pathIndex - 1].begin(), mRecordedsp2d[pathIndex - 1].end(), std::inserter(mRecordedsp2d[pathIndex], mRecordedsp2d[pathIndex].begin()));
		}
		uf32 getPathIndex() const { return pathIndex; }*/

		void recordSample(RandomInt v)
		{
			if(!mMutator)
				mRecordedsp[mRecordStreamIndex].push_back(v);
		}

		void clearRecordedSamples() noexcept
		{
			clearMainRecordedStream();

			mRecordStreamIndex = 0;
		}

		uf32 pathSplit(uf32 streamIndex = 0) noexcept
		{
			auto state = mRecordedsp[0].size();

			if(streamIndex > 0)
				appendStream(streamIndex);

			return state;
		}

		void clearMainRecordedStream() noexcept
		{
			mRecordedsp[0].clear();
		}

		void appendStream(uf32 streamIndex) noexcept
		{
			if(!mMutator)
			{
				mRecordedsp[0].insert(mRecordedsp[0].cend(), mRecordedsp[streamIndex].begin(), mRecordedsp[streamIndex].end());
			}
		}

		void pathSplitEnd(const uf32 state)
		{
			mRecordedsp[0].resize(state);
		}

		void beginRecordedStream()
		{
			if(++mRecordStreamIndex >= mRecordedsp.size())
				mRecordedsp.resize(mRecordStreamIndex + 1);

			mRecordedsp[mRecordStreamIndex].clear();
		}

		void endRecordedStream() noexcept
		{
			--mRecordStreamIndex;
		}

		[[nodiscard]] uf32 getRecordedStreamIndex() const noexcept { return mRecordStreamIndex; }
		void setRecordedStreamIndex(uf32 index) noexcept { mRecordStreamIndex = index; }

		virtual void seed() override;
		virtual void setPosition(u64 start, const Point2Di &p) final;
		virtual void setBounds(const Point2Di &r) override;

		[[nodiscard]] virtual std::any param() const final { return mRng.param(); }
		virtual void param(const std::any &param) final { mRng.param(param); }

		void setSampleBufferSize(u32 nb, u32 nbPaths);

		template <typename Distribution_>
		void requestSamples(std::span<typename std::decay_t<Distribution_>::result_type> samples, Distribution_&& distrib, bool /*record*/ = false) noexcept
		{
			const auto rngSize = mRng.max() - mRng.min();
			const auto strataSize = rngSize / (RandomInt)samples.size();

			mir(samples.size())
				| pl::scaled_view(strataSize)
				| pl::add_view(mRng.min())
				| rgv::transform([&](auto start)
				{
					std::uniform_int_distribution<RandomInt> localDist{start, start + (strataSize - 1)};
					RNGRemapped remapped{*this, localDist};

					return distrib(remapped);
				})
				| pl::copy(samples.begin());
		}

		[[nodiscard]] bool isMutatedPath() const noexcept { return mMutator != nullptr; }

		[[nodiscard]] auto beginMutation(IMutator &m) noexcept
		{
			mMutator = &m;

			return Mutation{*this};
		}

		[[nodiscard]] auto beginMutationPath()
		{
			mBksp = mRecordedsp[0];
			mMutator->newPath(mRecordedsp[0], mRng);

			return MutationPath{*this};
		}

		[[nodiscard]] auto beginMutationIteration()
		{
			mMutator->newIteration();

			return MutationIteration{*this};
		}

		void endMutationIteration() noexcept
		{
		}

		void endMutationPath() noexcept
		{
			mRecordedsp[0] = mBksp;
		}

		void endMutation() noexcept
		{
			mMutator = nullptr;
		}

	private:
		uf32 mRecordStreamIndex = 0;
		IMutator *mMutator = nullptr;
		std::vector<RandomInt> mBksp;

		struct Mutation : pl::no_copy
		{
			Mutation(Sampler &rngEngine_) noexcept : sampler(rngEngine_) {}
			Mutation(Mutation &&o) noexcept : sampler(o.sampler) {}
			~Mutation() { sampler.endMutation(); }

		private:
			Sampler &sampler;
		};

		struct MutationPath : pl::no_copy
		{
			MutationPath(Sampler &rngEngine_) noexcept : sampler(rngEngine_) {}
			MutationPath(MutationPath &&o) noexcept : sampler(o.sampler) {}
			~MutationPath() { sampler.endMutationPath(); }

		private:
			Sampler &sampler;
		};

		struct MutationIteration : pl::no_copy
		{
			MutationIteration(Sampler &rngEngine_) noexcept : sampler(rngEngine_) {}
			MutationIteration(MutationIteration &&o) noexcept : sampler(o.sampler) {}
			~MutationIteration() { sampler.endMutationIteration(); }

		private:
			Sampler &sampler;
		};
	};


	class MISHeuristicAverage : public IMISHeuristic
	{
	public:

		[[nodiscard]] virtual Real operator() (Real) const noexcept override { return 1; }
	};

	class MISHeuristicBalance : public IMISHeuristic
	{
	public:

		[[nodiscard]] virtual Real operator() (Real pdf) const noexcept override { return pdf; }
	};

	class MISHeuristicPower : public IMISHeuristic
	{
	public:

		[[nodiscard]] virtual Real operator() (Real pdf) const noexcept override { return pdf /opow2; } // pdf > constants::epsilon<Real> ? pdf /opow2 : 0;
	};

	class MISHeuristicCutoff : public IMISHeuristic
	{
	public:
		Real mCutoff = 0;

		[[nodiscard]] virtual Real operator() (Real pdf) const noexcept override { return pdf <= mCutoff ? 0 : pdf; }
	};


	class IMISHeuristicSet
	{
	public:
		virtual ~IMISHeuristicSet() = default;

		virtual void operator() (std::span<Real> weights) const noexcept = 0;
	};

	class MISHeuristicSet : public IMISHeuristicSet
	{
	public:
		EntityPtr<const IMISHeuristic> mMis;

		virtual void operator() (std::span<Real> weights) const noexcept override
		{
			Real total = 0;

			for(auto &w : weights)
				total += (w = (*mMis)(w));

			total = total ? total /oreciprocal : 0;

			for(auto &w : weights)
				w *= total;
		}

	};

	class MISHeuristicSetMaximum : public IMISHeuristicSet
	{
	public:

		virtual void operator() (std::span<Real> weights) const noexcept override
		{
			if(weights.empty())
				return;

			Real maxw = weights[0];
			uf32 maxIndex = 0;

			for(auto i : mir<uf32>(1, weights.size()))
			{
				if(weights[i] > maxw)
				{
					maxw = weights[i];
					maxIndex = i;
				}
			}

			for(const auto i : mir<uf32>(weights.size()))
				weights[i] = i == maxIndex ? 1 : 0;
		}

	};

	template <pl::CNumberFloat T_>
	[[nodiscard]] constexpr T_ misBalance(T_ v) noexcept { return v; }

	template <pl::CNumberFloat T_>
	[[nodiscard]] constexpr T_ misPower(T_ v) noexcept { return v * v; }

	template <pl::CNumberFloat T_, std::invocable<T_> FC_ = decltype(misBalance<T_>)>
	[[nodiscard]] constexpr T_ misWeight2(T_ pdf1, T_ pdf2, FC_ &&mis = misBalance<T_>) noexcept
	{
		pdf1 = std::invoke(PL_FWD(mis), pdf1);
		pdf2 = std::invoke(PL_FWD(mis), pdf2);

		const auto total = pdf1 + pdf2;

		return !total ? 0 : pl::plmin<T_>(1, pdf1 * (total /oprtreciprocal));
	}

	template <pl::CNumberFloat T_, std::invocable<T_> FC_ = decltype(misBalance<T_>)>
	[[nodiscard]] constexpr T_ misWeight2Cutoff(T_ pdf1, T_ pdf2, T_ cutoff, FC_ &&mis = misBalance<T_>) noexcept
	{
		if(pdf1 <= cutoff && pdf2 <= cutoff)
			return 0;

		pdf1 = std::invoke(PL_FWD(mis), pdf1);
		pdf2 = std::invoke(PL_FWD(mis), pdf2);

		const auto total = pdf1 + pdf2;

		return !total ? 0 : pl::plmin<T_>(1, pdf1 * (total /oprtreciprocal));
	}

	template <pl::CNumberFloat T_>
	[[nodiscard]] constexpr T_ updateMisWeight(T_ currentMis, T_ pdfF, T_ pdfB, bool delta) noexcept
	{
		return
			currentMis == 0 ?
				(delta ? 0 : pdfB * (pdfF /oprtreciprocal)) :
				pdfB * (currentMis + !delta) * (pdfF /oprtreciprocal);
	}

	template <pl::CNumberFloat T_>
	struct MisWeight
	{
		T_ value;

		auto &update(T_ pdfF, T_ pdfB, bool delta) noexcept
			{ value = updateMisWeight(value, pdfF, pdfB, delta); return *this; }
	};
}
