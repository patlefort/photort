/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Primitive_fwd.hpp"

#include "Base.hpp"
#include "SurfaceData.hpp"

#include <patlib/box.hpp>
#include <patlib/marked_optional.hpp>

namespace PhotoRT
{
	template <pl::simd::CNeutralOrPointGeometricVector VT_>
	struct SampledAreaResult
	{
		VT_ pos, uv;
		pl::simd::rebind_to_dir<VT_> normal, shadingNormal;
		typename VT_::value_type pdf = 0;

		constexpr void reset() noexcept { pdf = 0; }
		[[nodiscard]] constexpr bool has_value() const noexcept { return pdf != 0; }
	};

	template <pl::simd::CNeutralOrPointGeometricVector VT_>
	struct SampledAreaResultBackface : public SampledAreaResult<VT_>
	{
		bool backface;
	};

	namespace ImplPrimitives
	{
		// Work around problems with concepts and optional.
		constexpr auto conceptTestPDF(const auto &p, auto&&... args) noexcept
			{ return p.PDF(PL_FWD(args)...); }

		template <typename T_>
		concept CBasicPrimitive =
			std::semiregular<T_> &&
			std::constructible_from<T_, std::initializer_list<pl::zero_init_t>> &&

			requires
			{
				typename T_::point_type;
				typename T_::dir_type;
				typename T_::RealType;
				{ auto(T_::AlwaysFlat) } -> std::same_as<bool>;
				{ auto(T_::TypeID) } -> std::same_as<uf32>;
				{ auto(T_::IntersectionCost) } -> std::same_as<typename T_::RealType>;
			} &&

			pl::simd::CNeutralOrPointGeometricVector<typename T_::point_type> &&
			pl::simd::CNeutralOrDirGeometricVector<typename T_::dir_type> &&
			pl::simd::CSameNbAxis<typename T_::point_type, typename T_::dir_type> &&
			pl::CArithmetic<typename T_::RealType>;

		template <typename T_>
		concept CPrimitive =
			CBasicPrimitive<T_> &&

			requires(T_ p, const T_ cp, const typename T_::point_type v1, const typename T_::dir_type d1,
				const typename T_::dir_type d2, const pl::box<typename T_::point_type> b1,
				const typename T_::RealType r1, const bool bool1, std::ostream &os, std::istream &is)
			{
				{ cp.getSurfaceArea() } noexcept -> std::convertible_to<typename T_::RealType>;
				{ cp.getSurfaceNormal(v1) } noexcept -> std::convertible_to<typename T_::dir_type>;
				{ cp.getSurfaceDir(v1) } noexcept -> std::convertible_to<typename T_::dir_type>;

				{ cp.getAABox() } noexcept -> std::same_as<pl::box<typename T_::point_type>>;
				{ cp.getAABoxClipped(b1) } noexcept -> std::same_as<pl::marked_optional<pl::box<typename T_::point_type>>>;

				{ cp.clipBoxToPlane(b1, r1, u32{}, false) } noexcept -> std::same_as<pl::box<typename T_::point_type>>;

				{ cp.getMiddle() } noexcept -> std::convertible_to<typename T_::point_type>;
				{ cp.getPointOnSurface() } noexcept -> std::convertible_to<typename T_::point_type>;

				{ cp.getMinimalSafeIncrement() } noexcept -> std::convertible_to<typename T_::RealType>;

				{ cp.intersect(v1, d1, d2) } noexcept -> std::same_as<bool>;
				{ cp.intersectExt(v1, d1, d2) } noexcept -> std::same_as<pl::marked_optional<Collision<typename T_::point_type, typename T_::RealType>>>;
				{ cp.intersectExt(v1, d1, d2, r1) } noexcept -> std::same_as<pl::marked_optional<Collision<typename T_::point_type, typename T_::RealType>>>;
				{ cp.intersectExt(v1, d1, d2, r1, bool1) } noexcept -> std::same_as<pl::marked_optional<Collision<typename T_::point_type, typename T_::RealType>>>;
				{ cp.intersect(b1) } noexcept -> std::same_as<bool>;
			};

		template <typename T_, typename MatrixT_>
		concept CPrimitiveTransformable =
			CPrimitive<T_> &&
			pl::CMatrix4x4TransformPointAndDir<MatrixT_, typename T_::point_type, typename T_::dir_type> &&
			requires(T_ p, const T_ cp, const typename T_::point_type v1, const MatrixT_ matrix)
			{
				{ cp.getTangentSpace(v1, type_v<MatrixT_>) } noexcept -> std::same_as<MatrixT_>;
				{ p *= matrix } noexcept -> std::same_as<T_&>;
			};

		template <typename T_>
		concept CFinitePrimitive = CPrimitive<T_> &&
			requires(T_ p, const T_ cp, const typename T_::point_type v1, const typename T_::dir_type d1, const typename T_::dir_type d2,
				const RandomReals<2> rnd, const SampledAreaResult<typename T_::point_type> areaSp)
			{
				{ cp.PDF() } noexcept -> std::convertible_to<OptionalPDF<typename T_::RealType>>;
				{ cp.PDF(v1, d1, d2) } noexcept -> std::convertible_to<OptionalPDF<typename T_::RealType>>;
				{ conceptTestPDF(p, v1, d1, d2, areaSp) } noexcept -> std::convertible_to<OptionalPDF<typename T_::RealType>>;

				{ cp.sampleVisibleSurfaceArea(rnd, v1) } noexcept -> std::same_as<pl::marked_optional<SampledAreaResultBackface<typename T_::point_type>>>;
				{ cp.sampleArea(rnd) } noexcept -> std::same_as<pl::marked_optional<SampledAreaResult<typename T_::point_type>>>;
			};
	} // namespace ImplPrimitives

	template <typename T_>
	concept CBasicPrimitive = ImplPrimitives::CBasicPrimitive<std::decay_t<T_>>;
	template <typename T_>
	concept CPrimitive = ImplPrimitives::CPrimitive<std::decay_t<T_>>;
	template <typename T_, typename MatrixT_>
	concept CPrimitiveTransformable = ImplPrimitives::CPrimitiveTransformable<std::decay_t<T_>, std::decay_t<MatrixT_>>;
	template <typename T_>
	concept CFinitePrimitive = ImplPrimitives::CFinitePrimitive<std::decay_t<T_>>;


	template <typename T_, typename Primitive_>
	struct RebindPrimitive
	{
		static_assert(pl::dependent_false<T_>, "Invalid primitive type.");
	};

	template <typename T_, pl::simd::CNeutralOrPointGeometricVector VT_, template<typename> typename Primitive_>
	struct RebindPrimitive<T_, Primitive_<VT_>>
	{
		using value_type = Primitive_<pl::simd::rebind_best_vector<T_, VT_>>;
	};

	template <typename T_, pl::simd::CNeutralOrPointGeometricVector VT_, template<typename, typename> typename Primitive_>
	struct RebindPrimitive<T_, Primitive_<void, VT_>>
	{
		using value_type = Primitive_<void, pl::simd::rebind_best_vector<T_, VT_>>;
	};

	template <typename T_, CBasicPrimitive Primitive_>
	using RebindPrimitive_t = typename RebindPrimitive<T_, Primitive_>::value_type;
}
