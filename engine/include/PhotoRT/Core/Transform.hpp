/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Transform_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/ordered_index.hpp>

#include <patlib/clone.hpp>

namespace PhotoRT
{
	class ITransform : public virtual pl::ICloneableNewOrCopy<ITransform>
	{
	public:

		ITransform() = default;
		virtual ~ITransform() = default;

		[[nodiscard]] virtual Matrix getMatrix() const noexcept = 0;
		[[nodiscard]] virtual Matrix inverse() const = 0;

		ITransform(const ITransform &) = default;
		ITransform(ITransform &&) = default;

		virtual ITransform &operator=(const ITransform &) { return *this; }
		virtual ITransform &operator=(ITransform &&) noexcept { return *this; }

		[[nodiscard]] Matrix operator* (const ITransform &t) const noexcept
			{ return getMatrix() * t(); }

		template <typename T_>
		[[nodiscard]] Matrix operator* (const T_ &v) const noexcept
			{ return getMatrix() * v; }

		[[nodiscard]] Matrix operator() () const noexcept
			{ return getMatrix(); }

		[[nodiscard]] operator Matrix() const noexcept
			{ return getMatrix(); }

	};

	class TransformIdentity : public ITransform, public pl::CloneableNewOrCopy<ITransform, TransformIdentity>
	{
	public:
		[[nodiscard]] virtual Matrix getMatrix() const noexcept final { return pl::tag::identity; }
		[[nodiscard]] virtual Matrix inverse() const final { return pl::tag::identity; }
	};


	class PHOTORT_API TransformMulti : public ITransform, public pl::CloneableNewOrCopy<ITransform, TransformMulti>
	{
	public:

		struct ByOrder {};
		struct ByName {};

		struct TransformItem
		{
			int order;
			std::string name;
			EntityPtr<ITransform> transform;

			template <typename TransformType_>
			[[nodiscard]] EntityPtr<TransformType_> getTransformSptr() const
			{
				auto t = DynEntityCast<TransformType_>(transform);

				if(!t)
					throw Exception("ITransform requested with an invalid type.");

				return t;
			}

			template <typename TransformType_>
			[[nodiscard]] const TransformType_ &getTransform() const
			{
				auto t = dynamic_cast<const TransformType_ *>(transform.get());

				if(!t)
					throw Exception("ITransform requested with an invalid type.");

				return *t;
			}

			template <typename TransformType_>
			[[nodiscard]] TransformType_ &getTransform()
			{
				auto t = dynamic_cast<TransformType_ *>(transform.get());

				if(!t)
					throw Exception("ITransform requested with an invalid type.");

				return *t;
			}
		};

		using TransformListType = boost::multi_index::multi_index_container<
			TransformItem,
			boost::multi_index::indexed_by<
				boost::multi_index::ordered_unique<boost::multi_index::tag<ByOrder>, boost::multi_index::member<TransformItem, int, &TransformItem::order>>,
				boost::multi_index::ordered_unique<boost::multi_index::tag<ByName>, boost::multi_index::member<TransformItem, std::string, &TransformItem::name>>
			>
		>;

		TransformListType mTransforms;

		TransformMulti() = default;
		PL_DEFAULT_COPYMOVE(TransformMulti)
		PL_VIRTUAL_COPYMOVE(ITransform, TransformMulti)

		[[nodiscard]] virtual Matrix getMatrix() const noexcept final;
		[[nodiscard]] virtual Matrix inverse() const final;

		template <typename TransformType_>
		[[nodiscard]] const TransformType_ *getTransform(const std::string &name) const noexcept
		{
			auto &list = mTransforms.get<ByName>();

			if(auto t = list.find(name); t != list.end())
				return &t->getTransform<TransformType_>();

			return nullptr;
		}

		template <typename TransformType_>
		[[nodiscard]] TransformType_ *getTransform(const std::string &name) noexcept
		{
			auto &list = mTransforms.get<ByName>();

			if(auto t = list.find(name); t != list.end())
			{
				TransformType_ *res{};
				list.modify(t, [&](TransformItem &v){ res = &v.getTransform<TransformType_>(); });
				return res;
			}

			return nullptr;
		}

		template <typename TransformType_>
		[[nodiscard]] const TransformType_ *getTransform(int order) const noexcept
		{
			auto &list = mTransforms.get<ByOrder>();

			if(auto t = list.find(order); t != list.end())
				return &t->getTransform<TransformType_>();

			return nullptr;
		}

		template <typename TransformType_>
		[[nodiscard]] TransformType_ *getTransform(int order) noexcept
		{
			auto &list = mTransforms.get<ByOrder>();

			if(auto t = list.find(order); t != list.end())
				return &t->getTransform<TransformType_>();

			return nullptr;
		}

		template <typename TransformType_>
		[[nodiscard]] EntityPtr<TransformType_> getTransformSptr(int order) const
		{
			auto &list = mTransforms.get<ByOrder>();

			if(auto t = list.find(order); t != list.end())
				return t->getTransformSptr<TransformType_>();

			return nullptr;
		}

		template <typename TransformType_>
		[[nodiscard]] EntityPtr<TransformType_> getTransformSptr(const std::string &name) const
		{
			auto &list = mTransforms.get<ByName>();

			if(auto t = list.find(name); t != list.end())
				return t->getTransformSptr<TransformType_>();

			return nullptr;
		}

	};


	class PHOTORT_API TransformMatrix : public ITransform, public pl::CloneableNewOrCopy<ITransform, TransformMatrix>
	{
	public:

		TransformMatrix() = default;
		TransformMatrix(const Matrix &m) noexcept : matrix{m} {}

		PL_DEFAULT_COPYMOVE(TransformMatrix)
		PL_VIRTUAL_COPYMOVE(ITransform, TransformMatrix)

		[[nodiscard]] virtual Matrix getMatrix() const noexcept final { return matrix; }
		[[nodiscard]] virtual Matrix inverse() const final { return pl::try_inverse(matrix, "Failed to inverse transformation matrix."); }

		void setMatrix(const Matrix &m) noexcept { matrix = m; }

	protected:

		Matrix matrix{pl::tag::identity};

	};


	class PHOTORT_API TransformRotation : public ITransform, public pl::CloneableNewOrCopy<ITransform, TransformRotation>
	{
	public:

		TransformRotation() = default;
		TransformRotation(Point3Dr a) noexcept : mAngle{a} {}

		PL_DEFAULT_COPYMOVE(TransformRotation)
		PL_VIRTUAL_COPYMOVE(ITransform, TransformRotation)

		[[nodiscard]] virtual Matrix getMatrix() const noexcept final;
		[[nodiscard]] virtual Matrix inverse() const final;

		void setAngleX(Real ax) noexcept;
		void setAngleY(Real ay) noexcept;
		void setAngleZ(Real az) noexcept;
		void setAngle(Real ax, Real ay, Real az) noexcept;
		void setAngle(Point3Dr a) noexcept;

		void rotate(Dir3Dr rot) noexcept;
		void rotate(Real ax, Real ay, Real az) noexcept;
		void rotatex(Real ax) noexcept;
		void rotatey(Real ay) noexcept;
		void rotatez(Real az) noexcept;

		[[nodiscard]] Point3Dr getAngle() const noexcept { return mAngle; }

	protected:

		Point3Dr mAngle{};

	};


	class PHOTORT_API TransformTranslation : public ITransform, public pl::CloneableNewOrCopy<ITransform, TransformTranslation>
	{
	public:

		TransformTranslation() = default;
		TransformTranslation(Point3Dr p) noexcept : mPosition{p} {}

		PL_DEFAULT_COPYMOVE(TransformTranslation)
		PL_VIRTUAL_COPYMOVE(ITransform, TransformTranslation)

		[[nodiscard]] virtual Matrix getMatrix() const noexcept final;
		[[nodiscard]] virtual Matrix inverse() const final;

		void setPositionX(Real px) noexcept;
		void setPositionY(Real py) noexcept;
		void setPositionZ(Real pz) noexcept;
		void setPosition(Real px, Real py, Real pz) noexcept;
		void setPosition(Point3Dr p) noexcept;

		void moveX(Real px) noexcept;
		void moveY(Real py) noexcept;
		void moveZ(Real pz) noexcept;
		void move(Real px, Real py, Real pz) noexcept;
		void move(Dir3Dr p) noexcept;

		[[nodiscard]] Point3Dr getPosition() const noexcept { return mPosition; }

	protected:

		Point3Dr mPosition{};

	};


	class PHOTORT_API TransformScale : public ITransform, public pl::CloneableNewOrCopy<ITransform, TransformScale>
	{
	public:

		TransformScale() = default;
		TransformScale(Point3Dr s) noexcept : mScale{s} {}

		PL_DEFAULT_COPYMOVE(TransformScale)
		PL_VIRTUAL_COPYMOVE(ITransform, TransformScale)

		[[nodiscard]] virtual Matrix getMatrix() const noexcept final;
		[[nodiscard]] virtual Matrix inverse() const final;

		void setScaleX(Real sx) noexcept;
		void setScaleY(Real sy) noexcept;
		void setScaleZ(Real sz) noexcept;
		void setScale(Real sx, Real sy, Real sz) noexcept;
		void setScale(Point3Dr s) noexcept;

		void scaleX(Real sx) noexcept;
		void scaleY(Real sy) noexcept;
		void scaleZ(Real sz) noexcept;
		void scale(Real sx, Real sy, Real sz) noexcept;
		void scale(Dir3Dr s) noexcept;

		[[nodiscard]] Point3Dr getScale() const noexcept {return mScale;}

	protected:

		Point3Dr mScale{pl::tag::one};

	};

	class PHOTORT_API TransformCombined : public ITransform, public pl::CloneableNewOrCopy<ITransform, TransformCombined>
	{
	public:
		TransformCombined() = default;
		TransformCombined(Point3Dr s, Point3Dr r = {}, Point3Dr t = {}, Point3Dr p = {}) noexcept :
			mScale{s}, mRot{r}, mTrans{t}, mPivot{p} {}

		PL_DEFAULT_COPYMOVE(TransformCombined)
		PL_VIRTUAL_COPYMOVE(ITransform, TransformCombined)

		[[nodiscard]] virtual Matrix getMatrix() const noexcept final;
		[[nodiscard]] virtual Matrix inverse() const final;

		void scale(Dir3Dr s) noexcept;
		void setScale(Point3Dr s) noexcept;
		void rotate(Dir3Dr s) noexcept;
		void setAngle(Point3Dr s) noexcept;
		void translate(Dir3Dr s) noexcept;
		void setPosition(Point3Dr s) noexcept;
		void setPivot(Point3Dr s) noexcept;

		[[nodiscard]] Point3Dr getScale() const noexcept { return mScale.getScale(); }
		[[nodiscard]] Point3Dr getAngle() const noexcept { return mRot.getAngle(); }
		[[nodiscard]] Point3Dr getPosition() const noexcept { return mTrans.getPosition(); }
		[[nodiscard]] Point3Dr getPivot() const noexcept { return mPivot.getPosition(); }

	protected:
		TransformScale mScale;
		TransformRotation mRot;
		TransformTranslation mTrans, mPivot;

	};

	class PHOTORT_API TransformPerspective : public ITransform, public pl::CloneableNewOrCopy<ITransform, TransformPerspective>
	{
	public:

		TransformPerspective() = default;
		TransformPerspective(Real fov, Real near, Real far) noexcept : mFov{fov}, mNearp{near}, mFarp{far} {}

		PL_DEFAULT_COPYMOVE(TransformPerspective)
		PL_VIRTUAL_COPYMOVE(ITransform, TransformPerspective)

		[[nodiscard]] virtual Matrix getMatrix() const noexcept final;
		[[nodiscard]] virtual Matrix inverse() const final;

		[[nodiscard]] Real getFoV() const noexcept { return mFov; }
		void setFoV(Real f) noexcept { mFov = f; }

		[[nodiscard]] Real getNear() const noexcept { return mNearp; }
		void setNear(Real v) noexcept { mNearp = v; }

		[[nodiscard]] Real getFar() const noexcept { return mFarp; }
		void setFar(Real v) noexcept { mFarp = v; }

	protected:

		Real mFov = 0.7, mNearp = 0.01, mFarp = 1000;

	};

	class PHOTORT_API TransformOrthographic : public ITransform, public pl::CloneableNewOrCopy<ITransform, TransformOrthographic>
	{
	public:

		TransformOrthographic() = default;
		TransformOrthographic(Real fov, Real near, Real far) noexcept : mFov{fov}, mNearp{near}, mFarp{far} {}

		PL_DEFAULT_COPYMOVE(TransformOrthographic)
		PL_VIRTUAL_COPYMOVE(ITransform, TransformOrthographic)

		[[nodiscard]] virtual Matrix getMatrix() const noexcept final;
		[[nodiscard]] virtual Matrix inverse() const final;

		[[nodiscard]] Real getFoV() const noexcept { return mFov; }
		void setFoV(Real f) noexcept { mFov = f; }

		[[nodiscard]] Real getNear() const noexcept { return mNearp; }
		void setNear(Real v) noexcept { mNearp = v; }

		[[nodiscard]] Real getFar() const noexcept { return mFarp; }
		void setFar(Real v) noexcept { mFarp = v; }

	protected:

		Real mFov = 1, mNearp = 0.01, mFarp = 1000;

	};


	PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("transform-identity", AugmentedTransformIdentity, TransformIdentity)
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("transform-multi", AugmentedTransformMulti, TransformMulti)
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("transform-matrix", AugmentedTransformMatrix, TransformMatrix)
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("transform-rotation", AugmentedTransformRotation, TransformRotation)
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("transform-translation", AugmentedTransformTranslation, TransformTranslation)
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("transform-scale", AugmentedTransformScale, TransformScale)
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("transform-combined", AugmentedTransformCombined, TransformCombined)
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("transform-perspective", AugmentedTransformPerspective, TransformPerspective)
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("transform-orthographic", AugmentedTransformOrthographic, TransformOrthographic)
}
