/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "RayTracer_fwd.hpp"
#include "Normals_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"
#include "Texture.hpp"

namespace PhotoRT
{
	class NormalMapBase : public INormalMapper
	{
	public:

	};


	class PHOTORT_API BitmapToBumpmap2D : public BitmapReadChainBase2D
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("bitmapread-bump2d", BitmapToBumpmap2D)

		[[nodiscard]] virtual Color getPixel(if32 u, if32 v) const noexcept override;

		[[nodiscard]] virtual Color getPixel(szt offset) const noexcept override
			{ return getPixel(offset % mBitmap->getWidth(), offset / mBitmap->getWidth()); }

		[[nodiscard]] virtual Color operator() (Real u, Real v) const noexcept override
			{ return BitmapAlgo::getPixelLinearFilter(*this, u, v); }

		void setTiled(bool tiled) noexcept { mTiled = tiled; }
		[[nodiscard]] bool isTiled() const noexcept { return mTiled; }

		virtual void setParameters(const ParameterTree &params) override
		{
			BitmapReadChainBase2D::setParameters(params);

			params.getValueIfExistsBind("tiled", PL_LAMBDA_FORWARD_THIS(setTiled));
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {BitmapReadChainBase2D::getParameters(deep)};

			if(mBitmap)
				params["bitmap"].fromEntityRef<IBitmap2DRead>(*mBitmap, deep);

			params["tiled"] = mTiled;

			return params;
		}

	protected:
		bool mTiled = true;
	};

	class PHOTORT_API NormalMapBump2D : public SurfaceMapperDist<VecD, IColorDistribution2D>
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("surfacemap_normal-bump2d", NormalMapBump2D)

		[[nodiscard]] Real getStrength() const noexcept { return mStrength; }
		void setStrength(Real s) noexcept { mStrength = s; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

		[[nodiscard]] virtual VecD map(const MaterialInteraction &inter, const VecD odir, type_c<VecD> = {}) const noexcept override;

	protected:
		Real mStrength = 1;

	};

	class PHOTORT_API NormalMapBump3D : public SurfaceMapperDist<VecD, IColorDistribution3D>
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("surfacemap_normal-bump3d", NormalMapBump3D)

		[[nodiscard]] Real getStrength() const noexcept { return mStrength; }
		void setStrength(Real s) noexcept { mStrength = s; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

		[[nodiscard]] virtual VecD map(const MaterialInteraction &inter, const VecD odir, type_c<VecD> = {}) const noexcept override;

	protected:
		Real mStrength = 1;

	};

	class PHOTORT_API NormalMapMulti : public SurfaceMapperMultiBase<VecD>
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("surfacemap_normal-multi", NormalMapMulti)

		using SurfaceMapperMultiBase<VecD>::SurfaceMapperMultiBase;

		[[nodiscard]] virtual VecD map(const MaterialInteraction &inter, const VecD odir, type_c<VecD> = {}) const noexcept override;
	};
}
