/*
	PhotoRT

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "RenderEntityCollection_fwd.hpp"

#include "Base.hpp"
#include "RenderEntityFactory.hpp"
#include "RenderEntityBase.hpp"
#include "Parameter.hpp"

#include <boost/bimap/bimap.hpp>
#include <boost/bimap/set_of.hpp>

#include <filesystem>
#include <set>

namespace PhotoRT
{
	using RenderEntityMap = boost::bimaps::bimap<
		boost::bimaps::set_of<std::string, std::less<>>,
		boost::bimaps::set_of<EntityPtrBase, std::less<>>
	>;

	class IDataSource
	{
	public:
		virtual ~IDataSource() = default;

		virtual void serialize(const IRenderEntity &entity) = 0;
		virtual void unserialize(IRenderEntity &entity) = 0;
	};

	class PHOTORT_API DataSourceFile : public IDataSource
	{
	public:

		DataSourceFile() : mBuffer(PRT_IO_BUFFER_SIZE) {}

		virtual void setFilePath(const std::filesystem::path &p) { mPath = p; }
		[[nodiscard]] const std::filesystem::path &getFilePath() const noexcept { return mPath; }

		virtual void serialize(const IRenderEntity &entity) override;
		virtual void unserialize(IRenderEntity &entity) override;

	protected:
		std::filesystem::path mPath;
		std::vector<char> mBuffer;
	};

	class PHOTORT_API DataSourceStream : public IDataSource
	{
	public:

		void setIStream(std::istream &s) noexcept { is = &s; }
		[[nodiscard]] std::istream *getIStream() const noexcept { return is; }

		void setOStream(std::ostream &s) noexcept { os = &s; }
		[[nodiscard]] std::ostream *getOStream() const noexcept { return os; }

		void setCompressor(std::string s) noexcept { mCompressorType = std::move(s); }
		[[nodiscard]] const std::string &getCompressor() const noexcept { return mCompressorType; }

		virtual void serialize(const IRenderEntity &entity) override;
		virtual void unserialize(IRenderEntity &entity) override;

	protected:
		std::istream *is = nullptr;
		std::ostream *os = nullptr;
		std::string mCompressorType;
	};


	// Manages a list of RenderEntity objects. Not thread safe.
	class PHOTORT_API EntityCollection
	{
	public:

		// Entities
		RenderEntityMap mEntities;
		RenderEntityRelationMap mEntityRelations;

		std::shared_ptr<IDataSource> mDataSource;
		std::unique_ptr<IEntityFactoriesProxy> mFactories;
		std::shared_ptr<IParameterParser> mParser;

		ParameterTree mGlobalParamList;

		EntityCollection();
		virtual ~EntityCollection();

		[[nodiscard]] IRenderEntity& operator [] (std::string_view index)
			{ return *mEntities.left.at(mIdPrefix + index); }

		[[nodiscard]] const IRenderEntity& operator [] (std::string_view index) const
			{ return *mEntities.left.at(mIdPrefix + index); }

		EntityPtrBase createEntity(std::string_view type, std::string id = {}, const ParameterTree &params = {});
		EntityPtrBase createEntity(const Parameter &param);
		EntityPtrBase createEntity(std::string_view params);
		EntityPtrBase createEntity(std::istream &params);
		EntityPtrBase createEntity(std::string_view type, std::string id, std::istream &params);
		EntityPtrBase createEntity(std::string_view type, std::string id, std::string_view params);

		EntityPtrBase createEntity(IRenderEntityFactory &factory, std::string id = {}, const ParameterTree &params = {});
		EntityPtrBase createEntity(IRenderEntityFactory &factory, std::string id, std::string_view params);
		EntityPtrBase createEntity(IRenderEntityFactory &factory, std::string id, std::istream &params);

		template <typename EntityType_, typename... Args_>
		auto createEntity(type_c<EntityType_>, Args_&&... args)
			{ return DynEntityCast<EntityType_>(createEntity(PL_FWD(args)...)); }

		template <typename EntityType_>
		auto createEntity(type_c<EntityType_>, std::string_view type, std::string id, const ParameterTree &params)
			{ return DynEntityCast<EntityType_>(createEntity(type, std::move(id), params)); }

		template <typename EntityType_>
		EntityPtr<EntityType_> createEntityIfParamExists(type_c<EntityType_> type, const ParameterTree &params, std::string_view paramName)
		{
			if(const auto p = params.findParam(paramName); p && !p->value.empty())
				return createEntity(type, *p);
			else
				return nullptr;
		}

		[[nodiscard]] bool hasEntity(std::string_view id) const
			{ return mEntities.left.find(mIdPrefix + id) != mEntities.left.end(); }

		template <typename EntityType_ = IRenderEntity>
		[[nodiscard]] auto getEntitySptr(std::string_view id, type_c<EntityType_> = {})
		{
			auto entity = DynEntityCast<EntityType_>(mEntities.left.at(mIdPrefix + id));

			if(!entity)
				throw ExceptionEntityCollection(std::string("Entity \"") + mIdPrefix + id + "\" requested with an invalid type.");

			return entity;
		}

		template <typename EntityType_ = IRenderEntity>
		[[nodiscard]] auto *getEntity(std::string_view id, type_c<EntityType_> = {}) const
		{
			const auto *entity = dynamic_cast<const EntityType_ *>( std::to_address(mEntities.left.at(mIdPrefix + id)) );

			if(!entity)
				throw ExceptionEntityCollection(std::string("Entity \"") + id + "\" requested with an invalid type.");

			return entity;
		}

		template <typename EntityType_ = IRenderEntity>
		[[nodiscard]] EntityPtr<EntityType_> findEntitySptr(std::string_view id, type_c<EntityType_> = {})
		{
			auto it = mEntities.left.find(mIdPrefix + id);
			if(it == mEntities.left.end())
				return nullptr;

			return DynEntityCast<EntityType_>(it->second);
		}

		[[nodiscard]] std::vector<EntityPtrBase> getEntitiesByType(std::string_view type);

		virtual void clearEntities();

		void addEntityReference(const IRenderEntity &entity, const EntityPtrBase &entityRel);
		void releaseEntityReference(const IRenderEntity &entity, const EntityPtrBase &entityRel);
		void releaseAllEntityReferences(const IRenderEntity &entity);
		virtual void removeEntity(const IRenderEntity &entity);
		virtual void insertEntity(std::string id, const EntityPtrBase &entity);

		template <typename EntityType_, typename... Args_>
		auto addEntityReferenceCreate(type_c<EntityType_>, const IRenderEntity &entity, Args_&&... args)
		{
			auto entityRef = createEntity(PL_FWD(args)...);

			if(entityRef)
				addEntityReference(entity, entityRef);

			return DynEntityCast<EntityType_>(entityRef);
		}

		template <typename EntityType_, typename... Args_>
		void addEntityReferenceInto(const IRenderEntity &entity, EntityPtr<EntityType_> &into, Args_&&... args)
		{
			auto newEntity = createEntity(PL_FWD(args)...);

			if(newEntity)
				addEntityReference(entity, newEntity);

			into = DynEntityCast<EntityType_>(newEntity);
		}

		template <typename EntityType_>
		void removeEntity(const EntityType_ &entity)
			{ removeEntity(dynamic_cast<const IRenderEntity &>(entity)); }

		virtual EntityPtrBase importEntity(std::istream &inputStream, std::string_view genericType, std::string_view mimeType, std::string id, const ParameterTree &params = {}, std::string_view expectedType = {});
		virtual EntityPtrBase importEntity(std::span<const std::byte> data, std::string_view genericType, std::string_view mimeType, std::string id, const ParameterTree &params = {}, std::string_view expectedType = {});
		virtual EntityPtrBase importEntity(const std::filesystem::path &filename, std::string_view genericType, std::string_view mimeType, std::string id, const ParameterTree &params = {}, std::string_view expectedType = {});

		template <typename EntityType_>
		EntityPtr<EntityType_> importEntity(std::istream &inputStream, std::string_view genericType, std::string_view mimeType, std::string id, const ParameterTree &params = {}, std::string_view expectedType = {})
			{ return DynEntityCast<EntityType_>(importEntity(inputStream, genericType, mimeType, std::move(id), params, expectedType)); }

		template <typename EntityType_>
		EntityPtr<EntityType_> importEntity(std::span<const std::byte> data, std::string_view genericType, std::string_view mimeType, std::string id, const ParameterTree &params = {}, std::string_view expectedType = {})
			{ return DynEntityCast<EntityType_>(importEntity(data, genericType, mimeType, std::move(id), params, expectedType)); }

		template <typename EntityType_>
		EntityPtr<EntityType_> importEntity(const std::filesystem::path &filename, std::string_view genericType, std::string_view mimeType, std::string id, const ParameterTree &params = {}, std::string_view expectedType = {})
			{ return DynEntityCast<EntityType_>(importEntity(filename, genericType, mimeType, std::move(id), params, expectedType)); }

		virtual void exportEntity(const IExportHook &entity, std::ostream &outputStream, std::string_view mimeType, const ParameterTree &params = {});
		virtual void exportEntity(const IExportHook &entity, const std::filesystem::path &filename, std::string_view mimeType, const ParameterTree &params = {});
		virtual std::vector<std::byte> exportEntity(const IExportHook &entity, std::string_view mimeType, const ParameterTree &params = {});
		[[nodiscard]] static bool canImport(std::string_view mimeType);
		[[nodiscard]] static bool canExport(std::string_view mimeType);

		virtual void addEntities(const ParameterTree &params);
		virtual void addEntitiesFromFile(const std::filesystem::path &filename, std::string idPrefix);
		virtual void saveEntitiesToFile(const std::filesystem::path &filename);

		virtual void serializeEntities(std::ostream &outputStream);
		virtual void unserializeEntities(std::istream &inputStream, std::string_view idPrefix);

		virtual void setFilePath(const std::filesystem::path &p);
		[[nodiscard]] const auto &getFilePath() const noexcept { return mPath; }

		virtual void setOverwriteExisting(bool v) { mOverwriteExisting = v; }
		[[nodiscard]] virtual bool getOverwriteExisting() const { return mOverwriteExisting; }

		[[nodiscard]] virtual std::string generateID(std::string_view type);

		[[nodiscard]] const auto &getShmID() const noexcept { return mShmID; }
		void setShmID(std::string s) { mShmID = std::move(s); }
		void generateShmID();

	protected:
		uf64 mCounter = 0;
		std::string mIdPrefix, mShmID;
		std::filesystem::path mPath;
		std::set<std::string> mCreatedEntities;
		std::set<std::filesystem::path> mIncludedFiles;
		bool mOverwriteExisting = false;

		EntityPtrBase createNewEntity(std::string_view type, std::string id, const ParameterTree &params);

		virtual void onEntityCreate(const EntityPtrBase &, std::string &/*id*/) {}

		void setupEntity(EntityPtrBase entity, std::string id, const ParameterTree &params);

	};

	struct ScopedOverwrite
	{
		EntityCollection &list;

		ScopedOverwrite(EntityCollection &l)
			: list{l} { list.setOverwriteExisting(true); }

		~ScopedOverwrite()
			{ list.setOverwriteExisting(false); }
	};
}
