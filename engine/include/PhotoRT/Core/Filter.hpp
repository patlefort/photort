/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Filter_fwd.hpp"

#include "Base.hpp"
#include "Distribution.hpp"

#include <patlib/rectangle.hpp>
#include <patlib/array_view.hpp>

namespace PhotoRT
{
	template <typename ColorType_, typename DistType_>
	class Filter2D
	{
	public:
		using ColorType = ColorType_;
		using DistType = DistType_;

		Dir2Di mSize{1};
		if32 mOverSample = 100;

		EntityPtr<const IDistributionMapper2D<Real, DistType_>> mDistribution;

		[[nodiscard]] const auto &operator[] (const szt index) const noexcept
			{ return mPrecalc[index]; }

		[[nodiscard]] const auto &operator[] (const Point2Di p) const noexcept
			{ return mFilterView[{p[1], p[0]}]; }

		[[nodiscard]] auto getSizeRec() const noexcept { return mSizeRec; }

		void initFilter()
		{
			const Dir2Dr halfSize = mSize * 0.5;
			const Real tx = 1 / r_(mOverSample);
			const Real ty = tx;
			const ColorType_ avgFactor = 1 / r_(mOverSample /opow2);

			mSizeRec = static_cast<Dir2Dr>(1.0 / mSize);

			mPrecalc.clear();
			mPrecalc.reserve((mSize /ohmul)[0]);

			if(mDistribution)
			{
				const auto &dist = *mDistribution;
				const auto fr = pl::rectangle<>{pl::size_p{mSize}};
				const auto sr = pl::rectangle<>{pl::size_p{mOverSample}};

				fr | rgv::transform([&](const auto p){
						return avgFactor * rg::accumulate(
								sr,
								ColorType_{},
								rg::plus{},
								[&](const auto sp)
								{
									return pl::plmax<DistType_>(
											dist(
												pl::plabs(r_(p[0]) + sp[0] * tx + 0.5 * tx - halfSize[0]) / halfSize[0],
												pl::plabs(r_(p[1]) + sp[1] * ty + 0.5 * ty - halfSize[1]) / halfSize[1])
											,
											0
										);
								}
							);
					})
					| pl::validated_view
					| pl::back_insert_copy(mPrecalc);
			}
			else
			{
				mPrecalc.resize((mSize /ohmul)[0], 1);
			}

			mPrecalc.shrink_to_fit();

			mFilterView = {mPrecalc, pl::av::bounds<2>{mSize[1], mSize[0]}};

			normalizeFilter();
		}

		[[nodiscard]] auto getTotalWeight() const noexcept { return mTotalWeight; }

	private:

		ColorType_ mTotalWeight{};
		std::vector<ColorType_> mPrecalc;
		pl::av::array_view<ColorType_, 2> mFilterView;
		Dir2Dr mSizeRec{1};

		void normalizeFilter()
		{
			mTotalWeight = rg::accumulate(mPrecalc, ColorType_{});

			if(mTotalWeight > 0)
			{
				for(auto &p : mPrecalc)
					p /= mTotalWeight;
			}
		}
	};

}
