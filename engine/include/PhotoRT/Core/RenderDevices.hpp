/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Scene_fwd.hpp"
#include "Film_fwd.hpp"
#include "RenderDevices_fwd.hpp"

#include "Base.hpp"
#include "Statistics.hpp"
#include "RenderEntity.hpp"

#include <patlib/thread.hpp>
#include <patlib/rectangle.hpp>

namespace PhotoRT
{
	class IRenderDevice : public virtual pl::IThreadedDevice
	{
	public:
		virtual ~IRenderDevice() = default;

		virtual void getStats(Statistics &stats, const std::locale &loc) const = 0;
		virtual void setRenderContext(const EntityPtr<RenderContext> &context) = 0;
		virtual void notifyEntityUpdate(IRenderEntity &entity) = 0;
		virtual void init(Scene &scene) = 0;
		[[nodiscard]] virtual bool isReady() const = 0;
		[[nodiscard]] virtual std::string_view getDeviceName() const = 0;

		virtual void dumpInfo(Scene &scene, const std::filesystem::path &folder, const std::string &name) = 0;

	};

	class IRenderDeviceBuffered : public virtual IRenderDevice
	{
	public:
		virtual ~IRenderDeviceBuffered() = default;

		[[nodiscard]] virtual std::vector<EntityPtr<ContributionBuffer>> getContributionBuffers() const = 0;
		virtual void setRenderRect(std::string_view filmID, const pl::rectangle<> &rect) = 0;
		[[nodiscard]] virtual pl::rectangle<> getRenderRect() const = 0;

	};

	class RenderDeviceExecutionLockGuard : pl::no_copy
	{
	private:
		IRenderDevice &device;

	public:
		RenderDeviceExecutionLockGuard(IRenderDevice &d) : device{d} { d.lock_execution(); }
		~RenderDeviceExecutionLockGuard() { device.unlock_execution(); }
	};



	template <typename RenderDeviceClass_> using RenderDevicePool = pl::ThreadPool<EntityPtr<RenderDeviceClass_>>;
	template <typename RenderDeviceClass_>
	class RenderDeviceMulti : public virtual IRenderDevice, public RenderDevicePool<RenderDeviceClass_>
	{
	public:
		RenderDeviceMulti() = default;
		virtual ~RenderDeviceMulti() = default;

		virtual void getStats(Statistics &stats, const std::locale &loc) const override
		{
			for(const auto &t : this->mThreads)
			{
				Statistics tmpStats;
				const auto deviceName = t->getDeviceName() + ": ";

				t->getStats(tmpStats, loc);

				for(auto &s : tmpStats)
					stats[deviceName + s.first] = s.second;
			}
		}

		virtual void setRenderContext(const EntityPtr<RenderContext> &context) override
		{
			for(auto &t : this->mThreads)
				t->setRenderContext(context);
		}

		virtual void notifyEntityUpdate(IRenderEntity &entity) override
		{
			for(auto &t : this->mThreads)
				t->notifyEntityUpdate(entity);
		}

		virtual void init(Scene &scene) override
		{
			for(auto &t : this->mThreads)
				t->init(scene);
		}

		[[nodiscard]] virtual bool isReady() const override
		{
			for(auto &t : this->mThreads)
				if(!t->isReady())
					return false;

			return true;
		}

		[[nodiscard]] virtual std::string_view getDeviceName() const override { return {}; }

		virtual void dumpInfo(Scene &scene, const std::filesystem::path &folder, const std::string &name) override
		{
			for(auto&& [i, t] : rgv::enumerate(this->mThreads))
				t->dumpInfo(scene, folder, name + pl::io::to_string_noloc(i) + '_');
		}

		virtual void setRenderRect(std::string_view filmID, const pl::rectangle<> &rect)
		{
			for(auto &t : this->mThreads)
				t->setRenderRect(filmID, rect);
		}

		virtual boost::signals2::connection listen(const pl::ThreadEvent, std::function<void()>) override { return {}; }
	};


	class PHOTORT_API RenderDeviceController : public RenderDeviceMulti<IRenderDeviceBuffered>
	{
	public:
		RenderDeviceController() = default;
		virtual ~RenderDeviceController() = default;

		EntityPtr<RenderContext> mRenderContext;

		virtual void initTargets();

		virtual void setRenderContext(const EntityPtr<RenderContext> &context) override
		{
			mRenderContext = context;

			RenderDeviceMulti::setRenderContext(context);
		}

		virtual void init(Scene &scene) override;

	};

	class PHOTORT_API ClientDevice : public virtual pl::IThreadedDevice
	{
	protected:
		std::map<std::string, std::function<void(std::string_view command)>, std::less<>> mCommandHandlers;

		virtual bool handleCommand(std::string_view command);
	};
}
