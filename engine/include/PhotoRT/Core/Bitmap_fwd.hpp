/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Sampling_fwd.hpp"
#include "Distribution_fwd.hpp"
#include "Filter_fwd.hpp"

namespace PhotoRT
{
	using IColorDistribution1D = IDistributionMapper<Real, Color>;
	using IColorDistribution2D = IDistributionMapper2D<Real, Color>;
	using IColorDistribution3D = IDistributionMapper3D<Real, Color>;

	template <typename Operator_>
	using ColorDistributionAdaptor = DistributionMapperAdaptor<Real, Color, Operator_>;

	using ColorFilter2D = Filter2D<Color, Real>;
	using IColorFilterDistribution2D = IDistributionMapper2D<Real, Real>;

	class IBitmap1DWrite;
	class IBitmap2DWrite;
	class IBitmap3DWrite;

	class IBitmap1DRead;
	class IBitmap2DRead;
	class IBitmap3DRead;

	template <typename BitmapType_, typename ColorType_>
	struct BitmapOffsetReference;

	struct BitmapIterator;

	template <typename BitmapType_, typename ColorType_>
	class BitmapIterator1D;

	template <typename BitmapType_, typename ColorType_>
	class BitmapIterator2D;

	template <typename BitmapType_, typename ColorType_>
	class BitmapIterator3D;

	class BitmapBase;

	template <typename BitmapType_>
	class BitmapReadChainBase;

	class BitmapReadChainBase1D;
	class BitmapReadChainBase2D;
	class BitmapReadChainBase3D;

	class BitmapReadInvert1D;
	class BitmapReadInvert2D;
	class BitmapReadInvert3D;

	class BitmapReadFiltered2D;
}
