/*
	PhotoRT

	Copyright (C) 2022 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "SurfaceData_fwd.hpp"
#include "Material_fwd.hpp"
#include "Instance_fwd.hpp"
#include "PrimitiveGroup_fwd.hpp"

#include "Base.hpp"

#include <patlib/bitset.hpp>
#include <patlib/marked_optional.hpp>

namespace PhotoRT
{
	class PHOTORT_API SurfaceData
	{
	public:

		enum class EnumFlags : szt
		{
			BackfaceHit = 0
		};

		VecD shadingNormal{pl::tag::identity}, surfaceNormal{pl::tag::identity};
		VecP uv{pl::tag::identity}, worldPos{pl::tag::identity}, localPos{pl::tag::identity};
		Matrix pMat{pl::tag::identity}, worldMatrix{pl::tag::identity};

		const IObjectInstance *inst{};
		const IPrimitiveGroupProperties *objProps{};
		const PrimitiveMaterial *matEntry{};

		Real minimalSafeInc = 0;

		PrimitiveIndex primIndex{};
		if32 layerIndex{};
		pl::enum_bitset<EnumFlags, 1> flags{};

		[[nodiscard]] constexpr bool isBackfaceHit() const noexcept { return flags[EnumFlags::BackfaceHit]; }
		auto &isBackfaceHit(bool v) noexcept { flags[EnumFlags::BackfaceHit] = v; return *this; }

		[[nodiscard]] Lobe<> getShadingLobe() const noexcept
			{ return {anisotropic_p{shadingNormal}}; }

		[[nodiscard]] EnumHemisphere getHemisphere(VecD dir) const noexcept
		{
			const auto dp = dir /odot/ surfaceNormal;
			return dp > 0 ? EnumHemisphere::North : EnumHemisphere::South;
		}

		Real geometricTermBackface(VecP point) noexcept;

		[[nodiscard]] constexpr Real geometricTermPos(VecP pos) const noexcept
			{ return PhotoRT::geometricTermPos(localPos, pos); }

		[[nodiscard]] constexpr Real geometricTermPos(Real dist) const noexcept
			{ return PhotoRT::geometricTermPos(dist); }

		[[nodiscard]] Real geometricTermDir(VecD dir) const noexcept
			{ return surfaceNormal /odot/ dir /oabs; }

		void reverse() noexcept
		{
			shadingNormal = -shadingNormal;
			surfaceNormal = -surfaceNormal;
			isBackfaceHit(!isBackfaceHit());
		}

		auto &operator *= (const Matrix &m) noexcept
		{
			worldMatrix *= m;
			return *this;
		}

	};

	template <typename UVType = VecP, typename RealType = Real>
	struct Collision
	{
		UVType uv;
		RealType dist = std::numeric_limits<RealType>::quiet_NaN();
		bool backface;

		constexpr void reset() noexcept { dist = std::numeric_limits<RealType>::quiet_NaN(); }
		[[nodiscard]] constexpr bool has_value() const noexcept { return !(dist /oisnan); }
	};

	struct CollisionData
	{
		SurfaceData surface;
		Real distSqr = std::numeric_limits<Real>::quiet_NaN();

		constexpr void reset() noexcept { distSqr = std::numeric_limits<Real>::quiet_NaN(); }
		[[nodiscard]] constexpr bool has_value() const noexcept { return !(distSqr /oisnan); }
	};

	struct CollisionResult
	{
		pl::marked_optional<CollisionData> collision;

		#ifdef PRT_EXT_STATS
			StatCounter nbColTest = 0, nbAABBTest = 0;
		#endif

		template <std::predicate<CollisionData &> FC_ = pl::true_pred_t>
		Real update(CollisionResult &&o, Real distmax, FC_&& onCol = {}) noexcept
		{
			#ifdef PRT_EXT_STATS
				nbColTest += o.nbColTest;
				nbAABBTest += o.nbAABBTest;
			#endif

			if(o.collision && PL_FWD(onCol)(*o.collision))
			{
				distmax = o.collision->distSqr;
				collision = std::move(o.collision);
			}

			return distmax;
		}
	};
}
