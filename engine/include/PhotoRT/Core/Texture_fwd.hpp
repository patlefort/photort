/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

namespace PhotoRT
{
	class ColorDistributionSimple;
	class ColorDistributionBase;

	template <typename BitmapType_>
	class ColorDistributionBitmapBase;

	class ColorDistributionBitmap1D;
	class ColorDistributionBitmap2D;
	class ColorDistributionBitmap3D;

	template <typename DistType_>
	class ColorDistributionChainBase;

	template <template<typename, typename> class DistType_>
	class ColorDistributionMultiChannelBase;

	template <template<typename, typename> class DistType_>
	class ColorDistributionSingleChannelBase;

	class ColorDistributionMultiChannel1D;
	class ColorDistributionMultiChannel2D;
	class ColorDistributionMultiChannel3D;

	class ColorDistributionSingleChannel1D;
	class ColorDistributionSingleChannel2D;
	class ColorDistributionSingleChannel3D;

	class MapperRealValue;
	class MapperColorSimple;

	class MapperUVTransformBase;
	class MapperUVPlanar;
	class MapperUVPlanarWorld;
	class MapperUVPanoramic;
	class MapperUVSpherical;
	class MapperUVPanoramicView;
	class MapperUV3D;
	class MapperUV3Dworld;
	class MapperUVTextureCoord;

	template <typename MapperType_>
	class SurfaceMapperUV;

	template <typename MapperType_, typename DistType_>
	class SurfaceMapperDist;

	template <typename MapperType_, typename DistType_>
	class SurfaceMapper2D;

	template <typename MapperType_, typename DistType_>
	class SurfaceMapper3D;

	template <typename OutType_>
	class SurfaceMapperMultiBase;

	template <typename OutType_, typename OperatorType_>
	class SurfaceMapperMulti;
}