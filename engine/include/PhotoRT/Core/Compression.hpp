/*
	PhotoRT

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#if defined(PRT_COMPRESSION_BZIP2) || defined(PRT_COMPRESSION_GZIP) || defined(PRT_COMPRESSION_ZLIB)
	#define PRT_HAS_COMPRESSION
#endif

#ifdef PRT_COMPRESSION_ZLIB
	#include <boost/iostreams/filter/zlib.hpp>

	#ifndef PRT_DEFAULT_COMPRESSOR
		#define PRT_DEFAULT_COMPRESSOR zlib
	#endif
#endif

#ifdef PRT_COMPRESSION_BZIP2
	#include <boost/iostreams/filter/bzip2.hpp>

	#ifndef PRT_DEFAULT_COMPRESSOR
		#define PRT_DEFAULT_COMPRESSOR bzip2
	#endif
#endif

#ifdef PRT_COMPRESSION_GZIP
	#include <boost/iostreams/filter/gzip.hpp>

	#ifndef PRT_DEFAULT_COMPRESSOR
		#define PRT_DEFAULT_COMPRESSOR gzip
	#endif
#endif

#define PRT_COMPRESSOR_TYPE(c) boost::iostreams:: BOOST_JOIN(c, _compressor)
#define PRT_DECOMPRESSOR_TYPE(c) boost::iostreams:: BOOST_JOIN(c, _decompressor)

namespace PhotoRT
{
	#ifdef PRT_DEFAULT_COMPRESSOR
		using compressor = PRT_COMPRESSOR_TYPE(PRT_DEFAULT_COMPRESSOR);
		using decompressor = PRT_DECOMPRESSOR_TYPE(PRT_DEFAULT_COMPRESSOR);
	#else
		using compressor = pl::io::uncompressed_t;
		using decompressor = pl::io::uncompressed_t;
	#endif

	constexpr std::string_view compressorString{ BOOST_STRINGIZE(PRT_DEFAULT_COMPRESSOR) };

	bool selectCompressor(std::string_view type, auto&& fc)
	{
		if(type.empty())
		{
			PL_FWD(fc)(pl::io::uncompressed);
			return true;
		}

		#ifdef PRT_COMPRESSION_BZIP2
			if(type == "bzip2")
			{
				PL_FWD(fc)(PRT_COMPRESSOR_TYPE(bzip2){});
				return true;
			}
		#endif

		#ifdef PRT_COMPRESSION_GZIP
			if(type == "gzip")
			{
				PL_FWD(fc)(PRT_COMPRESSOR_TYPE(gzip){});
				return true;
			}
		#endif

		#ifdef PRT_COMPRESSION_ZLIB
			if(type == "zlib")
			{
				PL_FWD(fc)(PRT_COMPRESSOR_TYPE(zlib){});
				return true;
			}
		#endif

		return false;
	}

	bool selectDecompressor(std::string_view type, auto&& fc)
	{
		if(type.empty())
		{
			PL_FWD(fc)(pl::io::uncompressed);
			return true;
		}

		#ifdef PRT_COMPRESSION_BZIP2
			if(type == "bzip2")
			{
				PL_FWD(fc)(PRT_DECOMPRESSOR_TYPE(bzip2){});
				return true;
			}
		#endif

		#ifdef PRT_COMPRESSION_GZIP
			if(type == "gzip")
			{
				PL_FWD(fc)(PRT_DECOMPRESSOR_TYPE(gzip){});
				return true;
			}
		#endif

		#ifdef PRT_COMPRESSION_ZLIB
			if(type == "zlib")
			{
				PL_FWD(fc)(PRT_DECOMPRESSOR_TYPE(zlib){});
				return true;
			}
		#endif

		return false;
	}
}