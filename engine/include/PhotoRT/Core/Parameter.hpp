/*
	PhotoRT

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Parameter_fwd.hpp"

#include "Base.hpp"
#include "RenderEntityInterface.hpp"

#include <patlib/crtp.hpp>
#include <patlib/marked_optional.hpp>

namespace PhotoRT
{
	[[nodiscard]] constexpr bool isReservedAttribute(std::string_view name) noexcept
	{
		return name == pl::any_of{"array", "id", "type", "link", "value", "xmlns", "template"};
	}

	namespace ImplParameter
	{
		template <typename ContainerType_>
		class ParamContainer : public pl::crtp_base<ContainerType_>
		{
			using pl::crtp_base<ContainerType_>::derived;

		public:

			template <typename T_ = std::string>
			[[nodiscard]] auto findValue(std::string_view key, type_c<T_> = {}) const
				-> pl::marked_optional<T_, pl::marked_optional_optional_policy<T_>>
			{
				return derived().findParam(key)
					.transform([&](const auto &p){
						return p.to(type_v<T_>);
					}, type_v<pl::marked_optional_optional_policy<T_>>);
			}

			template <typename T_>
			[[nodiscard]] T_ getValueOrDefault(std::string_view key, T_&& def) const
			{
				return findValue(key, type_v<std::decay_t<T_>>).value_or(PL_FWD(def));
			}

			template <typename T_>
			auto getValueOrDefault(std::string_view key, T_ &value, auto&& def) const
			{
				value = getValueOrDefault<T_>(key, PL_FWD(def));

				return PL_LAMBDA_FORWARD_THIS(getValueOrDefault);
			}

			auto getValueOrDefaultBind(std::string_view key, auto&& fc, auto&& def) const
			{
				std::invoke(PL_FWD(fc), getValueOrDefault(key, PL_FWD(def)));

				return PL_LAMBDA_FORWARD_THIS(getValueOrDefaultBind);
			}

			auto getValueIfExists(std::string_view key, auto &value) const
			{
				optional_assign_to(derived().findParam(key), value);

				return PL_LAMBDA_FORWARD_THIS(getValueIfExists);
			}

			auto getValueIfExists(std::string_view key, std::string &value) const
			{
				if(auto p = derived().findParam(key); p)
					value = p->value;

				return PL_LAMBDA_FORWARD_THIS(getValueIfExists);
			}

			auto getValueIfExistsBind(std::string_view key, auto &&fc) const
			{
				if(auto p = derived().findParam(key); p)
					std::invoke(PL_FWD(fc), *p);

				return PL_LAMBDA_FORWARD_THIS(getValueIfExistsBind);
			}

			template <typename T_>
			auto insertReference(const std::string &key, const T_ *entity, bool deep = false)
			{
				if(entity)
					derived()[key].fromEntityRef(*entity, deep);

				return PL_LAMBDA_FORWARD_THIS(insertReference);
			}

		};
	} // namespace ImplParameter



	class PHOTORT_API ParameterTree : public ImplParameter::ParamContainer<ParameterTree>
	{
		using MapType = std::map<std::string, Parameter, std::less<>>;

		MapType mProps;

	public:

		using value_type = typename MapType::value_type;
		using key_type = typename MapType::key_type;
		using mapped_type = typename MapType::mapped_type;
		using size_type = typename MapType::size_type;
		using difference_type = typename MapType::difference_type;
		using key_compare = typename MapType::key_compare;
		using allocator_type = typename MapType::allocator_type;
		using reference = typename MapType::reference;
		using const_reference = typename MapType::const_reference;
		using pointer = typename MapType::pointer;
		using const_pointer = typename MapType::const_pointer;
		using iterator = typename MapType::iterator;
		using const_iterator = typename MapType::const_iterator;
		using reverse_iterator = typename MapType::reverse_iterator;
		using const_reverse_iterator = typename MapType::const_reverse_iterator;
		using node_type = typename MapType::node_type;
		using insert_return_type = typename MapType::insert_return_type;

		ParameterTree() = default;

		template<typename InputIterator_>
		ParameterTree(InputIterator_ first, InputIterator_ last)
			: mProps(first, last) {}

		ParameterTree(std::initializer_list<value_type> init)
			: mProps(init) {}

		[[nodiscard]] mapped_type &at(const auto &key);
		[[nodiscard]] const mapped_type &at(const auto &key) const;

		mapped_type & operator[] (const auto &key);
		[[nodiscard]] const mapped_type &operator[] (const auto &key) const;

		[[nodiscard]] auto get_allocator() const { return mProps.get_allocator(); }
		[[nodiscard]] auto key_comp() const { return mProps.key_comp(); }
		[[nodiscard]] auto value_comp() const { return mProps.value_comp(); }

		[[nodiscard]] auto begin() { return mProps.begin(); }
		[[nodiscard]] auto begin() const { return mProps.begin(); }
		[[nodiscard]] auto cbegin() const { return mProps.cbegin(); }

		[[nodiscard]] auto end() { return mProps.end(); }
		[[nodiscard]] auto end() const { return mProps.end(); }
		[[nodiscard]] auto cend() const { return mProps.cend(); }

		[[nodiscard]] auto rbegin();
		[[nodiscard]] auto rbegin() const;
		[[nodiscard]] auto crbegin() const;

		[[nodiscard]] auto rend();
		[[nodiscard]] auto rend() const;
		[[nodiscard]] auto crend() const;

		[[nodiscard]] bool empty() const noexcept { return mProps.empty(); }
		[[nodiscard]] auto size() const noexcept { return mProps.size(); }
		[[nodiscard]] auto max_size() const noexcept { return mProps.max_size(); }

		void clear() noexcept { mProps.clear(); }

		void swap(ParameterTree &o) noexcept { mProps.swap(o.mProps); }

		template <typename... Args_>
		decltype(auto) insert(Args_&&... args) { return mProps.insert(PL_FWD(args)...); }

		inline decltype(auto) insert(std::initializer_list<value_type> ilist);

		template <typename... Args_>
		decltype(auto) insert_or_assign(Args_&&... args) { return mProps.insert_or_assign(PL_FWD(args)...); }

		template <typename... Args_>
		decltype(auto) try_emplace(Args_&&... args) { return mProps.try_emplace(PL_FWD(args)...); }

		template <typename... Args_>
		[[nodiscard]] decltype(auto) extract(Args_&&... args) { return mProps.extract(PL_FWD(args)...); }

		inline void merge(ParameterTree &o);
		inline void merge(ParameterTree &&o);

		template<typename... Args_>
		auto emplace(Args_&&... args) { return mProps.emplace(PL_FWD(args)...); }

		template<typename... Args_>
		auto emplace_hint(const_iterator hint, Args_&&... args) { return mProps.emplace_hint(hint, PL_FWD(args)...); }

		template <typename... Args_>
		decltype(auto) erase(Args_&&... args) { return mProps.erase(PL_FWD(args)...); }

		[[nodiscard]] size_type count(const key_type &key) const { return mProps.count(key); }
		[[nodiscard]] size_type count(const szt key) const { return count(pl::io::to_string_noloc(key)); }
		template<typename K_>
		[[nodiscard]] size_type count(const K_ &key) const { return mProps.count(key); }

		template <typename... Args_>
		[[nodiscard]] decltype(auto) find(Args_&&... args) { return mProps.find(PL_FWD(args)...); }
		template <typename... Args_>
		[[nodiscard]] decltype(auto) find(Args_&&... args) const { return mProps.find(PL_FWD(args)...); }

		template <typename... Args_>
		[[nodiscard]] decltype(auto) equal_range(Args_&&... args) { return mProps.equal_range(PL_FWD(args)...); }
		template <typename... Args_>
		[[nodiscard]] decltype(auto) equal_range(Args_&&... args) const { return mProps.equal_range(PL_FWD(args)...); }

		template <typename... Args_>
		[[nodiscard]] decltype(auto) lower_bound(Args_&&... args) { return mProps.lower_bound(PL_FWD(args)...); }
		template <typename... Args_>
		[[nodiscard]] decltype(auto) lower_bound(Args_&&... args) const { return mProps.lower_bound(PL_FWD(args)...); }

		template <typename... Args_>
		[[nodiscard]] decltype(auto) upper_bound(Args_&&... args) { return mProps.upper_bound(PL_FWD(args)...); }
		template <typename... Args_>
		[[nodiscard]] decltype(auto) upper_bound(Args_&&... args) const { return mProps.upper_bound(PL_FWD(args)...); }

		[[nodiscard]] inline pl::marked_optional<const Parameter &> findParam(std::string_view key) const;
		[[nodiscard]] inline pl::marked_optional<Parameter &> findParam(std::string_view key);

		[[nodiscard]] inline ParameterTree operator+ (const ParameterTree &other);
		inline ParameterTree &operator+= (const ParameterTree &other);

		[[nodiscard]] const Parameter *findEntityRef(std::string_view id) const;

		[[nodiscard]] bool contains(auto&& key) const;

		template <std::invocable<const ParameterTree::value_type &, if32> FC_>
		void traverse(FC_ &&fc, if32 level = 0) const;

		void print(std::ostream &os) const;

		//void serializeWithTabs(std::ostream &outputStream, int nbTabs) const;

		friend void swap(ParameterTree &m1, ParameterTree &m2) noexcept
			{ m1.swap(m2); }

		[[nodiscard]] auto operator<=>(const ParameterTree &o) const;
	};


	// Represent a single parameter as a string.
	// Parameters are always converted from and to character strings.
	// Can contain child parameters in childParams.
	class PHOTORT_API Parameter : public ImplParameter::ParamContainer<Parameter>
	{
	public:
		Parameter() = default;

		Parameter(const Parameter &other);
		Parameter(Parameter &&other) noexcept;

		Parameter(const ParameterTree &other);
		Parameter(ParameterTree &&other);

		// Convert string to type T_
		template <typename T_>
		[[nodiscard]] T_ to(type_c<T_> = {}) const
			{ using pl::io::from_string_noloc; return from_string_noloc(value, type_v<T_>); }

		// Convert given type to string and store it
		void from(const auto &val)
			{ value = pl::io::to_string_noloc(val); }

		void from(Parameter val);
		void from(ParameterTree val);
		void from(const char *val);
		void from(u8 val);
		void from(char val);
		void from(std::string val) noexcept;

		template <typename T_>
		Parameter(T_ &&v)
				requires (
					!std::same_as<std::remove_cv_t<T_>, Parameter> &&
					!std::same_as<std::remove_cv_t<T_>, ParameterTree>)
			{ from(PL_FWD(v)); }

		Parameter(std::string value_, ParameterTree params);
		Parameter(std::string_view entityType, std::string_view entityId, ParameterTree params);

		Parameter &operator= (const Parameter &other);
		Parameter &operator= (Parameter &&other) noexcept;

		Parameter &operator= (const ParameterTree &other);
		Parameter &operator= (ParameterTree &&other);

		template <typename T_>
		Parameter &operator=(T_ &&v)
				requires (
					!std::same_as<std::remove_cv_t<T_>, Parameter> &&
					!std::same_as<std::remove_cv_t<T_>, ParameterTree>)
			{ from(PL_FWD(v)); return *this; }

		// Allow implicit conversions
		template <typename T_>
		[[nodiscard]] operator T_() const
			{ return this->template to<T_>(); }

		//[[nodiscard]] operator std::string() const
			//{ return value; }

		// Locale used for parsing text
		static std::locale loc;

		std::string value;
		pl::uninitialized_value<ParameterTree> childParams;

		// Entity reference parsing
		// A reference is encoded in this form:
		// $<generic type>-<specific type>/<id>
		// ex: $transform-rotation/rotationctrl
		[[nodiscard]] bool isEntity() const;
		[[nodiscard]] bool isEntityLink() const;
		void getEntityType(std::string &type, std::string &id) const;
		[[nodiscard]] std::pair<std::string, std::string> getEntityType() const noexcept;
		void toEntity(std::string_view type, std::string_view id, bool ref = false);
		void fromEntityRef(const IRenderEntity &entity, bool deep = false);

		template <typename EntityType_>
		void fromEntityRef(const EntityType_ &entity, bool deep = false)
		{
			const auto &entityTyped = dynamic_cast<const IRenderEntity &>(entity);

			fromEntityRef(entityTyped, deep);
		}

		[[nodiscard]] pl::marked_optional<const Parameter &> findParam(std::string_view key) const;
		[[nodiscard]] pl::marked_optional<Parameter &> findParam(std::string_view key);

		void hasChilds(bool hasChilds);
		[[nodiscard]] bool hasChilds() const noexcept { return childParams.has_value(); }
		void setChildParams(ParameterTree params);

		[[nodiscard]] ParameterTree getChildParamsCopy() const;
		[[nodiscard]] ParameterTree getChildParamsMove();

		[[nodiscard]] bool contains(std::string_view key) const;

		[[nodiscard]] Parameter &operator[] (const auto &key);
		[[nodiscard]] const Parameter &operator[] (const auto &key) const;

		friend auto &operator<<(std::ostream &os, const Parameter &p)
			{ return os << p.value; }

		[[nodiscard]] auto operator<=>(const Parameter &o) const { return value <=> o.value; }
	};

	template <>
	[[nodiscard]] u8 PHOTORT_API Parameter::to<u8>(type_c<u8>) const;
	template <>
	[[nodiscard]] char PHOTORT_API Parameter::to<char>(type_c<char>) const;
	template <>
	[[nodiscard]] std::string PHOTORT_API Parameter::to<std::string>(type_c<std::string>) const;
	template <>
	[[nodiscard]] std::string_view PHOTORT_API Parameter::to<std::string_view>(type_c<std::string_view>) const;

	Parameter &Parameter::operator[] (const auto &key)
		{ hasChilds(true); return (*childParams)[key]; }

	const Parameter &Parameter::operator[] (const auto &key) const
		{ return childParams->at(key); }


	[[nodiscard]] ParameterTree::mapped_type &ParameterTree::at(const auto &key)
		{ return mProps.at(key); }
	[[nodiscard]] const ParameterTree::mapped_type &ParameterTree::at(const auto &key) const
		{ return mProps.at(key); }

	[[nodiscard]] const ParameterTree::mapped_type &ParameterTree::operator[] (const auto &key) const
	{
		if constexpr(std::integral<std::decay_t<decltype(key)>>)
			return at(pl::io::to_string_noloc(key));
		else if constexpr(pl::is_a<decltype(key), std::string_view>)
			return at(std::string{key});
		else
			return at(key);
	}
	ParameterTree::mapped_type &ParameterTree::operator[] (const auto &key)
	{
		if constexpr(std::integral<std::decay_t<decltype(key)>>)
			return mProps[pl::io::to_string_noloc(key)];
		else if constexpr(pl::is_a<decltype(key), std::string_view>)
			return mProps[std::string{key}];
		else
			return mProps[key];
	}

	inline decltype(auto) ParameterTree::insert(std::initializer_list<value_type> ilist)
		{ return mProps.insert(ilist); }

	[[nodiscard]] bool ParameterTree::contains(auto&& key) const
		{ return mProps.contains(PL_FWD(key)); }

	inline void ParameterTree::merge(ParameterTree &o)
		{ mProps.merge(o.mProps); }
	inline void ParameterTree::merge(ParameterTree &&o)
		{ mProps.merge(std::move(o.mProps)); }

	template <std::invocable<const ParameterTree::value_type &, if32> FC_>
	void ParameterTree::traverse(FC_ &&fc, if32 level) const
	{
		for(const auto &e : mProps)
		{
			PL_FWD(fc)(e, level);

			if(e.second.childParams)
				e.second.childParams->traverse(PL_FWD(fc), level + 1);
		}
	}

	[[nodiscard]] inline pl::marked_optional<const Parameter &> ParameterTree::findParam(std::string_view key) const
	{
		if(auto it = mProps.find(key); it != mProps.end())
			return {it->second};

		return pl::nullopt;
	}

	[[nodiscard]] inline pl::marked_optional<Parameter &> ParameterTree::findParam(std::string_view key)
	{
		if(auto it = mProps.find(key); it != mProps.end())
			return {it->second};

		return pl::nullopt;
	}

	[[nodiscard]] inline ParameterTree ParameterTree::operator+ (const ParameterTree &other)
	{
		ParameterTree newl(*this);

		newl.insert(other.begin(), other.end());

		return newl;
	}

	inline ParameterTree &ParameterTree::operator+= (const ParameterTree &other)
	{
		mProps.insert(other.begin(), other.end());

		return *this;
	}

	[[nodiscard]] inline auto ParameterTree::rbegin() { return mProps.rbegin(); }
	[[nodiscard]] inline auto ParameterTree::rbegin() const { return mProps.rbegin(); }
	[[nodiscard]] inline auto ParameterTree::crbegin() const { return mProps.crbegin(); }

	[[nodiscard]] inline auto ParameterTree::rend() { return mProps.rend(); }
	[[nodiscard]] inline auto ParameterTree::rend() const { return mProps.rend(); }
	[[nodiscard]] inline auto ParameterTree::crend() const { return mProps.crend(); }

	[[nodiscard]] inline auto ParameterTree::operator<=>(const ParameterTree &o) const { return mProps <=> o.mProps; }
}
