/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "RenderEntityFactory_fwd.hpp"

#include "Base.hpp"
#include "RenderEntityBase.hpp"
#include "RenderEntityInterface.hpp"

#include <patlib/factory.hpp>

#include <boost/bimap/bimap.hpp>
#include <boost/bimap/multiset_of.hpp>
#include <boost/bimap/list_of.hpp>

#include <memory_resource>

namespace PhotoRT
{
	using IRenderEntityFactory = pl::IGenericSmartFactory<IRenderEntity>;

	using EntityFactoryMap = std::map<std::string, IRenderEntityFactory *, std::less<>>;

	using GenericTypeHandlersMap = boost::bimaps::bimap<
		boost::bimaps::multiset_of<std::string, std::less<>>,
		boost::bimaps::multiset_of<IEntityImportExport *, std::less<>>,
		boost::bimaps::list_of_relation
	>;

	template <typename EntityClass_>
	using EntityFactoryAllocator = pl::allocator_with_stats<EntityClass_, memtag::Entity>;

	template <typename EntityClass_>
	using EntityFactory = pl::GenericSmartFactoryAlloc<IRenderEntity, EntityClass_, EntityFactoryAllocator<EntityClass_>>;

	class IEntityFactoriesProxy
	{
	public:
		virtual ~IEntityFactoriesProxy() = default;

		[[nodiscard]] virtual EntityPtrBase createEntity(std::string_view id, std::string_view type) = 0;
	};

	class PHOTORT_API Factories
	{
	public:
		// Factories
		EntityFactoryMap mFactories;

		void registerFactory(std::string type, IRenderEntityFactory &factory);
		void registerAlias(std::string alias, std::string_view aliased);
		[[nodiscard]] EntityFactoryMap getTypeFactories(std::string_view type);
		void dumpFactories(std::ostream &os);
		[[nodiscard]] bool factoryExists(std::string_view type) const;

		// Import-exporters
		std::map<std::string, IEntityImportExport *, std::less<>> mEntityImportExport;
		ExtensionMimeTypeMap mMimeTypesImport, mMimeTypesExport;
		std::map<std::string, IEntityImportExport *, std::less<>> mMimeTypeHandlersImport, mMimeTypeHandlersExport;
		GenericTypeHandlersMap mGenericTypesHandlers;

		[[nodiscard]] std::string getMimeTypeImport(const std::filesystem::path &filename);
		[[nodiscard]] std::string getMimeTypeExport(const std::filesystem::path &filename);
		[[nodiscard]] std::string getMimeExtImport(std::string_view mimeType);
		[[nodiscard]] std::string getMimeExtExport(std::string_view mimeType);
		void registerImportExporter(std::string name, IEntityImportExport &importExport);
		[[nodiscard]] IEntityImportExport *getEntityImporter(std::string_view mimeType, std::string_view expectedType = "", bool streamable = false);
		[[nodiscard]] IEntityImportExport *getEntityExporter(std::string_view mimeType, std::string_view expectedType = "", bool streamable = false);
		void dumpImporterExporter(std::ostream &os);

		[[nodiscard]] EntityPtrBase createEntity(std::string_view type);

		template <typename EntityType_>
		[[nodiscard]] EntityPtr<EntityType_> createEntity(std::string_view type)
			{ return DynEntityCast<EntityType_>(createEntity(type)); }

	};


	class CachedFactorySearch : pl::copymove_initialize_only
	{
	public:

		Factories &mFactories;

		CachedFactorySearch(const CachedFactorySearch &) = default;
		CachedFactorySearch(Factories &f) :
			mFactories{f} {}

		[[nodiscard]] IRenderEntityFactory *find(std::string_view type)
		{
			IRenderEntityFactory *factory;

			if(mCachedType == type)
				factory = mCachedFactory;
			else
			{
				auto it = mFactories.mFactories.find(type);
				if(it == mFactories.mFactories.end())
					return nullptr;

				factory = mCachedFactory = it->second;
				mCachedType = type;
			}

			return factory;
		}

	private:
		std::string mCachedType;
		IRenderEntityFactory *mCachedFactory = nullptr;
	};


	class PHOTORT_API EntityFactoriesProxy : public IEntityFactoriesProxy, pl::copymove_initialize_only
	{
	public:

		Factories &mFactories;

		EntityFactoriesProxy(const EntityFactoriesProxy &) = default;
		EntityFactoriesProxy(Factories &f) :
			mFactories{f}, mFactorySearch{f} {}

		[[nodiscard]] virtual EntityPtrBase createEntity(std::string_view id, std::string_view type) override;

	private:
		CachedFactorySearch mFactorySearch;

	};

	class PHOTORT_API EntityFactoriesProxyMemoryResource : public IEntityFactoriesProxy, pl::copymove_initialize_only
	{
	public:

		std::pmr::memory_resource *mMemRes;
		Factories &mFactories;

		EntityFactoriesProxyMemoryResource(const EntityFactoriesProxyMemoryResource &) = default;
		EntityFactoriesProxyMemoryResource(Factories &f, std::pmr::memory_resource *mr = std::pmr::get_default_resource()) :
			mMemRes{mr}, mFactories{f}, mFactorySearch{f} {}

		[[nodiscard]] virtual EntityPtrBase createEntity(std::string_view id, std::string_view type) override;

	private:
		CachedFactorySearch mFactorySearch;
	};


	// Access global factories
	namespace impl
	{
		PL_CONST_FC( [[nodiscard]] Factories PHOTORT_API &factories() );
	}

	inline auto &factories = impl::factories();

	class PHOTORT_API EntityFactoryAlias
	{
	public:
		EntityFactoryAlias(std::string_view alias, std::string_view aliased);
	};

	template <typename BaseType_>
	class EntityFactoryProxy : public BaseType_
	{
	public:
		EntityFactoryProxy(const auto &name)
		{
			const auto sView = pl::to_string_view(name);
			plassert(!sView.empty());
			factories.registerFactory(std::string{sView}, *this);
		}
	};

	template <typename BaseType_>
	class EntityImportExportProxy : public BaseType_
	{
	public:
		EntityImportExportProxy(const auto &name)
		{
			const auto sView = pl::to_string_view(name);
			plassert(!sView.empty());
			factories.registerImportExporter(std::string{sView}, *this);
		}
	};

	#define PRT_ENTITY_FACTORY_ALIAS(factoryName, name, ...) \
		::PhotoRT::EntityFactoryAlias prtFactory_##factoryName {name, __VA_ARGS__};

	#define PRT_ENTITY_FACTORY_DECLARE(name, ...) \
		inline static constexpr char EntityType[] {name}; \
		static ::PhotoRT::EntityFactoryProxy<::PhotoRT::EntityFactory<__VA_ARGS__>> Factory; \
		[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return ::patlib::to_string_view(EntityType); }

	#define PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE_INCLASS(...) \
		static char EntityType[]; \
		static ::PhotoRT::EntityFactoryProxy<::PhotoRT::EntityFactory<__VA_ARGS__>> Factory; \
		[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return ::patlib::to_string_view(EntityType); }

	#define PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(...) \
		template <> char PHOTORT_API __VA_ARGS__::EntityType[]; \
		template <> PHOTORT_API ::PhotoRT::EntityFactoryProxy<::PhotoRT::EntityFactory<__VA_ARGS__>> __VA_ARGS__::Factory;

	#define PRT_ENTITY_FACTORY_DEFINE(...) \
		::PhotoRT::EntityFactoryProxy<::PhotoRT::EntityFactory<__VA_ARGS__>> __VA_ARGS__::Factory{EntityType};

	#define PRT_ENTITY_FACTORY_SPECIALIZE_DEFINE(name, ...) \
		template <> char PHOTORT_API __VA_ARGS__::EntityType[] {name}; \
		template <> ::PhotoRT::EntityFactoryProxy<::PhotoRT::EntityFactory<__VA_ARGS__>> __VA_ARGS__::Factory{EntityType};

	#define PRT_ENTITY_FACTORY_INLINE(name, ...) \
		inline static constexpr char EntityType[] {name}; \
		inline static ::PhotoRT::EntityFactoryProxy<::PhotoRT::EntityFactory<__VA_ARGS__>> Factory{::patlib::to_string_view(EntityType)}; \
		[[nodiscard]] virtual std::string_view getEntityType() const noexcept override { return ::patlib::to_string_view(EntityType); }

	#define PRT_ENTITY_FACTORY_AUGMENT_DECLARE(name, classname, ...) \
		class PHOTORT_API classname final : public ::PhotoRT::RenderEntityAugment<__VA_ARGS__> \
		{ \
		public: \
			PRT_ENTITY_FACTORY_DECLARE(name, classname) \
			virtual void setParameters(const ::PhotoRT::ParameterTree &params) final; \
			[[nodiscard]] virtual ::PhotoRT::ParameterTree getParameters(bool deep = false) const final; \
		};

	#define PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS(name, classname, ...) \
		class PHOTORT_API classname final : public ::PhotoRT::RenderEntityAugment<__VA_ARGS__> \
		{ \
		public: \
			PRT_ENTITY_FACTORY_DECLARE(name, classname) \
		};

	#define PRT_ENTITY_FACTORY_AUGMENT_DEFINE(...) \
		PRT_ENTITY_FACTORY_DEFINE(__VA_ARGS__)

	#define PRT_ENTITY_FACTORY_AUGMENT_INLINE(name, classname, ...) \
		class PHOTORT_API classname final : public ::PhotoRT::RenderEntityAugment<__VA_ARGS__> \
		{ \
		public: \
			PRT_ENTITY_FACTORY_INLINE(name, classname) \
		};


	#define PRT_IMPORTEXPORTER_DECLARE(...) \
		static char ImportExporterName[]; \
		static ::PhotoRT::EntityImportExportProxy<__VA_ARGS__> RegisterImportExporter; \
		[[nodiscard]] virtual std::string_view getImportExporterName() const noexcept override { return ::patlib::to_string_view(ImportExporterName); }

	#define PRT_IMPORTEXPORTER_DEFINE(name, ...) \
		char __VA_ARGS__::ImportExporterName[] = name; \
		::PhotoRT::EntityImportExportProxy<__VA_ARGS__> __VA_ARGS__::RegisterImportExporter{ImportExporterName};
}
