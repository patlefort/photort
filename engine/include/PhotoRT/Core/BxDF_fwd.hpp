/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "RenderEntity_fwd.hpp"

namespace PhotoRT
{
	class IMicrofacetsDistribution;
	class MicrofacetsBase;

	class IFresnel;
	class FresnelBase;

	template <pl::simd::CColorVector ColorT_>
	struct ColorPair;

	struct SampledBxDF;
	struct BxDFRadianceResult;

	class IBxDF;
	class BxDFBase;
	class BxDFChainBase;
}
