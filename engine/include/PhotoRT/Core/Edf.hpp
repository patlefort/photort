/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Edf_fwd.hpp"
#include "Interaction_fwd.hpp"

#include "Base.hpp"
//#include "RayTracer.hpp"

#include <patlib/marked_optional.hpp>

namespace PhotoRT
{
	struct EDFSample
	{
		Color color;
		VecD dir;
		Real pdf = 0;
		EnumHemisphere hemisphere;
		bool delta = false;

		constexpr void reset() noexcept { pdf = 0; }
		[[nodiscard]] constexpr bool has_value() const noexcept { return pdf != 0; }
	};

	// Emisison distribution function
	class IEDF
	{
	public:
		virtual ~IEDF() = default;

		[[nodiscard]] virtual Color radiance(const SurfaceInteraction &inter, VecD idir) const noexcept = 0;
		[[nodiscard]] virtual Color radianceDiracDelta(const SurfaceInteraction &inter, EnumHemisphere hemisphere) const noexcept = 0;
		[[nodiscard]] virtual pl::marked_optional<EDFSample> sampleDir(RandomReals<2> rnd, const SurfaceInteraction &inter) const noexcept = 0;
		[[nodiscard]] virtual OptionalPDF<Real> PDF(const SurfaceInteraction &inter, VecD idir) const noexcept = 0;

		[[nodiscard]] virtual bool isHemisphereValid(EnumHemisphere hemisphere) const noexcept = 0;
		[[nodiscard]] virtual bool isDiracDelta() const noexcept = 0;
		[[nodiscard]] virtual bool isAnisotropic() const noexcept = 0;

		[[nodiscard]] Color operator() (const SurfaceInteraction &inter, VecD idir) const noexcept
		{
			return radiance(inter, idir);
		}
	};
}
