/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Emitter_fwd.hpp"
//#include "Light/LightCommon_fwd.hpp"
#include "Material_fwd.hpp"
#include "Camera_fwd.hpp"
#include "Instance_fwd.hpp"
#include "PrimitiveGroup_fwd.hpp"
#include "SurfaceMapper_fwd.hpp"
#include "Scene_fwd.hpp"

#include "Base.hpp"
#include "RenderEntityInterface.hpp"
#include "RenderEntityCollection.hpp"
#include <PhotoRT/Emitter/Instance.hpp>

namespace PhotoRT
{
	class PHOTORT_API Scene : public EntityCollection, public IAABox
	{
	public:
		Scene();
		virtual ~Scene() override;

		virtual void removeEntity(const IRenderEntity &entity) override;

		virtual void initMaterialsForRendering();

		void clearScene();

		void removeObject(const IPrimitiveGroup &obj);

		[[nodiscard]] const auto &getInfiniteLights() const { return mInfiniteLights; }
		[[nodiscard]] auto &getInfiniteLights() { return mInfiniteLights; }
		[[nodiscard]] const auto &getFiniteLights() const { return mFiniteLights; }
		[[nodiscard]] auto &getFiniteLights() { return mFiniteLights; }
		[[nodiscard]] const auto &getStaticLights() const { return mStaticLights; }
		[[nodiscard]] auto &getStaticLights() { return mStaticLights; }
		[[nodiscard]] const auto &getLightsNoDelta() const { return mLightsNoDelta; }
		[[nodiscard]] auto &getLightsNoDelta() { return mLightsNoDelta; }
		[[nodiscard]] const auto &getLights() const { return mLights; }
		[[nodiscard]] auto &getLights() { return mLights; }
		[[nodiscard]] const auto &getLightPortals() const { return mLightPortals; }
		[[nodiscard]] auto &getLightPortals() { return mLightPortals; }
		[[nodiscard]] const auto &getLightObjects() const { return mLightObjects; }
		[[nodiscard]] auto &getLightObjects() { return mLightObjects; }

		[[nodiscard]] const auto &getLightPortalsPtr() const { return mLightPortalsPtr; }
		[[nodiscard]] auto &getLightPortalsPtr() { return mLightPortalsPtr; }
		[[nodiscard]] const auto &getLightObjectsPtr() const { return mLightObjectsPtr; }
		[[nodiscard]] auto &getLightObjectsPtr() { return mLightObjectsPtr; }

		[[nodiscard]] const auto &getMaterials() const { return mMaterials; }
		[[nodiscard]] auto &getMaterials() { return mMaterials; }
		[[nodiscard]] const auto &getCameras() const { return mCameras; }
		[[nodiscard]] auto &getCameras() { return mCameras; }
		[[nodiscard]] const auto &getObjects() const { return mObjects; }
		[[nodiscard]] auto &getObjects() { return mObjects; }
		[[nodiscard]] const auto &getCameraCtrls() const { return mCameraCtrls; }
		[[nodiscard]] auto &getCameraCtrls() { return mCameraCtrls; }
		[[nodiscard]] const auto &getLightGroups() const { return mLightGroups; }

		[[nodiscard]] bool hasInfiniteSurface() const noexcept { return mHasInfiniteSurface; }


		[[nodiscard]] virtual Box getAABox() const noexcept override { return mAaboxTotal; }

		void updateCameras();

		void init();

		void setRootInstance(EntityPtr<IObjectInstance> inst) noexcept;
		[[nodiscard]] auto &getRootInstance() noexcept { return mRootInstance; }
		[[nodiscard]] const auto &getRootInstance() const noexcept { return mRootInstance; }

		void setAmbientSubstance(EntityPtr<const Material> ambientSubstance) noexcept;
		[[nodiscard]] const auto &getAmbientSubstance() const noexcept { return mAmbientSubstance; }

		void setBackground(EntityPtr<const IColorMapper> bg) noexcept;
		[[nodiscard]] const auto &getBackground() const noexcept { return mBackground; }

		void setParameters(const ParameterTree &params);
		[[nodiscard]] ParameterTree getParameters(bool deep = false) const;

		void saveToDirectory(const std::filesystem::path &path);
		void loadFromDirectory(const std::filesystem::path &path);

	protected:

		Box mAaboxTotal{};

		EntityPtrPC<IObjectInstance> mRootInstance;

		std::vector<EmitterObjectInstance>
			mLightObjects,
			mLightPortals;

		std::vector<std::experimental::propagate_const<IEmitter *>>
			mLights,
			mInfiniteLights,
			mFiniteLights,
			mLightsNoDelta,
			mLightObjectsPtr,
			mLightPortalsPtr;

		std::vector<EntityPtrPC<IEmitter>> mStaticLights;
		std::vector<EntityPtrPC<Material>> mMaterials;
		std::vector<EntityPtrPC<ICamera>> mCameras;
		std::vector<EntityPtrPC<IPrimitiveGroup>> mObjects;
		std::vector<EntityPtrPC<ICameraController>> mCameraCtrls;

		std::set<std::string, std::less<>> mLightGroups;

		EntityPtr<const Material> mAmbientSubstance;
		EntityPtr<const IColorMapper> mBackground;

		bool mHasInfiniteSurface = false;

		virtual void onEntityCreate(const EntityPtrBase &entity, std::string &id) override;

		friend std::ostream &operator<<(std::ostream &os, const Scene &scene);
		friend std::istream &operator>>(std::istream &is, Scene &scene);

	};

	PHOTORT_API std::ostream &operator<<(std::ostream &os, const Scene &scene);
	PHOTORT_API std::istream &operator>>(std::istream &is, Scene &scene);
}
