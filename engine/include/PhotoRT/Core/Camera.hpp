/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "RayTracer_fwd.hpp"
#include "Emitter_fwd.hpp"
#include "Material_fwd.hpp"
#include "Transform_fwd.hpp"
#include "Camera_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"
#include "Sensors.hpp"
#include "SubstanceStack.hpp"
#include "Ray.hpp"

#include <patlib/rectangle.hpp>

namespace PhotoRT
{
	struct SampledImagePhotonDir
	{
		SampledSensorIn sensorIn;
		Point2Dr imagePoint;

		constexpr void reset() noexcept { sensorIn.reset(); }
		[[nodiscard]] constexpr bool has_value() const noexcept { return sensorIn.has_value(); }
	};

	struct CameraIntersection
	{
		Point2Dr position;
		Color color;
		Real dist = std::numeric_limits<Real>::quiet_NaN();

		constexpr void reset() noexcept { dist = std::numeric_limits<Real>::quiet_NaN(); }
		[[nodiscard]] constexpr bool has_value() const noexcept { return !pl::plisnan(dist); }
	};

	class ICamera : public ISensor
	{
	public:
		virtual ~ICamera() = default;

		virtual void setPosition(VecP pos) = 0;
		[[nodiscard]] virtual VecP getPosition() const noexcept = 0;
		virtual void setLookAt(VecP lookAt) = 0;
		[[nodiscard]] virtual VecP getLookAt() const noexcept = 0;
		virtual void setUp(VecP up) = 0;
		[[nodiscard]] virtual VecP getUp() const noexcept = 0;

		[[nodiscard]] virtual pl::marked_optional<SampledSensorOut> sampleImageDir(IRNGInt &rng, RandomReals<2> rndPos, Point2Dr pos) const noexcept = 0;
		[[nodiscard]] virtual pl::marked_optional<SampledImagePhotonDir> sampleImagePhotonDir(const MaterialInteraction &surface, IRNGInt &rng, RandomReals<2> rndPos, const ISensor *scaleTo = nullptr) const noexcept = 0;

		virtual pl::marked_optional<CameraIntersection> intersectExt(const VecP origin, const VecD dir) const noexcept = 0;
		[[nodiscard]] virtual bool intersect(const VecP origin, const VecD dir) const noexcept = 0;

		virtual void setAspectRatio(Real aspect) = 0;
		virtual void setFilmSize(Dir2Dr s) = 0;
		virtual void setFoVbounds(pl::box<Point2Dr> r) = 0;
		[[nodiscard]] virtual pl::box<Point2Dr> getFoVbounds() const noexcept = 0;
		[[nodiscard]] virtual Dir2Dr getFilmSize() const noexcept = 0;
		[[nodiscard]] virtual Lobe<> getCameraViewLobe() const noexcept = 0;
		[[nodiscard]] virtual Real getAspectRatio() const noexcept = 0;

		virtual void setFocus(Real dist) = 0;
		virtual void setFocusPos(VecP pos) = 0;
		virtual Ray getFocusDir(Point2Dr pos) const noexcept = 0;
		[[nodiscard]] virtual Real getFocus() const noexcept = 0;

		virtual void update() = 0;
	};

	class PHOTORT_API CameraBase : public ICamera, public RenderEntity
	{
	public:

		EntityPtr<const ITransform> projectionTransform;

		[[nodiscard]] virtual bool isInfinite() const noexcept override { return false; }
		[[nodiscard]] virtual bool hasArea() const noexcept override { return false; }

		[[nodiscard]] virtual const SubstanceStack &getSubstanceStack() const noexcept override { return mSubstanceStack; }
		[[nodiscard]] virtual SubstanceStack &getSubstanceStack() noexcept override { return mSubstanceStack; }

		virtual void setAspectRatio(Real aspect) override { mAspectRatio = aspect; }

		virtual void setFilmSize(Dir2Dr s) override
		{
			mFilmSize = s;

			mImageArea = s /osdot;
			mImageAreaInv = 1 / mImageArea;
		}
		[[nodiscard]] virtual Dir2Dr getFilmSize() const noexcept override { return mFilmSize; }

		virtual void setFoVbounds(pl::box<Point2Dr> r) override { mFovBounds = r; }
		[[nodiscard]] virtual pl::box<Point2Dr> getFoVbounds() const noexcept override { return mFovBounds; }

		[[nodiscard]] virtual VecD worldToLocal(VecD v) const noexcept override { return mMatrix.worldToCam * v; }
		[[nodiscard]] virtual VecD localToWorld(VecD v) const noexcept override { return mMatrix.world * v; }
		[[nodiscard]] virtual VecP worldToLocal(VecP v) const noexcept override { return mMatrix.worldToCam * v; }
		[[nodiscard]] virtual VecP localToWorld(VecP v) const noexcept override { return mMatrix.world * v; }

		[[nodiscard]] virtual Real getAspectRatio() const noexcept override { return mAspectRatio; }

		[[nodiscard]] virtual Lobe<> getCameraViewLobe() const noexcept override { return Lobe<>(VecD(0, 0, -1, 0)); }

		virtual void setFocus(Real /*dist*/) override {}
		virtual void setFocusPos(VecP /*pos*/) override {}
		[[nodiscard]] virtual Real getFocus() const noexcept override { return 0; }

		virtual void setFlat(bool f) { mFlat = f; }
		[[nodiscard]] virtual bool isFlat() const noexcept { return mFlat; }

		virtual void setDiracDelta(bool v) { mDiracDelta = v; }
		[[nodiscard]] virtual bool isDiracDelta() const noexcept override { return mDiracDelta; }

		virtual pl::marked_optional<CameraIntersection> intersectExt(const VecP /*origin*/, const VecD /*dir*/) const noexcept override
			{ return pl::nullopt; }
		[[nodiscard]] virtual bool intersect(const VecP /*origin*/, const VecD /*dir*/) const noexcept override { return false; }

		virtual void update() override;

		[[nodiscard]] virtual VecP getMiddle() const noexcept override { return pl::tag::identity; }

		[[nodiscard]] virtual Color averagePower() const noexcept override { return Black; }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

	protected:

		SubstanceStack mSubstanceStack;

		// Return origin / direction / image point
		std::tuple<VecP, VecD, VecP> projectPoint(const VecP &point) const noexcept;

		// Return origin / direction
		std::pair<VecP, VecD> rasterToRay(const Point2Dr &pos) const noexcept;

		struct CameraMatrix
		{
			Matrix worldToCam, world, cameraToScreen, screenToCamera,
				screenToRaster, rasterToScreen, rasterToCamera, cameraToRaster,
				worldToRaster, rasterToWorld;
		};

		CameraMatrix mMatrix{};

		Real mFieldOfView = 60.0 / 360.0 * std::numbers::pi_v<Real> * 2.0, mAspectRatio = 1;
		Dir2Dr mFilmSize{};
		Real mImageArea = 0, mImageAreaInv = 0, mCameraArea = 0, mCameraAreaInv = 0;

		pl::box<Point2Dr> mFovBounds{};

		bool mFlat = true, mDiracDelta = false;
	};



	class ICameraController
	{
	public:
		virtual ~ICameraController() = default;

		[[nodiscard]] virtual const VecP &getLookAt() const noexcept = 0;
		[[nodiscard]] virtual const VecP &getUp() const noexcept = 0;
		[[nodiscard]] virtual const VecP &getPosition() const noexcept = 0;
		[[nodiscard]] virtual const EntityPtr<ITransform> &getTransform() const noexcept = 0;

		[[nodiscard]] virtual std::span<const EntityPtr<ICamera>> getCameras() const noexcept = 0;

		[[nodiscard]] virtual VecP &getLookAt() noexcept = 0;
		[[nodiscard]] virtual VecP &getUp() noexcept = 0;

		virtual void setTransform(EntityPtr<ITransform> tr) = 0;

		virtual void setFocus(Real dist) = 0;
		virtual void setFocusPos(VecP pos) = 0;
		virtual Ray getFocusDir(Point2Dr pos) const noexcept = 0;
		[[nodiscard]] virtual Real getFocus() const noexcept = 0;
		virtual void setAspectRatio(Real aspect) = 0;
		virtual void setFilmSize(Dir2Dr s) = 0;
		[[nodiscard]] virtual Dir2Dr getFilmSize() const noexcept = 0;
		[[nodiscard]] virtual Lobe<> getCameraViewLobe() const noexcept = 0;
		[[nodiscard]] virtual Real getAspectRatio() const noexcept = 0;

		virtual void update() = 0;
	};

	class PHOTORT_API CameraControllerBase : public RenderEntity, public ICameraController
	{
	public:

		[[nodiscard]] virtual const VecP &getLookAt() const noexcept override { return mLookAt; }
		[[nodiscard]] virtual const VecP &getUp() const noexcept override { return mUp; }
		[[nodiscard]] virtual const VecP &getPosition() const noexcept override { return mwPosition; }
		[[nodiscard]] virtual const EntityPtr<ITransform> &getTransform() const noexcept override { return mTransform; }

		[[nodiscard]] virtual std::span<const EntityPtr<ICamera>> getCameras() const noexcept override { return mCameras; }

		[[nodiscard]] virtual VecP &getLookAt() noexcept override { return mLookAt; }
		[[nodiscard]] virtual VecP &getUp() noexcept override { return mUp; }

		virtual void setTransform(EntityPtr<ITransform> tr) override { mTransform = std::move(tr); }

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

		virtual void update() override;

	protected:
		VecP mUp{0, 1, 0, 1}, mLookAt{0, 0, 1, 1};
		EntityPtr<ITransform> mTransform;

		std::vector<EntityPtr<ICamera>> mCameras;

		VecP mwPosition{pl::tag::identity}, mwUp{pl::tag::identity}, mwLookAt{pl::tag::identity};
	};


}
