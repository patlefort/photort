/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Texture_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"
#include "SurfaceMapper.hpp"
#include "Bitmap.hpp"
#include "Transform.hpp"
#include <PhotoRT/Bitmap/BitmapAlgo.hpp>
#include "Interaction.hpp"

namespace PhotoRT
{
	class ColorDistributionSimple : public virtual IColorDistribution1D, public virtual IColorDistribution2D, public virtual IColorDistribution3D
	{
	public:
		ColorDistributionSimple() = default;
		ColorDistributionSimple(Color col) : mColor(col) {}

		[[nodiscard]] virtual Color operator() (Real) const noexcept final
		{
			return mColor;
		}

		[[nodiscard]] virtual Color operator() (Real, Real) const noexcept final
		{
			return mColor;
		}

		[[nodiscard]] virtual Color operator() (Real, Real, Real) const noexcept final
		{
			return mColor;
		}

		void setColor(Color c) noexcept { mColor = c; }
		[[nodiscard]] Color getColor() const noexcept { return mColor; }

	protected:
		Color mColor {0, 1};

	};

	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("colordist-value", AugmentedColorDistributionSimple, ColorDistributionSimple)

	class ColorDistributionBase : public RenderEntity
	{
	public:

		void setPower(Color p) noexcept { mPower = p; }
		[[nodiscard]] Color getPower() const noexcept { return mPower; }

		virtual void setParameters(const ParameterTree &params) override
		{
			RenderEntity::setParameters(params);

			params.getValueIfExistsBind("power", PL_LAMBDA_FORWARD_THIS(setPower));

		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {RenderEntity::getParameters(deep)};

			params["power"] = mPower;

			return params;
		}

	protected:
		Color mPower{1};

	};

	template <typename BitmapType_>
	class ColorDistributionBitmapBase : public ColorDistributionBase
	{
	public:
		void setTiled(bool tiled) noexcept { mTiled = tiled; }
		[[nodiscard]] bool isTiled() const noexcept { return mTiled; }

		void setLinearFilter(bool filter) noexcept { mLinearFilter = filter; }
		[[nodiscard]] bool isLinearFilter() const noexcept { return mLinearFilter; }

		void setBitmap(EntityPtr<const BitmapType_> map) { mBitmap = std::move(map); }
		[[nodiscard]] const auto &getBitmap() const noexcept { return mBitmap; }

		virtual void setParameters(const ParameterTree &params) override
		{
			ColorDistributionBase::setParameters(params);

			setBitmap(addParamReference(type_v<BitmapType_>, params, "bitmap"));

			params.getValueIfExistsBind("tiled", PL_LAMBDA_FORWARD_THIS(setTiled));
			params.getValueIfExistsBind("linear_filter", PL_LAMBDA_FORWARD_THIS(setLinearFilter));
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {ColorDistributionBase::getParameters(deep)};

			if(mBitmap)
				params["bitmap"].fromEntityRef(*mBitmap, deep);

			params["tiled"] = mTiled;
			params["linear_filter"] = mLinearFilter;

			return params;
		}

	protected:
		EntityPtr<const BitmapType_> mBitmap;
		bool mTiled = true, mLinearFilter = true;
	};

	class ColorDistributionBitmap1D : public ColorDistributionBitmapBase<IBitmap1DRead>, public virtual IColorDistribution1D
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("colordist-bitmap1d", ColorDistributionBitmap1D)

	protected:
		[[nodiscard]] virtual Color operator() (Real u) const noexcept override
		{
			if(!mBitmap || !mBitmap->getNbPixels())
				return Black;

			Color res;
			if(mLinearFilter)
			{
				res = BitmapAlgo::getPixelLinearFilter(*mBitmap, u, mTiled);
			}
			else
			{
				if(!mTiled && (u >= 1 || u < 0))
					return Black;

				u -= u /ofloor;

				const auto size = mBitmap->getNbPixels();
				if32 imageX = if32(u * size) % size;

				res = mBitmap->getPixel(imageX);
			}

			return res * mPower;
		}
	};



	class ColorDistributionBitmap2D : public ColorDistributionBitmapBase<IBitmap2DRead>, public virtual IColorDistribution2D
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("colordist-bitmap2d", ColorDistributionBitmap2D)

	protected:

		[[nodiscard]] virtual Color operator() (Real u, Real v) const noexcept override
		{
			if(!mBitmap || !mBitmap->getNbPixels())
				return Black;

			const auto res = std::invoke([&]{
					if(mLinearFilter)
						return BitmapAlgo::getPixelLinearFilter(*mBitmap, u, v, mTiled);

					if(!mTiled && (u >= 1 || u < 0 || v >= 1 || v < 0))
						return Black;

					u -= u /ofloor;
					v -= v /ofloor;

					const Point2Di size { mBitmap->getSize() };
					const auto p = Point2Di(Point2Dr{u, v} * size) % size;

					return mBitmap->getPixel(p[size_v<0>] % size[0], p[size_v<1>] % size[1]);
				});

			return res * mPower;
		}

	};

	class ColorDistributionBitmap3D : public ColorDistributionBitmapBase<IBitmap3DRead>, public virtual IColorDistribution3D
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("colordist-bitmap3d", ColorDistributionBitmap3D)

	protected:

		[[nodiscard]] virtual Color operator() (Real u, Real v, Real w) const noexcept override
		{
			if(!mBitmap || !mBitmap->getNbPixels())
				return Black;

			if(!mTiled && (u >= 1 || u < 0 || v >= 1 || v < 0 || w >= 1 || w < 0))
				return Black;

			u -= u /ofloor;
			v -= v /ofloor;
			w -= w /ofloor;

			const auto size = mBitmap->getSize();
			const auto p = Point3Di(Point3Dr{u, v, w} * size) % size;

			return mBitmap->getPixel(p[size_v<0>] % size[0], p[size_v<1>] % size[1], p[size_v<2>] % size[2]) * mPower;
		}

	};

	template <typename DistType_>
	class ColorDistributionChainBase : public RenderEntity, public virtual DistType_
	{
	protected:
		EntityPtr<const DistType_> mMapper;

	public:

		ColorDistributionChainBase() = default;
		ColorDistributionChainBase(EntityPtr<const DistType_> mapper) : mMapper(std::move(mapper)) {}

		virtual ~ColorDistributionChainBase() = default;

		void setMapper(EntityPtr<const DistType_> map) { mMapper = std::move(map); }
		[[nodiscard]] const auto &getMapper() const noexcept { return mMapper; }

		[[nodiscard]] virtual Color operator() (Real u, Real v) const noexcept override
		{
			return (*mMapper)(u, v);
		}

		virtual void setParameters(const ParameterTree &params) override
		{
			RenderEntity::setParameters(params);

			setMapper(addParamReference(type_v<DistType_>, params, "map"));
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {RenderEntity::getParameters(deep)};

			if(mMapper)
				params["map"].fromEntityRef(*mMapper, deep);

			return params;
		}
	};

	template <template<typename, typename> class DistType_>
	class ColorDistributionMultiChannelBase : public ColorDistributionBase
	{
	public:
		ColorDistributionMultiChannelBase() = default;
		ColorDistributionMultiChannelBase(
			EntityPtr<const DistType_<Real, Real>> mapRed,
			EntityPtr<const DistType_<Real, Real>> mapGreen = nullptr,
			EntityPtr<const DistType_<Real, Real>> mapBlue = nullptr,
			EntityPtr<const DistType_<Real, Real>> mapAlpha = nullptr
		) noexcept
		{
			mDistributionMapper[0] = std::move(mapRed);
			mDistributionMapper[1] = std::move(mapGreen);
			mDistributionMapper[2] = std::move(mapBlue);
			mDistributionMapper[3] = std::move(mapAlpha);
		}

		std::array<EntityPtr<const DistType_<Real, Real>>, 4> mDistributionMapper;

		virtual void setParameters(const ParameterTree &params) override
		{
			ColorDistributionBase::setParameters(params);

			addParamReference(mDistributionMapper[0], params, "mapr");
			addParamReference(mDistributionMapper[1], params, "mapg");
			addParamReference(mDistributionMapper[2], params, "mapb");
			addParamReference(mDistributionMapper[3], params, "mapa");
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {ColorDistributionBase::getParameters(deep)};

			if(mDistributionMapper[0])
				params["mapr"].fromEntityRef(*mDistributionMapper[0], deep);
			if(mDistributionMapper[1])
				params["mapg"].fromEntityRef(*mDistributionMapper[1], deep);
			if(mDistributionMapper[2])
				params["mapb"].fromEntityRef(*mDistributionMapper[2], deep);
			if(mDistributionMapper[3])
				params["mapa"].fromEntityRef(*mDistributionMapper[3], deep);

			return params;
		}
	};

	template <template<typename, typename> class DistType_>
	class ColorDistributionSingleChannelBase : public ColorDistributionBase
	{
	public:

		ColorDistributionSingleChannelBase() = default;
		ColorDistributionSingleChannelBase(EntityPtr<const DistType_<Real, Real>> map) noexcept
			: mDistributionMapper{std::move(map)} {}

		EntityPtr<const DistType_<Real, Real>> mDistributionMapper;

		virtual void setParameters(const ParameterTree &params) override
		{
			ColorDistributionBase::setParameters(params);

			addParamReference(mDistributionMapper, params, "map");
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {ColorDistributionBase::getParameters(deep)};

			if(mDistributionMapper)
				params["map"].fromEntityRef(*mDistributionMapper, deep);

			return params;
		}

	};

	class ColorDistributionMultiChannel1D : public ColorDistributionMultiChannelBase<IDistributionMapper>, public virtual IColorDistribution1D
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("colordist-dist1d_multichannel", ColorDistributionMultiChannel1D)
		using ColorDistributionMultiChannelBase<IDistributionMapper>::ColorDistributionMultiChannelBase;

	protected:

		[[nodiscard]] virtual Color operator() (Real u) const noexcept override
		{
			Color::aligned_array_type ar;

			for(if32 i=0; i<4; ++i)
				ar()[i] = mDistributionMapper[i] ? (*mDistributionMapper[i])(u) : 0;

			return Color{ar} * mPower;
		}
	};

	class ColorDistributionSingleChannel1D : public ColorDistributionSingleChannelBase<IDistributionMapper>, public virtual IColorDistribution1D
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("colordist-dist1d_singlechannel", ColorDistributionSingleChannel1D)
		using ColorDistributionSingleChannelBase<IDistributionMapper>::ColorDistributionSingleChannelBase;

	protected:

		[[nodiscard]] virtual Color operator() (Real u) const noexcept override
		{
			return mDistributionMapper ? (*mDistributionMapper)(u) * mPower : Black;
		}
	};

	class ColorDistributionMultiChannel2D : public ColorDistributionMultiChannelBase<IDistributionMapper2D>, public virtual IColorDistribution2D
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("colordist-dist2d_multichannel", ColorDistributionMultiChannel2D)
		using ColorDistributionMultiChannelBase<IDistributionMapper2D>::ColorDistributionMultiChannelBase;

	protected:

		[[nodiscard]] virtual Color operator() (Real u, Real v) const noexcept override
		{
			Color::aligned_array_type ar;

			for(if32 i=0; i<4; ++i)
				ar()[i] = mDistributionMapper[i] ? (*mDistributionMapper[i])(u, v) : 0;

			return Color{ar} * mPower;
		}
	};


	class ColorDistributionSingleChannel2D : public ColorDistributionSingleChannelBase<IDistributionMapper2D>, public virtual IColorDistribution2D
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("colordist-dist2d_singlechannel", ColorDistributionSingleChannel2D)
		using ColorDistributionSingleChannelBase<IDistributionMapper2D>::ColorDistributionSingleChannelBase;

	protected:

		[[nodiscard]] virtual Color operator() (Real u, Real v) const noexcept override
		{
			return mDistributionMapper ? (*mDistributionMapper)(u, v) * mPower : Black;
		}
	};

	class ColorDistributionAlphaBlend : public virtual IDistributionMapper2D<Color, Color>
	{
	public:

		[[nodiscard]] virtual Color operator() (Color c1, Color c2) const noexcept override
		{
			return blendColorsAlpha(c2, c1);
		}
	};


	class ColorDistributionMultiChannel3D : public ColorDistributionMultiChannelBase<IDistributionMapper3D>, public virtual IColorDistribution3D
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("colordist-dist3d_multichannel", ColorDistributionMultiChannel3D)

	protected:

		[[nodiscard]] virtual Color operator() (Real u, Real v, Real w) const noexcept override
		{
			Color::aligned_array_type ar;

			for(if32 i=0; i<4; ++i)
				ar()[i] = mDistributionMapper[i] ? (*mDistributionMapper[i])(u, v, w) : 0;

			return Color{ar} * mPower;
		}
	};

	class ColorDistributionSingleChannel3D : public ColorDistributionSingleChannelBase<IDistributionMapper3D>, public virtual IColorDistribution3D
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("colordist-dist3d_singlechannel", ColorDistributionSingleChannel3D)

	protected:

		[[nodiscard]] virtual Color operator() (Real u, Real v, Real w) const noexcept override
		{
			return mDistributionMapper ? (*mDistributionMapper)(u, v, w) * mPower : Black;
		}
	};


	class MapperRealValue : public virtual IColorMapper, public virtual IRealMapper
	{
	public:
		Real mValue;

		[[nodiscard]] virtual Color map(const MaterialInteraction &, const VecD /*odir*/, type_c<Color> = {}) const noexcept override
		{
			return mValue;
		}

		[[nodiscard]] virtual Real map(const MaterialInteraction &, const VecD /*odir*/, type_c<Real> = {}) const noexcept override
		{
			return mValue;
		}
	};

	class MapperColorSimple : public virtual IColorMapper, public virtual IRealMapper
	{
	public:
		MapperColorSimple() = default;
		MapperColorSimple(Color col) noexcept : mColor(col) {}

		virtual ~MapperColorSimple() = default;

		[[nodiscard]] virtual Color map(const MaterialInteraction &, const VecD /*odir*/, type_c<Color> = {}) const noexcept override
		{
			return mColor;
		}

		[[nodiscard]] virtual Real map(const MaterialInteraction &, const VecD /*odir*/, type_c<Real> = {}) const noexcept override
		{
			return mColor.scalar();
		}

		void setColor(Color c) noexcept { mColor = c; }
		[[nodiscard]] Color getColor() const noexcept { return mColor; }

	protected:
		Color mColor {0, 1};

	};

	class MapperUVTransformBase : public RenderEntity, public virtual IUVMapper
	{
	public:

		void setTransform(EntityPtr<const ITransform> tr);

		[[nodiscard]] const auto &getTransform() const noexcept { return mTransform; }

		[[nodiscard]] bool isTiled() const noexcept { return mTiled; }
		void setTiled(bool t) noexcept { mTiled = t; }

		virtual void setParameters(const ParameterTree &params) override
		{
			RenderEntity::setParameters(params);

			setTransform(addParamReference(type_v<const ITransform>, params, "transform"));
			params.getValueIfExistsBind("tiled", PL_LAMBDA_FORWARD_THIS(setTiled));
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {RenderEntity::getParameters(deep)};

			if(mTransform)
				params["transform"].fromEntityRef(*mTransform, deep);

			params["tiled"] = mTiled;

			return params;
		}

	protected:
		Matrix mMat{pl::tag::identity}, mMatTransposed{pl::tag::identity}, mInvMat{pl::tag::identity};
		EntityPtr<const ITransform> mTransform;
		bool mTiled = false;

		[[nodiscard]] VecP doTiling(const VecP uv) const noexcept
			{ return mTiled ? uv - VecD{uv /ofloor} : uv; }
	};

	class MapperUVPlanar : public MapperUVTransformBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("uvmap-planar", MapperUVPlanar)

		[[nodiscard]] virtual Matrix map(const MaterialInteraction &inter, const VecD /*odir*/, type_c<Matrix> = {}) const noexcept override
		{
			Matrix res = mMatTransposed;
			res[3] = doTiling(mInvMat * pl::visit_one(inter, [&](const auto &i){ return i.localPos(); }));

			return pl::assert_valid(res.transpose());
		}
	};

	class MapperUVPlanarWorld : public MapperUVTransformBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("uvmap-planar_world", MapperUVPlanarWorld)

		[[nodiscard]] virtual Matrix map(const MaterialInteraction &inter, const VecD /*odir*/, type_c<Matrix> = {}) const noexcept override
		{
			Matrix res = mMatTransposed;
			res[3] = doTiling(mInvMat * pl::visit_one(inter, [&](const auto &i){ return i.worldPos(); }));

			return pl::assert_valid(res.transpose());
		}
	};

	class MapperUVPanoramic : public MapperUVTransformBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("uvmap-panoramic", MapperUVPanoramic)

		[[nodiscard]] virtual Matrix map(const MaterialInteraction &inter, const VecD /*odir*/, type_c<Matrix> = {}) const noexcept override
		{
			return pl::visit_one(inter, pl::overload_set
				{
					[&](const SurfaceInteraction &i)
					{
						auto n = i.surface().shadingNormal;
						if(mTransform)
							n = mMat * n;

						const auto uv = *CoordinatePosition{tag::cartesian, n}.to(tag::polar);

						Matrix res;
						std::tie(res[0], res[1]) = lobeDirs(n);

						res[2].set_zero();
						res[3] = {uv[size_v<0>], uv[size_v<1>], 0, 1};

						return pl::assert_valid(res.transpose());
					},
					[&](const auto &)
					{
						return Matrix{pl::tag::identity};
					}
				});
		}

	};

	class MapperUVSpherical : public MapperUVTransformBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("uvmap-spherical", MapperUVSpherical)

		[[nodiscard]] virtual Matrix map(const MaterialInteraction &inter, const VecD /*odir*/, type_c<Matrix> = {}) const noexcept override
		{
			auto dir = (pl::visit_one(inter, [&](const auto &i){ return i.localPos(); }) - mCenter).as_dir() /oprtnormalize;
			if(mTransform)
				dir = mMat * dir;

			const auto uv = *CoordinatePosition{tag::cartesian, dir}.to(tag::polar);

			Matrix res;
			std::tie(res[0], res[1]) = lobeDirs(dir);

			res[2].set_zero();
			res[3] = {uv[size_v<0>], uv[size_v<1>], 0, 1};

			return pl::assert_valid(res.transpose());
		}

		[[nodiscard]] VecP getCenter() const noexcept { return mCenter; }
		void setCenter(VecP c) noexcept { mCenter = c; }

		virtual void setParameters(const ParameterTree &params) override
		{
			MapperUVTransformBase::setParameters(params);

			params.getValueIfExistsBind("center", PL_LAMBDA_FORWARD_THIS(setCenter));
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {MapperUVTransformBase::getParameters(deep)};

			params["center"] = mCenter;

			return params;
		}

	protected:
		VecP mCenter{pl::tag::identity};

	};

	class MapperUVPanoramicView : public RenderEntity, public virtual IUVMapper
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("uvmap-panoramic_view", MapperUVPanoramicView)

		[[nodiscard]] virtual Matrix map(const MaterialInteraction &, const VecD odir, type_c<Matrix> = {}) const noexcept override
		{
			const auto uv = *CoordinatePosition{tag::cartesian, odir}.to(tag::polar);

			Matrix res;
			std::tie(res[0], res[1]) = lobeDirs(odir);

			res[2].set_zero();
			res[3] = {uv[size_v<0>], uv[size_v<1>], 0, 1};

			return pl::assert_valid(res.transpose());
		}

	};

	class MapperUV3D : public MapperUVTransformBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("uvmap-3d", MapperUV3D)

		[[nodiscard]] virtual Matrix map(const MaterialInteraction &inter, const VecD /*odir*/, type_c<Matrix> = {}) const noexcept override
		{
			Matrix res = TransformTranslation{ pl::visit_one(inter, [&](const auto &i){ return i.localPos(); }) }();

			if(mTransform)
				res *= mMat;

			if(mTiled)
			{
				const pl::scoped_transpose sg{res};
				res[3] = doTiling(res[3]);
			}

			return pl::assert_valid(res);
		}

	};

	class MapperUV3Dworld : public MapperUVTransformBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("uvmap-3d_world", MapperUV3Dworld)

		[[nodiscard]] virtual Matrix map(const MaterialInteraction &inter, const VecD /*odir*/, type_c<Matrix> = {}) const noexcept override
		{
			Matrix res = TransformTranslation(pl::visit_one(inter, [&](const auto &i){ return i.worldPos(); }))();

			if(mTransform)
				res *= mMat;

			if(mTiled)
			{
				const pl::scoped_transpose sg{res};
				res[3] = doTiling(res[3]);
			}

			return pl::assert_valid(res);
		}

	};

	class MapperUVTextureCoord : public MapperUVTransformBase
	{
	public:
		PRT_ENTITY_FACTORY_DECLARE("uvmap-texcoord", MapperUVTextureCoord)

		[[nodiscard]] virtual Matrix map(const MaterialInteraction &inter, const VecD /*odir*/, type_c<Matrix> = {}) const noexcept override;

		[[nodiscard]] i32 getUVIndex() const noexcept { return mUvIndex; }
		void setUVIndex(i32 index) noexcept { mUvIndex = index; }

		virtual void setParameters(const ParameterTree &params) override
		{
			MapperUVTransformBase::setParameters(params);

			params.getValueIfExistsBind("uvindex", PL_LAMBDA_FORWARD_THIS(setUVIndex));
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {MapperUVTransformBase::getParameters(deep)};

			params["uvindex"] = mUvIndex;

			return params;
		}

	protected:
		i32 mUvIndex = -1;

	};

	template <typename MapperType_>
	class SurfaceMapperUV : public RenderEntity, public virtual ISurfaceMapper<MapperType_>
	{
	public:

		EntityPtr<const IUVMapper> mUvMap;

		SurfaceMapperUV() = default;
		SurfaceMapperUV(EntityPtr<const IUVMapper> uv) noexcept :
			mUvMap(std::move(uv))
		{}

		virtual void setParameters(const ParameterTree &params) override
		{
			RenderEntity::setParameters(params);

			addParamReference(mUvMap, params, "uvmap");
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {RenderEntity::getParameters(deep)};

			if(mUvMap)
				params["uvmap"].fromEntityRef(*mUvMap, deep);

			return params;
		}

	protected:

		[[nodiscard]] Matrix getTangentSpace(const SurfaceInteraction &inter, const VecD odir) const noexcept
		{
			return pl::assert_valid(
					Matrix{mUvMap ? mUvMap->map(inter, odir) : TransformTranslation(inter.surface().uv)()}
				);
		}

	};

	template <typename MapperType_, typename DistType_>
	class SurfaceMapperDist : public SurfaceMapperUV<MapperType_>
	{
	public:
		SurfaceMapperDist() = default;
		SurfaceMapperDist(EntityPtr<const DistType_> dist, EntityPtr<const IUVMapper> uv = {}) noexcept :
			SurfaceMapperUV<MapperType_>(std::move(uv)), mDist{std::move(dist)} {}

		EntityPtr<const DistType_> mDist;

		virtual void setParameters(const ParameterTree &params) override
		{
			SurfaceMapperUV<MapperType_>::setParameters(params);

			this->addParamReference(mDist, params, "dist");
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {SurfaceMapperUV<MapperType_>::getParameters(deep)};

			if(mDist)
				params["dist"].fromEntityRef(*mDist, deep);

			return params;
		}

	};

	template <typename MapperType_, typename DistType_>
	class SurfaceMapper2D : public SurfaceMapperDist<MapperType_, DistType_>
	{
	public:
		PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE_INCLASS(SurfaceMapper2D<MapperType_, DistType_>)

		[[nodiscard]] virtual MapperType_ map(const MaterialInteraction &inter, const VecD odir, type_c<MapperType_> = {}) const noexcept override
		{
			if(!this->mDist)
				return {};

			return pl::visit_one(inter, pl::overload_set
				{
					[&](const SurfaceInteraction &i)
					{
						const auto tgSpace = this->getTangentSpace(i, odir);
						const auto p = tgSpace * VecP{pl::tag::identity};

						using InType = typename pl::remove_cvref_t<decltype(*this->mDist)>::InType;

						return (MapperType_)(*this->mDist)(
								(InType)p[size_v<0>],
								(InType)p[size_v<1>]
							);
					},
					[&](const auto &)
					{
						return MapperType_{};
					}
				});
		}

	};

	template <typename MapperType_, typename DistType_>
	class SurfaceMapper3D : public SurfaceMapperDist<MapperType_, DistType_>
	{
	public:
		PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE_INCLASS(SurfaceMapper3D<MapperType_, DistType_>)

		[[nodiscard]] virtual MapperType_ map(const MaterialInteraction &inter, const VecD odir, type_c<MapperType_> = {}) const noexcept override
		{
			if(!this->mDist)
				return {};

			return pl::visit_one(inter, pl::overload_set
				{
					[&](const SurfaceInteraction &i)
					{
						const auto tgSpace = this->getTangentSpace(i, odir);
						const auto p = tgSpace * VecP{pl::tag::identity};

						using InType = typename pl::remove_cvref_t<decltype(*this->mDist)>::InType;

						return (MapperType_)(*this->mDist)(
								(InType)p[size_v<0>],
								(InType)p[size_v<1>],
								(InType)p[size_v<2>]
							);
					},
					[&](const auto &)
					{
						return MapperType_{};
					}
				});
		}

	};


	template <typename OutType_>
	class SurfaceMapperMultiBase : public RenderEntity, public virtual ISurfaceMapper<OutType_>
	{
	public:

		std::vector<EntityPtr<const ISurfaceMapper<OutType_>>> mMappers;

		auto &operator<< (EntityPtr<const ISurfaceMapper<OutType_>> mapper)
		{
			mMappers.push_back(std::move(mapper));

			return *this;
		}

		virtual void setParameters(const ParameterTree &params) override
		{
			RenderEntity::setParameters(params);

			mMappers.clear();

			if(const auto p = params.findParam("maps"); p && p->childParams)
				for(const auto &param : *p->childParams)
				{
					if(!param.second.value.empty())
						*this << addReference(type_v<ISurfaceMapper<OutType_>>, param.second);
				}
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {RenderEntity::getParameters(deep)};

			uf32 i = 0;
			for(const auto &map : mMappers)
			{
				params["maps"][i].fromEntityRef<ISurfaceMapper<OutType_>>(*map, deep);
				i++;
			}

			return params;
		}
	};

	template <typename OutType_, typename OperatorType_>
	class SurfaceMapperMulti : public SurfaceMapperMultiBase<OutType_>
	{
	public:
		PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE_INCLASS(SurfaceMapperMulti<OutType_, OperatorType_>)

		using SurfaceMapperMultiBase<OutType_>::SurfaceMapperMultiBase;
		using SurfaceMapperMultiBase<OutType_>::mMappers;

		[[nodiscard]] virtual OutType_ map(const MaterialInteraction &inter, const VecD odir, type_c<OutType_> = {}) const noexcept override
		{
			OutType_ res{};

			auto it = mMappers.begin();
			if(it != mMappers.end())
			{
				res = (*it)->map(inter, odir);
				++it;
			}

			for(; it != mMappers.end(); ++it)
				res = mOp(res, (*it)->map(inter, odir));

			return res;
		}

	protected:
		OperatorType_ mOp;
	};

	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(SurfaceMapper2D<Color, IColorDistribution2D>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(SurfaceMapper2D<Real, IDistributionMapper2D<Real, Real>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(SurfaceMapper3D<Color, IColorDistribution3D>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(SurfaceMapper3D<Real, IDistributionMapper3D<Real, Real>>)

	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(SurfaceMapperMulti<Color, std::plus<>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(SurfaceMapperMulti<Color, std::multiplies<>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(SurfaceMapperMulti<Real, std::plus<>>)
	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(SurfaceMapperMulti<Real, std::multiplies<>>)

}
