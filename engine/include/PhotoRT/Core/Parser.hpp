/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Parser_fwd.hpp"

#include "Base.hpp"
#include "Parameter.hpp"

#include <filesystem>

namespace PhotoRT
{
	class PHOTORT_API ParserException : public Exception
	{
		using Exception::Exception;

		[[nodiscard]] virtual const char *module() const noexcept override;
	};

	class IParameterParser
	{
	public:
		virtual ~IParameterParser() = default;

		[[nodiscard]] virtual ParameterTree load(const std::filesystem::path &filename) = 0;
		[[nodiscard]] virtual ParameterTree load(std::istream &is) = 0;
		[[nodiscard]] virtual ParameterTree loadFragment(std::string_view s) = 0;

		virtual void save(const ParameterTree &params, const std::filesystem::path &filename) = 0;
		virtual void save(const ParameterTree &params, std::ostream &os) = 0;
		virtual std::string save(const ParameterTree &params) = 0;
	};


	#ifdef PRT_XML_PARSER

		class PHOTORT_API ParameterParserXerces : public IParameterParser
		{
		public:

			[[nodiscard]] virtual ParameterTree load(const std::filesystem::path &filename) override;
			[[nodiscard]] virtual ParameterTree load(std::istream &is) override;
			[[nodiscard]] virtual ParameterTree loadFragment(std::string_view s) override;

			virtual void save(const ParameterTree &params, const std::filesystem::path &filename) override;
			virtual void save(const ParameterTree &params, std::ostream &os) override;
			virtual std::string save(const ParameterTree &params) override;
		};

	#endif

}
