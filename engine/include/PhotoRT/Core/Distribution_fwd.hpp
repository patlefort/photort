/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

namespace PhotoRT
{
	template <typename InType_, typename OutType_>
	class IDistributionMapper;

	template <typename InType_, typename OutType_>
	class IDistributionMapper2D;

	template <typename InType_, typename OutType_>
	class IDistributionMapper3D;

	template <typename T_>
	class DistributionMapperVirtual;

	template <typename T_>
	class DistributionMapperVirtual2D;

	template <typename T_>
	class DistributionMapperVirtual3D;

	template <typename InType_, typename OutType_>
	class DistributionMapper1Dto2D;

	template <typename InType_, typename OutType_>
	class DistributionMapper1Dto3D;

	template <typename Type_>
	class DistributionMapperMultiBase;

	template <typename InType_, typename OutType_, typename Operator_>
	class DistributionMapperMulti;

	template <typename Type_>
	class DistributionMapperMultiIO;

	template <typename InType_, typename OutType_, template<typename, typename> class DistType_, template<typename, typename> class DistTypeNext_>
	class DistributionMapperChainBase;

	template <typename InType_, typename OutType_>
	class DistributionMapperChain1D;

	template <typename InType_, typename OutType_>
	class DistributionMapperChain2D;

	template <typename InType_, typename OutType_>
	class DistributionMapperChain3D;

	template <typename InType_, typename OutType_>
	class DistributionMapperChainIO;

	template <typename InType_, typename OutType_, typename Operator_>
	class DistributionMapperAdaptor;

	template <typename InType_, typename OutType_>
	class DistributionMapperScaler1D;

	template <typename InType_, typename OutType_>
	class DistributionMapperScaler2D;

	template <typename InType_, typename OutType_>
	class DistributionMapperScaler3D;

	template <typename InType_, typename OutType_>
	class DistributionMapper2D;

	template <typename InType_, typename OutType_>
	class DistributionMapper3D;

}