
/*
	PhotoRT

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "MemorySystem_fwd.hpp"

#include "Base.hpp"
#include "RenderEntity.hpp"

#ifdef PRT_IPC
	#include <boost/interprocess/interprocess_fwd.hpp>

	#ifdef _WIN32
		#include <boost/interprocess/managed_windows_shared_memory.hpp>
	#endif
#endif

namespace PhotoRT
{
	template <typename ManagedMemoryType_>
	class MemorySystemEntity : public ManagedMemoryType_, public RenderEntity
	{
	public:
		PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE_INCLASS(MemorySystemEntity<ManagedMemoryType_>)

		virtual void setParameters(const ParameterTree &params) override
		{
			RenderEntity::setParameters(params);

			std::string instanceId;

			params.getValueIfExists("max_size", mMaxMemory);
			params.getValueIfExists("instance_id", instanceId);

			if(getMemoryOwnership())
			{
				if(instanceId.empty())
					this->init_random(mMaxMemory);
				else
					this->init(instanceId, true, mMaxMemory);
			}
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {RenderEntity::getParameters(deep)};

			params["max_size"] = mMaxMemory;
			params["instance_id"] = this->instance_id();

			return params;
		}

		[[nodiscard]] virtual szt max_size() const noexcept override { return mMaxMemory; }

	protected:
		szt mMaxMemory = 0;
	};

	PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(MemorySystemEntity<pl::HeapMemorySystem<VoidOffsetPtr>>)
	#ifdef PRT_IPC
		#ifdef _WIN32
			PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(MemorySystemEntity<pl::SharedMemorySystem<boost::interprocess::managed_windows_shared_memory>>)
		#else
			PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(MemorySystemEntity<pl::SharedMemorySystem<boost::interprocess::managed_shared_memory>>)
		#endif

		PRT_ENTITY_FACTORY_SPECIALIZE_DECLARE(MemorySystemEntity<pl::SharedMemorySystem<boost::interprocess::managed_mapped_file>>)
	#endif
}
