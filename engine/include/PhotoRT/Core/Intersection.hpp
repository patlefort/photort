/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Base.hpp"

#include <bitset>
#include <bit>
#include <optional>

namespace PhotoRT::Intersection
{
	#ifdef PL_LIB_VECTOR_SIMD

		// From http://www.flipcode.com/archives/SSE_RayBox_Intersection_Test.shtml
		template <pl::simd::CNeutralOrPointGeometricVector VT_, pl::simd::CNeutralOrDirGeometricVector DirVT_>
			requires (pl::simd::CSimdVector<DirVT_> && std::same_as<typename VT_::value_type, typename DirVT_::value_type> && pl::simd::CSameNbAxis<VT_, DirVT_>)
		[[nodiscard]] constexpr auto testAABBMinMax(
			VT_ bp1,
			VT_ bp2,
			DirVT_ dirInv,
			VT_ origin) noexcept
		{
			const auto
				infhighest = pl::constants::infhighest<DirVT_>,
				inflowest = pl::constants::inflowest<DirVT_>;

			const auto
				vec1 = static_cast<DirVT_>(bp1 - origin) * dirInv,
				vec2 = static_cast<DirVT_>(bp2 - origin) * dirInv;

			const auto
				f1a = vec1 /omin/ infhighest,
				f2a = vec2 /omin/ infhighest;

			const auto
				f1b = vec1 /omax/ inflowest,
				f2b = vec2 /omax/ inflowest;

			auto lmax{ f1a /omax/ f2a }, lmin{ f1b /omin/ f2b };

			lmax = lmax.min_scalar(lmax /orotate);
			lmin = lmin.max_scalar(lmin /orotate);

			lmax = lmax.min_scalar(lmax /omovehl/ lmax);
			lmin = lmin.max_scalar(lmin /omovehl/ lmin);

			const std::bitset<1> resMask{ std::bit_cast<std::make_unsigned_t<int>>(lmax.cmp_scalar_ge({}) & lmax.cmp_scalar_ge(lmin)) };

			return std::make_tuple(resMask, lmin.scalar(), lmax.scalar());
		}

	#endif

	template <pl::simd::CNeutralOrPointGeometricVector VT_, pl::simd::CNeutralOrDirGeometricVector DirVT_>
		requires (pl::simd::CBasicVector<DirVT_> && std::same_as<typename VT_::value_type, typename DirVT_::value_type> && pl::simd::CSameNbAxis<VT_, DirVT_>)
	[[nodiscard]] constexpr auto testAABBMinMax(
		VT_ v1,
		VT_ v2,
		DirVT_ packedDirReci,
		VT_ packedOrig) noexcept
	{
		auto
			distmin{pl::constants::inflowest<typename VT_::value_type>},
			distmax{pl::constants::infhighest<typename VT_::value_type>};

		const auto aabbAxisTest = [&]
			<pl::CNumberPack UVT_, pl::CNumberPack UDirVT_>
			(UVT_ v1, UVT_ v2, UDirVT_ packedDirReci, UVT_ packedOrig)
			{
				static_assert(std::same_as<pl::underlying_type_t<UVT_>, pl::underlying_type_t<UDirVT_>>, "Underlying type of vectors must be the same.");

				const auto
					inf{pl::constants::infhighest<UDirVT_>},
					neginf{pl::constants::inflowest<UDirVT_>};
				const auto
					vec1 = static_cast<UDirVT_>(v1 - packedOrig) * packedDirReci,
					vec2 = static_cast<UDirVT_>(v2 - packedOrig) * packedDirReci;

				distmax = (vec1 /omin/ inf) /omax/ (vec2 /omin/ inf) /omin/ distmax;
				distmin = (vec1 /omax/ neginf) /omin/ (vec2 /omax/ neginf) /omax/ distmin;
			};

		for(const auto i : mir(VT_::category::NbAxis))
			aabbAxisTest(v1[i], v2[i], packedDirReci[i], packedOrig[i]);

		const auto resMask = pl::mask( (distmax >= pl::constants::zero<typename VT_::value_type>) & (distmax >= distmin) );

		return std::make_tuple(resMask, distmin, distmax);
	}


	template <pl::simd::CNeutralOrPointGeometricVector VT_, pl::simd::CNeutralOrDirGeometricVector DirVT_>
	[[nodiscard]] constexpr auto testTriangle(
		VT_ rayOrigin,
		DirVT_ rayDir,
		VT_ p1,
		DirVT_ p2,
		DirVT_ p3) noexcept
		requires (std::same_as<typename VT_::value_type, typename DirVT_::value_type> && pl::simd::CSameNbAxis<VT_, DirVT_>)
	{
		using RT = typename VT_::value_type;

		const auto vec1 = rayDir /ocross/ p2;
		const auto vec2 = static_cast<DirVT_>(rayOrigin - p1);

		const auto det = p3 /odot/ vec1;

		auto bf = det >= constants::epsilon<RT>;

		const auto detInv = det /oprtreciprocal;

		auto v = vec1 /odot/ vec2;

		const auto vec3 = vec2 /ocross/ p3;
		v *= detInv;

		const auto dist = p2 /odot/ vec3 * detInv;

		const auto u = rayDir /odot/ vec3 * detInv;

		const auto resMask = pl::mask(
				(dist > pl::constants::zero<RT>) &
				(bf | (det <= -constants::epsilon<RT>)) &
				(v >= pl::constants::zero<RT>) &
				(v <= pl::constants::one<RT>) &
				(u >= pl::constants::zero<RT>) &
				(u + v <= pl::constants::one<RT>)
			);

		return std::make_tuple(resMask, u, v, dist, pl::mask(bf));
	}

	template <pl::simd::CNeutralOrPointGeometricVector VT_, pl::simd::CNeutralOrDirGeometricVector DirVT_>
	[[nodiscard]] constexpr auto testPlane(
		VT_ rayOrigin,
		DirVT_ rayDir,
		VT_ plOrigin,
		DirVT_ plUdir,
		DirVT_ plVdir) noexcept
		requires (std::same_as<typename VT_::value_type, typename DirVT_::value_type> && pl::simd::CSameNbAxis<VT_, DirVT_>)
	{
		using RT = typename VT_::value_type;

		auto vec1 = rayDir /ocross/ plUdir;
		const auto det = plVdir /odot/ vec1;

		const auto bf = det >= (constants::epsilon<RT>*100);

		auto vec2 = static_cast<DirVT_>(rayOrigin - plOrigin);
		const auto invDet = det /oprtreciprocal;

		const auto v = vec1 /odot/ vec2 * invDet;
		vec1 = vec2 /ocross/ plVdir;

		const auto dist = plUdir /odot/ vec1 * invDet;
		const auto u = rayDir /odot/ vec1 * invDet;

		const auto resMask = pl::mask(
				(bf | (det < -constants::epsilon<RT>*100)) &
				(dist > 0)
			);

		return std::make_tuple(resMask, u, v, dist, pl::mask(bf));
	}


	template <pl::simd::CNeutralOrPointGeometricVector VT_, pl::simd::CNeutralOrDirGeometricVector DirVT_>
	[[nodiscard]] constexpr auto testSphere(
		VT_ rayOrigin,
		DirVT_ rayDir,
		VT_ spherePos,
		const typename VT_::value_type sphereRadius2) noexcept
		requires (std::same_as<typename VT_::value_type, typename DirVT_::value_type> && pl::simd::CSameNbAxis<VT_, DirVT_>)
	{
		using RT = typename VT_::value_type;

		const auto sphereRayVec = static_cast<DirVT_>(spherePos - rayOrigin);

		const auto tca = sphereRayVec /odot/ rayDir;
		const auto d2 = sphereRayVec /odot/ sphereRayVec - tca * tca;

		const RT zero{};

		const auto dif = sphereRadius2 - d2;
		const auto difgt0 = dif >= zero;

		if(pl::mask_none(difgt0))
		{
			if constexpr(pl::simd::CVector<RT>)
				return std::make_tuple(std::bitset<RT::NbValues>{}, RT{}, std::bitset<RT::NbValues>{});
			else
				return std::make_tuple(std::bitset<1>{}, RT{}, std::bitset<1>{});
		}

		const auto thc = dif /oprtsqrt;

		const auto t0 = tca - thc;
		const auto t1 = tca + thc;
		const auto tmin = t0 /omin/ t1;
		const auto tmax = t0 /omax/ t1;

		const auto bf = tmin < zero;

		const auto dist = pl::select(tmax, tmin, bf);

		const auto distgt0 = dist >= zero;
		const auto resMask = pl::mask(
				(dist/oisnan /oandnot/ distgt0) &
				difgt0
			);

		return std::make_tuple(resMask, dist, pl::mask(bf));
	}

	template <typename T_>
	std::optional<std::pair<T_, T_>> solveSphereQuadratic(T_ a, T_ b, T_ c) noexcept
	{
		T_ t0, t1;
		T_ discr = b * b - 4 * a * c;

		if(discr < 0)
			return std::nullopt;

		if(discr == 0)
			t0 = t1 = b / a * -0.5;
		else
		{
			T_ q = (b > 0) ? (b + discr /oprtsqrt) * -0.5 : (b - discr /oprtsqrt) * -0.5;

			t0 = q / a;
			t1 = c / q;
		}

		if(t0 > t1)
			pl::adl_swap(t0, t1);

		return std::make_pair(t0, t1);
	}

	template <pl::simd::CNeutralOrPointGeometricVector VT_, pl::simd::CNeutralOrDirGeometricVector DirVT_>
	[[nodiscard]] constexpr auto testSphereQuadratic(
		VT_ rayOrigin,
		DirVT_ rayDir,
		VT_ spherePos,
		const typename VT_::value_type sphereRadius2) noexcept
		requires (std::same_as<typename VT_::value_type, typename DirVT_::value_type> && pl::simd::CSameNbAxis<VT_, DirVT_>)
	{
		using RT = typename VT_::value_type;

		const auto sphereRayVec = static_cast<DirVT_>(rayOrigin - spherePos);

		const auto a = rayDir /odot/ rayDir;
		const auto b = 2 * rayDir /odot/ sphereRayVec;
		const auto c = sphereRayVec /odot/ sphereRayVec - sphereRadius2;

		auto r = solveSphereQuadratic(a, b, c);
		if(!r)
			return std::make_tuple(false, RT{});

		auto [t0, t1] = *r;

		bool backfaceHit = false;

		if(t0 < 0)
		{
			backfaceHit = true;
			t0 = t1;
			if(t0 < 0)
				return std::make_tuple(false, RT{}, backfaceHit);
		}

		return std::make_tuple(true, t0, backfaceHit);
	}


	// The following is from http://fileadmin.cs.lth.se/cs/Personal/Tomas_Akenine-Moller/code/tribox2.txt, slighty modified.

	#define FINDMINMAX(x0,x1,x2,min,max) \
		min = max = x0;   \
		if(x1<min) min=x1;\
		if(x1>max) max=x1;\
		if(x2<min) min=x2;\
		if(x2>max) max=x2;

	template <pl::simd::CNeutralOrDirGeometricVector DirVT_>
	[[nodiscard]] bool planeBoxOverlap(const DirVT_ &normal, typename DirVT_::value_type d, const DirVT_ &maxbox) noexcept
	{
		int q;
		DirVT_ vmin{}, vmax{};

		for(q=0;q<=2;q++)
		{
			if(normal[q] > 0)
			{
				vmin.set(-maxbox[q], q);
				vmax.set(maxbox[q], q);
			}
			else
			{
				vmin.set(maxbox[q], q);
				vmax.set(-maxbox[q], q);
			}
		}

		if(normal /odot/ vmin + d > 0)
			return false;

		if(normal /odot/ vmax + d >= 0)
			return true;

		return false;
	}


	/*======================== X-tests ========================*/
	#define AXISTEST_X01(a, b, fa, fb)			   \
		p0 = a*v0[1] - b*v0[2];			       	   \
		p2 = a*v2[1] - b*v2[2];			       	   \
					if(p0<p2) {min=p0; max=p2;} else {min=p2; max=p0;} \
		rad = fa * boxhalfsize[1] + fb * boxhalfsize[2];   \
		if(min>rad || max<-rad) return 0;

	#define AXISTEST_X2(a, b, fa, fb)			   \
		p0 = a*v0[1] - b*v0[2];			           \
		p1 = a*v1[1] - b*v1[2];			       	   \
					if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} \
		rad = fa * boxhalfsize[1] + fb * boxhalfsize[2];   \
		if(min>rad || max<-rad) return 0;

	/*======================== Y-tests ========================*/
	#define AXISTEST_Y02(a, b, fa, fb)			   \
		p0 = -a*v0[0] + b*v0[2];		      	   \
		p2 = -a*v2[0] + b*v2[2];	       	       	   \
					if(p0<p2) {min=p0; max=p2;} else {min=p2; max=p0;} \
		rad = fa * boxhalfsize[0] + fb * boxhalfsize[2];   \
		if(min>rad || max<-rad) return 0;

	#define AXISTEST_Y1(a, b, fa, fb)			   \
		p0 = -a*v0[0] + b*v0[2];		      	   \
		p1 = -a*v1[0] + b*v1[2];	     	       	   \
					if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} \
		rad = fa * boxhalfsize[0] + fb * boxhalfsize[2];   \
		if(min>rad || max<-rad) return 0;

	/*======================== Z-tests ========================*/

	#define AXISTEST_Z12(a, b, fa, fb)			   \
		p1 = a*v1[0] - b*v1[1];			           \
		p2 = a*v2[0] - b*v2[1];			       	   \
					if(p2<p1) {min=p2; max=p1;} else {min=p1; max=p2;} \
		rad = fa * boxhalfsize[0] + fb * boxhalfsize[1];   \
		if(min>rad || max<-rad) return 0;

	#define AXISTEST_Z0(a, b, fa, fb)			   \
		p0 = a*v0[0] - b*v0[1];				   \
		p1 = a*v1[0] - b*v1[1];			           \
					if(p0<p1) {min=p0; max=p1;} else {min=p1; max=p0;} \
		rad = fa * boxhalfsize[0] + fb * boxhalfsize[1];   \
		if(min>rad || max<-rad) return 0;

	template <pl::simd::CNeutralOrPointGeometricVector VT_, pl::simd::CNeutralOrDirGeometricVector DirVT_>
	[[nodiscard]] bool triInCollisionWithAAbox(const VT_ &tp1, const VT_ &tp2, const VT_ &tp3, const VT_ &boxcenter, const DirVT_ &boxhalfsize) noexcept
		requires (std::same_as<typename VT_::value_type, typename DirVT_::value_type> && pl::simd::CSameNbAxis<VT_, DirVT_>)
	{

		/*    use separating axis theorem to test overlap between triangle and box */
		/*    need to test for overlap in these directions: */
		/*    1) the {x,y,z}-directions (actually, since we use the AABB of the triangle */
		/*       we do not even need to test these) */
		/*    2) normal of the triangle */
		/*    3) crossproduct(edge from tri, {x,y,z}-directin) */
		/*       this gives 3x3=9 more tests */

		using R = typename VT_::value_type;

		DirVT_ v0,v1,v2;
		//double axis[3];
		R min,max,p0,p1,p2,rad,fex,fey,fez;
		DirVT_ normal,e0,e1,e2;

		/* This is the fastest branch on Sun */
		/* move everything so that the boxcenter is in (0,0,0) */
		v0 = static_cast<DirVT_>(tp1 - boxcenter);
		v1 = static_cast<DirVT_>(tp2 - boxcenter);
		v2 = static_cast<DirVT_>(tp3 - boxcenter);

		/* compute triangle edges */
		e0 = v1 - v0;      /* tri edge 0 */
		e1 = v2 - v1;      /* tri edge 1 */
		e2 = v0 - v2;      /* tri edge 2 */

		/* Bullet 3:  */
		/*  test the 9 tests first (this was faster) */
		fex = e0[0] /oabs;
		fey = e0[1] /oabs;
		fez = e0[2] /oabs;
		AXISTEST_X01(e0[2], e0[1], fez, fey);
		AXISTEST_Y02(e0[2], e0[0], fez, fex);
		AXISTEST_Z12(e0[1], e0[0], fey, fex);

		fex = e1[0] /oabs;
		fey = e1[1] /oabs;
		fez = e1[2] /oabs;
		AXISTEST_X01(e1[2], e1[1], fez, fey);
		AXISTEST_Y02(e1[2], e1[0], fez, fex);
		AXISTEST_Z0(e1[1], e1[0], fey, fex);

		fex = e2[0] /oabs;
		fey = e2[1] /oabs;
		fez = e2[2] /oabs;
		AXISTEST_X2(e2[2], e2[1], fez, fey);
		AXISTEST_Y1(e2[2], e2[0], fez, fex);
		AXISTEST_Z12(e2[1], e2[0], fey, fex);

		/* Bullet 1: */
		/*  first test overlap in the {x,y,z}-directions */
		/*  find min, max of the triangle each direction, and test for overlap in */
		/*  that direction -- this is equivalent to testing a minimal AABB around */
		/*  the triangle against the AABB */

		/* test in X-direction */
		FINDMINMAX(v0[0],v1[0],v2[0],min,max);
		if(min>boxhalfsize[0] || max<-boxhalfsize[0]) return false;

		/* test in Y-direction */
		FINDMINMAX(v0[1],v1[1],v2[1],min,max);
		if(min>boxhalfsize[1] || max<-boxhalfsize[1]) return false;

		/* test in Z-direction */
		FINDMINMAX(v0[2],v1[2],v2[2],min,max);
		if(min>boxhalfsize[2] || max<-boxhalfsize[2]) return false;

		/* Bullet 2: */
		/*  test if the box intersects the plane of the triangle */
		/*  compute plane equation of triangle: normal*x+d=0 */
		normal = e0 /ocross/ e1;
		if(!planeBoxOverlap(normal, -(normal /odot/ v0), boxhalfsize))
			return false;

		return true;   /* box and triangle overlaps */
	}
}
