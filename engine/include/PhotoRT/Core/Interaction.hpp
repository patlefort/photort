/*
	PhotoRT

	Copyright (C) 2020 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Interaction_fwd.hpp"
#include "Instance_fwd.hpp"
#include "Medium_fwd.hpp"
#include "BxDF_fwd.hpp"

#include "Base.hpp"
#include "Distribution.hpp"
#include "SurfaceData.hpp"
#include "Primitive.hpp"
#include "Material.hpp"
#include "PrimitiveGroup.hpp"

#include <patlib/scoped.hpp>
#include <patlib/variant.hpp>

#include <variant>

namespace PhotoRT
{
	struct RadianceResult
	{
		Color color;
		CReal albedo;
		Real laPdf;
	};

	class PointInteraction
	{
	public:

		VecP mLocalPos{pl::tag::identity}, mWorldPos{pl::tag::identity};
		Real colDist = 0;
		const MaterialLayer *enteredSub{};

		constexpr void initialSurface(const MaterialLayer &insideSubstance) noexcept
		{
			enteredSub = &insideSubstance;
		}

		[[nodiscard]] const auto &worldPos() const noexcept { return mWorldPos; }
		auto &worldPos(VecP p) noexcept { mWorldPos = p; return *this; }
		[[nodiscard]] const auto &localPos() const noexcept { return mLocalPos; }
		auto &localPos(VecP p) noexcept { mLocalPos = p; return *this; }
		[[nodiscard]] const IObjectInstance *instance() const noexcept { return nullptr; }
		auto &instance(const IObjectInstance *) noexcept { return *this; }

		[[nodiscard]] constexpr bool isSafeDir(const VecD) const noexcept { return true; }
		[[nodiscard]] constexpr Real geometricTermDir(VecD) const noexcept { return 1; }
		[[nodiscard]] constexpr EnumHemisphere getHemisphere(const VecD) const noexcept { return EnumHemisphere::North; }

		[[nodiscard]] constexpr Real shadingNormalCorrection(EnumTransportMode, VecD, VecD) const noexcept
			{ return 1; }

		[[nodiscard]] VecP safeIncrement(VecP, VecP point, EnumHemisphere = EnumHemisphere::North) const noexcept
			{ return point; }

		[[nodiscard]] constexpr OptionalPDF<Real> PDF(const EnumTransportMode, const VecD, const VecD) const noexcept
			{ return pl::nullopt; }

		[[nodiscard]] Color emission(const VecD) const noexcept
			{ return Black; }

		[[nodiscard]] constexpr OptionalPDF<Real> emissionPDF(const VecD) const noexcept
			{ return pl::nullopt; }

		[[nodiscard]] RadianceResult radiance(const EnumTransportMode, const VecD, const VecD) const noexcept
			{ return {}; }

		[[nodiscard]] RadianceResult radiance(const EnumTransportMode, const VecD, const VecD, const EnumHemisphere) const noexcept
			{ return {}; }
	};

	class PHOTORT_API SurfaceInteraction
	{
	private:
		SurfaceData mSurface;
		Lobe<> mLobe;

	public:

		Real colDist = 0;
		const MaterialLayer *enteredSub{}, *nextSubstance{};

		SurfaceInteraction() = default;
		SurfaceInteraction(const SurfaceData &s, Real d = 0) :
			mSurface{s}, colDist{d} {}
		SurfaceInteraction(const SurfaceData &s, const Lobe<> &l, Real d = 0) :
			mSurface{s}, mLobe{l}, colDist{d} {}

		constexpr void initialSurface(const MaterialLayer &insideSubstance) noexcept
		{
			nextSubstance = enteredSub = &insideSubstance;
		}

		[[nodiscard]] const auto &worldPos() const noexcept { return mSurface.worldPos; }
		auto &worldPos(VecP p) noexcept { mSurface.worldPos = p; return *this; }
		[[nodiscard]] const auto &localPos() const noexcept { return mSurface.localPos; }
		auto &localPos(VecP p) noexcept { mSurface.localPos = p; return *this; }
		[[nodiscard]] const auto &instance() const noexcept { return mSurface.inst; }
		auto &instance(const IObjectInstance *p) noexcept { mSurface.inst = p; return *this; }

		[[nodiscard]] const auto &surface() const noexcept { return mSurface; }
		[[nodiscard]] auto &surface() noexcept { return mSurface; }
		[[nodiscard]] const auto &lobe() const noexcept { return mLobe; }
		[[nodiscard]] auto &lobe() noexcept { return mLobe; }

		[[nodiscard]] const auto &getMaterial() const noexcept { return *mSurface.matEntry->material; }
		[[nodiscard]] const auto &getLayer() const noexcept { return getMaterial().mLayers[mSurface.layerIndex]; }
		[[nodiscard]] const auto *getBxDF() const noexcept { return std::to_address(getLayer().getBxDF()); }
		[[nodiscard]] const auto *getEmission() const noexcept { return std::to_address(getLayer().getEmission()); }

		[[nodiscard]] bool hasBxDF() const noexcept { return (bool)getBxDF(); }
		[[nodiscard]] bool hasEmission() const noexcept { return (bool)getEmission(); }

		[[nodiscard]] bool shouldPassthrough() const noexcept;

		[[nodiscard]] constexpr bool isSafeDir(const VecD) const noexcept
		{
			return true;
			/*return
				dir /odot/ mSurface.surfaceNormal /oabs > constants::epsilon<Real> &&
				dir /odot/ mSurface.shadingNormal /oabs > constants::epsilon<Real>;*/
		}

		[[nodiscard]] Real shadingNormalCorrection(EnumTransportMode mode, VecD odir, VecD idir) const noexcept
		{
			if(mode == EnumTransportMode::Importance)
			{
				return pl::plabs(
						(odir /odot/ mSurface.shadingNormal) *
						(idir /odot/ mSurface.surfaceNormal) *
						(odir /odot/ mSurface.surfaceNormal * (idir /odot/ mSurface.shadingNormal) /oprtreciprocal)
					);
			}

			return 1;
		}

		[[nodiscard]] VecP safeIncrement(VecP origin, VecP point, EnumHemisphere hemisphere = EnumHemisphere::North) const noexcept
		{
			return PhotoRT::safeIncrement(
					point,
					hemisphere == EnumHemisphere::North ? mSurface.surfaceNormal : -mSurface.surfaceNormal,
					(point /oabs /omax/ (origin /oabs)).minimal_safe_increment() /omax/ mSurface.minimalSafeInc
				);
		}

		[[nodiscard]] Real geometricTermDir(VecD dir) const noexcept
			{ return mSurface.surfaceNormal /odot/ dir /oabs; }

		[[nodiscard]] bool isDelta() const noexcept;
		[[nodiscard]] OptionalPDF<Real> PDF(const EnumTransportMode mode, const VecD odir, const VecD idir) const noexcept;
		[[nodiscard]] Color emission(const VecD idir) const noexcept;
		[[nodiscard]] OptionalPDF<Real> emissionPDF(const VecD idir) const noexcept;
		[[nodiscard]] RadianceResult radiance(const EnumTransportMode mode, const VecD odir, const VecD idir) const noexcept;
		[[nodiscard]] RadianceResult radiance(const EnumTransportMode mode, const VecD odir, const VecD idir, const EnumHemisphere idirHemisphere) const noexcept;

		[[nodiscard]] EnumHemisphere getHemisphere(const VecD dir) const noexcept
		{
			return mLobe.getHemisphere(dir);
		}

		auto &operator *=(const Matrix &m) noexcept
		{
			mSurface.pMat *= m;
			mSurface.surfaceNormal = dirSafeTransform(mSurface.surfaceNormal, m);
			mSurface.shadingNormal = dirSafeTransform(mSurface.shadingNormal, m);
			return *this;
		}
	};

	class PHOTORT_API MediumInteraction
	{
	public:

		VecP mWorldPos{pl::tag::identity}, mLocalPos{pl::tag::identity};
		Matrix mWorldMatrix{pl::tag::identity};
		Real colDist = 0;

		const IObjectInstance *mInst{};
		const IMedium *mMedium{};
		const MaterialLayer *enteredSub{};

		constexpr void initialSurface(const MaterialLayer &insideSubstance) noexcept
		{
			enteredSub = &insideSubstance;
		}

		[[nodiscard]] const auto &worldPos() const noexcept { return mWorldPos; }
		auto &worldPos(VecP p) noexcept { mWorldPos = p; return *this; }
		[[nodiscard]] const auto &localPos() const noexcept { return mLocalPos; }
		auto &localPos(VecP p) noexcept { mLocalPos = p; return *this; }
		[[nodiscard]] const auto &instance() const noexcept { return mInst; }
		auto &instance(const IObjectInstance *p) noexcept { mInst = p; return *this; }

		[[nodiscard]] const auto &getMedium() const noexcept { return *mMedium; }

		[[nodiscard]] constexpr Real shadingNormalCorrection(EnumTransportMode, VecD, VecD) const noexcept
			{ return 1; }

		[[nodiscard]] VecP safeIncrement(VecP, VecP point, EnumHemisphere = EnumHemisphere::North) const noexcept
			{ return point; }

		[[nodiscard]] constexpr Real geometricTermDir(VecD) const noexcept
			{ return 1; }

		[[nodiscard]] bool isDelta() const noexcept;

		[[nodiscard]] constexpr bool isSafeDir(const VecD) const noexcept
			{ return true; }

		[[nodiscard]] OptionalPDF<Real> PDF(const EnumTransportMode, const VecD odir, const VecD idir) const noexcept;

		[[nodiscard]] Color emission(const VecD) const noexcept
			{ return Black; }

		[[nodiscard]] constexpr OptionalPDF<Real> emissionPDF(const VecD) const noexcept
			{ return pl::nullopt; }

		[[nodiscard]] RadianceResult radiance(const EnumTransportMode, const VecD odir, const VecD idir) const noexcept;
		[[nodiscard]] RadianceResult radiance(const EnumTransportMode, const VecD odir, const VecD idir, const EnumHemisphere) const noexcept;

		[[nodiscard]] constexpr EnumHemisphere getHemisphere(const VecD) const noexcept
			{ return EnumHemisphere::North; }
	};

	template <typename Interaction_>
	struct [[nodiscard]] InteractionReverseGuard : pl::no_copy
	{
		static_assert(pl::dependent_false<Interaction_>, "Invalid surface interaction.");
	};
	template <typename Interaction_, typename Dir_>
	InteractionReverseGuard(Interaction_ &, Dir_) -> InteractionReverseGuard<Interaction_>;

	template <>
	struct PHOTORT_API InteractionReverseGuard<SurfaceInteraction> : pl::no_copy
	{
		SurfaceInteraction &interaction;
		pl::scoped_reverse<SurfaceData, Lobe<>> rg;
		bool swapSubs = false;

		[[nodiscard]] InteractionReverseGuard(SurfaceInteraction &si, VecD dir) noexcept;

		~InteractionReverseGuard()
		{
			if(swapSubs)
				pl::adl_swap(interaction.enteredSub, interaction.nextSubstance);
		}
	};

	template <>
	struct [[nodiscard]] InteractionReverseGuard<MediumInteraction> : pl::no_copy
	{
		constexpr InteractionReverseGuard(MediumInteraction &, VecD) noexcept {}
	};

	template <>
	struct [[nodiscard]] InteractionReverseGuard<PointInteraction> : pl::no_copy
	{
		constexpr InteractionReverseGuard(PointInteraction &, VecD) noexcept {}
	};

	[[nodiscard]] inline SurfaceInteraction sampledAreaToInteraction(const SampledAreaResult<VecP> &areaSp) noexcept
	{
		return {
				{
					.shadingNormal = areaSp.shadingNormal,
					.surfaceNormal = areaSp.normal,
					.uv = areaSp.uv,
					.worldPos = areaSp.pos,
					.localPos = areaSp.pos
				},
				{areaSp.shadingNormal}
			};
	}

	[[nodiscard]] PHOTORT_API SurfaceInteraction sampledAreaToShadedInteraction(
		const SampledAreaResult<VecP> &areaSp,
		const IPrimitiveGroupProperties &props,
		const IObjectInstance &inst,
		const Matrix &worldMatrix,
		const VecP worldPos,
		const Real dist,
		const PrimitiveIndex primIndex,
		const VecD odir,
		bool backface) noexcept;
	[[nodiscard]] PHOTORT_API SurfaceInteraction sampledAreaToShadedInteraction(
		const SampledAreaResult<VecP> &areaSp,
		const IPrimitiveGroupProperties &props,
		const IObjectInstance &inst,
		const Matrix &worldMatrix,
		const VecP worldPos,
		const Real dist,
		const PrimitiveIndex primIndex,
		const VecD odir,
		bool backface,
		pl::marked_optional<const PrimitiveMaterial &> matEntry) noexcept;
	[[nodiscard]] PHOTORT_API SurfaceInteraction collisionToShadedInteraction(
		const PrimGroupCollision &collision,
		const IPrimitiveGroupProperties &props,
		const IObjectInstance &inst,
		const Matrix &worldMatrix,
		const VecD surfaceNormal,
		const VecD shadingNormal,
		const VecP localPt,
		const VecD odir,
		pl::marked_optional<const PrimitiveMaterial &> matEntry) noexcept;
	[[nodiscard]] PHOTORT_API SurfaceInteraction collisionToShadedInteraction(
		const PrimGroupCollision &collision,
		const IPrimitiveGroupProperties &props,
		const IObjectInstance &inst,
		const Matrix &worldMatrix,
		const VecD surfaceNormal,
		const VecD shadingNormal,
		const VecP localPt,
		const VecD odir) noexcept;
	[[nodiscard]] PHOTORT_API SurfaceInteraction collisionToShadedInteraction(const CollisionData &cd, VecD odir, bool transformToWorld) noexcept;

	[[nodiscard]] PHOTORT_API SurfaceInteraction shadedInteraction(const SurfaceData &sd, const Real dist, VecD odir, bool transformToWorld) noexcept;

	template <bool WorldCoord_ = true>
	void surfaceWalk(const MaterialInteraction &inter, const VecD dir, Real maxDist, Real step, Real rndStart, auto &&fc, bool_c<WorldCoord_> = {})
	{
		auto interCopy = inter;

		pl::visit_one(interCopy, [&](auto &vi)
		{
			if constexpr(WorldCoord_)
				vi.worldPos( vi.worldPos() - dir * maxDist );
			else
				vi.localPos( vi.localPos() + dir * maxDist );

			if32 i = 0;
			for(Real dist = 0; dist < maxDist; ++i)
			{
				const auto curStep = i == 0 ? step * rndStart : step;

				if(!PL_FWD(fc)(i, dist, pl::plmin(maxDist - dist, curStep), interCopy))
					break;

				if constexpr(WorldCoord_)
					vi.worldPos( vi.worldPos() + dir * curStep );
				else
					vi.localPos( vi.localPos() + dir * curStep );

				dist += curStep;
			}
		});
	}
}
