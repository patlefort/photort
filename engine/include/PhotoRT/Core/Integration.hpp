/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Base.hpp"
#include "Distribution.hpp"
#include "Sampling.hpp"
#include "BxDF.hpp"
#include "Medium.hpp"
#include "RayTracer.hpp"
#include "Camera.hpp"
#include "Material.hpp"
#include "Emitter.hpp"
#include "Scene.hpp"
#include "Film.hpp"
#include "PrimitiveGroup.hpp"
#include "Interaction.hpp"
#include "SubstanceStack.hpp"

#include <patlib/marked_optional.hpp>

#include <optional>

namespace PhotoRT
{
	struct Contribution
	{
		Color color, rrColor;
		Real rrFactor;
		szt groupIndex;
	};

	struct WeightContribution
	{
		Color weight{pl::tag::one};
		Real rrFactor = 1;
	};

	class GroupedContribution
	{
	private:
		std::vector<ValidColor<Color>> mColor, mRrColor;
		bool mBlack = true;

	public:

		[[nodiscard]] auto get(szt i) const noexcept { return mColor[i]; }
		[[nodiscard]] auto getrr(szt i) const noexcept { return mRrColor[i]; }

		void resize(szt size)
		{
			mColor.resize(size);
			mColor.shrink_to_fit();
			mRrColor.resize(size);
			mRrColor.shrink_to_fit();
		}

		[[nodiscard]] auto size() const noexcept { return mColor.size(); }

		void reset() noexcept
		{
			mColor.assign(mColor.size(), {});
			mRrColor.assign(mRrColor.size(), {});
			mBlack = true;
		}

		/*void add(const Color c, Real rrFactor, szt index) noexcept
		{
			mColor[index] += c;
			mRrColor[index] += c * rrFactor;
			mBlack &= isAlmostBlack(c);
		}

		void add(const Color c, const Color rrc, Real rrFactor, szt index) noexcept
		{
			mColor[index] += c;
			mRrColor[index] += rrc * rrFactor;
			mBlack &= isAlmostBlack(c);
		}*/

		void add(const Contribution &c) noexcept
		{
			mColor[c.groupIndex] += c.color;
			mRrColor[c.groupIndex] += c.rrColor * c.rrFactor;
			mBlack &= isAlmostBlack(c.color);
		}

		/*void assign(const Color c, Real rrFactor, szt index) noexcept
		{
			mColor[index] = c;
			mRrColor[index] = c * rrFactor;
			mBlack = isAlmostBlack(c);
		}

		void assign(const Color c, const Color rrc, Real rrFactor, szt index) noexcept
		{
			mColor[index] = c;
			mRrColor[index] = rrc * rrFactor;
			mBlack = isAlmostBlack(c);
		}*/

		void assign(const Contribution &c) noexcept
		{
			mColor[c.groupIndex] = c.color;
			mRrColor[c.groupIndex] = c.rrColor * c.rrFactor;
			mBlack = isAlmostBlack(c.color);
		}

		[[nodiscard]] ValidColor<Color> total() const noexcept
		{
			return rg::accumulate(mColor | pl::static_cast_view<Color>, Color{});
		}

		[[nodiscard]] ValidColor<Color> rrtotal() const noexcept
		{
			return rg::accumulate(mRrColor | pl::static_cast_view<Color>, Color{});
		}

		[[nodiscard]] bool isBlack() const noexcept { return mBlack; }
	};

	class Contributor
	{
	private:
		GroupedContribution *mContribution{};
		ValidColor<Color> mWeight{pl::tag::one};
		ValidWeight<CReal> mRrFactor{1};

	public:

		Contributor(GroupedContribution &cont, const WeightContribution &w = {}) :
			mContribution{&cont}, mWeight{w.weight}, mRrFactor{w.rrFactor} {}

		[[nodiscard]] auto weight() const noexcept { return mWeight; }
		[[nodiscard]] auto rrFactor() const noexcept { return mRrFactor; }

		void reset()
		{
			mWeight = pl::tag::one;
			mRrFactor = 1;
		}

		[[nodiscard]] auto scale(const WeightContribution &w) const noexcept
			{ return Contributor{*mContribution, {.weight = static_cast<Color>(mWeight) * w.weight, .rrFactor = r_(mRrFactor) * w.rrFactor}}; }

		auto &operator() (Color c, szt index) const noexcept
		{
			c *= static_cast<Color>(mWeight);

			mContribution->add({
					.color = c,
					.rrColor = c,
					.rrFactor = mRrFactor,
					.groupIndex = index
				});

			return *this;
		}

		/*void operator() (const Color c, const Color rrc, szt index) const noexcept
			{ mContribution->add(c * static_cast<Color>(mWeight), rrc * static_cast<Color>(mWeight), mRrFactor, index); }*/

		auto &operator() (const Contribution &c) const noexcept
		{
			mContribution->add({
					.color = c.color * static_cast<Color>(mWeight),
					.rrColor = c.rrColor * static_cast<Color>(mWeight),
					.rrFactor = c.rrFactor * mRrFactor,
					.groupIndex = c.groupIndex
				});

			return *this;
		}
	};

	[[nodiscard]] constexpr auto accumulateContribution(const Contributor &contributor) noexcept
	{
		return pl::for_each([&](const Contribution &c) { contributor(c); });
	}

	namespace impl
	{
		template <bool Const_>
		struct PathSegment
		{
			using PtrType = std::conditional_t<Const_, const MaterialInteraction *, MaterialInteraction *>;

			PtrType start{}, end{}, lpSurface{};
			Real pdfW = 0, pdfTr = 1, contProb = 1, infPdfTr = 1;
			EnumRayType type{};
			u32 level = 0;
			bool insideLayer = false;

			constexpr PathSegment() = default;
			constexpr PathSegment(const PathSegment &) = default;
			constexpr PathSegment(const PathSegment &seg, PtrType next) noexcept :
				start{seg.end},
				end{next},
				level{seg.level + 1},
				insideLayer{seg.insideLayer}
			{}
			constexpr PathSegment(PtrType start_, PtrType end_) noexcept :
				start{start_}, end{end_} {}
			constexpr PathSegment(PtrType start_, Real pw = 0, EnumRayType t = {}) noexcept :
				start{start_}, pdfW{pw}, type{t} {}

			constexpr void flip() noexcept
				{ pl::adl_swap(start, end); }

			constexpr void next(PtrType s) noexcept
				{ start = end; end = s; ++level; }

			constexpr void startsAt(PtrType s) noexcept
				{ start = s; end = nullptr; level = 0; }

			[[nodiscard]] constexpr EnumRayType getType() const noexcept { return type; }
			[[nodiscard]] constexpr bool isType(EnumRayType t) const noexcept { return PhotoRT::isType(*this, t); }
		};
	} // namespace impl

	using PathSegment = impl::PathSegment<false>;
	using PathSegmentConst = impl::PathSegment<true>;


	struct PathStateBasic
	{
		Color throughput{pl::tag::one};
		Real weight = 1, importance = 1, distributionFactor = 1, rrFactor = 1;
		EnumTransportMode mode{EnumTransportMode::Importance};
		u32 nbBounces = 0;
		bool mutated = false, terminated = false;
		SubstanceStack substanceStack;
	};


	using IntegrateFc = std::function<GroupedContribution(Ray &ray, PathSegment &segment, bool hasCol, if32 index)>;

	constexpr void updateSegment(const Ray &ray, PathSegment &segment, Real pdf, Real contProb, const PathSegment *prevSegment = nullptr)
	{
		if(ray.isType(EnumRayType::Transparency | EnumRayType::Passthrough))
		{
			pl::assert_valid(prevSegment);
			segment.start = prevSegment->start;
			segment.pdfTr = pdf * prevSegment->pdfTr;
			segment.level = prevSegment->level;
			segment.pdfW = prevSegment->pdfW;
			segment.contProb = contProb * prevSegment->contProb;
			segment.type = prevSegment->type;
			segment.lpSurface = prevSegment->lpSurface;
		}
		else
		{
			segment.pdfW = pdf;
			segment.contProb = contProb;
			segment.pdfTr = 1;
			segment.type = ray.type;
		}
	}

	auto traceOne(IRayTracer &rayTracer, Ray &ray, PathSegment &segment, Real pdf, Real contProb, auto&& createInteractionFc, const PathSegment *prevSegment = nullptr)
	{
		auto res = rayTracer.traceRay(ray);

		updateSegment(ray, segment, pdf, contProb, prevSegment);

		if(res)
			segment.end = &PL_FWD(createInteractionFc)(collisionToShadedInteraction(*res, ray.dir, true));

		return res;
	}


	struct TraversalEntry
	{
		PathSegment segment, prevSegment;
		Real pdf, contProb;
		Color color;
		CReal albedo;
		SubstanceEntryEvent subsEntryEvent;
	};

	using TraversalData = boost::container::static_vector<TraversalEntry, RayBatchMaxSize>;

	void traceBatch(IRayTracer &rayTracer, RayBatch &rayBatch, TraversalData &data, auto&& createInteractionFc) noexcept
	{
		/*for(auto&& [rb, d] : rgv::zip(rayBatch, data))
			traceOne(rayTracer, rb.ray, d.segment, d.pdf, d.contProb, PL_FWD(createInteractionFc), &d.prevSegment);*/

		if(rayBatch.size() == 1)
			rayBatch[0].result.collision = rayTracer.traceRay(rayBatch[0].ray);
		else
			rayTracer.traceRayBatch(rayBatch);

		for(auto&& [rb, d] : rgv::zip(rayBatch, data))
		{
			updateSegment(rb.ray, d.segment, d.pdf, d.contProb, &d.prevSegment);

			if(rb.result.collision)
				d.segment.end = &PL_FWD(createInteractionFc)(collisionToShadedInteraction(*rb.result.collision, rb.ray.dir, true));
		}
	}


	template <typename T_>
	concept CLightRange = rg::input_range<T_> && (
			std::convertible_to<pl::pointer_dereference_t<rg::range_reference_t<T_>>, const IEmitter &> ||
			std::convertible_to<rg::range_reference_t<T_>, const IEmitter &>
		);

	template <CLightRange InfLightRange_>
	[[nodiscard]] std::tuple<u32, Real, Real> infiniteLightsPDF(
		const InfLightRange_ &infLights,
		const IEmitterSampler &lightSampler,
		const IEmitter *light,
		const MaterialInteraction &inter,
		VecD idir) noexcept
	{
		u32 nb = 0;
		Real pdfW = 0, pdfS = 0;

		for(const auto &l : infLights | pl::to_ref_view)
			if(!pl::same_underlying_address<IEmitter>(&l, light) && !l.isDiracDelta())
			{
				pdfW += l.PDFW(inter, idir).value();
				pdfS += lightSampler.PDF(inter, l).value();
				++nb;
			}

		return {nb, pdfW, pdfS};
	}

	template <CLightRange InfLightRange_>
	[[nodiscard]] std::pair<u32, Real> infiniteLightsPDFW(
		const InfLightRange_ &infLights,
		const IEmitter *light,
		const MaterialInteraction &inter,
		VecD idir) noexcept
	{
		u32 nb = 0;
		Real pdfW = 0;

		for(const auto &l : infLights | pl::to_ref_view)
			if(!pl::same_underlying_address<IEmitter>(&l, light) && !l.isDiracDelta())
			{
				pdfW += l.PDFW(inter, idir).value();
				++nb;
			}

		return {nb, pdfW};
	}

	struct InfiniteLightsContributionResult
	{
		u32 nb;
		Color color;
		Real pdfW, pdfS;
	};

	template <CLightRange InfLightRange_>
	[[nodiscard]] InfiniteLightsContributionResult infiniteLightsContribution(
		const InfLightRange_ &infLights,
		const IEmitterSampler &lightSampler,
		const IEmitter *light,
		const MaterialInteraction &inter,
		VecD idir) noexcept
	{
		InfiniteLightsContributionResult res{};

		for(const auto &l : infLights | pl::to_ref_view)
		{
			if(!pl::same_underlying_address<IEmitter>(&l, light) && !l.isDiracDelta())
			{
				res.color += l.radiance(inter, idir);
				res.pdfW += l.PDFW(inter, idir).value();
				res.pdfS += lightSampler.PDF(inter, l).value();
				++res.nb;
			}
		}

		return res;
	}

	struct SampledLight
	{
		RadianceResult radiance;
		SampledSensorIn sample;
		const IEmitter *lp;
		Ray ray;
		EnumHemisphere hemisphere;

		constexpr void reset() noexcept { sample.reset(); }
		[[nodiscard]] constexpr bool has_value() const noexcept { return sample.has_value(); }
	};

	[[nodiscard]] pl::marked_optional<SampledLight> sampleLightDirect(
		RandomReals<2> rnd,
		const IEmitter &light,
		const Ray &oray,
		const MaterialInteraction &inter,
		Real lpSampleProb,
		const IEmitterSampler *lpSampler,
		const IEmitterSampler *dlSampler,
		const CLightRange auto &infLights,
		IRNGInt &rng) noexcept
	{
		const IEmitter *lp = nullptr;
		//Color weight{1};
		const bool isInfLight = light.isInfinite();

		auto sp = std::invoke([&]() -> pl::marked_optional<SampledSensorIn> {
				lpSampleProb = isInfLight && !light.isDiracDelta() && lpSampler ? lpSampleProb : 0;

				if( lpSampleProb > 0 && rng() < lpSampleProb )
				{
					const auto [slp, lpPdfS] = lpSampler->sampleOne(inter, rng);
					auto sp = slp->sampleIn(inter, rng, rnd);

					if(!sp)
						return pl::nullopt;

					const auto [nb, ipdfW, ipdfS] = infiniteLightsPDF(infLights, *dlSampler, nullptr, inter, sp->dir);
					//const auto lwPdf = (ipdfW * ipdfS) / nb;

					//sp.wPdf *= lpPdfS;

					if(ipdfW / nb > sp->pdf)
						return pl::nullopt;

					//weight = sp.wPdf;
					sp->color *= light(sp->sensor.interaction, sp->dir);// / lpSampleProb;
					sp->sensor.pdf *= lpPdfS * ipdfS / nb * lpSampleProb;

					lp = slp;

					return sp;
				}
				else
				{
					auto sp = light.sampleIn(inter, rng, rnd);
					if(!sp)
						return pl::nullopt;

					//sp.color /= 1 - lpSampleProb;

					if(isInfLight && !sp->delta)
					{
						const auto [nb, ipdfW, ipdfS] = infiniteLightsPDF(infLights, *dlSampler, &light, inter, sp->dir);
						sp->sensor.pdf = (sp->sensor.pdf + ipdfS) / (nb + 1);
						sp->pdf = (sp->pdf + ipdfW) / (nb + 1);
					}

					sp->sensor.pdf *= 1 - lpSampleProb;

					return sp;
				}
			});

		if(!sp || isAlmostBlack(sp->color))
			return pl::nullopt;

		return pl::visit_one(inter, [&](const auto &i) -> pl::marked_optional<SampledLight>
		{
			const auto hemisphere = i.getHemisphere(sp->dir);
			const auto rad = i.radiance(EnumTransportMode::Radiance, oray.dir, sp->dir);

			//auto weight = rad.color;// * (sp.wPdf /oprtreciprocal);

			if(isAlmostBlack(rad.color))
				return pl::nullopt;

			Ray ray;
			ray.setupShadow();
			ray.origin = i.safeIncrement(oray.origin, i.worldPos(), hemisphere);

			if(!isInfLight)
			{
				if(!pl::visit_one(sp->sensor.interaction, [&](const auto &li)
				{
					ray.setDestination( li.safeIncrement(ray.origin, li.worldPos(), EnumHemisphere::North) );

					if(ray.length <= constants::epsilon<Real>)
						return false;

					return true;
				}))
					return pl::nullopt;
			}
			else
			{
				ray.dir = sp->dir;
				ray.length = Ray::MaxDistance;
			}

			return SampledLight{
					.radiance = std::move(rad),
					.sample = std::move(*sp),
					.lp = lp,
					.ray = std::move(ray),
					.hemisphere = hemisphere
				};
		});
	}

	/*[[nodiscard]] std::pair<EnumHemisphere, Real> sampleLightArea(
		const SubstanceStack &substanceStack,
		RandomReals<2> rnd,
		const IEmitter &light,
		Ray &ray,
		const Ray &oray,
		PathSegment &segment,
		SurfaceData &lightSurface,
		const auto &interaction,
		IRNGInt &rng) noexcept
	{
		Real pdf = light.sampleArea(lightSurface, rng, rnd);
		if(!pdf)
			return {EnumHemisphere::Invalid, 0};

		ray.setupShadow();

		const auto dir = (lightSurface.worldPos - segment.start->worldPos).as_dir();
		const auto hemisphere = interaction.getHemisphere(dir);

		ray.origin = segment.start->safeIncrement(oray.origin, segment.start->worldPos, hemisphere);

		if(!light.isInfinite())
		{
			const auto lightHemisphere = lightSurface.getHemisphere(-dir);
			ray.setDestination( lightSurface.safeIncrement(ray.origin, lightSurface.worldPos, lightHemisphere) );

			if(ray.length <= constants::epsilon<Real> || !lightSurface.isSafeDir(ray.dir))
				return {EnumHemisphere::Invalid, 0};
		}
		else
		{
			ray.dir = dir /oprtnormalize;
			ray.length = Ray::MaxDistance;
		}

		if(!segment.start->isSafeDir(ray.dir))
			return {EnumHemisphere::Invalid, 0};

		substanceStack.checkSubstanceEntry(hemisphere, *segment.end);

		return {hemisphere, pdf};
	}*/

	struct SampledCamera
	{
		RadianceResult radiance;
		SampledImagePhotonDir sample;
		Ray ray;
		EnumHemisphere hemisphere;

		constexpr void reset() noexcept { sample.reset(); }
		[[nodiscard]] constexpr bool has_value() const noexcept { return sample.has_value(); }
	};

	[[nodiscard]] pl::marked_optional<SampledCamera> sampleCameraDirect(
		RandomReals<2> rnd,
		const ICamera &camera,
		const IEmitter &light,
		IRNGInt &rng,
		const Ray &oray,
		const auto &interaction) noexcept
	{
		auto sp = camera.sampleImagePhotonDir(interaction, rng, rnd, &light);
		if(!sp)
			return pl::nullopt;

		const auto hemisphere = interaction.getHemisphere(sp->sensorIn.dir);
		auto surfaceRad = interaction.radiance(EnumTransportMode::Importance, oray.dir, sp->sensorIn.dir);

		if(isAlmostBlack(surfaceRad.color))
			return pl::nullopt;

		Ray ray;
		ray.length = pl::visit_one(sp->sensorIn.sensor.interaction, [&](const auto &i){ return i.colDist; });

		if(ray.length <= constants::epsilon<Real>)
			return pl::nullopt;

		ray.dir = sp->sensorIn.dir;
		ray.origin = interaction.safeIncrement(oray.origin, interaction.worldPos(), hemisphere);
		ray.setupShadow();

		surfaceRad.color *= interaction.shadingNormalCorrection(EnumTransportMode::Importance, oray.dir, sp->sensorIn.dir);// * sp.wPdf;
		if(isAlmostBlack(surfaceRad.color))
			return pl::nullopt;

		return SampledCamera{
				.radiance = std::move(surfaceRad),
				.sample = std::move(*sp),
				.ray = std::move(ray),
				.hemisphere = hemisphere
			};
	}

	[[nodiscard]] pl::marked_optional<SampledBxDF> PHOTORT_API sampleLayer(
		EnumTransportMode mode,
		RandomReals<2> rnd,
		const VecD odir,
		const SurfaceInteraction &interaction,
		Sampler &sampler,
		Real importance) noexcept;

	struct ShadowRayResult
	{
		ValidColor<Color> color;
		StatCounter nbTraced;
		Real pdfTr, pdfLpForW, pdfLpA, infPdfTr;
		VecP lpLocalPos;
		const IObjectInstance *lp;
	};

	[[nodiscard]] ShadowRayResult PHOTORT_API traceShadow(
		EnumTransportMode mode,
		const Ray &shadowRay,
		const MaterialInteraction &fromInter,
		TracerParams &tracer,
		SubstanceStack &substanceStack,
		uf32 maxBounces = 100,
		std::optional<MaterialInteraction> *lpSurface = nullptr,
		bool passthroughLightPortals = true) noexcept;

	/*template <typename FC_, typename TileType_>
	uf32 sampleMutations(
		const std::vector<MutatorEntry> &mutators,
		const MaterialLayer &cameraSubstance,
		bool doJitter,
		bool sampleRandom,
		bool weightSamples,
		TileType_ &samples,
		TracerParams &tracer,
		Point2Di pos,
		const GroupedContribution &contrib,
		const FC_ &integrateFc) noexcept
	{
		Point2Dr jitter{0.5, 0.5};
		const auto &rect = samples.rect;
		const auto size = rect.dims();
		auto lastContrib = contrib;
		auto albedoLastRec = pl::reciprocal(getColorY(lastContrib.color));
		//PrimitiveIndex lastSpIndex = pos[1] * size[0] + pos[0];
		auto lastPos = pos;
		SurfaceData cameraSurface, nextSurface;
		PathSegment segment;
		const auto nbPixels = size[0] * size[1];
		uf32 nbSamples = 0;

		segment.start = &cameraSurface;
		segment.end = &nextSurface;
		segment.level = 0;
		segment.pdfTr = 1;

		nextSurface.initialSurface(cameraSubstance);
		cameraSurface.initialSurface(cameraSubstance);

		const Real mutSpWeight1 = r_(mutators.size()) /oprtreciprocal;

		if(pl::plisnan(albedoLastRec))
			albedoLastRec = 1;
			//return;

		for(auto &mutator : mutators)
		{
			const Color mutSpWeight = weightSamples ? mutSpWeight1 * nbPixels * (r_(mutator.nbSamples) /oprtreciprocal) : mutSpWeight1;

			auto mutation = tracer.sampler.beginMutation(*mutator.mutator);
			auto mutationPath = tracer.sampler.beginMutationPath();

			nbSamples += mutator.nbSamples;

			for([[maybe_unused]] auto i : mir(mutator.nbSamples))
			{
				auto mutationIteration = tracer.sampler.beginMutationIteration();

				Ray ray;

				ray.setupPrimary();

				ray.rayImportance = samples.rayImportance;

				Point2Dr camPos;
				if(sampleRandom)
					camPos = tracer.sampler.get2d() * size;
				else if(doJitter)
					camPos = tracer.sampler.get2d() + pos;
				else
					camPos = pos;

				//const PrimitiveIndex spIndex = (if32)camPos[1] * size[0] + (if32)camPos[0];

				Real posPdf, wpdf;
				ray.color = samples.camera->sampleImageDir(cameraSurface, tracer.sampler, 0, tracer.sampler.get2d(), camPos + rect.p1, ray.dir, posPdf, wpdf);
				ray.pdf = posPdf * wpdf;
				ray.color *= ray.pdf /oprtreciprocal;
				ray.origin = cameraSurface.worldPos;

				cameraSurface.enteredSub = &cameraSubstance;

				segment.pdfW = ray.pdf;
				segment.type = ray.type;

				const auto proposedContrib = traceAndIntegrate(tracer.rayTracer, ray, segment, 0, integrateFc);
				const auto proposedAlbedo = getColorY(proposedContrib.color);

				if(!proposedAlbedo)
				{
					mutator.mutator->reject();
					continue;
				}

				Real acceptProb;
				const auto proposedAlbedoRec = proposedAlbedo /oprtreciprocal;

				if(pl::plisnan(albedoLastRec))
				{
					acceptProb = 1;

					//samples.color[spIndex] += proposedContrib.rrColor * mutSpWeight;
					samples.addSample(camPos, proposedContrib.rrColor * mutSpWeight, 1);
				}
				else
				{
					acceptProb = pl::plmin<Real>(1, proposedAlbedo * albedoLastRec);

					samples.addSample(lastPos, lastContrib.rrColor * mutSpWeight * (1 - acceptProb), 1);
					//samples.color[lastSpIndex] += lastContrib.rrColor * mutSpWeight * (1 - acceptProb);// * ((1 - acceptProb) * albedoLastRec);

					if(acceptProb > 0)
						samples.addSample(camPos, proposedContrib.rrColor * mutSpWeight * acceptProb, 1);
						//samples.color[spIndex] += proposedContrib.rrColor * mutSpWeight * acceptProb;// * (acceptProb * proposedAlbedoRec);
				}

				//pixelColor += totalColor * mutSpWeight;
				//pixelColor += proposedColor * mutSpWeight;

				if((*tracer.sampler.mRng1d)() < acceptProb)
				{
					lastContrib = proposedContrib;
					lastPos = camPos;
					albedoLastRec = proposedAlbedoRec;
					mutator.mutator->accept();
				}
				else
					mutator.mutator->reject();

			}
		}

		return nbSamples;
	}*/

	PL_MAKE_MULTITAGGEDSPAN_TAG(jitter_left)
	PL_MAKE_MULTITAGGEDSPAN_TAG(jitter_right)
	PL_MAKE_MULTITAGGEDSPAN_TAG(camera_left)
	PL_MAKE_MULTITAGGEDSPAN_TAG(camera_right)

	void samplePixels(
		TracerParams &tracer,
		bool doJitter,
		bool sampleRandom,
		const MaterialLayer &cameraSubstance,
		auto &samples,
		u32 nbSamplesPixel,
		auto &&createInteractionFc,
		auto &&batchDone,
		auto &&integrateCameraFc) noexcept
	{
		RandomReal
			rndJitterBufferX[PRT_MAX_NBSAMPLES], rndJitterBufferY[PRT_MAX_NBSAMPLES],
			rndCameraBufferX[PRT_MAX_NBSAMPLES], rndCameraBufferY[PRT_MAX_NBSAMPLES];

		nbSamplesPixel = pl::plmin<uf32>(PRT_MAX_NBSAMPLES, nbSamplesPixel);

		const pl::dynamic_multi_tagged_span<
					hn::pair<type_c<jitter_left_t>, type_c<RandomReal>>,
					hn::pair<type_c<jitter_right_t>, type_c<RandomReal>>,
					hn::pair<type_c<camera_left_t>, type_c<RandomReal>>,
					hn::pair<type_c<camera_right_t>, type_c<RandomReal>>
				>
			rndSpan{
					nbSamplesPixel,
					hn::make_pair(jitter_left, pl::range_data(rndJitterBufferX)), hn::make_pair(jitter_right, pl::range_data(rndJitterBufferY)),
					hn::make_pair(camera_left, pl::range_data(rndCameraBufferX)), hn::make_pair(camera_right, pl::range_data(rndCameraBufferY))
				};

		const auto size = samples.tileDims.dims();
		std::array<Point2Di, PRT_SAMPLE_RECT_SIZE_MAX * PRT_SAMPLE_RECT_SIZE_MAX> spPos;
		RayBatch rayBatch;
		TraversalData traversalEntries;

		tracer.sampler.clearRecordedSamples();

		const auto doTraceAndInt = [&]
			{
				traceBatch(tracer.rayTracer, rayBatch, traversalEntries, PL_FWD(createInteractionFc));
				for(const auto i : mir(rayBatch.size()))
					PL_FWD(integrateCameraFc)(rayBatch[i], traversalEntries[i], spPos[i], i);

				PL_FWD(batchDone)();
			};

		const auto sampleCamera =
			[&](const Point2Di spp, const Point2Dr imgpos, const RandomReals<2> rndCam)
			{
				auto sp = samples.camera->sampleImageDir(tracer.sampler, rndCam, imgpos);
				if(!sp)
					return;

				auto &rbEntry = rayBatch.emplace_back();
				auto &trEntry = traversalEntries.emplace_back();

				auto &ray = rbEntry.ray;
				auto &segment = trEntry.segment;

				trEntry.contProb = 1;

				spPos[rayBatch.size() - 1] = spp;

				ray.setupPrimary();
				pl::visit_one(sp->sensor.interaction, [&](auto &i)
				{
					i.initialSurface(cameraSubstance);
					ray.origin = i.worldPos();

				});

				segment = {&sp->sensor.interaction, nullptr};
				segment.level = 0;
				segment.insideLayer = false;

				if(const auto pdf = pl::assert_valid( sp->sensor.pdf * sp->dir.pdf, valid::Weight ); pdf > 0)
				{
					trEntry.color = sp->dir.color * (pdf /oprtreciprocal);
					ray.length = isAlmostBlack(trEntry.color) ? 0 : ray.length;
				}
				else
				{
					trEntry.color.set_zero();
					ray.length = 0;
				}

				ray.dir = sp->dir.dir;
				trEntry.pdf = pl::assert_valid(sp->dir.pdf, valid::Weight);
				trEntry.albedo = 1;

				pl::assert_valid(ray.origin);
				pl::assert_valid(ray.dir);
				pl::assert_valid(trEntry.color);

				// Shoot rays whenever ray batch is full
				if(rayBatch.capacity() == rayBatch.size())
				{
					doTraceAndInt();
					tracer.sampler.clearRecordedSamples();
					rayBatch.clear();
					traversalEntries.clear();
				}
			};

		if(sampleRandom)
		{
			tracer.sampler.setPosition(samples.tileSampleCount, samples.tileDims.p1);

			tracer.sampler.requestSamples(rndSpan[jitter_left], uniformRealRng<>());
			tracer.sampler.requestSamples(rndSpan[jitter_right], uniformRealRng<>());
			tracer.sampler.requestSamples(rndSpan[camera_left], uniformRealRng<>());
			tracer.sampler.requestSamples(rndSpan[camera_right], uniformRealRng<>());

			rg::shuffle(rndSpan[jitter_right], tracer.sampler.mRng);
			rg::shuffle(rndSpan[camera_left], tracer.sampler.mRng);
			rg::shuffle(rndSpan[camera_right], tracer.sampler.mRng);

			for(const auto [rndJitterX, rndJitterY, rndCamX, rndCamY] : rgv::zip(rndSpan[jitter_left], rndSpan[jitter_right], rndSpan[camera_left], rndSpan[camera_right]))
			{
				tracer.sampler.beginRecordedStream();

				tracer.sampler.recordSample(rndJitterX);
				tracer.sampler.recordSample(rndJitterY);
				tracer.sampler.recordSample(rndCamX);
				tracer.sampler.recordSample(rndCamY);

				const auto rpos = Point2Dr{static_cast<Real>(rndJitterX), static_cast<Real>(rndJitterY)} * size;
				const auto ipos = static_cast<Point2Di>(rpos);
				sampleCamera(ipos, rpos + samples.tileDims.p1, {rndCamX, rndCamY});
			}
		}
		else
		{
			samples.tileDims.loop([&](const auto p){
				tracer.sampler.setPosition(samples.tileSampleCount, p);

				if(doJitter)
				{
					tracer.sampler.requestSamples(rndSpan[jitter_left], uniformRealRng<>());
					tracer.sampler.requestSamples(rndSpan[jitter_right], uniformRealRng<>());
					rg::shuffle(rndSpan[jitter_left], tracer.sampler.mRng);
					rg::shuffle(rndSpan[jitter_right], tracer.sampler.mRng);
				}

				tracer.sampler.requestSamples(rndSpan[camera_left], uniformRealRng<>());
				tracer.sampler.requestSamples(rndSpan[camera_right], uniformRealRng<>());

				for(const auto [rndJitterX, rndJitterY, rndCamX, rndCamY] : rgv::zip(rndSpan[jitter_left], rndSpan[jitter_right], rndSpan[camera_left], rndSpan[camera_right]))
				{
					tracer.sampler.beginRecordedStream();

					const auto jitter = std::invoke([&]
						{
							if(doJitter)
							{
								tracer.sampler.recordSample(rndJitterX);
								tracer.sampler.recordSample(rndJitterY);
								return Point2Dr{static_cast<Real>(rndJitterX), static_cast<Real>(rndJitterY)};
							}

							return Point2Dr{0.5, 0.5};
						});

					tracer.sampler.recordSample(rndCamX);
					tracer.sampler.recordSample(rndCamY);

					sampleCamera(p - samples.tileDims.p1, Point2Dr{p} + jitter, {rndCamX, rndCamY});
				}
			});
		}

		// Shoot remaining rays
		doTraceAndInt();
	}

	void PHOTORT_API updateSensorSubstanceStack(ISensor &sensor, IRayTracer &rayTracer);
	void PHOTORT_API updateSceneSubstanceStack(IRayTracer &rayTracer, Scene &scene);
	void PHOTORT_API buildSubstanceStack(IRayTracer &rayTracer, VecP position, SubstanceStack &stack);

}
