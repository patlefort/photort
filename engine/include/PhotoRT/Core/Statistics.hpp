/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Statistics_fwd.hpp"

#include "Base.hpp"

#include <patlib/accumulator.hpp>
#include <patlib/clone.hpp>
#include <patlib/chrono.hpp>

#include <fmt/format.h>

namespace PhotoRT
{
	class IStatsAccumulator : public virtual pl::ICloneableCopy<IStatsAccumulator>
	{
	public:
		virtual ~IStatsAccumulator() = default;

		[[nodiscard]] virtual Statistics toMap(const std::locale &loc, const std::string &prefix = {}) const = 0;
		virtual void aggregate(const IStatsAccumulator &other) = 0;
		virtual void reset() = 0;

		IStatsAccumulator &operator+= (const IStatsAccumulator &other)
		{
			aggregate(other);

			return *this;
		}
	};

	// Base type for extractors
	template <typename FeatureType_>
	struct StatExtractorBase
	{
		using FeatureType = FeatureType_;

		static constexpr auto tag = hn::type_c<FeatureType>;
	};


	template <typename SampleType_, typename... Stats_>
	class StatsAccumulator : public pl::CloneableCopy<IStatsAccumulator, StatsAccumulator<SampleType_, Stats_...>>, public IStatsAccumulator
	{
	public:
		using SampleType = SampleType_;
		static constexpr auto stats = hn::make_set(hn::type_c<Stats_>...);

	private:
		hn::tuple<Stats_...> mExtractors;
		pl::accumulators::accumulator_set<SampleType, typename Stats_::FeatureType...> mAccumulators;

	public:

		auto &operator() (const SampleType &sp)
		{
			mAccumulators(sp);
			return *this;
		}

		auto &operator() (std::initializer_list<SampleType> samples)
		{
			mAccumulators(samples);
			return *this;
		}

		template <typename Feature_>
		[[nodiscard]] auto extract(const Feature_ &f) const
		{
			return mAccumulators.extract(f.tag);
		}

		virtual void reset() final
			{ mAccumulators = {}; }

		// Transform to a Statistics map
		[[nodiscard]] virtual Statistics toMap(const std::locale &loc, const std::string &prefix = {}) const final
		{
			Statistics map;

			hn::for_each(mExtractors, [&](const auto &ex){
				const auto r = extract(ex);
				auto formatted = std::invoke([&]{
						if constexpr(std::floating_point<std::decay_t<decltype(r)>>)
							return fmt::format(loc, "{:.2f} ", r);
						else
							return fmt::format(loc, "{:L}    ", r);
					});


				map.emplace(prefix + ex.name, std::move(formatted));
			});

			return map;
		}

		// Aggregate with another accumulator set, assuming it is of the same type
		virtual void aggregate(const IStatsAccumulator &other) final
		{
			auto &accset = static_cast<const StatsAccumulator &>(other);

			mAccumulators += accset.mAccumulators;
		}

		StatsAccumulator &operator+= (const StatsAccumulator &other)
		{
			aggregate(other);
			return *this;
		}

	};

	// Merge 2 StatsAccumulatorList lists. Accumulator sets from list2 will be moved to list1 if list1 doesn't have the given accumulator.
	inline void mergeStats(StatsAccumulatorList &list1, StatsAccumulatorList &list2)
	{
		for(auto&& [key, acc] : list2)
		{
			if(auto it = list1.find(key); it != list1.end())
				*it->second += *acc;
			else
				list1.emplace(key, std::move(acc));
		}
	}

	namespace stats
	{
		#define PRT_MAKE_STAT_TAG(tagname, actag, namestr) \
			struct tagname : public StatExtractorBase<actag> { using StatExtractorBase<actag>::tag; static constexpr char name[] = namestr; }; \
			constexpr tagname tagname##_c{};

		PRT_MAKE_STAT_TAG(StatCount,  pl::accumulators::tag::count,   "count")
		PRT_MAKE_STAT_TAG(StatSum,    pl::accumulators::tag::sum,     "sum")
		PRT_MAKE_STAT_TAG(StatMin,    pl::accumulators::tag::min,     "min")
		PRT_MAKE_STAT_TAG(StatMax,    pl::accumulators::tag::max,     "max")
		PRT_MAKE_STAT_TAG(StatAvg,    pl::accumulators::tag::mean<>,  "avg")

		using AccumulatorTimed = StatsAccumulator<
			MReal,
			StatSum, StatMin, StatMax, StatAvg, StatCount
		>;

	} // namespace stats


	namespace Profiler
	{
		namespace impl
		{
			template <typename... StatTags_>
			class Profiler
			{
			public:

				using AccMapType = hn::map<
					hn::pair<hn::type<StatTags_>, stats::AccumulatorTimed>...
				>;

				using TimerMapType = hn::map<
					hn::pair<hn::type<StatTags_>, pl::stop_watch<i64>>...
				>;

				AccMapType accumulators;
				TimerMapType timers;


				template <typename Tag_>
				void start(const Tag_ tag)
				{
					timers[tag].start();
				}

				template <typename Tag_>
				void stop(const Tag_ tag)
				{
					accumulators[tag]( std::chrono::duration_cast<std::chrono::duration<MReal, std::milli>>(timers[tag].total_running_time()).count() );
				}

				[[nodiscard]] StatsAccumulatorList getStats() const
				{
					StatsAccumulatorList list;

					hn::for_each(accumulators, [&](const auto &acc){
						list.emplace("profile: "s + pl::hana_type_as_value(hn::first(acc)).name, pl::clone(hn::second(acc)));
					});

					return list;
				}
			};

		} // namespace impl

		#define PRT_MAKE_PROFILE_TAG(tagclass, namestr) \
			struct tagclass##_t { static constexpr char name[] = namestr; PL_TAG_COMP_OP(tagclass) }; \
			template <typename T_> concept tagclass##_c = std::same_as<T_, tagclass##_t>; \
			constexpr auto tagclass = hn::type_c<tagclass##_t>;

		#define PRT_MAKE_STATIC_PROFILE_TAG(tagclass, namestr) \
			struct tagclass##_t { static constexpr char name[] = namestr; PL_TAG_COMP_OP(tagclass) }; \
			static constexpr auto tagclass = hn::type_c<tagclass##_t>;

		namespace tag
		{
			PRT_MAKE_PROFILE_TAG(Rays,            "rays")
			PRT_MAKE_PROFILE_TAG(RayBatches,      "ray_batches")
			PRT_MAKE_PROFILE_TAG(AccelStruct,     "accel_struct")
			PRT_MAKE_PROFILE_TAG(PathWalk,        "path_walk")
			PRT_MAKE_PROFILE_TAG(Contribution,    "contribution")
		}

		using RayTrace = impl::Profiler<tag::Rays_t, tag::RayBatches_t, tag::AccelStruct_t, tag::PathWalk_t, tag::Contribution_t>;

		namespace impl
		{
			[[nodiscard]] PHOTORT_API RayTrace &raytrace();
		}

		PHOTORT_API extern thread_local RayTrace &raytrace;
	} // namespace Profiler

	template <typename Profiler_, typename Tag_>
	class ScopedProfilePhase
	{
	public:
		Profiler_ &prof;

		ScopedProfilePhase(Profiler_ &p, const Tag_ tag)
			: prof{p} { prof.start(tag); }

		~ScopedProfilePhase()
			{ prof.stop(Tag_{}); }
	};
}
