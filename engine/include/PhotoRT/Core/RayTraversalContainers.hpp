/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Instance_fwd.hpp"
#include "RayTraversalContainers_fwd.hpp"

#include "Base.hpp"
#include "RayTracer.hpp"
#include "PrimitiveGroup.hpp"
#include "Parameterized.hpp"
#include <PhotoRT/AccelStruct/AccelStructBVH.hpp>

#include <patlib/stable_block_vector.hpp>
#include <patlib/clone.hpp>

namespace PhotoRT
{
	template <typename T_>
	using AccelStructAllocator = pl::allocator_with_stats<T_, memtag::AccelStruct, std::pmr::polymorphic_allocator<T_>>;

	template <typename T_, typename IndexType_ = PrimitiveIndex>
	using ContainerVectorType = pl::stable_block_vector<T_, IndexType_, 16, AccelStructAllocator<T_>>;

	template <typename T_>
	using FinalContainerVectorType = std::vector<T_, AccelStructAllocator<T_>>;

	class ITraversable
	{
	public:
		virtual ~ITraversable() = default;

		[[nodiscard]] virtual CollisionResult traverse(const BasicRay &ray, Real distmax) const noexcept = 0;
		[[nodiscard]] virtual RayBatchMask traverseBatch(RayBatch &batch, RayBatchMask mask) const noexcept = 0;

	};

	class IAABoxHitDetector : public IAABox, public ITraversable
	{
	public:
		virtual ~IAABoxHitDetector() = default;
	};


	class PHOTORT_API EntityContainer : public IParameterized, public virtual pl::ICloneable<EntityContainer>, public IAABoxHitDetector
	{
	public:

		EntityContainer() = default;
		EntityContainer(const std::vector<pl::UniquePtr<EntityContainer>> &globalContainers)
			: mGlobalContainers(&globalContainers) {}

		EntityContainer(const EntityContainer &) = default;
		EntityContainer(EntityContainer &&) = default;

		std::pmr::memory_resource *mMemRes = nullptr, *mMonoMemRes = nullptr;
		const std::vector<pl::UniquePtr<EntityContainer>> *mGlobalContainers = nullptr;

		virtual void finalize() = 0;
		virtual void clearContainer() = 0;
		[[nodiscard]] virtual Real judgeCost() const = 0;
		[[nodiscard]] virtual Real judgeGeometryCost() const = 0;
		virtual void dumpInfo(std::ostream &outputStream, if32 level) const { outputStream << pl::io::indent(level) << boost::typeindex::type_id_runtime(*this).pretty_name() << '\n'; }
		[[nodiscard]] virtual PrimitiveIndex getNbPrimitives() const { return 0; }

		virtual void setMemoryResource(std::pmr::memory_resource *mr, std::pmr::memory_resource *monomr)
		{
			mMemRes = mr;
			mMonoMemRes = monomr;
		}

		virtual EntityContainer &operator= (const EntityContainer &o)
		{
			mInstance = o.mInstance;
			return *this;
		}

		virtual EntityContainer &operator= (EntityContainer &&o) noexcept
		{
			mInstance = std::move(o.mInstance);
			return *this;
		}

		virtual void attachToInstance(const IObjectInstance &) {}

		[[nodiscard]] virtual RayBatchMask traverseBatch(RayBatch &batch, RayBatchMask mask) const noexcept override;

		[[nodiscard]] const IObjectInstance *getInstance() const { return mInstance; }

		virtual void setParameters(const ParameterTree &) override {}
		[[nodiscard]] virtual ParameterTree getParameters(bool /*deep*/ = false) const override { return {}; }

	protected:
		const IObjectInstance *mInstance = nullptr;

		[[nodiscard]] EntityContainer &getContainer(szt index) noexcept { return *(*mGlobalContainers)[index]; }
		[[nodiscard]] const EntityContainer &getContainer(szt index) const noexcept { return *(*mGlobalContainers)[index]; }
	};

	class PHOTORT_API EntityContainerMulti final : public EntityContainer, public pl::Cloneable<EntityContainer, EntityContainerMulti>
	{
	protected:

		[[nodiscard]] auto getContainerRange() noexcept
		{
			return mChildContainers | rgv::transform([&](auto &c){
				return std::make_pair(std::ref(c), std::ref(getContainer(c)));
			});
		}

		[[nodiscard]] auto getContainerRange() const noexcept
		{
			return mChildContainers | rgv::transform([&](const auto &c){
				return std::make_pair(std::ref(c), std::ref(getContainer(c)));
			});
		}

	public:

		using EntityContainer::EntityContainer;

		VectorWithStatsPMRnoInit<i32, memtag::AccelStruct> mChildContainers;

		virtual EntityContainer &operator= (const EntityContainer &o) override
		{
			return (*this = static_cast<const EntityContainerMulti &>(o));
		}

		virtual EntityContainer &operator= (EntityContainer &&o) noexcept override
		{
			return (*this = static_cast<EntityContainerMulti &&>(o));
		}

		virtual void setMemoryResource(std::pmr::memory_resource *mr, std::pmr::memory_resource *monomr) override
		{
			EntityContainer::setMemoryResource(mr, monomr);

			pl::recreate(mChildContainers, typename decltype(mChildContainers)::allocator_type{std::in_place, mr});
		}

		virtual void finalize() override
		{
			for(const auto &c : getContainerRange())
				c.second.finalize();
		}

		virtual void attachToInstance(const IObjectInstance &inst) override
		{
			mInstance = &inst;

			for(const auto &c : getContainerRange())
				c.second.attachToInstance(inst);
		}

		virtual void clearContainer() override
		{
			for(const auto &c : getContainerRange())
				c.second.clearContainer();
		}

		[[nodiscard]] virtual Real judgeCost() const override
		{
			return rg::accumulate(
				getContainerRange(),
				Real{},
				rg::plus{},
				[&](const auto &c){ return c.second.judgeCost(); }
			);
		}

		[[nodiscard]] virtual Real judgeGeometryCost() const override
		{
			return rg::accumulate(
				getContainerRange(),
				Real{},
				rg::plus{},
				[&](const auto &c){ return c.second.judgeGeometryCost(); }
			);
		}

		[[nodiscard]] virtual PrimitiveIndex getNbPrimitives() const override
		{
			return rg::accumulate(
				getContainerRange(),
				PrimitiveIndex{},
				rg::plus{},
				[&](const auto &c){ return c.second.getNbPrimitives(); }
			);
		}

		virtual void dumpInfo(std::ostream &outputStream, if32 level) const override;
		[[nodiscard]] virtual Box getAABox() const noexcept override;
		[[nodiscard]] virtual CollisionResult traverse(const BasicRay &ray, Real distmax) const noexcept override;
		[[nodiscard]] virtual RayBatchMask traverseBatch(RayBatch &batch, RayBatchMask mask) const noexcept override;
	};


	class PHOTORT_API EntityContainerInstanceTree final : public EntityContainer, public pl::Cloneable<EntityContainer, EntityContainerInstanceTree>
	{
	public:

		using EntityContainer::EntityContainer;

		struct ChildContainerEntry
		{
			Matrix worldMatrix, worldToLocalMatrix;
			Box aaboxWorld;
			u32 containerIndex;
			PrimGroupPropertiesMapPtr propMap;
		};

		class LeafContainer
		{
		public:

			using BoundaryEntryType = AccelStruct::BoundaryEntryMiddle<u32, Point3Dr>;

			VectorWithStatsPMR<ChildContainerEntry, memtag::AccelStruct> mContainers;
			VectorWithStatsPMRnoInit<u32, memtag::AccelStruct> mLeaves;
			VectorWithStatsPMRnoInit<u32, memtag::AccelStruct> mLeafContainers;

			void setMemoryResource(std::pmr::memory_resource *mr)
			{
				pl::recreate(mLeaves, typename decltype(mLeaves)::allocator_type{std::in_place, mr});
				pl::recreate(mLeafContainers, typename decltype(mLeafContainers)::allocator_type{std::in_place, mr});
			}

			void prepare(Real targetCost, u32 /*nbThreads*/)
			{
				const auto primitiveCost = getGeometricCost(1);
				const auto nb = pl::plmax<szt>(1, pl::plmax<Real>(targetCost, primitiveCost) / primitiveCost);

				mLeaves.reserve(nb);
				mLeafContainers.reserve(nb);
			}

			[[nodiscard]] bool leafIndexIsValid(u32 index) const noexcept
				{ return index < mLeaves.size(); }

			[[nodiscard]] u32 getNbPrimitivesInLeaves() const noexcept { return mLeafContainers.size(); }
			[[nodiscard]] u32 getNbPrimitives() const noexcept { return mContainers.size(); }
			[[nodiscard]] u32 getNbPrimitives(u32 index) const noexcept
			{
				return (index + 1 == mLeaves.size() ? mLeafContainers.size() : mLeaves[index + 1]) - mLeaves[index];
			}

			[[nodiscard]] szt memory_capacity() const noexcept { return pl::container_memory_capacity(mContainers) + pl::container_memory_capacity(mLeaves) + pl::container_memory_capacity(mLeafContainers); }

			void finalize()
			{
				mLeaves.shrink_to_fit();
				mLeafContainers.shrink_to_fit();
			}

			void clear()
			{
				mLeaves.clear();
				mLeaves.shrink_to_fit();
				mLeafContainers.clear();
				mLeafContainers.shrink_to_fit();
			}

			[[nodiscard]] u32 getNbLeaves() const noexcept { return mLeaves.size(); }

			template <typename RangeType_>
			u32 createLeaf(const RangeType_ &list, u32 /*threadNo*/)
			{
				return pl::tasks.critical([&]{
						mLeaves.push_back(mLeafContainers.size());

						for(const auto &e : list)
							mLeafContainers.push_back(e.getIndex());

						return mLeaves.size() - 1;
					});
			}

			[[nodiscard]] auto createPrimitiveBoundaryList() const
			{
				#ifdef PL_LIB_HUGEPAGES
					std::pmr::vector<BoundaryEntryType> boundaries{{&pl::hugepages_resource()}};
				#else
					std::vector<BoundaryEntryType> boundaries;
				#endif

				boundaries.reserve(mContainers.size());

				for(const auto i : mir(mContainers.size()))
					boundaries.emplace_back((u32)i, mContainers[i].aaboxWorld);

				return boundaries;
			}

			[[nodiscard]] Real getGeometricCost(u32 nbPrimitives) const noexcept
			{
				return r_(nbPrimitives) * 100;
			}
		};

		Box mAaboxTotal;
		LeafContainer mLeafContainer;

		virtual void setMemoryResource(std::pmr::memory_resource *mr, std::pmr::memory_resource *monomr) override;

		virtual EntityContainer &operator= (const EntityContainer &o) override
		{
			return (*this = static_cast<const EntityContainerInstanceTree &>(o));
		}

		virtual EntityContainer &operator= (EntityContainer &&o) noexcept override
		{
			return (*this = static_cast<EntityContainerInstanceTree &&>(o));
		}

		virtual void setAABBchecks(bool c) { mDoAABB = c; }
		[[nodiscard]] virtual bool doAABBchecks() const { return mDoAABB; }

		virtual void finalize() override;
		virtual void attachToInstance(const IObjectInstance &inst) override;
		virtual void clearContainer() override;
		[[nodiscard]] virtual Real judgeCost() const override;
		[[nodiscard]] virtual Real judgeGeometryCost() const override;
		virtual void dumpInfo(std::ostream &outputStream, if32 level) const override;
		[[nodiscard]] virtual PrimitiveIndex getNbPrimitives() const override;
		[[nodiscard]] virtual Box getAABox() const noexcept override;

		[[nodiscard]] virtual CollisionResult traverse(const BasicRay &ray, Real distmax) const noexcept override;
		[[nodiscard]] virtual RayBatchMask traverseBatch(RayBatch &batch, RayBatchMask mask) const noexcept override;

	protected:
		bool mDoAABB = true;

		template <typename T_> using BuildVectorType = ContainerVectorType<T_>;
		using BuilderType = AccelStruct::BVHBuilderSAH<LeafContainer, u32, Point3Dr, BuildVectorType, FinalContainerVectorType>;

		pl::uninitialized_value<BuilderType::DataType> mBvhData;

		[[nodiscard]] auto getContainerRange() noexcept
		{
			return mLeafContainer.mContainers | rgv::transform([&](auto &c){
				return std::make_pair(std::ref(c), std::ref(getContainer(c.containerIndex)));
			});
		}

		[[nodiscard]] auto getContainerRange() const noexcept
		{
			return mLeafContainer.mContainers | rgv::transform([&](const auto &c){
				return std::make_pair(std::ref(c), std::ref(getContainer(c.containerIndex)));
			});
		}
	};

}
