/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>

#include <PhotoRT/Core/Sensors.hpp>
#include <PhotoRT/Core/Transform.hpp>
#include <PhotoRT/Core/Integration.hpp>

#include <patlib/task.hpp>
#include <patlib/array_view.hpp>
#include <patlib/marked_optional.hpp>

namespace PhotoRT
{
	template <typename SensorType_ = ISensor>
	class SensorSamplerGridDistribution : public SensorSamplerBase<SensorType_>
	{
	protected:

		template <typename T_>
		using VectorType = VectorWithStats<T_, memtag::Other>;

		VectorType<Real> mGrid, mCdfData, mCdfIntegrals;
		VectorType<u32> mLightIndexes;

		u32 mMaxNbLightsPerCell = 10, mNbLightsPerCell = 0;
		std::ptrdiff_t mNbCells = 100;
		Box mAabox;
		Point3Di mNbGridCells, mLastGridCell;
		VecD mGridSize, mGridSizeInv, mGridHalfSize;

		bool mHasOtherLights = true, mTestVisibility = true, mSampleArea = true;
		Real mLightPerCellsProportion;

		struct SensorEntry
		{
			const SensorType_ *sensor;
			std::ptrdiff_t index;
		};
		VectorType<SensorEntry> mSensorsIndex;

		[[nodiscard]] auto getSensorIndex(const SensorType_ &sensor) const noexcept
		{
			return rg::lower_bound(mSensorsIndex, &sensor, rg::less{}, [](const auto &a){ return a.sensor; })->index;
		}

		[[nodiscard]] auto getCell(const VecP pos) const noexcept
		{
			return static_cast<Point3Di>(
					static_cast<VecD>(pos - mAabox.v1) * mGridSizeInv /omax/ VecD{} /omin/ static_cast<VecD>(mLastGridCell)
				).as_point();
		}

		[[nodiscard]] pl::marked_optional<Point3Di> getCellMaybe(const VecP pos) const noexcept
		{
			if(mSensors.size() <= 1)
				return pl::nullopt;

			return getCell(pos);
		}

		template <typename T_>
		[[nodiscard]] static auto getCdfDataViewImpl(T_ &this_) noexcept
		{
			return pl::av::array_view{
					this_.mCdfData,
					pl::av::bounds<4>{
						this_.mNbGridCells[2],
						this_.mNbGridCells[1],
						this_.mNbGridCells[0],
						static_cast<std::ptrdiff_t>(this_.mNbLightsPerCell) + this_.mHasOtherLights + 2
					}
				};
		}

		[[nodiscard]] auto getCdfDataView() noexcept { return getCdfDataViewImpl(*this); }
		[[nodiscard]] auto getCdfDataView() const noexcept { return getCdfDataViewImpl(*this); }

		template <typename T_>
		[[nodiscard]] static auto getGridViewImpl(T_ &this_) noexcept
		{
			return pl::av::array_view{
					this_.mGrid,
					pl::av::bounds<4>{
						this_.mNbGridCells[2],
						this_.mNbGridCells[1],
						this_.mNbGridCells[0],
						static_cast<std::ptrdiff_t>(this_.mNbLightsPerCell) + this_.mHasOtherLights
					}
				};
		}

		[[nodiscard]] auto getGridView() noexcept { return getGridViewImpl(*this); }
		[[nodiscard]] auto getGridView() const noexcept { return getGridViewImpl(*this); }

		template <typename T_>
		[[nodiscard]] static auto getCdfImpl(T_ &this_, const Point3Di pos) noexcept
		{
			const auto cdfv = this_.getCdfDataView()[pos[2]][pos[1]][pos[0]];
			const auto gridv = this_.getGridView()[pos[2]][pos[1]][pos[0]];

			return CDFDistribution{
					cdfv.size(),
					gridv.data(),
					cdfv.data()
				};
		}

		[[nodiscard]] auto getCdf(const Point3Di pos) noexcept { return getCdfImpl(*this, pos); }
		[[nodiscard]] auto getCdf(const Point3Di pos) const noexcept { return getCdfImpl(*this, pos); }

		template <typename T_>
		[[nodiscard]] static auto getLightIndexViewImpl(T_ &this_) noexcept
		{
			return pl::av::array_view{
					this_.mLightIndexes,
					pl::av::bounds<4>{
						this_.mNbGridCells[2],
						this_.mNbGridCells[1],
						this_.mNbGridCells[0],
						static_cast<std::ptrdiff_t>(this_.mNbLightsPerCell)
					}
				};
		}

		[[nodiscard]] auto getLightIndexView() noexcept { return getLightIndexViewImpl(*this); }
		[[nodiscard]] auto getLightIndexView() const noexcept { return getLightIndexViewImpl(*this); }

		[[nodiscard]] pl::marked_optional<u32> findLightIndex(const Point3Di pos, u32 lindex) const noexcept
		{
			const auto liSpan = std::span{ getLightIndexView()[pos[2]][pos[1]][pos[0]] };

			if(auto it = std::lower_bound(liSpan.begin(), liSpan.end(), lindex); it != liSpan.end() && *it == lindex)
				return std::distance(liSpan.begin(), it);

			return pl::nullopt;
		}

	public:

		using SampleFunctor = typename SensorSamplerBase<SensorType_>::SampleFunctor;
		using SensorSamplerBase<SensorType_>::mSensors;

		EntityPtr<const ISensorSampler<SensorType_>> mFallbackSampler;

		void setNbCells(std::ptrdiff_t nb) noexcept { mNbCells = nb; }
		[[nodiscard]] std::ptrdiff_t getNbCells() const noexcept { return mNbCells; }

		void setMaxNbLightsPerCell(std::ptrdiff_t nb) noexcept { mMaxNbLightsPerCell = nb; }
		[[nodiscard]] std::ptrdiff_t getMaxNbLightsPerCell() const noexcept { return mMaxNbLightsPerCell; }

		void setTestVisibility(bool t) noexcept { mTestVisibility = t; }
		[[nodiscard]] bool getTestVisibility() const noexcept { return mTestVisibility; }

		void setSampleArea(bool t) noexcept { mSampleArea = t; }
		[[nodiscard]] bool getSampleArea() const noexcept { return mSampleArea; }

		[[nodiscard]] virtual OptionalPDF<Real> PDF(const MaterialInteraction &inter, const SensorType_ &sensor) const noexcept override
		{
			if(mSensors.empty())
				return pl::nullopt;

			return getCellMaybe(pl::visit_one(inter, [&](const auto &i){ return i.worldPos(); }))
				.and_then([&](const auto &pos) -> OptionalPDF<Real>
				{
					const auto index = getSensorIndex(sensor);
					const auto cdf = getCdf(pos);
					const auto calcPMF = [&]
						{
							if(mHasOtherLights)
							{
								if(const auto lindex = findLightIndex(pos, index); lindex)
									return cdf.joinPMF(cdf.PMF(0) * this->mPdf, cdf.PMF(*lindex + 1));
								else
									return cdf.PMF(0) * this->mPdf;
							}

							return cdf.PMF(index);
						};

					return pl::assert_valid(calcPMF() * mLightPerCellsProportion * this->mPdf, valid::Weight);
				})
				.or_else([&]() -> OptionalPDF<Real>
				{
					if(!mFallbackSampler)
						return pl::nullopt;

					return mFallbackSampler->PDF(inter, sensor);
				});
		}

		virtual void sample(const MaterialInteraction &inter, uf32 nbSamples, uf32 nbSamplesPerSensors, Sampler &rng, const SampleFunctor &sampleFc) const noexcept override
		{
			if(mSensors.empty())
				return;

			if(const auto optPos = getCellMaybe(pl::visit_one(inter, [&](const auto &i){ return i.worldPos(); })); !optPos)
			{
				if(!mFallbackSampler)
					return;

				mFallbackSampler->sample(inter, nbSamples, nbSamplesPerSensors, rng, sampleFc);
			}
			else
			{
				const auto pos = *optPos;
				const auto cdf = getCdf(pos);

				if(!cdf.integral())
					return;

				nbSamples = pl::plmin<uf32>(PRT_MAX_NBSAMPLES, nbSamples);
				nbSamplesPerSensors = pl::plmin<uf32>(PRT_MAX_NBSAMPLES, nbSamplesPerSensors);

				const auto w = Real(nbSamples * nbSamplesPerSensors) /oprtreciprocal;
				RandomReal rndl[PRT_MAX_NBSAMPLES];
				const std::span rndlSpan{rndl, nbSamples};
				RandomReal rndx[PRT_MAX_NBSAMPLES], rndy[PRT_MAX_NBSAMPLES];
				const std::span rndSpanX{rndx, nbSamplesPerSensors}, rndSpanY{rndy, nbSamplesPerSensors};

				rng.requestSamples(rndlSpan, uniformRealRng<>(), true);

				const auto lightIndexesSlice = mHasOtherLights ? getLightIndexView()[pos[2]][pos[1]][pos[0]] : pl::av::array_view<u32, 1>{};
				auto uiRng = uniformIndexRng(mSensors.size());

				for(const auto i : rndlSpan)
				{
					auto index = (u32)cdf(i);
					auto lsPdf = cdf.PMF(index);
					plassert(lsPdf > 0);

					if(mHasOtherLights)
					{
						if(!index)
						{
							index = uiRng(rng);
							lsPdf *= this->mPdf;

							if(const auto lindex = findLightIndex(pos, index); lindex)
							{
								lsPdf = cdf.joinPMF(lsPdf, cdf.PMF(*lindex + 1));
								plassert(lsPdf > 0);
							}
						}
						else
						{
							index = lightIndexesSlice[index - 1];
							lsPdf = cdf.joinPMF(lsPdf, cdf.PMF(0) * this->mPdf);
							plassert(lsPdf > 0);
						}

						lsPdf *= mLightPerCellsProportion;
					}

					lsPdf *= this->mPdf;

					rng.requestSamples(rndSpanX, uniformRealRng<>(), true);
					rng.requestSamples(rndSpanY, uniformRealRng<>(), true);

					rg::shuffle(rndSpanY, rng.mRng);

					const auto &sensor = *mSensors[index];

					for(const auto c : mir(nbSamplesPerSensors))
						sampleFc(sensor, index, w, lsPdf, {rndSpanX[c], rndSpanY[c]});
				}
			}
		}

		virtual void init(const Scene &scene, TracerParams &tracer) override
		{
			SensorSamplerBase<SensorType_>::init(scene, tracer);

			if(!mFallbackSampler)
			{
				auto sampler = std::make_shared<SensorSamplerOne<SensorType_>>();
				sampler->setList(mSensors);
				sampler->init(scene, tracer);
				mFallbackSampler = std::move(sampler);
			}

			if(mSensors.size() <= 1)
			{
				mGrid.clear();
				mGrid.shrink_to_fit();
				mSensorsIndex.clear();
				mSensorsIndex.shrink_to_fit();
				mCdfIntegrals.clear();
				mCdfIntegrals.shrink_to_fit();
				mCdfData.clear();
				mCdfData.shrink_to_fit();
				mLightIndexes.clear();
				mLightIndexes.shrink_to_fit();
			}
			else
			{

				mSensorsIndex.clear();
				mSensorsIndex.reserve(mSensors.size());
				for(const auto i : mir(mSensors.size()))
					mSensorsIndex.push_back({mSensors[i].get(), (std::ptrdiff_t)i});
				mSensorsIndex.shrink_to_fit();

				rg::sort(mSensorsIndex, [](const auto &a, const auto &b){ return a.sensor < b.sensor; });

				mAabox = scene.getAABox();

				mNbLightsPerCell = std::min<u32>(mMaxNbLightsPerCell, mSensors.size());
				mHasOtherLights = mNbLightsPerCell != mSensors.size();

				{
					const auto boxSize = mAabox.dims();
					mGridSize = boxSize / VecD{r_(mNbCells), 1};

					const auto maxIndex = mGridSize.max3_index();

					mGridSizeInv.set(1 / mGridSize[maxIndex], maxIndex);

					mNbGridCells[maxIndex] = mNbCells;
					for(const auto i : mir<decltype(maxIndex)>(3))
						if(i != maxIndex)
						{
							mNbGridCells[i] = boxSize[i] * mGridSizeInv[maxIndex] /oceil;
							mGridSize.set(boxSize[i] / mNbGridCells[i], i);
							mGridSizeInv.set(1 / mGridSize[i], i);
						}

					mGridHalfSize = mGridSize * 0.5;
					mLastGridCell = mNbGridCells - 1;
				}

				const pl::av::bounds<4> gridPowerBounds{mNbGridCells[2], mNbGridCells[1], mNbGridCells[0], (std::ptrdiff_t)mNbLightsPerCell + mHasOtherLights};
				const pl::av::bounds<4> gridIndexBounds{mNbGridCells[2], mNbGridCells[1], mNbGridCells[0], (std::ptrdiff_t)mNbLightsPerCell};
				mGrid.resize(gridPowerBounds.size());
				mGrid.shrink_to_fit();

				const auto gridView = getGridView();

				if(mHasOtherLights)
				{
					mLightPerCellsProportion = r_(mSensors.size()) / (mNbLightsPerCell + 1);
					mLightIndexes.resize(gridIndexBounds.size());
				}
				else
				{
					mLightPerCellsProportion = 1;
					mLightIndexes.clear();
				}
				mLightIndexes.shrink_to_fit();

				const auto nbCellsTotal = (mNbGridCells /ohmul)[0];
				mCdfIntegrals.resize(nbCellsTotal);
				mCdfIntegrals.shrink_to_fit();
				mCdfData.resize(nbCellsTotal * (mNbLightsPerCell + mHasOtherLights + 2));
				mCdfData.shrink_to_fit();

				const auto gridBounds = pl::av::bounds<3>{mNbGridCells[2], mNbGridCells[1], mNbGridCells[0]};

				const auto lightsPower = [&]
					{
						std::vector<Real> lightsPower(mSensors.size());
						pl::policy::scoped_schedule sched{pl::policy::schedule::dynamic, 1};

						rgv::zip(lightsPower, mSensors) |
						pl::for_each
						(
							pl::applier([](auto &lp, const auto &sensor) { lp = getColorY(pl::assert_valid(sensor->averagePower())); }),
							std::identity{}, pl::policy::parallel_taskqueue_or_parallel
						);

						return lightsPower;
					}();

				const auto getLightAreaPower =
					[](const auto &ls, const auto lp, const auto &pos) -> Real
					{
						return lp ? pl::one_if_zero( lp / ( ls.isInfinite() ? 1 : pl::one_if_zero((ls.getMiddle() - ls.worldToLocal(pos)) /osdot) ) ) : 0;
					};

				gridBounds | pl::for_each
				(
					[&](auto &&cell)
					{
						const auto pos = (mAabox.v1 + VecD{r_(cell[2]), r_(cell[1]), r_(cell[0])} * mGridSize).as_point() + mGridHalfSize;
						const auto gridSliceView = gridView[cell[0]][cell[1]][cell[2]];

						static constexpr auto nbDivide = 5;
						const auto lightVisibilityTest =
							[&,
								bst = pos - mGridHalfSize + mGridSize / (nbDivide * 2),
								bsize = mGridSize / nbDivide
							](const SensorType_ &ls) -> Real
							{
								const auto bnds = pl::av::bounds<3>{nbDivide, nbDivide, nbDivide};
								const auto w1 = 1.0 / bnds.size();

								return getColorY(rg::accumulate(bnds, Black, rg::plus{}, [&](const auto cp)
									{
										const PointInteraction fromInter{
												.mWorldPos = bst + bsize * VecD{r_(cp[2]), r_(cp[1]), r_(cp[0])}
											};

										const auto bnds = ls.isInfinite() ? pl::av::bounds<2>{10, 10} : pl::av::bounds<2>{3, 3};
										const auto st = Point2Dr{r_(bnds[0]), r_(bnds[1])} * 2 /oreciprocal;
										const auto w2 = w1 / bnds.size();

										return rg::accumulate(bnds, Black, rg::plus{}, [&](const auto lp)
											{
												auto sp = ls.sampleIn(fromInter, tracer.sampler, {r_(lp[1]) / bnds[1] + st[1], r_(lp[0]) / bnds[0] + st[0]});

												if(!sp || isAlmostBlack(sp->color))
													return Black;

												return static_cast<Color>(sp->color) * (sp->pdf /oprtreciprocal)

													* [&]{
															if(!mTestVisibility)
																return White;

															Ray ray{};
															SubstanceStack subs;

															return pl::visit_one(sp->sensor.interaction, [&](auto &i)
															{
																ray.origin = i.worldPos();
																const auto hemisphere = i.getHemisphere(-sp->dir);

																if(!ls.isInfinite())
																	ray.origin = i.safeIncrement(ray.origin, ray.origin, hemisphere);

																ray.setDestination(i.worldPos());

																if constexpr(pl::is_a<decltype(i), SurfaceInteraction>)
																{
																	if(hemisphere == EnumHemisphere::South && i.surface().matEntry &&
																		i.surface().matEntry->material->mLayers.back().isSubstance())
																	{
																		subs.push(i.surface().matEntry->material->mLayers.back(), i.surface().matEntry->material->getRefractionPriority());
																		i.initialSurface(i.surface().matEntry->material->mLayers.back());
																	}
																	else
																	{
																		subs = ls.getSubstanceStack();
																		i.initialSurface(subs.back());
																	}
																}
																else
																{
																	subs = ls.getSubstanceStack();
																	i.initialSurface(subs.back());
																}

																const auto sres = traceShadow(EnumTransportMode::Importance, ray, sp->sensor.interaction, tracer, subs, 100);
																return static_cast<Color>(sres.color);
															});
														}()

													* w2;
											});
									}));
							};

						if(mHasOtherLights)
						{
							thread_local std::vector<Real> lpowers;
							thread_local std::vector<u32> lindexes;

							lpowers.clear();
							lpowers.reserve(mSensors.size());
							lindexes.clear();
							lindexes.reserve(mSensors.size());

							for(auto&& [ls, lp, i] : rgv::zip(mSensors, lightsPower, rgv::iota(0, rg::unreachable)))
							{
								lpowers.push_back( getLightAreaPower(*ls, lp, pos) );
								lindexes.push_back(i);
							}

							const auto startIndex = lindexes.size() - mNbLightsPerCell;
							std::nth_element(lindexes.begin(), lindexes.begin() + startIndex, lindexes.end(),
								[&](const auto &a, const auto &b){ return lpowers[a] < lpowers[b]; }
							);

							const auto importantLightsRange = lindexes | rgv::drop(startIndex);

							if(mSampleArea)
							{
								for(const auto i : importantLightsRange)
									lpowers[i] *= pl::pllerp(lpowers[i], lightVisibilityTest(*mSensors[i]), Real{0.8});
							}

							gridSliceView[0] = 0.1 * rg::accumulate(importantLightsRange, Real{}, rg::plus{},
								[&](const auto &b){ return lpowers[b]; }
							);

							importantLightsRange |= rg::actions::sort;

							for(const auto gridSliceViewIndexes = getLightIndexView()[cell[0]][cell[1]][cell[2]];
							    auto&& [i, lindex] : rgv::enumerate(importantLightsRange))
							{
								gridSliceView[i + 1] = lpowers[lindex];
								gridSliceViewIndexes[i] = lindex;
							}
						}
						else
						{
							const auto b = gridSliceView.bounds();
							for(auto&& [ls, lp, bi] : rgv::zip(mSensors, lightsPower, b))
							{
								auto ap = getLightAreaPower(*ls, lp, pos);
								if(mSampleArea)
									ap = pl::pllerp(ap, lightVisibilityTest(*ls), Real{0.8});
								gridSliceView[bi] = ap;
							}
						}

						const auto cdf = getCdf({
							static_cast<Point3Di::value_type>(cell[2]),
							static_cast<Point3Di::value_type>(cell[1]),
							static_cast<Point3Di::value_type>(cell[0])});
						cdf.createDistribution();
					},

					std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(100)
				);

				//std::cout << pl::io::range{pl::io::mode_text, '\n', rgv::all(mGrid)} << '\n';
			}
		}

	};

	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("lightsampler-grid", AugmentedLightSamplerGridDistribution, SensorSamplerGridDistribution<IEmitter>)
}
