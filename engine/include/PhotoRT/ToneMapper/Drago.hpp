/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/ToneMapper.hpp>
#include <PhotoRT/Core/ColorProfile.hpp>

namespace PhotoRT
{
	class PHOTORT_API ToneMapperDrago : public ToneMapperBase
	{
	public:
		ToneMapperDrago()
		{
			setBias(0.85);
		}

		[[nodiscard]] virtual Color operator() (Color value) const noexcept override;

		#ifdef PRT_FEATURE_PACKED_COLOR
			[[nodiscard]] virtual PackedColor operator() (PackedColor value) const noexcept override;
		#endif

		virtual void prepare(const FilmBitmap &bitmap, const IColorProfile &colorProfile) override;

		void setBias(CReal b)
		{
			mBias = b;
			mBiasP = mBias /olog / ((CReal)0.5 /olog);
		}

		[[nodiscard]] CReal getBias() const noexcept { return mBias; }

		[[nodiscard]] bool getConstantBrightness() const noexcept { return mConstantBrightness; }
		void setConstantBrightness(bool v) noexcept { mConstantBrightness = v; }

	protected:
		CReal mLavg = 0, mLavgInv = 0, mLmax = 0, mLmaxInv = 0, mScaleFactor = 0, mBias = 0, mBiasP = 0;
		bool mConstantBrightness = true;

		template <typename T_>
		[[nodiscard]] T_ toneMapDrago(const T_ c) const noexcept
		{
			if(!mLmaxInv) [[unlikely]]
				return c;

			const auto Y = mColorProfile->getY(c);
			const auto Yw = Y * mLavgInv;
			const auto L = pl::pllog(Yw + pl::constants::one<std::decay_t<decltype(Yw)>>) / pl::pllog(Yw * mLmaxInv /opow/ mBiasP * 8 + pl::constants::two<std::decay_t<decltype(Yw)>>) * mScaleFactor;

			return pl::select(
					c * (L /omin/ pl::constants::one<std::decay_t<decltype(L)>>),
					Yw > pl::constants::zero<std::decay_t<decltype(Yw)>>
				)
				/omin/ constants::white<T_>;
		}
	};


	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("tonemapper-drago", AugmentedToneMapperDrago, ToneMapperDrago)
}
