/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/RenderEntity.hpp>
#include <PhotoRT/Core/ToneMapper.hpp>

namespace PhotoRT
{
	class ToneMapperClamper : public ToneMapperBase
	{
	public:
		[[nodiscard]] virtual Color operator() (Color value) const noexcept override
		{
			return reduceColor(value);
		}

		#ifdef PRT_FEATURE_PACKED_COLOR
			[[nodiscard]] virtual PackedColor operator() (PackedColor value) const noexcept override
			{
				return reduceColor(value);
			}
		#endif
	};

	class ToneMapperExposure : public ToneMapperBase
	{
	public:

		[[nodiscard]] virtual Color operator() (Color value) const noexcept override
		{
			value = White - (-value * mExposureVec) /oprtexp2;

			return value;
		}

		#ifdef PRT_FEATURE_PACKED_COLOR
			[[nodiscard]] virtual PackedColor operator() (PackedColor value) const noexcept override
			{
				value = constants::white<PackedColor> - (-value * mExposureFactor) /oprtexp2;

				return value;
			}
		#endif

		void setExposure(CReal expos)
		{
			mExposure = expos;
			mExposureFactor = (CReal)64 /opow/ expos;
			mExposureVec = mExposureFactor;
		}

		[[nodiscard]] CReal getExposure() const noexcept { return mExposure; }

	protected:
		Color mExposureVec{};
		CReal mExposure = 0, mExposureFactor = 1;

	};

	class ToneMapperScale : public ToneMapperBase
	{
	public:
		Color mPower{pl::tag::one};

		[[nodiscard]] virtual Color operator() (Color value) const noexcept override
		{
			return value * mPower;
		}

		#ifdef PRT_FEATURE_PACKED_COLOR
			[[nodiscard]] virtual PackedColor operator() (PackedColor value) const noexcept override
			{
				return value * mPower;
			}
		#endif
	};


	PRT_ENTITY_FACTORY_AUGMENT_DECLARE_NOPARAMS("tonemapper-clamper", AugmentedToneMapperClamper, ToneMapperClamper)
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("tonemapper-exposure", AugmentedToneMapperExposure, ToneMapperExposure)
	PRT_ENTITY_FACTORY_AUGMENT_DECLARE("tonemapper-scale", AugmentedToneMapperScale, ToneMapperScale)
}
