/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Sampling_fwd.hpp>

#include <PhotoRT/Core/Base.hpp>

#include <PhotoRT/Core/Integrator.hpp>
#include <PhotoRT/Core/PathWalker.hpp>

#include <boost/hana/map.hpp>

#include <patlib/stable_block_vector.hpp>

namespace PhotoRT::PathIntegrator
{
	namespace statscollection
	{
		using namespace stats::collection;

		using Stats = hn::map<
			hn::pair<type_c<Sample_t>,               stats::AccumulatorSample>,
			hn::pair<type_c<ShadowRays_t>,           stats::AccumulatorShadowRays>,
			hn::pair<type_c<Path_t>,                 stats::AccumulatorPath>,
			hn::pair<type_c<ShadowRaysOccluded_t>,   stats::AccumulatorShadowRaysOccluded>,
			hn::pair<type_c<ZeroContribPath_t>,      stats::AccumulatorZeroContrib>
		>;
	} // namespace statscollection

	class PHOTORT_API IntegratorFactoryPath : public RenderEntity, public IIntegratorFactory
	{
	protected:

		bool mJitter = true, mTraceShadows = true, mRandomPixel = false;
		uf32 mNbSamplesPixel = 1, mNbSamplesDirectLight = 1, mNbSamplesDirectPerLights = 4;
		Real mInvNbSamplesPixel = 1, mInvNbLightPaths = 1.0 / 8.0, mInvNbSamplesDirectLight = 1.0 / 4.0;

		bool mHasLightsM = false;

		PathWalker mWalker;

		EntityPtr<IEmitterSampler> mDirectLightSampler, mLightPortalSampler;

		Real mLightPortalSampleProb = 0.5;

	public:
		PRT_ENTITY_FACTORY_DECLARE("int_path-", IntegratorFactoryPath)

		std::vector<MutatorFactoryEntry> mMutatorFactories;

		virtual void init(const Scene &scene, TracerParams &tracer) override;
		[[nodiscard]] virtual std::shared_ptr<IIntegrator> createIntegrator() const override;

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;
		[[nodiscard]] virtual RenderEntitySocketDescMap getSockets() const override;
		[[nodiscard]] virtual EntityPtrBase getConnected(std::string_view socket) override;
		virtual bool connect(std::string_view socket, const EntityPtrBase &entity) override;

		[[nodiscard]] const auto &getPathWalker() const noexcept { return mWalker; }

		[[nodiscard]] bool hasLights() const noexcept { return mHasLightsM; }

		void setDirectLightSampler(EntityPtr<IEmitterSampler> o) { mDirectLightSampler = std::move(o); }
		[[nodiscard]] const auto &getDirectLightSampler() const noexcept { return mDirectLightSampler; }

		void setLightPortalSampler(EntityPtr<IEmitterSampler> o) { mLightPortalSampler = std::move(o); }
		[[nodiscard]] const auto &getLightPortalSampler() const noexcept { return mLightPortalSampler; }

		void setJittering(bool j) noexcept { mJitter = j; }
		[[nodiscard]] bool isJittering() const noexcept { return mJitter; }

		void setNbSamplesPixel(uf32 s) { mNbSamplesPixel = pl::plclamp<uf32>(s, 1, sizeof(RayBatchMask) * CHAR_BIT); mInvNbSamplesPixel = 1.0 / mNbSamplesPixel; }
		[[nodiscard]] auto getNbSamplesPixel() const noexcept { return mNbSamplesPixel; }

		void setNbSamplesPath(uf32 s) { mWalker.setNbSamplesPath(s); }
		[[nodiscard]] auto getNbSamplesPath() const noexcept { return mWalker.getNbSamplesPath(); }

		void setLightPortalSampleProb(Real p) { mLightPortalSampleProb = p; }
		[[nodiscard]] auto getLightPortalSampleProb() const noexcept { return mLightPortalSampleProb; }

		void setNbSamplesDirectLight(uf32 s) { mNbSamplesDirectLight = pl::plclamp<uf32>(s, 0, 64); mInvNbSamplesDirectLight = mNbSamplesDirectLight ? 1.0 / mNbSamplesDirectLight : 0; }
		[[nodiscard]] auto getNbSamplesDirectLight() const noexcept { return mNbSamplesDirectLight; }

		void setNbSamplesDirectPerLight(uf32 s) { mNbSamplesDirectPerLights = pl::plclamp<uf32>(s, 0, 64); }
		[[nodiscard]] auto getNbSamplesDirectPerLight() const noexcept { return mNbSamplesDirectPerLights; }

		void setMaxPathLengthCamera(uf32 m) { mWalker.setMaxPathLength(m); }
		[[nodiscard]] auto getMaxPathLengthCamera() const noexcept { return mWalker.getMaxPathLength(); }

		void setLayerBouncesMax(uf32 m) { mWalker.setLayerBouncesMax(m); }
		[[nodiscard]] auto getLayerBouncesMax() const noexcept { return mWalker.getLayerBouncesMax(); }

		void setUseRR(bool v) noexcept { mWalker.setUseRR(v); }
		[[nodiscard]] bool isUsingRR() const noexcept { return mWalker.isUsingRR(); }

		void setRRfactor(Real f) noexcept { mWalker.setRRfactor(f); }
		[[nodiscard]] auto getRRfactor() const noexcept { return mWalker.getRRfactor(); }

		void setRRmin(Real f) noexcept { mWalker.setRRmin(f); }
		[[nodiscard]] auto getRRmin() const noexcept { return mWalker.getRRmin(); }

		void setRandomPixel(bool v) noexcept { mRandomPixel = v; }
		[[nodiscard]] bool useRandomPixel() const noexcept { return mRandomPixel; }

		void setTraceShadow(bool v) noexcept { mTraceShadows = v; }
		[[nodiscard]] bool isTracingShadow() const noexcept { return mTraceShadows; }
	};

	class PHOTORT_API IntegratorPath : pl::no_copy, public IntegratorBase, public virtual IIntegratorTile
	{
	public:

		IntegratorPath(const IntegratorFactoryPath &factory);
		virtual ~IntegratorPath() override;

		virtual void integrate(PixelSampleTileLayers &samples, TracerParams &tracer) override;

		virtual void init(const Scene &scene) override;
		virtual void initRNG(Sampler &sampler) override;
		virtual void setPhotonBuffers(std::span<const EntityPtr<ContributionBufferLight>> /*photonBuffers*/) override {}

		[[nodiscard]] virtual StatsAccumulatorList getStats() const override;
		virtual void resetStats() override;

	protected:

		PRT_MAKE_STATIC_PROFILE_TAG(tag_DirectLight,  "direct_light")
		PRT_MAKE_STATIC_PROFILE_TAG(tag_Background,   "background")
		PRT_MAKE_STATIC_PROFILE_TAG(tag_Emission,     "emission")
		using IntProfiler = Profiler::impl::Profiler<tag_DirectLight_t, tag_Background_t, tag_Emission_t>;

		mutable IntProfiler mProfiler;

		std::vector<MutatorEntry> mMutators;

		pl::stable_block_vector<MaterialInteraction, u32, 10> mInteractions;

		Real mLpSampleProb = 0;

		const IntegratorFactoryPath &mFactory;

		statscollection::Stats mStats;

		GroupedContribution mContribution;

	};

}
