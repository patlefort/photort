/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Sampling_fwd.hpp>

#include <PhotoRT/Core/Base.hpp>

#include <PhotoRT/Core/Integrator.hpp>
#include <PhotoRT/Core/PathWalker.hpp>

#include <patlib/scoped.hpp>

#include <forward_list>

#include <boost/hana/map.hpp>

namespace PhotoRT::BidirIntegrator
{
	using AccumulatorConnection = StatsAccumulator<
		StatCounter,
		stats::StatMin, stats::StatMax, stats::StatAvg, stats::StatSum
	>;

	using AccumulatorConnectionOccluded = StatsAccumulator<
		StatCounter,
		stats::StatSum
	>;

	using AccumulatorConnectionRR = StatsAccumulator<
		StatCounter,
		stats::StatMin, stats::StatMax, stats::StatAvg, stats::StatSum
	>;

	namespace statscollection
	{
		using namespace stats::collection;

		PL_MAKE_HANA_TAG(LightPath)
		PL_MAKE_HANA_TAG(Conn)
		PL_MAKE_HANA_TAG(ConnRR)
		PL_MAKE_HANA_TAG(ConnOccluded)

		using Stats = hn::map<
			hn::pair<type_c<Sample_t>,               stats::AccumulatorSample>,
			hn::pair<type_c<ShadowRays_t>,           stats::AccumulatorShadowRays>,
			hn::pair<type_c<Path_t>,                 stats::AccumulatorPath>,
			hn::pair<type_c<LightPath_t>,            stats::AccumulatorPath>,
			hn::pair<type_c<Conn_t>,                 AccumulatorConnection>,
			hn::pair<type_c<ConnRR_t>,               AccumulatorConnectionRR>,
			hn::pair<type_c<ShadowRaysOccluded_t>,   stats::AccumulatorShadowRaysOccluded>,
			hn::pair<type_c<ConnOccluded_t>,         AccumulatorConnectionOccluded>,
			hn::pair<type_c<ZeroContribPath_t>,      stats::AccumulatorZeroContrib>
		>;
	} // namespace statscollection

	struct VertexState
	{
		Real distSqr = 0, pdfForA = 0, pdfTr = 1, contProb = 1;
		VecD dir;
		MaterialInteraction interaction;
		bool delta = false, insideLayer = false;

		decltype(auto) visit(auto&& fc) noexcept { return std::visit(PL_FWD(fc), interaction); }
		decltype(auto) visit(auto&& fc) const noexcept { return std::visit(PL_FWD(fc), interaction); }

		[[nodiscard]] auto localPos() const noexcept { return visit([&](auto &i){ return i.localPos(); }); }
		[[nodiscard]] auto worldPos() const noexcept { return visit([&](auto &i){ return i.worldPos(); }); }

		[[nodiscard]] auto geometricTermDir(VecD dir) const noexcept
			{ return visit([&](const auto &i){ return i.geometricTermDir(dir); }); }
	};

	struct ConnectionVertex
	{
		VertexState state;
		Color contrib, rrContrib;
		Real misWeight;
	};

	template <typename T_>
	struct Path
	{
		//using VecListType = std::list<ConnectionVertex>;
		using VecListType = std::list<ConnectionVertex, std::pmr::polymorphic_allocator<ConnectionVertex>>;
		//using VecListType = pl::stable_block_vector<ConnectionVertex, u32, 10, std::pmr::polymorphic_allocator<ConnectionVertex>>;
		//using VecListType = pl::stable_block_vector<ConnectionVertex, u32, 10>;
		using VertexIterator = VecListType::iterator;
		using ConstVertexIterator = VecListType::const_iterator;

		const T_ *startedFrom = nullptr;
		const IEmitter *lp = nullptr;
		const MaterialInteraction *lpSurface = nullptr;

		VertexIterator sampleStart;
		Real pdf = 0, weight = 1, infPdfTr = 1;
		MisWeight<Real> misWeight{};
		bool deltaSource = false;

		#ifndef NDEBUG
			szt debugSize = 0;
		#endif

		pl::back_buffered<pl::uninitialized_value<VertexState>> vertexState;

		VecListType vertex;

		Path(std::pmr::memory_resource &mr = *std::pmr::get_default_resource())
			 : vertex{VecListType::allocator_type{&mr}}
			{}
		Path(const T_ &from, Real p, Real w, std::pmr::memory_resource &mr = *std::pmr::get_default_resource()) :
			startedFrom{&from}, pdf{p}, weight{w}
			, vertex{VecListType::allocator_type{&mr}}
			{}
		/*Path(const T_ &from, Real p, Real w) :
			startedFrom{&from}, pdf{p}, weight{w} {}*/

		void clear() noexcept
		{
			vertex.clear();
			sampleStart = {};
		}

	};

	class PHOTORT_API IntegratorFactoryBidir : public RenderEntity, public IIntegratorFactory
	{
	protected:

		bool mJitter = true, mConnectLightToCamera = true, mRandomPixel = false;
		uf32 mNbSamplesPixel = 1, mNbSamplesLight = 2, mNbSamplesDirectPerLights = 1, mNbSamplesDirectLight = 4;
		Real mInvNbSamplesPixel = 1, mInvNbSamplesDirectLight = 1.0 / 4.0;

		bool mHasLightsM = false;

		PathWalker mCameraWalker, mLightWalker;

		//EntityPtr<IMISHeuristic> mMisHeuristic;
		EntityPtr<IEmitterSampler> mLightSampler, mLightPortalSampler;

		Real mLightPortalSampleProb = 0.5, mLightPortalSampleProbDL = 0.5;

	public:
		PRT_ENTITY_FACTORY_DECLARE("int_bidir-", IntegratorFactoryBidir)

		std::vector<MutatorFactoryEntry> mMutatorFactories;

		[[nodiscard]] bool hasLights() const noexcept { return mHasLightsM; }

		virtual void init(const Scene &scene, TracerParams &tracer) override;
		[[nodiscard]] virtual std::shared_ptr<IIntegrator> createIntegrator() const override;

		virtual void setParameters(const ParameterTree &params) override;
		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;
		[[nodiscard]] virtual RenderEntitySocketDescMap getSockets() const override;
		[[nodiscard]] virtual EntityPtrBase getConnected(std::string_view socket) override;
		virtual bool connect(std::string_view socket, const EntityPtrBase &entity) override;

		[[nodiscard]] const auto &getCameraPathWalker() const noexcept { return mCameraWalker; }
		[[nodiscard]] const auto &getLightPathWalker() const noexcept { return mLightWalker; }

		void setLightSampler(EntityPtr<IEmitterSampler> o) { mLightSampler = std::move(o); }
		[[nodiscard]] const auto &getLightSampler() const noexcept { return mLightSampler; }

		void setLightPortalSampler(EntityPtr<IEmitterSampler> o) { mLightPortalSampler = std::move(o); }
		[[nodiscard]] const auto &getLightPortalSampler() const noexcept { return mLightPortalSampler; }

		void setNbSamplesPixel(uf32 s) { mNbSamplesPixel = pl::plclamp<uf32>(s, 1, sizeof(RayBatchMask) * CHAR_BIT); mInvNbSamplesPixel = 1.0 / mNbSamplesPixel; }
		[[nodiscard]] auto getNbSamplesPixel() const noexcept { return mNbSamplesPixel; }

		void setNbSamplesPath(uf32 s) { mCameraWalker.setNbSamplesPath(s); }
		[[nodiscard]] auto getNbSamplesPath() const noexcept { return mCameraWalker.getNbSamplesPath(); }

		void setNbSamplesLight(uf32 s) { mNbSamplesLight = pl::plclamp<uf32>(s, 0, 64); }
		[[nodiscard]] auto getNbSamplesLight() const noexcept { return mNbSamplesLight; }

		void setNbSamplesLightPath(uf32 s) { mLightWalker.setNbSamplesPath(s); }
		[[nodiscard]] auto getNbSamplesLightPath() const noexcept { return mLightWalker.getNbSamplesPath(); }

		void setNbSamplesDirectLight(uf32 s) { mNbSamplesDirectLight = pl::plclamp<uf32>(s, 0, 64); mInvNbSamplesDirectLight = mNbSamplesDirectLight ? 1.0 / mNbSamplesDirectLight : 0; }
		[[nodiscard]] auto getNbSamplesDirectLight() const noexcept { return mNbSamplesDirectLight; }

		void setNbSamplesDirectPerLight(uf32 s) { mNbSamplesDirectPerLights = pl::plclamp<uf32>(s, 0, 64); }
		[[nodiscard]] auto getNbSamplesDirectPerLight() const noexcept { return mNbSamplesDirectPerLights; }

		void setLightPortalSampleProb(Real p) { mLightPortalSampleProb = p; }
		[[nodiscard]] auto getLightPortalSampleProb() const noexcept { return mLightPortalSampleProb; }

		void setLightPortalSampleProbDL(Real p) { mLightPortalSampleProbDL = p; }
		[[nodiscard]] auto getLightPortalSampleProbDL() const noexcept { return mLightPortalSampleProbDL; }

		void setMaxPathLengthLight(uf32 m) { mLightWalker.setMaxPathLength(m); }
		[[nodiscard]] auto getMaxPathLengthLight() const noexcept { return mLightWalker.getMaxPathLength(); }
		void setMaxPathLengthCamera(uf32 m) { mCameraWalker.setMaxPathLength(m); }
		[[nodiscard]] auto getMaxPathLengthCamera() const noexcept { return mCameraWalker.getMaxPathLength(); }

		void setLayerBouncesMax(uf32 m) { mCameraWalker.setLayerBouncesMax(m); mLightWalker.setLayerBouncesMax(m); }
		[[nodiscard]] auto getLayerBouncesMax() const noexcept { return mCameraWalker.getLayerBouncesMax(); }

		void setUseRR(bool v) noexcept { mCameraWalker.setUseRR(v); mLightWalker.setUseRR(v); }
		[[nodiscard]] bool isUsingRR() const noexcept { return mCameraWalker.isUsingRR(); }

		void setRRfactor(Real f) noexcept { mLightWalker.setRRfactor(f); mCameraWalker.setRRfactor(f); }
		[[nodiscard]] auto getRRfactor() const noexcept { return mCameraWalker.getRRfactor(); }

		void setRRmin(Real f) noexcept { mLightWalker.setRRmin(f); mCameraWalker.setRRmin(f); }
		[[nodiscard]] auto getRRmin() const noexcept { return mCameraWalker.getRRmin(); }

		void setConnectLightToCamera(bool v) { mConnectLightToCamera = v; }
		[[nodiscard]] bool isConnectingLightToCamera() const noexcept { return mConnectLightToCamera; }

		void setJittering(bool j) noexcept { mJitter = j; }
		[[nodiscard]] bool isJittering() const noexcept { return mJitter; }

		void setRandomPixel(bool v) noexcept { mRandomPixel = v; }
		[[nodiscard]] bool useRandomPixel() const noexcept { return mRandomPixel; }

	};

	class PHOTORT_API IntegratorBidir :
		pl::no_copy, public IntegratorBase, public virtual IIntegratorTile, public virtual IIntegratorPhoton
	{
	public:

		IntegratorBidir(const IntegratorFactoryBidir &factory);
		virtual ~IntegratorBidir() override;

		[[nodiscard]] virtual StatsAccumulatorList getStats() const override;
		virtual void resetStats() override;

		virtual void integrate(PixelSampleTileLayers &samples, TracerParams &tracer) override;
		virtual void integrate(Real rayImportance, Real rayDistributionFactor, u32 nbSamples, PhotonSampleList &photonSamples, TracerParams &tracer) override;

		virtual void init(const Scene &scene) override;
		virtual void setPhotonBuffers(std::span<const EntityPtr<ContributionBufferLight>> photonBuffers) override
			{ mPhotonBuffers = photonBuffers; }
		virtual void initRNG(Sampler &sampler) override;

	protected:

		PRT_MAKE_STATIC_PROFILE_TAG(tag_DirectLight,    "direct_light")
		PRT_MAKE_STATIC_PROFILE_TAG(tag_Connections,    "connections")
		PRT_MAKE_STATIC_PROFILE_TAG(tag_Background,     "background")
		PRT_MAKE_STATIC_PROFILE_TAG(tag_Emission,       "emission")
		PRT_MAKE_STATIC_PROFILE_TAG(tag_LightToCamera,  "light_to_camera")
		PRT_MAKE_STATIC_PROFILE_TAG(tag_LightPath,      "light_path")
		PRT_MAKE_STATIC_PROFILE_TAG(tag_CameraPath,     "camera_path")
		using IntProfiler = Profiler::impl::Profiler<
			tag_DirectLight_t, tag_Connections_t, tag_Background_t, tag_Emission_t, tag_LightToCamera_t, tag_LightPath_t, tag_CameraPath_t
		>;

		mutable IntProfiler mProfiler;

		const IntegratorFactoryBidir &mFactory;

		pl::MonotonicBufferResource mMemoryArena{
			//sizeof(ConnectionVertex) * 1024 * 16 + sizeof(Path<IEmitter>) * 128,
			1024 * 1024 * 2,
			std::max({alignof(ConnectionVertex), alignof(Path<IEmitter>), alignof(SurfaceData)})
		};

		using ListPathListType = std::forward_list<Path<IEmitter>, std::pmr::polymorphic_allocator<Path<IEmitter>>>;
		ListPathListType mLightPaths{ListPathListType::allocator_type{&mMemoryArena}};
		/*using ListPathListType = std::forward_list<Path<IEmitter>>;
		ListPathListType mLightPaths;*/

		pl::MonotonicBufferResource mDirectLightPathArena{sizeof(ConnectionVertex), alignof(ConnectionVertex)};
		Path<IEmitter> mDirectLightPath{mDirectLightPathArena};
		//Path<IEmitter> mDirectLightPath;

		const MaterialInteraction *mLightSamplingSurface = nullptr;

		bool mDoBidirMIS;

		statscollection::Stats mStats;

		GroupedContribution mContribution;
		std::span<const EntityPtr<ContributionBufferLight>> mPhotonBuffers;

		//pl::stable_block_vector<MaterialInteraction, u32, 10> mCameraInteractions, mLightInteractions;
		pl::MonotonicBufferResource mMemoryArenaCameraInteractions{
			1024 * 4,
			std::max({alignof(ConnectionVertex), alignof(Path<IEmitter>), alignof(MaterialInteraction)})
		};
		std::pmr::forward_list<MaterialInteraction> mCameraInteractions{&mMemoryArenaCameraInteractions}, mLightInteractions{&mMemoryArena};

		std::vector<MutatorEntry> mMutators;

		Real mLpSampleProb = 0, mLpSampleProbDL = 0;

		[[nodiscard]] Real getLpSampleProb(const IEmitter &light) const noexcept
			{ return light.isInfinite() && !light.isDiracDelta() ? mLpSampleProb : 0; }

		void traceLightPath(TracerParams &tracer, Path<IEmitter> &path, RandomReals<2> rnd);
		void traceCameraPath(const Contributor &contrib, TracerParams &tracer, Path<ICamera> &path, RayBatchEntry &rbEntry, TraversalEntry &trEntry, Real distributionFactor = 1, Real rayImportance = 1);
		void traceLightPaths(TracerParams &tracer, const MaterialInteraction &cameraVertex);

		void clearLightPath() noexcept
		{
			mLightInteractions.clear();
			mLightPaths.clear();
		}
	};

}
