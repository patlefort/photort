/*
	PhotoRT

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/SurfaceMapper_fwd.hpp>

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Primitive/Triangle.hpp>

#include "Base.hpp"

namespace PhotoRT
{
	namespace PrimGroupTriImpl
	{
		template <pl::simd::CNeutralOrPointGeometricVector VertexType_ = VecP, std::integral IndexType_ = PrimitiveIndex, typename Alloc_ = std::allocator<VertexType_>>
		class TriangleBuffer
		{
		public:
			using VertexType = VertexType_;
			using NormalType = pl::simd::rebind_to_dir<VertexType>;
			using IndexType = IndexType_;
			using PrimitiveType = PrimitiveTriangleEx<>;
			using AllocatorType = Alloc_;
			static constexpr uf32 PrimitiveID = PrimitiveType::TypeID;

			static_assert(CPrimitiveTransformable<PrimitiveType, Matrix>);
			static_assert(pl::io::CSerializable<char, PrimitiveType>);

			using VoidAlloc = typename std::allocator_traits<Alloc_>::template rebind_alloc<void>;
			using VertexAlloc = typename std::allocator_traits<Alloc_>::template rebind_alloc<VertexType>;
			using NormalAlloc = typename std::allocator_traits<Alloc_>::template rebind_alloc<NormalType>;

			using VertexList = PrimitiveVectorTypeNoInit<VertexType, VertexAlloc>;
			using NormalList = PrimitiveVectorTypeNoInit<NormalType, NormalAlloc>;

			struct TriangleEntry
			{
				TriangleEntry() = default;
				TriangleEntry(const TriangleEntry &) = default;
				TriangleEntry(IndexType i1, IndexType i2, IndexType i3, const NormalType &n) noexcept
					: vIndex{i1, i2, i3}, normal{n} {}
				TriangleEntry(const std::array<IndexType, 3> &a) noexcept
					: vIndex{a} {};

				std::array<IndexType, 3> vIndex;
				NormalType normal;
			};

			using TriangleAlloc = typename std::allocator_traits<Alloc_>::template rebind_alloc<std::array<IndexType, 3>>;
			using TriangleList = PrimitiveVectorTypeNoInit<std::array<IndexType, 3>, TriangleAlloc>;

			VertexList mVts;
			NormalList mVtsNormals;
			TriangleList mTriangles;
			NormalList mTriangleNormals;

			TriangleBuffer(const VoidAlloc &a) :
				mVts(typename VertexList::allocator_type{a}),
				mVtsNormals(typename NormalList::allocator_type{a}),
				mTriangles(typename TriangleList::allocator_type{a}),
				mTriangleNormals(typename NormalList::allocator_type{a}) {}

			TriangleBuffer(const VertexAlloc &a1 = {}, const NormalAlloc &a2 = {}, const TriangleAlloc &a3 = {}, const NormalAlloc &a4 = {}) :
				mVts(typename VertexList::allocator_type{a1}),
				mVtsNormals(typename NormalList::allocator_type{a2}),
				mTriangles(typename TriangleList::allocator_type{a3}),
				mTriangleNormals(typename NormalList::allocator_type{a4}) {}

			void setAllocator(const VoidAlloc &a)
			{
				pl::recreate(mVts, typename VertexList::allocator_type{a});
				pl::recreate(mVtsNormals, typename NormalList::allocator_type{a});
				pl::recreate(mTriangles, typename TriangleList::allocator_type{a});
				pl::recreate(mTriangleNormals, typename NormalList::allocator_type{a});
			}

			[[nodiscard]] PrimitiveIndex getNbPrimitives() const noexcept
				{ return mTriangles.size(); }

			[[nodiscard]] PrimitiveType getPrimitive(PrimitiveIndex primitiveIndex) const noexcept
				{ return getPrimitive<VecP>(primitiveIndex); }

			[[nodiscard]] TriangleEntry operator[](PrimitiveIndex primitiveIndex) const noexcept
				{ return {.vIndex = mTriangles[primitiveIndex], .normal = mTriangleNormals[primitiveIndex]}; }

			void setTriangleEntry(const TriangleEntry &t, PrimitiveIndex primitiveIndex)
			{
				mTriangles[primitiveIndex] = t.vIndex;
				mTriangleNormals[primitiveIndex] = t.normal;
			}

			template <pl::simd::CNeutralOrPointGeometricVector VT_>
			[[nodiscard]] PrimitiveTriangleEx<VT_> getPrimitive(PrimitiveIndex primitiveIndex) const noexcept
			{
				const auto &tri = mTriangles[primitiveIndex];
				const auto &trin = mTriangleNormals[primitiveIndex];

				if(mVtsNormals.empty())
				{
					return {
						mVts[tri[0]], mVts[tri[1]], mVts[tri[2]],
						trin,
						trin, trin, trin
					};
				}
				else
				{
					return {
						mVts[tri[0]], mVts[tri[1]], mVts[tri[2]],
						trin,
						mVtsNormals[tri[0]], mVtsNormals[tri[1]], mVtsNormals[tri[2]]
					};
				}
			}

			[[nodiscard]] PrimitiveTriangle<> getTriangle(PrimitiveIndex primitiveIndex) const noexcept
			{
				const auto &tri = mTriangles[primitiveIndex];

				return {mVts[tri[0]], mVts[tri[1]], mVts[tri[2]]};
			}

			template <pl::simd::CNeutralOrPointGeometricVector VT_>
			[[nodiscard]] PrimitiveTriangle<VT_> getTriangle(PrimitiveIndex primitiveIndex) const noexcept
			{
				const auto &tri = mTriangles[primitiveIndex];

				return {mVts[tri[0]], mVts[tri[1]], mVts[tri[2]]};
			}

			[[nodiscard]] PrimitiveTriangle<> getPrimitiveMinimal(PrimitiveIndex primitiveIndex) const noexcept
				{ return getTriangle(primitiveIndex); }

			template <pl::simd::CNeutralOrPointGeometricVector VT_>
			[[nodiscard]] PrimitiveTriangle<VT_> getPrimitiveMinimal(PrimitiveIndex primitiveIndex) const noexcept
				{ return getTriangle<VT_>(primitiveIndex); }

			decltype(auto) add(const TriangleEntry &tri)
			{
				mTriangleNormals.push_back(tri.normal);
				return mTriangles.push_back(tri.vIndex);
			}

			void clear() noexcept
			{
				mTriangles.clear();
				mTriangles.shrink_to_fit();
				mTriangleNormals.clear();
				mTriangleNormals.shrink_to_fit();
				mVts.clear();
				mVts.shrink_to_fit();
				mVtsNormals.clear();
				mVtsNormals.shrink_to_fit();
			}
		};


		class PHOTORT_API PrimGroupTriangles final : public PrimitiveGroupListBase<TriangleBuffer<VecP3r, PrimitiveIndex, PmrAllocatorGlobal<VecP3r, memtag::Primitive>>>
		{
		private:
			using ThisBase = PrimitiveGroupListBase<TriangleBuffer<VecP3r, PrimitiveIndex, PmrAllocatorGlobal<VecP3r, memtag::Primitive>>>;

		public:
			PRT_ENTITY_FACTORY_DECLARE("object-tris", PrimGroupTriangles)

			using VT = VecP3r;
			using NT = pl::simd::rebind_to_dir<VT>;

			struct TriOrientationData
			{
				PrimitiveIndex index;
				std::array<PrimitiveIndex, 3> connected;
				char checkIndex;
				std::array<std::array<u8, 2>, 3> connIndex;
			};

			std::vector<PrimitiveIndex, pl::allocator_noinit<PrimitiveIndex>> mVtsTriIndex, mVtsTriIndexStart;

			virtual void setParameters(const ParameterTree &params) override;
			[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override;

			virtual void serialize(std::ostream &outputStream) const override;
			virtual void unserialize(std::istream &inputStream) override;

			[[nodiscard]] virtual bool hasData(bool ipcStream = false) const noexcept override { return ThisBase::hasData() || !ipcStream; }
			[[nodiscard]] virtual bool canHaveData() const noexcept override { return true; }

			[[nodiscard]] auto getTriangle(const PrimitiveIndex index) const noexcept { return mData->primitives.getTriangle(index); }
			template <typename VT_>
			[[nodiscard]] auto getTriangle(const PrimitiveIndex index) const noexcept { return mData->primitives.getTriangle<VT_>(index); }

			virtual Box computeAABox() noexcept override;

			[[nodiscard]] virtual VecP getPointOnSurface(PrimitiveIndex primitiveIndex = 0) const noexcept override
				{ return mData->primitives.mVts[mData->primitives.mTriangles[primitiveIndex][0]]; }

			[[nodiscard]] virtual VecD getSurfaceNormal(VecP /*position*/, PrimitiveIndex primitiveIndex = 0) const noexcept override
				{ return mData->primitives.mTriangleNormals[primitiveIndex]; }

			[[nodiscard]] virtual VecD getSurfaceDir(VecP /*position*/, PrimitiveIndex primitiveIndex = 0) const noexcept override
				{ return mData->primitives.mTriangleNormals[primitiveIndex]; }

			[[nodiscard]] virtual pl::marked_optional<SampledAreaResultBackface<VecP>> sampleVisibleSurfaceArea(RandomReals<2> rnd, VecP point, PrimitiveIndex primitiveIndex = 0) const noexcept override
			{
				return mData->primitives.getPrimitive(primitiveIndex).sampleVisibleSurfaceArea(rnd, point);
			}

			[[nodiscard]] virtual pl::marked_optional<SampledAreaResult<VecP>> sampleArea(RandomReals<2> rnd, PrimitiveIndex primitiveIndex = 0) const noexcept override
				{ return mData->primitives.getPrimitive(primitiveIndex).sampleArea(rnd); }

			[[nodiscard]] virtual Matrix getTextureTangentSpace(const SurfaceData &surfaceData, const UVMapping *tcMaps, const Matrix &texSpaceTransform) const noexcept override;

			virtual void getShadingInfo(SurfaceData &surfaceData, PrimitiveIndex primIndex = 0) const noexcept override
			{
				//mData->primitives.getPrimitive(primIndex).getShadingInfo(surfaceData);
				const auto p = mData->primitives.getPrimitive(primIndex);

				surfaceData.shadingNormal = pl::assert_valid( p.getInterpolatedNormal(static_cast<VecP>(surfaceData.uv)) );
				surfaceData.pMat = pl::assert_valid( p.getTangentSpace(surfaceData.localPos, type_v<Matrix>) );
			}

			void initCube(Real sizex, Real sizey, Real sizez);
			void initCube(Real posx1, Real posy1, Real posz1, Real posx2, Real posy2, Real posz2);
			void initMap(Real tilesizex, Real tilesizez, PrimitiveIndex nbTileX, PrimitiveIndex nbTileZ);
			void initSphere(Real sizex, Real sizey, Real sizez, PrimitiveIndex nbStepsRad, PrimitiveIndex nbStepsCirc);

			PrimitiveIndex addTriangle(PrimitiveIndex number = 1);

			auto addVertex(PrimitiveIndex number = 1)
			{
				mData->primitives.mVts.resize(mData->primitives.mVts.size() + number);

				return mData->primitives.mVts.begin() + (mData->primitives.mVts.size() - number);
			}

			auto addNormal(PrimitiveIndex number = 1)
			{
				mData->primitives.mVtsNormals.resize(mData->primitives.mVtsNormals.size() + number);

				return mData->primitives.mVtsNormals.begin() + (mData->primitives.mVtsNormals.size() - number);
			}

			void removeVertices(PrimitiveIndex number = 1);
			void removeTriangles(PrimitiveIndex number = 1);
			void removeNormals(PrimitiveIndex number = 1);
			void clearVertices() noexcept;
			void clearTriangles() noexcept;
			void clearNormals() noexcept;
			void clearAll() noexcept;

			void clearUVs() noexcept;

			void buildVtsTriIndex();
			void clearVtsTriIndex() noexcept;

			void alignToAxis(int axis, bool left) noexcept;

			void computeSmoothVerticesNormals(Real maxdist);
			bool computeTriangleNormals(bool removeIfInvalid = false);
			void invertNormals() noexcept;

			void removeDuplicateVertices();
			void orientTriangles(PrimitiveIndex refTriIndex = 0) noexcept;
			void reorientNormals() noexcept;
			void swapTrianglesOrientation() noexcept;
			void findConnectedTriangles(TriOrientationData &data);
			void moveVertices(const VecD &av);
			void transformVertices(const Matrix &m);

			[[nodiscard]] bool vertexNormalsValid() const noexcept;
			[[nodiscard]] bool verticesValid() const noexcept;
			[[nodiscard]] bool trianglesValid() const noexcept;
			[[nodiscard]] bool uvsValid() const noexcept;

			bool checkAndFixVertexNormals();

			void computeTangents(const Matrix &texSpaceTransform);
			szt computeUVmapping(const IUVMapper &uvMapper);

		};
	} // namespace PrimGroupTriImpl

	using PrimGroupTriImpl::PrimGroupTriangles;
}
