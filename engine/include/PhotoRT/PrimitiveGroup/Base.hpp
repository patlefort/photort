/*
	PhotoRT

	Copyright (C) 2019 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/PrimitiveGroup/Base_fwd.hpp>

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/PrimitiveGroup.hpp>
#include <PhotoRT/Core/Serialization.hpp>
#include <PhotoRT/Core/Distribution.hpp>

#include <patlib/io.hpp>

namespace PhotoRT
{
	template <typename PrimitiveListType_>
	class PrimitiveGroupListBase : public PrimitiveGroupBase
	{
	protected:
		Box mAabox{pl::tag::nan};

		VectorWithStats<Real, memtag::Other> cdfData, surfaceAreas;

		[[nodiscard]] static auto getCdfImpl(auto &this_) noexcept
		{
			return CDFDistribution{
				this_.cdfData.size(),
				this_.surfaceAreas.data(),
				this_.cdfData.data()
			};
		}

		[[nodiscard]] auto getCdf() noexcept { return getCdfImpl(*this); }
		[[nodiscard]] auto getCdf() const noexcept { return getCdfImpl(*this); }

		[[nodiscard]] auto getPrimitiveRange() noexcept
		{
			return rgv::iota(PrimitiveIndex{}, mData->primitives.getNbPrimitives())
				| rgv::transform([&](auto i) -> decltype(auto) { return mData->primitives.getPrimitive(i); });
		}

		[[nodiscard]] auto getPrimitiveRange() const noexcept
		{
			return rgv::iota(PrimitiveIndex{}, mData->primitives.getNbPrimitives())
				| rgv::transform([&](auto i) -> decltype(auto) { return mData->primitives.getPrimitive(i); });
		}

	public:

		struct Data
		{
			PrimitiveListType_ primitives;
			PrimitiveVectorType<UVMapping, PmrAllocatorGlobal<UVMapping, memtag::Primitive>> uvMaps;
		};

		pl::memory_system_pointer<Data, pl::IMemorySystem<VoidOffsetPtr>> mData{};

		virtual void setEntityId(std::string v) override
		{
			auto &memSys = *getMemorySystem<memtag::Primitive>();
			std::string buffId = entityCollection ? entityCollection->getShmID() + v : v;

			mData.find_or_construct(memSys, std::move(buffId), boost::indeterminate);

			PrimitiveGroupBase::setEntityId(std::move(v));
		}

		static constexpr uf32 PrimitiveID = PrimitiveListType_::PrimitiveID;
		using PrimitiveListType = PrimitiveListType_;
		using PrimitiveType = typename PrimitiveListType_::PrimitiveType;

		[[nodiscard]] virtual bool isInfinite() const noexcept override { return PrimitiveType::Infinite; }
		[[nodiscard]] virtual bool isAlwaysFlat() const noexcept override { return PrimitiveType::AlwaysFlat; }
		[[nodiscard]] virtual uf32 getType() const noexcept override { return PrimitiveID; }

		[[nodiscard]] virtual PrimitiveIndex getNbPrimitives() const noexcept override
			{ return mData->primitives.getNbPrimitives(); }

		[[nodiscard]] decltype(auto) getPrimitive(PrimitiveIndex primitiveIndex) const noexcept
			{ return mData->primitives.getPrimitive(primitiveIndex); }

		[[nodiscard]] decltype(auto) getPrimitiveMinimal(PrimitiveIndex primitiveIndex) const noexcept
			{ return mData->primitives.getPrimitive(primitiveIndex); }

		template <typename VT_>
		[[nodiscard]] auto getPrimitive(const PrimitiveIndex primitiveIndex) const noexcept
			{ return mData->primitives.template getPrimitive<VT_>(primitiveIndex); }

		template <typename VT_>
		[[nodiscard]] auto getPrimitiveMinimal(const PrimitiveIndex primitiveIndex) const noexcept
			{ return mData->primitives.template getPrimitive<VT_>(primitiveIndex); }

		[[nodiscard]] virtual Box getPrimitiveAABox(PrimitiveIndex primitiveIndex = 0) const noexcept override
			{ return mData->primitives.getPrimitive(primitiveIndex).getAABox(); }

		[[nodiscard]] virtual VecP getPrimitiveMiddle(PrimitiveIndex primitiveIndex = 0) const noexcept override
			{ return mData->primitives.getPrimitive(primitiveIndex).getMiddle(); }

		[[nodiscard]] virtual VecP getPointOnSurface(PrimitiveIndex primitiveIndex = 0) const noexcept override
			{ return mData->primitives.getPrimitive(primitiveIndex).getPointOnSurface(); }

		[[nodiscard]] virtual VecD getSurfaceNormal(VecP position, PrimitiveIndex primitiveIndex = 0) const noexcept override
		{
			return mData->primitives.getPrimitive(primitiveIndex).getSurfaceNormal(position);
		}

		[[nodiscard]] virtual VecD getSurfaceDir(VecP position, PrimitiveIndex primitiveIndex = 0) const noexcept override
		{
			return mData->primitives.getPrimitive(primitiveIndex).getSurfaceDir(position);
		}

		virtual void buildCdf() override
		{
			const auto nb = getNbPrimitives();

			if(cdfData.empty() && nb > 1)
			{
				surfaceAreas.reserve(nb);

				for(const auto i : mir(nb))
					surfaceAreas.push_back(getSurfaceArea(i));

				cdfData.resize(nb + 2);

				auto cdf = getCdf();
				cdf.createDistribution();
			}
		}

		[[nodiscard]] virtual bool isCdfBuilt() const noexcept override { return !cdfData.empty(); }

		virtual void clearCdf() noexcept override
		{
			cdfData.clear();
			cdfData.shrink_to_fit();
			surfaceAreas.clear();
			surfaceAreas.shrink_to_fit();
		}

		[[nodiscard]] virtual Real PMF(PrimitiveIndex primitiveIndex = 0) const noexcept override
		{
			const auto invNb = r_(getNbPrimitives()) /oprtreciprocal;

			if(cdfData.empty())
				return invNb;

			return getCdf().PMF(primitiveIndex) * invNb;
		}

		[[nodiscard]] virtual SampledPrimitive samplePrimitive(RandomReal r) const noexcept override
		{
			const auto invNb = r_(getNbPrimitives()) /oprtreciprocal;

			if(cdfData.empty())
			{
				const auto nb = getNbPrimitives();
				const auto [roff, i] = remapRng(r, nb);
				plassert(i < nb);
				return {.index = i, .pdf = invNb, .offset = roff};
			}

			const auto cdf = getCdf();
			const auto index = cdf(r);
			plassert(index < getNbPrimitives());

			return {.index = (PrimitiveIndex)index, .pdf = cdf.PMF(index) * invNb, .offset = cdf.getSampleLocalOffset(r, index)};
		}

		[[nodiscard]] virtual OptionalPDF<Real> PDF(PrimitiveIndex primitiveIndex = 0) const noexcept override
		{
			if constexpr(PrimitiveType::Infinite)
				return pl::nullopt;
			else
				return mData->primitives.getPrimitive(primitiveIndex).PDF();
		}

		[[nodiscard]] virtual OptionalPDF<Real> PDF(const Matrix &m, PrimitiveIndex primitiveIndex = 0) const noexcept override
		{
			if constexpr(PrimitiveType::Infinite)
				return pl::nullopt;
			else
			{
				auto p = mData->primitives.getPrimitive(primitiveIndex);
				p *= m;
				return p.PDF();
			}
		}

		[[nodiscard]] virtual OptionalPDF<Real> PDF(VecP point, VecD dir, VecD dirInv, PrimitiveIndex primitiveIndex = 0, pl::marked_optional<const SampledAreaResult<VecP> &> sampled = pl::nullopt) const noexcept override
		{
			if constexpr(PrimitiveType::Infinite)
				return pl::nullopt;
			else
				return mData->primitives.getPrimitive(primitiveIndex).PDF(point, dir, dirInv, sampled);
		}

		[[nodiscard]] virtual OptionalPDF<Real> PDF(const Matrix &m, VecP point, VecD dir, VecD dirInv, PrimitiveIndex primitiveIndex = 0, pl::marked_optional<const SampledAreaResult<VecP> &> sampled = pl::nullopt) const noexcept override
		{
			if constexpr(PrimitiveType::Infinite)
				return pl::nullopt;
			else
			{
				auto p = mData->primitives.getPrimitive(primitiveIndex);
				p *= m;
				return p.PDF(point, dir, dirInv, sampled);
			}
		}

		[[nodiscard]] virtual pl::marked_optional<SampledAreaResultBackface<VecP>> sampleVisibleSurfaceArea(RandomReals<2> rnd, VecP point, PrimitiveIndex primitiveIndex = 0) const noexcept override
		{
			if constexpr(PrimitiveType::Infinite)
				return pl::nullopt;
			else
				return mData->primitives.getPrimitive(primitiveIndex).sampleVisibleSurfaceArea(rnd, point);
		}

		[[nodiscard]] virtual Real getSurfaceArea(PrimitiveIndex primitiveIndex = 0) const noexcept override
		{
			return mData->primitives.getPrimitive(primitiveIndex).getSurfaceArea();
		}

		virtual pl::marked_optional<SampledAreaResult<VecP>> sampleArea(RandomReals<2> rnd, PrimitiveIndex primitiveIndex = 0) const noexcept override
		{
			if constexpr(PrimitiveType::Infinite)
				return pl::nullopt;
			else
				return mData->primitives.getPrimitive(primitiveIndex).sampleArea(rnd);
		}

		[[nodiscard]] virtual std::pair<bool, StatCounter> intersect(const VecP rayOrigin, const VecD rayDir, const VecD rayDirReciprocal) const noexcept override
		{
			bool gotHit = false;
			const auto nb = mData->primitives.getNbPrimitives();
			StatCounter nbt = 0;

			for(const auto i : mir(nb))
			{
				++nbt;
				if(mData->primitives.getPrimitive(i).intersect(rayOrigin, rayDir, rayDirReciprocal))
				{
					gotHit = true;
					break;
				}
			}

			return {gotHit, nbt};
		}

		[[nodiscard]] virtual std::pair<pl::marked_optional<PrimGroupCollision>, StatCounter> intersectExt(const VecP rayOrigin, const VecD rayDir, const VecD rayDirReciprocal, Real dist = std::numeric_limits<Real>::infinity(), bool calcUv = false) const noexcept override
		{
			bool gotHit = false;
			PrimGroupCollision collision;

			const auto nb = mData->primitives.getNbPrimitives();

			collision.dist = dist;

			for(const auto i : mir(nb))
			{
				if(const auto res = mData->primitives.getPrimitive(i).intersectExt(rayOrigin, rayDir, rayDirReciprocal, collision.dist, calcUv); res)
				{
					gotHit = true;
					static_cast<Collision<> &>(collision) = *res;
					collision.primIndex = i;
				}
			}

			if(gotHit)
				return {collision, (StatCounter)nb};

			return {pl::nullopt, (StatCounter)nb};
		}

		[[nodiscard]] virtual pl::marked_optional<PrimGroupCollision> intersectExt(PrimitiveIndex primitiveIndex, const VecP rayOrigin, const VecD rayDir, const VecD rayDirReciprocal, Real dist = std::numeric_limits<Real>::infinity(), bool calcUv = false) const noexcept override
		{
			PrimGroupCollision collision;

			collision.primIndex = primitiveIndex;
			collision.dist = dist;

			if(const auto res = mData->primitives.getPrimitive(primitiveIndex).intersectExt(rayOrigin, rayDir, rayDirReciprocal, dist, calcUv); res)
			{
				static_cast<Collision<> &>(collision) = *res;
				return collision;
			}

			return pl::nullopt;
		}

		[[nodiscard]] virtual bool intersect(const Box &box) const noexcept override
		{
			return rg::any_of(getPrimitiveRange(), [&](auto&& p){ return p.intersect(box); });
		}

		[[nodiscard]] virtual Box getAABox() const noexcept override
		{
			return mAabox;
		}

		virtual Box computeAABox() noexcept override
		{
			if(isInfinite())
				mAabox.set_infmax();
			else
				mAabox = rg::accumulate(
						getPrimitiveRange(),
						Box{pl::tag::inflowest},
						pl::accumulator_join,
						[&](const auto &p){ return pl::assert_valid(p.getAABox()); }
					);

			return mAabox;
		}

		virtual void getShadingInfo(SurfaceData &surfaceData, PrimitiveIndex primIndex = 0) const noexcept override
		{
			const auto p = getPrimitive(primIndex);
			surfaceData.shadingNormal = pl::assert_valid(surfaceData.surfaceNormal);
			surfaceData.pMat = pl::assert_valid( p.getTangentSpace(surfaceData.localPos, type_v<Matrix>) );
		}

		[[nodiscard]] virtual Real getMinimalSafeIncrement(PrimitiveIndex primIndex = 0) const noexcept override
		{
			return mData->primitives.getPrimitive(primIndex).getMinimalSafeIncrement();
		}

		[[nodiscard]] virtual Matrix getTextureTangentSpace(const SurfaceData &surfaceData, const UVMapping *tcMaps, const Matrix &texSpaceTransform) const noexcept override
		{
			const auto uv = tcMaps ? VecP{tcMaps->uv[surfaceData.primIndex]} : VecP{pl::tag::identity};

			const auto primSpaceToTexSpace = TransformTranslation(uv) * texSpaceTransform;
			Matrix tangentSpace;

			if(tcMaps && !tcMaps->tangents.empty())
			{
				const auto &tg = tcMaps->tangents[surfaceData.primIndex];
				tangentSpace[0] = tg[0];
				tangentSpace[1] = tg[1];
				tangentSpace[2] = {0, 0, 1, 0};
				tangentSpace[3].set_identity();
				tangentSpace = surfaceData.worldMatrix * tangentSpace;

				const pl::scoped_transpose sg{tangentSpace};
				tangentSpace[3] = primSpaceToTexSpace * surfaceData.uv;
			}
			else if(const auto texSpaceToPrimSpace = primSpaceToTexSpace.inverse(); texSpaceToPrimSpace)
			{
				tangentSpace = *texSpaceToPrimSpace * surfaceData.pMat;

				const pl::scoped_transpose sg{tangentSpace};
				tangentSpace[3] = primSpaceToTexSpace * surfaceData.uv;
			}
			else
				tangentSpace.set_identity();

			return pl::assert_valid(tangentSpace);
		}

		[[nodiscard]] virtual const UVMapping &getUVmap(u32 mapIndex) const noexcept override { return mData->uvMaps[mapIndex]; }

	private:

		static auto buildUVserializer(auto &pg, bool isDouble)
		{
			// UV maps
			return pl::io::container(pg.mData->uvMaps, pl::io::mode_select, false,
				pl::io::default_initialize_proj(type_v<UVMapping>, [isDouble](auto, auto&& fc, auto &m)
				{
					PL_FWD(fc)(pl::io::range{pl::io::mode_select, '\n', std::make_tuple(
						// UVs
						pl::io::container(m.uv, pl::io::mode_select, (isDouble ? sizeof(double) : sizeof(float)) == sizeof(UV::value_type),
							pl::io::default_initialize_proj(type_v<UV>,
								conditionalValueTranslationProj(
									type_v<pl::simd::rebind_vector<double, UV>>,
									type_v<pl::simd::rebind_vector<float, UV>>,
									isDouble
								)
							)
						),

						// Tangents
						pl::io::container(m.tangents, pl::io::mode_select, (isDouble ? sizeof(double) : sizeof(float)) == sizeof(UVdir::value_type),
							pl::io::default_initialize_proj(type_v<UVDirPair>,
								conditionalValueTranslationProj(
									type_v<pl::simd::rebind_vector_under_t<double, UVDirPair>>,
									type_v<pl::simd::rebind_vector_under_t<float, UVDirPair>>,
									isDouble
								)
							)
						)
					)});
				})
			);
		}

	public:

		virtual void serializeUVmaps(std::ostream &outputStream) const
		{
			outputStream << buildUVserializer(*this, sizeof(UV::value_type) == sizeof(double)) << '\n';
		}

		virtual void unserializeUVmaps(std::istream &inputStream, bool isDouble)
		{
			inputStream >> buildUVserializer(*this, isDouble) >> pl::io::skip_one;
		}

		virtual void setParameters(const ParameterTree &params) override
		{
			PrimitiveGroupBase::setParameters(params);

			if(const auto p = params.findParam("uvmaps"); p && p->childParams)
			{
				mData->uvMaps.clear();
				mData->uvMaps.shrink_to_fit();

				for(const auto &uvparam : *p->childParams)
				{
					auto &uvmap = mData->uvMaps.emplace_back();

					pl::io::stream_from(uvparam.second.value, pl::io::throw_on_p{std::ios_base::failbit | std::ios_base::badbit}, [&](auto &is, auto&&){
						rg::istream<UV>(is)
							| pl::back_insert_copy(uvmap.uv);
					});
				}
			}
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {PrimitiveGroupBase::getParameters(deep)};

			return params;
		}
	};

	template <template<typename> typename PrimitiveType_, pl::simd::CNeutralOrPointGeometricVector VT_, typename Alloc_ = std::allocator<PrimitiveType_<VT_>>>
		requires (
			CPrimitiveTransformable<PrimitiveType_<VT_>, Matrix> &&
			CPrimitiveTransformable<PrimitiveType_<VecP>, Matrix> &&
			pl::io::CSerializable<char, PrimitiveType_<VT_>>)
	struct PrimitiveBuffer
	{
		static constexpr uf32 PrimitiveID = PrimitiveType_<VT_>::TypeID;
		using VertexType = VT_;
		using PrimitiveType = PrimitiveType_<VecP>;
		using StoredPrimitiveType = PrimitiveType_<VT_>;
		using AllocatorType = Alloc_;
		using PrimitiveListType = PrimitiveVectorTypeNoInit<StoredPrimitiveType, typename std::allocator_traits<AllocatorType>::template rebind_alloc<StoredPrimitiveType>>;

		PrimitiveListType mPrimitives;

		PrimitiveBuffer(const AllocatorType &a = {})
			: mPrimitives(typename PrimitiveListType::allocator_type{a}) {}

		void setAllocator(const AllocatorType &a)
		{
			pl::recreate(mPrimitives, typename PrimitiveListType::allocator_type{a});
		}

		[[nodiscard]] PrimitiveIndex getNbPrimitives() const noexcept
			{ return mPrimitives.size(); }

		[[nodiscard]] decltype(auto) getPrimitive(PrimitiveIndex primitiveIndex) const noexcept
		{
			using T = std::conditional_t<std::same_as<VT_, VecP>, const PrimitiveType &, PrimitiveType>;
			return static_cast<T>(mPrimitives[primitiveIndex]);
		}

		[[nodiscard]] PrimitiveType &getPrimitive(PrimitiveIndex primitiveIndex) noexcept
			requires std::same_as<VT_, VecP>
			{ return mPrimitives[primitiveIndex]; }

		template <pl::simd::CNeutralOrPointGeometricVector T_>
		[[nodiscard]] decltype(auto) getPrimitive(PrimitiveIndex primitiveIndex) const noexcept
		{
			using T = std::conditional_t<std::same_as<T_, VT_>, const PrimitiveType_<T_> &, PrimitiveType_<T_>>;
			return static_cast<T>(mPrimitives[primitiveIndex]);
		}

		template <pl::simd::CNeutralOrPointGeometricVector T_>
		[[nodiscard]] PrimitiveType_<T_> &getPrimitive(PrimitiveIndex primitiveIndex) noexcept
			requires std::same_as<T_, VT_>
			{ return mPrimitives[primitiveIndex]; }
	};

	template <template<typename> class PrimitiveType_, pl::simd::CNeutralOrPointGeometricVector VT_>
	class PrimitiveGroup : public PrimitiveGroupListBase<PrimitiveBuffer<PrimitiveType_, VT_, PmrAllocatorGlobal<PrimitiveType_<VT_>, memtag::Primitive>>>
	{
	private:
		using ThisBase = PrimitiveGroupListBase<PrimitiveBuffer<PrimitiveType_, VT_, PmrAllocatorGlobal<PrimitiveType_<VT_>, memtag::Primitive>>>;

	public:

		using PrimitiveListType = typename ThisBase::PrimitiveListType;
		using StoredPrimitiveType = typename PrimitiveListType::StoredPrimitiveType;

		virtual void setParameters(const ParameterTree &params) override
		{
			ThisBase::setParameters(params);

			const auto pNbPrim = params.findParam("nb_primitives");
			const auto pPrims = params.findParam("primitives");
			if(pNbPrim && pPrims)
			{
				szt nbPrimitives = *pNbPrim;

				if(nbPrimitives > 0)
				{
					pl::io::stream_from(pPrims->value, pl::io::throw_on_failure, [&](auto&& is, auto&&){
						auto &prims = this->mData->primitives.mPrimitives;

						prims.clear();
						prims.reserve(nbPrimitives);

						is
							>> pl::io::fillinto{pl::io::mode_text, type_v<rg::range_value_t<decltype(prims)>>, std::back_inserter(prims), nbPrimitives};

						prims.shrink_to_fit();
					});
				}
			}
		}

		[[nodiscard]] virtual ParameterTree getParameters(bool deep = false) const override
		{
			ParameterTree params {ThisBase::getParameters(deep)};

			return params;
		}

		static auto buildPrimitiveSerializer(auto& prims, bool isDouble)
		{
			return
				pl::io::container(prims, pl::io::mode_select, (isDouble ? sizeof(double) : sizeof(float)) == sizeof(typename VT_::value_type),
					pl::io::default_initialize_proj(type_v<StoredPrimitiveType>,
						conditionalValueTranslationProj(
							type_v<RebindPrimitive_t<double, StoredPrimitiveType>>,
							type_v<RebindPrimitive_t<float, StoredPrimitiveType>>,
							isDouble
						)
					)
				);
		}

		virtual void serialize(std::ostream &os) const override
		{
			ThisBase::serialize(os);

			auto &memSys = *getMemorySystem<memtag::Primitive>();

			if(!this->mData || (pl::io::is_ipc(os) && memSys.is_shared()))
			{
				os << '\0';
				return;
			}

			os << sizeof(typename VT_::value_type) << '\n';

			pl::io::stream_safe_from(os, [&](auto &os){
				os << std::hexfloat << std::dec
					<< buildPrimitiveSerializer(this->mData->primitives.mPrimitives, sizeof(typename VT_::value_type) == sizeof(double))
					<< '\n';

				this->serializeUVmaps(os);
			});
		}

		virtual void unserialize(std::istream &is) override
		{
			ThisBase::unserialize(is);

			auto &memSys = *getMemorySystem<memtag::Primitive>();

			if(!this->mData || (pl::io::is_ipc(is) && memSys.is_shared()))
			{
				is >> pl::io::skip_one;
				return;
			}

			const bool isDouble = std::invoke([&]{
					const auto line = pl::io::readline(is);
					const bool is64 = line == "8";

					if(!is64 && line != "4")
						throw Exception("Unsupported floating-point size of " + line + " (4 or 8 bytes supported).");

					return is64;
				});

			auto &prims = this->mData->primitives.mPrimitives;
			prims.clear();

			if( (isDouble ? sizeof(double) : sizeof(float)) != sizeof(typename VT_::value_type) )
				this->log() << "Floating-point format is different and will be translated.";

			pl::io::stream_safe_from(is, [&](auto &is){

				is
					>> buildPrimitiveSerializer(this->mData->primitives.mPrimitives, isDouble)
					>> pl::io::skip_one;

				this->unserializeUVmaps(is, isDouble);
			});

			prims.shrink_to_fit();
		}

		[[nodiscard]] virtual bool hasData(bool ipcStream = false) const noexcept override
		{
			auto &memSys = *getMemorySystem<memtag::Primitive>();
			return ThisBase::hasData() || !ipcStream || !memSys.is_shared();
		}
		[[nodiscard]] virtual bool canHaveData() const noexcept override { return true; }

	};
}
