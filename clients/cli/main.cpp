/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT.hpp>
#include <PhotoRT/Core/Compression.hpp>

#include <PhotortClientLib/Client.hpp>
#include <PhotortClientLib/Proc.hpp>
#include <PhotortClientLib/Network.hpp>

#include <patlib/task.hpp>
#include <patlib/chrono.hpp>
#include <patlib/path.hpp>
#include <patlib/io.hpp>
#include <patlib/string.hpp>

#ifdef PL_NUMA
	#include <patlib/numa.hpp>
#endif

#include <boost/program_options.hpp>
#include <boost/signals2.hpp>

#include <boost/logic/tribool.hpp>
#include <boost/logic/tribool_io.hpp>

#include <boost/optional.hpp>
#include <boost/optional/optional_io.hpp>

#include <csignal>

#ifdef PRT_CLI_NCURSES
	#include <curses.h>
#endif

#include <fmt/format.h>
#include <fmt/chrono.h>

namespace PhotoRT_CLI
{

using namespace std::chrono_literals;

namespace rg = ranges;
namespace rgv = rg::views;

namespace po = boost::program_options;

namespace sa = shared_access;

PRT_IMPORT_NS

namespace paths = pl::paths;

class ApplicationException : public pl::Exception
{
public:
	using pl::Exception::Exception;

	[[nodiscard]] virtual const char *module() const noexcept override
	{
		return "Application";
	}
};

struct CliConfig
{
	bool dumpInfo;
	#ifdef PRT_CLI_NCURSES
		bool noNCurses;
	#endif
	std::filesystem::path modulePath, configFile, sceneFile, clientConfigFile;
	std::string connAddress, connPort, serverPort;
	std::vector<std::filesystem::path> outFile, hdrOutFile;
	uf32 serverMaxClients, autoSaveRenderState, autoSaveRender;
	bool autoFocus;
	std::chrono::seconds::rep renderTimeLimit;
	boost::tribool ioBinary{boost::indeterminate};
	boost::optional<std::string> ioCompressor;
};

void printDump()
{
	std::cout << "Factories:\n";
	prt::factories.dumpFactories(std::cout);
	std::cout << "\n\n";

	std::cout << "Importers/exporters:\n";
	prt::factories.dumpImporterExporter(std::cout);
	std::cout << "\n\n";
}

#ifdef PRT_CLI_NCURSES

	// From https://code.google.com/archive/p/v8-juice/source
	class NcStreambuf : public std::streambuf
	{
	protected:
		WINDOW *m_pnl = nullptr;
		unsigned long m_flags;
		std::ostream * m_os = nullptr;
		std::streambuf * m_old = nullptr;
		bool escSequence = false;
		std::string esc;
		short bc = 0, fc = 0;

		void copy(const NcStreambuf & rhs)
		{
			if( this != &rhs )
			{
				this->m_pnl = rhs.m_pnl;
				this->m_flags = rhs.m_flags;
				this->m_os = rhs.m_os;
				this->m_old = rhs.m_old;
			}
		}

		[[nodiscard]] attr_t translateAttr(std::string::value_type v) const
		{
			attr_t res = A_NORMAL;

			switch(v)
			{
			case '0': break;
			case '1': res = A_BOLD; break;
			case '2': res = A_DIM; break;
			case '4': res = A_UNDERLINE; break;
			case '5': res = A_BLINK; break;
			case '7': res = A_REVERSE; break;
			case '8': res = A_INVIS; break;
			}

			return res;
		}

		[[nodiscard]] short translateColor(std::string::value_type v) const
		{
			short res = 0;

			switch(v)
			{
			case '0': break;
			case '1': res = COLOR_RED; break;
			case '2': res = COLOR_GREEN; break;
			case '3': res = COLOR_YELLOW; break;
			case '4': res = COLOR_BLUE; break;
			case '5': res = COLOR_MAGENTA; break;
			case '6': res = COLOR_CYAN; break;
			case '7': res = COLOR_WHITE; break;
			}

			return res;
		}

		virtual int sync() final
		{
			if( stdscr && this->m_pnl )
			{
				//return (ERR == wrefresh( this->m_pnl )) ? EOF : 0;
				// ^^^ doing wrefresh here can hose panels :(
				/*if( 0 != panel_below(0) )
				{
					update_panels();
					return doupdate();
				}
				else
				{*/
					return (ERR == wrefresh( this->m_pnl ))
						? EOF
						: 0;
				//}
			}
			return EOF;
		}

	public:
		bool refreshOnSpace = false;

		NcStreambuf(WINDOW *p, std::ostream & os, unsigned long curses_attr = 0)
			: m_pnl(p), m_flags(curses_attr), m_os(&os), m_old(os.rdbuf())
		{
			this->setp( 0, 0 );
			this->setg( 0, 0, 0 );
			os.rdbuf( this );
			scrollok(p,true);
			//mvwinch(p, p->_maxy, 0 );
			mvwinch( p, 0, 0 );

			esc.reserve(50);
		}

		NcStreambuf(WINDOW * p, unsigned long curses_attr = 0 )
			: m_pnl(p), m_flags(curses_attr)
		{
			// Tell parent class that we want to call overflow() for each
			// input char:
			this->setp( 0, 0 );
			this->setg( 0, 0, 0 );
			scrollok(p,true);
			//mvwinch( p, p->_maxy, 0 );
			mvwinch( p, 0, 0 );

			esc.reserve(50);
		}

		NcStreambuf(const NcStreambuf & rhs) { this->copy(rhs); }
		NcStreambuf &operator=(const NcStreambuf & rhs)  { this->copy(rhs); return *this; }

		virtual ~NcStreambuf()
		{
			if( this->m_os )
			{
				this->m_os->rdbuf( this->m_old );
			}
		}

		virtual int overflow( int c ) final
		{
			int ret = c;
			if( c != EOF )
			{
				if(escSequence)
				{
					if(c == 'm')
					{
						escSequence = false;

						//short seqAttr = (short)std::atoi(esc.c_str() + 3);

						//wcolor_set(m_pnl, COLOR_PAIRS(seqAttr), nullptr);
						if(esc.size() >= 2)
						{
							if(esc[1] == '0' && esc[2] == '0')
							{
								fc = 0;
								bc = 0;
								wattr_set(m_pnl, A_NORMAL, 0, nullptr);
							}
							else if(esc[1] == '3' && esc.size() >= 3)
							{
								fc = translateColor(esc[2]);
								init_pair(1, fc, bc);
								wcolor_set(m_pnl, 1, nullptr);
							}
							else if(esc[1] == '4' && esc.size() >= 3)
							{
								bc = translateColor(esc[2]);
								init_pair(1, fc, bc);
								wcolor_set(m_pnl, 1, nullptr);
							}
							else
								wattr_on(m_pnl, translateAttr(esc[1]), nullptr);
						}

						esc.clear();
					}
					else
						esc += (std::string::value_type)c;

					return 0;
				}
				else if(c == '\033')
				{
					escSequence = true;
					return 0;
				}
				else if( this->m_flags )
				{
					wattron(this->m_pnl, this->m_flags );
					if( ERR == waddch(this->m_pnl, (chtype)c ) ) ret = EOF;
					wattroff( this->m_pnl, this->m_flags );
				}
				else if( ERR == waddch( this->m_pnl, (chtype)c ) ) ret = EOF;
			}

			if( (EOF==c) || (refreshOnSpace && std::isspace(c)) || c == '\n' )
			{
				if( EOF == this->sync() ) ret = EOF;
			}

			return ret;
		}


	};

#endif

template <typename T_>
constexpr auto makePoValue(T_ &v)
	{ return po::value<T_>(&v); }


CliConfig config;
std::stringstream statsOutput;

void printUsage()
{
	std::cout << "\nUsage: photort [OPTIONS] <Scene file> <output file 1> <output file 2>...\n\n";
	std::cout << "If no scene file is specified, the client will start in network client mode.\n\n";
}

std::atomic<bool> needToEnd{false};

void signalHandler(int sig)
{
	if(sig == SIGTERM || sig == SIGINT)
	{
		pl::log("Application") << "Received signal to end.";
		needToEnd = true;
	}
}

boost::signals2::signal<void()> sigSave, sigQuit, sigSaveState, sigPause;

#ifdef PRT_CLI_NCURSES

	void handleEvents()
	{
		auto c = getch();

		switch(c)
		{
		case 'q':
			sigQuit();
			break;
		case 's':
			sigSave();
			break;
		case 't':
			sigSaveState();
			break;
		case 'p':
			sigPause();
			break;
		}
	}

	WINDOW
		*logWindow = nullptr,
		*errorWindow = nullptr,
		*statsWindow = nullptr,
		*keysWindow = nullptr;

	NcStreambuf *logWindowBuf = nullptr, *errorWindowBuf = nullptr, *errorWindowBuf2 = nullptr, *statsOutputBuf = nullptr;

	std::atomic<bool> needResize{false};

	void doResize()
	{
		//int h, w;
		//getmaxyx(stdscr, h, w);

		//clear();

		//touchwin(logWindow);
		wresize(logWindow, 8, COLS);
		//pl::log("Application") << std::dec << COLS << '/' << LINES;
		//wsetscrreg(logWindow, 0, h - 10);
		wnoutrefresh(logWindow);

		//touchwin(errorWindow);
		wresize(errorWindow, 8, COLS);
		mvwin(errorWindow, 8, 0);
		wnoutrefresh(errorWindow);

		//touchwin(statsWindow);
		wresize(statsWindow, LINES - 17, COLS);
		mvwin(statsWindow, 16, 0);
		//wsetscrreg(statsWindow, 0, 9);
		wnoutrefresh(statsWindow);

		//touchwin(keysWindow);
		mvwin(keysWindow, LINES - 1, 0);
		wnoutrefresh(keysWindow);

		doupdate();
	}

	void initNC()
	{
		//slk_init(2);
		initscr();
		start_color();
		use_default_colors();
		cbreak();
		noecho();
		nodelay(stdscr, true);
		keypad(stdscr, true);
		intrflush(stdscr, false);
		curs_set(0);

		/*slk_set(1, "[Q]uit", 0);
		slk_set(2, "[S]ave", 0);
		slk_set(3, "Save HDR", 0);

		slk_refresh();*/
	}

	void updateSize()
	{
		//deleteWindows();
		//initNCurses();

		endwin();
		initNC();

		//refresh();
		//clear();

		doResize();
		//refresh();
	}

	void resizeHandler(int sig)
	{
		needResize = true;
		//updateSize();
	}

	void initNCurses()
	{
		initNC();

		//clear();
		//refresh();

		logWindow = newwin(1, COLS, 0, 0);
		logWindowBuf = new NcStreambuf(logWindow, std::cout);

		errorWindow = newwin(1, COLS, 2, 0);
		errorWindowBuf = new NcStreambuf(errorWindow, std::cerr);
		errorWindowBuf2 = new NcStreambuf(errorWindow, std::clog);

		statsWindow = newwin(1, COLS, 4, 0);
		statsOutputBuf = new NcStreambuf(statsWindow, statsOutput);

		keysWindow = newwin(1, COLS, 6, 0);

		doResize();

		std::signal(SIGWINCH, resizeHandler);
	}

	void deleteWindows()
	{
		if(logWindow)
		{
			delete logWindowBuf;
			delwin(logWindow);

			delete errorWindowBuf;
			delete errorWindowBuf2;
			delwin(errorWindow);

			delete statsOutputBuf;
			delwin(statsWindow);

			delwin(keysWindow);

			endwin();
			//refresh();
			//clear();

			logWindow = nullptr;
		}
	}

#endif

void initStreams()
{
	static_cast<std::ostream &>(statsOutput).rdbuf(std::cout.rdbuf());
}

#ifdef PRT_NETWORKING
void startNetworkClientMode(PRTClientLib::Client &client)
{
	PRTClientLib::NetworkClient networkClient{client};

	networkClient.mRender.loadFromConfig("devices");

	pl::trim(config.connAddress);
	pl::trim(config.connPort);

	networkClient.setAddress(config.connAddress);
	networkClient.setPort(config.connPort);
	networkClient.priority(pl::EnumThreadPriority::Low);
	networkClient.setBinary(config.ioBinary);
	if(config.ioCompressor)
	{
		if(*config.ioCompressor == "uncompressed")
			networkClient.setCompressor("");
		else
			networkClient.setCompressor(*config.ioCompressor);
	}
	//networkClient.setTaskScheduler(&taskScheduler);

	sigQuit.connect([]{
		pl::log("Application") << "Quitting.";

		needToEnd = true;
	});

	#ifdef PRT_NCURSES
	{
		wattr_on(keysWindow, A_STANDOUT, nullptr);
		waddstr(keysWindow, "(Q)");
		wattr_off(keysWindow, A_STANDOUT, nullptr);

		waddstr(keysWindow, " Quit");

		wrefresh(keysWindow);
	}
	#endif

	std::locale statsLoc = pl::io::try_get_locale("");

	pl::stop_watch<> statsTimer{10s};

	networkClient.start();

	statsTimer.start();

	//std::cin.ignore(std::numeric_limits<std::streamsize>::max());

	while(!needToEnd.load(std::memory_order::acquire))
	{
		#ifdef PRT_NCURSES
			bool expected = true;
			if(needResize.compare_exchange_strong(expected, false))
				updateSize();
		#endif

		if(statsTimer.check() && networkClient.running())
		{
			prt::Statistics stats;
			networkClient.getStats(stats, statsLoc);

			#ifdef PRT_NCURSES
				werase(statsWindow);
			#endif

			PRTClientLib::printStats(statsOutput, stats);
		}

		#ifdef PRT_NCURSES
			handleEvents();
		#endif

		std::this_thread::sleep_for(1s);
	}

	networkClient.end();
	networkClient.join();
}
#endif

void startClientMode(PRTClientLib::Client &client)
{
	PRTClientLib::Render render{client};

	if(config.outFile.empty() && config.hdrOutFile.empty())
		pl::log("Application", pl::logtag::error) << "No output file specified.";

	render.loadFromConfig("devices");

	client.openScene(config.sceneFile);

	render.loadRenderParams("main_final_render_context");

	if(render.mRenderContext->mFilms.empty())
		throw ApplicationException("No film defined for the scene.");

	{
		const auto fileConfig = PRTClientLib::loadConfigFile(config.clientConfigFile, *client.mParser);

		fileConfig.getValueOrDefault
			("auto_save_render_state", config.autoSaveRenderState, 0)
			("auto_focus", config.autoFocus, true)
		;
	}

	render.initForRendering();
	render.mRenderContext->zeroBuffers();
	render.mRenderContext->zeroFilms();

	const auto loadedParams = render.loadState();

	if(loadedParams)
	{
		for(const auto &p : *loadedParams)
		{
			if(!p.first.empty() && p.second.childParams)
			{
				client.mScene[p.first].setParameters(*p.second.childParams);
			}
		}
	}

	pl::tasks.push([&]{
		client.initForRendering();
		client.mScene.updateCameras();
	}).future.get();

	if(config.autoFocus && !loadedParams)
	{
		for(auto &camera : client.mScene.getCameraCtrls())
			if(auto res = client.mRayTracer->traceRay(camera->getPosition(), camera->getCameraViewLobe().normal); res)
				camera->setFocusPos(res->surface.worldPos);

		client.mScene.updateCameras();
	}

	std::locale statsLoc = pl::io::try_get_locale("");

	pl::stop_watch<>
		renderTime,
		statsTimer{10s},
		saveRenderStateTimer{std::chrono::seconds(config.autoSaveRenderState)},
		saveRenderTimer{std::chrono::seconds(config.autoSaveRender)};

	auto timerTpl = std::make_tuple(
		std::ref(renderTime),
		std::ref(statsTimer),
		std::ref(saveRenderStateTimer),
		std::ref(saveRenderTimer));

	const auto compileFilm = [&]
		{
			for(auto &contribution : render.mRenderContext->mFilms | rgv::values)
				contribution.compile();
		};

	const auto saveRender = [&]
		{
			if(config.outFile.empty())
				return;

			pl::log("Application") << "Compiling films.";
			for(auto &contribution : render.mRenderContext->mFilms | rgv::values)
				contribution.compile();

			for(auto&& [outFile, index, contribution] : rgv::zip(config.outFile, rgv::iota(0, rg::unreachable), render.mRenderContext->mFilms | rgv::values))
			{
				pl::log("Application") << "Saving render to " << outFile << '.';

				auto facc = contribution.mFilm.access(sa::mode::shared);
				const auto &film = **facc;

				prt::VirtualBitmap<
					prt::ImplBitmap::BitmapBuffer<
						prt::ColorInt24,
						pl::allocator_with_stats<prt::ColorInt24, prt::memtag::Film, prt::PmrAllocatorGlobal<prt::ColorInt24, prt::memtag::Texture>>
					>
				> bitmapFinal;

				bitmapFinal.allocate(film.getSize());

				PRTClientLib::DrawDestinationDithered<decltype(bitmapFinal) &, const prt::IColorProfile &> drawDest{bitmapFinal, *film.getColorProfile()};

				film.applyToneMap(drawDest);

				const auto &cp = dynamic_cast<const prt::IRenderEntity &>(*film.getColorProfile());

				client.mScene.exportEntity(bitmapFinal, outFile, prt::factories.getMimeTypeExport(outFile), {
					{"colorprofile", {cp.getEntityType(), "", cp.getParameters(true)}}
				});
			}

			for(auto&& [outFile, index, contribution] : rgv::zip(config.hdrOutFile, rgv::iota(0, rg::unreachable), render.mRenderContext->mFilms | rgv::values))
			{
				pl::log("Application") << "Saving HDR render to " << outFile << '.';

				auto facc = contribution.mFilm.access(sa::mode::shared);
				const auto &film = **facc;

				//std::scoped_lock g(prt::objectSharedMutex(film.getBitmap()));

				client.mScene.exportEntity(film.getBitmap(), outFile, prt::factories.getMimeTypeExport(outFile), {});
			}
		};

	const auto saveRenderState = [&]
		{
			pl::log("Application") << "Saving render state.";
			compileFilm();
			render.saveState(loadedParams ? *loadedParams : prt::ParameterTree{});
		};

	sigQuit.connect([]
		{
			pl::log("Application") << "Quitting.";

			needToEnd = true;
		});

	sigSave.connect(saveRender);
	sigSaveState.connect(saveRenderState);
	sigPause.connect([&]
		{
			if(!render.mRenderDeviceController->running())
			{
				render.mRenderDeviceController->start();
				timerTpl | pl::for_each([](auto&& t){ t.unpause(); });
			}
			else
			{
				render.mRenderDeviceController->pause();
				timerTpl | pl::for_each([](auto&& t){ t.pause(); });
			}
		});

	#ifdef PRT_NCURSES

		{
			wattr_on(keysWindow, A_STANDOUT, nullptr);
			waddstr(keysWindow, "(Q)");
			wattr_off(keysWindow, A_STANDOUT, nullptr);

			waddstr(keysWindow, " Quit");
		}

		waddstr(keysWindow, " ");

		{
			wattr_on(keysWindow, A_STANDOUT, nullptr);
			waddstr(keysWindow, "(S)");
			wattr_off(keysWindow, A_STANDOUT, nullptr);

			waddstr(keysWindow, " Save");
		}

		waddstr(keysWindow, " ");

		{
			wattr_on(keysWindow, A_STANDOUT, nullptr);
			waddstr(keysWindow, "(T)");
			wattr_off(keysWindow, A_STANDOUT, nullptr);

			waddstr(keysWindow, " Save render state");
		}

		waddstr(keysWindow, " ");

		{
			wattr_on(keysWindow, A_STANDOUT, nullptr);
			waddstr(keysWindow, "(P)");
			wattr_off(keysWindow, A_STANDOUT, nullptr);

			waddstr(keysWindow, " Start/pause");
		}

		wrefresh(keysWindow);

	#endif

	const auto maxDuration = std::chrono::duration_cast<pl::stop_watch<>::duration_type>(std::chrono::seconds{config.renderTimeLimit});

	render.mRenderDeviceController->start();

	timerTpl | pl::for_each([](auto&& t){ t.start(); });

	//std::cin.ignore(std::numeric_limits<std::streamsize>::max());

	while(!needToEnd.load(std::memory_order::relaxed))
	{
		//bool expected = true;
		//if(needResize.compare_exchange_strong(expected, false))

		#ifdef PRT_NCURSES
			updateSize();
		#endif

		if(config.renderTimeLimit > 0 && renderTime.total_running_time() >= maxDuration)
		{
			pl::log("Application") << "Render duration limit reached.";
			break;
		}

		if(statsTimer.check() && render.mRenderDeviceController->running())
		{
			prt::Statistics stats;
			render.getStats(stats, statsLoc);
			stats["render-time"] = fmt::format(statsLoc, "{:L}:{:%M:%S}",
				std::chrono::duration_cast<std::chrono::hours>(renderTime.total_running_time()).count(), std::chrono::duration_cast<std::chrono::seconds>(renderTime.total_running_time()));

			#ifdef PRT_NCURSES
				werase(statsWindow);
			#endif

			PRTClientLib::printStats(statsOutput, stats);
		}

		if(config.autoSaveRenderState && saveRenderStateTimer.check())
			saveRenderState();

		if(config.autoSaveRender && saveRenderTimer.check())
			saveRender();

		#ifdef PRT_NCURSES
			handleEvents();
		#endif

		std::this_thread::sleep_for(1s);
	}

	render.mRenderDeviceController->end();
	render.mRenderDeviceController->join();

	saveRender();
}

[[nodiscard]] int appMain(int argc, char **argv) noexcept
try
{
	std::pmr::set_default_resource(&pl::default_resource());

	// User locale
	PRTClientLib::try_([&]
	{
		std::cin.imbue(std::locale{""});
		std::cout.imbue(std::locale::classic());
		std::cerr.imbue(std::locale::classic());
		std::clog.imbue(std::locale::classic());

		std::cout << std::defaultfloat << std::dec;
		std::cerr << std::defaultfloat << std::dec;
		std::clog << std::defaultfloat << std::dec;
	});

	//prt::setErrorStream(std::clog);

	{
		po::options_description desc("General options");
		desc.add_options()
			("help", "Display help")
			("config,c", makePoValue(config.configFile)->default_value(paths::user_config_dir() / "photort/config.xml"), "Render configuration file to use.")
			("clientconfig", makePoValue(config.clientConfigFile)->default_value(paths::user_config_dir() / "photort/client_config.xml"), "Client configuration file to use.")
			("modules,m", makePoValue(config.modulePath)->default_value(PRTClientLib::getAppPluginDir()), "Path to PhotoRT modules.")
			("dump,d", "Dump some information.")
			#ifdef PRT_CLI_NCURSES
				("noncurses", "Don't use ncurses if the client is compiled with ncurses support.")
			#endif
		;

		po::options_description clientDesc("Client mode options");
		clientDesc.add_options()
			//("server_port,s", makePoValue(config.serverPort)->default_value(BOOST_STRINGIZE(PRT_DEFAULT_PORT)), "Listen to incoming connection from this port.")
			//("max_nbclients", makePoValue(config.serverMaxClients)->default_value(10), "Maximum number of clients to service.")
			("scene", makePoValue(config.sceneFile), "Path to a scene.")
			("outfile", makePoValue(config.outFile), "LDR output filename.")
			("hdroutfile,h", makePoValue(config.hdrOutFile), "HDR output filename.")
			("auto_save", makePoValue(config.autoSaveRender)->default_value(0), "Auto-save render in [outfile] and [hdroutfile] every X seconds.")
			("time,t", makePoValue(config.renderTimeLimit)->default_value(0), "Render for X seconds. 0 for no limit.")
		;

		po::options_description networkClientDesc("Network client mode options");
		networkClientDesc.add_options()
			("connect_address,a", makePoValue(config.connAddress)->default_value("localhost"), "Connect to server at this address.")
			("connect_port,p", makePoValue(config.connPort)->default_value(BOOST_STRINGIZE(PRT_DEFAULT_PORT)), "Connect to server at this port.")
			("binary", makePoValue(config.ioBinary), "Use binary serialization instead of text, will override server config.")
			("compressor", makePoValue(config.ioCompressor)->implicit_value(boost::optional<std::string>(prt::compressorString)), "Use compressor, will override server config. (=uncompressed for no compression or one of supported compressors)")
		;

		po::options_description allDesc("All options");
		allDesc.add(desc).add(clientDesc).add(networkClientDesc);

		po::positional_options_description p;
		p.add("scene", 1).add("outfile", -1);

		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).options(allDesc).positional(p).run(), vm);
		po::notify(vm);

		if(vm.count("help"))
		{
			printUsage();
			std::cout << desc << '\n' << clientDesc << '\n' << networkClientDesc;
			return EXIT_SUCCESS;
		}

		config.dumpInfo = vm.count("dump") > 0;

		#ifdef PRT_CLI_NCURSES
			config.noNCurses = vm.count("noncurses") > 0;
		#endif
	}

	#ifdef PRT_CLI_NCURSES
	if(!config.dumpInfo && !config.noNCurses)
		initNCurses();
	else
	#endif
		initStreams();

	PRTClientLib::Client client;

	client.loadAllModules(config.modulePath);

	if(config.dumpInfo)
	{
		printDump();
		return EXIT_SUCCESS;
	}

	client.setParser(std::make_shared<prt::ParameterParserXerces>());
	client.loadConfig(config.configFile);

	{
		pl::task::scoped_scheduler schedGuard{pl::tasks};

		pl::log("Application", "Number of cores detected: {:L}.", std::thread::hardware_concurrency());
		pl::log("Application", "Maximum number of threads: {:L}.", pl::tasks.max_threads());

		#ifdef PL_LIB_NUMA
			if(numa_available() > -1)
				pl::log("Application", "Number of numa nodes: {:L}.", pl::numa::node_count());
			else
				pl::log("Application") << "Numa information not available.";
		#endif

		pl::log("Application", "Client config file: {}.", config.clientConfigFile);
		pl::log("Application", "Render config file: {}.", config.configFile);

		std::signal(SIGTERM, signalHandler);
		std::signal(SIGINT, signalHandler);

		#ifdef PRT_NETWORKING
		if(config.sceneFile.empty())
		{
			pl::log("Application") << "Starting in network client mode.";
			startNetworkClientMode(client);
		}
		else
		#endif
		{
			pl::log("Application") << "Starting in client mode.";
			startClientMode(client);
		}
	}

	#ifdef PRT_CLI_NCURSES
		deleteWindows();
	#endif

	return EXIT_SUCCESS;
}
catch(...)
{
	//deleteWindows();
	PRTClientLib::logException(std::current_exception(), "cli");

	//nodelay(stdscr, false);
	//std::cout << "Press a key to exit.\n";
	//getch();
	return EXIT_FAILURE;
}

} // namespace PhotoRT_CLI

int main(int argc, char **argv)
{
	return PhotoRT_CLI::appMain(argc, argv);
}
