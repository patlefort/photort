/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT.hpp>
#include <PhotoRT/Bitmap/Bitmap.hpp>
#include <PhotoRT/PrimitiveGroup/Triangle.hpp>

#include <PhotortClientLib/Client.hpp>

#include <patlib/path.hpp>

#define wxMEMBITMAP(name) wxBitmap(name##_xpm)
#define wxMEMICON(name) wxIcon(name##_xpm)

#include <wx/config.h>
#include <wx/string.h>

namespace PhotoRTGUI
{
	using namespace std::chrono_literals;

	PRT_IMPORT_NS

	using prt::r_;

	namespace paths = pl::paths;

	namespace rg = ranges;
	namespace rgv = rg::views;

	namespace sa = shared_access;

	namespace hn = boost::hana;

	class ApplicationException : public pl::Exception
	{
	public:
		using pl::Exception::Exception;

		[[nodiscard]] virtual const char *module() const noexcept override
		{
			return "Application";
		}
	};

	inline wxString stringViewToWxString(std::string_view v)
		{ return wxString{v.data(), v.size()}; }

}
