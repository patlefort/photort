/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <GL/glew.h>

#include "InteractiveRenderPanel.hpp"

#include <PhotoRT/Core/Integration.hpp>

#include <wx/dcclient.h>
#include <wx/graphics.h>
#include <wx/sizer.h>
#include <wx/toolbar.h>
#include <wx/timer.h>
#include <wx/msgdlg.h>

#include <fmt/format.h>
#include <fmt/chrono.h>
#include <fmt/xchar.h>

namespace PhotoRTGUI
{

void PixelPlotterWxImage::onPaint(wxPaintEvent &evt)
{
	evt.Skip();

	//wxAutoBufferedPaintDC dc(this);
	wxPaintDC dc(this);
	PrepareDC(dc);

	auto bmpacc = bmp.access(sa::mode::shared);

	if(bmpacc->IsOk())
	{
		//prt::ObjectSharedLock guard{*this, std::try_to_lock};

		//if(guard)
		//{
			std::unique_ptr<wxGraphicsContext> gc{wxGraphicsContext::Create(dc)};
			//wxBitmap bitmap(image);
			pl::rectangle<> drawSize {0, 0, GetSize().GetWidth(), GetSize().GetHeight()};

			//auto view = GetViewStart();

			const auto region = GetUpdateRegion();

			const auto updateFc = [&](const auto &rect)
				{
					//std::cout << std::dec << rect << '\n';

					auto updRect = rect.clip(drawSize);
					auto s = updRect.dims();

					gc->Clip(updRect.p1[0], updRect.p1[1], s[0], s[1]);

					//gc->DrawBitmap(*bmpacc, updRect.p1[0], updRect.p1[1], s[0], s[1]);
					gc->DrawBitmap(*bmpacc, 0, 0, drawSize.p2[0], drawSize.p2[1]);
				};

			if(region.IsEmpty())
			{
				updateFc(drawSize);
			}
			else
			{
				for(wxRegionIterator upd(region); upd; ++upd)
					updateFc( pl::rectangle<>{upd.GetX(), upd.GetY(), upd.GetX() + upd.GetW(), upd.GetY() + upd.GetH()} );
			}

			//gc->DrawBitmap(*bmpacc, 0.0, 0.0, drawSize.p2[0], drawSize.p2[1]);

			if(rectSelected)
			{
				auto pen = gc->CreatePen(wxPen{*wxRED, 1, wxPENSTYLE_LONG_DASH});
				gc->SetPen(pen);

				auto rect = targetRect.reorder();

				rect = {
					(rect.p1 * drawSize.p2) / mSize,
					(rect.p2 * drawSize.p2) / mSize
				};

				auto size = rect.dims();
				gc->DrawRectangle(rect.p1[0], rect.p1[1], size[0], size[1]);
			}
		//}
	}
}

GLImageDisplay::GLImageDisplay(wxWindow *parent) :
	wxGLCanvas(parent, wxID_ANY, nullptr, wxDefaultPosition, wxDefaultSize, 0, L"GLCanvas", wxNullPalette)
{
	Bind(wxEVT_PAINT, &GLImageDisplay::onPaint, this);

	glContext = std::make_unique<wxGLContext>(this);
	SetCurrent(*glContext);

	if(const auto err = glewInit(); err != GLEW_OK)
	{
		//mGlewError = true;
		pl::log("Application", pl::logtag::error) <<
			"Error initializing glew: [" << err << "] " << reinterpret_cast<const char *>(glewGetErrorString(err));
	}
}

GLImageDisplay::~GLImageDisplay()
{
	if(textureId)
	{
		glDeleteTextures(1, &textureId);
		glDeleteBuffers(1, &pboId);
	}
}

void GLImageDisplay::setSize(prt::Dir2Di size)
{
	mSize = size;

	drawTargets[0] | sa::mode::excl |
		[&](auto&& b)
		{
			b.mBitmap.setSize(size);
			b.zeroBitmap();
		};
	drawTargets[1].access(sa::mode::excl)->mBitmap.setSize(size);

	displayTarget = drawTarget = drawTargets[0];
	drawInterface = drawDestInterfaces[0];
}

void GLImageDisplay::onPaint(wxPaintEvent &evt)
{
	evt.Skip();

	wxPaintDC dc(this);
	PrepareDC(dc);

	if(!mGlewError && displayTarget)
	{
		SetCurrent(*glContext);

		auto dtacc = displayTarget.access(sa::mode::shared);

		glEnable(GL_TEXTURE_2D);

		const auto viewSize = GetSize();
		const auto dataSize = dtacc->mBitmap.bmp.getNbPixels() * 4;

		if(!textureId)
		{
			glGenTextures(1, &textureId);
			glBindTexture(GL_TEXTURE_2D, textureId);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, mSize[0], mSize[1], 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

			glGenBuffers(1, &pboId);
			glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pboId);
			glBufferData(GL_PIXEL_UNPACK_BUFFER_ARB, dataSize, 0, GL_STREAM_DRAW_ARB);

			glDepthFunc(GL_LESS);
			glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glShadeModel(GL_FLAT);
		}

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, viewSize.GetWidth(), viewSize.GetHeight(), 0, 0, 100);

		glMatrixMode(GL_MODELVIEW);

		glViewport(0, 0, viewSize.GetWidth(), viewSize.GetHeight());

		glBindTexture(GL_TEXTURE_2D, textureId);
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, pboId);

		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, mSize[0], mSize[1], GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

		if(auto ptr = (GLubyte *)glMapBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, GL_WRITE_ONLY_ARB); ptr)
		{
			std::memcpy(ptr, dtacc->mBitmap.bmp.getRawData(), dataSize);
			glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER_ARB);
		}

		glBegin(GL_QUADS);
			glTexCoord2i(0, 0); glVertex2i(-1, -1);
			glTexCoord2i(0, 1); glVertex2i(-1, viewSize.GetHeight() + 1);
			glTexCoord2i(1, 1); glVertex2i(viewSize.GetWidth() + 1, viewSize.GetHeight() + 1);
			glTexCoord2i(1, 0); glVertex2i(viewSize.GetWidth() + 1, -1);
		glEnd();

		glBindTexture(GL_TEXTURE_2D, 0);
		glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, 0);
	}

	glFlush();

	SwapBuffers();
}

PostProcessWindow::PostProcessWindow(wxWindow *parent, wxWindowID id, const wxPoint &pos, const wxSize &size, long style, const wxString &name) :
	wxWindow(parent, id, pos, size, style, name)
{
	wxSizer *sizer = new wxBoxSizer(wxVERTICAL);
	wxToolBar *toolBar = new wxToolBar(this, wxID_ANY);
	wxToolBar *toolBar2 = new wxToolBar(this, wxID_ANY);

	SetSizer(sizer);

	toolBar->SetMargins(wxSize(3, 3));

	filmSelect = new wxChoice(toolBar, wxID_ANY);
	toolBar->AddControl(filmSelect);

	layerSelect = new wxChoice(toolBar, wxID_ANY);
	toolBar->AddControl(layerSelect);

	toolBar->AddSeparator();

	postProcessBtn = new wxButton(toolBar, wxID_ANY, L"Update image");
	toolBar->AddControl(postProcessBtn);
	//saveLightButton = new wxButton(toolBar, wxID_ANY, L"Save light image");
	//toolBar->AddControl(saveLightButton);
	//saveUnfilteredHDRbutton = new wxButton(toolBar, wxID_ANY, L"Save unfiltered HDR");
	//toolBar->AddControl(saveUnfilteredHDRbutton);
	saveHDRbutton = new wxButton(toolBar, wxID_ANY, L"Save HDR");
	toolBar->AddControl(saveHDRbutton);
	saveLDRbutton = new wxButton(toolBar, wxID_ANY, L"Save LDR");
	toolBar->AddControl(saveLDRbutton);

	toolBar->Realize();
	sizer->Add(toolBar, wxSizerFlags(0).Expand());

	layerParamSelect = new wxChoice(toolBar2, wxID_ANY);
	toolBar2->AddControl(layerParamSelect);

	layerPowerCtrl = new wxSlider(toolBar2, wxID_ANY, 5000, 0, 15000);
	{
		auto size = layerPowerCtrl->GetSize();
		size.SetWidth(400);
		layerPowerCtrl->SetSize(size);
	}
	toolBar2->AddControl(layerPowerCtrl);

	layerPowerResetBtn = new wxButton(toolBar2, wxID_ANY, L"Reset");
	toolBar2->AddControl(layerPowerResetBtn);

	toolBar2->Realize();
	sizer->Add(toolBar2, wxSizerFlags(0).Expand());

	scrolledImage = new wxScrolledCanvas(this);
	scrolledImage->ShowScrollbars(wxSHOW_SB_ALWAYS, wxSHOW_SB_ALWAYS);
	scrolledImage->SetScrollRate(1, 1);
	image = new PixelPlotterWxImage(scrolledImage);
	sizer->Add(scrolledImage, wxSizerFlags(1).Border(wxALL, 3).Expand());

	scrolledImage->Bind(wxEVT_MOUSEWHEEL, [this] (wxMouseEvent &evt)
	{
		evt.Skip();

		const auto rotation = r_(evt.GetWheelRotation()) / r_(evt.GetWheelDelta());
		const auto view = scrolledImage->GetViewStart();

		if(evt.GetWheelAxis() == wxMOUSE_WHEEL_VERTICAL)
			scrolledImage->Scroll(view.x, view.y - rotation * 60);
		else
			scrolledImage->Scroll(view.x - rotation * 60, view.y);

	}, scrolledImage->GetId());

	/*imageDisplay->Bind(wxEVT_PAINT, [this](wxPaintEvent &event)
	{
		wxPaintDC dc(this->imageDisplay);
		PrepareDC(dc);

		if(this->image.image.IsOk())
		{
			this->imageDisplay->SetSize(this->image.image.GetSize());
			this->imageDisplay->SetMinSize(this->image.image.GetSize());
			this->scrolledImage->SetVirtualSize(this->image.image.GetSize());
			this->scrolledImage->SetScrollRate(5, 5);

			wxBitmap bitmap(image.image);
			dc.DrawBitmap(bitmap, wxPoint(0, 0), false);
		}
	}, imageDisplay->GetId());*/
}

std::pair<std::string, i32> PostProcessWindow::getSelectedLayer()
{
	const auto layer = layerSelect->GetSelection() - 1;
	const auto film = filmSelect->GetSelection();

	return {
		film >= 0 ? filmSelect->GetString(film).ToStdString() : "",
		layer
	};
}

void PostProcessWindow::setPowerSelect(prt::CReal power)
{
	layerPowerCtrl->SetValue( pl::plsqrt(power) * 5000 );
}

prt::CReal PostProcessWindow::getPowerSelect() const noexcept
{
	return pl::pow2((prt::CReal)layerPowerCtrl->GetValue() / 5000);
}

InteractiveRayTracerWindow::InteractiveRayTracerWindow(wxWindow *parent, wxWindowID id, const wxPoint &pos, const wxSize &size, long style, const wxString &name) :
	InteractiveRenderWindow(parent, id, pos, size, style, name)
{
	plotter = new PixelPlotterWxImage(this);
	previewPlotter = new GLImageDisplay(this);

	curPlotter = plotter;
	curPlotterWindow = plotter;

	previewPlotter->Hide();

	curRefreshTimer = &refreshTimer;

	redrawTimer = new wxTimer(this);

	Bind(wxEVT_TIMER, [this](wxTimerEvent &)
	{
		if(!previewMode)
			return;

		compileFilm();
		refreshImage();

		if(!previewPlotter->drawAndDisplayTargetIsSame())
			previewPlotter->swapDisplayTarget();

		if(hasMovement)
		{
			hasMovement = false;

			{
				// Prevent render devices to work while we clear buffers and modify the camera
				prt::RenderDeviceExecutionLockGuard exeg{*renderDevice};

				*cameraTransform = cameraTransformCopy;

				if(getAutoFocus())
					focusCamera();

				camera->update();

				for(auto &cam : camera->getCameras())
					updateSensorSubstanceStack(*cam, *rayTracer);

				currentDevice->notifyEntityUpdate(dynamic_cast<prt::IRenderEntity &>(*camera));

				if(previewPlotter->drawAndDisplayTargetIsSame())
				{
					previewPlotter->swapDrawTarget();
					previewPlotter->getDrawDestination().access(sa::mode::excl)->zeroBitmap();

					renderDevice->restart();

					startNewFrame();
				}
			}
		}
	}, redrawTimer->GetId());

	//statsWindow = new wxPanel(this);
	//statsWindow->Hide();

	//auto sizer = new wxBoxSizer(wxHORIZONTAL);
	//statsWindow->SetSizer(sizer);

	//sizer->Add(listView, wxSizerFlags(1).Expand().Border(wxALL, 3));

	//Bind(wxEVT_PAINT, &InteractiveRayTracerWindow::onPaint, this);

	keyStates[PRT_INTRTWND_KEY_FORWARD] = false;
	keyStates[PRT_INTRTWND_KEY_BACKWARD] = false;
	keyStates[PRT_INTRTWND_KEY_LEFT] = false;
	keyStates[PRT_INTRTWND_KEY_RIGHT] = false;
	keyStates[PRT_INTRTWND_KEY_UP] = false;
	keyStates[PRT_INTRTWND_KEY_DOWN] = false;
	//keyStates[PRT_INTRTWND_KEY_FOV_UP] = false;
	//keyStates[PRT_INTRTWND_KEY_FOV_DOWN] = false;
	keyStates[PRT_INTRTWND_KEY_ROLL_LEFT] = false;
	keyStates[PRT_INTRTWND_KEY_ROLL_RIGHT] = false;

	//Bind(wxEVT_KEY_DOWN, &InteractiveRayTracerWindow::processKeyStates, this);
	//Bind(wxEVT_KEY_UP, &InteractiveRayTracerWindow::processKeyStates, this);

	previewPlotter->Bind(wxEVT_RIGHT_DOWN, [this] (wxMouseEvent &evt)
	{
		evt.Skip();

		if(previewMode && running())
		{
			previewPlotter->SetFocus();
			previewPlotter->SetCursor(wxCursor(wxCURSOR_RIGHT_BUTTON));
			//CaptureMouse();
			mousePos = lastMousePos = wxGetMousePosition();
			lastCamAngle = cameraTransformCopy.getTransform<prt::TransformRotation>("rotation")->getAngle();
			mouseMove = true;
		}
	}, previewPlotter->GetId());

	previewPlotter->Bind(wxEVT_RIGHT_UP, [this] (wxMouseEvent &evt)
	{
		evt.Skip();

		//if(HasCapture())
			//ReleaseMouse();
		previewPlotter->SetCursor(wxCursor(wxCURSOR_DEFAULT));
		mouseMove = false;
	}, previewPlotter->GetId());

	plotter->Bind(wxEVT_LEFT_DOWN, [this] (wxMouseEvent &evt)
	{
		evt.Skip();

		if(evt.ShiftDown())
		{
			rectSelect = true;

			plotter->targetRect.p1 = plotter->targetRect.p2 = mouseToFilmPos(evt);

		}

	}, plotter->GetId());

	plotter->Bind(wxEVT_MOTION, [this] (wxMouseEvent &evt)
	{
		evt.Skip();

		if(rectSelect)
		{
			plotter->rectSelected = true;
			plotter->targetRect.p2 = mouseToFilmPos(evt);

			refreshImage();
		}

	}, plotter->GetId());

	plotter->Bind(wxEVT_LEFT_UP, [this] (wxMouseEvent &evt)
	{
		evt.Skip();

		if(rectSelect)
		{
			plotter->targetRect.p2 = mouseToFilmPos(evt);

			selectArea(plotter->targetRect);

			rectSelect = false;

			refreshImage();

		}

	}, plotter->GetId());



	previewPlotter->Bind(wxEVT_LEFT_UP, [this] (wxMouseEvent &evt)
	{
		evt.Skip();

		prt::Point2Dr filmPos;

		{
			const auto pos = evt.GetPosition();
			const auto size = previewPlotter->GetSize();
			auto facc = currentDevice->mRenderContext->mFilms.begin()->second.mFilm.access(sa::mode::shared);
			const auto filmBounds = (**facc).getFilmBounds();
			const auto filmSize = (**facc).getSize();

			filmPos = {r_(pos.x) / size.x * filmSize[0] + filmBounds.p1[0], r_(pos.y) / size.y * filmSize[1] + filmBounds.p1[1]};
		}

		if(evt.ShiftDown())
		{
			getInfoAtPixel(filmPos);
		}
		else
		{
			if(running())
			{
				focusCamera(filmPos);
				restart();
			}
		}

	}, previewPlotter->GetId());

	Bind(wxEVT_MOUSEWHEEL, [this] (wxMouseEvent &evt)
	{
		evt.Skip();

		const auto rotation = r_(evt.GetWheelRotation()) / r_(evt.GetWheelDelta());

		if(evt.ControlDown())
		{
			setZoom(getZoom() * (rotation > 1 ? pl::plpow(Real{1.0/0.97}, (float)rotation) : pl::plpow(Real{0.97}, (float)-rotation)));
			refreshSize();
			refreshImage();
		}
		else if(previewMode && running())
		{
			if(evt.AltDown())
			{
				if(cameraPerspective)
				{
					auto params = cameraPerspective->getParameters();
					auto fov = params.getValueOrDefault<Real>("fov", 40);
					params["fov"] = fov * (1 + (-rotation * 0.02));
					cameraPerspective->setParameters(params);
					hasMovement = true;
				}
			}
			else
			{
				auto tr = cameraTransformCopy.getTransform<prt::TransformTranslation>("transform_point");
				tr->setPositionZ( pl::plmin<Real>(0, tr->getPosition()[2] + rotation * 5) );
				hasMovement = true;
			}
		}
		else
		{
			auto view = GetViewStart();
			/*int sx, sy;
			GetScrollPixelsPerUnit(&sx, &sy);

			SetScrollRate(1, 1);*/

			if(evt.GetWheelAxis() == wxMOUSE_WHEEL_VERTICAL)
				Scroll(view.x, view.y - rotation * 60);
			else
				Scroll(view.x - rotation * 60, view.y);

			//SetScrollRate(sx, sy);
		}
	}, GetId());

	//previewPlotter->Bind(wxEVT_MOTION, &InteractiveRayTracerWindow::processMouseMove, this);

	//Bind(EVT_STATS_UPDATE, &InteractiveRayTracerWindow::onUpdateStats, this);

	//lookAt.setParentBonePoint(&cameraPos);
	//cameraUp.setParentBonePoint(&cameraPos);
}

void InteractiveRayTracerWindow::selectArea(const pl::rectangle<> &r)
{
	auto facc = currentDevice->mRenderContext->mFilms.begin()->second.mFilm.access(sa::mode::shared);
	const auto &film = **facc;
	const auto filmBounds = film.getFilmBounds();
	const auto filmSize = film.getSize();
	const auto size = film.getSize();

	plotter->targetRect = r.clip({pl::size_p{size}});

	targetRect = {
		(r.p1 * filmSize) / size + filmBounds.p1,
		(r.p2 * filmSize) / size + filmBounds.p1
	};
	targetRect = targetRect.reorder().clip(filmBounds);

	if(targetRect.is_zero_area())
	{
		unselectArea();
	}
	else
	{
		currentDevice->setRenderRect(film.getEntityId(), targetRect);
		plotter->rectSelected = true;
	}
}

void InteractiveRayTracerWindow::unselectArea()
{
	plotter->rectSelected = false;
	currentDevice->setRenderRect("", pl::tag::zero);
}

void InteractiveRayTracerWindow::refreshImage()
{
	curPlotterWindow->Refresh(false);
	//curPlotterWindow->Update();
}

void InteractiveRayTracerWindow::refreshSize()
{
	auto facc = mainRenderDevice->mRenderContext->mFilms.begin()->second.mFilm.access(sa::mode::shared);
	auto filmSize = (**facc).getSize();

	wxSize size = {
		int(filmSize[0] * getZoom()),
		int(filmSize[1] * getZoom())
	};

	const prt::Dir2Di dispSize{size.GetWidth(), size.GetHeight()};
	plotter->setDisplaySize(dispSize);
	previewPlotter->setDisplaySize(dispSize);

	//SetSize(size);
	//SetMinSize(size);
	SetVirtualSize(size);
	//SetScrollbar(1, 1, size.GetWidth(), size.GetHeight());
	//SetScrollRate(5, 5);
}

void InteractiveRayTracerWindow::setRenderDevice(prt::RenderDeviceController &d, prt::RenderDeviceController &pd)
{
	mainRenderDevice = &d;
	previewRenderDevice = &pd;

	auto facc = mainRenderDevice->mRenderContext->mFilms.begin()->second.mFilm.access(sa::mode::shared);
	auto &film = **facc;
	auto pfacc = previewRenderDevice->mRenderContext->mFilms.begin()->second.mFilm.access(sa::mode::shared);
	auto &previewFilm = **pfacc;
	auto filmSize = film.getSize();
	auto previewFilmSize = previewFilm.getSize();

	plotter->setSize(filmSize);
	previewPlotter->setSize(previewFilmSize);

	plotter->colorProfile = film.getColorProfile();
	previewPlotter->colorProfile = previewFilm.getColorProfile();

	currentDevice = mainRenderDevice;
	mOtherThread = renderDevice = currentDevice;

	CallAfter([this]{
		refreshSize();
		refreshImage();
	});
}

void InteractiveRayTracerWindow::setCamera(prt::ICameraController &camera)
{
	InteractiveRenderWindow::setCamera(camera);

	initialFocus = false;
}

void InteractiveRayTracerWindow::createRenderStatsWindow(wxWindow *parent)
{
	wxSizer *sizer = new wxBoxSizer(wxVERTICAL);

	parent->SetSizer(sizer);

	statsList = new wxListView(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT | wxLC_HRULES | wxLC_VRULES);
	statsList->SetMinSize(wxSize(100, 100));

	statsList->SetFont(wxFont{10, wxFONTFAMILY_MODERN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL});

	sizer->Add(statsList, wxSizerFlags(1).Expand());
}

void InteractiveRayTracerWindow::processMouseMove(wxMouseEvent &evt)
{
	if(mouseMove && previewMode)
	{
		mousePos = evt.GetLogicalPosition(wxWindowDC(previewPlotter));

		// Why do I have to do this or the image won't refresh properly on long mouse movement?
		refreshImage();
	}
}

void InteractiveRayTracerWindow::processKeyStates(wxKeyEvent &evt)
{
	if(auto key = keyStates.find(evt.GetKeyCode()); key != keyStates.end())
		keyStates[evt.GetKeyCode()] = evt.GetEventType() == wxEVT_KEY_DOWN ? true : false;
}

void InteractiveRayTracerWindow::pollKeyStates(Real modifier)
{
	if(mouseMove)
		mousePos = wxGetMousePosition();

	for(auto &key : keyStates)
		keyStates[key.first] = wxGetKeyState(wxKeyCode(key.first));

	checkInputs(modifier);
}

void InteractiveRayTracerWindow::clearKeyStates()
{
	for(auto &key : keyStates)
		keyStates[key.first] = false;
}

void InteractiveRayTracerWindow::onPaint(wxPaintEvent &)
{
	wxPaintDC dc(this);
	PrepareDC(dc);

	//if(curPlotter->image.IsOk())
	//{
		//glReadPixels(0, 0, curPlotter->getWidth(), curPlotter->getHeight(), GL_RGB, GL_UNSIGNED_BYTE, curPlotter->image.GetData());

		//glCanvas->SwapBuffers();
	//}

	/*if(curPlotter->image.IsOk())
	{
		wxGraphicsContext *gc = wxGraphicsContext::Create(dc);
		wxBitmap bitmap(curPlotter->image);

		gc->DrawBitmap(bitmap, 0.0, 0.0, plotter.getWidth(), plotter.getHeight());

		delete gc;
	}*/
}

void InteractiveRayTracerWindow::on_start()
{
	mSwitchPreviewModeToken.enabled(true);

	updateTarget();
	startNewFrame();
	compileFilm();

	setClearOnStart(true);

	CallAfter([&]
	{
		refreshImage();

		if(previewMode)
			redrawTimer->Start( std::chrono::duration_cast<std::chrono::milliseconds>(previewRefreshTimer.duration()).count() );
	});

	//if(previewMode)
		//CallAfter([this]{ redrawTimer->Start( std::chrono::duration_cast<std::chrono::milliseconds>(previewRefreshTimer.duration()).count() ); });

	evtEnd.disconnect();
	evtEnd = currentDevice->listen(pl::ThreadEvent::End, [this] {
		end();
	});

	InteractiveRenderWindow::on_start();
}

void InteractiveRayTracerWindow::pause()
{
	InteractiveRenderWindow::pause();

	CallAfter([&] { refreshImage(); });
	//updateStats(timer.Time());

	//totalRenderTime += timer.Time() - timeStart;
}

void InteractiveRayTracerWindow::on_end()
{
	/*if(!mEndAndDestroy)
	{
		CallAfter([&] { refreshImage(); });
		//updateStats(timer.Time());
	}*/

	mSwitchPreviewModeToken.enabled(false);

	CallAfter([this]{ redrawTimer->Stop(); });

	InteractiveRenderWindow::on_end();
}

void InteractiveRayTracerWindow::on_pause()
{
	InteractiveRenderWindow::on_pause();

	statsTimer.pause();
	curRefreshTimer->pause();

	if(previewMode)
		CallAfter([this]{ redrawTimer->Stop(); });
}

void InteractiveRayTracerWindow::on_restart()
{
	prt::RenderDeviceExecutionLockGuard exeg{*renderDevice};

	InteractiveRenderWindow::on_restart();
	startNewFrame();

	compileFilm();

	//if(previewMode)
		//CallAfter([this]{ redrawTimer->Start( std::chrono::duration_cast<std::chrono::milliseconds>(previewRefreshTimer.duration()).count() ); });

	if(!previewMode)
		CallAfter([&] { refreshImage(); });
}

void InteractiveRayTracerWindow::on_pause_end()
{
	InteractiveRenderWindow::on_pause_end();

	statsTimer.unpause();
	curRefreshTimer->unpause();

	if(previewMode)
		CallAfter([this]{ redrawTimer->Start( std::chrono::duration_cast<std::chrono::milliseconds>(previewRefreshTimer.duration()).count() ); });
}

void InteractiveRayTracerWindow::switchPreviewMode()
{
	CallAfter([&]{ redrawTimer->Stop(); });

	clearKeyStates();
	updateTarget();
	unselectArea();
	startNewFrame();
	compileFilm();

	CallAfter([&]
	{
		refreshImage();

		if(previewMode && running())
			redrawTimer->Start( std::chrono::duration_cast<std::chrono::milliseconds>(previewRefreshTimer.duration()).count() );
	});

	/*if(!previewMode)
		CallAfter([this]{ redrawTimer->Stop(); });
	else if(running())
		CallAfter([this]{ redrawTimer->Start( std::chrono::duration_cast<std::chrono::milliseconds>(previewRefreshTimer.duration()).count() ); });*/
}

void InteractiveRayTracerWindow::restart()
{
	InteractiveRenderWindow::restart();
}

void InteractiveRayTracerWindow::checkInputs(Real modifier)
{
	if(mouseMove)
	{
		Real movx = mousePos.x - lastMousePos.x, movy = mousePos.y - lastMousePos.y;
		auto camMove = prt::VecD(movy * (getInvertMouse() ? -0.003 : 0.003), movx * -0.003, 0, 0);
		auto newAngle = lastCamAngle + camMove;

		auto angleTr = cameraTransformCopy.getTransform<prt::TransformRotation>("rotation");

		if(pl::mask_any(angleTr->getAngle() != static_cast<prt::Point3Dr>(newAngle)))
		{
			angleTr->setAngle(newAngle);
			hasMovement = true;
		}
	}

	if(keyStates[PRT_INTRTWND_KEY_FORWARD] || keyStates[PRT_INTRTWND_KEY_BACKWARD])
	{
		const auto &cameraLobe = camera->getCameraViewLobe();

		auto dir = cameraLobe.normal;

		if(keyStates[PRT_INTRTWND_KEY_BACKWARD])
			dir = -dir;

		dir *= 0.05 * modifier;

		//camera->setPosition(camera->getPosition() + dir);
		//camera->getPosition() += dir;
		auto posTr = cameraTransformCopy.getTransform<prt::TransformTranslation>("position");
		posTr->move(dir);

		hasMovement = true;
	}

	if(keyStates[PRT_INTRTWND_KEY_LEFT] || keyStates[PRT_INTRTWND_KEY_RIGHT])
	{
		const auto &cameraLobe = camera->getCameraViewLobe();

		auto dir = cameraLobe.lobeX;

		if(keyStates[PRT_INTRTWND_KEY_LEFT])
			dir = -dir;

		dir *= 0.05 * modifier;

		//camera->getPosition() += dir;
		auto posTr = cameraTransformCopy.getTransform<prt::TransformTranslation>("position");
		posTr->move(dir);

		hasMovement = true;
	}

	if(keyStates[PRT_INTRTWND_KEY_UP] || keyStates[PRT_INTRTWND_KEY_DOWN])
	{
		//const auto &cameraLobe = camera->getCameraViewLobe();

		prt::VecD dir{0, 1, 0};

		if(keyStates[PRT_INTRTWND_KEY_DOWN])
			dir = -dir;

		dir *= 0.05 * modifier;

		//camera->getPosition() += dir;
		auto posTr = cameraTransformCopy.getTransform<prt::TransformTranslation>("position");
		posTr->move(dir);

		hasMovement = true;
	}

	if(keyStates[PRT_INTRTWND_KEY_ROLL_LEFT] || keyStates[PRT_INTRTWND_KEY_ROLL_RIGHT])
	{
		cameraTransformCopy.getTransform<prt::TransformRotation>("rotation")->rotatez(0.0005 * modifier * (keyStates[PRT_INTRTWND_KEY_ROLL_LEFT] ? 1.0 : -1.0));

		hasMovement = true;
	}

	/*if(keyStates[PRT_INTRTWND_KEY_FOV_UP])
	{
		if(cameraPerspective)
		{
			auto params = cameraPerspective->getParameters();
			auto fov = params.getValueOrDefault<Real>("fov", 40);
			params["fov"] = fov * (1.0 + (0.0003 * modifier));
			cameraPerspective->setParameters(params);
			hasMovement = true;
		}
	}

	if(keyStates[PRT_INTRTWND_KEY_FOV_DOWN])
	{
		if(cameraPerspective)
		{
			auto params = cameraPerspective->getParameters();
			auto fov = params.getValueOrDefault<Real>("fov", 40);
			params["fov"] = fov * (1.0 - (0.0003 * modifier));
			cameraPerspective->setParameters(params);
			hasMovement = true;
		}
	}*/
}

void InteractiveRayTracerWindow::focusCamera()
{
	if(auto res = rayTracer->traceRay(camera->getPosition(), camera->getCameraViewLobe().normal); res)
		camera->setFocusPos(res->surface.worldPos);
}

void InteractiveRayTracerWindow::getInfoAtPixel(prt::Point2Dr p)
{
	const auto ray = camera->getFocusDir(p);

	if(auto res = rayTracer->traceRay(ray); res)
	{
		auto inter = prt::collisionToShadedInteraction(*res, ray.dir, true);

		std::locale loc = pl::io::try_get_locale("");

		wxString message = fmt::format(loc,
			"Instance ID:                  {instid}\n"
			"Object ID:                    {objid}\n"
			"Primitive index:              {primindex}\n"
			"Object name:                  {name}\n"
			"Object properties group ID:   {groupid}\n"
			"Distance:                     {dist}\n"
			"Position:                     {pos}\n"
			"Backface hit:                 {backface}\n"
			"Surface normal:               {surfnormal}\n"
			"Shading normal:               {shadenormal}\n",
			fmt::arg("instid",             pl::io::quoted{inter.surface().inst->getInstanceID()}),
			fmt::arg("objid",              pl::io::quoted{inter.surface().inst->getObject()->getObjectID()}),
			fmt::arg("primindex",          inter.surface().primIndex),
			fmt::arg("name",               pl::io::quoted{dynamic_cast<const prt::IRenderEntity &>(*inter.surface().inst->getObject()).getEntityName()}),
			fmt::arg("groupid",            pl::io::quoted{inter.surface().inst->getObject()->getClassName().first}),
			fmt::arg("dist",               inter.colDist),
			fmt::arg("pos",                inter.surface().worldPos),
			fmt::arg("backface",           inter.surface().isBackfaceHit()),
			fmt::arg("surfnormal",         inter.surface().surfaceNormal),
			fmt::arg("shadenormal",        inter.surface().shadingNormal)
		);

		if(inter.surface().matEntry)
		{
			message += fmt::format(loc,
				"Material ID:                {id}\n"
				"Material name:              {name}\n",
				fmt::arg("id",               pl::io::quoted{inter.surface().matEntry->material->getEntityId()}),
				fmt::arg("name",             pl::io::quoted{inter.surface().matEntry->material->getEntityName()})
			);
		}

		//CallAfter([this, message]{ wxMessageBox(message, "Collision information"); });
		wxMessageBox(message, "Collision information");
	}

}

void InteractiveRayTracerWindow::focusCamera(prt::Point2Dr p)
{
	if(auto res = rayTracer->traceRay(camera->getFocusDir(p)); res)
		camera->setFocusPos(res->surface.worldPos);
}

void InteractiveRayTracerWindow::updateStats()
{
	if(!statsList)
		return;

	prt::Statistics stats;

	auto loc = pl::io::try_get_locale("");
	currentDevice->getStats(stats, loc);

	using pl::alloc_stat;
	stats["render-time"] = fmt::format(loc, "{:L}:{:%M:%S}",
		std::chrono::duration_cast<std::chrono::hours>(statsTimer.total_running_time()).count(), std::chrono::duration_cast<std::chrono::seconds>(statsTimer.total_running_time()));

	stats["memory: texture"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::Texture{}).totalAllocated)});
	stats["memory: contribution-buffers"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::ContBuffer{}).totalAllocated)});
	stats["memory: film"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::Film{}).totalAllocated)});
	stats["memory: primitive"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::Primitive{}).totalAllocated)});
	stats["memory: accel-struct"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::AccelStruct{}).totalAllocated)});
	stats["memory: entity"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::Entity{}).totalAllocated)});
	stats["memory: other"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::Other{}).totalAllocated)});

	if(statToColumnIndex.empty() || stats.size() != statToColumnIndex.size())
	{
		statsList->ClearAll();
		statToColumnIndex.clear();

		statsList->InsertColumn(0, L"Stat", wxLIST_FORMAT_LEFT, 300);
		statsList->InsertColumn(1, L"Value", wxLIST_FORMAT_RIGHT, 130);

		for(auto&& [i, stat] : rgv::enumerate(stats))
		{
			wxListItem *listItem = new wxListItem();
			listItem->SetText(stat.first);
			listItem->SetColumn(0);
			listItem->SetId(i);
			statsList->InsertItem(*listItem);

			listItem = new wxListItem();
			listItem->SetColumn(1);
			listItem->SetId(i);
			statsList->SetItem(*listItem);

			statToColumnIndex.insert({stat.first, i});
		}
	}

	for(const auto &stat : stats)
		statsList->SetItem(statToColumnIndex[stat.first], 1, wxString(stat.second));

}

void InteractiveRayTracerWindow::startNewFrame()
{
	if(getClearOnStart())
		clearCurrentDevice();

	statsTimer.start();
	curRefreshTimer->start();
}

void InteractiveRayTracerWindow::updateTarget()
{
	if(previewMode)
	{
		curPlotter = previewPlotter;
		curPlotterWindow = previewPlotter;
		renderDevice = currentDevice = previewRenderDevice;
		curRefreshTimer = &previewRefreshTimer;
	}
	else
	{
		curPlotter = plotter;
		curPlotterWindow = plotter;
		renderDevice = currentDevice = mainRenderDevice;
		curRefreshTimer = &refreshTimer;
	}

	mOtherThread = renderDevice;

	//currentDevice->setRenderRect("final_render", {50, 50, 350, 300});
	currentDevice->mRenderContext->initCamera();

	if(getClearOnStart() && !initialFocus && getAutoFocus())
	{
		focusCamera();
		initialFocus = true;
	}

	camera->update();
	currentDevice->notifyEntityUpdate(*dynamic_cast<prt::IRenderEntity *>(camera));

	statToColumnIndex.clear();

	CallAfter([this]{
		if(previewMode)
		{
			previewPlotter->getDrawDestination().access(sa::mode::excl)->zeroBitmap();
			plotter->Hide();
			previewPlotter->Show();
		}
		else
		{
			plotter->getDrawDestination().access(sa::mode::excl)->zeroBitmap();
			previewPlotter->Hide();
			plotter->Show();
		}
	});
}

bool InteractiveRayTracerWindow::compileFilm()
{
	auto &film = currentDevice->mRenderContext->mFilms.begin()->second;

	//if(previewMode)
	//{
		{
			std::shared_lock g{*mRenderPropsMutex};
			film.compile();
		}

		film.mFilm                          | sa::mode::shared |
		curPlotter->getDrawDestination()    | sa::mode::excl |
			[&](const auto &film, auto &dest)
			{
				film->applyToneMap(dest);
			};

		//(**film.mFilm.access(sa::mode::shared)).applyToneMap( *curPlotter->getDrawDestination().access(sa::mode::excl) );

		curPlotter->updateDisplay();

		return true;
	/*}
	else
	{
		prt::ObjectLock guard{*film, std::try_to_lock};
		if(guard)
		{
			film->compile();

			prt::ObjectLock guard{*curPlotter};
			film->applyToneMap(*curPlotter);

			curPlotter->updateDisplay();

			return true;
		}
	}*/

	return false;
}

bool InteractiveRayTracerWindow::do_iteration()
{
	if(mSwitchPreviewModeToken.handle(SwitchPreviewToken::SwitchPreview))
	{
		renderDevice->end();
		renderDevice->join();

		switchPreviewMode();

		renderDevice->start();
	}

	if(!previewMode && curRefreshTimer->check())
	{
		if(compileFilm())
			CallAfter([&] { refreshImage(); });
	}

	if(statsTimer.check())
	{
		CallAfter([this]{ updateStats(); });
	}

	sleep_for(10ms);

	return true;
}

}
