/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Base.hpp"

#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
#include <wx/xrc/xh_bmp.h>
#include <wx/panel.h>
#include <wx/dcclient.h>
#include <wx/bitmap.h>
#include <wx/image.h>

namespace PhotoRTGUI
{

class BitmapDisplayPanel : public wxPanel
{
public:
	BitmapDisplayPanel(wxWindow *parent,
            wxWindowID winid = wxID_ANY,
            const wxPoint& pos = wxDefaultPosition) :
						wxPanel(parent, winid, pos, wxSize(-1, -1))
	{}

	void setImage(wxImage &image)
	{
		SetVirtualSize(image.GetSize());
		this->image = &image;

		//image.SetData((unsigned char *)ldrBitmap.pixels, ldrBitmap.width, ldrBitmap.height, true);

		//bitmap.Create();
		//image.SaveFile("/media/Data2/Projects/PhotoRT/v2/bin/Release/test.png", wxBITMAP_TYPE_PNG);
	}

protected:
	//wxBitmap bitmap;
	//TextureMapperBitmap *ldrBitmap = nullptr;
	wxImage *image = nullptr;

	void OnPaint(wxPaintEvent &)
	{
		wxPaintDC dc(this);
		PrepareDC(dc);

		if(image)
		{
			wxBitmap bitmap(*image);

			dc.DrawBitmap(bitmap, wxPoint(0, 0), false);
		}
	}

};

}