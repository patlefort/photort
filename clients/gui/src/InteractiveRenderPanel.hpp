/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Base.hpp"

#include <patlib/chrono.hpp>
#include <patlib/rectangle.hpp>

#include <wx/image.h>
#include <wx/window.h>
#include <wx/glcanvas.h>
#include <wx/bitmap.h>
#include <wx/panel.h>
#include <wx/scrolwin.h>
#include <wx/button.h>
#include <wx/slider.h>
#include <wx/choice.h>
#include <wx/listctrl.h>
#include <wx/dcclient.h>
#include <wx/graphics.h>

namespace PhotoRTGUI
{

using DrawDestInterfaceType = sa::shared<PRTClientLib::IDrawDestination &, sa::default_mutex_type &>;

class IDrawTarget
{
public:
	virtual ~IDrawTarget() = default;

	[[nodiscard]] virtual sa::shared_view<DrawDestInterfaceType> getDrawDestination() noexcept = 0;
	virtual void updateDisplay() = 0;
	virtual void setSize(prt::Dir2Di size) = 0;
	[[nodiscard]] virtual prt::Dir2Di getSize() const noexcept = 0;
	virtual void setDisplaySize(prt::Dir2Di size) = 0;
};

class WxImageDrawDestination final : public PRTClientLib::IDrawDestination
{
public:

	wxImage image;

	virtual void setPixel(prt::Color color, if32 x, if32 y) noexcept final
	{
		prt::convertFromColor(color, image.GetData() + (y * image.GetWidth() + x) * 3);
	}

	virtual void setPixel(prt::Color color, szt offset) noexcept final
	{
		prt::convertFromColor(color, image.GetData() + offset * 3);
	}

	void setSize(prt::Dir2Di size)
	{
		image.Create(size[0], size[1], true);
	}

	[[nodiscard]] if32 getWidth() const noexcept
		{ return image.GetWidth(); }

	[[nodiscard]] prt::Dir2Di getSize() const noexcept
		{ return {image.GetWidth(), image.GetHeight()}; }

	virtual void zeroBitmap() noexcept final
		{ image.Clear(); }

};

class PixelPlotterWxImage final : public wxWindow, public IDrawTarget
{
public:
	sa::shared<wxBitmap> bmp;

	prt::EntityPtr<const prt::IColorProfile> colorProfile;
	pl::rectangle<> targetRect{};
	bool rectSelected = false;

	sa::shared<PRTClientLib::DrawDestinationDithered<WxImageDrawDestination, const prt::EntityPtr<const prt::IColorProfile> &>> drawDest{
		sa::in_place_list,
		WxImageDrawDestination{},
		colorProfile
	};
	DrawDestInterfaceType drawDestInterface{
		sa::reference<PRTClientLib::IDrawDestination>,
		drawDest
	};

	PixelPlotterWxImage(wxWindow *parent) : wxWindow(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0)
	{
		Bind(wxEVT_PAINT, &PixelPlotterWxImage::onPaint, this);

		SetBackgroundStyle(wxBG_STYLE_PAINT);
	}

	[[nodiscard]] virtual sa::shared_view<DrawDestInterfaceType> getDrawDestination() noexcept final { return drawDestInterface; }

	virtual void updateDisplay() final
	{
		bmp      | sa::mode::excl |
		drawDest | sa::mode::excl |
			[&](auto &b1, auto &b2)
			{
				//pl::recreate(b1, b2.mBitmap.image);
				b1 = b2.mBitmap.image;
			};
	}

	virtual void setSize(prt::Dir2Di size) final
	{
		mSize = size;

		bmp      | sa::mode::excl |
		drawDest | sa::mode::excl |
			[&](auto &b1, auto &b2)
			{
				b1.Create(size[0], size[1]);
				b2.mBitmap.setSize(size);
			};
	}

	[[nodiscard]] virtual prt::Dir2Di getSize() const noexcept final
		{ return mSize; }

	virtual void setDisplaySize(prt::Dir2Di size) final
	{
		SetSize(wxSize(size[0], size[1]));
	}

private:

	prt::Dir2Di mSize{};

	void onPaint(wxPaintEvent &event);

};

template <typename BitmapType_>
class BitmapDrawDestination final : public PRTClientLib::IDrawDestination
{
public:

	BitmapType_ bmp;

	virtual void setPixel(prt::Color color, if32 x, if32 y) noexcept final
	{
		bmp.setPixel(prt::convertFromColor<typename BitmapType_::PixelType>(color), x, y);
	}

	virtual void setPixel(prt::Color color, szt offset) noexcept final
	{
		bmp.setPixel(prt::convertFromColor<typename BitmapType_::PixelType>(color), offset);
	}

	void setSize(prt::Dir2Di size)
	{
		bmp.allocate(size);
	}

	[[nodiscard]] if32 getWidth() const noexcept
		{ return bmp.getSize()[0]; }

	[[nodiscard]] prt::Dir2Di getSize() const noexcept
		{ return bmp.getSize(); }

	virtual void zeroBitmap() noexcept final
		{ bmp.zeroBitmap(); }

};

class GLImageDisplay final : public wxGLCanvas, public IDrawTarget
{
public:

	using BitmapType = prt::BitmapBuffer<prt::ImplBitmap::BitmapBuffer<u32, pl::allocator_with_stats<u32, prt::memtag::Film>>>;
	using DrawDestType = PRTClientLib::DrawDestinationDithered<
		BitmapDrawDestination<BitmapType>,
		const prt::EntityPtr<const prt::IColorProfile> &
	>;

	prt::EntityPtr<const prt::IColorProfile> colorProfile;

	std::array<sa::shared<DrawDestType>, 2> drawTargets{
		sa::shared<DrawDestType>{sa::in_place_list, BitmapDrawDestination<BitmapType>{}, colorProfile},
		sa::shared<DrawDestType>{sa::in_place_list, BitmapDrawDestination<BitmapType>{}, colorProfile}
	};
	std::array<DrawDestInterfaceType, 2> drawDestInterfaces{
		DrawDestInterfaceType{sa::reference<PRTClientLib::IDrawDestination>, drawTargets[0]},
		DrawDestInterfaceType{sa::reference<PRTClientLib::IDrawDestination>, drawTargets[1]}
	};
	sa::shared_view<sa::shared<DrawDestType>> drawTarget, displayTarget;
	sa::shared_view<DrawDestInterfaceType> drawInterface;

	GLImageDisplay(wxWindow *parent);
	virtual ~GLImageDisplay();

	[[nodiscard]] virtual sa::shared_view<DrawDestInterfaceType> getDrawDestination() noexcept final { return drawInterface; }

	//virtual void setImage(wxImage *image);

	virtual void setSize(prt::Dir2Di size) final;
	[[nodiscard]] virtual prt::Dir2Di getSize() const noexcept final { return mSize; }

	virtual void setDisplaySize(prt::Dir2Di size) final
	{
		SetSize(wxSize(size[0], size[1]));
	}

	virtual void updateDisplay() final
	{
	}

	void swapDrawTarget() noexcept
	{
		drawTarget = drawTarget == drawTargets[0] ? drawTargets[1] : drawTargets[0];
		drawInterface = drawInterface == drawDestInterfaces[0] ? drawDestInterfaces[1] : drawDestInterfaces[0];
	}

	void swapDisplayTarget() noexcept
	{
		displayTarget = displayTarget == drawTargets[0] ? drawTargets[1] : drawTargets[0];
	}

	[[nodiscard]] bool drawAndDisplayTargetIsSame() noexcept
	{
		return displayTarget == drawTarget;
	}

private:

	GLuint textureId = 0, pboId = 0;
	std::unique_ptr<wxGLContext> glContext;
	prt::Dir2Di mSize{};
	bool mGlewError = false;

	void onPaint(wxPaintEvent &event);

};

class PostProcessWindow : public wxWindow
{
public:
	PostProcessWindow(wxWindow *parent, wxWindowID id=wxID_ANY, const wxPoint &pos=wxDefaultPosition, const wxSize &size=wxDefaultSize, long style=0, const wxString &name=wxPanelNameStr);

	virtual void updateVirtualSize()
	{
		const auto size = image->getSize();
		image->setDisplaySize(size);
		scrolledImage->SetVirtualSize(size[0], size[1]);
	}

	std::pair<std::string, i32> getSelectedLayer();
	void setPowerSelect(prt::CReal power);
	prt::CReal getPowerSelect() const noexcept;

	wxButton *postProcessBtn, *saveHDRbutton, *saveLDRbutton, *layerPowerResetBtn;
	PixelPlotterWxImage *image;
	wxPanel *imageDisplay;
	wxScrolledCanvas *scrolledImage;
	wxChoice *filmSelect, *layerSelect, *layerParamSelect;
	wxSlider *layerPowerCtrl;

};

class RenderStatsWindow : public wxPanel
{
public:
	RenderStatsWindow(wxWindow *parent, wxWindowID id=wxID_ANY, const wxPoint &pos=wxDefaultPosition, const wxSize &size=wxDefaultSize, long style=0, const wxString &name=wxPanelNameStr) :
	    wxPanel(parent, id, pos, size, style, name) {}

};

class InteractiveRenderWindow : public wxScrolledCanvas, public pl::ThreadDelegateWithin<prt::IRenderDevice *>
{
public:
	InteractiveRenderWindow(wxWindow *parent, wxWindowID id=wxID_ANY, const wxPoint &pos=wxDefaultPosition, const wxSize &size=wxDefaultSize, long style=0, const wxString &name=wxPanelNameStr) :
	    wxScrolledCanvas(parent, id, pos, size, style, name) {}

	virtual void setScene(prt::Scene &scene) { this->scene = &scene; }
	prt::Scene *getScene() const { return scene; }

	virtual void setCamera(prt::ICameraController &cam)
	{
		camera = &cam;

		cameraTransform = dynamic_cast<prt::TransformMulti *>(std::to_address(camera->getTransform()));
		cameraTransformCopy = *cameraTransform;
	}
	const prt::ICameraController *getCamera() const { return this->camera; }
	prt::ICameraController *getCamera() { return this->camera; }

	virtual void setCameraPerspective(prt::IRenderEntity &cameraPerspective) { this->cameraPerspective = &cameraPerspective; }
	const prt::IRenderEntity *getCameraPerspective() const { return this->cameraPerspective; }
	prt::IRenderEntity *getCameraPerspective() { return this->cameraPerspective; }

	virtual bool ready_to_start() override { return renderDevice->isReady(); }

	virtual void createRenderStatsWindow(wxWindow */*parent*/) {}

	virtual void pollKeyStates(Real /*modifier*/) {}

protected:
	prt::TransformMulti cameraTransformCopy;
	prt::TransformMulti *cameraTransform;
	prt::ICameraController *camera;
	prt::IRenderEntity *cameraPerspective;
	prt::IRenderDevice *renderDevice;
	prt::Scene *scene = nullptr;

};

#define PRT_INTRTWND_KEY_FORWARD 'W'
#define PRT_INTRTWND_KEY_BACKWARD 'S'
#define PRT_INTRTWND_KEY_LEFT 'A'
#define PRT_INTRTWND_KEY_RIGHT 'D'
#define PRT_INTRTWND_KEY_UP 'R'
#define PRT_INTRTWND_KEY_DOWN 'F'
//#define PRT_INTRTWND_KEY_FOV_UP wxKeyCode::WXK_END
//#define PRT_INTRTWND_KEY_FOV_DOWN wxKeyCode::WXK_HOME
#define PRT_INTRTWND_KEY_ROLL_LEFT wxKeyCode::WXK_NUMPAD7
#define PRT_INTRTWND_KEY_ROLL_RIGHT wxKeyCode::WXK_NUMPAD9

inline wxWindow *getTopLevelParentWindow(wxWindow *w)
{
	auto p = w->GetParent();
	return p ? getTopLevelParentWindow(p) : w;
}

class InteractiveRayTracerWindow : public InteractiveRenderWindow
{
public:
	InteractiveRayTracerWindow(wxWindow *parent, wxWindowID id=wxID_ANY, const wxPoint &pos=wxDefaultPosition, const wxSize &size=wxDefaultSize, long style=0, const wxString &name=wxPanelNameStr);

	virtual void setCamera(prt::ICameraController &camera) override;

	virtual void setRefreshRate(uf32 rate) { refreshTimer.duration(std::chrono::milliseconds(rate)); }
	[[nodiscard]] auto getRefreshRate() const { return std::chrono::duration_cast<std::chrono::milliseconds>(refreshTimer.duration()).count(); }

	virtual void setPreviewRefreshRate(uf32 rate) { previewRefreshTimer.duration(std::chrono::milliseconds(rate)); }
	[[nodiscard]] auto getPreviewRefreshRate() const { return std::chrono::duration_cast<std::chrono::milliseconds>(previewRefreshTimer.duration()).count(); }

	virtual void setStatsRefreshRate(uf32 rate) { statsTimer.duration(std::chrono::milliseconds(rate)); }
	[[nodiscard]] auto getStatsRefreshRate() const { return std::chrono::duration_cast<std::chrono::milliseconds>(statsTimer.duration()).count(); }

	virtual void setRayTracer(prt::IRayTracer *rt) { rayTracer = rt; }
	[[nodiscard]] auto getRayTracer() const { return rayTracer; }

	virtual void setRenderPropsMutex(std::shared_mutex *m) { mRenderPropsMutex = m; }
	[[nodiscard]] auto getRenderPropsMutex() const { return mRenderPropsMutex; }

	virtual void setRenderDevice(prt::RenderDeviceController &d, prt::RenderDeviceController &pd);

	[[nodiscard]] auto getImagePlotter() { return plotter; }
	[[nodiscard]] auto getImagePlotterPreview() { return previewPlotter; }

	virtual void restart() override;
	virtual void pause() override;

	virtual void setPreviewMode(bool previewMode)
	{
		this->previewMode = previewMode;

		mSwitchPreviewModeToken.request(SwitchPreviewToken::SwitchPreview);
	}

	[[nodiscard]] bool getPreviewMode() const { return previewMode; }

	virtual void setClearOnStart(bool v) { clearOnStart = v; }
	[[nodiscard]] bool getClearOnStart() const { return clearOnStart; }

	virtual void refreshImage();

	virtual void setAutoFocus(bool v) { autoFocus = v; }
	[[nodiscard]] bool getAutoFocus() const { return autoFocus; }

	virtual void setInvertMouse(bool v) { invertMouse = v; }
	[[nodiscard]] bool getInvertMouse() const { return invertMouse; }

	virtual void createRenderStatsWindow(wxWindow *parent) override;

	[[nodiscard]] auto getZoom() const { return zoom; }
	virtual void setZoom(Real z)
	{
		zoom = pl::plclamp<Real>(z, 0.1, 100);
	}

	virtual void pollKeyStates(Real modifier) override;
	virtual void clearKeyStates();

	virtual bool compileFilm();

	virtual void selectArea(const pl::rectangle<> &r);
	virtual void unselectArea();

	virtual void clearCurrentDevice()
	{
		currentDevice->mRenderContext->zeroBuffers();
		currentDevice->mRenderContext->zeroFilms();
	}

	virtual void zeroDisplay()
	{
		curPlotter->getDrawDestination().access(sa::mode::excl)->zeroBitmap();
		curPlotter->updateDisplay();
	}

protected:
	bool clearOnStart = true, rectSelect = false;
	std::atomic<bool> previewMode{false};

	prt::IRayTracer *rayTracer = nullptr;

	IDrawTarget *curPlotter = nullptr;
	wxWindow *curPlotterWindow = nullptr;
	PixelPlotterWxImage *plotter;
	GLImageDisplay *previewPlotter;

	wxTimer *redrawTimer = nullptr;

	std::shared_mutex *mRenderPropsMutex = nullptr;

	boost::signals2::connection evtEnd;

	enum class SwitchPreviewToken
	{
		SwitchPreview = 1
	};

	pl::request_token<SwitchPreviewToken> mSwitchPreviewModeToken;

	pl::stop_watch<>
		statsTimer {3100ms},
		refreshTimer {1000ms},
		previewRefreshTimer {100ms},
		*curRefreshTimer = nullptr;

	bool mouseMove = false, hasMovement = false, initialFocus = false, invertMouse = true;
	wxPoint lastMousePos, mousePos;
	prt::VecP lastCamAngle;
	std::map<int, bool> keyStates;

	bool autoFocus = true;
	wxListView *statsList = nullptr;

	pl::rectangle<> targetRect{};
	std::string targetFilmID;

	prt::RenderDeviceController *mainRenderDevice, *previewRenderDevice, *currentDevice;
	GLImageDisplay *glCanvas = nullptr;

	Real zoom = 1;

	std::map<std::string, long> statToColumnIndex;

	virtual void switchPreviewMode();
	virtual void startNewFrame();

	virtual void updateTarget();

	virtual void on_end() override;
	virtual void on_pause() override;
	virtual void on_pause_end() override;
	virtual void on_start() override;
	virtual void on_restart() override;

	virtual void onPaint(wxPaintEvent &event);

	virtual bool do_iteration() override;
	virtual void checkInputs(Real modifier);

	virtual void processKeyStates(wxKeyEvent &evt);
	virtual void processMouseMove(wxMouseEvent &evt);

	virtual void focusCamera();
	virtual void focusCamera(prt::Point2Dr p);

	virtual void getInfoAtPixel(prt::Point2Dr p);

	virtual void updateStats();

	virtual void refreshSize();

	[[nodiscard]] prt::Point2Di mouseToFilmPos(const wxMouseEvent &evt) const
	{
		auto pos = evt.GetPosition();

		return {if32(pos.x / getZoom()), if32(pos.y / getZoom())};
	}
};

}