/*
	PhotoRT

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string_view>

namespace PhotoRTGUI
{

std::string_view default_prt_client_config =
R"(<?xml version="1.0" encoding="UTF-8"?>
<config xmlns="http://photort.org/params/">
	<!-- Target number of pixels for preview bitmap -->
	<preview_nb_pixels value="26000" />

	<!-- Refresh rate in milliseconds for preview mode -->
	<preview_refresh_rate value="50" />

	<!-- Refresh rate in milliseconds for full render mode -->
	<refresh_rate value="10000" />

	<!-- Automatically save render state every X seconds. 0 to disable. -->
	<auto_save_render_state value="1800" />

	<!-- Automatically focus camera -->
	<auto_focus value="1" />

	<!-- Invert vertical axis for camera -->
	<invert_mouse value="1" />

	<!-- Automatically open the last opened scene -->
	<auto_open value="1" />
</config>
)";

}