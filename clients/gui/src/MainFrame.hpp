/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Base.hpp"

#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
#include <wx/xrc/xh_bmp.h>
#include <wx/frame.h>
#include <wx/sizer.h>
#include <wx/iconbndl.h>
#include <wx/artprov.h>
#include <wx/menu.h>
#include <wx/toolbar.h>
#include <wx/button.h>
#include <wx/aui/aui.h>

namespace PhotoRTGUI
{

class MainFrame : public wxFrame
{
public:
	wxMenuBar* m_menuBar;
	wxMenu* m_name6;
	wxMenuItem* m_menuOpen, *m_menuSave, *m_menuDumpInfo, *m_menuLoadTestScene1, *m_menuSaveRender;
	wxMenuItem* m_menuExit;
	wxMenu* m_name8;
	wxMenuItem* m_menuItem9;

	wxAuiManager auiManager;
	wxAuiToolBar *auiToolBar;
	wxAuiToolBarItem *buttonStart, *buttonEnd, *buttonRestart, *buttonSave, *buttonPreviewMode, *buttonReload, *buttonUnselectArea;

public:
	MainFrame(wxWindow* parent);
	virtual ~MainFrame() override;


protected:

	virtual void showAbout();

};

}