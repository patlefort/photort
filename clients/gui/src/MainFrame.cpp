/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "MainFrame.hpp"

#include <wx/aboutdlg.h>

#include <fmt/xchar.h>

namespace PhotoRTGUI
{

MainFrame::MainFrame(wxWindow* parent)
	: wxFrame(parent, wxID_ANY, L"PhotoRT", wxDefaultPosition, wxSize(1400, 1000), wxMAXIMIZE|wxCAPTION|wxRESIZE_BORDER|wxMAXIMIZE_BOX|wxMINIMIZE_BOX|wxSYSTEM_MENU|wxCLOSE_BOX)
{
	auiManager.SetManagedWindow(this);

	auiToolBar = new wxAuiToolBar(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxAUI_TB_DEFAULT_STYLE | wxAUI_TB_HORIZONTAL);

	//auiToolBar->SetToolBitmapSize(wxSize(16, 16));

	buttonPreviewMode = auiToolBar->AddTool(1, L"Render mode", wxArtProvider::GetBitmap("semi-starred", wxART_MENU), L"Switch to preview/full render mode. (F2)");

	auiToolBar->AddSeparator();

	buttonStart = auiToolBar->AddTool(2, L"Start", wxArtProvider::GetBitmap("media-playback-start", wxART_MENU), L"Start/pause rendering. (F1)");
	//buttonEnd = auiToolBar->AddTool(4, L"End", wxBITMAP_PNG_FROM_DATA(control_stop_blue));
	//buttonEnd->SetActive(false);
	buttonRestart = auiToolBar->AddTool(5, L"Restart", wxArtProvider::GetBitmap("media-seek-backward", wxART_MENU), L"Clear films.");
	buttonRestart->SetActive(false);

	buttonReload = auiToolBar->AddTool(6, L"Reload", wxArtProvider::GetBitmap("view-refresh", wxART_MENU), L"Reload config and scene. (F5)");

	auiToolBar->AddSeparator();

	buttonUnselectArea = auiToolBar->AddTool(7, L"Unselect", wxArtProvider::GetBitmap("edit-clear", wxART_MENU), L"Unselect render area.");

	auiToolBar->Realize();

	auiManager.AddPane(auiToolBar, wxAuiPaneInfo().Name(L"maintoolbar").Caption(L"Main tool bar").ToolbarPane().Top().Row(1).Position(1));

	SetIcons(wxArtProvider::GetIconBundle("photort", wxART_FRAME_ICON));

	m_menuBar = new wxMenuBar(0);
	this->SetMenuBar(m_menuBar);

	m_name6 = new wxMenu();
	m_menuBar->Append(m_name6, L"File");

	m_menuOpen = new wxMenuItem(m_name6, wxID_ANY, L"Open", L"", wxITEM_NORMAL);
	m_name6->Append(m_menuOpen);

	m_menuSave = new wxMenuItem(m_name6, wxID_ANY, L"Save As", L"", wxITEM_NORMAL);
	m_name6->Append(m_menuSave);

	m_menuSaveRender = new wxMenuItem(m_name6, wxID_ANY, L"Save render state", L"", wxITEM_NORMAL);
	m_name6->Append(m_menuSaveRender);

	m_menuDumpInfo = new wxMenuItem(m_name6, wxID_ANY, L"Dump information", L"", wxITEM_NORMAL);
	m_name6->Append(m_menuDumpInfo);

	//m_menuLoadTestScene1 = new wxMenuItem(m_name6, wxID_ANY, _("Load test scene"), L"", wxITEM_NORMAL);
	//m_name6->Append(m_menuLoadTestScene1);

	m_menuExit = new wxMenuItem(m_name6, wxID_EXIT, L"Exit", L"Quit", wxITEM_NORMAL);
	m_name6->Append(m_menuExit);

	m_name8 = new wxMenu();
	m_menuBar->Append(m_name8, L"Help");

	m_menuItem9 = new wxMenuItem(m_name8, wxID_ABOUT, L"About...", L"", wxITEM_NORMAL);
	m_name8->Append(m_menuItem9);

	Bind(wxEVT_COMMAND_MENU_SELECTED, [this](wxCommandEvent &) { Close(); }, m_menuExit->GetId());
	Bind(wxEVT_COMMAND_MENU_SELECTED, [this](wxCommandEvent &) { showAbout(); }, m_menuItem9->GetId());

	auiManager.Update();
}

MainFrame::~MainFrame()
{
	auiManager.UnInit();
}

void MainFrame::showAbout()
{
	wxAboutDialogInfo info;
	info.AddDeveloper(L"Patrick Northon");
	info.SetCopyright(L"(C) 2018 Patrick Northon");
	info.SetLicence(L"GPL v3 or later");
	info.SetWebSite(L"https://gitlab.com/patlefort/photort");

	#if BOOST_COMP_GNUC
		#define PRT_GUI_COMP_NAME BOOST_COMP_GNUC_NAME

		#define PRT_GUI_COMP_VER_MAJOR BOOST_VERSION_NUMBER_MAJOR(BOOST_COMP_GNUC)
		#define PRT_GUI_COMP_VER_MINOR BOOST_VERSION_NUMBER_MINOR(BOOST_COMP_GNUC)
		#define PRT_GUI_COMP_VER_PATCH BOOST_VERSION_NUMBER_PATCH(BOOST_COMP_GNUC)

	#elif BOOST_COMP_CLANG
		#define PRT_GUI_COMP_NAME BOOST_COMP_CLANG_NAME

		#define PRT_GUI_COMP_VER_MAJOR BOOST_VERSION_NUMBER_MAJOR(BOOST_COMP_CLANG)
		#define PRT_GUI_COMP_VER_MINOR BOOST_VERSION_NUMBER_MINOR(BOOST_COMP_CLANG)
		#define PRT_GUI_COMP_VER_PATCH BOOST_VERSION_NUMBER_PATCH(BOOST_COMP_CLANG)

	#elif BOOST_COMP_MSVC
		#define PRT_GUI_COMP_NAME BOOST_COMP_MSVC_NAME

		#define PRT_GUI_COMP_VER_MAJOR BOOST_VERSION_NUMBER_MAJOR(BOOST_COMP_MSVC)
		#define PRT_GUI_COMP_VER_MINOR BOOST_VERSION_NUMBER_MINOR(BOOST_COMP_MSVC)
		#define PRT_GUI_COMP_VER_PATCH BOOST_VERSION_NUMBER_PATCH(BOOST_COMP_MSVC)

	#elif BOOST_COMP_INTEL
		#define PRT_GUI_COMP_NAME BOOST_COMP_INTEL_NAME

		#define PRT_GUI_COMP_VER_MAJOR BOOST_VERSION_NUMBER_MAJOR(BOOST_COMP_INTEL)
		#define PRT_GUI_COMP_VER_MINOR BOOST_VERSION_NUMBER_MINOR(BOOST_COMP_INTEL)
		#define PRT_GUI_COMP_VER_PATCH BOOST_VERSION_NUMBER_PATCH(BOOST_COMP_INTEL)

	#endif

	#ifdef PRT_GUI_COMP_NAME
		info.SetDescription(
			fmt::format("Compiled with {} {}.{}.{}",
				PRT_GUI_COMP_NAME,
				PRT_GUI_COMP_VER_MAJOR, PRT_GUI_COMP_VER_MINOR, PRT_GUI_COMP_VER_PATCH)
		);
	#else
		info.SetDescription(L"Compiled with an unknown compiler.");
	#endif

	wxIcon icon;
	icon.CopyFromBitmap(wxArtProvider::GetBitmap("photort", wxART_FRAME_ICON, {64, 64}));

	info.SetIcon(icon);

	wxAboutBox(info);
}

}
