/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Base.hpp"

#include "MainFrame.hpp"

#include "BitmapDisplay.hpp"
#include "InteractiveRenderPanel.hpp"

#include <patlib/chrono.hpp>
#include <patlib/task.hpp>

#ifdef PL_NUMA
	#include <patlib/numa.hpp>
#endif

#include <wx/settings.h>
#include <wx/app.h>
#include <wx/msgdlg.h>
#include <wx/dirdlg.h>
#include <wx/filedlg.h>
#include <wx/cmdline.h>

#if defined(__WXGTK__) && defined(GDK_WINDOWING_X11)
	#include <X11/Xlib.h>
#endif

#include <fmt/format.h>
#include <fmt/chrono.h>
#include <fmt/ostream.h>

#include <random>
#include <fstream>

namespace PhotoRTGUI
{
extern std::string_view default_prt_client_config;

struct TaskDesc
{
	std::string id;
	bool unique = true;
};

template <typename FC_>
struct Task
{
	TaskDesc desc;
	std::function<FC_> f;
	std::atomic_flag onceFlag = ATOMIC_FLAG_INIT;
};

enum class CallMode
{
	deferred, immediate
};

class TaskScheduler
{
protected:

	template <typename Task_, typename Handle_>
	void replaceCancelFc(Task_ &task, Handle_ &handle)
	{
		if(task.desc.unique)
		{
			handle.cancel_fc = [&task, oldCancelFc = handle.cancel_fc](const auto &future){
				task.onceFlag.clear(std::memory_order::release);

				return oldCancelFc(future);
			};
		}
	}

public:

	template <typename RetType_, typename... FcArgs_, typename... Args_, typename TimeType_ = std::chrono::milliseconds>
	auto push(Task<RetType_(FcArgs_...)> &task, TimeType_ time = 0ms, Args_&&... args)
	{
		if(task.desc.unique && task.onceFlag.test_and_set(std::memory_order::acquire))
		{
			//pl::log("Application") << "Pushing task failed: " << task.desc.id;
			return pl::task::task_handle<RetType_>{};
		}

		//pl::log("Application") << "Pushing task: " << task.desc.id;

		//return task.f(args...);

		auto handle = pl::tasks.push(
			[&task, args...]
			{
				if constexpr(std::is_void_v<RetType_>)
				{
					task.f(args...);

					if(task.desc.unique)
						task.onceFlag.clear(std::memory_order::release);
				}
				else
				{
					auto ret = task.f(args...);

					if(task.desc.unique)
						task.onceFlag.clear(std::memory_order::release);

					return ret;
				}
			},
			time
		);

		replaceCancelFc(task, handle);

		return handle;
	}

	template <typename HandleType_, typename Task_, typename TimeType_, typename... Args_>
	bool pushOrReschedule(pl::task::task_handle<HandleType_> &handle, Task_ &task, TimeType_ newTime, Args_&&... args)
	{
		if(!handle.reschedule(newTime))
		{
			if(auto newHandle = push(task, newTime, PL_FWD(args)...); newHandle.future.valid())
			{
				handle = std::move(newHandle);
				return true;
			}

			return false;
		}

		//pl::log(("Application") << "Recheduled task: " << task.desc.id;

		return true;
	}
};

template <typename FC_, typename ParamsType_, typename MutexType_ = std::shared_mutex>
class QueuedUniqueTask
{
private:
	TaskScheduler &mTs;
	std::function<FC_> mTaskFunction;
	std::function<void(const ParamsType_ &)> mUpdateFunction;
	Task<void()> mTask;
	Task<void()> mParamChangeTask;
	std::atomic<std::chrono::steady_clock::time_point> mTaskTime{std::chrono::steady_clock::now()};

	pl::task::task_handle<void> mTaskHandle;

	sa::shared<ParamsType_, MutexType_> mParams;

public:

	QueuedUniqueTask(TaskScheduler &ts, TaskDesc desc, std::function<void(const ParamsType_ &)> upf, std::function<FC_> f) :
		mTs{ts},
		mTaskFunction{std::move(f)},
		mUpdateFunction{std::move(upf)},

		mTask{desc, [this]{
			{ mParams | sa::mode::shared | mUpdateFunction; }

			mTaskFunction();
		}},

		mParamChangeTask{{"", true}, [this]
		{
			if(!mTs.pushOrReschedule(mTaskHandle, mTask, mTaskTime.load(std::memory_order::acquire)))
			{
				if(mTaskHandle.future.valid())
				{
					mTaskHandle.future.wait();
					mTaskHandle = mTs.push(mTask, mTaskTime.load(std::memory_order::acquire));
				}
			}
		}}
	{
		mTask.desc.unique = true;
	}

	template <typename Duration_ = std::chrono::milliseconds>
	void launch(Duration_ /*delay*/ = {})
	{
		mTaskHandle = mTs.push(mTask);
	}

	template <typename Duration_ = std::chrono::milliseconds>
	void launchQueued(Duration_ delay = {})
	{
		mTaskTime.store(std::chrono::steady_clock::now() + delay, std::memory_order::release);
		mTs.push(mParamChangeTask);
	}

	template <typename T_>
	void updateParams(const T_ &p)
	{
		*mParams.access(sa::mode::excl) = p;
	}

	template <typename T_>
	void accessParams(T_ &&f) const
	{
		mParams | sa::mode::shared | PL_FWD(f);
	}

	template <typename T_>
	void modifyParams(T_ &&f)
	{
		mParams | sa::mode::excl | PL_FWD(f);
	}
};

struct RenderConfig
{
	szt previewNbPixels = 20000;
	uf32 previewRefreshRate = 100, refreshRate = 10000, autoSaveRenderState = 1800;
	bool autoFocus = true, invertMouse = true, autoOpen = true;
};

class MainApp : public wxApp
{
public:
	MainApp() :

		taskInfoDump {{"info_dump", true}, [this](const std::filesystem::path &path)
		{
			pl::log("Application") << "Dumping information.";

			try
			{
				std::shared_lock g{mRenderPropsMutex};

				fullRender.mRenderDeviceController->dumpInfo(client.mScene, path, "");
				previewRender.mRenderDeviceController->dumpInfo(client.mScene, path, "preview_");

				//char prev = std::cout.fill(' ');

				auto rayTracers = client.mRender.getEntitiesByType("raytracer-");
				for(auto &rt : rayTracers)
				{
					std::ofstream ofs(path / (rt->getEntityId() + ".txt"));
					ofs.imbue(userLoc);
					dynamic_cast<prt::IRayTracerEngine &>(*rt).printInfo(ofs);
				}

				//std::cout.fill(prev);
			}
			catch(...) { handleException(std::current_exception(), CallMode::deferred); }

		}},

		taskRayTraceStart {{"raytrace_start", true}, [this]
		{
			try
			{
				startRayTrace();
			}
			catch(...) { handleException(std::current_exception(), CallMode::deferred); }
		}},

		taskReload {{"reload", true}, [this]
		{
			std::scoped_lock g{mRenderPropsMutex};

			//pl::tasks.critical([&](){

				pl::log("Application") << "Reloading scene.";

				bool isAct = rayTraceRenderWindow && rayTraceRenderWindow->started();

				if(rayTraceRenderWindow)
				{
					rayTraceRenderWindow->end();
					rayTraceRenderWindow->join();

					CallAfter([this]{
						destroyRenderWindow();
					});
				}

				destroyReferences();

				try
				{
					client.loadConfig(oConfigPath);
					client.openScene(client.mScene.getFilePath());
					buildRenderDevices();

					/*std::string configPath = client.mScene.getFilePath() + "/config.xml";
					if(boost::filesystem::exists(configPath))
						buildRenderDevices(configPath);
					else
						buildRenderDevices(oConfigPath.ToStdString());*/

					plugScene();

					CallAfter([this, isAct] {

						buildRenderWindow();
						updateUI();

						rayTraceRenderWindow->clearCurrentDevice();
						rayTraceRenderWindow->zeroDisplay();
						rayTraceRenderWindow->refreshImage();

						if(isAct)
							taskScheduler.push(taskRayTraceStart);

					});
				}
				catch(...) { handleException(std::current_exception(), CallMode::deferred); }

			//});
		}},

		taskSaveRenderState {{"save_render_state", true}, [this]
		{
			pl::log("Application") << "Saving render state...";

			try
			{
				std::shared_lock g{mRenderPropsMutex};
				const auto &camEntity = *prt::DynEntityCast<prt::IRenderEntity>(camera);

				prt::ParameterTree params {
					{std::string{camEntity.getEntityId()}, {"", camEntity.getParameters(false)}}/*,
					{"stats", {"", {
						{"render-time", {pl::io::to_string_noloc(std::chrono::duration_cast<std::chrono::seconds>(statsTimer.total_running_time()).count())}}
					}}}*/
				};

				if(cameraPerspective)
					params[cameraPerspective->getEntityId()].childParams = cameraPerspective->getParameters(false);

				fullRender.saveState(params);

				pl::log("Application") << "Done.";
			}
			catch(...) { handleException(std::current_exception(), CallMode::deferred); }

		}},

		taskOpen {{"open", true}, [this](const std::filesystem::path &path)
		{
			std::scoped_lock g{mRenderPropsMutex};

			bool isAct = mUiReady.load() && rayTraceRenderWindow && rayTraceRenderWindow->started();

			if(rayTraceRenderWindow)
			{
				rayTraceRenderWindow->end();
				rayTraceRenderWindow->join();

				CallAfter([this]{
					destroyRenderWindow();
				});
			}

			destroyReferences();

			bool success = false, hasRenderData = false;

			try
			{
				client.loadConfig(oConfigPath);
				client.openScene(path);
				buildRenderDevices();

				/*std::string configPath { client.mScene.getFilePath() + "/config.xml" };
				if(boost::filesystem::exists(configPath))
					buildRenderDevices(configPath);
				else
					buildRenderDevices(oConfigPath.ToStdString());*/

				plugScene();

				const auto params = fullRender.loadState();

				hasRenderData = (bool)params;

				if(hasRenderData)
				{
					for(const auto &p : *params)
						if(!p.first.empty() && p.second.childParams)
							client.mScene[p.first].setParameters(*p.second.childParams);

					/*if(params.contains("camera_ctl") && params["camera_ctl"].childParams)
						prt::DynEntityCast<prt::IRenderEntity>(camera)->setParameters(*params["camera_ctl"].childParams);

					if(cameraPerspective && params.contains("camera_perspective") && params["camera_perspective"].childParams)
						cameraPerspective->setParameters(*params["camera_perspective"].childParams);*/

					camera->update();
				}

				success = true;
			}
			catch(...) { handleException(std::current_exception(), CallMode::deferred); }

			if(success)
			{
				if(oDumpInfo)
				{
					std::cout << "Entity relation list:\n";

					for(auto&& [key, id] : client.mScene.mEntityRelations.left | pl::pair_view)
						std::cout << key << " -> " << id << '\n';
				}

				//while(!mUiReady) pl::tasks.yield();

				CallAfter([this, success, path, hasRenderData, isAct] {

					buildRenderWindow();
					updateUI();

					if(hasRenderData)
					{
						setPreviewMode(false);
						rayTraceRenderWindow->setClearOnStart(false);
						rayTraceRenderWindow->compileFilm();
					}
					else
					{
						setPreviewMode(true);
						rayTraceRenderWindow->setClearOnStart(true);
						//rayTraceRenderWindow->clearCurrentTarget();
						fullRender.mRenderContext->zeroBuffers();
						fullRender.mRenderContext->zeroFilms();
						rayTraceRenderWindow->zeroDisplay();
					}

					previewRender.mRenderContext->zeroBuffers();
					previewRender.mRenderContext->zeroFilms();

					rayTraceRenderWindow->refreshImage();

					if(success)
						appConfig.Write("last-load-path", wxString(path.string()));

					if(isAct)
						taskScheduler.push(taskRayTraceStart);

				});
			}

		}},

		taskSave {{"save", true}, [this](const std::filesystem::path &path)
		{
			pl::log("Application") << "Saving scene.";

			try
			{
				std::shared_lock g{mRenderPropsMutex};
				client.saveScene(path);

				//client.mParser->save(mRenderContext, scene.getFilePath() + "/config.xml");

				CallAfter([this, path]{
					appConfig.Write("last-load-path", wxString(path.string()));
				});

			}
			catch(...) { handleException(std::current_exception(), CallMode::deferred); }

		}},

		taskQueuedPostProcess (taskScheduler, {"postprocess", true},

			[this](const auto &params)
			{
				if(previewRender.mRenderContext && fullRender.mRenderContext)
				{
					std::scoped_lock g{mRenderPropsMutex};
					previewRender.mRenderContext->mLayerParams = fullRender.mRenderContext->mLayerParams = params;
				}
			},

			[this]
			{
				const auto layer = postProcessWindow->getSelectedLayer();

				if(layer.first.empty())
					return;

				auto &filmData = fullRender.mRenderContext->mFilms[layer.first];

				try
				{
					std::shared_lock g{mRenderPropsMutex};

					filmData.compile(layer.second);

					filmData.mFilm                     | sa::mode::shared |
					bitmapFinal                        | sa::mode::excl |
					postProcessWindow->image->drawDest | sa::mode::excl |
						[&](const auto &film, auto &bf, auto &drawDestImage)
						{
							if(film->getBitmap().empty())
								return;

							postProcessWindow->image->colorProfile = film->getColorProfile();

							PRTClientLib::DrawDestinationDithered<decltype(bf), const prt::IColorProfile &> drawDest{bf, *film->getColorProfile()};
							film->applyToneMap(drawDest);

							rg::copy(
								std::as_bytes(std::span{bf.buffer().mPixels.data(), bf.buffer().mPixels.size()}),
								std::as_writable_bytes(std::span{drawDestImage.mBitmap.image.GetData(), static_cast<szt>(drawDestImage.mBitmap.image.GetWidth()) * static_cast<szt>(drawDestImage.mBitmap.image.GetHeight()) * 3}).begin()
							);
						};

					postProcessWindow->image->updateDisplay();

				}
				catch(...) { handleException(std::current_exception(), CallMode::deferred); }

				postProcessWindow->CallAfter([this] { postProcessWindow->updateVirtualSize(); postProcessWindow->Refresh(false); });
			}

		)

	{}

	virtual ~MainApp() = default;

	// User locale
	std::locale userLoc{ pl::io::try_get_locale("") };

	MainFrame *mainFrame = nullptr;
	PostProcessWindow *postProcessWindow = nullptr;
	wxAuiNotebook *noteBook = nullptr;
	InteractiveRayTracerWindow *rayTraceRenderWindow = nullptr;
	RenderStatsWindow *renderStatsPanel = nullptr;
	wxPanel *renderStatsWindow = nullptr;
	//wxWindow *panelWindow = nullptr;

	wxAuiNotebook *panelNoteBook = nullptr;

	std::atomic<bool> mUiReady{false};

	sa::shared<
		prt::VirtualBitmap<
			prt::ImplBitmap::BitmapBuffer<
				prt::ColorInt24,
				pl::allocator_with_stats<prt::ColorInt24, prt::memtag::Film, prt::PmrAllocatorGlobal<prt::ColorInt24, prt::memtag::Film>>
			>
		>
	> bitmapFinal;

	wxTimer *inputTimer = nullptr, *autoSaveTimer = nullptr;
	pl::stop_watch<> inputTimerTime;

	RenderConfig clientConfig;

	std::random_device rng;

	prt::EntityPtr<prt::ICameraController> camera;
	prt::EntityPtr<prt::IRenderEntity> cameraPerspective;

	PRTClientLib::Client client;
	PRTClientLib::Render fullRender{client}, previewRender{client};

	std::shared_mutex mRenderPropsMutex;

	wxConfig appConfig {"photort-gui"};
	//wxLocale locale;

	// Can't initialize here with clang...
	Task<void(const std::filesystem::path &)> taskInfoDump;
	Task<void()> taskRayTraceStart;
	Task<void()> taskReload;
	Task<void()> taskSaveRenderState;
	Task<void(const std::filesystem::path &)> taskOpen;
	Task<void(const std::filesystem::path &)> taskSave;

	QueuedUniqueTask<void(), std::vector<prt::LayerParams>> taskQueuedPostProcess;

	TaskScheduler taskScheduler;

	void destroyReferences()
	{
		client.clear();
		fullRender.clear();
		previewRender.clear();

		camera = nullptr;
		cameraPerspective = nullptr;

		rayTraceReady = false;
	}

	void destroyRenderWindow()
	{
		if(!rayTraceRenderWindow)
			return;

		noteBook->Freeze();
		noteBook->RemovePage(noteBook->GetPageIndex(rayTraceRenderWindow));
		noteBook->Thaw();
		noteBook->Show(false);
		rayTraceRenderWindow->Destroy();
		rayTraceRenderWindow = nullptr;

		//mainFrame->auiManager.DetachPane(renderStatsPanel);
		//renderStatsPanel->Destroy();
		//renderStatsPanel = nullptr;
		//mainFrame->auiManager.Update();
		renderStatsWindow->DestroyChildren();
	}

	void buildRenderWindow()
	{
		rayTraceRenderWindow = new InteractiveRayTracerWindow(noteBook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxWANTS_CHARS);
		rayTraceRenderWindow->ShowScrollbars(wxSHOW_SB_ALWAYS, wxSHOW_SB_ALWAYS);
		rayTraceRenderWindow->SetScrollRate(1, 1);
		rayTraceRenderWindow->setScene(client.mScene);

		rayTraceRenderWindow->setPreviewRefreshRate(clientConfig.previewRefreshRate);
		rayTraceRenderWindow->setRefreshRate(clientConfig.refreshRate);
		rayTraceRenderWindow->setAutoFocus(clientConfig.autoFocus);
		rayTraceRenderWindow->setInvertMouse(clientConfig.invertMouse);

		rayTraceRenderWindow->listen(pl::ThreadEvent::Start, [this] {
			mainFrame->CallAfter([this]{
				mainFrame->buttonStart->SetBitmap(wxArtProvider::GetBitmap("media-playback-pause", wxART_MENU));
				mainFrame->auiToolBar->Realize();
			});
		});

		rayTraceRenderWindow->listen(pl::ThreadEvent::Pause, [this] {
			mainFrame->CallAfter([this]{
				mainFrame->buttonStart->SetBitmap(wxArtProvider::GetBitmap("media-playback-start", wxART_MENU));
				mainFrame->auiToolBar->Realize();
			});
		});

		rayTraceRenderWindow->listen(pl::ThreadEvent::PauseEnd, [this] {
			mainFrame->CallAfter([this]{
				mainFrame->buttonStart->SetBitmap(wxArtProvider::GetBitmap("media-playback-pause", wxART_MENU));
				mainFrame->auiToolBar->Realize();
			});
		});

		rayTraceRenderWindow->listen(pl::ThreadEvent::End, [this] {
			mainFrame->CallAfter([this]{
				mainFrame->buttonStart->SetBitmap(wxArtProvider::GetBitmap("media-playback-start", wxART_MENU));
				mainFrame->auiToolBar->Realize();
			});
		});

		noteBook->Freeze();
		noteBook->InsertPage(0, rayTraceRenderWindow, L"Render", true);
		noteBook->Thaw();

		noteBook->Show(true);

		renderStatsWindow->Freeze();
		//renderStatsPanel = new RenderStatsWindow(renderStatsWindow);
		//renderStatsWindow->DestroyChildren();
		rayTraceRenderWindow->createRenderStatsWindow(renderStatsWindow);
		renderStatsWindow->Layout();
		//renderStatsWindow->GetSizer()->Add(renderStatsPanel, wxSizerFlags(1).Expand().Border(wxALL));
		//renderStatsPanel->Fit();
		//renderStatsWindow->Fit();
		renderStatsWindow->Thaw();

		mainFrame->auiManager.Update();
	}

	void loadClientConfig()
	{
		const auto config = PRTClientLib::loadConfigFile(oClientConfigPath, *client.mParser, default_prt_client_config);

		//std::cout << config << '\n';

		config.getValueIfExists
			("preview_nb_pixels",        clientConfig.previewNbPixels)
			("preview_refresh_rate",     clientConfig.previewRefreshRate)
			("refresh_rate",             clientConfig.refreshRate)
			("auto_save_render_state",   clientConfig.autoSaveRenderState)
			("auto_focus",               clientConfig.autoFocus)
			("invert_mouse",             clientConfig.invertMouse)
			("auto_open",                clientConfig.autoOpen)
		;
	}

	void buildRenderDevices()
	{
		//mRenderContext.print(std::cout);

		fullRender.loadFromConfig("devices");
		fullRender.loadRenderParams("main_final_render_context");
		previewRender.loadFromConfig("preview_devices");

		if(fullRender.mRenderContext->mFilms.empty())
			throw ApplicationException("No film defined for the scene.");

		// Create preview render film
		previewRender.mRenderContext = client.mRender.createEntity(type_v<prt::RenderContext>, "rendercontext-", "preview_render_context");
		auto scenePreviewParams = client.mScene.getEntitySptr<prt::RenderContext>("preview_render_context");

		previewRender.mRenderContext->mRenderParams = scenePreviewParams->mRenderParams;

		*previewRender.mRenderContext->mFilms["preview_film"].mFilm.access(sa::mode::excl) = client.mRender.createEntity(type_v<prt::Film>, "film-", "preview_film");

		previewRender.mRenderDeviceController->setRenderContext(previewRender.mRenderContext);
	}

	void plugScene()
	{
		camera = client.mScene.getEntitySptr("main_cameractl", type_v<prt::ICameraController>);

		if(!camera)
			camera = createDefaultCamera();

		if(client.mScene.hasEntity("main_camera_projection"))
			cameraPerspective = client.mScene.getEntitySptr("main_camera_projection");
		else
			cameraPerspective = nullptr;

		auto &renderContext = fullRender.mRenderContext;

		if(renderContext->mFilms.empty())
			throw ApplicationException("No film defined for the scene.");

		renderContext->mFilms.begin()->second.mFilm                | sa::mode::shared |
		previewRender.mRenderContext->mFilms["preview_film"].mFilm | sa::mode::excl |
		bitmapFinal                                                | sa::mode::excl |
			[&](const auto &firstFilm, auto &previewFilm, auto &bf)
			{
				previewFilm->setToneMapper(firstFilm->getToneMapper());
				previewFilm->setCamera(firstFilm->getCamera());
				previewFilm->setColorProfile(firstFilm->getColorProfile());

				const auto filmSize = firstFilm->getSize();

				const auto size = prt::BitmapAlgo::fitSizeToAspectRatio(filmSize[0], filmSize[1], clientConfig.previewNbPixels);
				previewFilm->setSize(size);

				//previewRender.mRenderDeviceController->setRenderContext(previewRender.mRenderContext);

				bf.allocate(filmSize);

				pl::log("Application", "Preview bitmap: {} x {}", size[0], size[1]);
			};

		/*std::cout << "Render entities:" << '\n';

		for(auto &entity : client.mScene.entities.left | rgv::values)
		{
			entity->getParameters(true).serialize(std::cout);
			std::cout << '\n';
		}*/

		fullRender.initForRendering();
		previewRender.initForRendering();

		previewRender.mRenderContext->zeroBuffers();
		previewRender.mRenderContext->zeroFilms();

		camera->update();
	}

	void updateUI()
	{
		postProcessWindow->image->setSize( static_cast<prt::Dir2Di>(bitmapFinal.access(sa::mode::shared)->getSize()) );

		taskQueuedPostProcess.modifyParams([&](auto &params){
			std::scoped_lock g{mRenderPropsMutex};
			params = fullRender.mRenderContext->mLayerParams;
		});

		rayTraceRenderWindow->setRenderPropsMutex(&mRenderPropsMutex);
		rayTraceRenderWindow->setCamera(*camera);
		rayTraceRenderWindow->setCameraPerspective(*cameraPerspective);
		rayTraceRenderWindow->setRenderDevice(*fullRender.mRenderDeviceController, *previewRender.mRenderDeviceController);
		rayTraceRenderWindow->priority(pl::EnumThreadPriority::Low);
		rayTraceRenderWindow->setPreviewMode(true);

		postProcessWindow->filmSelect->Clear();

		for(const auto &film : fullRender.mRenderContext->mFilms)
			postProcessWindow->filmSelect->Append(film.first);

		postProcessWindow->filmSelect->SetSelection(0);

		postProcessWindow->layerSelect->Clear();

		postProcessWindow->layerSelect->Append("All");
		postProcessWindow->layerSelect->Append("default");
		for(const auto &layer : client.mScene.getLightGroups())
			postProcessWindow->layerSelect->Append(layer);

		postProcessWindow->layerSelect->SetSelection(0);

		postProcessWindow->layerParamSelect->Clear();
		postProcessWindow->layerParamSelect->Append("default");
		for(const auto &layer : client.mScene.getLightGroups())
			postProcessWindow->layerParamSelect->Append(layer);

		if(postProcessWindow->layerParamSelect->GetCount() > 0)
		{
			postProcessWindow->layerParamSelect->SetSelection(0);

			taskQueuedPostProcess.accessParams([&](const auto &params){
				postProcessWindow->setPowerSelect(params[postProcessWindow->layerParamSelect->GetSelection()].power);
			});

			taskQueuedPostProcess.launchQueued();
		}

		auto path = wxFileName(client.mScene.getFilePath().string());

		path.Normalize(wxPATH_NORM_ABSOLUTE);

		mainFrame->SetTitle("PhotoRT - " + path.GetFullPath());

		if(clientConfig.autoSaveRenderState > 0)
			autoSaveTimer->Start(clientConfig.autoSaveRenderState * 1000);
		else
			autoSaveTimer->Stop();
	}


	prt::EntityPtr<prt::ICameraController> createDefaultCamera()
	{
		return client.mScene.createEntity(type_v<prt::ICameraController>, "cameractl-single", "main_cameractl", R"(
			<e>
				<transform type="transform-multi">
					<transforms>
						<transform_point o="0">
							<t type="transform-translation" position="0" />
						</transform_point>

						<rotation o="1">
							<t type="transform-rotation" angle="25 180 0 0" />
						</rotation>

						<position o="2">
							<t type="transform-translation" position="-2.11568 69.7959 124.05 0" />
						</position>
					</transforms>
				</transform>

				<up>0 1 0 1</up>
				<lookat>0 0 1 1</lookat>

				<camera type="camera-lens" id="main_camera">
					<flat />

					<projection type="transform-perspective" id="main_camera_projection" fov="50" />

					<lens type="lens-primitive">
						<primitive type="object-disks">
							<nb_primitives>1</nb_primitives>

							<primitives>
								0 0 0 1 0 0 0 1 0 0.3 0
							</primitives>
						</primitive>
					</lens>
				</camera>
			</e>
		)");
	}

	prt::EntityPtr<prt::ICameraController> createDefault2EyesCamera()
	{
		return client.mScene.createEntity(type_v<prt::ICameraController>, "cameractl-single", "main_cameractl", R"(
			<e>
				<transform type="transform-multi">
					<transforms>
						<transform_point o="0">
							<t type="transform-translation" position="0" />
						</transform_point>

						<rotation o="1">
							<t type="transform-rotation" angle="25 180 0 0" />
						</rotation>

						<position o="2">
							<t type="transform-translation" position="-2.11568 69.7959 124.05 0" />
						</position>
					</transforms>
				</transform>

				<up>0 1 0 1</up>
				<lookat>0 0 1 1</lookat>

				<camera_left type="camera-lens" id="main_camera">
					<flat />

					<projection type="transform-perspective" id="main_camera_projection" fov="50" />

					<lens type="lens-primitive">
						<primitive type="object-disks">
							<nb_primitives>1</nb_primitives>

							<primitives>
								0 0 0 1 0 0 0 1 0 0.3 0
							</primitives>
						</primitive>
					</lens>
				</camera_left>

				<camera_right type="camera-lens" id="right_camera">
					<flat />

					<projection type="transform-perspective" id="main_camera_projection" fov="50" />

					<lens type="lens-primitive">
						<primitive type="object-disks">
							<nb_primitives>1</nb_primitives>

							<primitives>
								0 0 0 1 0 0 0 1 0 0.3 0
							</primitives>
						</primitive>
					</lens>
				</camera_right>
			</e>
		)");
	}

	/*template <CDeferTag Defer_ = tag::deferred_t>
	void displayError(const pl::Exception &e, Defer_ defer = {}) noexcept
	{
		pl::log(e.module(), pl::logtag::error) << e.message();

		if constexpr(defer == CallMode::deferred)
			CallAfter([this, mes = stringViewToWxString(e.message()), mod = wxString(e.module())]{ wxMessageBox(mes, wxString{"Error in "} + mod, wxOK | wxCENTER | wxICON_ERROR, mainFrame); });
		else
			wxMessageBox(stringViewToWxString(e.message()), wxString{"Error in "} + e.module(), wxOK | wxCENTER | wxICON_ERROR, mainFrame);
	}

	template <CDeferTag Defer_ = tag::deferred_t>
	void displayError(const std::exception &e, Defer_ defer = {}) noexcept
	{
		pl::log("Application", pl::logtag::error) << "Error: " << e.what();

		if constexpr(defer == CallMode::deferred)
			CallAfter([this, mes = wxString(e.what())]{ wxMessageBox(mes, "Error", wxOK | wxCENTER | wxICON_ERROR, mainFrame); });
		else
			wxMessageBox(e.what(), "Error", wxOK | wxCENTER | wxICON_ERROR, mainFrame);
	}

	template <CDeferTag Defer_ = tag::deferred_t>
	void displayError(Defer_ defer = {}) noexcept
	{
		pl::log("Application", pl::logtag::error) << "Unexpected error.";

		if constexpr(defer == CallMode::deferred)
			CallAfter([this]{ wxMessageBox("Unexpected error.", "Error", wxOK | wxCENTER | wxICON_ERROR, mainFrame); });
		else
			wxMessageBox("Unexpected error.", "Error", wxOK | wxCENTER | wxICON_ERROR, mainFrame);
	}*/

	void handleException(std::exception_ptr eptr, CallMode mode = CallMode::deferred) noexcept
	{
		PRTClientLib::logException(eptr, "gui");

		std::string message;

		pl::recurse_nested_exceptions(eptr, [&](std::exception_ptr e){
			try { std::rethrow_exception(e); }
			catch(const pl::Exception &e)
			{
				message.append(fmt::format(userLoc, "[{}] {}\n", e.module(), e.message()));
			}
			catch(const std::system_error &e)
			{
				pl::strconcat(message, fmt::format("System error: [{}] {}\n", fmt::streamed(e.code()), e.what()));
			}
			catch(const std::exception &e)
			{
				pl::strconcat(message, e.what(), '\n');
			}
			catch(...)
			{
				message.append("Unexpected error.\n");
			}
		});

		if(mode == CallMode::deferred)
			CallAfter([this, message = std::move(message)]{ wxMessageBox(message, L"Error", wxOK | wxCENTER | wxICON_ERROR, mainFrame); });
		else
			wxMessageBox(message, L"Error", wxOK | wxCENTER | wxICON_ERROR, mainFrame);
	}

	virtual bool OnExceptionInMainLoop() override
	{
		handleException(std::current_exception(), CallMode::immediate);

		return false;
	}

	void setPreviewMode(bool previewMode)
	{
		rayTraceRenderWindow->setPreviewMode(previewMode);

		if(previewMode)
			mainFrame->buttonPreviewMode->SetBitmap(wxArtProvider::GetBitmap("semi-starred", wxART_MENU));
		else
			mainFrame->buttonPreviewMode->SetBitmap(wxArtProvider::GetBitmap("starred", wxART_MENU));

		mainFrame->auiToolBar->Realize();
	}

	bool getPreviewMode() const
	{
		return rayTraceRenderWindow->getPreviewMode();
	}

	void loadModules()
	{
		client.loadAllModules(oModulePath.c_str());
	}

	std::atomic<bool> rayTraceReady{false};
	void startRayTrace()
	{
		if(!rayTraceReady)
		{
			client.initForRendering();
			rayTraceRenderWindow->setRayTracer(std::to_address(client.mRayTracer));
			rayTraceReady = true;
		}

		//std::cout << client.mScene.getEntity("main_camera", type_v<prt::ICamera>)->getSubstanceStack() << '\n';

		rayTraceRenderWindow->start();

		/*CallAfter([this] {
			char prev = std::cout.fill(' ');

			std::cout << "\n\n --- Ray tracer information --- \n";

			client.mRayTracer->printInfo(std::cout);
			std::cout << '\n';

			std::cout.fill(prev);
		});*/

	}

	/*void loadTestScene()
	{
		client.mScene.loadFromDirectory(appPath + "/../../resources/testscene/basic/");
		buildTestScene();
		plugScene();
		buildRenderWindow();
		updateUI();
	}*/

	virtual bool OnInit() override
	{
		if(!wxApp::OnInit())
			return false;

		wxInitAllImageHandlers();

		SetAppName(L"PhotoRT");
		SetAppDisplayName(L"PhotoRT");

		//locale.Init(wxLANGUAGE_DEFAULT);
		try
		{
			//pl::recreate(userLoc, "");

			std::cin.imbue(std::locale::classic());
			std::cout.imbue(userLoc);
			std::cerr.imbue(userLoc);
			std::clog.imbue(userLoc);

			std::cout << std::defaultfloat << std::dec;
			std::cerr << std::defaultfloat << std::dec;
			std::clog << std::defaultfloat << std::dec;
		}
		catch(...)
		{
			handleException(std::current_exception(), CallMode::immediate);
		}

		fmt::print(
			"Number of cores detected: {:L}.\n"
			"Maximum number of threads: {:L}.\n",
			std::thread::hardware_concurrency(),
			pl::tasks.max_threads()
		);

		#ifdef PL_LIB_NUMA
			if(numa_available() > -1)
				fmt::print("Number of numa nodes: {:L}.\n", pl::numa::node_count());
			else
				std::cout << "Numa information not available.\n";
		#endif

		fmt::print(
			"App path: {}.\n"
			"Client config file: {}.\n"
			"Render config file: {}.\n"
			"Modules path: {}.\n",
			paths::app_dir(),
			oClientConfigPath,
			oConfigPath,
			oModulePath
		);

		mainFrame = new MainFrame(nullptr);

		try
		{
			loadModules();
		}
		catch(...)
		{
			handleException(std::current_exception(), CallMode::immediate);
			return false;
		}

		if(oDumpInfo)
		{
			//std::cout << "Locale: " << locale.GetName().ToStdString();
			//std::cout << "\n\n";

			std::cout << "Factories:\n";
			prt::factories.dumpFactories(std::cout);
			std::cout << "\n\n";

			std::cout << "Importers/exporters:\n";
			prt::factories.dumpImporterExporter(std::cout);
			std::cout << "\n\n";
		}

		//taskScheduler.nb_threads(nbCores);

		/*std::cout << "Filter dump:" << '\n';

		wxImage filterDump(filter.filterWidth, filter.filterHeight, true);

		filterDump.InitAlpha();

		for(int y=0; y<filter.filterHeight; y++)
		{
			for(int x=0; x<filter.filterWidth; x++)
			{
				//std::cout << filter.precalc[y * filter.filterWidth + x] << '\n';
				filterDump.SetRGB(x, y, 255, 0, 0);
				filterDump.SetAlpha(x, y, pl::plmin(filter.precalc[y * filter.filterWidth + x] * 255.0f * filter.width * filter.height, 255.0f));
			}
		}

		filterDump.SaveFile("filter.png", wxBITMAP_TYPE_PNG);*/


		client.setParser(std::make_shared<prt::ParameterParserXerces>());

		try
		{
			loadClientConfig();
		}
		catch(...)
		{
			handleException(std::current_exception(), CallMode::immediate);
		}

		{
			std::filesystem::path path = !oPath.empty() ? oPath :
				std::filesystem::path(clientConfig.autoOpen ? appConfig.Read("last-load-path").ToStdString() : std::string{});

			if(!path.empty())
			{
				path = pl::paths::absolute(path);

				if(!path.empty())
				{
					pl::log("Application") << "Opening scene from " << path;

					taskScheduler.push(taskOpen, 0ms, path);
				}
			}
		}

		noteBook = new wxAuiNotebook(mainFrame, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxAUI_NB_TOP | wxAUI_NB_TAB_SPLIT);

		postProcessWindow = new PostProcessWindow(noteBook);

		noteBook->Freeze();
		noteBook->AddPage(postProcessWindow, L"Post processing");
		noteBook->Thaw();

		Bind(wxEVT_COMMAND_BUTTON_CLICKED, [this](wxCommandEvent &)
		{
			taskQueuedPostProcess.launchQueued();

		}, postProcessWindow->postProcessBtn->GetId());

		Bind(wxEVT_CHOICE, [this](wxCommandEvent &)
		{
			taskQueuedPostProcess.launchQueued();

		}, postProcessWindow->filmSelect->GetId());

		Bind(wxEVT_CHOICE, [this](wxCommandEvent &)
		{
			taskQueuedPostProcess.launchQueued();

		}, postProcessWindow->layerSelect->GetId());

		Bind(wxEVT_CHOICE, [this](wxCommandEvent &)
		{
			taskQueuedPostProcess.accessParams([&](const auto &params){
				postProcessWindow->setPowerSelect(params[postProcessWindow->layerParamSelect->GetSelection()].power);
			});

		}, postProcessWindow->layerParamSelect->GetId());

		Bind(wxEVT_SLIDER, [this](wxCommandEvent &)
		{
			if(const auto sel = postProcessWindow->layerParamSelect->GetSelection(); sel >= 0)
			{
				taskQueuedPostProcess.modifyParams([&](auto &params){
					params[sel].power = postProcessWindow->getPowerSelect();
				});

				taskQueuedPostProcess.launchQueued(250ms);
			}

		}, postProcessWindow->layerPowerCtrl->GetId());

		Bind(wxEVT_COMMAND_BUTTON_CLICKED, [this](wxCommandEvent &)
		{
			if(const auto sel = postProcessWindow->layerParamSelect->GetSelection(); sel >= 0)
			{
				postProcessWindow->setPowerSelect(1);

				taskQueuedPostProcess.modifyParams([&](auto &params){
					params[sel].power = 1;
				});

				taskQueuedPostProcess.launchQueued();
			}

		}, postProcessWindow->layerPowerResetBtn->GetId());


		SetTopWindow(mainFrame);

		/*mainFrame->SetSize(wxSize(1400, 1000));
		mainFrame->CentreOnParent(wxBOTH);
		mainFrame->Maximize();*/

		inputTimer = new wxTimer(this);

		Bind(wxEVT_TIMER, [this](wxTimerEvent &) {
			inputTimerTime.check();

			if(mainFrame->IsActive() && rayTraceRenderWindow && getPreviewMode() && rayTraceRenderWindow->running())
			{
				rayTraceRenderWindow->pollKeyStates(inputTimerTime.period() * 1000.0);
			}
		}, inputTimer->GetId());

		inputTimerTime.start();
		inputTimer->Start(60);

		autoSaveTimer = new wxTimer(this);

		Bind(wxEVT_TIMER, [this](wxTimerEvent &) {

			std::shared_lock g{mRenderPropsMutex};

			if(rayTraceRenderWindow && !getPreviewMode() && rayTraceRenderWindow->running())
				taskScheduler.push(taskSaveRenderState);

		}, autoSaveTimer->GetId());

		Bind(wxEVT_COMMAND_MENU_SELECTED, [this](wxCommandEvent &)
		{
			wxDirDialog dirDialog(mainFrame, L"Dump render information", appConfig.Read("last-dump-path"), wxDD_DEFAULT_STYLE | wxDD_NEW_DIR_BUTTON);

			if(dirDialog.ShowModal() != wxID_CANCEL)
			{
				taskScheduler.push(taskInfoDump, 0ms, dirDialog.GetPath().ToStdString());

				appConfig.Write("last-dump-path", dirDialog.GetPath());
			}

		}, mainFrame->m_menuDumpInfo->GetId());

		/*Bind(wxEVT_COMMAND_MENU_SELECTED, [this](wxCommandEvent &evt)
		{
			bool isAct = rayTraceRenderWindow && rayTraceRenderWindow->started();

			if(rayTraceRenderWindow)
			{
				rayTraceRenderWindow->end();
				rayTraceRenderWindow->join();

				destroyRenderWindow();
			}

			try
			{
				loadTestScene();
			}
			catch(...) { handleException(std::current_exception()); }

			if(isAct)
				taskScheduler.push(taskRayTraceStart);

		}, mainFrame->m_menuLoadTestScene1->GetId());*/

		Bind(wxEVT_COMMAND_MENU_SELECTED, [this](wxCommandEvent &)
		{
			wxDirDialog dirDialog(mainFrame, L"Save scene", appConfig.Read("last-load-path"), wxDD_DEFAULT_STYLE | wxDD_NEW_DIR_BUTTON);

			if(dirDialog.ShowModal() != wxID_CANCEL)
			{
				taskScheduler.push(taskSave, 0ms, dirDialog.GetPath().ToStdString());
			}

		}, mainFrame->m_menuSave->GetId());

		Bind(wxEVT_COMMAND_MENU_SELECTED, [this](wxCommandEvent &)
		{
			taskScheduler.push(taskSaveRenderState);

			autoSaveTimer->Stop();
			autoSaveTimer->Start(clientConfig.autoSaveRenderState * 1000);

		}, mainFrame->m_menuSaveRender->GetId());

		Bind(wxEVT_COMMAND_MENU_SELECTED, [this](wxCommandEvent &)
		{
			wxDirDialog dirDialog(mainFrame, L"Open scene", appConfig.Read("last-load-path"), wxDD_DEFAULT_STYLE | wxDD_DIR_MUST_EXIST);

			if(dirDialog.ShowModal() != wxID_CANCEL)
			{
				taskScheduler.push(taskOpen, 0ms, dirDialog.GetPath().ToStdString());
			}

		}, mainFrame->m_menuOpen->GetId());

		Bind(wxEVT_MENU, [this](wxCommandEvent &)
		{
			std::shared_lock g{mRenderPropsMutex};

			if(!rayTraceRenderWindow)
				return;

			if(!rayTraceRenderWindow->started() || rayTraceRenderWindow->paused())
				taskScheduler.push(taskRayTraceStart);
			else
				rayTraceRenderWindow->pause();

		}, mainFrame->buttonStart->GetId());

		/*Bind(wxEVT_MENU, [this](wxCommandEvent &)
		{
			rayTraceRenderWindow->pause();
			mainFrame->buttonStart->SetActive(true);

		}, mainFrame->buttonPause->GetId());*/

		Bind(wxEVT_MENU, [this](wxCommandEvent &)
		{
			std::shared_lock g{mRenderPropsMutex};

			if(!rayTraceRenderWindow)
				return;

			if(rayTraceRenderWindow->running())
				rayTraceRenderWindow->restart();
			else
			{
				rayTraceRenderWindow->clearCurrentDevice();
				rayTraceRenderWindow->zeroDisplay();
				rayTraceRenderWindow->refreshImage();

				if(rayTraceRenderWindow->started())
					rayTraceRenderWindow->restart();
			}

			noteBook->SetSelection(noteBook->FindPage(rayTraceRenderWindow));

		}, mainFrame->buttonRestart->GetId());

		/*Bind(wxEVT_MENU, [this](wxCommandEvent &)
		{
			if(!rayTraceRenderWindow)
				return;

			rayTraceRenderWindow->end();

		}, mainFrame->buttonEnd->GetId());*/

		Bind(wxEVT_MENU, [this](wxCommandEvent &)
		{
			std::shared_lock g{mRenderPropsMutex};

			if(!rayTraceRenderWindow)
				return;

			setPreviewMode(!getPreviewMode());

		}, mainFrame->buttonPreviewMode->GetId());

		Bind(wxEVT_MENU, [this](wxCommandEvent &)
		{
			if(!rayTraceRenderWindow)
				return;

			taskScheduler.push(taskReload);

		}, mainFrame->buttonReload->GetId());

		Bind(wxEVT_MENU, [this](wxCommandEvent &)
		{
			std::shared_lock g{mRenderPropsMutex};

			if(!rayTraceRenderWindow)
				return;

			rayTraceRenderWindow->unselectArea();
			rayTraceRenderWindow->refreshImage();

		}, mainFrame->buttonUnselectArea->GetId());


		mainFrame->Bind(wxEVT_CLOSE_WINDOW, [this](wxCloseEvent &evt)
		{
			evt.Skip();

			if(evt.CanVeto())
				evt.Veto();

			if(pl::tasks.started())
			{
				pl::tasks.end();
				pl::tasks.join();
			}

			std::scoped_lock g{mRenderPropsMutex};

			if(rayTraceRenderWindow)
			{
				rayTraceRenderWindow->end();
				rayTraceRenderWindow->join();
				rayTraceRenderWindow = nullptr;
			}

			inputTimer->Stop();

			appConfig.Write("Perspective", mainFrame->auiManager.SavePerspective());

			mainFrame->Destroy();

		}, mainFrame->GetId());



		//renderStatsPanel->

		/*Bind(wxEVT_COMMAND_BUTTON_CLICKED, [this](wxCommandEvent &evt)
		{
			auto postProcessWindow = postProcessWindow;

			wxFileDialog fileDialog(postProcessWindow, L"Save as .exr", "", "", "OpenEXR files (*.exr)|*.exr", wxFD_SAVE|wxFD_OVERWRITE_PROMPT);

			fileDialog.SetPath(appConfig.Read("last-save-path"));

			if(fileDialog.ShowModal() != wxID_CANCEL)
			{
				//contributionBuffer->saveLightImageToFile(fileDialog.GetPath().ToStdString());

				appConfig.Write("last-save-path", fileDialog.GetPath());
			}

		}, postProcessWindow->saveLightButton->GetId());*/

		/*Bind(wxEVT_COMMAND_BUTTON_CLICKED, [this](wxCommandEvent &)
		{
			auto layer = postProcessWindow->getSelectedLayer();
			auto &film = fullRender.mRenderContext->mFilms[layer.first];

			wxFileDialog fileDialog(postProcessWindow, L"Save as .exr", "", "", "OpenEXR files (*.exr)|*.exr", wxFD_SAVE|wxFD_OVERWRITE_PROMPT);

			fileDialog.SetPath(appConfig.Read("last-save-path"));

			if(fileDialog.ShowModal() != wxID_CANCEL)
			{
				wxFileName path(fileDialog.GetPath());

				if(!path.HasExt())
					path.SetExt("exr");

				try
				{
					std::scoped_lock g(prt::objectMutex(*film), prt::objectSharedMutex(*fullRender.mRenderContext));
					film->compile(layer.second);

					std::scoped_lock g2(prt::objectSharedMutex(film->getBitmap()));

					client.mScene.exportEntity(film->getBitmap(), path.GetFullPath().ToStdString(), "image/x-exr", prt::ParameterTree());
				}
				catch(...) { handleException(std::current_exception(), bool_v<false>); }

				appConfig.Write("last-save-path", path.GetPath() + '/' + path.GetName());
			}

		}, postProcessWindow->saveUnfilteredHDRbutton->GetId());*/

		Bind(wxEVT_COMMAND_BUTTON_CLICKED, [this](wxCommandEvent &)
		{
			auto layer = postProcessWindow->getSelectedLayer();
			auto &filmData = fullRender.mRenderContext->mFilms[layer.first];

			wxFileDialog fileDialog(postProcessWindow, L"Save as .exr", "", "", "OpenEXR files (*.exr)|*.exr", wxFD_SAVE|wxFD_OVERWRITE_PROMPT);

			fileDialog.SetPath(appConfig.Read("last-save-path"));

			if(fileDialog.ShowModal() != wxID_CANCEL)
			{
				//contributionBuffer->saveToFile(fileDialog.GetPath().ToStdString());

				wxFileName path(fileDialog.GetPath());

				if(!path.HasExt())
					path.SetExt("exr");

				try
				{
					{
						std::shared_lock g{mRenderPropsMutex};
						filmData.compile(layer.second);
					}

					client.mScene.exportEntity((**filmData.mFilm.access(sa::mode::shared)).getBitmap(), path.GetFullPath().ToStdString(), "image/x-exr", prt::ParameterTree{});
				}
				catch(...) { handleException(std::current_exception(), CallMode::immediate); }

				appConfig.Write("last-save-path", path.GetPath() + '/' + path.GetName());
			}

		}, postProcessWindow->saveHDRbutton->GetId());

		Bind(wxEVT_COMMAND_BUTTON_CLICKED, [this](wxCommandEvent &)
		{
			auto bmpAccess = bitmapFinal.access(sa::mode::shared);

			if(bmpAccess->isEmpty())
			{
				wxMessageBox("No image to save.", "Error", wxOK | wxCENTER | wxICON_ERROR, mainFrame);
				return;
			}

			auto layer = postProcessWindow->getSelectedLayer();
			auto &filmData = fullRender.mRenderContext->mFilms[layer.first];

			wxFileDialog fileDialog(postProcessWindow, L"Save as .png", "", "", "PNG files (*.png)|*.png", wxFD_SAVE|wxFD_OVERWRITE_PROMPT);

			fileDialog.SetPath(appConfig.Read("last-save-path"));

			if(fileDialog.ShowModal() != wxID_CANCEL)
			{
				wxFileName path(fileDialog.GetPath());

				if(!path.HasExt())
					path.SetExt("png");

				filmData.mFilm | sa::mode::shared |
					[&](const auto &film)
					{
						const auto &cp = dynamic_cast<const prt::IRenderEntity &>(*film->getColorProfile());

						std::shared_lock g{mRenderPropsMutex};

						client.mScene.exportEntity(*bmpAccess, path.GetFullPath().ToStdString(), "image/png", prt::ParameterTree{
							{"colorprofile", {cp.getEntityType(), "", cp.getParameters(true)}}
						});
					};

				appConfig.Write("last-save-path", path.GetPath() + '/' + path.GetName());
			}

		}, postProcessWindow->saveLDRbutton->GetId());

		mainFrame->auiManager.AddPane(noteBook, wxAuiPaneInfo().Name(L"film").Caption(L"Film").CloseButton(false)
			.Floatable().Resizable().PaneBorder().CenterPane());
		noteBook->Show(false);

		{
			//panelWindow = new wxWindow();
			wxSizer *sizer = new wxBoxSizer(wxHORIZONTAL);

			panelNoteBook = new wxAuiNotebook(mainFrame, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxAUI_NB_TOP | wxAUI_NB_TAB_SPLIT);
			panelNoteBook->Freeze();

			renderStatsWindow = new wxPanel(panelNoteBook, wxID_ANY);
			renderStatsWindow->SetSizer(sizer);

			panelNoteBook->AddPage(renderStatsWindow, L"Statistics");

			panelNoteBook->Thaw();

			mainFrame->auiManager.AddPane(panelNoteBook, wxAuiPaneInfo().Name(L"panel").Caption(L"Tools").Resizable().Floatable().Left().GripperTop()
				.CaptionVisible().CloseButton(false).PaneBorder().PinButton().BestSize(wxSize(470, 100)).MinSize(wxSize(200, 100)));
		}

		{
			wxString cfgdata;
			appConfig.Read("Perspective", &cfgdata);

			if(!cfgdata.IsEmpty())
				mainFrame->auiManager.LoadPerspective(cfgdata, false);

			mainFrame->auiManager.Update();
		}

		std::vector<wxAcceleratorEntry> mAccelerators{
			{wxACCEL_NORMAL, WXK_F1, mainFrame->buttonStart->GetId()},
			{wxACCEL_NORMAL, WXK_F2, mainFrame->buttonPreviewMode->GetId()},
			{wxACCEL_NORMAL, WXK_F5, mainFrame->buttonReload->GetId()}
		};

		#if wxCHECK_VERSION(3, 1, 0)
			mAccelerators.emplace_back(wxACCEL_NORMAL, WXK_MEDIA_PLAY_PAUSE, mainFrame->buttonStart->GetId());
		#endif

		wxAcceleratorTable accelTable{(int)mAccelerators.size(), mAccelerators.data()};
		mainFrame->SetAcceleratorTable(accelTable);

		if(!GetTopWindow()->Show())
			return false;

		mUiReady = true;
		//mUiReady.notify_all();

		return true;
	}

	std::filesystem::path oPath, oConfigPath, oModulePath, oClientConfigPath;

	bool oDumpInfo = false;

	virtual void OnInitCmdLine(wxCmdLineParser &parser) override
	{
		wxApp::OnInitCmdLine(parser);

		parser.EnableLongOptions();

		static const wxCmdLineEntryDesc cmdLineDesc[] = {
			{ wxCMD_LINE_PARAM, nullptr, nullptr, "Scene path", wxCMD_LINE_VAL_STRING, wxCMD_LINE_PARAM_OPTIONAL },
			{ wxCMD_LINE_OPTION, "c", "config", "Render configuration file to use", wxCMD_LINE_VAL_STRING, wxCMD_LINE_PARAM_OPTIONAL },
			{ wxCMD_LINE_OPTION, nullptr, "clientconfig", "Client configuration file to use", wxCMD_LINE_VAL_STRING, wxCMD_LINE_PARAM_OPTIONAL },
			{ wxCMD_LINE_OPTION, "m", "modules", "Path to PhotoRT modules", wxCMD_LINE_VAL_STRING, wxCMD_LINE_PARAM_OPTIONAL },
			{ wxCMD_LINE_SWITCH, "d", "dumpinfo", "Dump some information on output", wxCMD_LINE_VAL_NONE, wxCMD_LINE_PARAM_OPTIONAL },
			{ wxCMD_LINE_NONE, nullptr, nullptr, nullptr, wxCMD_LINE_VAL_NONE, 0 }
		};

		parser.SetDesc(cmdLineDesc);
	}

	virtual bool OnCmdLineParsed(wxCmdLineParser &parser) override
	{
		if(!wxApp::OnCmdLineParsed(parser))
			return false;

		if(parser.GetParamCount())
			oPath = parser.GetParam(0).ToStdString();

		{
			wxString str;

			parser.Found("config", &str);
			oConfigPath = str.ToStdString();
			if(oConfigPath.empty())
				oConfigPath = paths::user_config_dir() / "photort/config.xml";
		}

		oDumpInfo = parser.FoundSwitch("dumpinfo");

		{
			wxString str;

			parser.Found("modules", &str);
			oModulePath = str.ToStdString();
			if(oModulePath.empty())
				oModulePath = PRTClientLib::getAppPluginDir();
		}

		{
			wxString str;

			parser.Found("clientconfig", &str);
			oClientConfigPath = str.ToStdString();
			if(oClientConfigPath.empty())
				oClientConfigPath = paths::user_config_dir() / "photort/client_config.xml";
		}

		/*std::cout <<
			"oPath: " << oPath << '\n' <<
			"oConfigPath: " << oConfigPath << '\n' <<
			"oModulePath: " << oModulePath << '\n' <<
			"oClientConfigPath: " << oClientConfigPath << '\n';*/

		return true;
	}
};

wxDECLARE_APP(MainApp);
wxIMPLEMENT_APP_NO_MAIN(MainApp);

[[nodiscard]] int appMain(int argc, char **argv) noexcept
try
{
	wxDISABLE_DEBUG_SUPPORT();

	#if defined(__WXGTK__) && defined(GDK_WINDOWING_X11)
		XInitThreads();
	#endif

	std::pmr::set_default_resource(&pl::default_resource());

	return wxEntry(argc, argv);
}
catch(...)
{
	PRTClientLib::logException(std::current_exception(), "gui");
	return EXIT_FAILURE;
}

} // namespace PhotoRTGUI

int main(int argc, char **argv)
{
	return PhotoRTGUI::appMain(argc, argv);
}
