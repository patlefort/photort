/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotoRT.hpp>

#include <PhotortClientLib/Proc.hpp>

#include <patlib/task.hpp>
#include <patlib/path.hpp>

#include <boost/program_options.hpp>

#include <boost/interprocess/ipc/message_queue.hpp>

#include <csignal>

namespace PhotoRT_RenderProc
{
	using namespace std::chrono_literals;

	namespace po = boost::program_options;

	PRT_IMPORT_NS

	namespace paths = pl::paths;

	namespace rg = ranges;
	namespace rgv = rg::views;

	template <typename T_>
	[[nodiscard]] constexpr auto makePoValue(T_ &v)
		{ return po::value<T_>(&v); }

	struct ClientConfig
	{
		std::string id, modulePath, outputFile,
			msysId_default, msysId_primitive, msysId_contbuf, msysId_texture, msysId_film;
	};

	std::atomic<bool> needToEnd{false};
	PRTClientLib::ProcClient procClient;

	void signalHandler(int sig)
	{
		if(sig == SIGTERM || sig == SIGINT)
		{
			needToEnd = true;
			procClient.end();
		}
	}

	[[nodiscard]] int appMain(int argc, char **argv) noexcept
	try
	{
		ClientConfig config;
		boost::iostreams::stream<boost::iostreams::null_sink> nullStream( boost::iostreams::null_sink{} );
		//std::ofstream fileOutput;

		std::pmr::set_default_resource(&pl::default_resource());

		std::cin.imbue(std::locale::classic());
		std::cout.imbue(std::locale::classic());
		std::cerr.imbue(std::locale::classic());
		std::clog.imbue(std::locale::classic());

		std::signal(SIGTERM, signalHandler);
		std::signal(SIGINT, signalHandler);

		{
			po::options_description desc("General options");
			desc.add_options()
				("id", makePoValue(config.id), "Client ID.")
				("modules,m", makePoValue(config.modulePath)->default_value(PRTClientLib::getAppPluginDir().string()), "Path to PhotoRT modules.")
				//("output,o", makePoValue(config.outputFile), "Log output file.")
				//("shmid", makePoValue(config.shmId), "Shared memory ID.")
				("msysid_default", makePoValue(config.msysId_default))
				("msysid_primitive", makePoValue(config.msysId_primitive))
				("msysid_contbuf", makePoValue(config.msysId_contbuf))
				("msysid_texture", makePoValue(config.msysId_texture))
				("msysid_film", makePoValue(config.msysId_film))
			;

			po::variables_map vm;
			po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
			po::notify(vm);

			/*if(config.outputFile.empty())
				prt::logger.set_logger(pl::logtag::message, nullStream);
			else
			{
				fileOutput.open(config.outputFile, std::ios_base::out | std::ios_base::binary | std::ios_base::app);
				fileOutput.imbue(loc);

				fileOutput << std::defaultfloat << std::dec;

				prt::logger.set_logger(pl::logtag::message, fileOutput);
				prt::logger.set_logger(pl::logtag::error, fileOutput);

				std::cerr.rdbuf(fileOutput.rdbuf());
			}*/

			if(config.id.empty())
			{
				pl::log("Render proc", pl::logtag::error) << "No client ID given.";
				return EXIT_FAILURE;
			}
		}

		//prt::getMemorySystem().init(config.msysId, false);
		//pl::log("Render proc") << "Memory system ID: \"" << prt::getMemorySystem().instance_id() << "\".";
		prt::getMemoryOwnership() = false;

		boost::interprocess::message_queue messageQueue{boost::interprocess::open_only, (config.id + "-mq").c_str()};

		pl::VoidLogger<pl::logtag::message_t> logMessages;
		PRTClientLib::IpcMessageQueueLogger<pl::logtag::error_t> logError{messageQueue};

		pl::scoped_logger_guard mqGuard{logMessages, logError};

		//procClient.setTaskScheduler(&taskScheduler);
		procClient.setClientID(config.id);
		procClient.mClient.loadAllModules(config.modulePath);
		procClient.setOStream(*std::cout.rdbuf());
		procClient.setIStream(*std::cin.rdbuf());

		procClient.setParser(std::make_shared<prt::ParameterParserXerces>());

		procClient.mClient.loadConfig(std::cin);
		prt::getMemorySystem<>()->init(config.msysId_default, false);

		const auto initMemory =
			[&](const std::string &name, auto tag)
			{
				using Cat = pl::decay_t<decltype(tag)>;
				if(!prt::getMemorySystem<Cat>()->is_initialized())
					prt::getMemorySystem<Cat>()->init(name, false);
			};

		initMemory(config.msysId_primitive, prt::memtag::Primitive{});
		initMemory(config.msysId_contbuf, prt::memtag::ContBuffer{});
		initMemory(config.msysId_texture, prt::memtag::Texture{});
		initMemory(config.msysId_film, prt::memtag::Film{});

		procClient.mRenderDevice = procClient.mClient.mRender.createEntity(type_v<prt::IRenderDeviceCPU>, std::cin);
		procClient.mRenderDevice->priority(pl::EnumThreadPriority::Low);

		{
			pl::task::scoped_scheduler schedGuard{pl::tasks};
			//pl::tasks.start();

			if(!needToEnd)
				procClient.start();
			procClient.join();
		}

		return EXIT_SUCCESS;
	}
	catch(...)
	{
		PRTClientLib::logException(std::current_exception(), "render_proc");
		return EXIT_FAILURE;
	}
} // namespace PhotoRT_RenderProc

int main(int argc, char **argv)
{
	return PhotoRT_RenderProc::appMain(argc, argv);
}

