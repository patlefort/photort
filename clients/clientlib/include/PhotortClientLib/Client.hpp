/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Base.hpp"

#include <forward_list>

#include <boost/dll/shared_library.hpp>

#include <patlib/pointer.hpp>

#include <optional>

namespace PRTClientLib
{
	PRTCLIENTLIB_API extern std::string_view default_config;

	[[nodiscard]] PRTCLIENTLIB_API std::filesystem::path getAppPluginDir();

	PRTCLIENTLIB_API void printStats(std::ostream &os, const prt::Statistics &stats);

	[[nodiscard]] PRTCLIENTLIB_API prt::ParameterTree loadConfigFile(const std::filesystem::path &filename, prt::IParameterParser &mParser, std::string_view defaultConfig = {});

	PRTCLIENTLIB_API void logException(std::exception_ptr eptr, std::string_view module = "ClientLib", std::string_view message = {}) noexcept;

	template <typename OnErrorFc_ = pl::do_nothing_t>
	void try_(std::invocable auto&& fc, std::string_view module = "ClientLib", std::string_view message = {}, OnErrorFc_&& onErrorFc = {}) noexcept
	try
	{
		PL_FWD(fc)();
	}
	catch(...)
	{
		logException(std::current_exception(), module, message);
		PL_FWD(onErrorFc)(std::current_exception());
	}

	template <typename OnErrorFc_ = pl::do_nothing_t>
	void try_(std::invocable auto&& fc, std::string_view module, std::invocable auto&& message, OnErrorFc_&& onErrorFc = {}) noexcept
	try
	{
		PL_FWD(fc)();
	}
	catch(...)
	{
		logException(std::current_exception(), module, message());
		PL_FWD(onErrorFc)(std::current_exception());
	}

	class PRTCLIENTLIB_API Client
	{
	public:
		prt::Scene mScene;
		prt::EntityCollection mRender;

		prt::EntityPtr<prt::IRayTracerEngine> mRayTracerEngine;
		std::unique_ptr<prt::IRayTracer> mRayTracer;
		std::shared_ptr<prt::IParameterParser> mParser;

		prt::ParameterTree mConfig;

		std::forward_list<boost::dll::shared_library> mModules;

		Client();
		~Client();

		void loadModule(const std::filesystem::path &path);
		void loadAllModules(const std::filesystem::path &path);

		void setParser(const std::shared_ptr<prt::IParameterParser> &p) { mParser = mScene.mParser = mRender.mParser = p; }

		void loadConfig(const std::filesystem::path &filename);
		void loadConfig(std::istream &is);

		void openScene(const std::filesystem::path &path);
		void saveScene(const std::filesystem::path &path);

		void openScene(std::istream &is);

		void clear();

		void initForRendering();

	protected:
		void parseConfig();
		void initScene();
	};

	class PRTCLIENTLIB_API Render
	{
	public:
		Render(Client &c)
			: mClient{c} {}

		Client &mClient;

		prt::EntityPtr<prt::RenderDeviceController> mRenderDeviceController;
		prt::EntityPtr<prt::RenderContext> mRenderContext;

		void loadFromConfig(std::string_view devKey);
		void loadRenderParams(std::string_view paramKey);
		void initForRendering();
		void clear();
		void saveState(const prt::ParameterTree &params);
		[[nodiscard]] std::optional<prt::ParameterTree> loadState();

		void getStats(prt::Statistics &stats, const std::locale &loc) const;

	protected:

		void initForRendering2();

	};

	class IDrawDestination : public virtual prt::IBitmap2DWrite, public virtual prt::IBitmap1DWrite
	{
	public:
		virtual ~IDrawDestination() = default;

		virtual void setPixel(prt::Color color, szt offset) noexcept = 0;
		using IBitmap2DWrite::setPixel;
		using IBitmap1DWrite::zeroBitmap;
	};

	template <typename BitmapType_, typename ColorProfileType_>
	class DrawDestinationDithered final : public IDrawDestination, pl::copymove_initialize_only
	{
	public:
		BitmapType_ mBitmap;
		ColorProfileType_ mColorProfile;

		DrawDestinationDithered(BitmapType_ bitmap, ColorProfileType_ colorProfile) :
			mBitmap{bitmap}, mColorProfile{colorProfile} {}

		virtual void setPixel(prt::Color color, if32 x, if32 y) noexcept final
		{
			pl::to_ref(mBitmap).setPixel(convertColor(color, x, y), x, y);
		}

		virtual void setPixel(prt::Color color, szt offset) noexcept final
		{
			pl::to_ref(mBitmap).setPixel(convertColor(color, if32(offset % pl::to_ref(mBitmap).getWidth()), if32(offset / pl::to_ref(mBitmap).getWidth())), offset);
		}

		virtual void zeroBitmap() noexcept final
		{
			pl::to_ref(mBitmap).zeroBitmap();
		}

	protected:
		[[nodiscard]] prt::Color convertColor(prt::Color color, if32 x, if32 y) const noexcept
		{
			return prt::ditherOrdered<4>::dither(pl::to_ref(mColorProfile).colorInter()(color), prt::CReal(1.0 / 255.0), {x, y});
		}
	};
}
