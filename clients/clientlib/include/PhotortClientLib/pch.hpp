#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/logic/tribool_io.hpp>

#ifdef PRT_IPC
	#include <boost/interprocess/ipc/message_queue.hpp>

	#include <boost/iostreams/stream.hpp>
	#include <boost/iostreams/device/back_inserter.hpp>

	#include <boost/algorithm/string/replace.hpp>
#endif

#include <boost/dll/shared_library.hpp>
