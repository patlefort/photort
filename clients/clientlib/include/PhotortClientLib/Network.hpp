/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifdef PRT_NETWORKING

#include "Base.hpp"
#include "Client.hpp"

#include <PhotoRT/Device/Network.hpp>

#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/logic/tribool.hpp>
#include <boost/logic/tribool_io.hpp>

#include <patlib/chrono.hpp>
#include <patlib/thread.hpp>
#include <patlib/task.hpp>
#include <patlib/rectangle.hpp>

#include <optional>

namespace PRTClientLib
{
	class PRTCLIENTLIB_API NetworkClient : public prt::NetworkedDevice, public pl::ThreadBase, public virtual prt::IRenderDevice
	{
	public:

			class PRTCLIENTLIB_API ModuleException : public prt::Exception
			{
			public:
				ModuleException(std::string_view message, const boost::system::error_code &er, const std::experimental::source_location &sl = std::experimental::source_location::current()) noexcept;

				[[nodiscard]] virtual const char *module() const noexcept override;
				[[nodiscard]] const boost::system::error_code &getErrorCode() const noexcept { return er; }

			protected:
				boost::system::error_code er;
			};

		NetworkClient(Client &c);

		Client &mClient;
		Render mRender;

		virtual void setRenderContext(const prt::EntityPtr<prt::RenderContext> &context) override;
		virtual void notifyEntityUpdate(prt::IRenderEntity &entity) override;
		virtual void init(prt::Scene &scene) override;

		[[nodiscard]] virtual bool isReady() const override { return mRender.mRenderDeviceController && mRender.mRenderDeviceController->isReady(); }

		[[nodiscard]] virtual std::string_view getDeviceName() const override;

		virtual void dumpInfo(prt::Scene &scene, const std::filesystem::path &folder, const std::string &name) override;

		virtual void lock_execution() override
		{
			pl::ThreadBase::lock_execution();

			try
			{
				mRender.mRenderDeviceController->lock_execution();
			}
			catch(...)
			{
				pl::ThreadBase::unlock_execution();

				throw;
			}
		}

		virtual void unlock_execution() override
		{
			pl::ThreadBase::unlock_execution();
			mRender.mRenderDeviceController->unlock_execution();
		}

		virtual void on_start() override;
		//virtual void pause();
		//virtual void restart();
		virtual void end() override;

		virtual void on_pause() override;
		virtual void on_pause_end() override;

		void setAddress(std::string_view addr) { mAddress = addr; }
		[[nodiscard]] std::string_view getAddress() const noexcept { return mAddress; }

		void setBinary(boost::tribool v) noexcept { mIoBinary = v; }
		[[nodiscard]] boost::tribool getBinary() const noexcept { return mIoBinary; }

		void setCompressor(std::optional<std::string> v) noexcept { mIoCompressor = v; }
		[[nodiscard]] const std::optional<std::string> &getCompressor() const noexcept { return mIoCompressor; }

		virtual void getStats(prt::Statistics &stats, const std::locale &loc) const override;

	protected:

		pl::rectangle<> mRenderRect{};
		std::string mRenderFilmID;

		boost::tribool mIoBinary{boost::indeterminate}, mIoCompress{boost::indeterminate};
		std::optional<std::string> mIoCompressor;

		prt::ParameterTree finalOptions;

		std::shared_ptr<boost::asio::ip::tcp::iostream> mStream;
		std::string mAddress;
		pl::stop_watch<> mRenderTime;

		std::string mRenderGUID;

		virtual void on_end() override;
		virtual bool do_iteration() override;

		virtual void tryConnect();

		virtual void startNewRender();
	};
}

#endif
