/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifdef PRT_IPC

#include "Base.hpp"
#include "Client.hpp"

#include <PhotoRT/Device/ProcServer.hpp>

#include <patlib/chrono.hpp>
#include <patlib/thread.hpp>
#include <patlib/task.hpp>
#include <patlib/path.hpp>
#include <patlib/rectangle.hpp>

#include <boost/interprocess/ipc/message_queue.hpp>

#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/back_inserter.hpp>

#include <boost/algorithm/string/replace.hpp>

namespace PRTClientLib
{
	class CommandFilter : public prt::IProcServerCommandFilter
	{
	public:
		virtual void filterCommand(std::string &command)
		{
			boost::algorithm::replace_all(command, "{$app_dir}", pl::paths::app_dir().string());
		}
	};

	template <typename Category_>
	class IpcMessageQueueLogger : public pl::ILogger<Category_>
	{
	private:
		using DevType = boost::iostreams::back_insert_device<std::string>;

		std::string mMessage;
		boost::iostreams::stream<DevType> mSs{DevType{mMessage}, 0, 0};

	public:

		IpcMessageQueueLogger(boost::interprocess::message_queue &mq)
			: mMessageQueue{mq}
		{
			try { mSs.imbue(std::locale{""}); } catch(...) {}
		}

		boost::interprocess::message_queue &mMessageQueue;

		virtual std::ostream &log_start(std::string_view module, std::ostream &) override
		{
			if constexpr(std::same_as<Category_, pl::logtag::error_t>)
				mSs << "1\n";
			else
				mSs << "0\n";

			return mSs << module << '\n';
		}

		virtual void log_end(std::ostream &) override
		{
			mMessageQueue.try_send(mMessage.data(), mMessage.size() * sizeof(mMessage[0]), 0);
			mMessage.clear();
		}
	};

	class PRTCLIENTLIB_API ProcClient : public virtual prt::IRenderDevice, public prt::ClientDevice, public pl::ThreadBase
	{
	public:

			class PRTCLIENTLIB_API ModuleException : public prt::Exception
			{
			public:

				[[nodiscard]] virtual const char *module() const noexcept override;

			};

		ProcClient();

		using pl::ThreadBase::process_loop;

		prt::EntityPtr<prt::IRenderDeviceCPU> mRenderDevice;
		Client mClient;
		prt::EntityPtr<prt::RenderContext> mRenderContext;

		virtual void setParser(const std::shared_ptr<prt::IParameterParser> &p) { mClient.setParser(p); }

		virtual void setRenderContext(const prt::EntityPtr<prt::RenderContext> &context) override;
		virtual void notifyEntityUpdate(prt::IRenderEntity &entity) override;
		virtual void init(prt::Scene &scene) override;

		[[nodiscard]] virtual bool isReady() const override { return mRenderDevice->isReady(); }

		[[nodiscard]] virtual std::string_view getDeviceName() const override;

		virtual void dumpInfo(prt::Scene &scene, const std::filesystem::path &folder, const std::string &name) override;

		virtual void lock_execution() override
		{
			pl::ThreadBase::lock_execution();

			try
			{
				mRenderDevice->lock_execution();
			}
			catch(...)
			{
				pl::ThreadBase::unlock_execution();

				throw;
			}
		}

		virtual void unlock_execution() override
		{
			pl::ThreadBase::unlock_execution();
			mRenderDevice->unlock_execution();
		}

		virtual void on_start() override;
		//virtual void pause();
		//virtual void restart();
		virtual void end() override;

		virtual void on_pause() override;
		virtual void on_pause_end() override;

		virtual void getStats(prt::Statistics &stats, const std::locale &loc) const override;

		void setOStream(std::basic_streambuf<std::ostream::char_type, std::ostream::traits_type> &buf) { mOs.rdbuf(&buf); }
		void setIStream(std::basic_streambuf<std::istream::char_type, std::istream::traits_type> &buf) { mIs.rdbuf(&buf); }

		void setClientID(std::string id) { mClientID = std::move(id); }

	protected:

		pl::rectangle<> mRenderRect{};
		std::string mRenderFilmID, mClientID;

		boost::iostreams::stream_buffer<boost::iostreams::null_sink> mNullStreamBuf{boost::iostreams::null_sink{}};

		std::ostream mOs{&mNullStreamBuf};
		std::istream mIs{&mNullStreamBuf};

		pl::stop_watch<> mRenderTime;

		virtual void on_end() override;
		virtual void process_loop() override;

		virtual void startNewRender();
	};
}

#endif