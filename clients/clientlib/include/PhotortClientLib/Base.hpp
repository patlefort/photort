/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/Scene.hpp>
#include <PhotoRT/Core/RenderDevices.hpp>
#include <PhotoRT/Core/RayTracer.hpp>
#include <PhotoRT/Core/Parser.hpp>
#include <PhotoRT/Core/Bitmap.hpp>
#include <PhotoRT/Core/Film.hpp>

#ifdef PhotoRT_ClientLib_SHARED
	#ifdef PhotoRT_ClientLib_EXPORTS
		#define PRTCLIENTLIB_API BOOST_SYMBOL_EXPORT
	#else
		#define PRTCLIENTLIB_API BOOST_SYMBOL_IMPORT
	#endif
#else
	#define PRTCLIENTLIB_API
#endif

namespace PRTClientLib
{
	using namespace std::chrono_literals;

	namespace rg = ranges;
	namespace rgv = rg::views;

	PRT_IMPORT_NS

	class PRTCLIENTLIB_API ClientLibException : public prt::Exception
	{
	public:
		using prt::Exception::Exception;

		[[nodiscard]] virtual const char *module() const noexcept override
		{
			return "ClientLib";
		}
	};
}
