/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef PRT_NETWORKING

#include <PhotortClientLib/Network.hpp>

#include <patlib/endian.hpp>
#include <patlib/io.hpp>

#include <PhotoRT/Core/Compression.hpp>

#include <boost/iostreams/filtering_stream.hpp>

#include <fmt/format.h>
#include <fmt/chrono.h>

namespace PRTClientLib
{
	namespace bio = boost::asio;

	NetworkClient::ModuleException::ModuleException(std::string_view message, const boost::system::error_code &er_, const std::experimental::source_location &sl) noexcept
		: prt::Exception(message, sl), er{er_}
	{}

	const char *NetworkClient::ModuleException::module() const noexcept { return "ClientLib"; }

	NetworkClient::NetworkClient(Client &c)
		: mClient{c}, mRender{mClient}
	{
		mCommandHandlers.insert({
			{"entity_update",
				[this] (std::string_view /*command*/)
				{
					if(!mRender.mRenderDeviceController)
						return;

					const auto params = mClient.mScene.mParser->load(*mStream);

					for(auto &param : params)
					{
						if(param.second.childParams)
						{
							auto &entity = mClient.mScene[param.second.value];
							entity.setParameters(*param.second.childParams);
							//entity.init(scene);

							mRender.mRenderDeviceController->notifyEntityUpdate(entity);
						}
					}
				}
			},

			{"get_buffers",
				[this] (std::string_view /*command*/)
				{
					if(!mRender.mRenderDeviceController)
						return;

					pl::log("ClientLib") << "Sending buffers.";

					for(auto &film : mRender.mRenderDeviceController->mRenderContext->mFilms)
					{
						for(auto &ag : film.second.mAggregators)
						{
							*mStream << film.first << ' ' << ag.first << '\n';

							ag.second.compile();

							prt::selectCompressor(finalOptions["compressor"].value, [&](auto&& compressor){
								pl::io::write_stream(*mStream, [&](auto& os){ os << *ag.second.mFinalBuffer; }, PL_FWD(compressor));
							});

							//*mStream << *ag.second.mFinalBuffer;
							ag.second.mFinalBuffer->zeroBuffers();
						}
					}

					*mStream << std::endl;

					pl::log("ClientLib") << "Done sending buffers.";
				}
			},

			{"get_cb_types",
				[this] (std::string_view /*command*/)
				{
					if(mRender.mRenderDeviceController)
					{
						for(auto &film : mRender.mRenderDeviceController->mRenderContext->mFilms)
						{
							for(auto &ag : film.second.mAggregators)
								*mStream << film.first << ' ' << ag.first << '\n';
						}
					}

					*mStream << "done." << std::endl;
				}
			},

			{"start",
				[this] (std::string_view /*command*/)
				{
					if(!mRender.mRenderDeviceController)
						return;

					if(!mRender.mRenderDeviceController->mRenderContext)
						return;

					if(!mRender.mRenderDeviceController->started())
					{
						pl::log("ClientLib") << "Starting render.";
						startNewRender();
					}
					else if(mRender.mRenderDeviceController->paused())
						mRenderTime.unpause();
						//renderDeviceController->mRenderContext->zeroBuffers();

					mRender.mRenderDeviceController->start();
				}
			},

			{"pause",
				[this] (std::string_view /*command*/)
				{
					if(!mRender.mRenderDeviceController)
						return;

					if(!mRender.mRenderDeviceController->paused())
						mRenderTime.pause();

					mRender.mRenderDeviceController->pause();
				}
			},

			{"restart",
				[this] (std::string_view /*command*/)
				{
					if(!mRender.mRenderDeviceController)
						return;

					if(mRender.mRenderDeviceController->started())
					{
						pl::log("ClientLib") << "Restarting render.";

						prt::RenderDeviceExecutionLockGuard exeg{*mRender.mRenderDeviceController};
						mRender.mRenderDeviceController->restart();
						startNewRender();
					}
				}
			},

			{"end",
				[this] (std::string_view /*command*/)
				{
					if(!mRender.mRenderDeviceController)
						return;

					pl::log("ClientLib") << "Ending render.";

					mRender.mRenderDeviceController->end();

					mStream->close();
				}
			},

			{"set_render_rect",
				[this] (std::string_view /*command*/)
				{
					if(!mRender.mRenderDeviceController)
						return;

					mRenderFilmID = pl::io::readline_trim(*mStream, prt::Parameter::loc);
					*mStream >> mRenderRect;

					mRender.mRenderDeviceController->setRenderRect(mRenderFilmID, mRenderRect);

					if(!mRenderFilmID.empty())
						pl::log("ClientLib", "Setting render rectangle as {} on film {}.",
							pl::io::value_formatter{mRenderRect}, pl::io::quoted{mRenderFilmID});
					else
						pl::log("ClientLib") << "Unselecting render rectangle.";
				}
			},

			{"scene_data",
				[this] (std::string_view /*command*/)
				{
					auto renderRectParams = mClient.mScene.mParser->load(*mStream);

					const auto &newGUID = renderRectParams["render_guid"].value;
					mRenderFilmID = renderRectParams["film"].value;
					mRenderRect = renderRectParams["rect"];

					pl::log("ClientLib") << "Render GUID: " << newGUID;
					pl::log("ClientLib") << "Using render parameter: " << renderRectParams["param_id"].value;
					pl::log("ClientLib") << "Film: " << mRenderFilmID;
					pl::log("ClientLib") << "rectangle: " << mRenderRect;

					if(mRenderGUID != newGUID)
					{
						*mStream << "need_data" << std::endl;

						bool act = false;

						if(mRender.mRenderDeviceController)
						{
							act = mRender.mRenderDeviceController->started();
							mRender.mRenderDeviceController->end();
							mRender.mRenderDeviceController->join();
						}

						mRenderGUID = newGUID;

						mClient.mScene.clearScene();

						auto dsn = std::make_shared<prt::DataSourceStream>();
						mClient.mScene.mDataSource = dsn;

						//dsn->setIStream(filterStream);
						dsn->setCompressor(finalOptions["compressor"].value);

						dsn->setIStream(*mStream);
						dsn->setOStream(*mStream);

						mClient.openScene(*mStream);

						mClient.mScene.mDataSource = nullptr;

						*mStream << "done." << std::endl;

						mRender.loadRenderParams(renderRectParams["param_id"].value);
						mRender.initForRendering();

						mRender.mRenderDeviceController->setRenderRect(mRenderFilmID, mRenderRect);

						pl::tasks.push([&]
						{
							mClient.initForRendering();
							mClient.mScene.updateCameras();
						}).future.get();

						if(act)
						{
							startNewRender();
							mRender.mRenderDeviceController->start();
						}
					}
					else
					{
						*mStream << "dont_need_data" << std::endl;

						mRender.mRenderDeviceController->setRenderRect(mRenderFilmID, mRenderRect);
					}

					/*std::ofstream filest("/tmp/render_params.xml");

					renderContext->renderContext >> filest;

					boost::filesystem::create_directory("/tmp/testrender");

					scene.saveToDirectory("/tmp/testrender/");*/
				}
			}
		});
	}

	std::string_view NetworkClient::getDeviceName() const { return {}; }

	void NetworkClient::setRenderContext(const prt::EntityPtr<prt::RenderContext> &context)
	{
		mRender.mRenderDeviceController->setRenderContext(context);
	}

	void NetworkClient::notifyEntityUpdate(prt::IRenderEntity &entity)
	{
		mRender.mRenderDeviceController->notifyEntityUpdate(entity);
	}

	void NetworkClient::init(prt::Scene &scene)
	{
		mRender.mRenderDeviceController->init(scene);
	}

	void NetworkClient::dumpInfo(prt::Scene &, const std::filesystem::path &/*folder*/, const std::string &/*name*/)
	{

	}

	void NetworkClient::startNewRender()
	{
		mRender.mRenderContext->zeroBuffers();
		mRenderTime.start();
	}

	void NetworkClient::on_start()
	{
		tryConnect();

		ThreadBase::on_start();
	}

	/*void NetworkClient::pause()
	{
		renderDeviceController->pause();
	}

	void NetworkClient::restart()
	{
		renderDeviceController->restart();
	}*/

	void NetworkClient::on_pause()
	{
		//statsTimer.pause();
	}

	void NetworkClient::on_pause_end()
	{
		//statsTimer.unpause();
	}

	void NetworkClient::end()
	{
		ThreadBase::end();

		if(mRender.mRenderDeviceController)
			mRender.mRenderDeviceController->end();
	}

	void NetworkClient::on_end()
	{
		if(mStream)
			mStream->close();

		if(mRender.mRenderDeviceController)
			mRender.mRenderDeviceController->join();

		ThreadBase::on_end();
	}

	bool NetworkClient::do_iteration()
	{
		if(!mStream->socket().is_open())
		{
			tryConnect();
			sleep_for(3s);

			return true;
		}

		std::array<char, 100> line;
		mStream->getline(line.data(), line.size());

		auto er = mStream->error();

		if(er == boost::asio::error::operation_aborted)
		{
			mStream->clear();
		}
		else if(er)
		{
			pl::log("ClientLib", pl::logtag::error, "Disconnected from server: {}.", er.message());
			tryConnect();

			sleep_for(3s);

			return true;
		}

		if(mStream->eof())
		{
			pl::log("ClientLib", pl::logtag::error) << "mStream->eof() returned true.";
			sleep_for(3s);
			tryConnect();

			return true;
		}

		if(mStream->bad())
		{
			pl::log("ClientLib", pl::logtag::error) << "mStream->bad() returned true.";
			sleep_for(3s);
			tryConnect();

			return true;
		}

		mStream->clear();

		std::string_view command(line.data(), mStream->gcount() - 1);

		if(!command.empty() && command.starts_with("$cmd: "))
		{
			command.remove_prefix(6);

			pl::log("ClientLib") << "Command: " << command;

			PRTClientLib::try_([&]
			{
				if(!handleCommand(command))
					pl::log("ClientLib", pl::logtag::error, "Invalid command {}.", command);
			}, "Exception thrown by command handler: ");
		}

		sleep_for(100ms);

		return true;
	}

	void NetworkClient::getStats(prt::Statistics &stats, const std::locale &loc) const
	{
		if(!mRender.mRenderDeviceController || !mRender.mRenderDeviceController->running())
			return;

		mRender.getStats(stats, loc);

		stats["render-time"] = fmt::format(loc, "{:L}:{:%M:%S}",
			std::chrono::duration_cast<std::chrono::hours>(mRenderTime.total_running_time()).count(), std::chrono::duration_cast<std::chrono::seconds>(mRenderTime.total_running_time()));
	}

	void NetworkClient::tryConnect()
	{
		pl::log("ClientLib", "Trying to connect to {}:{}...", mAddress, mPort);

		/*try
		{
			mStream = std::make_shared<bio::ip::tcp::iostream>(bio::ip::tcp::v6(), mAddress, mPort, bio::ip::resolver_query_base::numeric_service);
		}
		catch(...)
		{*/
			mStream = std::make_shared<bio::ip::tcp::iostream>(mAddress, mPort, bio::ip::resolver_query_base::numeric_service);
		//}

		if(!mStream->error())
		{
			//setSocketOptions(mStream.rdbuf(), 10000);
			mStream->socket().set_option(bio::socket_base::send_buffer_size(1024 * 1024 * 1024));
			mStream->socket().set_option(bio::socket_base::receive_buffer_size(1024 * 1024 * 1024));

			//init();

			mStream->imbue(prt::Parameter::loc);
			mStream->exceptions(pl::io::failure_bits);

			prt::ParameterTree clientOptions{
				{"binary", {getBinary()}},
				{"double", {prt::useDouble}},
				{"64_bits_index", {prt::use64BitsIndex}}
			};

			if(mIoCompressor)
				clientOptions["compressor"] = *mIoCompressor;

			mClient.mScene.mParser->save(clientOptions, *mStream);

			finalOptions = mClient.mScene.mParser->load(*mStream);

			std::endian endian;

			const auto endianStr = finalOptions.findValue("endian").value_or("");

			if(endianStr == "little")
				endian = std::endian::little;
			else if(endianStr == "big")
				endian = std::endian::big;
			else
				endian = std::endian::native;

			switch(endian)
			{
			case std::endian::big:
				*mStream << pl::io::endian_big;
				*mStream >> pl::io::endian_big;
				break;
			case std::endian::little:
				*mStream << pl::io::endian_little;
				*mStream >> pl::io::endian_little;
				break;
			default:
				*mStream << pl::io::endian_native;
				*mStream >> pl::io::endian_native;
			};

			if(finalOptions.findValue("binary", type_v<bool>).value_or(false))
			{
				pl::log("ClientLib") << "Using binary serialization.";
				*mStream << pl::io::binary;
				*mStream >> pl::io::binary;
			}
			else
			{
				pl::log("ClientLib") << "Using text serialization.";
				*mStream << pl::io::text;
				*mStream >> pl::io::text;
			}

			if(const auto p = finalOptions.findValue("compressor"); p && !p->empty())
			{
				pl::log("ClientLib", "Using {} compression.", *p);
			}
			else
			{
				pl::log("ClientLib") << "Using no compression.";
			}

			pl::log("ClientLib") << "Connected.";
		}
		else
			pl::log("ClientLib", pl::logtag::error, "Unable to connect: {}.", mStream->error().message());
	}
}

#endif
