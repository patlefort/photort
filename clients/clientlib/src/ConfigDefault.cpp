/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string_view>

namespace PRTClientLib
{
std::string_view default_config =
R"(<?xml version="1.0" encoding="UTF-8"?>
<render xmlns="http://photort.org/params/">

	<!-- Entity importers and exporters config -->
	<import_export_config array="1">
	</import_export_config>

	<!-- Reusable entities for renderers -->
	<entities array="1">

		<!--
			Primitive IDs:
			1: Triangle
			2: Sphere
			3: Plane
			4: Disk
		-->

		<e type="raytracer-cpu" id="main_raytracer">

			<!-- Best acceleration structure type defined by the implementation: container_factory-<primitive type>_best -->
			<accel_struct array="1">
				<e pid="1">
					<!-- No acceleration structure types: triangle_generic, triangle_generic_cached, triangle_prepacked -->

					<!-- KD-tree types: triangle_kdtree_generic, triangle_kdtree_cached -->

					<!-- BVH types: triangle_bvh_generic, triangle_bvh_cached -->

					<factory type="container_factory-triangle_best">
						<!--<params
							max_depth="100"
							target_cost="80"
							sah_at_depth="0"
							nb_sah_tests="100"
							optimize_tree="1"
						/>-->
					</factory>
				</e>

				<e pid="2">
					<!-- No acceleration structure types: sphere_generic, sphere_generic_cached, sphere_prepacked -->

					<!-- KD-tree types: sphere_kdtree_generic, sphere_kdtree_cached -->

					<!-- BVH types: sphere_bvh_generic, sphere_bvh_cached -->
					<factory type="container_factory-sphere_best" />
				</e>

				<e pid="3">
					<factory type="container_factory-plane_best" />
				</e>

				<e pid="4">
					<!-- No acceleration structure types: disk_generic, disk_generic_cached, disk_prepacked -->

					<!-- BVH types: disk_bvh_generic, disk_bvh_cached -->
					<factory type="container_factory-disk_best" />
				</e>
			</accel_struct>
		</e>

	</entities>

	<!-- Render devices for final renders -->
	<devices array="1">

		<e type="device-cpu" id="d_cpu" enable="1">
			<devices array="1">
				<e type="device-tiled" id="d_cpu_pixel">
					<raytracer type="raytracer" link="main_raytracer" />

					<use_pixel_buffer />

					<pixel_sampler_params type="pixelsampler-variance" weight_cap="10" nb_calculate_weight_cap="50" />

					<nb_threads>-1</nb_threads>
					<work_list_size>128</work_list_size>
					<max_samples>0</max_samples>
				</e>

				<e type="device-tiled" id="d_cpu_pixel_light">
					<raytracer type="raytracer" link="main_raytracer" />

					<use_pixel_buffer />
					<use_light_buffer />

					<pixel_sampler_params type="pixelsampler-variance" weight_cap="10" nb_calculate_weight_cap="50" />

					<nb_threads>-1</nb_threads>
					<work_list_size>128</work_list_size>
					<max_samples>0</max_samples>
				</e>

				<e type="device-light_trace" id="d_cpu_light">
					<raytracer type="raytracer" link="main_raytracer" />

					<nb_threads>-1</nb_threads>
					<work_list_size>128</work_list_size>
					<max_samples>0</max_samples>
				</e>
			</devices>
		</e>

		<e type="device-procserver" id="d_procserver" enable="0">
			<clients array="1">
				<e>photort-render_proc</e>
			</clients>

			<config>
				<entities array="1">
					<e type="raytracer-cpu" id="main_raytracer">
						<accel_struct array="1">
							<e pid="1"><factory type="container_factory-triangle_best" /></e>
							<e pid="2"><factory type="container_factory-sphere_best" /></e>
							<e pid="3"><factory type="container_factory-plane_best" /></e>
							<e pid="4"><factory type="container_factory-disk_best" /></e>
						</accel_struct>
					</e>
				</entities>
			</config>

			<device_params type="device-cpu" id="d_cpu">
				<devices array="1">
					<e type="device-tiled" id="d_cpu_pixel">
						<raytracer type="raytracer" link="main_raytracer" />

						<use_pixel_buffer />

						<pixel_sampler_params type="pixelsampler-variance" weight_cap="10" nb_calculate_weight_cap="50" />

						<nb_threads>-1</nb_threads>
						<work_list_size>128</work_list_size>
						<max_samples>0</max_samples>
					</e>

					<e type="device-tiled" id="d_cpu_pixel_light">
						<raytracer type="raytracer" link="main_raytracer" />

						<use_pixel_buffer />
						<use_light_buffer />

						<pixel_sampler_params type="pixelsampler-variance" weight_cap="10" nb_calculate_weight_cap="50" />

						<nb_threads>-1</nb_threads>
						<work_list_size>128</work_list_size>
						<max_samples>0</max_samples>
					</e>

					<e type="device-light_trace" id="d_cpu_light">
						<raytracer type="raytracer" link="main_raytracer" />

						<nb_threads>-1</nb_threads>
						<work_list_size>128</work_list_size>
						<max_samples>0</max_samples>
					</e>
				</devices>
			</device_params>
		</e>

		<e type="device-networkserver" id="d_networkserver" enable="0" max_nbclients="10" />

	</devices>

	<!-- Render devices for preview renders -->
	<preview_devices array="1">

		<e type="device-cpu" id="dp_cpu" enable="1">
			<devices array="1">
				<e type="device-tiled" id="dp_cpu_pixel">
					<raytracer type="raytracer" link="main_raytracer" />

					<use_pixel_buffer />

					<pixel_sampler_params type="pixelsampler-variance" weight_cap="4" nb_calculate_weight_cap="20" />

					<nb_threads>-1</nb_threads>
					<work_list_size>24</work_list_size>
					<max_samples>0</max_samples>
				</e>

				<e type="device-tiled" id="dp_cpu_pixel_light">
					<raytracer type="raytracer" link="main_raytracer" />

					<use_pixel_buffer />
					<use_light_buffer />

					<pixel_sampler_params type="pixelsampler-variance" weight_cap="4" nb_calculate_weight_cap="20" />

					<nb_threads>-1</nb_threads>
					<work_list_size>24</work_list_size>
					<max_samples>0</max_samples>
				</e>

				<e type="device-light_trace" id="dp_cpu_light">
					<raytracer type="raytracer" link="main_raytracer" />

					<nb_threads>-1</nb_threads>
					<work_list_size>24</work_list_size>
					<max_samples>0</max_samples>
				</e>
			</devices>
		</e>

	</preview_devices>
</render>
)";
}
