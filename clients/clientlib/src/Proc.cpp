/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef PRT_IPC

#include <PhotortClientLib/Proc.hpp>

#include <fmt/format.h>
#include <fmt/chrono.h>

namespace PRTClientLib
{
	const char *ProcClient::ModuleException::module() const noexcept
		{ return "ClientLib"; }

	ProcClient::ProcClient()
	{
		mIs.exceptions(~std::ios_base::goodbit);
		mOs.exceptions(~std::ios_base::goodbit);

		mCommandHandlers.insert({
			{"isdone",
				[this] (std::string_view /*command*/)
				{
					mOs << '0' << std::flush;
				}
			},

			{"stats",
				[this] (std::string_view /*command*/)
				{
					prt::Statistics stats;
					getStats(stats, pl::io::try_get_locale(""));

					prt::ParameterTree params;

					for(auto&& [i, s] : rgv::enumerate(stats))
					{
						params[i]["n"] = s.first;
						params[i]["v"] = s.second;
					}

					//std::ofstream of{"/tmp/stats.txt", std::ios_base::binary | std::ios_base::out};
					//params.print(of);
					//mClient.mScene.mParser->save(params, "/tmp/stats.txt");
					mClient.mScene.mParser->save(params, mOs);
				}
			},

			{"entity_update",
				[this] (std::string_view /*command*/)
				{
					const auto params = mClient.mScene.mParser->load(mIs);

					for(const auto &param : params)
					{
						if(param.second.childParams)
						{
							auto &entity = mClient.mScene[param.second.value];
							entity.setParameters(*param.second.childParams);

							mRenderDevice->notifyEntityUpdate(entity);
						}
					}
				}
			},

			{"get_cb_types",
				[this] (std::string_view /*command*/)
				{
					for(const auto &film : mRenderContext->mFilms)
						for(const auto &ag : film.second.mAggregators)
							for(const auto &buf : ag.second.mBuffers)
							{
								mOs << film.first << ' ' << ag.first << ' ' << buf->getEntityId() << '\n';
								pl::log("ClientLib", "{} {} {} / {}", film.first, ag.first, buf->getEntityId(), buf->getRenderRect());
							}

					mOs << "done." << std::endl;
				}
			},

			{"start",
				[this] (std::string_view /*command*/)
				{
					if(!mRenderContext)
						return;

					if(!mRenderDevice->started())
					{
						pl::log("ClientLib") << "Starting render.";
						startNewRender();
					}
					else if(mRenderDevice->paused())
						mRenderTime.unpause();
						//renderDeviceController->mRenderContext->zeroBuffers();

					mRenderDevice->start();
				}
			},

			{"pause",
				[this] (std::string_view /*command*/)
				{
					if(!mRenderDevice->paused())
						mRenderTime.pause();

					mRenderDevice->pause();
				}
			},

			{"restart",
				[this] (std::string_view /*command*/)
				{
					if(mRenderDevice->started())
					{
						pl::log("ClientLib") << "Restarting render.";

						prt::RenderDeviceExecutionLockGuard exeg{*mRenderDevice};
						mRenderDevice->restart();
						startNewRender();
					}
				}
			},

			{"end",
				[this] (std::string_view /*command*/)
				{
					pl::log("ClientLib") << "Ending render.";

					mRenderDevice->end();
					mRenderDevice->join();

					mClient.mScene.clearScene();
				}
			},

			{"terminate",
				[this] (std::string_view /*command*/)
				{
					pl::log("ClientLib") << "Terminating process.";

					mRenderDevice->end();
					mRenderDevice->join();

					mClient.mScene.clearScene();

					end();
				}
			},

			{"set_render_rect",
				[this] (std::string_view /*command*/)
				{
					mRenderFilmID = pl::io::readline_trim(mIs, prt::Parameter::loc);
					mIs >> mRenderRect;

					mRenderDevice->setRenderRect(mRenderFilmID, mRenderRect);

					if(!mRenderFilmID.empty())
						pl::log("ClientLib", "Setting render rectangle as {} on film {}.",
							pl::io::value_formatter{mRenderRect}, pl::io::quoted{mRenderFilmID});
					else
						pl::log("ClientLib") << "Unselecting render rectangle.";
				}
			},

			{"scene_data",
				[this] (std::string_view /*command*/)
				{
					bool act = false;

					act = mRenderDevice->started();

					mRenderDevice->end();
					mRenderDevice->join();

					mClient.mScene.clearScene();

					auto renderRectParams = mClient.mScene.mParser->load(mIs);

					mRenderFilmID = renderRectParams["film"].value;
					mRenderRect = renderRectParams["rect"];
					mClient.mScene.setShmID(renderRectParams["shmid"].value);

					pl::log("ClientLib") << "Using render parameter: " << renderRectParams["param_id"].value;
					pl::log("ClientLib") << "Film: " << mRenderFilmID;
					pl::log("ClientLib") << "rectangle: " << mRenderRect;
					pl::log("ClientLib") << "Scene shared memory ID: " << mClient.mScene.getShmID();

					auto dsn = std::make_shared<prt::DataSourceStream>();
					mClient.mScene.mDataSource = dsn;

					mIs >> pl::io::binary >> pl::io::ipc_on;
					mOs << pl::io::binary << pl::io::ipc_on;

					dsn->setIStream(mIs);
					dsn->setOStream(mOs);

					mClient.openScene(mIs);

					mClient.mScene.mDataSource = nullptr;

					mOs << "done." << std::endl;

					mRenderContext = mClient.mScene.getEntitySptr(renderRectParams["param_id"].value, type_v<prt::RenderContext>);

					if(!mRenderContext)
						throw ClientLibException("Render parameter \"" + renderRectParams["param_id"].value + "\" not found.");

					mRenderDevice->setRenderContext(mRenderContext);

					//mRenderContext->initFilmBuffers();
					mRenderContext->initFilters();
					mRenderContext->setNbLayers(mClient.mScene.getLightGroups().size() + 1);

					mRenderDevice->init(mClient.mScene);

					mRenderContext->deleteBuffers();
					for(auto &contribBuffer : mRenderDevice->getContributionBuffers())
					{
						//pl::log("ClientLib") << "Registered buffer: " << contribBuffer->getRenderRect();
						mRenderContext->registerBuffer(contribBuffer);
					}
					//mRenderContext->initBuffers();

					mRenderContext->initCamera();

					mRenderDevice->setRenderRect(mRenderFilmID, mRenderRect);

					//mClient.saveScene("/tmp/testsave");

					pl::tasks.push([&]
					{
						mClient.initForRendering();
						mClient.mScene.updateCameras();
					}).future.get();

					if(act)
					{
						startNewRender();
						mRenderDevice->start();
					}

					pl::log("ClientLib") << "scene_data done.";

				}
			}
		});
	}

	std::string_view ProcClient::getDeviceName() const { return {}; }

	void ProcClient::setRenderContext(const prt::EntityPtr<prt::RenderContext> &context)
	{
		mRenderContext = context;
		mRenderDevice->setRenderContext(context);
	}

	void ProcClient::notifyEntityUpdate(prt::IRenderEntity &entity)
	{
		mRenderDevice->notifyEntityUpdate(entity);
	}

	void ProcClient::init(prt::Scene &scene)
	{
		mRenderDevice->init(scene);
	}

	void ProcClient::dumpInfo(prt::Scene &, const std::filesystem::path &/*folder*/, const std::string &/*name*/)
	{

	}

	void ProcClient::startNewRender()
	{
		mRenderContext->zeroBuffers();
		mRenderTime.start();
	}

	void ProcClient::on_start()
	{
		pl::log("ClientLib") << "ProcClient started.";

		ThreadBase::on_start();
	}

	void ProcClient::on_pause()
	{
		//statsTimer.pause();
	}

	void ProcClient::on_pause_end()
	{
		//statsTimer.unpause();
	}

	void ProcClient::end()
	{
		ThreadBase::end();

		mRenderDevice->end();
	}

	void ProcClient::on_end()
	{
		mRenderDevice->join();

		pl::log("ClientLib") << "ProcClient ended.";

		ThreadBase::on_end();
	}

	void ProcClient::process_loop()
	{
		work_loop([this]{

			//pl::log("ClientLib") << "Reading line.";

			std::array<char, 100> line;
			mIs.getline(line.data(), line.size());

			std::string_view command(line.data());

			//pl::log("ClientLib") << "Line: " << command;

			if(!command.empty() && command.starts_with("$cmd: "))
			{
				command.remove_prefix(6);

				pl::log("ClientLib") << "Command: " << command;

				PRTClientLib::try_([&]
				{
					if(!handleCommand(command))
						pl::log("ClientLib", pl::logtag::error, "Invalid command {}.", command);
				}, "Exception thrown by command handler: ");
			}

			//if(renderDeviceController->started() && statsTimer.check())
				//printStats();

			sleep_for(100ms);

			return true;
		});
	}

	void ProcClient::getStats(prt::Statistics &stats, const std::locale &loc) const
	{
		if(!mRenderDevice->running())
			return;

		mRenderDevice->getStats(stats, loc);

		using pl::alloc_stat;
		stats["memory: texture"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::Texture{}).totalAllocated)});
		stats["memory: contribution-buffers"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::ContBuffer{}).totalAllocated)});
		stats["memory: film"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::Film{}).totalAllocated)});
		stats["memory: primitive"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::Primitive{}).totalAllocated)});
		stats["memory: accel-struct"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::AccelStruct{}).totalAllocated)});
		stats["memory: entity"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::Entity{}).totalAllocated)});
		stats["memory: other"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::Other{}).totalAllocated)});

		stats["render-time"] = fmt::format(loc, "{:L}:{:%M:%S}",
			std::chrono::duration_cast<std::chrono::hours>(mRenderTime.total_running_time()).count(), std::chrono::duration_cast<std::chrono::seconds>(mRenderTime.total_running_time()));	}
}

#endif
