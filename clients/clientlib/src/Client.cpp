/*
	PhotoRT

	Copyright (C) 2017 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <PhotortClientLib/Client.hpp>

#include <PhotoRT/Core/Integration.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/RenderEntityImportExport.hpp>
#include <PhotoRT/Core/Compression.hpp>
#include <PhotoRT/Core/MemorySystem.hpp>
#include <PhotoRT/Core/Instance.hpp>

#include <patlib/scoped.hpp>
#include <patlib/task.hpp>
#include <patlib/path.hpp>
#include <patlib/mutex.hpp>
#include <patlib/io.hpp>

#include <fmt/format.h>
#include <fmt/ostream.h>

#if __has_include(<dlfcn.h>)
	#include <dlfcn.h>
#endif

namespace PRTClientLib
{
	std::filesystem::path getAppPluginDir()
	{
		return pl::paths::app_prefix_dir() / PRTCLIENT_MODULE_DIR;
	}

	prt::ParameterTree loadConfigFile(const std::filesystem::path &filename, prt::IParameterParser &mParser, std::string_view defaultConfig)
	{
		if(!std::filesystem::exists(filename))
		{
			if(defaultConfig.empty())
				return {};

			pl::log("ClientLib") << "Creating default config file in " << filename;

			auto dirs = filename;
			std::filesystem::create_directories(dirs.remove_filename());

			std::ofstream fs{filename};

			if(!fs)
				throw ClientLibException(fmt::format("Error while trying to write default config file {}.", filename));

			fs.exceptions(pl::io::failure_bits);
			fs << defaultConfig;
		}

		pl::log("ClientLib") << "Loading config file " << filename;

		return mParser.load(filename);
	}

	void logException(std::exception_ptr eptr, std::string_view module, std::string_view message) noexcept
	{
		pl::recurse_nested_exceptions(eptr, [&](std::exception_ptr e){
			try { std::rethrow_exception(e); }
			catch(const pl::Exception &e)
			{
				pl::log(e.module(), pl::logtag::error) << "Error: " << message << e.message();
			}
			catch(const std::system_error &e)
			{
				pl::log(module, pl::logtag::error) << "System error: [" << e.code() << "] " << message << e.what();
			}
			catch(const std::exception &e)
			{
				pl::log(module, pl::logtag::error) << "Error: " << message << e.what();
			}
			catch(...)
			{
				pl::log(module, pl::logtag::error) << message << "Unexpected error.";
			}
		});
	}

	Client::Client()
	{
		mScene.mFactories = std::make_unique<prt::EntityFactoriesProxy>(prt::factories);
		mRender.mFactories = std::make_unique<prt::EntityFactoriesProxy>(prt::factories);
	}

	Client::~Client() = default;

	void Client::loadModule(const std::filesystem::path &path)
	{
		pl::log("ClientLib") << "Loading module: " << path;

		boost::dll::fs::error_code ec;

		mModules.emplace_front(boost::dll::fs::path(path.string()), ec);

		if(ec)
			throw ClientLibException(fmt::format("Failed loading module: {}: {}", path,
				#if __has_include(<dlfcn.h>)
					dlerror()
				#else
					ec.message()
				#endif
			));
	}

	void Client::loadAllModules(const std::filesystem::path &path)
	{
		if(std::filesystem::exists(path))
			for(const auto &filename : std::filesystem::directory_iterator{path})
				if(const auto p = filename.path(); std::filesystem::is_regular_file(p) && p.filename().string()[0] != '.')
					loadModule(p);
	}

	void Client::parseConfig()
	{
		auto ieMap = prt::factories.mEntityImportExport;

		pl::log("ClientLib") << "Configuring importers/exporters.";
		if(auto p = mConfig.findParam("import_export_config"); p && p->childParams)
		{
			for(const auto &module : *p->childParams | rgv::values)
			{
				if(auto ie = ieMap.find(module["module"].value); ie != ieMap.end())
				{
					ie->second->setConfig(*module["config"].childParams);
					ieMap.erase(ie);
				}
			}
		}

		for(auto &ie : ieMap | rgv::values)
			ie->setConfig({});

		if(auto p = mConfig.findParam("entities"); p && p->childParams)
			mRender.addEntities(*p->childParams);

		mRayTracerEngine = mRender.getEntitySptr("main_raytracer", type_v<prt::IRayTracerEngine>);

		if(!mRayTracerEngine)
			throw ClientLibException("No main ray-tracer engine found in render parameter. A ray-tracer engine with ID main_raytracer must be defined.");

		auto memSys = prt::DynEntityCast<pl::HeapMemorySystem<prt::VoidOffsetPtr>>(prt::MemorySystemEntity<pl::HeapMemorySystem<prt::VoidOffsetPtr>>::Factory.create_shared());
		#ifdef PL_LIB_HUGEPAGES
			memSys->resource(pl::hugepages_resource());
		#endif
		prt::getMemorySystem<>() = memSys;

		if(prt::getMemoryOwnership())
			prt::getMemorySystem<>()->init_random();

		const auto setMemSys =
			[&](std::string_view name, auto tag)
			{
				using Cat = pl::decay_t<decltype(tag)>;
				prt::getMemorySystem<Cat>() = mRender.findEntitySptr(name, type_v<pl::IMemorySystem<prt::VoidOffsetPtr>>);

				if(!prt::getMemorySystem<Cat>())
					prt::getMemorySystem<Cat>() = memSys;
			};

		setMemSys("memory_system-cont_buffers", prt::memtag::ContBuffer{});
		setMemSys("memory_system-primitive", prt::memtag::Primitive{});
		setMemSys("memory_system-texture", prt::memtag::Texture{});
		setMemSys("memory_system-film", prt::memtag::Film{});
	}

	void Client::loadConfig(const std::filesystem::path &filename)
	{
		mRender.clearEntities();

		mConfig = loadConfigFile(filename, *mParser, default_config);

		parseConfig();
	}

	void Client::loadConfig(std::istream &is)
	{
		mRender.clearEntities();

		mConfig = mParser->load(is);

		parseConfig();
	}

	void Client::initScene()
	{
		mScene.setRootInstance(mScene.getEntitySptr("root_instance", type_v<prt::IObjectInstance>));
		mScene.init();
	}

	void Client::openScene(const std::filesystem::path &path)
	{
		mScene.loadFromDirectory(path);

		initScene();
	}

	void Client::openScene(std::istream &is)
	{
		is >> mScene;

		initScene();
	}

	void Client::saveScene(const std::filesystem::path &path)
	{
		mScene.saveToDirectory(path);
	}

	void Client::clear()
	{
		mScene.clearScene();
		mRender.clearEntities();

		mRayTracer = nullptr;
		mRayTracerEngine = nullptr;
		mConfig.clear();
	}

	void Client::initForRendering()
	{
		const auto startTime = pl::atomic_fence_return([]{ return std::chrono::high_resolution_clock::now(); });

		pl::tasks.task_group([&](auto &tg)
		{
			for(auto &rtEntity :
				mRender.mEntities.left
					| rgv::filter([](auto &e){ return e.second->getEntityType().starts_with("raytracer-"); })
					| rgv::values
			)
			{
				tg.run([&, &rt = *rtEntity]{
					pl::log("ClientLib", "Initializing ray-tracer {}.", pl::io::quoted{rt.getEntityId()});
					dynamic_cast<prt::IRayTracerEngine &>(rt).init(mScene);
				});
			}
		});

		mRayTracer = mRayTracerEngine->createTracer(-1, false);

		updateSceneSubstanceStack(*mRayTracer, mScene);

		const auto totalTime = pl::atomic_fence_return([]{ return std::chrono::high_resolution_clock::now(); }) - startTime;

		pl::log("ClientLib", "Preparations for rendering done in {} seconds.", std::chrono::duration_cast<std::chrono::duration<double>>(totalTime).count());
	}

	void Render::loadFromConfig(std::string_view devKey)
	{
		mRenderDeviceController = std::make_shared<prt::RenderDeviceController>();

		if(const auto p = mClient.mConfig.findParam(devKey); p && p->childParams)
		{
			for(const auto &entity : *p->childParams)
			{
				auto [type, id] = entity.second.getEntityType();

				if(!type.empty() && entity.second.getValueOrDefault("enable", false))
				{
					pl::log("ClientLib", "Render device: \"{}/{}\"", type, id);

					auto device = mClient.mRender.createEntity(type_v<prt::IRenderDeviceBuffered>, type, std::move(id), entity.second.childParams ? *entity.second.childParams : prt::ParameterTree{});

					mRenderDeviceController->mThreads.push_back(device);
				}
			}
		}

		mRenderDeviceController->priority(pl::EnumThreadPriority::Low);

		if(mRenderDeviceController->mThreads.empty())
			throw ClientLibException(fmt::format("No render device found for {}.", pl::io::quoted{devKey}));
	}

	void Render::loadRenderParams(std::string_view paramKey)
	{
		mRenderContext = mClient.mScene.getEntitySptr(paramKey, type_v<prt::RenderContext>);

		if(!mRenderContext)
			throw ClientLibException(fmt::format("Render parameter {} not found.", pl::io::quoted{paramKey}));

		mRenderDeviceController->setRenderContext(mRenderContext);
	}

	void Render::initForRendering()
	{
		mRenderContext->setNbLayers(mClient.mScene.getLightGroups().size() + 1);
		initForRendering2();
	}

	void Render::initForRendering2()
	{
		mRenderContext->initFilters();
		mRenderContext->initFilmBuffers();
		mRenderDeviceController->init(mClient.mScene);
		mRenderDeviceController->initTargets();
		mRenderContext->initBuffers();
		mRenderContext->initCamera();
	}

	void Render::getStats(prt::Statistics &stats, const std::locale &loc) const
	{
		if(!mRenderDeviceController || !mRenderDeviceController->running())
			return;

		mRenderDeviceController->getStats(stats, loc);

		using pl::alloc_stat;
		stats["memory: texture"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::Texture{}).totalAllocated)});
		stats["memory: contribution-buffers"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::ContBuffer{}).totalAllocated)});
		stats["memory: film"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::Film{}).totalAllocated)});
		stats["memory: primitive"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::Primitive{}).totalAllocated)});
		stats["memory: accel-struct"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::AccelStruct{}).totalAllocated)});
		stats["memory: entity"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::Entity{}).totalAllocated)});
		stats["memory: other"] = fmt::format(loc, "{:.2f}", pl::io::memory_size{static_cast<double>(alloc_stat(prt::memtag::Other{}).totalAllocated)});
	}

	void printStats(std::ostream &os, const prt::Statistics &stats)
	{
		if(!stats.empty())
		{
			const auto length = rg::accumulate(stats, szt{}, pl::ops::op_max, [](const auto &s){ return s.first.length(); });

			for(const auto &s : stats)
				os << std::setfill(' ') << std::left << std::setw(length) << s.first << " : " << std::right << std::setw(18) << s.second << '\n';
		}
	}

	void Render::saveState(const prt::ParameterTree &params)
	{
		auto dsf = std::make_shared<prt::DataSourceFile>();

		const pl::fast_scoped_assignment ta(mClient.mScene.mDataSource, dsf);

		dsf->setFilePath(mClient.mScene.getFilePath() / "render_state");
		const auto path = dsf->getFilePath();
		std::filesystem::create_directories(path);

		if(mRenderContext->mFilms.empty())
			throw ClientLibException("No film defined for the scene.");

		for(auto &f : mRenderContext->mFilms)
		{
			f.second.compile();

			std::ofstream of{path / (f.first + ".prtfilm"), std::ios_base::binary | std::ios_base::out};
			of.exceptions(pl::io::failure_bits);

			pl::io::data_header headers;

			headers.mIsBinary = true;

			#ifdef PRT_HAS_COMPRESSION
				headers.mCompressor = BOOST_STRINGIZE(PRT_DEFAULT_COMPRESSOR);
			#endif
			using compressor = prt::compressor;
			//using compressor = pl::io::uncompressed_t;

			headers.apply_to(of);

			of << headers;

			pl::io::write_stream(of, [&](auto& os){ os << f.second; }, compressor{});
		}

		{
			const auto params = mRenderContext->getParameters(true);
			mClient.mParser->save(params, path / "render_params.prts");

			params.traverse([&](const auto &p, auto /*level*/){
				if(p.second.isEntity())
					if(const auto [type, id] = p.second.getEntityType(); !id.empty() && id[0] != '&')
						dsf->serialize(mClient.mScene[id]);
			});
		}

		mClient.mParser->save(params, path / "params.prts");

		for(const auto &device : mRenderDeviceController->mThreads)
			dsf->serialize(dynamic_cast<const prt::IRenderEntity &>(*device));
	}

	std::optional<prt::ParameterTree> Render::loadState()
	{
		auto dsf = std::make_shared<prt::DataSourceFile>();

		const pl::fast_scoped_assignment ta(mClient.mScene.mDataSource, dsf);

		dsf->setFilePath(mClient.mScene.getFilePath() / "render_state");
		const auto path = dsf->getFilePath();

		if(std::filesystem::exists(path))
		{
			pl::log("ClientLib") << "Restoring render state.";

			{
				prt::ScopedOverwrite guard{mClient.mScene};

				mRenderContext->setParameters(mClient.mParser->load(path / "render_params.prts"));
				initForRendering2();
			}

			for(auto &f : mRenderContext->mFilms)
			{
				std::ifstream infile{path / (f.first + ".prtfilm"), std::ios_base::binary | std::ios_base::in};

				f.second.zeroBuffers();
				f.second.zeroFilm();

				if(infile)
				{
					infile.exceptions(pl::io::failure_bits);

					pl::io::data_header headers;
					infile >> headers;
					headers.apply_to(infile);

					const bool valid = prt::selectDecompressor(headers.mCompressor, [&](auto&& decompressor){
						pl::io::read_stream(infile, [&](auto& is){ is >> f.second; }, PL_FWD(decompressor));
					});

					if(!valid)
						throw ClientLibException(fmt::format("Invalid or unsupported compression type {} for film {}.", pl::io::quoted{headers.mCompressor}, pl::io::quoted{f.first}));
				}
			}

			for(auto &device : mRenderDeviceController->mThreads)
				dsf->unserialize(dynamic_cast<prt::IRenderEntity &>(*device));

			return mClient.mParser->load(path / "params.prts");
		}

		return std::nullopt;
	}

	void Render::clear()
	{
		mRenderDeviceController = nullptr;
		mRenderContext = nullptr;
	}
}
