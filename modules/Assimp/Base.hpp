/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/RenderEntityImportExport.hpp>
#include <PhotoRT/PrimitiveGroup/Triangle.hpp>
#include <PhotoRT/Bitmap/Bitmap.hpp>
#include <PhotoRT/Core/Material.hpp>

#ifdef PrtModule_Assimp_SHARED
	#ifdef PrtModule_Assimp_EXPORTS
		#define PRTMODULE_ASSIMP_API BOOST_SYMBOL_EXPORT
	#else
		#define PRTMODULE_ASSIMP_API BOOST_SYMBOL_IMPORT
	#endif
#else
	#define PRTMODULE_ASSIMP_API
#endif

namespace PRTAssimp
{
	PRT_IMPORT_NS

	namespace rg = ranges;
	namespace rgv = rg::views;
}
