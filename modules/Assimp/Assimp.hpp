/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Base.hpp"

#include <assimp/material.h>

struct aiMesh;
struct aiScene;
struct aiNode;
struct aiMaterial;

namespace PRTAssimp
{
	class PRTMODULE_ASSIMP_API AssimpException : public prt::Exception
	{
	public:
		using Exception::Exception;

		[[nodiscard]] virtual const char *module() const noexcept override;
	};

	class PRTMODULE_ASSIMP_API ImportExportAssimp : public prt::EntityImportExportBase
	{
	protected:
		prt::ExtensionMimeTypeMap mMimeTypesImport, mMimeTypesExport;

		struct MeshParams
		{
			prt::EntityPtr<prt::Material> material;
			bool isLightPortal, isEmitter, removeDuplicateVertices, swapTrianglesOrientation;
			Real interpolateNormals;
		};

	public:
		PRT_IMPORTEXPORTER_DECLARE(ImportExportAssimp)

		ImportExportAssimp();

		[[nodiscard]] virtual prt::ParameterTree getConfig() const override;
		virtual void setConfig(const prt::ParameterTree &params) override;

		[[nodiscard]] virtual bool canImport(std::string_view mimeType) const noexcept override;
		[[nodiscard]] virtual bool canExport(std::string_view mimeType) const noexcept override;

		[[nodiscard]] virtual std::string getEntityType(std::string_view mimeType) const override;
		[[nodiscard]] virtual prt::ExtensionMimeTypeMap getMimeTypesImport() const override;
		[[nodiscard]] virtual prt::ExtensionMimeTypeMap getMimeTypesExport() const override;
		[[nodiscard]] virtual std::vector<std::string> getGenericTypes() const override;

		[[nodiscard]] virtual bool isStreamable(std::string_view /*mimeType*/) const noexcept override { return false; }

		[[nodiscard]] virtual prt::EntityPtrBase importEntity(prt::EntityCollection &entityCollection, const std::filesystem::path &filename, std::string_view genericType, std::string_view mimeType, std::string id, const prt::ParameterTree &params = {}) override;
		//virtual void exportEntity(prt::EntityCollection &entityCollection, const prt::IExportHook &entity, const std::filesystem::path &filename, std::string_view mimeType, const ParameterTree &params) const;

	protected:

		std::filesystem::path mTempPath;

		void processNode(prt::EntityCollection &entityCollection, std::string_view id, const aiNode &node, prt::IObjectInstance &instance, std::span<const prt::EntityPtr<prt::PrimGroupTriangles>> meshes) const;
		void processMesh(prt::PrimGroupTriangles &obj, const aiMesh &mesh, std::span<const prt::EntityPtr<prt::Material>> materials) const;

		void processTexture(prt::EntityCollection &entityCollection, const aiScene &scene, std::string_view id, const std::filesystem::path &basePath, const aiMaterial &mat, aiTextureType type, std::string_view typeStr, u32 index, bool deleteTempFile, std::string_view uid) const;
		[[nodiscard]] std::string getTextureName(std::string_view id, std::string_view typeStr, std::string_view file) const;

		void applyMeshParams(const MeshParams &meshParams, prt::PrimGroupTriangles &mesh);
	};
}
