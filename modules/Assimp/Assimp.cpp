/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Assimp.hpp"

#include <PhotoRT/Core/Instance.hpp>
#include <PhotoRT/Core/PrimitiveGroup.hpp>

#include <boost/assign/list_inserter.hpp>
#include <boost/algorithm/string.hpp>

#include <patlib/task.hpp>
#include <patlib/path.hpp>
#include <patlib/algorithm.hpp>

#include <shared_access/shared.hpp>

#include <boost/dynamic_bitset.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/DefaultLogger.hpp>
#include <assimp/importerdesc.h>
#include <assimp/cexport.h>

namespace PRTAssimp
{
	PRT_IMPORTEXPORTER_DEFINE("assimp", ImportExportAssimp)

	namespace sa = shared_access;

	const char *AssimpException::module() const noexcept
		{ return "ModuleAssimp"; }

	[[nodiscard]] std::string_view removeTrailingEOL(const char *message) noexcept
	{
		std::string_view v{message};

		if(v.ends_with('\n'))
			v.remove_suffix(1);

		return v;
	}

	class AssimpLoggerInfo : public Assimp::LogStream
	{
	public:

		virtual void write(const char *message)
		{
			pl::log("Assimp") << removeTrailingEOL(message);
		}
	};

	class AssimpLoggerWarning : public Assimp::LogStream
	{
	public:

		virtual void write(const char *message)
		{
			pl::log("Assimp") << removeTrailingEOL(message);
		}
	};

	class AssimpLoggerError : public Assimp::LogStream
	{
	public:

		virtual void write(const char *message)
		{
			pl::log("Assimp", pl::logtag::error) << removeTrailingEOL(message);
		}
	};


	ImportExportAssimp::ImportExportAssimp()
	{
		auto &logger = *Assimp::DefaultLogger::create("");

		logger.attachStream(new AssimpLoggerInfo(),  Assimp::Logger::ErrorSeverity::Info);
		logger.attachStream(new AssimpLoggerWarning(), Assimp::Logger::ErrorSeverity::Warn);
		logger.attachStream(new AssimpLoggerError(), Assimp::Logger::ErrorSeverity::Err);

		Assimp::Importer importer;

		for(const auto nb = importer.GetImporterCount();
			const auto importerInfo : mir(nb) | rgv::transform([&](const auto i){ return importer.GetImporterInfo(i); }))
		{
			for(const auto ext : std::string_view{importerInfo->mFileExtensions} | std::ranges::views::split(' ') | pl::string_adaptor<>)
				mMimeTypesImport.left.insert({ext, std::string{"custom:"} + ext});
		}

		/*Assimp::Exporter exporter;

		for(const auto nb = exporter.GetExportFormatCount();
			const auto exporterInfo : mir(nb) | rgv::transform([&](const auto i){ return exporter.GetExportFormatDescription(i); }))
		{
			for(const auto ext : std::string_view{exporterInfo->fileExtension} | std::ranges::views::split(' '))
			{
				mMimeTypesExport.left.insert({std::string{ext}, "custom:" + ext});
			}
		}*/

		const auto tryInsertMime = [&](std::string ext, std::string mimeType)
			{
				if(mMimeTypesImport.left.find(ext) == mMimeTypesImport.left.end())
					mMimeTypesImport.left.insert({ext, mimeType});
				if(mMimeTypesExport.left.find(ext) == mMimeTypesExport.left.end())
					mMimeTypesExport.left.insert({std::move(ext), std::move(mimeType)});
			};

		tryInsertMime("dae", "model/vnd.collada+xml");
		tryInsertMime("3ds", "application/x-3ds");
		tryInsertMime("obj", "model/obj");
		tryInsertMime("dxf", "application/dxf");
		tryInsertMime("stl", "model/stl");
		tryInsertMime("lwo", "image/x-lwo");
		tryInsertMime("lws", "image/x-lws");
		tryInsertMime("x3d", "model/x3d+xml");
	}

	prt::ParameterTree ImportExportAssimp::getConfig() const
	{
		return {
			{"temp_path", mTempPath}
		};
	}

	void ImportExportAssimp::setConfig(const prt::ParameterTree &params)
	{
		params.getValueOrDefault("temp_path", mTempPath, pl::paths::user_temp_dir() / "photort");

		std::filesystem::create_directories(mTempPath);

		pl::log("Assimp") << "Using temporary path " << mTempPath << '.';
	}

	std::vector<std::string> ImportExportAssimp::getGenericTypes() const
		{ return {"object"}; }

	bool ImportExportAssimp::canImport(std::string_view mimeType) const noexcept
		{ return mMimeTypesImport.right.find(std::string(mimeType)) != mMimeTypesImport.right.end(); }

	bool ImportExportAssimp::canExport(std::string_view mimeType) const noexcept
		{ return mMimeTypesExport.right.find(std::string(mimeType)) != mMimeTypesExport.right.end(); }

	std::string ImportExportAssimp::getEntityType(std::string_view /*mimeType*/) const
		{ return "instance"; }

	prt::ExtensionMimeTypeMap ImportExportAssimp::getMimeTypesImport() const
		{ return mMimeTypesImport; }
	prt::ExtensionMimeTypeMap ImportExportAssimp::getMimeTypesExport() const
		{ return mMimeTypesExport; }

	void ImportExportAssimp::applyMeshParams(const MeshParams &meshParams, prt::PrimGroupTriangles &mesh)
	{
		if(meshParams.isLightPortal)
			mesh.setMaterial(nullptr);
		else if(meshParams.material)
			mesh.setMaterial(meshParams.material);

		if(meshParams.removeDuplicateVertices)
			mesh.removeDuplicateVertices();

		if(meshParams.swapTrianglesOrientation)
			mesh.swapTrianglesOrientation();

		if(meshParams.interpolateNormals > 0)
		{
			mesh.buildVtsTriIndex();
			mesh.computeSmoothVerticesNormals( pl::plcos(pl::degree_to_radian(meshParams.interpolateNormals)) );
			mesh.clearVtsTriIndex();
		}

		mesh.setEmitter(meshParams.isEmitter);
		mesh.setLightPortal(meshParams.isLightPortal);
	}

	std::string ImportExportAssimp::getTextureName(std::string_view id, std::string_view typeStr, std::string_view file) const
	{
		if(file[0] == '*')
			file.remove_prefix(1);

		std::string tName{file};
		tName | pl::transform([](auto c){ return c == '/' || c == '\\' ? '_' : c; });

		return pl::strconcat(std::string{}, id, "_texture_", typeStr, '_', tName);
	}

	void ImportExportAssimp::processTexture(prt::EntityCollection &entityCollection, const aiScene &scene, std::string_view id, const std::filesystem::path &basePath, const aiMaterial &mat, aiTextureType type, std::string_view typeStr, u32 index, bool /*deleteTempFile*/, std::string_view /*uid*/) const
	{
		auto tfile = std::invoke([&]{
				aiString path;

				mat.GetTexture(type, index, &path);

				return std::string{ pl::trim(std::string_view{path.C_Str(), path.length}) };
			});

		if(tfile[0] == '*')
		{
			const std::string_view indexStr{tfile.begin() + 1, tfile.end()};
			const auto index = pl::io::from_string_noloc<szt>(indexStr);

			if(index >= scene.mNumTextures)
				pl::log("Assimp", pl::logtag::error, "Invalid embedded texture index {}.", index);
			else
			{
				auto tName = getTextureName(id, typeStr, tfile);
				const auto &stored = *scene.mTextures[index];

				if(stored.mWidth > 0 && !entityCollection.hasEntity(tName))
				{
					if(stored.mHeight > 0)
					{
						const auto texture = entityCollection.createEntity(type_v<prt::BitmapBase>, "texture-32", std::move(tName));
						texture->allocate(prt::Dir3Di{
							static_cast<prt::Dir3Di::value_type>(stored.mWidth),
							static_cast<prt::Dir3Di::value_type>(stored.mHeight), 1});

						const auto cv = [](const auto &p)
							{ return prt::convertToColor<prt::Color>((u32)p.r | ((u32)p.g << 8) | ((u32)p.b << 16) | ((u32)p.a << 24)); };

						for(const auto i : mir(texture->getNbPixels()))
							texture->setPixel(cv(stored.pcData[i]), i);
					}
					else
					{
						entityCollection.importEntity(std::span<const std::byte>{reinterpret_cast<const std::byte *>(stored.pcData), stored.mWidth}, "image", "", std::move(tName));
					}
				}
			}
		}
		else
		{
			tfile | pl::transform([](auto c){ return c == '\\' ? '/' : c; });

			const auto tpath = basePath / tfile;

			if(std::filesystem::exists(tpath))
			{
				if(auto tName = getTextureName(id, typeStr, tfile); !entityCollection.hasEntity(tName))
					entityCollection.importEntity(tpath, "image", prt::factories.getMimeTypeImport(tpath), std::move(tName));
			}
			else
				pl::log("Assimp", pl::logtag::error, "Texture file {} not found.", tpath);
		}
	}

	prt::EntityPtrBase ImportExportAssimp::importEntity(prt::EntityCollection &entityCollection, const std::filesystem::path &filename, std::string_view /*genericType*/, std::string_view /*mimeType*/, std::string id, const prt::ParameterTree &params)
	{
		unsigned int flags = aiProcess_Triangulate | aiProcess_SortByPType | aiProcess_TransformUVCoords | aiProcess_GenUVCoords | aiProcess_FlipUVs;

		if(params.getValueOrDefault("validate", false))
			flags |= aiProcess_FindDegenerates | aiProcess_FindInvalidData | aiProcess_ValidateDataStructure;

		if(params.getValueOrDefault("join_identical_vertices", false))
			flags |= aiProcess_JoinIdenticalVertices;

		if(params.getValueOrDefault("optimize", false))
			flags |= aiProcess_OptimizeGraph;

		/*if(params.getValueOrDefault("interpolate_normals", false))
		{
			importer.SetPropertyFloat(AI_CONFIG_PP_GSN_MAX_SMOOTHING_ANGLE, 180 - pl::radian_to_degree(std::acos(r_(params["interpolate_normals"]))));
			flags |= aiProcess_GenSmoothNormals;
		}*/

		Assimp::Importer importer;

		importer.SetPropertyInteger(AI_CONFIG_PP_SBP_REMOVE, aiPrimitiveType_POINT | aiPrimitiveType_LINE);
		importer.SetPropertyInteger(AI_CONFIG_PP_FD_REMOVE, 1);

		const auto scene = importer.ReadFile(filename.string(), flags);

		if(!scene)
			throw AssimpException(fmt::format("Failed to load file {}: {}", filename, importer.GetErrorString()));

		const auto uid = pl::io::stream_into(std::string{}, pl::io::throw_on_failure, [&](auto &os, auto&& str){
				os << std::this_thread::get_id();
				return str;
			}) | pl::transform([&](const auto c){ return c == pl::any_of{'\\', '/', '~', ' ', '*', ':', '"', '?', '<', '>', '|', '\0'} ? '_' : c; });

		const auto basePath                  = filename.parent_path();
		const auto tryLoadMaterials          = params.getValueOrDefault("try_load_materials", false);
		const auto deleteTempFiles           = params.getValueOrDefault("delete_temp_files", true);
		const auto tryLoadTextures           = params.getValueOrDefault("try_load_textures", true);
		const std::string_view diffuseType   = params.getValueOrDefault("use_oren_nayar", false) ?
			R"(type="bxdf-orennayar" roughness="25")" :
			R"(type="bxdf-lambertian")";

		const auto defaultMat = std::invoke([&]{
				if(const auto p = params.findParam("default_material"); p)
					return entityCollection.createEntity(type_v<prt::Material>, *p);

				return entityCollection.createEntity(type_v<prt::Material>, prt::Material::Factory, id + "_material",
						R"(<e><bxdf type="bxdf-lambertian"><texture type="surfacemap_color-value" v="0.5 0.5 0.5 1.0"></texture></bxdf></e>)"
					);
			});

		std::vector<prt::EntityPtr<prt::PrimGroupTriangles>>  meshes;
		std::vector<prt::EntityPtr<prt::Material>>            materials;

		sa::shared sharedEntityList{sa::in_place, std::ref(entityCollection)};

		materials.reserve(scene->mNumMaterials);

		if(tryLoadTextures)
			for(const auto &mat : std::span{scene->mMaterials, scene->mNumMaterials})
			{
				for(const auto c : mir(mat->GetTextureCount(aiTextureType_DIFFUSE)))
					processTexture(entityCollection, *scene, id, basePath, *mat, aiTextureType_DIFFUSE, "diffuse", c, deleteTempFiles, uid);

				for(const auto c : mir(mat->GetTextureCount(aiTextureType_SPECULAR)))
					processTexture(entityCollection, *scene, id, basePath, *mat, aiTextureType_SPECULAR, "specular", c, deleteTempFiles, uid);

				for(const auto c : mir(mat->GetTextureCount(aiTextureType_EMISSIVE)))
					processTexture(entityCollection, *scene, id, basePath, *mat, aiTextureType_EMISSIVE, "emissive", c, deleteTempFiles, uid);

				for(const auto c : mir(mat->GetTextureCount(aiTextureType_HEIGHT)))
					processTexture(entityCollection, *scene, id, basePath, *mat, aiTextureType_HEIGHT, "height", c, deleteTempFiles, uid);

				for(const auto c : mir(mat->GetTextureCount(aiTextureType_OPACITY)))
					processTexture(entityCollection, *scene, id, basePath, *mat, aiTextureType_OPACITY, "opacity", c, deleteTempFiles, uid);
			}

		std::map<std::string, prt::EntityPtr<prt::Material>> providedMaterialsName;
		std::map<u32, prt::EntityPtr<prt::Material>> providedMaterialsIndex;

		if(const auto p = params.findParam("materials"); p && p->childParams)
			for(auto &m : *p->childParams)
			{
				if(const auto pmat = m.second.findParam("material"); pmat)
				{
					const auto material = entityCollection.createEntity(type_v<prt::Material>, *pmat);

					if(const auto pi = m.second.findParam("index"); pi)
						providedMaterialsIndex[static_cast<u32>(*pi)] = material;

					if(const auto pi = m.second.findParam("name"); pi)
					{
						providedMaterialsName[pi->value] = material;
						material->setEntityName(pi->value);
					}
				}
			}

		for(const auto i : mir(scene->mNumMaterials))
		{
			auto mat = scene->mMaterials[i];

			const auto matName = std::invoke([&]{
					aiString n;
					mat->Get(AI_MATKEY_NAME, n);
					return std::string{n.C_Str()};
				});

			const auto material = std::invoke([&]() -> prt::EntityPtr<prt::Material> {
					if(auto it = providedMaterialsName.find(matName); it != providedMaterialsName.end())
						return it->second;
					if(auto it = providedMaterialsIndex.find(i); it != providedMaterialsIndex.end())
						return it->second;

					return {};
				});

			if(material)
				materials.push_back(material);
			else if(tryLoadMaterials)
			{
				aiTextureMapping tMapping;
				u32 uvIndex;
				aiString path;
				std::string matparams, texParams, alphaParams;
				aiColor3D diffuseColor, specularColor, emissiveColor;
				float ior;
				float opacity;
				bool hasDiffuse = true, hasSpecular = true, hasOpacity = true;//, hasEmission = true;

				if(mat->Get(AI_MATKEY_COLOR_DIFFUSE, diffuseColor) != AI_SUCCESS || diffuseColor.IsBlack())
					hasDiffuse = false;
				if(mat->Get(AI_MATKEY_COLOR_SPECULAR, specularColor) != AI_SUCCESS || specularColor.IsBlack())
					hasSpecular = false;
				//if(mat->Get(AI_MATKEY_COLOR_EMISSIVE, emissiveColor) != AI_SUCCESS || emissiveColor.IsBlack())
					//hasEmission = false;
				if(mat->Get(AI_MATKEY_OPACITY, opacity) != AI_SUCCESS)
					hasOpacity = false;

				matparams = R"(<e>)";
				matparams += R"(<transparent />)";

					if(mat->Get(AI_MATKEY_REFRACTI, ior) == AI_SUCCESS)
						matparams += R"(<refraction_index value=")" + pl::io::to_string_noloc(ior) + R"(" />)";

					matparams += R"(<bxdf type="bxdf-multi"><bxdfs array="1">)";

					u32 nbTex = 0;

					for(const auto c : mir(mat->GetTextureCount(aiTextureType_OPACITY)))
					{
						uvIndex = 0;
						mat->GetTexture(aiTextureType_OPACITY, c, &path, &tMapping, &uvIndex);

						if(const auto tName = getTextureName(id, "opacity", std::string(path.C_Str(), path.length)); entityCollection.hasEntity(tName))
						{
							texParams += R"(<e type="surfacemap_color-dist2d">)";
								texParams += R"(<uvmap type="uvmap-texcoord" uvindex=")" + pl::io::to_string_noloc(uvIndex) + R"(" />)";
								texParams += R"(<dist type="colordist-bitmap2d"><bitmap type="texture" link=")" + tName + R"(" /></dist>)";
							texParams += R"(</e>)";

							++nbTex;
						}
					}

					if(nbTex || hasOpacity)
					{
						alphaParams += R"(<alpha type="surfacemap_color-multi_multiply"><maps array="1">)";

						if(hasOpacity)
						{
							alphaParams += R"(<e type="surfacemap_color-value" v=")" + pl::io::to_string_noloc(opacity) + R"( 1" />)";
						}

						if(nbTex)
						{
							alphaParams += texParams;
						}

						alphaParams += R"(</maps></alpha>)";
					}

					texParams.clear();
					nbTex = 0;

					for(const auto c : mir(mat->GetTextureCount(aiTextureType_DIFFUSE)))
					{
						uvIndex = 0;
						mat->GetTexture(aiTextureType_DIFFUSE, c, &path, &tMapping, &uvIndex);

						if(const auto tName = getTextureName(id, "diffuse", std::string(path.C_Str(), path.length)); entityCollection.hasEntity(tName))
						{
							texParams += R"(<e type="surfacemap_color-dist2d">)";
								texParams += R"(<uvmap type="uvmap-texcoord" uvindex=")" + pl::io::to_string_noloc(uvIndex) + R"(" />)";
								texParams += R"(<dist type="colordist-bitmap2d"><bitmap type="texture" link=")" + tName + R"(" /></dist>)";
							texParams += R"(</e>)";

							++nbTex;
						}
					}

					if(nbTex)
					{
						matparams += "<e><bxdf " + diffuseType + '>' + alphaParams + R"(<texture type="surfacemap_color-multi"><maps array="1">)";
							matparams += texParams;
						matparams += R"(</maps></texture></bxdf></e>)";
					}
					else if(hasDiffuse)
					{
						matparams += "<e><bxdf " + diffuseType + '>' + alphaParams + R"(<texture type="surfacemap_color-value" v=")" + pl::io::to_string_noloc((prt::CReal)diffuseColor.r) + ' ' + pl::io::to_string_noloc((prt::CReal)diffuseColor.g) + ' ' + pl::io::to_string_noloc((prt::CReal)diffuseColor.b) + R"( 1" /></bxdf></e>)";
					}

					texParams.clear();
					nbTex = 0;

					for(const auto c : mir(mat->GetTextureCount(aiTextureType_SPECULAR)))
					{
						uvIndex = 0;
						mat->GetTexture(aiTextureType_SPECULAR, c, &path, &tMapping, &uvIndex);

						if(const auto tName = getTextureName(id, "specular", std::string(path.C_Str(), path.length)); entityCollection.hasEntity(tName))
						{
							texParams += R"(<e type="surfacemap_color-dist2d">)";
								texParams += R"(<uvmap type="uvmap-texcoord" uvindex=")" + pl::io::to_string_noloc(uvIndex) + R"(" />)";
								texParams += R"(<dist type="colordist-bitmap2d" power="0.2 1"><bitmap type="texture" link=")" + tName + R"(" /></dist>)";
							texParams += R"(</e>)";

							++nbTex;
						}
					}

					if(nbTex)
					{
						matparams += R"(<e><bxdf type="bxdf-specular_reflect">)" + alphaParams + R"(<fresnel type="fresnel-dielectric_schlick" ior="1.5" /><microfacets type="microfacets-trowbridge" roughness="0.1" /><texture type="surfacemap_color-multi"><maps array="1">)";
							matparams += texParams;
						matparams += R"(</maps></texture></bxdf></e>)";
					}
					else if(hasSpecular)
					{
						matparams += R"(<e><bxdf type="bxdf-specular_reflect">)" + alphaParams + R"(<fresnel type="fresnel-dielectric_schlick" ior="1.5" /><microfacets type="microfacets-trowbridge" roughness="0.1" />)";
							matparams += R"(<texture type="surfacemap_color-value" v=")" + pl::io::to_string_noloc((prt::CReal)specularColor.r) + ' ' + pl::io::to_string_noloc((prt::CReal)specularColor.g) + ' ' + pl::io::to_string_noloc((prt::CReal)specularColor.b) + R"( 1" />)";
						matparams += R"(</bxdf></e>)";
					}

					matparams += R"(</bxdfs></bxdf>)";

					if(const auto nbBump = mat->GetTextureCount(aiTextureType_HEIGHT); nbBump)
					{
						matparams += R"(<normalmap type="surfacemap_normal-multi"><maps array="1">)";

							for(const auto c : mir(nbBump))
							{
								uvIndex = 0;
								mat->GetTexture(aiTextureType_HEIGHT, c, &path, &tMapping, &uvIndex);

								if(const auto tName = getTextureName(id, "height", std::string(path.C_Str(), path.length)); entityCollection.hasEntity(tName))
								{
									matparams += R"(<e type="surfacemap_normal-bump2d" strength="2">)";
										matparams += R"(<uvmap type="uvmap-texcoord" uvindex=")" + pl::io::to_string_noloc(uvIndex) + R"(" />)";
										matparams += R"(<dist type="colordist-bitmap2d"><bitmap type="texture-hdr32"><copy_from_2d type="bitmapread-bump2d"><bitmap type="texture" link=")" + tName + R"(" /></copy_from_2d></bitmap></dist>)";
									matparams += R"(</e>)";
								}
							}

						matparams += R"(</maps></normalmap>)";
					}

				matparams += R"(</e>)";

				auto newmat = entityCollection.createEntity(type_v<prt::Material>, prt::Material::Factory, id + "_material_" + pl::io::to_string_noloc(i), matparams);
				newmat->setEntityName(matName);
				materials.push_back(newmat);
			}
			else
				materials.push_back(defaultMat);
		}

		meshes.reserve(scene->mNumMeshes);

		prt::Matrix tMat{pl::tag::identity};
		const auto hasTransform = params.findParam("transform");
		if(hasTransform)
		{
			const auto transform = entityCollection.createEntity(type_v<prt::ITransform>, *hasTransform);
			tMat = (*transform)();
		}

		const auto interpolateNormals = params.findParam("interpolate_normals");
		Real smoothAmount = 0;
		bool forceIt = false;

		if(interpolateNormals)
		{
			smoothAmount = *interpolateNormals;
			forceIt = params.getValueOrDefault("interpolate_normals_force", false);
		}

		const auto smoothIfInvalid = params.findParam("interpolate_normals_if_invalid");
		if(smoothIfInvalid)
			smoothAmount = *smoothIfInvalid;

		smoothAmount = pl::plcos(pl::degree_to_radian(smoothAmount));


		const auto meshTask = [&](auto &meshEntity, const auto &mesh)
			{
				processMesh(meshEntity, mesh, materials);
				meshEntity.setClassName(materials[mesh.mMaterialIndex]->getEntityName());
				if(meshEntity.getClassName().first.empty())
					meshEntity.setClassName(meshEntity.getEntityId());

				if(hasTransform)
					meshEntity.transformVertices(tMat);

				if(!meshEntity.computeTriangleNormals(true))
					pl::log("Assimp", pl::logtag::error, "Some triangles normals are invalid in object {}.", pl::io::quoted{meshEntity.getEntityId()});

				if(interpolateNormals && (forceIt || meshEntity.mData->primitives.mVtsNormals.empty()))
				{
					meshEntity.buildVtsTriIndex();
					meshEntity.computeSmoothVerticesNormals(smoothAmount);
					meshEntity.clearVtsTriIndex();
				}

				if(!meshEntity.checkAndFixVertexNormals())
				{
					pl::log("Assimp", pl::logtag::error, "Some vertex normals are invalid in object {}.", pl::io::quoted{meshEntity.getEntityId()});

					if(smoothIfInvalid)
					{
						meshEntity.buildVtsTriIndex();
						meshEntity.computeSmoothVerticesNormals(smoothAmount);
						meshEntity.clearVtsTriIndex();
					}
				}

				meshEntity.computeTangents(pl::tag::identity);

			};

		pl::tasks.task_group([&](auto &tg)
		{
			for(const auto i : mir(scene->mNumMeshes))
			{
				auto mesh = scene->mMeshes[i];
				auto meshEntity = std::to_address( meshes.emplace_back(
						entityCollection.createEntity(type_v<prt::PrimGroupTriangles>, prt::PrimGroupTriangles::Factory, pl::strconcat(std::string{}, id + "_mesh_" + pl::io::to_string_noloc(i)))
					) );

				tg.run([=, &meshTask]{ meshTask(*meshEntity, *mesh); });
			}
		});


		if(const auto p = params.findParam("mesh_params"); p && p->childParams)
		{
			boost::dynamic_bitset<> removedObjects{meshes.size()};

			*p->childParams | rgv::values | pl::for_each([&](const auto &m)
			{
				const bool removeObj = m.getValueOrDefault("remove", false);

				MeshParams meshParams;

				if(const auto pmat = m.findParam("material"); pmat && !pmat->value.empty())
					meshParams.material = sharedEntityList.access(sa::mode::excl)->createEntity(type_v<prt::Material>, *pmat);

				m.getValueOrDefault
					("emitter",                      meshParams.isEmitter, false)
					("light_portal",                 meshParams.isLightPortal, false)
					("interpolate_normals",          meshParams.interpolateNormals, 0)
					("join_identical_vertices",      meshParams.removeDuplicateVertices, false)
					("reorient_triangles",           meshParams.swapTrianglesOrientation, false)
				;

				if(const auto pi = m.findParam("mesh"); pi && !pi->value.empty())
				{
					for(const auto i : mir(meshes.size()))
						if(meshes[i]->getEntityName() == pi->value)
						{
							if(removeObj)
							{
								sharedEntityList | sa::mode::excl |
									[&](auto &el)
									{
										el.removeEntity(*meshes[i]);
										removedObjects.set(i);
									};
							}
							else
								applyMeshParams(meshParams, *meshes[i]);
						}
				}

				if(const auto pi = m.findParam("mesh_id"); pi && !pi->value.empty())
				{
					if(auto it = rg::find_if(meshes, [&](const auto &o){ return o->getEntityId() == pi->value; }); it != meshes.end())
					{
						if(removeObj)
						{
							sharedEntityList | sa::mode::excl |
								[&](auto &el)
								{
									el.removeEntity(**it);
									removedObjects.set(std::distance(meshes.begin(), it));
								};
						}
						else
							applyMeshParams(meshParams, **it);
					}
					else
						pl::log("Assimp", pl::logtag::error, "Mesh ID {} not found.", pl::io::quoted{pi->value});
				}

				if(const auto pi = m.findParam("mesh_index"); pi && !pi->value.empty())
				{
					const uf32 index = *pi;

					if(index < meshes.size())
					{
						if(removeObj)
						{
							sharedEntityList | sa::mode::excl |
								[&](auto &el)
								{
									el.removeEntity(*meshes[index]);
									removedObjects.set(index);
								};
						}
						else
							applyMeshParams(meshParams, *meshes[index]);
					}
				}

				if(const auto pi = m.findParam("mesh_material"); pi && !pi->value.empty())
				{
					if(auto it = rg::find_if(materials, [&](const auto &o){ return o->getEntityName() == pi->value; }); it != materials.end())
					{
						for(const auto mIndex = std::distance(materials.begin(), it); const auto i : mir(scene->mNumMeshes))
						{
							if((if32)scene->mMeshes[i]->mMaterialIndex == mIndex && meshes[i])
							{
								if(removeObj)
								{
									sharedEntityList | sa::mode::excl |
										[&](auto &el)
										{
											el.removeEntity(*meshes[i]);
											removedObjects.set(i);
										};
								}
								else
									applyMeshParams(meshParams, *meshes[i]);
							}
						}
					}
					else
						pl::log("Assimp", pl::logtag::error, "Material named {} not found.", pl::io::quoted{pi->value});
				}
			}, std::identity{}, pl::policy::parallel_taskqueue_one);

			for(const auto i : mir(meshes.size()))
				if(removedObjects[i])
					meshes[i] = nullptr;
		}

		const auto instance = entityCollection.createEntity(type_v<prt::ObjectInstance>, prt::ObjectInstance::Factory, id);

		processNode(entityCollection, id, *scene->mRootNode, *instance, meshes);

		if(params.getValueOrDefault("group", true))
		{
			auto flags = instance->getFlags();
			flags.set(prt::InstanceFlagEnum::Group, true);
			instance->setFlags(flags);
		}

		if(params.contains("align"))
		{
			const int axis = params["align"].getValueOrDefault("axis", (int)0);
			const bool left = params["align"].getValueOrDefault("left", true);

			if(axis < 0 || axis > 3)
				throw AssimpException("Invalid axis for alignment. Must be 0, 1 or 2.");

			prt::Box box{pl::tag::inflowest};

			prt::traverseRecursiveTransform(*instance, [&](auto &inst, auto &matrix, auto &/*curMatrixInv*/){
				if(const auto object = std::to_address(inst.getObject()); object)
					box = box.join(object->computeAABox() * matrix);

				return prt::TraverseRecursiveState::Continue;
			});

			if(box.is_valid())
			{
				prt::Point3Dr tr{};

				if(left)
					tr.set(-box.v1[axis], axis);
				else
					tr.set(-box.v2[axis], axis);

				for(const prt::TransformTranslation tMat{tr}; auto &m : meshes)
					m->transformVertices(tMat);
			}
		}

		importer.FreeScene();

		return prt::DynEntityCast<prt::IRenderEntity>(instance);
	}

	void ImportExportAssimp::processNode(prt::EntityCollection &entityCollection, std::string_view id, const aiNode &node, prt::IObjectInstance &instance, std::span<const prt::EntityPtr<prt::PrimGroupTriangles>> meshes) const
	{
		const auto nodeInstance = entityCollection.createEntity(type_v<prt::ObjectInstance>, prt::ObjectInstance::Factory);

		dynamic_cast<prt::IRenderEntity &>(*nodeInstance).setEntityName(node.mName.C_Str());

		const auto nodeMatrix = std::invoke([&]{
				prt::Matrix m;
				for(const auto y : mir(4))
					pl::simd::vector_transform(m.v[y], m.v[y], [&](auto&& i, auto&&){ return node.mTransformation[y][i]; });
				return m;
			});

		{
			auto tr = prt::AugmentedTransformMatrix::Factory.construct_shared();
			tr->setMatrix(nodeMatrix);
			nodeInstance->setTransform(std::move(tr));
		}

		for(const auto &o : std::span{node.mMeshes, node.mNumMeshes}
			| rgv::transform([&](const auto &e) -> const auto& { return meshes[e]; })
			| rgv::remove_if([&](const auto &e){ return !e; })
		)
		{
			auto subInst = entityCollection.createEntity(type_v<prt::ObjectInstance>, prt::ObjectInstance::Factory);

			subInst->setObject(o);

			nodeInstance->addNode({.instance = std::move(subInst)});
		}

		instance.addNode({.instance = nodeInstance});

		for(const auto &child : std::span{node.mChildren, node.mNumChildren})
			processNode(entityCollection, id, *child, *nodeInstance, meshes);
	}

	void ImportExportAssimp::processMesh(prt::PrimGroupTriangles &obj, const aiMesh &mesh, std::span<const prt::EntityPtr<prt::Material>> materials) const
	{
		obj.setEntityName(mesh.mName.C_Str());

		obj.mData->primitives.mVts.resize(mesh.mNumVertices);

		if(mesh.HasNormals())
			obj.mData->primitives.mVtsNormals.resize(mesh.mNumVertices);

		if(mesh.HasFaces())
			obj.mData->primitives.mTriangles.resize(mesh.mNumFaces);

		obj.mData->uvMaps.reserve(AI_MAX_NUMBER_OF_TEXTURECOORDS);

		pl::tasks.sub_tasks(
			[&]
			{
				std::span{mesh.mVertices, mesh.mNumVertices} |
					pl::transform
					(
						obj.mData->primitives.mVts.begin(),
						[](auto&& s){ return prt::PrimGroupTriangles::VT{r_(s[0]), r_(s[1]), r_(s[2])}; },
						std::identity{}, pl::policy::parallel_taskqueue_t{}.chunk_size(1024*128)
					);
			},

			[&]
			{
				if(mesh.HasNormals())
				{
					std::span{mesh.mNormals, mesh.mNumVertices} |
						pl::transform
						(
							obj.mData->primitives.mVtsNormals.begin(),
							[](auto&& s){ return prt::PrimGroupTriangles::NT{r_(s[0]), r_(s[1]), r_(s[2])}; },
							std::identity{}, pl::policy::parallel_taskqueue_t{}.chunk_size(1024*128)
						);
				}
			},

			[&]
			{
				if(mesh.HasFaces())
				{
					auto &tris = obj.mData->primitives.mTriangles;

					mir(mesh.mNumFaces) |
						pl::for_each
						(
							[&](const auto i)
							{
								const auto &mv = mesh.mFaces[i].mIndices;
								tris[i] = {static_cast<PrimitiveIndex>(mv[0]), static_cast<PrimitiveIndex>(mv[1]), static_cast<PrimitiveIndex>(mv[2])};
							},
							std::identity{}, pl::policy::parallel_taskqueue_t{}.chunk_size(1024*128)
						);
				}
			},

			[&]
			{
				for(const auto i : mir(AI_MAX_NUMBER_OF_TEXTURECOORDS))
				{
					auto &uvmap = obj.mData->uvMaps.emplace_back();

					if(mesh.HasTextureCoords(i))
					{
						uvmap.uv.resize(mesh.mNumVertices);

						std::span{mesh.mTextureCoords[i], mesh.mNumVertices} |
							pl::transform
							(
								uvmap.uv.begin(),
								[](auto&& s){ return prt::PrimGroupTriangles::VT{r_(s[0]), r_(s[1]), r_(s[2])}; },
								std::identity{}, pl::policy::parallel_taskqueue_t{}.chunk_size(1024*128)
							);
					}
				}

				obj.mData->uvMaps.erase(
					rg::find_if(rgv::all(obj.mData->uvMaps) | rgv::reverse, [](auto&& e){ return !e.uv.empty(); }).base(),
					obj.mData->uvMaps.end()
				);
			}
		);

		obj.setMaterial(materials[mesh.mMaterialIndex]);
	}
}
