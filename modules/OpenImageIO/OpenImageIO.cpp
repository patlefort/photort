/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "OpenImageIO.hpp"

#include <PhotoRT/Core/ColorProfile.hpp>
#include <PhotoRT/Bitmap/BitmapHDR16.hpp>

#include <OpenImageIO/imageio.h>
#include <OpenImageIO/filesystem.h>

//#include <boost/assign/list_inserter.hpp>

#include <patlib/algorithm.hpp>
#include <patlib/task.hpp>

namespace PRTOpenImageIO
{
	PRT_IMPORTEXPORTER_DEFINE("oiio", ImportExportBitmapOpenImageIO)

	const char *OpenImageIOException::module() const noexcept
		{ return "ModuleOpenImageIO"; }

	ImportExportBitmapOpenImageIO::ImportExportBitmapOpenImageIO()
	{
		const auto extMap = OIIO::get_extension_map();
		const std::string_view inputFormats = OIIO::get_string_attribute("input_format_list");
		const std::string_view outputFormats = OIIO::get_string_attribute("output_format_list");

		const auto buildList = [&](auto listStr, auto &formatList, auto &mimeMap)
			{
				for(const auto format : listStr | std::ranges::views::split(',') | pl::string_adaptor<>)
				{
					for(const auto &ext : extMap.at(format))
					{
						formatList.insert(format);
						mimeMap.left.insert({ext, std::string{"custom:"} + format});
					}
				}
			};

		std::set<std::string> inputFormatList, outputFormatList;

		buildList(inputFormats, inputFormatList, mMimeTypesImport);
		buildList(outputFormats, outputFormatList, mMimeTypesExport);

		const auto insertMimeType = [&](std::string format, std::string ext, std::string mimeType)
			{
				if(inputFormatList.contains(format))
					mMimeTypesImport.left.insert({ext, mimeType});
				if(outputFormatList.contains(format))
					mMimeTypesExport.left.insert({std::move(ext), std::move(mimeType)});
			};

		insertMimeType("png", "png", "image/png");
		insertMimeType("jpg", "jpg", "image/png");
		insertMimeType("jpg", "jpeg", "image/jpeg");
		insertMimeType("ico", "ico", "image/vnd.microsoft.icon");
		insertMimeType("bmp", "bmp", "image/bmp");
		insertMimeType("ico", "ico", "image/vnd.microsoft.icon");
		insertMimeType("targa", "tga", "image/targa");
		insertMimeType("openexr", "exr", "image/x-exr");
		insertMimeType("openexr", "sxr", "image/x-exr");
		insertMimeType("openexr", "mxr", "image/x-exr");
		insertMimeType("hdr", "hdr", "image/vnd.radiance");
		insertMimeType("hdr", "rgbe", "image/vnd.radiance");
		insertMimeType("tiff", "tif", "image/tiff");
		insertMimeType("tiff", "tiff", "image/tiff");
		insertMimeType("webp", "webp", "image/webp");
		insertMimeType("gif", "gif", "image/gif");
		insertMimeType("pnm", "dds", "image/x-portable-anymap");
		insertMimeType("pnm", "dds", "image/x-portable-bitmap");
		insertMimeType("pnm", "dds", "image/x-portable-graymap");
		insertMimeType("pnm", "dds", "image/x-portable-pixmap");
		insertMimeType("openvdb", "vdb", "model/vdb");
		insertMimeType("sgi", "sgi", "image/sgi");
		insertMimeType("sgi", "rgb", "image/sgi");
		insertMimeType("sgi", "rgba", "image/sgi");
		insertMimeType("sgi", "bw", "image/sgi");
		insertMimeType("sgi", "int", "image/sgi");
		insertMimeType("sgi", "inta", "image/sgi");
		insertMimeType("sgi", "inta", "image/sgi");
	}

	prt::ParameterTree ImportExportBitmapOpenImageIO::getConfig() const
	{
		return {
		};
	}

	void ImportExportBitmapOpenImageIO::setConfig(const prt::ParameterTree &)
	{
	}

	std::vector<std::string> ImportExportBitmapOpenImageIO::getGenericTypes() const
	{
		return {"image"};
	}

	OIIO_NAMESPACE_USING

	bool ImportExportBitmapOpenImageIO::canImport(std::string_view mimeType) const noexcept
		{ return mMimeTypesImport.right.find(std::string(mimeType)) != mMimeTypesImport.right.end(); }

	bool ImportExportBitmapOpenImageIO::canExport(std::string_view mimeType) const noexcept
		{ return mMimeTypesExport.right.find(std::string(mimeType)) != mMimeTypesExport.right.end(); }

	std::string ImportExportBitmapOpenImageIO::getEntityType(std::string_view /*mimeType*/) const
		{ return "texture"; }

	prt::ExtensionMimeTypeMap ImportExportBitmapOpenImageIO::getMimeTypesImport() const
		{ return mMimeTypesImport; }
	prt::ExtensionMimeTypeMap ImportExportBitmapOpenImageIO::getMimeTypesExport() const
		{ return mMimeTypesExport; }

	bool ImportExportBitmapOpenImageIO::isStreamable(std::string_view /*mimeType*/) const noexcept
		{ return false; }


	#if OIIO_PLUGIN_VERSION < 22
		struct RAIIImageInput
		{
			ImageInput *input = nullptr;

			template <typename... Args_>
			RAIIImageInput(Args_&&... args) { input = ImageInput::open(PL_FWD(args)...); }
			~RAIIImageInput() { if(input) { input->close(); ImageInput::destroy(input); } }
		};

		struct RAIIImageOutput
		{
			ImageOutput *output = nullptr;

			template <typename... Args_>
			RAIIImageOutput(Args_&&... args) { output = ImageOutput::create(PL_FWD(args)...); }
			~RAIIImageOutput() { if(output) { output->close(); ImageOutput::destroy(output); } }
		};
	#endif


	[[nodiscard]] prt::EntityPtrBase readImage(const auto &input, prt::EntityCollection &entityCollection, std::string id)
	{
		prt::EntityPtrBase res;
		prt::BitmapBase *baseBmp = nullptr;

		const auto &spec = input->spec();

		const auto tryReadImage = [&](const auto type, auto *data)
			{
				if(!input->read_image(0, 0, 0, -1, type, data))
					throw OpenImageIOException("Reading image failed.");
			};

		if((spec.format == TypeDesc::DOUBLE) && spec.nchannels == 1)
		{
			auto bmp = entityCollection.createEntity(type_v<prt::BitmapEntity<prt::TextureBitmapBuffer<double>>>, prt::BitmapEntity<prt::TextureBitmapBuffer<double>>::Factory, std::move(id));
			bmp->allocate(prt::Dir3Di{spec.width, spec.height, spec.depth});

			const auto size = bmp->getNbPixels();
			prt::VectorNoInit<double> data(size * spec.nchannels);
			tryReadImage(TypeDesc::DOUBLE, data.data());

			data | pl::copy(bmp->buffer().begin(), pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128));

			baseBmp = std::to_address(bmp);
			res = std::move(bmp);
		}
		else if((spec.format == TypeDesc::FLOAT || spec.format == TypeDesc::HALF) && spec.nchannels == 1)
		{
			auto bmp = entityCollection.createEntity(type_v<prt::BitmapEntity<prt::TextureBitmapBuffer<float>>>, prt::BitmapEntity<prt::TextureBitmapBuffer<float>>::Factory, std::move(id));
			bmp->allocate(prt::Dir3Di{spec.width, spec.height, spec.depth});

			const auto size = bmp->getNbPixels();

			{
				prt::VectorNoInit<float> data(size * spec.nchannels);
				tryReadImage(TypeDesc::FLOAT, data.data());

				data | pl::copy(bmp->buffer().begin(), pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128));
			}

			baseBmp = std::to_address(bmp);
			res = std::move(bmp);
		}
		else if(spec.format == TypeDesc::FLOAT)
		{
			if(spec.nchannels == 4)
			{
				auto bmp = entityCollection.createEntity(type_v<prt::BitmapEntity<prt::TextureBitmapBuffer<prt::Color32>>>, prt::BitmapEntity<prt::TextureBitmapBuffer<prt::Color32>>::Factory, std::move(id));
				bmp->allocate(prt::Dir3Di{spec.width, spec.height, spec.depth});

				const auto size = bmp->getNbPixels();

				{
					prt::VectorNoInit<float> data(size * spec.nchannels);
					tryReadImage(TypeDesc::FLOAT, data.data());

					mir(size) |
						pl::transform(bmp->buffer().begin(), [&](const auto i){ const auto c = i*4; return prt::Color32{data[c], data[c+1], data[c+2], data[c+3]}; }, std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128));
				}

				baseBmp = std::to_address(bmp);
				res = std::move(bmp);
			}
			else if(spec.nchannels == 3)
			{
				auto bmp = entityCollection.createEntity(type_v<prt::BitmapEntity<prt::TextureBitmapBuffer<prt::Color3_32>>>, prt::BitmapEntity<prt::TextureBitmapBuffer<prt::Color3_32>>::Factory, std::move(id));
				bmp->allocate(prt::Dir3Di{spec.width, spec.height, spec.depth});

				const auto size = bmp->getNbPixels();

				{
					prt::VectorNoInit<float> data(size * spec.nchannels);
					tryReadImage(TypeDesc::FLOAT, data.data());

					mir(size) |
						pl::transform(bmp->buffer().begin(), [&](const auto i){ const auto c = i*3; return prt::Color3_32{data[c], data[c+1], data[c+2]}; }, std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128));
				}

				baseBmp = std::to_address(bmp);
				res = std::move(bmp);
			}
		}
		else if(spec.format == TypeDesc::DOUBLE)
		{
			if(spec.nchannels == 4)
			{
				auto bmp = entityCollection.createEntity(type_v<prt::BitmapEntity<prt::TextureBitmapBuffer<prt::Color64>>>, prt::BitmapEntity<prt::TextureBitmapBuffer<prt::Color64>>::Factory, std::move(id));
				bmp->allocate(prt::Dir3Di{spec.width, spec.height, spec.depth});

				const auto size = bmp->getNbPixels();

				{
					prt::VectorNoInit<double> data(size * spec.nchannels);
					tryReadImage(TypeDesc::DOUBLE, data.data());

					mir(size) |
						pl::transform(bmp->buffer().begin(), [&](const auto i){ const auto c = i*4; return prt::Color64{data[c], data[c+1], data[c+2], data[c+3]}; }, std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128));
				}

				baseBmp = std::to_address(bmp);
				res = std::move(bmp);
			}
			else if(spec.nchannels == 3)
			{
				auto bmp = entityCollection.createEntity(type_v<prt::BitmapEntity<prt::TextureBitmapBuffer<prt::Color3_64>>>, prt::BitmapEntity<prt::TextureBitmapBuffer<prt::Color3_64>>::Factory, std::move(id));
				bmp->allocate(prt::Dir3Di{spec.width, spec.height, spec.depth});

				const auto size = bmp->getNbPixels();

				{
					prt::VectorNoInit<double> data(size * spec.nchannels);
					tryReadImage(TypeDesc::DOUBLE, data.data());

					mir(size) |
						pl::transform(bmp->buffer().begin(), [&](const auto i){ const auto c = i*3; return prt::Color3_64{data[c], data[c+1], data[c+2]}; }, std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128));
				}

				baseBmp = std::to_address(bmp);
				res = std::move(bmp);
			}
		}
		else if(spec.format == TypeDesc::HALF)
		{
			if(spec.nchannels == 4)
			{
				auto bmp = entityCollection.createEntity(type_v<prt::BitmapEntity<prt::TextureBitmapBuffer<prt::Color16>>>, prt::BitmapEntity<prt::TextureBitmapBuffer<prt::Color16>>::Factory, std::move(id));
				bmp->allocate(prt::Dir3Di{spec.width, spec.height, spec.depth});

				const auto size = bmp->getNbPixels();

				{
					std::vector<half_float::half> data(size * spec.nchannels);
					tryReadImage(TypeDesc::HALF, data.data());

					mir(size) |
						pl::transform(bmp->buffer().begin(), [&](const auto i){ const auto c = i*4; return prt::Color16{data[c], data[c+1], data[c+2], data[c+3]}; }, std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128));
				}

				baseBmp = std::to_address(bmp);
				res = std::move(bmp);
			}
			else if(spec.nchannels == 3)
			{
				auto bmp = entityCollection.createEntity(type_v<prt::BitmapEntity<prt::TextureBitmapBuffer<prt::Color3_16>>>, prt::BitmapEntity<prt::TextureBitmapBuffer<prt::Color3_16>>::Factory, std::move(id));
				bmp->allocate(prt::Dir3Di{spec.width, spec.height, spec.depth});

				const auto size = bmp->getNbPixels();

				{
					std::vector<half_float::half> data(size * spec.nchannels);
					tryReadImage(TypeDesc::HALF, data.data());

					mir(size) |
						pl::transform(bmp->buffer().begin(), [&](const auto i){ const auto c = i*3; return prt::Color3_16{data[c], data[c+1], data[c+2]}; }, std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128));
				}

				baseBmp = std::to_address(bmp);
				res = std::move(bmp);
			}
		}
		else
		{
			switch(spec.nchannels)
			{
			case 1:

				{
					auto bmp = entityCollection.createEntity(type_v<prt::BitmapEntity<prt::TextureBitmapBuffer<u8>>>, prt::BitmapEntity<prt::TextureBitmapBuffer<u8>>::Factory, std::move(id));

					bmp->allocate(prt::Dir3Di{spec.width, spec.height, spec.depth});

					const auto size = bmp->getNbPixels();

					{
						prt::VectorNoInit<u8> data(size * spec.nchannels);
						tryReadImage(TypeDesc::UINT8, data.data());

						data | pl::transform(bmp->buffer().begin(), [](auto&& c){ return c; }, std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128));
					}

					baseBmp = std::to_address(bmp);
					res = std::move(bmp);
				}

				break;
			case 2:

				{
					auto bmp = entityCollection.createEntity(type_v<prt::BitmapEntity<prt::TextureBitmapBuffer<u8>>>, prt::BitmapEntity<prt::TextureBitmapBuffer<u8>>::Factory, std::move(id));

					bmp->allocate(prt::Dir3Di{spec.width, spec.height, spec.depth});

					const auto size = bmp->getNbPixels();

					{
						prt::VectorNoInit<u8> data(size * spec.nchannels);
						tryReadImage(TypeDesc::UINT8, data.data());

						mir(size) |
							pl::transform(bmp->buffer().begin(), [&](const auto i){ return data[i*2]; }, std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128));
					}

					baseBmp = std::to_address(bmp);
					res = std::move(bmp);
				}

				break;
			case 3:

				{
					auto bmp = entityCollection.createEntity(type_v<prt::BitmapEntity<prt::TextureBitmapBuffer<prt::ColorInt24>>>, prt::BitmapEntity<prt::TextureBitmapBuffer<prt::ColorInt24>>::Factory, std::move(id));

					bmp->allocate(prt::Dir3Di{spec.width, spec.height, spec.depth});

					const auto size = bmp->getNbPixels();

					{
						prt::VectorNoInit<u8> data(size * spec.nchannels);
						tryReadImage(TypeDesc::UINT8, data.data());

						mir(size) |
							pl::transform(bmp->buffer().begin(), [&](const auto i){ const auto c = i*3; return prt::ColorInt24{data[c], data[c+1], data[c+2]}; }, std::identity{}, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128));
					}

					baseBmp = std::to_address(bmp);
					res = std::move(bmp);
				}

				break;
			case 4:

				{
					auto bmp = entityCollection.createEntity(type_v<prt::BitmapEntity<prt::TextureBitmapBuffer<u32>>>, prt::BitmapEntity<prt::TextureBitmapBuffer<u32>>::Factory, std::move(id));

					bmp->allocate(prt::Dir3Di{spec.width, spec.height, spec.depth});

					const auto size = bmp->getNbPixels();

					{
						prt::VectorNoInit<u32> data(size);
						tryReadImage(TypeDesc::UINT8, data.data());

						data | pl::copy(bmp->buffer().begin(), pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128));
					}

					baseBmp = std::to_address(bmp);
					res = std::move(bmp);
				}

				break;
			default:
				throw OpenImageIOException("File format not supported for \"" + id + "\".");
			}
		}

		plassert(baseBmp);

		if(const auto colorSpace = spec.get_string_attribute("oiio:ColorSpace", ""); colorSpace == "GammaCorrected")
		{
			if(const auto gamma = spec.get_float_attribute("oiio:Gamma", 0); gamma)
				prt::BitmapAlgo::gammaCorrect(*baseBmp, gamma, 1, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128));
		}
		else if(colorSpace == "sRGB")
		{
			baseBmp->transform(*baseBmp, *baseBmp, [](auto &&p1, auto &&/*p2*/)
			{
				prt::ColorProfileSRGB<std::decay_t<decltype(p1)>> cp;
				return cp.inverse(p1);
			}, pl::bool_false, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128));
		}
		else if(colorSpace == "AdobeRGB")
		{
			baseBmp->transform(*baseBmp, *baseBmp, [](auto &&p1, auto &&/*p2*/)
			{
				prt::ColorProfileAdobeRGB<std::decay_t<decltype(p1)>> cp;
				return cp.inverse(p1);
			}, pl::bool_false, pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128));
		}

		return res;
	}

	prt::EntityPtrBase ImportExportBitmapOpenImageIO::importEntity(prt::EntityCollection &entityCollection, std::span<const std::byte> data, std::string_view /*genericType*/, std::string_view /*mimeType*/, std::string id, const prt::ParameterTree &)
	{
		Filesystem::IOMemReader memReader{static_cast<const void *>(data.data()), data.size()};

		#if OIIO_PLUGIN_VERSION >= 22
			auto input = ImageInput::open("mem", nullptr, &memReader);
		#else
			RAIIImageInput raii("mem", nullptr, &memReader);
			auto input = raii.input;
		#endif

		if(!input)
			throw OpenImageIOException("Error creating image input.");

		return readImage(input, entityCollection, std::move(id));
	}

	prt::EntityPtrBase ImportExportBitmapOpenImageIO::importEntity(prt::EntityCollection &entityCollection, const std::filesystem::path &filename, std::string_view /*genericType*/, std::string_view /*mimeType*/, std::string id, const prt::ParameterTree &)
	{
		#if OIIO_PLUGIN_VERSION >= 22
			auto input = ImageInput::open(filename.string());
		#else
			RAIIImageInput raii(filename.string());
			auto input = raii.input;
		#endif

		if(!input)
			throw OpenImageIOException("Error opening file \"" + filename.string() + "\" for reading.");

		return readImage(input, entityCollection, std::move(id));
	}


	using OutputVector = std::vector<std::byte, pl::aligned_allocator<std::byte, pl::allocator_noinit<std::byte>>>;

	template <typename BitmapType_>
	[[nodiscard]] OutputVector writePixels(const BitmapType_ &bmp)
	{
		using PixelType = typename BitmapType_::PixelType;

		const auto nb = bmp.getNbPixels();
		OutputVector pixVec(nb * sizeof(PixelType), OutputVector::allocator_type{{}, alignof(PixelType)});

		mir(nb)
			| rgv::transform([&](const auto i){ return bmp.buffer().getDirectOffset(i); })
			| pl::copy(reinterpret_cast<PixelType *>(pixVec.data()));

		return pixVec;
	}


	template <typename T_>
	[[nodiscard]] const std::byte *getBitmapBuffer(const prt::BitmapBase &bmp, OutputVector &pixVec)
	{
		const auto pixelType = bmp.getUnderlyingPixelType();

		if(pixelType.type() == typeid(type_c<T_>))
		{
			if(bmp.isContiguous())
				return reinterpret_cast<const std::byte *>(bmp.getRawData());

			pl::assign_copy(pixVec, bmp.getNbPixels() * sizeof(T_), OutputVector::allocator_type{OutputVector::allocator_type::upstream_allocator_type{}, alignof(T_)});
			bmp.copyIntoContiguousMemory(pixVec.data());
			return reinterpret_cast<const std::byte *>(pixVec.data());
		}

		return nullptr;
	}

	void writeImage(auto &output, const std::string &filename, prt::EntityCollection &entityCollection, const prt::IExportHook &entity, const prt::ParameterTree &params)
	{
		const auto &bmp = dynamic_cast<const prt::BitmapBase &>(entity);
		prt::EntityPtr<prt::IColorProfile> colorProfile;

		if(const auto p = params.findParam("colorprofile"); p)
			colorProfile = entityCollection.createEntity<prt::IColorProfile>(type_v<prt::IColorProfile>, *p);

		TypeDesc typedesc;
		int channels;
		const std::byte *data = nullptr;
		OutputVector outVec;

		if( (data = getBitmapBuffer<float>(bmp, outVec)) )
		{
			typedesc = TypeDesc::FLOAT;
			channels = 1;
		}
		else if( (data = getBitmapBuffer<double>(bmp, outVec)) )
		{
			typedesc = TypeDesc::DOUBLE;
			channels = 1;
		}
		else if( (data = getBitmapBuffer<prt::Color3_32>(bmp, outVec)) )
		{
			typedesc = TypeDesc::FLOAT;
			channels = 3;
		}
		else if( (data = getBitmapBuffer<prt::Color32>(bmp, outVec)) )
		{
			typedesc = TypeDesc::FLOAT;
			channels = 4;
		}
		else if( (data = getBitmapBuffer<prt::Color3_64>(bmp, outVec)) )
		{
			typedesc = TypeDesc::DOUBLE;
			channels = 3;
		}
		else if( (data = getBitmapBuffer<prt::Color64>(bmp, outVec)) )
		{
			typedesc = TypeDesc::DOUBLE;
			channels = 4;
		}
		else if( (data = getBitmapBuffer<prt::Color3_16>(bmp, outVec)) )
		{
			typedesc = TypeDesc::HALF;
			channels = 3;
		}
		else if( (data = getBitmapBuffer<prt::Color16>(bmp, outVec)) )
		{
			typedesc = TypeDesc::HALF;
			channels = 4;
		}
		else if( (data = getBitmapBuffer<prt::ColorInt24>(bmp, outVec)) )
		{
			typedesc = TypeDesc::UINT8;
			channels = 3;
		}
		else if( (data = getBitmapBuffer<u32>(bmp, outVec)) )
		{
			typedesc = TypeDesc::UINT8;
			channels = 4;
		}
		else
		{
			typedesc = TypeDesc::UINT8;
			channels = 4;

			const auto nb = bmp.getNbPixels();
			outVec.resize(nb * sizeof(u32));
			u32 *pix = reinterpret_cast<u32 *>(outVec.data());
			data = outVec.data();

			for(const auto i : mir(nb))
				prt::convertFromColor(bmp.getPixel(i), *pix++);
		}


		ImageSpec spec{static_cast<int>(bmp.getWidth()), static_cast<int>(bmp.getHeight()), channels, typedesc};

		if(colorProfile)
		{
			if(dynamic_cast<prt::ColorProfileVirtual<prt::ColorProfileSRGB> *>(std::to_address(colorProfile)) != nullptr)
				spec.attribute("oiio:ColorSpace", "sRGB");
			else if(dynamic_cast<prt::ColorProfileVirtual<prt::ColorProfileAdobeRGB> *>(std::to_address(colorProfile)) != nullptr)
				spec.attribute("oiio:ColorSpace", "AdobeRGB");
			else
				spec.attribute("oiio:ColorSpace", "Linear");
		}
		else
			spec.attribute("oiio:ColorSpace", "Linear");

		output->open(filename, spec);
		output->write_image(typedesc, reinterpret_cast<const u8 *>(data));
	}

	void ImportExportBitmapOpenImageIO::exportEntity(prt::EntityCollection &entityCollection, const prt::IExportHook &entity, const std::filesystem::path &filename, std::string_view /*mimeType*/, const prt::ParameterTree &params)
	{
		#if OIIO_PLUGIN_VERSION >= 22
			auto output = ImageOutput::create(filename.string());
		#else
			RAIIImageOutput raii{filename.string()};
			auto output = raii.output;
		#endif

		if(!output)
			throw OpenImageIOException("Error opening file \"" + filename.string() + "\" for writing.");

		writeImage(output, filename.string(), entityCollection, entity, params);
	}

	std::vector<std::byte> ImportExportBitmapOpenImageIO::exportEntity(prt::EntityCollection &entityCollection, const prt::IExportHook &entity, std::string_view mimeType, const prt::ParameterTree &params)
	{
		Filesystem::IOVecOutput vecOutput;
		const auto ext = [&]
			{
				if(auto it = mMimeTypesImport.right.find(std::string{mimeType}); it != mMimeTypesImport.right.end())
					return it->first;

				throw OpenImageIOException("Invalid mime-type \"" + mimeType + "\" for writing.");
			}();
		const auto filename = std::string{"out."} + ext;

		#if OIIO_PLUGIN_VERSION >= 22
			auto output = ImageOutput::create(filename, &vecOutput);
		#else
			RAIIImageOutput raii{filename, &vecOutput};
			auto output = raii.output;
		#endif

		if(!output)
			throw OpenImageIOException("Error creating image output.");

		writeImage(output, filename, entityCollection, entity, params);

		std::vector<std::byte> data(vecOutput.buffer().size());
		auto trRange = vecOutput.buffer() | pl::static_cast_view<std::byte>;
		trRange | pl::copy(data.begin(), pl::policy::parallel_taskqueue_or_parallel_t{}.chunk_size(1024*128));

		return data;
	}
}
