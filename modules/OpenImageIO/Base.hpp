/*
	PhotoRT

	Copyright (C) 2016 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <PhotoRT/Core/Base.hpp>
#include <PhotoRT/Core/RenderEntityFactory.hpp>
#include <PhotoRT/Core/RenderEntityImportExport.hpp>
#include <PhotoRT/Bitmap/Bitmap.hpp>

#ifdef PrtModule_OpenImageIO_SHARED
	#ifdef PrtModule_OpenImageIO_EXPORTS
		#define PRTMODULE_OPENIMAGEIO_API BOOST_SYMBOL_EXPORT
	#else
		#define PRTMODULE_OPENIMAGEIO_API BOOST_SYMBOL_IMPORT
	#endif
#else
	#define PRTMODULE_OPENIMAGEIO_API
#endif

namespace PRTOpenImageIO
{
	PRT_IMPORT_NS

	namespace rg = ranges;
	namespace rgv = rg::views;
}
